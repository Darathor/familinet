<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting;

/**
 * @name \Project\Accounting\AccountingServices
 */
class AccountingServices extends \Zend\ServiceManager\ServiceManager
{
	use \Change\Services\ServicesCapableTrait;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	public function setApplicationServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
		return $this;
	}

	/**
	 * @return array<alias => className>
	 */
	protected function loadInjectionClasses()
	{
		$classes = $this->getApplication()->getConfiguration('Rbs/Storelocator/Services');
		return is_array($classes) ? $classes : [];
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices)
	{
		$this->setApplication($application);
		$this->setApplicationServices($applicationServices);

		parent::__construct(['shared_by_default' => true]);

		// MovementDbRepository: DbProvider
		$class = $this->getInjectedClassName('MovementDbRepository', \Project\Accounting\Movements\DbRepository::class);
		$this->setAlias('MovementDbRepository', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Movements\DbRepository $repository */
			$repository = new $requestedName;
			$repository->setDbProvider($this->applicationServices->getDbProvider());
			return $repository;
		});

		// MovementManager: Application, MovementDbRepository
		$class = $this->getInjectedClassName('MovementManager',\Project\Accounting\Movements\Manager::class);
		$this->setAlias('MovementManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Movements\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbRepository($this->get('MovementDbRepository'));
			return $manager;
		});

		// InvoiceDbRepository: DbProvider, DocumentManager
		$class = $this->getInjectedClassName('InvoiceDbRepository', \Project\Accounting\Invoices\DbRepository::class);
		$this->setAlias('InvoiceDbRepository', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Invoices\DbRepository $repository */
			$repository = new $requestedName;
			$repository->setDbProvider($this->applicationServices->getDbProvider())
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $repository;
		});

		// InvoiceManager: Application, InvoiceDbRepository
		$class = $this->getInjectedClassName('InvoiceManager',\Project\Accounting\Invoices\Manager::class);
		$this->setAlias('InvoiceManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Invoices\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbRepository($this->get('InvoiceDbRepository'));
			return $manager;
		});

		// TransferDbRepository: DbProvider, DocumentManager
		$class = $this->getInjectedClassName('TransferDbRepository', \Project\Accounting\Transfers\DbRepository::class);
		$this->setAlias('TransferDbRepository', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Transfers\DbRepository $repository */
			$repository = new $requestedName;
			$repository->setDbProvider($this->applicationServices->getDbProvider())
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $repository;
		});

		// TransferManager: Application, TransferDbRepository
		$class = $this->getInjectedClassName('TransferManager',\Project\Accounting\Transfers\Manager::class);
		$this->setAlias('TransferManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName) {
			/** @var \Project\Accounting\Transfers\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbRepository($this->get('TransferDbRepository'));
			return $manager;
		});
	}

	/**
	 * @return \Project\Accounting\Movements\Manager
	 * @api
	 */
	public function getMovementManager()
	{
		return $this->get('MovementManager');
	}

	/**
	 * @return \Project\Accounting\Invoices\Manager
	 * @api
	 */
	public function getInvoiceManager()
	{
		return $this->get('InvoiceManager');
	}

	/**
	 * @return \Project\Accounting\Transfers\Manager
	 * @api
	 */
	public function getTransferManager()
	{
		return $this->get('TransferManager');
	}
}