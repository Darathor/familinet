(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('ProjectAccountingMovements', ['$scope', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter',
		'RbsChange.ErrorFormatter', 'RbsChange.Navigation', function(scope, REST, i18n, NotificationCenter, ErrorFormatter, Navigation) {
			scope.keys = Object.keys;

			scope.pagination = {
				currentPage: 1,
				totalItems: 0,
				itemsPerPage: null,
				currentItems: [],
				sort: 'movement_id',
				desc: true,
				loading: false
			};

			scope.filters = {
				userId: null,
				sourceId: null
			};

			scope.$on('Navigation.saveContext', function(event, args) {
				var contextData = {
					filters: scope.filters
				};
				args.context.savedData('projectAccountingMovements', contextData);
			});

			var currentContext = Navigation.getCurrentContext();
			if (currentContext) {
				var contextData = currentContext.savedData('projectAccountingMovements');
				if (angular.isObject(contextData)) {
					scope.filters = contextData.filters;
				}
			}

			function reload() {
				if (scope.pagination.busy || !scope.pagination.itemsPerPage) {
					return;
				}
				scope.pagination.loading = true;
				var params = {
					offset: (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage,
					limit: scope.pagination.itemsPerPage,
					sort: scope.pagination.sort,
					desc: scope.pagination.desc,
					userId: scope.filters.userId,
					sourceId: scope.filters.sourceId
				};
				REST.call(REST.getBaseUrl('projectAccounting/movements'), params).then(
					function(data) {
						scope.pagination.totalItems = data.pagination.count;
						scope.pagination.currentItems = data.resources;
						scope.pagination.loading = false;
					},
					function(error) {
						console.error(error);
						NotificationCenter.error(i18n.trans('m.project.accounting.admin.load_movements_error | ucf'),
							ErrorFormatter.format(error));
						scope.pagination.currentItems = [];
						scope.pagination.loading = false;
					}
				);
			}

			scope.reload = reload;

			scope.$watch('pagination.currentPage', reload);
			scope.$watch('pagination.itemsPerPage', reload);
			scope.$watch('pagination.sort', reload);
			scope.$watch('pagination.desc', reload);
			scope.$watch('filters', reload, true);
		}
	]);
})();