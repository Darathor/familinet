<?php
namespace Project\Accounting\Documents;

/**
 * @name \Project\Accounting\Documents\Invoice
 */
class Invoice extends \Compilation\Project\Accounting\Documents\Invoice
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function (\Change\Documents\Events\Event $event) {
			$this->buildLabel($event);
			$this->refreshCompiledFields($event);
		}, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE,
			function (\Change\Documents\Events\Event $event) { $this->refreshCompiledFields($event); }, 5);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED,
			function (\Change\Documents\Events\Event $event) { $this->onRefreshMovements($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED,
			function (\Change\Documents\Events\Event $event) { $this->onRefreshMovements($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED,
			function (\Change\Documents\Events\Event $event) { $this->onDeleteMovements($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function buildLabel(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$this->setLabel($accountingServices->getInvoiceManager()->getNextLabel());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function refreshCompiledFields(\Change\Documents\Events\Event $event)
	{
		$propertiesErrors = $event->getParam('propertiesErrors');

		$this->getParticipants()->add($this->getPayer());

		// Calculate lines amount.
		$linesAmount = 0;
		$linesAmountByUserId = [];
		foreach ($this->getLines() as $index => $line)
		{
			$quantity = $line->getQuantity();
			$unitPrice = $line->getUnitPrice();
			$amount = $line->getAmount();
			if ($unitPrice && $amount === null)
			{
				$amount = $unitPrice * $quantity;
				$line->setAmount($amount);
			}
			elseif ($unitPrice === null && $amount)
			{
				$line->setUnitPrice($amount / $quantity);
			}
			elseif ($amount === null && $unitPrice === null)
			{
				$line->setAmount(0);
				$line->setUnitPrice(0);
			}
			elseif (!\Change\Stdlib\FloatUtils::equals($amount, $quantity * $unitPrice, 0.01))
			{
				$propertiesErrors['lines'][] = $event->getApplicationServices()->getI18nManager()->trans(
					'm.project.accounting.webapp.line_amount_error', ['ucf'], ['NUMBER' => $index + 1]
				);
			}

			$linesAmount += $amount;

			// Distribution.
			$errors = [];
			$distribution = $this->normalizeDistribution((array)$line->getDistribution(), $errors);
			foreach ($errors as $error)
			{
				$error['NUMBER'] = $index + 1;
				$propertiesErrors['lines'][] = $event->getApplicationServices()->getI18nManager()->trans(
					'm.project.accounting.webapp.line_distribution_invalid_part', ['ucf'], $error
				);
			}
			if (!\Change\Stdlib\FloatUtils::equals(\array_sum($distribution), $quantity, 0.01))
			{
				$propertiesErrors['lines'][] = $event->getApplicationServices()->getI18nManager()->trans(
					'm.project.accounting.webapp.line_distribution_invalid_total', ['ucf'],
					['NUMBER' => $index + 1, 'QUANTITY' => $quantity, 'TOTAL' => \array_sum($distribution)]
				);
			}

			foreach ($distribution as $id => $value)
			{
				$linesAmountByUserId[$id] = ($linesAmountByUserId[$id] ?? 0) + ($value * $line->getUnitPrice());
			}

			$line->setDistribution($distribution);
		}
		$this->setLinesAmount($linesAmount);

		// Calculate modifiers amount.
		$modifiersAmount = 0;
		foreach ($this->getModifiers() as $index => $modifier)
		{
			$modifiersAmount += $modifier->getAmount();

			// Distribution.
			if ($modifier->getProrata())
			{
				$distribution = [];
				foreach ($this->getParticipants() as $participant)
				{
					$participantId = $participant->getId();
					$distribution[$participantId] = ($linesAmountByUserId[$participantId] ?? 0) / $linesAmount;
				}
				$modifier->setDistribution($distribution);
			}
			else
			{
				$errors = [];
				$distribution = $this->normalizeDistribution((array)$modifier->getDistribution(), $errors);
				$modifier->setDistribution($distribution);
				foreach ($errors as $error)
				{
					$error['NUMBER'] = $index + 1;
					$propertiesErrors['modifiers'][] = $event->getApplicationServices()->getI18nManager()->trans(
						'm.project.accounting.webapp.modifier_distribution_invalid_part', ['ucf'], $error
					);
				}
				if (!\Change\Stdlib\FloatUtils::equals(\array_sum($distribution), 1, 0.01))
				{
					$propertiesErrors['lines'][] = $event->getApplicationServices()->getI18nManager()->trans(
						'm.project.accounting.webapp.modifier_distribution_invalid_total', ['ucf'],
						['NUMBER' => $index + 1, 'QUANTITY' => 1, 'TOTAL' => \array_sum($distribution)]
					);
				}
			}
		}
		$this->setModifiersAmount($modifiersAmount);

		$calculatedAmount = $linesAmount + $modifiersAmount;
		$paidAmount = $this->getPaidAmount();
		if ($paidAmount !== null)
		{
			if (!\Change\Stdlib\FloatUtils::equals($paidAmount, $calculatedAmount, 0.01))
			{
				$propertiesErrors['paidAmount'][] = $event->getApplicationServices()->getI18nManager()->trans(
					'm.project.accounting.webapp.invoice_paid_amount_error', ['ucf'],
					['AMOUNT' => $paidAmount, 'CALCULATED' => $calculatedAmount]
				);
			}
		}
		else
		{
			$this->setPaidAmount($calculatedAmount);
		}

		$event->setParam('propertiesErrors', $propertiesErrors);
	}

	/**
	 * @param array $distribution
	 * @param array $errors
	 * @return array
	 */
	protected function normalizeDistribution(array $distribution, array &$errors)
	{
		$newDistribution = [];
		foreach ($this->getParticipants() as $participant)
		{
			$givenValue = $distribution[$participant->getId()];
			if (!$givenValue)
			{
				continue;
			}
			if (\strpos($givenValue, '/'))
			{
				[$quotient, $divisor] = explode('/', $givenValue);
				if (!\ctype_digit($quotient) || !\ctype_digit($divisor))
				{
					$errors[] = ['PART' => $givenValue, 'ACCOUNT' => $participant->getLabel()];
					continue;
				}
				$value = (float)$quotient / (float)$divisor;
			}
			else
			{
				$value = \str_replace(',', '.', $givenValue);
				if (!\is_numeric($value))
				{
					$errors[] = ['PART' => $givenValue, 'ACCOUNT' => $participant->getLabel()];
					continue;
				}
				$value = (float)$value;
			}
			$newDistribution[(string)($participant->getId())] = $value;
		}

		return $newDistribution;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onRefreshMovements(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$amountsByUserId = [];
		if (!$this->getCancelled())
		{
			$amountsByUserId[$this->getPayerId()] = $this->getPaidAmount();
			foreach ($this->getLines() as $line)
			{
				foreach ($line->getDistribution() as $id => $value)
				{
					$amountsByUserId[$id] = ($amountsByUserId[$id] ?? 0) - ($value * $line->getUnitPrice());
				}
			}

			foreach ($this->getModifiers() as $modifier)
			{
				foreach ($modifier->getDistribution() as $id => $value)
				{
					$amountsByUserId[$id] = ($amountsByUserId[$id] ?? 0) - ($value * $modifier->getAmount());
				}
			}
		}
		$accountingServices->getMovementManager()->replaceForSource($this->getId(), $amountsByUserId);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDeleteMovements(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$accountingServices->getMovementManager()->deleteForSource($this->getId());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$substitutions = [
			'count' => $this->getParticipantsCount(),
			'list' => \implode(', ', \array_map(static function (\Rbs\User\Documents\User $user) { return $user->getDisplayName(); },
				$this->getParticipants()->toArray()))
		];
		$legend = $i18nManager->trans('m.project.accounting.webapp.invoice_participants_legend', ['ucf'], $substitutions);
		$restResult->setProperty('participantsLegend', $legend);

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('modelLabel', $i18nManager->trans($this->getDocumentModel()->getLabelKey(), ['ucf']));
			$restResult->removeRelAction('delete');
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$linesDistribution = [];
			foreach ($this->getLines() as $line)
			{
				foreach ($line->getDistribution() as $id => $value)
				{
					$linesDistribution[$id] = ($value * $line->getUnitPrice()) + ($linesDistribution[$id] ?? 0);
				}
			}
			$restResult->setProperty('linesDistribution', $linesDistribution);

			$modifiersDistribution = [];
			foreach ($this->getModifiers() as $modifier)
			{
				foreach ($modifier->getDistribution() as $id => $value)
				{
					$modifiersDistribution[$id] = ($value * $modifier->getAmount()) + ($modifiersDistribution[$id] ?? 0);
				}
			}
			$restResult->setProperty('modifiersDistribution', $modifiersDistribution);
			$restResult->removeRelAction('delete');
		}
	}
}
