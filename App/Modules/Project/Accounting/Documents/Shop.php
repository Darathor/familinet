<?php
namespace Project\Accounting\Documents;

/**
 * @name \Project\Accounting\Documents\Shop
 */
class Shop extends \Compilation\Project\Accounting\Documents\Shop
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$image = $this->getIcon();
		if ($image)
		{
			$restResult->setProperty('adminthumbnail', $image->getPublicURL(128, 128));
		}
	}
}
