<?php
namespace Project\Accounting\Documents;

/**
 * @name \Project\Accounting\Documents\Transfer
 */
class Transfer extends \Compilation\Project\Accounting\Documents\Transfer
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE,
			function (\Change\Documents\Events\Event $event) { $this->buildLabel($event); }, 5);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED,
			function (\Change\Documents\Events\Event $event) { $this->onRefreshMovements($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED,
			function (\Change\Documents\Events\Event $event) { $this->onRefreshMovements($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED,
			function (\Change\Documents\Events\Event $event) { $this->onDeleteMovements($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function buildLabel(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$this->setLabel($accountingServices->getTransferManager()->getNextLabel());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onRefreshMovements(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$amountsByUserId = $this->getCancelled() ? [] : [$this->getSourceId() => $this->getAmount(), $this->getReceiverId() => -$this->getAmount()];
		$accountingServices->getMovementManager()->replaceForSource($this->getId(), $amountsByUserId);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDeleteMovements(\Change\Documents\Events\Event $event)
	{
		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$accountingServices->getMovementManager()->deleteForSource($this->getId());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$restResult->setProperty('modelLabel', $i18nManager->trans($this->getDocumentModel()->getLabelKey(), ['ucf']));
			$restResult->removeRelAction('delete');
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$restResult->setProperty('modeLabel', $i18nManager->trans('m.project.accounting.webapp.transfer_mode_' . $this->getMode(), ['ucf']));
			$restResult->removeRelAction('delete');
		}
	}
}
