<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Events\Documents;

/**
 * @name \Project\Accounting\Events\Documents\Documents
 */
class Documents
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onUpdateUserRestResult(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		$extraColumn = $event->getParam('extraColumn');
		if ($extraColumn && $document instanceof \Rbs\User\Documents\User && \in_array('balance', $extraColumn, true))
		{
			/** @var \Project\Accounting\AccountingServices $accountingServices */
			$accountingServices = $event->getServices('Project_AccountingServices');
			$restResult = $event->getParam('restResult');
			$restResult->setProperty('balance', $accountingServices->getMovementManager()->getBalanceByUserId($document->getId()));
		}
	}
}