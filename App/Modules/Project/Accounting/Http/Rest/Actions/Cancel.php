<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Http\Rest\Actions;

/**
 * @name \Project\Accounting\Http\Rest\Actions\Cancel
 */
class Cancel
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();

		$user = $documentManager->getDocumentInstance($event->getAuthenticationManager()->getCurrentUser()->getId());
		if (!($user instanceof \Rbs\User\Documents\User))
		{
			throw new \RuntimeException('No current user.');
		}

		$postParameters = $event->getRequest()->getPost();
		$event->getApplication()->getLogging()->fatal(__METHOD__, \var_export($postParameters, true));

		$comment = $postParameters->get('comment');
		if (!$comment)
		{
			throw new \RuntimeException('Missing comment.');
		}

		$document = $documentManager->getDocumentInstance($postParameters->get('documentId'));
		if (!($document instanceof \Project\Accounting\Documents\Invoice) && !($document instanceof \Project\Accounting\Documents\Transfer))
		{
			throw new \RuntimeException('Invalid document.');
		}

		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			if ($document instanceof \Project\Accounting\Documents\Invoice)
			{
				$accountingServices->getInvoiceManager()->cancel($document, $user, new \DateTime(), $comment);
			}
			else
			{
				$accountingServices->getTransferManager()->cancel($document, $user, new \DateTime(), $comment);
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult(new \Change\Http\Rest\V1\ArrayResult(['cancelled' => true]));
	}
}