<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Http\Rest\Actions;

/**
 * @name \Project\Accounting\Http\Rest\Actions\GetBalance
 */
class GetBalance
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$query = $event->getRequest()->getQuery();

		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$balance = $accountingServices->getMovementManager()->getBalanceByUserId((int)$query->get('userId'));

		$event->setResult(new \Change\Http\Rest\V1\ArrayResult(['balance' => $balance]));
	}
}