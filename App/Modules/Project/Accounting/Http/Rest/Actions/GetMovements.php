<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Http\Rest\Actions;

/**
 * @name \Project\Accounting\Http\Rest\Actions\GetMovements
 */
class GetMovements
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$urlManager = $event->getUrlManager();
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$query = $event->getRequest()->getQuery();

		/** @var \Project\Accounting\AccountingServices $accountingServices */
		$accountingServices = $event->getServices('Project_AccountingServices');
		$count = $accountingServices->getMovementManager()->getCount((int)$query->get('userId'), (int)$query->get('sourceId'));

		$result = new \Change\Http\Rest\V1\CollectionResult();
		$result->setCount($count);
		if ($count)
		{
			$movements = $accountingServices->getMovementManager()->getPaginated(
				$query->get('userId'), $query->get('sourceId'), $query->get('offset', 0), $query->get('limit', 10)
			);

			$modeList = \Change\Http\Rest\V1\Resources\DocumentLink::MODE_LIST;
			foreach ($movements as $movement)
			{
				$user = $documentManager->getDocumentInstance($movement->getUserId());
				$source = $documentManager->getDocumentInstance($movement->getSourceId());

				$result->addResource([
					'id' => $movement->getId(),
					'user' => $user ? (new \Change\Http\Rest\V1\Resources\DocumentLink($urlManager, $user, $modeList))->toArray() : null,
					'source' => $source ? (new \Change\Http\Rest\V1\Resources\DocumentLink($urlManager, $source, $modeList))->toArray() : null,
					'amount' => $movement->getAmount()
				]);
			}
		}
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}
}