<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Http\Rest;

/**
 * @name \Project\Accounting\Http\Rest\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_REQUEST, static function (\Change\Events\Event $event) {
			$controller = $event->getTarget();
			if ($controller instanceof \Change\Http\Rest\V1\Controller)
			{
				$resolver = $controller->getActionResolver();
				if ($resolver instanceof \Change\Http\Rest\V1\Resolver)
				{
					$resolver->addResolverClasses('projectAccounting', \Project\Accounting\Http\Rest\Resolver::class);
				}
			}
		});
	}
}