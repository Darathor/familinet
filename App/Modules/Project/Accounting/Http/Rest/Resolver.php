<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Http\Rest;

/**
 * @name \Project\Accounting\Http\Rest\Resolver
 */
class Resolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		return ['projectAccounting.movements', 'projectAccounting.balance', 'projectAccounting.cancel'];
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = \count($resourceParts);
		if ($nbParts === 0 && $method === \Change\Http\Rest\Request::METHOD_GET)
		{
			\array_unshift($resourceParts, 'plugins');
			$event->setParam('namespace', \implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = static function ($event) {
				$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		if ($nbParts === 1)
		{
			$actionName = $resourceParts[0];
			if ($actionName === 'movements')
			{
				$event->setAction(static function ($event) { (new \Project\Accounting\Http\Rest\Actions\GetMovements())->execute($event); });
				$authorisation = static function () use ($event) {
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
			elseif ($actionName === 'balance')
			{
				$event->setAction(static function ($event) { (new \Project\Accounting\Http\Rest\Actions\GetBalance())->execute($event); });
				$authorisation = static function () use ($event) {
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
			elseif ($actionName === 'cancel')
			{
				$event->setAction(static function ($event) { (new \Project\Accounting\Http\Rest\Actions\Cancel())->execute($event); });
				$authorisation = static function () use ($event) {
					return $event->getPermissionsManager()->isAllowed('Administrator');
				};
				$event->setAuthorization($authorisation);
			}
		}
	}
}