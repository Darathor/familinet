<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Invoices;

/**
 * @name \Project\Accounting\Invoices\Manager
 */
class Manager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_MANAGER_IDENTIFIER = 'InvoiceManager';

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Project/Accounting/Events/InvoiceManager');
	}

	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{

	}

	/**
	 * @var \Project\Accounting\Invoices\DbRepository
	 */
	protected $dbRepository;

	/**
	 * @param \Project\Accounting\Invoices\DbRepository $dbRepository
	 * @return $this
	 */
	public function setDbRepository($dbRepository)
	{
		$this->dbRepository = $dbRepository;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNextLabel()
	{
		$lastLabel = $this->dbRepository->getLastLabel();
		if (!$lastLabel)
		{
			return 'FACT00001';
		}
		return 'FACT' . (\str_pad((int)\substr($lastLabel, 4) + 1, 5, '0', STR_PAD_LEFT));
	}

	/**
	 * Needs started transaction.
	 * @param \Project\Accounting\Documents\Invoice $invoice
	 * @param \Rbs\User\Documents\User $user
	 * @param \DateTime $date
	 * @param string $comment
	 */
	public function cancel(\Project\Accounting\Documents\Invoice $invoice, \Rbs\User\Documents\User $user, \DateTime $date, string $comment)
	{
		$invoice->setCancelled(true);
		$invoice->setCanceller($user);
		$invoice->setCancelDate($date);
		$invoice->setCancelComment($comment);
		$invoice->save();
	}
}