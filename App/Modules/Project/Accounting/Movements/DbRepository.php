<?php
namespace Project\Accounting\Movements;

/**
 * @name \Project\Accounting\Movements\DbRepository
 */
class DbRepository
{
	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider(\Change\Db\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param int $offset
	 * @param int $limit
	 * @return \Project\Accounting\Movements\Movement[]
	 */
	public function getPaginated(int $offset = 0, int $limit = 10)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select('movement_id', 'user_id', 'source_id', 'amount');
			$query->from('project_accounting_movements');
			$query->orderDesc($fb->column('movement_id'));
		}

		$select = $query->query();
		$select->setStartIndex($offset);
		$select->setMaxResults($limit);

		$movements = [];
		foreach ($select->getResults($select->getRowsConverter()->addIntCol('movement_id', 'user_id', 'source_id')->addNumCol('amount')) as $row)
		{
			$movements[] = $this->buildMovementByRow($row);
		}
		return $movements;
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select($fb->alias($fb->count($fb->allColumns()), 'count'));
			$query->from('project_accounting_movements');
		}

		$select = $query->query();
		return $select->getFirstResult($select->getRowsConverter()->addIntCol('count')->singleColumn('count'));
	}

	/**
	 * @param int $userId
	 * @param int $offset
	 * @param int $limit
	 * @return \Project\Accounting\Movements\Movement[]
	 */
	public function getPaginatedByUserId(int $userId, int $offset = 0, int $limit = 10)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select('movement_id', 'user_id', 'source_id', 'amount');
			$query->from('project_accounting_movements');
			$query->where($fb->eq('user_id', $fb->parameter('userId')));
			$query->orderDesc($fb->column('movement_id'));
		}

		$select = $query->query();
		$select->bindParameter('userId', $userId);
		$select->setStartIndex($offset);
		$select->setMaxResults($limit);

		$movements = [];
		foreach ($select->getResults($select->getRowsConverter()->addIntCol('movement_id', 'user_id', 'source_id')->addNumCol('amount')) as $row)
		{
			$movements[] = $this->buildMovementByRow($row);
		}
		return $movements;
	}

	/**
	 * @param int $userId
	 * @return int
	 */
	public function getCountByUserId(int $userId)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select($fb->alias($fb->count($fb->allColumns()), 'count'));
			$query->from('project_accounting_movements');
			$query->where($fb->eq('user_id', $fb->parameter('userId')));
		}

		$select = $query->query();
		$select->bindParameter('userId', $userId);
		return $select->getFirstResult($select->getRowsConverter()->addIntCol('count')->singleColumn('count'));
	}

	/**
	 * @param int $sourceId
	 * @param int $offset
	 * @param int $limit
	 * @return \Project\Accounting\Movements\Movement[]
	 */
	public function getPaginatedBySourceId(int $sourceId, int $offset = 0, int $limit = 10)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select('movement_id', 'user_id', 'source_id', 'amount');
			$query->from('project_accounting_movements');
			$query->where($fb->eq('source_id', $fb->parameter('sourceId')));
			$query->orderDesc($fb->column('movement_id'));
		}

		$select = $query->query();
		$select->bindParameter('sourceId', $sourceId);
		$select->setStartIndex($offset);
		$select->setMaxResults($limit);

		$movements = [];
		foreach ($select->getResults($select->getRowsConverter()->addIntCol('movement_id', 'user_id', 'source_id')->addNumCol('amount')) as $row)
		{
			$movements[] = $this->buildMovementByRow($row);
		}
		return $movements;
	}

	/**
	 * @param int $sourceId
	 * @return int
	 */
	public function getCountBySourceId(int $sourceId)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select($fb->alias($fb->count($fb->allColumns()), 'count'));
			$query->from('project_accounting_movements');
			$query->where($fb->eq('source_id', $fb->parameter('sourceId')));
		}

		$select = $query->query();
		$select->bindParameter('sourceId', $sourceId);
		return $select->getFirstResult($select->getRowsConverter()->addIntCol('count')->singleColumn('count'));
	}

	/**
	 * @param int $sourceId
	 * @return \Project\Accounting\Movements\Movement[]
	 */
	public function getBySourceId(int $sourceId)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select('movement_id', 'user_id', 'source_id', 'amount');
			$query->from('project_accounting_movements');
			$query->where($fb->eq('source_id', $fb->parameter('sourceId')));
		}

		$select = $query->query();
		$select->bindParameter('sourceId', $sourceId);

		$movements = [];
		foreach ($select->getResults($select->getRowsConverter()->addIntCol('movement_id', 'user_id', 'source_id')->addNumCol('amount')) as $row)
		{
			$movements[] = $this->buildMovementByRow($row);
		}
		return $movements;
	}

	/**
	 * @param array $row
	 * @return \Project\Accounting\Movements\Movement
	 */
	protected function buildMovementByRow(array $row)
	{
		return new \Project\Accounting\Movements\Movement($row['movement_id'], $row['user_id'], $row['source_id'], $row['amount']);
	}

	/**
	 * @param int $userId
	 * @return float
	 */
	public function getBalanceByUserId(int $userId)
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select($fb->alias($fb->sum($fb->column('amount')), 'balance'));
			$query->from('project_accounting_movements');
			$query->where($fb->eq('user_id', $fb->parameter('userId')));
		}

		$select = $query->query();
		$select->bindParameter('userId', $userId);
		return (float)$select->getFirstResult($select->getRowsConverter()->addNumCol('balance')->singleColumn('balance'));
	}

	/**
	 * @param \Project\Accounting\Movements\Movement $movement
	 */
	public function insert(\Project\Accounting\Movements\Movement $movement)
	{
		$sb = $this->dbProvider->getNewStatementBuilder(__METHOD__);
		if (!$sb->isCached())
		{
			$fb = $sb->getFragmentBuilder();
			$sb->insert($fb->table('project_accounting_movements'));
			$sb->addColumns(
				$fb->column('user_id'),
				$fb->column('source_id'),
				$fb->column('amount')
			);
			$sb->addValues(
				$fb->integerParameter('userId'),
				$fb->integerParameter('sourceId'),
				$fb->parameter('amount')
			);
		}

		$iq = $sb->insertQuery();
		$iq->bindParameter('userId', $movement->getUserId());
		$iq->bindParameter('sourceId', $movement->getSourceId());
		$iq->bindParameter('amount', $movement->getAmount());
		$iq->execute();

		$movement->setId($iq->getDbProvider()->getLastInsertId('project_accounting_movements'));
	}

	/**
	 * @param \Project\Accounting\Movements\Movement $movement
	 */
	public function update(\Project\Accounting\Movements\Movement $movement)
	{
		$sb = $this->dbProvider->getNewStatementBuilder(__METHOD__);
		if (!$sb->isCached())
		{
			$fb = $sb->getFragmentBuilder();
			$sb->update($fb->table('project_accounting_movements'));
			$sb->assign($fb->column('user_id'), $fb->integerParameter('userId'));
			$sb->assign($fb->column('source_id'), $fb->integerParameter('sourceId'));
			$sb->assign($fb->column('amount'), $fb->parameter('amount'));
			$sb->where($fb->eq($fb->column('movement_id'), $fb->integerParameter('movementId')));
		}
		$uq = $sb->updateQuery();
		$uq->bindParameter('movementId', $movement->getId());
		$uq->bindParameter('userId', $movement->getUserId());
		$uq->bindParameter('sourceId', $movement->getSourceId());
		$uq->bindParameter('amount', $movement->getAmount());
		$uq->execute();
	}

	/**
	 * @param \Project\Accounting\Movements\Movement $movement
	 */
	public function delete(\Project\Accounting\Movements\Movement $movement)
	{
		$sb = $this->dbProvider->getNewStatementBuilder();
		if (!$sb->isCached())
		{
			$fb = $sb->getFragmentBuilder();
			$sb->delete($fb->table('project_accounting_movements'))->where($fb->eq($fb->column('movement_id'), $fb->integerParameter('movementId')));
		}
		$dq = $sb->deleteQuery();
		$dq->bindParameter('movementId', $movement->getId());
		$dq->execute();
	}
}