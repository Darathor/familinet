<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Movements;

/**
 * @name \Project\Accounting\Movements\Manager
 */
class Manager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_MANAGER_IDENTIFIER = 'MovementManager';

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Project/Accounting/Events/MovementManager');
	}

	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{

	}

	/**
	 * @var \Project\Accounting\Movements\DbRepository
	 */
	protected $dbRepository;

	/**
	 * @param \Project\Accounting\Movements\DbRepository $dbRepository
	 * @return $this
	 */
	public function setDbRepository($dbRepository)
	{
		$this->dbRepository = $dbRepository;
		return $this;
	}

	/**
	 * @param int|null $userId
	 * @param int|null $sourceId
	 * @return int
	 */
	public function getCount(?int $userId, ?int $sourceId)
	{
		if ($userId)
		{
			return $this->dbRepository->getCountByUserId($userId);
		}
		if ($sourceId)
		{
			return $this->dbRepository->getCountBySourceId($sourceId);
		}
		return $this->dbRepository->getCount();
	}

	/**
	 * @param int|null $userId
	 * @param int|null $sourceId
	 * @param int $offset
	 * @param int $limit
	 * @return \Project\Accounting\Movements\Movement[]
	 */
	public function getPaginated(?int $userId, ?int $sourceId, int $offset = 0, int $limit = 10)
	{
		if ($userId)
		{
			return $this->dbRepository->getPaginatedByUserId($userId, $offset, $limit);
		}
		if ($sourceId)
		{
			return $this->dbRepository->getPaginatedBySourceId($sourceId, $offset, $limit);
		}
		return $this->dbRepository->getPaginated($offset, $limit);
	}

	/**
	 * @param int $userId
	 * @return float
	 */
	public function getBalanceByUserId(int $userId)
	{
		return $this->dbRepository->getBalanceByUserId($userId);
	}

	/**
	 * Needs started transaction.
	 * @param int $sourceId
	 * @param array $amountsByUserId
	 */
	public function replaceForSource(int $sourceId, array $amountsByUserId)
	{
		$existingMovements = $this->dbRepository->getBySourceId($sourceId);

		foreach ($existingMovements as $existingMovement)
		{
			$userId = $existingMovement->getUserId();
			$amount = (float)($amountsByUserId[$userId] ?? 0);
			unset($amountsByUserId[$userId]);

			if (\Change\Stdlib\FloatUtils::equals(0, $amount, 0.001))
			{
				$this->dbRepository->delete($existingMovement);
			}
			elseif (!\Change\Stdlib\FloatUtils::equals($existingMovement->getAmount(), $amount, 0.001))
			{
				$existingMovement->setAmount($amount);
				$this->dbRepository->update($existingMovement);
			}
		}

		foreach ($amountsByUserId as $userId => $amount)
		{
			if (!\Change\Stdlib\FloatUtils::equals(0, $amount, 0.001))
			{
				$movement = new \Project\Accounting\Movements\Movement(null, $userId, $sourceId, $amount);
				$this->dbRepository->insert($movement);
			}
		}
	}

	/**
	 * Needs started transaction.
	 * @param int $sourceId
	 */
	public function deleteForSource(int $sourceId)
	{
		$existingMovements = $this->dbRepository->getBySourceId($sourceId);
		foreach ($existingMovements as $existingMovement)
		{
			$this->dbRepository->delete($existingMovement);
		}
	}
}