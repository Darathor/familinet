<?php
namespace Project\Accounting\Movements;

/**
 * @name \Project\Accounting\Movements\Movement
 */
class Movement
{
	/**
	 * @var int|null
	 */
	protected $id;

	/**
	 * @var int
	 */
	protected $userId;

	/**
	 * @var int
	 */
	protected $sourceId;

	/**
	 * @var float
	 */
	protected $amount;

	/**
	 * Movement constructor.
	 * @param int|null $id
	 * @param int $userId
	 * @param int $sourceId
	 * @param float $amount
	 */
	public function __construct(?int $id, int $userId, int $sourceId, float $amount)
	{
		$this->id = $id;
		$this->userId = $userId;
		$this->sourceId = $sourceId;
		$this->amount = $amount;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * @param int $userId
	 * @return $this
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSourceId()
	{
		return $this->sourceId;
	}

	/**
	 * @param int $sourceId
	 * @return $this
	 */
	public function setSourceId($sourceId)
	{
		$this->sourceId = $sourceId;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param float $amount
	 * @return $this
	 */
	public function setAmount($amount)
	{
		$this->amount = $amount;
		return $this;
	}
}