<?php
namespace Project\Accounting\Setup;

/**
 * @name \Project\Accounting\Setup\Install
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		parent::executeDbSchema($plugin, $schemaManager);
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/ListenerAggregateClasses/Project_Accounting',
			\Project\Accounting\Events\SharedListeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Rest/Project_Accounting', \Project\Accounting\Http\Rest\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Documents/Project_Accounting', \Project\Accounting\Events\Documents\SharedListeners::class);

		$configuration->addPersistentEntry('Project/Familinet/webApps/familinet/modules/Project_Accounting', true);
	}
}
