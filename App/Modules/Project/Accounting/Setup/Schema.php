<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Setup;

/**
 * @name \Project\Accounting\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();

			// Movements.
			$td = $schemaManager->newTableDefinition('project_accounting_movements');
			$td->addField($schemaManager->newIntegerFieldDefinition('movement_id')->setNullable(false)->setAutoNumber(true))
				->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('source_id')->setNullable(false))
				->addField($schemaManager->newFloatFieldDefinition('amount')->setNullable(false))
				->addKey($this->newPrimaryKey()->addField($td->getField('movement_id')))
				->addKey($this->newIndexKey()->setName('BY_USER_ID')->addField($td->getField('user_id')))
				->addKey($this->newIndexKey()->setName('BY_SOURCE_ID')->addField($td->getField('source_id')));
			$this->tables['project_accounting_movements'] = $td;
		}
		return $this->tables;
	}
}
