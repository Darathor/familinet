<?php
namespace Project\Accounting\Transfers;

/**
 * @name \Project\Accounting\Transfers\DbRepository
 */
class DbRepository
{
	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider(\Change\Db\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastLabel()
	{
		$query = $this->dbProvider->getNewQueryBuilder(__METHOD__);
		if (!$query->isCached())
		{
			$fb = $query->getFragmentBuilder();
			$query->select($fb->alias($fb->max($fb->getDocumentColumn('label')), 'maxLabel'));
			$query->from($fb->getDocumentTable($this->getModel()->getRootName()));
		}

		$sq = $query->query();
		return $sq->getFirstResult($sq->getRowsConverter()->addStrCol('maxLabel')->singleColumn('maxLabel'));
	}

	/**
	 * @return \Change\Documents\AbstractModel
	 */
	protected function getModel()
	{
		return $this->documentManager->getModelManager()->getModelByName('Project_Accounting_Transfer');
	}
}