<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Accounting\Transfers;

/**
 * @name \Project\Accounting\Transfers\Manager
 */
class Manager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_MANAGER_IDENTIFIER = 'TransferManager';

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Project/Accounting/Events/TransferManager');
	}

	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{

	}

	/**
	 * @var \Project\Accounting\Transfers\DbRepository
	 */
	protected $dbRepository;

	/**
	 * @param \Project\Accounting\Transfers\DbRepository $dbRepository
	 * @return $this
	 */
	public function setDbRepository($dbRepository)
	{
		$this->dbRepository = $dbRepository;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNextLabel()
	{
		$lastLabel = $this->dbRepository->getLastLabel();
		if (!$lastLabel)
		{
			return 'VERS00001';
		}
		return 'VERS' . (\str_pad((int)\substr($lastLabel, 4) + 1, 5, '0', STR_PAD_LEFT));
	}


	/**
	 * Needs started transaction.
	 * @param \Project\Accounting\Documents\Transfer $transfer
	 * @param \Rbs\User\Documents\User $user
	 * @param \DateTime $date
	 * @param string $comment
	 */
	public function cancel(\Project\Accounting\Documents\Transfer $transfer, \Rbs\User\Documents\User $user, \DateTime $date, string $comment)
	{
		$transfer->setCancelled(true);
		$transfer->setCanceller($user);
		$transfer->setCancelDate($date);
		$transfer->setCancelComment($comment);
		$transfer->save();
	}
}