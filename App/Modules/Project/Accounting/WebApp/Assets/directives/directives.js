/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectAccountingUserLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Accounting/directives/user-link.twig',
			scope: { user: '=projectAccountingUserLink' }
		};
	});

	app.directive('projectAccountingShopLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Accounting/directives/shop-link.twig',
			scope: { shop: '=projectAccountingShopLink' }
		};
	});

	app.directive('projectAccountingInvoiceLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Accounting/directives/invoice-link.twig',
			scope: { invoice: '=projectAccountingInvoiceLink' }
		};
	});

	app.directive('projectAccountingTransferLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Accounting/directives/transfer-link.twig',
			scope: { transfer: '=projectAccountingTransferLink' }
		};
	});
})();