(function() {
	'use strict';

	var moduleName = 'Project_Accounting';

	__change.routes['/Accounting/'] = {
		name: 'home',
		module: 'Project_Accounting',
		rule: {
			labelKey: 'm.project.accounting.webappjs.accounting | ucf',
			icon: 'piggy-bank',
			templateUrl: moduleName + '/views/home.twig',
			controller: 'EmptyController'
		}
	};

// USER

	__change.routes['/Accounting/User/'] = {
		name: 'accountingList',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.accounting.webappjs.user_list | ucf',
			templateUrl: moduleName + '/views/user/user-list.twig',
			controller: 'AccountingUserListController'
		}
	};

	__change.routes['/Accounting/User/:id'] = {
		name: 'accountingDetail',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.accounting.webappjs.user | ucf',
			templateUrl: moduleName + '/views/user/user-detail.twig',
			controller: 'AccountingUserDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Accounting/User/:id/Invoice/'] = {
		name: 'invoices',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.accounting.webappjs.invoice_list | ucf',
			templateUrl: moduleName + '/views/user/user-invoice-list.twig',
			controller: 'AccountingUserDetailController'
		}
	};

	__change.routes['/Accounting/User/:id/Transfer/'] = {
		name: 'transfers',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.accounting.webappjs.transfer | ucf',
			templateUrl: moduleName + '/views/user/user-transfer-list.twig',
			controller: 'AccountingUserDetailController'
		}
	};

// SHOP

	__change.routes['/Accounting/Shop/'] = {
		name: 'list',
		model: 'Project_Accounting_Shop',
		rule: {
			labelKey: 'm.project.accounting.webappjs.shop_list | ucf',
			templateUrl: moduleName + '/views/shop/shop-list.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Accounting/Shop/new'] = {
		name: 'new',
		model: 'Project_Accounting_Shop',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/shop/shop-new.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Accounting/Shop/:id'] = {
		name: 'detail',
		model: 'Project_Accounting_Shop',
		rule: {
			labelKey: 'm.project.accounting.webappjs.invoice | ucf',
			templateUrl: moduleName + '/views/shop/shop-detail.twig',
			controller: 'AccountingShopDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Accounting/Shop/:id/edit'] = {
		name: 'edit',
		model: 'Project_Accounting_Shop',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/shop/shop-edit.twig',
			controller: 'AccountingShopEditController'
		}
	};

	__change.routes['/Accounting/Shop/:id/Invoice/'] = {
		name: 'invoices',
		model: 'Project_Accounting_Shop',
		rule: {
			labelKey: 'm.project.accounting.webappjs.invoice_list | ucf',
			templateUrl: moduleName + '/views/shop/shop-invoice-list.twig',
			controller: 'AccountingShopDetailController'
		}
	};

// INVOICE

	__change.routes['/Accounting/Invoice/'] = {
		name: 'list',
		model: 'Project_Accounting_Invoice',
		rule: {
			labelKey: 'm.project.accounting.webappjs.invoice_list | ucf',
			templateUrl: moduleName + '/views/invoice/invoice-list.twig',
			controller: 'AccountingInvoiceListController'
		}
	};

	__change.routes['/Accounting/Invoice/new'] = {
		name: 'new',
		model: 'Project_Accounting_Invoice',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/invoice/invoice-new.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Accounting/Invoice/:id'] = {
		name: 'detail',
		model: 'Project_Accounting_Invoice',
		rule: {
			labelKey: 'm.project.accounting.webappjs.invoice | ucf',
			templateUrl: moduleName + '/views/invoice/invoice-detail.twig',
			controller: 'AccountingInvoiceDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Accounting/Invoice/:id/edit'] = {
		name: 'edit',
		model: 'Project_Accounting_Invoice',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/invoice/invoice-edit.twig',
			controller: 'AccountingInvoiceEditController'
		}
	};

// TRANSFER

	__change.routes['/Accounting/Transfer/'] = {
		name: 'list',
		model: 'Project_Accounting_Transfer',
		rule: {
			labelKey: 'm.project.accounting.webappjs.transfer_list | ucf',
			templateUrl: moduleName + '/views/transfer/transfer-list.twig',
			controller: 'AccountingTransferListController'
		}
	};

	__change.routes['/Accounting/Transfer/new'] = {
		name: 'new',
		model: 'Project_Accounting_Transfer',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/transfer/transfer-new.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Accounting/Transfer/:id'] = {
		name: 'detail',
		model: 'Project_Accounting_Transfer',
		rule: {
			labelKey: 'm.project.accounting.webappjs.transfer | ucf',
			templateUrl: moduleName + '/views/transfer/transfer-detail.twig',
			controller: 'AccountingTransferDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Accounting/Transfer/:id/edit'] = {
		name: 'edit',
		model: 'Project_Accounting_Transfer',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/transfer/transfer-edit.twig',
			controller: 'AccountingTransferEditController'
		}
	};

})();