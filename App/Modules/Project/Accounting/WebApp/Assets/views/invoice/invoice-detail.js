/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingInvoiceDetailController',
		['$rootScope', '$scope', 'RbsChange.Document', 'RbsChange.REST', '$routeParams', 'RbsChange.i18n', 'RbsChange.NotificationCenter','RbsChange.ErrorFormatter',
			function($rootScope, scope, Document, REST, $routeParams, i18n, NotificationCenter, ErrorFormatter) {
				Document.initDetailView(scope, 'Project_Accounting_Invoice');

				var documentId = $routeParams.id;

				scope.data = {
					movements: [],
					cancelComment: null
				};

				var loadMovements = function () {
					REST.call(REST.getBaseUrl('projectAccounting/movements'), { sourceId: documentId }).then(
						function(data) {
							scope.data.movements = data.resources
						},
						function(error) {
							console.error(error);
						}
					);
				};
				loadMovements();

				scope.confirmCancel = function() {
					if (confirm(i18n.trans('m.project.accounting.webappjs.invoice_confirm_cancel'))) {
						REST.apiPost('projectAccounting/cancel', { documentId: scope.document.id, comment: scope.data.cancelComment }).then(
							function() {
								Document.reloadDetailView(scope).then(
									function () {
										loadMovements();
									},
									function () {}
								);
							},
							function(error) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(error),
									'CANCEL_DOCUMENT'
								);
							}
						);
					}
				};
			}
		]
	);
})();