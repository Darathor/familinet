/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingInvoiceListController', ['$scope', '$rootScope', function(scope, $rootScope) {
		scope.staticFilters = [
			{
				name: 'accessible',
				parameters: { operator: 'OR' },
				filters: [
					{
						name: 'hidden',
						parameters: { propertyName: 'hidden', operator: 'eq', value: false }
					},
					{
						name: 'participants',
						parameters: { propertyName: 'participants', operator: 'eq', value: $rootScope.user.id }
					}
				]
			}
		];
	}]);
})();