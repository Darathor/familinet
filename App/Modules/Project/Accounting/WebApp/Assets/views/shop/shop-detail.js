/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingShopDetailController',
		['$rootScope', '$scope', 'RbsChange.Document', '$routeParams', function($rootScope, scope, Document, $routeParams) {
			Document.initDetailView(scope, 'Project_Accounting_Shop');

			var documentId = $routeParams.id;
			var currentUserId = $rootScope.user.id;

			scope.invoiceStaticFilters = [
				{
					name: 'group',
					parameters: { operator: 'AND' },
					filters: [
						{
							name: 'shop',
							parameters: { propertyName: 'shop', operator: 'eq', value: documentId }
						},
						{
							name: 'accessible',
							parameters: { operator: 'OR' },
							filters: [
								{
									name: 'hidden',
									parameters: { propertyName: 'hidden', operator: 'eq', value: false }
								},
								{
									name: 'participants',
									parameters: { propertyName: 'participants', operator: 'eq', value: currentUserId }
								}
							]
						}
					]
				}
			];
		}]);
})();