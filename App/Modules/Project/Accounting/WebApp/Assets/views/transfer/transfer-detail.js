/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingTransferDetailController',
		['$rootScope', '$scope', 'RbsChange.Document', 'RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter',
			function($rootScope, scope, Document, REST, i18n, NotificationCenter, ErrorFormatter) {
				Document.initDetailView(scope, 'Project_Accounting_Transfer');

				scope.data = {
					cancelComment: null
				};

				scope.confirmCancel = function() {
					if (confirm(i18n.trans('m.project.accounting.webappjs.transfer_confirm_cancel'))) {
						REST.apiPost('projectAccounting/cancel', { documentId: scope.document.id, comment: scope.data.cancelComment }).then(
							function() {
								Document.reloadDetailView(scope);
							},
							function(error) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(error),
									'CANCEL_DOCUMENT'
								);
							}
						);
					}
				};
			}
		]
	);
})();