/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingTransferEditController', ['$scope', 'RbsChange.Document', function(scope, Document) {
		Document.initEditView(scope, 'Project_Accounting_Transfer');
	}]);
})();