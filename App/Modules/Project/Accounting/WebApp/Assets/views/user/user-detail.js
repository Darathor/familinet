/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('AccountingUserDetailController',
		['$rootScope', '$scope', 'RbsChange.REST', 'RbsChange.Document', '$routeParams', function($rootScope, scope, REST, Document, $routeParams) {
			Document.initDetailView(scope, 'Rbs_User_User');

			var documentId = $routeParams.id;
			var currentUserId = $rootScope.user.id;

			scope.data = {
				balance: null
			};

			REST.call(REST.getBaseUrl('projectAccounting/balance'), {userId: documentId }).then(
				function(data) {
					scope.data.balance = data.balance
				},
				function(error) {
					console.error(error);
				}
			);

			scope.invoiceStaticFilters = [
				{
					name: 'group',
					parameters: { operator: 'AND' },
					filters: [
						{
							name: 'participants',
							parameters: { propertyName: 'participants', operator: 'eq', value: documentId }
						},
						{
							name: 'accessible',
							parameters: { operator: 'OR' },
							filters: [
								{
									name: 'hidden',
									parameters: { propertyName: 'hidden', operator: 'eq', value: false }
								},
								{
									name: 'participants',
									parameters: { propertyName: 'participants', operator: 'eq', value: currentUserId }
								}
							]
						}
					]
				}
			];

			scope.transferStaticFilters = [
				{
					name: 'group',
					parameters: { operator: 'AND' },
					filters: [
						{
							name: 'participant',
							parameters: { operator: 'OR' },
							filters: [
								{
									name: 'source',
									parameters: { propertyName: 'source', operator: 'eq', value: documentId }
								},
								{
									name: 'receiver',
									parameters: { propertyName: 'receiver', operator: 'eq', value: documentId }
								}
							]
						},
						{
							name: 'accessible',
							parameters: { operator: 'OR' },
							filters: [
								{
									name: 'hidden',
									parameters: { propertyName: 'hidden', operator: 'eq', value: false }
								},
								{
									name: 'source',
									parameters: { propertyName: 'source', operator: 'eq', value: currentUserId }
								},
								{
									name: 'receiver',
									parameters: { propertyName: 'receiver', operator: 'eq', value: currentUserId }
								}
							]
						}
					]
				}
			];

			scope.newSubDocumentUrlParameters = { userId: documentId };

			scope.isCurrent = currentUserId === documentId;
		}]);
})();