/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	"use strict";

	function Editor() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',
			link: function(scope) {
				scope.newMappingItem = { v: null, m: null };
				scope.pagination = { size: 15, offset: 0, items: [] };

				scope.$watchCollection('document.mappingData', function(mappingData) {
					if (angular.isArray(mappingData)) {
						scope.buildPageItems(mappingData);
					}
					else {
						scope.pagination = { size: 15, offset: 0, items: [] };
					}
				});

				scope.changePage = function(page) {
					var length = scope.document.mappingData.length;
					if (length) {
						scope.pagination.offset += page * scope.pagination.size;
						if (scope.pagination.offset >= length) {
							scope.pagination.offset = length;
							scope.pagination.offset -= length % scope.pagination.size;
						}

						if (scope.pagination.offset < 0) {
							scope.pagination.offset = 0;
						}
						scope.buildPageItems(scope.document.mappingData);
					}
				};

				scope.buildPageItems = function(mappingData) {
					scope.pagination.items = [];
					var maxOffset = scope.pagination.offset + scope.pagination.size;
					if (maxOffset > mappingData.length) {
						maxOffset = mappingData.length
					}
					for (var i = scope.pagination.offset; i < maxOffset; i++) {
						scope.pagination.items.push(mappingData[i]);
					}
				};

				scope.onReload = function() {
					scope.newMappingItem = { v: null, m: null };
				};

				scope.deleteMappingItem = function(item) {
					var mappingData = [];
					angular.forEach(scope.document.mappingData, function(ci) {
						if (ci !== item) {
							mappingData.push(ci)
						}
					});
					scope.document.mappingData = mappingData;
				};

				scope.addMappingItem = function() {
					if (scope.newMappingItem && scope.newMappingItem.v) {
						if (!angular.isArray(scope.document.mappingData)) {
							scope.document.mappingData = [];
						}
						scope.document.mappingData.push(scope.newMappingItem);
						scope.newMappingItem = { v: null, m: null };
					}
				};
			}
		};
	}

	angular.module('RbsChange').directive('rbsDocumentEditorProjectFamilinetMappingNew', Editor);
	angular.module('RbsChange').directive('rbsDocumentEditorProjectFamilinetMappingEdit', Editor);
})();