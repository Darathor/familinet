<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Commands;

/**
 * @name \Project\Familinet\Commands\UpdateWebApp
 */
class UpdateWebApp
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\Presentation\Templates\TemplateManager
	 */
	protected $templateManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var \Change\Http\OAuth\OAuthManager
	 */
	protected $oAuthManager;

	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$this->application = $event->getApplication();
		$this->templateManager = $event->getApplicationServices()->getTemplateManager();
		$this->i18nManager = $event->getApplicationServices()->getI18nManager();
		$this->pluginManager = $event->getApplicationServices()->getPluginManager();
		$this->modelManager = $event->getApplicationServices()->getModelManager();
		$this->oAuthManager = $event->getApplicationServices()->getOAuthManager();

		$minify = $event->getParam('min') ?? false;

		// Update all web apps.
		if ($event->getParam('all'))
		{
			$configurations = $this->application->getConfiguration()->getEntry('Project/Familinet/webApps');
			if (!\is_array($configurations) || !$configurations)
			{
				$response->addErrorMessage('There is no declared web app in this project.');
				return;
			}

			$response->addInfoMessage(\count($configurations) . ' web app(s) to update');
			foreach ($configurations as $appName => $configuration)
			{
				$response->addInfoMessage('Update web app: ' . $appName);
				$this->doUpdateWebApp($configuration, $minify);
			}
		}
		// Update a single web app.
		else
		{
			$webAppName = $event->getParam('webAppName');
			if (!$webAppName)
			{
				$response->addErrorMessage('You must specify a web app name or use option --all.');
				return;
			}

			$configuration = $this->application->getConfiguration()->getEntry('Project/Familinet/webApps/familinet');
			if (!isset($configuration['serverBaseUrl'], $configuration['modules']))
			{
				$response->addErrorMessage('You must specify a valid web app name.');
				return;
			}

			$response->addInfoMessage('Update web app: ' . $webAppName);
			$this->doUpdateWebApp($configuration, $minify);
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param array $configuration
	 * @param bool $minify
	 */
	protected function doUpdateWebApp(array $configuration, bool $minify)
	{
		$webBaseDirectory = $this->application->getWorkspace()
			->composeAbsolutePath($this->application->getConfiguration()->getEntry('Change/Install/webBaseDirectory'));
		if (!\is_dir($webBaseDirectory))
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBaseDirectory .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}

		/** @var \Change\Plugins\Plugin $corePlugin */
		$corePlugin = $this->pluginManager->getModule('Project', 'Familinet');
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets';

		/** @var \Change\Plugins\Plugin[] $plugins */
		$plugins = [];
		foreach (\array_keys($configuration['modules']) as $fullName)
		{
			[$vendorName, $moduleName] = \explode('_', $fullName);
			$plugin = $this->pluginManager->getModule($vendorName, $moduleName);
			if ($plugin && $plugin->getActivated())
			{
				$plugins[] = $plugin;
			}
		}

		// Resolve i18n keys.
		$i18nData = [];
		$this->addI18nKeysForJS($corePlugin, $i18nData);
		foreach ($plugins as $plugin)
		{
			$this->addI18nKeysForJS($plugin, $i18nData);
		}

		// Index.html.
		$appDirectory = $webBaseDirectory . DIRECTORY_SEPARATOR . $configuration['appPath'];
		\Change\Stdlib\FileUtils::rmdir($appDirectory);
		\Change\Stdlib\FileUtils::mkdir($appDirectory);
		$srcPath = $assetsPath . '/index.twig';
		$destPath = $appDirectory . DIRECTORY_SEPARATOR . 'index.html';
		$realm = $configuration['realm'];
		$consumer = $this->oAuthManager->getConsumerByApplication($realm);
		$attributes = [
			'applicationShortName' => 'Familinet',
			'applicationPackageName' => $this->i18nManager->trans('m.rbs.ua.common.application_package_name', ['ucf']),
			'application' => [
				'uuid' => $this->application->getConfiguration('Change/Application/uuid'),
				'env' => $this->application->getConfiguration('Change/Application/env')
			],
			'configuration' => $configuration,
			'oAuth' => $consumer ? \array_merge($consumer->toArray(), ['realm' => $realm]) : [],
			'i18n' => $i18nData,
			'modelDefinitions' => $this->getModelDefinitions(),
			'cachedScripts' => $this->getCachedScripts($corePlugin, $plugins, $minify),
			'cachedStylesheets' => $this->getCachedStylesheets($corePlugin, $plugins, $minify),
			'cachedTemplates' => $this->getCachedTemplates($corePlugin, $plugins),
		];
		\file_put_contents($destPath, $this->templateManager->renderTemplateFile($srcPath, $attributes));

		// File copy.
		$this->copyDirectory($assetsPath . '/app', $appDirectory . '/app', ['png', 'jpg', 'css']);
		$this->copyDirectory($assetsPath . '/lib', $appDirectory . '/lib');
		$this->copyFile($assetsPath . '/LICENSE.txt', $appDirectory . '/LICENSE.TXT');

		// Copy UA libs.
		/** @var \Change\Plugins\Plugin $uaPlugin */
		$uaPlugin = $this->pluginManager->getModule('Rbs', 'Ua');
		$uaAssetsPath = $uaPlugin->getAbsolutePath() . '/Assets';
		$this->copyFile($uaAssetsPath . '/lib/jquery-3.4.1.min.js', $appDirectory . '/lib/jquery-3.4.1.min.js');
		$this->copyFile($uaAssetsPath . '/lib/ui-bootstrap-tpls-2.5.0.min.js', $appDirectory . '/lib/ui-bootstrap-tpls-2.5.0.min.js');
		$this->copyDirectory($uaAssetsPath . '/lib/angular-1.7.8', $appDirectory . '/lib/angular-1.7.8');
		$this->copyDirectory($uaAssetsPath . '/lib/angular-local-storage-0.7.1', $appDirectory . '/lib/angular-local-storage-0.7.1');

		// Update modules.
		foreach ($plugins as $plugin)
		{
			$this->copyDirectory($plugin->getAbsolutePath() . '/WebApp/Assets', $appDirectory . '/' . $plugin->getName(), ['png', 'jpg']);
		}
	}

	/**
	 * @return array
	 */
	protected function getModelDefinitions()
	{
		$models = [];
		foreach ($this->modelManager->getModelsNames() as $modelName)
		{
			$model = $this->modelManager->getModelByName($modelName);
			if (!$model)
			{
				continue;
			}

			$pluginKey = \strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
			$pluginLabel = $this->i18nManager->trans($pluginKey, ['ucf']);
			if ($pluginKey === $pluginLabel)
			{
				continue;
			}

			$models[] = [
				'name' => $model->getName(),
				'label' => $this->i18nManager->trans($model->getLabelKey(), ['ucf']),
				'leaf' => !$model->hasDescendants(),
				'root' => !$model->hasParent(),
				'abstract' => $model->isAbstract(),
				'inline' => $model->isInline(),
				'publishable' => $model->isPublishable(),
				'localized' => $model->isLocalized(),
				'activable' => $model->isActivable(),
				'useCorrection' => $model->useCorrection(),
				'editable' => $model->isEditable(),
				'plugin' => $pluginLabel,
				'descendants' => $model->getDescendantsNames(),
				'ancestors' => $model->getAncestorsNames(),
				'compatible' => \array_merge([$model->getName()], $model->getAncestorsNames())
			];
		}
		return $models;
	}

	/**
	 * @param string $srcPath
	 * @param string $targetPath
	 * @param string[] $includedExtensions
	 */
	protected function copyDirectory($srcPath, $targetPath, $includedExtensions = [])
	{
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && \strpos($current->getBasename(), '.') !== 0
				&& (!$includedExtensions || \in_array($current->getExtension(), $includedExtensions, true))
			)
			{
				$filePath = $current->getPathname();
				$this->copyFile($filePath, $targetPath . \str_replace($srcPath, '', $filePath));
			}
			$it->next();
		}
	}

	/**
	 * @param string $srcPath
	 * @param string $targetPath
	 * @param string[] $vars
	 * @param string[] $values
	 */
	protected function copyFile($srcPath, $targetPath, $vars = [], $values = [])
	{
		if ($vars && \count($vars) === \count($values))
		{
			$content = \str_replace($vars, $values, \Change\Stdlib\FileUtils::read($srcPath));
		}
		else
		{
			$content = \Change\Stdlib\FileUtils::read($srcPath);
		}
		\Change\Stdlib\FileUtils::write($targetPath, $content);
	}

	/**
	 * @param \Change\Plugins\Plugin $corePlugin
	 * @param \Change\Plugins\Plugin[] $plugins
	 * @param bool $minify
	 * @return array
	 */
	protected function getCachedScripts(\Change\Plugins\Plugin $corePlugin, array $plugins, bool $minify)
	{
		$cachedScripts = [];

		// Ua core packages.
		/** @var \Change\Plugins\Plugin $uaPlugin */
		$uaPlugin = $this->pluginManager->getModule('Rbs', 'Ua');
		$assetsPath = $uaPlugin->getAbsolutePath() . '/Assets';
		$this->addScripts($cachedScripts, $assetsPath . '/ProximisIntl', 'Rbs_Ua/ProximisIntl', $minify);
		$assetsPath = $uaPlugin->getAbsolutePath() . '/Assets/ProximisCore';
		$this->addScript($cachedScripts, $assetsPath, 'Rbs_Ua/ProximisCore', $assetsPath . '/proximisCore.js', $minify);
		$this->addScripts($cachedScripts, $assetsPath . '/filters', 'Rbs_Ua/ProximisCore/filters', $minify);
		$this->addScripts($cachedScripts, $assetsPath . '/services', 'Rbs_Ua/ProximisCore/services', $minify);

		// -- Core plugin.
		$pluginName = $corePlugin->getName();

		// rbschange.js
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets';
		$path = $assetsPath . '/rbschange.js';
		$this->addScript($cachedScripts, $assetsPath, $pluginName, $path, $minify);

		// Change lib.
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets/change';
		$this->addScripts($cachedScripts, $assetsPath, $pluginName . '/change', $minify);

		// Core JS.
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets/app';
		$this->addScripts($cachedScripts, $assetsPath, $pluginName . '/app', $minify);

		// Other plugins.
		foreach ($plugins as $plugin)
		{
			$assetsPath = $plugin->getAbsolutePath() . '/WebApp/Assets';
			if (!\is_dir($assetsPath))
			{
				continue;
			}

			$pluginName = $plugin->getName();
			$this->addScripts($cachedScripts, $assetsPath, $pluginName, $minify);
		}

		return $cachedScripts;
	}

	/**
	 * @param array $cachedScripts
	 * @param string $assetsPath
	 * @param string $pluginName
	 * @param bool $minify
	 */
	protected function addScripts(array &$cachedScripts, string $assetsPath, string $pluginName, bool $minify)
	{
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($assetsPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && $current->getExtension() === 'js')
			{
				$this->addScript($cachedScripts, $assetsPath, $pluginName, $current->getRealPath(), $minify);
			}
			$it->next();
		}
	}

	/**
	 * @param array $cachedScripts
	 * @param string $assetsPath
	 * @param string $pluginName
	 * @param string $path
	 * @param bool $minify
	 */
	protected function addScript(array &$cachedScripts, string $assetsPath, string $pluginName, string $path, bool $minify)
	{
		$content = \file_get_contents($path);
		$cachedScripts[\str_replace($assetsPath, $pluginName, $path)] = $minify ? \JSMin\JSMin::minify($content) : $content;
	}

	/**
	 * @param \Change\Plugins\Plugin $corePlugin
	 * @param \Change\Plugins\Plugin[] $plugins
	 * @param bool $minify
	 * @return array
	 */
	protected function getCachedStylesheets(\Change\Plugins\Plugin $corePlugin, array $plugins, bool $minify)
	{
		$cachedScripts = [];

		// Core plugin.
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets/app';
		$this->addStylesheets($cachedScripts, $assetsPath, $corePlugin->getName() . '/app', $minify);

		// Other plugins.
		foreach ($plugins as $plugin)
		{
			$assetsPath = $plugin->getAbsolutePath() . '/WebApp/Assets';
			if (!\is_dir($assetsPath))
			{
				continue;
			}

			$this->addStylesheets($cachedScripts, $assetsPath, $plugin->getName(), $minify);
		}

		return $cachedScripts;
	}

	/**
	 * @param array $cachedScripts
	 * @param string $assetsPath
	 * @param string $pluginName
	 * @param bool $minify
	 */
	protected function addStylesheets(array &$cachedScripts, string $assetsPath, string $pluginName, bool $minify)
	{
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($assetsPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && $current->getExtension() === 'css')
			{
				$this->addStylesheet($cachedScripts, $assetsPath, $pluginName, $current->getRealPath(), $minify);
			}
			$it->next();
		}
	}

	/**
	 * @param array $cachedScripts
	 * @param string $assetsPath
	 * @param string $pluginName
	 * @param string $path
	 * @param bool $minify
	 */
	protected function addStylesheet(array &$cachedScripts, string $assetsPath, string $pluginName, string $path, bool $minify)
	{
		$content = \file_get_contents($path);
		$cachedScripts[\str_replace($assetsPath, $pluginName, $path)] = $minify ? (new \MatthiasMullie\Minify\CSS($content))->minify() : $content;
	}

	/**
	 * @param \Change\Plugins\Plugin $corePlugin
	 * @param \Change\Plugins\Plugin[] $plugins
	 * @return array
	 */
	protected function getCachedTemplates(\Change\Plugins\Plugin $corePlugin, array $plugins)
	{
		$cachedTemplates = [];

		// Core plugin.
		$assetsPath = $corePlugin->getAbsolutePath() . '/WebApp/Assets/app';
		$this->addTemplates($cachedTemplates, $assetsPath, 'app');

		// Other plugins.
		foreach ($plugins as $plugin)
		{
			$assetsPath = $plugin->getAbsolutePath() . '/WebApp/Assets';
			if (!\is_dir($assetsPath))
			{
				continue;
			}

			$this->addTemplates($cachedTemplates, $assetsPath, $plugin->getName());
		}

		return $cachedTemplates;
	}

	/**
	 * @param array $cachedTemplates
	 * @param string $assetsPath
	 * @param string $pluginName
	 */
	protected function addTemplates(array &$cachedTemplates, string $assetsPath, string $pluginName)
	{
		$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($assetsPath,
			\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
		while ($it->valid())
		{
			/* @var $current \RecursiveDirectoryIterator */
			$current = $it->current();
			if ($current->isFile() && $current->getExtension() === 'twig' &&
				!\Change\Stdlib\StringUtils::endsWith($current->getFilename(), '.inc.twig'))
			{
				$this->addTemplate($cachedTemplates, $assetsPath, $pluginName, $current->getRealPath());
			}
			$it->next();
		}
	}

	/**
	 * @param array $cachedTemplates
	 * @param string $assetsPath
	 * @param string $pluginName
	 * @param string $path
	 */
	protected function addTemplate(array &$cachedTemplates, string $assetsPath, string $pluginName, string $path)
	{
		$cachedTemplates[\str_replace($assetsPath, $pluginName, $path)] = $this->renderTemplateFile($path, []);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param array $i18nData
	 */
	protected function addI18nKeysForJS(\Change\Plugins\Plugin $plugin, array &$i18nData)
	{
		$webappjsFilePath = $plugin->getAssetsPath() . '/I18n/fr_FR/webappjs.json';
		if (\is_readable($webappjsFilePath))
		{
			$pluginKey = \Change\Stdlib\StringUtils::toLower('m.' . $plugin->getVendor() . '.' . $plugin->getShortName() . '.webappjs');
			$i18n = \json_decode(\file_get_contents($webappjsFilePath), true, 512, JSON_THROW_ON_ERROR);
			foreach ($i18n as $key => $data)
			{
				$i18nData[$pluginKey][\Change\Stdlib\StringUtils::toLower($key)] = $data['message'];
			}
		}
	}

	/**
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 */
	public function renderTemplateFile($pathName, array $attributes)
	{
		$loader = new \Project\Familinet\WebApp\TwigLoader(\dirname($pathName), $this->pluginManager);
		$twig = new \Twig\Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->templateManager->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render(\basename($pathName), $attributes);
	}

	/**
	 * @var string|null
	 */
	protected $cachePath;

	/**
	 * @return string
	 */
	protected function getCachePath()
	{
		if ($this->cachePath === null)
		{
			$this->cachePath = $this->application->getWorkspace()->cachePath('WebApp', 'Templates', 'Compiled');
			\Change\Stdlib\FileUtils::mkdir($this->cachePath);
		}
		return $this->cachePath;
	}
}