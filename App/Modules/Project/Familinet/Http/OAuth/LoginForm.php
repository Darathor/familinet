<?php
namespace Project\Familinet\Http\OAuth;

/**
 * @name \Rbs\Admin\Http\OAuth\LoginForm
 */
class LoginForm
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function execute($event)
	{
		$data = $event->getParam('data');
		/** @var $httpEvent \Change\Http\Event */
		$httpEvent = $event->getParam('httpEvent');
		if ($data['realm'] === 'familinet')
		{
			$applicationServices = $event->getApplicationServices();
			$uri = $httpEvent->getUrlManager()->getSelf();
			$uri->setPath($event->getApplication()->getConfiguration('Change\Install\webBaseURLPath') . '/admin.php/')->setQuery('');
			$data['baseUrl'] = $uri->normalize()->toString();
			$path = $event->getApplication()->getWorkspace()->pluginsModulesPath('Rbs', 'Admin', 'Http', 'OAuth', 'Assets', 'login.twig');
			$templateManager = $applicationServices->getTemplateManager();
			$templateManager->addExtension(new \Rbs\Ua\Presentation\Twig\Extension($event->getApplication(),
				$applicationServices->getI18nManager(), $applicationServices->getModelManager()));
			$html = $templateManager->renderTemplateFile($path, $data);
			$event->setParam('html', $html);
		}
	}
}