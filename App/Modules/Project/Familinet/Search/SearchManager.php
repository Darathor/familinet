<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\Search;

/**
 * @name \Project\Familinet\Search\SearchManager
 */
class SearchManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_MANAGER_IDENTIFIER = 'SearchManager';

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Project/Familinet/Events/SearchManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('searchDocuments', function ($event) { $this->onQuerySearchDocuments($event); }, 5);
		$eventManager->attach('searchDocuments', function ($event) { $this->onDefaultSearchDocuments($event); });
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider($dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @param string $modelName
	 * @param string $searchString
	 * @param int $searchMode
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 */
	public function searchDocuments($modelName, $searchString, $searchMode = \Change\Db\Query\Predicates\Like::ANYWHERE, $offset = 0, $limit = 10)
	{
		$args = $this->getEventManager()->prepareArgs([
			'modelName' => $modelName,
			'searchString' => $searchString,
			'searchMode' => $searchMode,
			'offset' => $offset,
			'limit' => $limit,
			'propertyNames' => ['label']
		]);
		$this->getEventManager()->trigger('searchDocuments', $this, $args);

		return [
			'documents' => isset($args['documents']) && \is_array($args['documents']) ? $args['documents'] : [],
			'count' => (int)($args['count'] ?? 0),
			'offset' => (int)$args['offset'],
			'limit' => (int)$args['limit']
		];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onQuerySearchDocuments($event)
	{
		if ($event->getParam('documents') !== null || $event->getParam('query') !== null)
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$model = $applicationServices->getModelManager()->getModelByName($event->getParam('modelName'));
		if (!$model)
		{
			return;
		}
		$propertyNames = [];
		foreach ($event->getParam('propertyNames') as $propertyName)
		{
			$property = $model->getProperty($propertyName);
			if ($property && !$property->getStateless() && $property->getType() === \Change\Documents\Property::TYPE_STRING)
			{
				$propertyNames[] = $propertyName;
			}
		}
		if (!$propertyNames)
		{
			return;
		}

		$query = $applicationServices->getDocumentManager()->getNewQuery($model);
		$restrictions = [];
		foreach ($propertyNames as $propertyName)
		{
			$restrictions[] = $query->like($propertyName, $event->getParam('searchString'));
			$query->addOrder($propertyName);
		}
		$query->orPredicates($restrictions);
		if (!\in_array('id', $propertyNames, true))
		{
			$query->addOrder('id');
		}
		$event->setParam('query', $query);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultSearchDocuments($event)
	{
		$query = $event->getParam('query');
		if (!($query instanceof \Change\Documents\Query\Query) || $event->getParam('documents') !== null)
		{
			return;
		}

		$count = $query->getCountDocuments();

		$offset = $event->getParam('offset');
		if ($offset > $count)
		{
			$event->setParam('offset', 0);
		}

		$event->setParam('documents', $query->getDocuments($event->getParam('offset'), $event->getParam('limit'))->toArray());
	}
}