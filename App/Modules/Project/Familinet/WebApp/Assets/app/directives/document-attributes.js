(function() {
	'use strict';

	var app = angular.module('RbsChange');

	function appDocumentAttributesInputLink(REST, scope, element, attributes) {
		var context = attributes['context'];

		scope.data = {
			typologies: []
		};

		scope.$watch('document', function(newValue) {
			if (newValue !== undefined) {
				if (!angular.isObject(newValue.typology$)) {
					newValue.typology$ = { __id: 0 };
				}
			}
		});

		scope.$watch('document.typology$.__id', function(newValue, oldValue) {
			if (newValue) {
				REST.resource('Rbs_Generic_Typology', newValue).then(scope.generateAttributesEditor);
			}
			else if (oldValue) {
				scope.clearAttributesEditor();
			}
		});

		scope.clearAttributesEditor = function() {
			scope.attributeGroups = [];
		};

		scope.generateAttributesEditor = function(typology) {
			scope.attributeGroups = [];
			var attributesDefinitions = typology['attributesDefinitions'];
			for (var groupIndex = 0; groupIndex < typology['groups'].length; groupIndex++) {
				var group = typology['groups'][groupIndex];
				var groupData = {
					id: group.id,
					label: group.label,
					title: group.title,
					attributes: []
				};

				for (var attributeIndex = 0; attributeIndex < group.attributes.length; attributeIndex++) {
					var attributeDefinition = attributesDefinitions[group.attributes[attributeIndex].id];
					if (!attributeDefinition) {
						continue;
					}
					if (context && (!typology.visibilities[context] ||
						!typology.visibilities[context].attributes[attributeDefinition.id])) {
						continue;
					}
					groupData.attributes.push(attributeDefinition);

					// Handle default values.
					if (scope.document.typology$[attributeDefinition.name] === undefined) {
						scope.document.typology$[attributeDefinition.name] = attributeDefinition.defaultValue || null;
					}
				}

				if (groupData.attributes.length) {
					scope.attributeGroups.push(groupData);
				}
			}
		};
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentAttributesInput
	 * @name Document attributes edition
	 * @element fieldset
	 * @restrict A
	 *
	 * @description
	 * Used to display the <em>Attributes</em> section in Document editors.
	 *
	 * @example
	 * <pre>
	 *     <div data-app-document-attributes-input=""></div>
	 * </pre>
	 */
	app.directive('appDocumentAttributesInput', ['RbsChange.REST', function(REST) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-attributes-input.twig',
			replace: false,
			scope: {
				document: '=appDocumentAttributesInput'
			},
			link: function(scope, element, attributes) {
				appDocumentAttributesInputLink(REST, scope, element, attributes);
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentPersistedAttributesInput
	 * @name Document attributes edition with persistence
	 * @element fieldset
	 * @restrict A
	 *
	 * @description
	 * Used to display the <em>Attributes</em> section in Document editors with persistence.
	 *
	 * @example
	 * <pre>
	 *     <div data-app-document-persisted-attributes-input=""></div>
	 * </pre>
	 */
	app.directive('appDocumentPersistedAttributesInput', ['RbsChange.REST', function(REST) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-attributes-input.twig',
			replace: false,
			scope: {
				document: '=appDocumentPersistedAttributesInput',
				persist: '='
			},
			link: function(scope, element, attributes) {
				scope.handlePersist = 'true';
				scope.documentName = attributes['documentName'] || 'document';
				appDocumentAttributesInputLink(REST, scope, element, attributes);
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentAttributesContext
	 * @name Document attributes data
	 * @element fieldset
	 * @restrict A
	 *
	 * @description
	 * Used to display the <em>Attributes</em> section in Document editors.
	 *
	 * @example
	 * <pre>
	 *     <div data-app-document-attributes-context=""></div>
	 * </pre>
	 */
	app.directive('appDocumentAttributesContext', ['RbsChange.REST', function(REST) {
		return {
			restrict: 'A',

			link: function(scope, element, attributes) {
				var contextName = attributes['appDocumentAttributesContext'];
				scope.$watch('document.typology$.__id', function(newValue, oldValue) {
					if (newValue) {
						// TODO: optimize with a service.
						REST.resource('Rbs_Generic_Typology', newValue).then(scope.extractContextAttributes);
					}
					else if (oldValue) {
						scope.clearContextAttributes();
					}
				});

				scope.clearContextAttributes = function() {
					scope.typology = null;
					scope.attributeContext = [];
				};

				scope.extractContextAttributes = function(typology) {
					if (!angular.isObject(scope.document.typology$)) {
						scope.clearContextAttributes();
						return;
					}

					scope.typology = typology;
					scope.attributeContext = [];
					var attributesDefinitions = typology['attributesDefinitions'];
					if (angular.isObject(typology.visibilities[contextName])) {
						angular.forEach(typology.visibilities[contextName].attributes, function(boolean, id) {
							var definition = attributesDefinitions[id];
							if (!definition) {
								return;
							}
							var value = scope.document.typology$[definition.name];
							if (value || definition.type === 'Boolean') {
								scope.attributeContext.push({ id: id, definition: definition, value: value });
							}
						});
					}
				};
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @name RbsChange.directive:appAttributesValue
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * Renders an attribute value.
	 *
	 * @param {Object} appAttributesValue The attribute data.
	 */
	app.directive('appAttributesValue', ['$sce', 'RbsChange.REST', function($sce, REST) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-attributes-value.twig',
			replace: false,
			scope: { attribute: '=appAttributesValue' },
			link: function(scope, elm, attr) {
				scope.imageFormat = attr['imageFormat'] || 'attribute';

				if (scope.attribute.definition.collectionCode) {
					var params = { code: scope.attribute.definition.collectionCode };
					REST.action('collectionItems', params).then(
						function(data) {
							scope.collectionItems = data.items;
						},
						function(data) {
							console.error(data);
						}
					);
				}
				else if (scope.attribute.definition.type === 'DocumentId') {
					scope.docList = [];
					angular.forEach(REST.getResources([scope.attribute.value]), function(doc) {
						scope.docList.push(doc);
					});
				}
				else if (scope.attribute.definition.type === 'DocumentIdArray') {
					scope.docList = [];
					angular.forEach(REST.getResources(scope.attribute.value), function(doc) {
						scope.docList.push(doc);
					});
				}

				scope.isCollection = function() {
					return !!scope.attribute.definition.collectionCode;
				};

				scope.isArrayValue = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.definition.type === 'DocumentIdArray');
				};

				scope.isDocument = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.definition.type === 'DocumentId');
				};

				scope.isLinkValue = function(value) {
					return (value && value.common.URL && value.common.URL['canonical']);
				};

				scope.isLink = function(attribute) {
					attribute = attribute || scope.attribute;
					return (scope.isDocument(attribute) && scope.isLinkValue(attribute.value));
				};

				scope.isImageValue = function(value) {
					return (value && value['formats'] && value['formats'][scope.imageFormat]);
				};

				scope.isImage = function(attribute) {
					attribute = attribute || scope.attribute;
					return (scope.isDocument(attribute) && scope.isImageValue(attribute.value));
				};

				scope.isHtml = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.definition.type === 'RichText' && angular.isString(attribute.value));
				};

				scope.isDate = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.definition.type === 'Date');
				};

				scope.isDateTime = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && attribute.definition.type === 'DateTime');
				};

				scope.isString = function(attribute) {
					attribute = attribute || scope.attribute;
					return (attribute && !scope.isDocument(attribute) && !scope.isArrayValue(attribute)
						&& !scope.isHtml(attribute) && !scope.isDate(attribute) && !scope.isDateTime(attribute));
				};

				scope.trustHtml = function(html) {
					return $sce.trustAsHtml(html);
				};
			}
		}
	}]);
})();