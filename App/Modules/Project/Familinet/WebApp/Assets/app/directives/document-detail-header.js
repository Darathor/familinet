(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appDocumentDetailHeader', DocumentDetailHeader);
	function DocumentDetailHeader() {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-detail-header.twig',
			replace: true,
			scope: {
				document: '=appDocumentDetailHeader'
			},
			link: function(scope, element, attrs) {
				attrs.$observe('subtitle', function(newValue) {
					scope.subtitle = newValue;
				});
				attrs.$observe('title', function(newValue) {
					scope.title = newValue;
				});
				attrs.$observe('readonly', function(newValue) {
					scope.readonly = newValue && newValue !== 'false';
				});
				attrs.$observe('allowCreateFrom', function(newValue) {
					scope.allowCreateFrom = newValue && newValue !== 'false';
				});
			}
		};
	}
})();