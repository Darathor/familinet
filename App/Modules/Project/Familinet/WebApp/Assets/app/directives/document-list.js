(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('appDocumentList', ['RbsChange.REST', 'RbsChange.ArrayUtils', '$location', DocumentListDirective]);

	function DocumentListDirective(REST, ArrayUtils, $location) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-list.twig',
			scope: {
				staticFilters: '=',
				newUrlParameters: '=',
				fullListUrl: '=',
				exposedData: '=?'
			},

			link: function DocumentListDirectiveLink(scope, elm, attrs) {
				var loading = false;

				scope.initialized = false;
				scope.contentTemplateUrl = attrs.contentTemplateUrl;
				scope.blockLevel = attrs.blockLevel ? attrs.blockLevel : 'primary';
				scope.allowCreation = attrs.allowCreation === 'true';
				scope.newRouteName = attrs.newRouteName ? attrs.newRouteName : 'new';
				scope.listTitle = attrs.listTitle;
				scope.modelName = attrs.modelName;
				scope.compact = attrs.compact === 'true';
				scope.list = {
					documents: []
				};
				scope.data = {
					typologies: {}
				};

				function getIntegerQueryParam(name, defaultValue) {
					if (!scope.compact) {
						var value = parseInt($location.search()[name], 10);
						return (value > 0) ? value : defaultValue;
					}
					return defaultValue;
				}

				function getStringQueryParam(name, defaultValue) {
					if (!scope.compact) {
						var value = $location.search()[name];
						return value ? value : defaultValue;
					}
					return defaultValue;
				}

				scope.pagination = {
					maxSize: 5,
					currentPage: getIntegerQueryParam('page', 1),
					limit: getIntegerQueryParam('limit', 10) // TODO: store the value in local storage.
				};
				// Temporary value to not loose the current page.
				scope.pagination.totalItems = scope.pagination.currentPage * scope.pagination.limit;

				scope.sort = {
					property: attrs.sortProperty ? attrs.sortProperty : 'label',
					desc: attrs.sortDesc === 'true'
				};
				scope.filter = {
					enabled: attrs.filterProperties !== 'none',
					shown: !scope.compact,
					properties: (attrs.filterProperties || 'label').split(','),
					value: getStringQueryParam('filterValue', ''),
					mode: getStringQueryParam('filterMode', 'contains'),
					typology: getIntegerQueryParam('typology', null),
					show: function() {
						scope.filter.shown = true;
					},
					hide: function() {
						scope.filter.shown = false;
						scope.filter.value = '';
						scope.filter.typology = null;
					}
				};

				scope.columns = attrs.columns ? attrs.columns.split(',') : [];
				if (ArrayUtils.inArray('label', scope.columns) < 0) {
					scope.columns.push('label');
				}

				function load() {
					loading = false;
					if (scope.modelName) {
						var params = {
							offset: (scope.pagination.currentPage - 1) * scope.pagination.limit,
							limit: scope.pagination.limit,
							sort: scope.sort.property,
							desc: scope.sort.desc,
							column: scope.columns
						};

						if (scope.staticFilters || scope.filter.value || scope.filter.typology) {
							params.filter = {
								name: 'group',
								parameters: { all: 0, configured: 0 },
								operator: 'AND',
								filters: []
							};

							if (scope.filter.value) {
								if (scope.filter.properties.length > 1) {
									var group = {
										name: 'group',
										parameters: { all: 0, configured: 0 },
										operator: 'OR',
										filters: []
									};
									params.filter.filters.push(group);
								}
								else {
									group = params.filter;
								}
								angular.forEach(scope.filter.properties, function(propertyName) {
									group.filters.push({
										name: propertyName,
										parameters: {
											propertyName: propertyName,
											operator: scope.filter.mode,
											value: scope.filter.value
										}
									});
								});
							}

							if (scope.filter.typology) {
								params.filter.filters.push({
									name: 'hasTypology',
									parameters: {
										restriction: 'hasTypology',
										operator: 'eq',
										value: scope.filter.typology
									}
								});
							}

							if (scope.staticFilters) {
								for (var i = 0; i < scope.staticFilters.length; i++) {
									params.filter.filters.push(scope.staticFilters[i]);
								}
							}
						}

						REST.collection(scope.modelName, params).then(function(data) {
							scope.list.data = data;
							scope.list.documents = data.resources;
							scope.pagination.totalItems = data.pagination.count;
							scope.pagination.offset = data.pagination.offset;
							scope.pagination.currentPage = Math.floor(data.pagination.offset / data.pagination.limit) + 1;
							if (!scope.compact) {
								$location.search('limit', data.pagination.limit);
								$location.search('page', scope.pagination.currentPage);
								$location.search('filterValue', scope.filter.value || null);
								$location.search('filterMode', (scope.filter.mode && scope.filter.mode !== 'contains') ? scope.filter.mode : null);
								$location.search('typology', scope.filter.typology ? scope.filter.typology : null);
							}
							scope.initialized = true;

							if (angular.isObject(scope.exposedData)) {
								// noinspection JSPrimitiveTypeWrapperUsage
								scope.exposedData.totalItems = data.pagination.count;
							}
						});
					}
				}

				if (!scope.compact) {
					scope.location = $location;
					var currentPath = scope.location.path();
					scope.$watch('location.search()', function() {
						// Are we leaving this place?
						if (currentPath !== scope.location.path()) {
							return;
						}

						var page = getIntegerQueryParam('page', false);
						if (page > 0 && scope.pagination.currentPage !== page) {
							scope.pagination.currentPage = page;
						}
						var limit = getIntegerQueryParam('limit', false);
						if (limit > 0 && scope.pagination.limit !== limit) {
							scope.pagination.limit = limit;
						}
					});
				}

				function reload() {
					if (!loading) {
						loading = true;
						setTimeout(load, 100);
					}
				}

				scope.$watch('pagination.currentPage', function() {
					reload();
				});
				scope.$watch('pagination.limit', function() {
					reload();
				});
				scope.$watch('filter', function() {
					reload();
				}, true);
				scope.$watch('sort', function() {
					reload();
				}, true);
				scope.$watch('columns', function() {
					reload();
				}, true);
				scope.$on('AppReloadList', function() {
					reload();
				}, true);
			}
		};
	}

	app.directive('appDocumentListContent', function() {
		return {
			restrict: 'A',
			link: function DocumentListDirectiveLink(scope, elm) {
				elm.addClass('table table-striped table-bordered table-hover');
				if (scope.compact) {
					elm.addClass('table-condensed');
				}
			}
		};
	});
})();