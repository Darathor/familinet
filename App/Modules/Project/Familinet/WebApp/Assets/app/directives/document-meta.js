(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('appDocumentMeta', function () {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/document-meta.twig',

			link: function (scope) {
			}
		};
	});
})();