(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('appDocumentSelect', ['RbsChange.REST', 'RbsChange.Utils', 'RbsChange.i18n', function(REST, Utils, i18n) {
		return {
			restrict: 'A',
			require: 'ngModel',
			templateUrl: 'app/directives/document-select.twig',
			scope: true,

			link: function(scope, element, attrs, ngModel) {
				var valueIds = (attrs.valueIds === 'true');
				scope.selector = {
					value: 0,
					options: undefined
				};

				var loadOptions = function() {
					if (!attrs.acceptedModel) {
						console.error('appDocumentSelect[' + attrs.propertyLabel + ']', 'missing accepted-model');
						return;
					}

					var params = { limit: 100 };
					if (attrs.filter) {
						params.filter = jQuery.parseJSON(attrs.filter);
					}

					REST.collection(attrs.acceptedModel, params).then(
						function(docs) {
							setOptions(docs.resources);
						},
						function(result) { console.error(result); }
					);
				};

				if (attrs.hasOwnProperty('ngDisabled')) {
					attrs.$observe('disabled', function(disabled) {
						scope.disabled = disabled;
						if (disabled === false) {
							loadOptions();
						}
					});
				}
				else {
					loadOptions();
				}

				attrs.$observe('filter', function () {
					loadOptions();
				});

				function setOptions(options) {
					options.unshift({
						id: 0,
						label: '- ' + i18n.trans('m.project.familinet.webappjs.document_selector_select_element | ucf') + ' -'
					});
					scope.selector.options = options;
					ngModel.$setViewValue(findOption(scope.selector.value));
				}

				function findOption(id) {
					if (id && scope.selector.options) {
						for (var i = 0; i < scope.selector.options.length; i++) {
							if (scope.selector.options[i].id === id) {
								return scope.selector.options[i];
							}
						}
					}
				}

				scope.$watch('selector.value', function(value) {
					if (angular.isArray(scope.selector.options)) {
						var selectedOption = findOption(value);
						var selectedValue = angular.isObject(selectedOption) ? selectedOption.id : null;
						var viewValue = angular.isObject(ngModel.$viewValue) ? ngModel.$viewValue.id : ngModel.$viewValue;
						if (selectedValue !== viewValue) {
							ngModel.$setViewValue(selectedOption);
						}
					}
				});

				// viewValue => modelValue
				ngModel.$parsers.unshift(function(value) {
					if (value === undefined) {
						return value;
					}
					if (valueIds) {
						if (angular.isObject(value) && value.hasOwnProperty('id')) {
							if (value.id) {
								return value.id;
							}
						}
						return value;
					}
					else {
						if (angular.isObject(value) && value.hasOwnProperty('id')) {
							if (value.id) {
								return value;
							}
						}
						return null;
					}
				});

				// modelValue => viewValue
				ngModel.$formatters.unshift(function(value) {
					if (value === undefined) {
						return value;
					}
					if (valueIds) {
						if (value) {
							scope.selector.value = value;
						}
						else {
							scope.selector.value = 0;
						}
						return findOption(value);
					}
					else {
						if (Utils.isDocument(value)) {
							scope.selector.value = value.id;
						}
						else {
							scope.selector.value = 0;
						}
						return value;
					}
				});
			}
		};
	}]);
})();