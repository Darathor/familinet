(function($) {
	'use strict';

	var app = angular.module('RbsChange');

	function documentPickerLinkFunction(scope, element, attrs, ngModel, multiple, REST, Utils, i18n, Document) {
		var valueIds = (attrs.valueIds === 'true');

		scope.multiple = multiple;
		scope.allowCreation = attrs['allowCreation'] === 'true';
		scope.selectorElement = element;

		scope.doc = { list: [] };

		attrs.$observe('acceptedModel', function(value) {
			scope.acceptedModel = value;
			if (angular.isObject(scope.autoComplete) && angular.isFunction(scope.autoComplete.refresh)) {
				scope.autoComplete.refresh();
			}
		});

		function arrayCopy(array) {
			var copy = [];
			angular.forEach(array, function(item) {copy.push(item);});
			return copy;
		}

		function getDocById(array, id) {
			for (var i = 0; i < array.length; i++) {
				if (array[i].id === id) {
					return array[i];
				}
			}
			return null;
		}

		// viewValue => modelValue
		ngModel.$parsers.unshift(function(viewValue) {
			if (viewValue === undefined) {
				return viewValue;
			}

			var modelValue;
			if (valueIds) {
				if (multiple) {
					modelValue = Utils.toIds(viewValue);
				}
				else {
					modelValue = (Utils.isDocument(viewValue)) ? viewValue.id : 0
				}
			}
			else {
				if (multiple) {
					modelValue = arrayCopy(viewValue)
				}
				else {
					modelValue = viewValue
				}
			}
			return modelValue;
		});

		// modelValue => viewValue
		ngModel.$formatters.unshift(function(modelValue) {
			if (modelValue === undefined) {
				return modelValue;
			}
			var viewValue = multiple ? [] : null;
			var oldList = scope.doc.list;
			var docList = [];

			if (valueIds) {
				if (multiple) {
					if (angular.isArray(modelValue)) {
						var ids = [], doc;
						angular.forEach(modelValue, function(id) {
							if (angular.isNumber(id) && id > 0) {
								doc = getDocById(oldList, id);
								if (doc) {
									docList.push(doc);
								}
								else {
									ids.push(id);
								}
							}
							else {
								console.error('Invalid number value: ', id);
							}
						});
						if (ids.length) {
							angular.forEach(REST.getResources(ids), function(doc) {
								docList.push(doc);
							});
						}
						viewValue = arrayCopy(docList);
					}
				}
				else {
					if (angular.isNumber(modelValue) && modelValue > 0) {
						viewValue = REST.getResources([modelValue])[0];
						docList.push(viewValue);
					}
				}
			}
			else {
				if (multiple) {
					if (angular.isArray(modelValue)) {
						angular.forEach(modelValue, function(doc) {
							if (Utils.isDocument(doc)) {
								docList.push(doc);
								viewValue.push(doc);
							}
						});
					}
				}
				else {
					if (Utils.isDocument(modelValue)) {
						viewValue = modelValue;
						docList.push(viewValue);
					}
				}
			}

			scope.doc.list = docList;
			return viewValue;
		});

		// If applied when valueIds it breaks the display (#38).
		// If not applied on multiple it breaks content update on external site data import (#40).
		if (multiple && !valueIds) {
			scope.$watchCollection(attrs.ngModel, function() {
				scope.doc.list = ngModel.$modelValue || [];
			}, true);
		}

		// Watch from changes coming from the <rbs-token-list/> which is bound to `scope.doc.list`.
		scope.$watchCollection('doc.list', function() {
			if (scope.doc.list.length === 0 && ngModel.$viewValue === undefined) {
				return;
			}
			if (multiple) {
				ngModel.$setViewValue(arrayCopy(scope.doc.list));
			}
			else {
				ngModel.$setViewValue(scope.doc.list.length ? scope.doc.list[0] : null);
			}
		});

		// Options.

		scope.selector = {
			mode: attrs.mode === 'options' ? 'options' : 'search',
			options: [],
			selectedValue: 0,
			selectOption: selectOption,
			notInSelection: scope.notInSelection,
			createNew: false
		};

		var staticFilterKey = attrs.staticFilters;
		if (scope.selector.mode === 'options') {
			if (attrs.hasOwnProperty('ngDisabled')) {
				attrs.$observe('disabled', function(disabled) {
					scope.disabled = disabled;
					if (disabled === false) {
						scope.$watch(staticFilterKey, function() {
							loadOptions();
						}, true);
					}
				});
			}
			else {
				scope.$watch(staticFilterKey, function() {
					loadOptions();
				}, true);
			}
		}

		function loadOptions() {
			var params = {
				limit: 250,
				sort: 'label',
				desc: false
			};

			if (staticFilterKey && scope[staticFilterKey]) {
				params.filter = {
					name: 'group',
					parameters: { all: 0, configured: 0 },
					operator: 'AND',
					filters: []
				};

				for (var i = 0; i < scope[staticFilterKey].length; i++) {
					params.filter.filters.push(scope[staticFilterKey][i]);
				}
			}

			REST.collection(attrs.acceptedModel, params).then(function(docs) {
				var options = docs.resources;
				if (attrs['allowCreation'] === 'true') {
					options.unshift({
						id: 'new',
						label: '- ' + i18n.trans('m.project.familinet.webappjs.document_selector_new_element | ucf') + ' -'
					});
				}
				options.unshift({
					id: 0,
					label: '- ' + i18n.trans('m.project.familinet.webappjs.document_selector_select_element | ucf') + ' -'
				});
				scope.selector.options = options;
			});
		}

		function selectOption() {
			// Create new element.
			if (scope.selector.selectedValue === 'new') {
				scope.selector.createNew = true;
				scope.selector.selectedValue = 0;
				// We need to defer focusing the field because until next digest, it is hidden.
				setTimeout(function() { element.find('input.auto-complete-value').focus(); }, 100);
			}
			// Select existing element.
			else if (scope.selector.selectedValue > 0) {
				REST.resource(scope.selector.selectedValue).then(function(doc) {
					scope.doc.list.push(doc);
				});
				scope.selector.selectedValue = 0;
			}
		}

		scope.notInSelection = function (doc) {
			return getDocById(scope.doc.list, doc.id) == null;
		};

		// If model = user, add "me" button.
		scope.addMe = function() {
			if (scope.acceptedModel === 'Rbs_User_User') {
				REST.resource(scope.user.id).then(function(doc) {
					scope.doc.list.push(doc);
				});
			}
		};

		scope.amISelected = function() {
			if (scope.acceptedModel === 'Rbs_User_User') {
				for (var i = 0; i < scope.doc.list.length; i++) {
					if (scope.doc.list[i].id === scope.user.id) {
						return true;
					}
				}
			}
			return false;
		};

		scope.getItemTemplateName = function(item) {
			return Document.getItemTemplateName(item);
		};
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentSelector
	 * @name Document picker (single)
	 * @restrict A
	 *
	 * @param {Object} ng-model The ng-model to bind.
	 * @param {Boolean=} value-ids If true, stores the ID instead of the Document object.
	 * @param {String=} accepted-model Model name of the Documents that can be selected.
	 *
	 * @description
	 * Displays a Document selector to let the user select a single Document.
	 * The user is redirected to the list of Documents of the desired model name and a
	 * <strong>Navigation Context</strong> is created.
	 */
	app.directive('appDocumentSelector', ['RbsChange.REST', 'RbsChange.Utils', 'RbsChange.i18n', 'RbsChange.Document',
		function(REST, Utils, i18n, Document) {
			return {
				restrict: 'A',
				templateUrl: 'app/directives/document-selector.twig',
				require: 'ngModel',
				scope: true,

				link: {
					pre: function(scope, iElement, attrs, ngModel) {
						documentPickerLinkFunction(scope, iElement, attrs, ngModel, false, REST, Utils, i18n, Document);
					}
				}
			};
		}
	]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentArraySelector
	 * @name Document picker (multiple)
	 * @restrict A
	 *
	 * @param {Object} ng-model The ng-model to bind.
	 * @param {Boolean=} value-ids If true, stores the ID instead of the Document object.
	 * @param {String=} accepted-model Model name of the Documents that can be selected.
	 *
	 * @description
	 * Displays a Document selector to let the user select multiple Documents.
	 * The user is redirected to the list of Documents of the desired model name and a
	 * <strong>Navigation Context</strong> is created.
	 */
	app.directive('appDocumentArraySelector', ['RbsChange.REST', 'RbsChange.Utils', 'RbsChange.i18n', 'RbsChange.Document',
		function(REST, Utils, i18n, Document) {
			return {
				restrict: 'A',
				templateUrl: 'app/directives/document-selector.twig',
				require: 'ngModel',
				scope: true,

				link: {
					pre: function(scope, iElement, attrs, ngModel) {
						documentPickerLinkFunction(scope, iElement, attrs, ngModel, true, REST, Utils, i18n, Document);
					}
				}
			};
		}
	]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:appDocumentSelectorTokenList
	 * @name Token list
	 * @restrict A
	 *
	 * @description
	 * Displays a list with multiple items inside that can be reordered with drag and drop.
	 *
	 * Items can be Documents objects.
	 *
	 * ### Defining the template for each item ###
	 *
	 * If the items are Documents, it is possible to define a template for each item, based on the Model
	 * of the Documents. In a JavaScript file of the plugin (ie. `admin.js`), add something like this:
	 * <pre>
	 * app.run(['$templateCache', function ($templateCache)
	 * {
	 *    $templateCache.put(
	 *       // Template ID: 'picker-item-' + Document_Model_Name + '.twig'
	 *       'picker-item-Rbs_Catalog_Product.twig',
	 *       // The Document is available in this scope as 'item'
	 *       '<span style="line-height: 30px"><img rbs-storage-image="item.adminthumbnail" thumbnail="XS"/> (= item.label =)</span>'
	 *    );
	 * }]);
	 * </pre>
	 *
	 * @param {Array} items Array of items to display in the list (two-way data-binding).
	 * @param {Boolean} disable-reordering If true, the user cannot reorder the elements in the list.
	 */
	app.directive('appDocumentSelectorTokenList', ['RbsChange.ArrayUtils', 'RbsChange.Document', function(ArrayUtils, Document) {
		return {
			restrict: 'A',
			replace: true,
			templateUrl: 'app/directives/document-selector-token-list.twig',
			scope: {
				items: '='
			},

			link: function linkFn(scope, elm, attrs) {
				var dragging, isHandle, startIndex, stopIndex,
					placeholder = $('<li class="sortable-placeholder"></li>');

				scope.readonly = !!attrs.readonly;

				scope.getItemTemplateName = function(item) {
					return Document.getItemTemplateName(item);
				};

				scope.remove = function(index) {
					ArrayUtils.remove(scope.items, index);
				};

				// Enable drag and drop to reorder the items.
				elm.on({
					mousedown: function() {
						isHandle = true;
					},
					mouseup: function() {
						isHandle = false;
					}
				}, 'span.glyphicon-sort');

				elm.on({
					dragstart: function(e) {
						if (!isHandle) {
							return false;
						}
						isHandle = false;
						var dt = e.originalEvent.dataTransfer;
						dt.effectAllowed = 'move';
						dragging = $(this);
						dragging.addClass('sortable-dragging');
						startIndex = dragging.index();
						dt.setData('Text', dragging.text());
					},

					dragend: function() {
						dragging.removeClass('sortable-dragging').show();
						stopIndex = placeholder.index();
						placeholder.detach();
						if (startIndex !== stopIndex) {
							ArrayUtils.move(scope.items, startIndex, stopIndex);
							scope.$apply();
						}
						dragging = null;
					}
				}, 'li[data-id]');

				elm.on({
					dragenter: function(e) {
						e.preventDefault();
						e.originalEvent.dataTransfer.dropEffect = 'move';
					},

					dragover: function(e) {
						e.preventDefault();
						e.originalEvent.dataTransfer.dropEffect = 'move';

						if (dragging) {
							if (!$(this).is(placeholder)) {
								dragging.hide();
								placeholder.height(dragging.height());
								$(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
								if (placeholder.index() > startIndex) {
									placeholder.html(placeholder.index());
								}
								else {
									placeholder.html(placeholder.index() + 1);
								}
							}
						}
					},

					drop: function(e) {
						e.stopPropagation();
						e.preventDefault();
						placeholder.after(dragging);
					}
				}, 'li[data-id], li.sortable-placeholder');
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis:pxItemSelectorAutoCompleteList
	 * @name pxItemSelectorAutoCompleteList
	 * @restrict A
	 *
	 * @description
	 * Used in document and path rule selectors to handle the auto-completion.
	 */
	app.directive('appDocumentSelectorAutoCompleteList', ['$timeout', 'RbsChange.REST', function($timeout, REST) {
		return {
			restrict: 'A',
			link: function(scope, element) {
				scope.autoComplete = {
					show: false,
					refreshing: false,
					value: '',
					items: [],
					offset: 0,
					limit: 10,
					totalCount: 0,
					loadingNextPage: false,
					open: openAutoCompleteList,
					close: closeAutoCompleteList,
					refresh: refreshAutoCompleteList,
					loadPreviousPage: loadAutoCompleteListPreviousPage,
					loadNextPage: loadAutoCompleteListNextPage,
					add: autoCompleteAdd,
					allowCreation: scope.allowCreation,
					create: autoCompleteCreate,
					notInSelection: scope.notInSelection
				};

				function autoCompleteAdd(item) {
					if (!scope.multiple) {
						closeAutoCompleteList();
						scope.doc.list.length = 0;
					}
					scope.doc.list.push(item);
				}

				function autoCompleteCreate() {
					var doc = REST.newResource(scope.acceptedModel);
					doc.label = getTrimmedValue();
					REST.save(doc).then(
						function(savedDoc) {
							scope.doc.list.push(savedDoc);
							scope.autoComplete.value = '';
						},
						function (result) {
							console.error(result);
						}
					);
					if (!scope.multiple) {
						closeAutoCompleteList();
					}
				}

				function openAutoCompleteList() {
					if (scope.autoComplete.show) {
						return;
					}

					$timeout(refreshListPosition, 300);
					scope.autoComplete.show = true;
				}

				function refreshListPosition() {
					var selector = scope.selectorElement;
					var css = {
						left: 0,
						top: (selector.outerHeight() + selector.offset().top + 5 - selector.offset().top) + 'px',
						width: selector.outerWidth(),
						transition: 'top 1s ease'
					};
					element.css(css);
				}

				function closeAutoCompleteList() {
					if (!scope.autoComplete.show) {
						return;
					}
					scope.autoComplete.value = '';
					scope.autoComplete.show = false;
					scope.selector.createNew = false;
				}

				function refreshAutoCompleteList() {
					scope.autoComplete.error = null;
					if (!getTrimmedValue().length) {
						scope.autoComplete.items = [];
						scope.autoComplete.totalCount = 0;
						closeAutoCompleteList();
					}
					else if (!scope.autoComplete.refreshing) {
						openAutoCompleteList();
						scope.autoComplete.refreshing = true;
						$timeout(doRefreshAutoCompleteList);
					}
				}

				function doRefreshAutoCompleteList() {
					if (!getTrimmedValue().length) {
						scope.autoComplete.items = [];
						scope.autoComplete.totalCount = 0;
						scope.autoComplete.refreshing = false;
						return;
					}

					scope.autoComplete.offset = 0;
					REST.call(REST.getBaseUrl('projectFamilinet/searchDocuments'), getAutoCompleteParams()).then(
						function(data) {
							scope.autoComplete.items = data.resources;
							scope.autoComplete.totalCount = data.pagination.count;
							scope.autoComplete.refreshing = false;
						},
						function(error) {
							console.error('projectFamilinet/searchDocuments', error);
							scope.autoComplete.error = error;
							scope.autoComplete.refreshing = false;
						}
					);
				}

				function loadAutoCompleteListPreviousPage() {
					if (scope.autoComplete.offset > 0) {
						scope.autoComplete.offset -= scope.autoComplete.limit;
						doLoadPage()
					}
				}

				function loadAutoCompleteListNextPage() {
					if ((scope.autoComplete.offset + scope.autoComplete.items.length) < scope.autoComplete.totalCount) {
						scope.autoComplete.offset += scope.autoComplete.limit;
						doLoadPage();
					}
				}

				function doLoadPage() {
					scope.autoComplete.error = null;
					scope.autoComplete.refreshing = true;
					REST.call(REST.getBaseUrl('projectFamilinet/searchDocuments'), getAutoCompleteParams()).then(
						function(data) {
							scope.autoComplete.items = data.resources;
							scope.autoComplete.refreshing = false;
						},
						function(error) {
							console.error('projectFamilinet/searchDocuments', error);
							scope.autoComplete.error = error;
							scope.autoComplete.refreshing = false;
						}
					);
				}

				function getAutoCompleteParams() {
					return {
						modelName: scope.acceptedModel,
						searchString: getTrimmedValue().toLowerCase(),
						offset: scope.autoComplete.offset,
						limit: scope.autoComplete.limit
					};
				}

				function getTrimmedValue() {
					return scope.autoComplete.value.trim();
				}

				// Watch from changes coming from the list which is bound to `scope.doc.list`.
				scope.$watchCollection('doc.list', function() {
					if (scope.autoComplete.show) {
						$timeout(refreshListPosition, 300);
					}
				});

				scope.$watch('autoComplete.value', refreshAutoCompleteList);
			}
		}
	}]);

	app.run(['$templateCache', function($templateCache) {
		if (!$templateCache.get('picker-item-default.twig')) {
			$templateCache.put('picker-item-default.twig', '(= item.label =)' +
				' <small data-ng-if="item.foundAlias">[ (= item.foundAlias =) ]</small>');
		}
	}]);
})(window.jQuery);