(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appPagination', PaginationDirective);
	function PaginationDirective() {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/pagination.twig',
			replace: true,
			scope: {
				pagination: '=appPagination'
			},
			link: function(scope) {
				scope.options = [{ value: 5 }, {value: 10}, {value:25}, {value:50}, {value:75}, {value:100}];
			}
		};
	}
})();