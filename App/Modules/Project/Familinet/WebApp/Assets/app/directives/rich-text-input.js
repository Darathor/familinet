(function() {
	"use strict";

	var app = angular.module('RbsChange');
	var editorIdCounter = 0;

	app.directive('appRichTextInput', ['$timeout', RichTextInput]);
	function RichTextInput() {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/rich-text-input.twig',
			require: '?ngModel',
			scope: { ngModel: '=ngModel'},
			link: function(scope, element, attrs, ngModel) {
				scope.editorId = ++editorIdCounter;
				scope.containerId = 'rbsInputMarkdownEditor' + scope.editorId;

				ngModel.$render = function() {
					if (angular.isObject(ngModel.$viewValue)) {
						if (!ngModel.$viewValue.e) {
							ngModel.$setViewValue({ e: 'Markdown', t: '', h: null });
						}
					}
				};
			}
		};
	}
})();