(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('appRichText', ['RbsChange.REST', RichtextDirective]);
	function RichtextDirective(REST) {
		return {
			restrict: 'A',
			replace: false,
			scope: true,

			link: function RichtextDirectiveLink(scope, elm, attrs) {
				scope.path = attrs.path;
				scope.$watch(scope.path, function(value) {
					var params = {
						'profile': (attrs.profile || 'Website'),
						'editor': value.e
					};
					REST.postAction('renderRichText', value.t, params).then(function(data) {
						elm.html(data.html);
						value.h = data.html;
					});
				}, true);
			}
		};
	}
})();