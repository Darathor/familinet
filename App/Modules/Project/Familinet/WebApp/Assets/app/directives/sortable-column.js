(function() {
	'use strict';

	let app = angular.module('RbsChange');

	app.directive('appSortableColumn', [DocumentListDirective]);
	function DocumentListDirective() {
		return {
			restrict: 'A',
			transclude: true,
			templateUrl: 'app/directives/sortable-column.twig',
			scope: true,

			link: function DocumentListDirectiveLink(scope, elm, attrs) {
				scope.columnName = attrs.appSortableColumn;

				scope.applySort = function() {
					if (scope.sort.property === scope.columnName) {
						scope.sort.desc = (scope.sort.desc !== 'true') ? 'true' : 'false';
					}
					else {
						scope.sort.property = scope.columnName;
						scope.sort.desc = 'false';
					}
				};
			}
		};
	}
})();