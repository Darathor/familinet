(function() {
	"use strict";

	var app = angular.module('RbsChange');

	function buildRatingLabel(value, scale, i18n) {
		var text = i18n.trans('m.project.familinet.webappjs.star_rating_labeled', { 'RATINGVALUE': value, 'BESTRATING': scale });

		var absoluteValue = value * 100 / scale;

		text += ' (';
		if (absoluteValue <= 10) {
			text += i18n.trans('m.project.familinet.webappjs.star_rating_bad');
		}
		else if (absoluteValue <= 30) {
			text += i18n.trans('m.project.familinet.webappjs.star_rating_medium');
		}
		else if (absoluteValue <= 50) {
			text += i18n.trans('m.project.familinet.webappjs.star_rating_good');
		}
		else if (absoluteValue <= 80) {
			text += i18n.trans('m.project.familinet.webappjs.star_rating_very_good');
		}
		else {
			text += i18n.trans('m.project.familinet.webappjs.star_rating_excellent');
		}
		text += ')';

		return text;
	}

	app.directive('appStarRating', ['RbsChange.i18n', function(i18n) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/star-rating.twig',
			scope: {
				rating: '=appStarRating',
				scale: '@'
			},
			link: function(scope, elm, attrs) {
				scope.stars = [];
				for (var i = 0; i < scope.scale; i++) {
					scope.stars.push(i);
				}

				scope.mode = attrs['mode'] || 'star';

				scope.$watch('rating', function() {
					if (attrs['scaled'] !== 'true') {
						scope.scaledRating = Math.floor(scope.rating / (100 / scope.scale));
					}
					else {
						scope.scaledRating = scope.rating;
					}
					scope.star_rating = scope.starRating(scope.scaledRating);
				});

				scope.starRating = function(value) {
					return buildRatingLabel(value, scope.scale, i18n);
				}
			}
		}
	}]);

	app.directive('appStarRatingInput', ['RbsChange.i18n', function(i18n) {
		return {
			restrict: 'A',
			templateUrl: 'app/directives/star-rating-input.twig',
			require: '?ngModel',
			scope: {
				scale: '@'
			},
			link: function(scope, elm, attrs, ngModel) {
				if (!ngModel) {
					return;
				}
				scope.stars = [];
				for (var i = 0; i < scope.scale; i++) {
					scope.stars.push(i + 1);
				}
				scope.scaled = {
					rating: null
				};

				ngModel.$render = function() {
					if (ngModel.$viewValue === '' || ngModel.$viewValue === null || isNaN(ngModel.$viewValue)) {
						scope.scaled.rating = '';
					}
					else {
						scope.scaled.rating = Math.floor(ngModel.$viewValue / (100 / scope.scale));
					}
				};

				scope.$watch('scaled.rating', function(value, oldValue) {
					if (value !== oldValue) {
						if (value === '' || value === null || isNaN(value)) {
							ngModel.$setViewValue(null);
						}
						else {
							ngModel.$setViewValue(Math.ceil(value * (100 / scope.scale)));
						}
					}
					scope.ratingText = scope.starRating(value);
				});

				scope.starRating = function(value) {
					if (value === '') {
						return i18n.trans('m.project.familinet.webappjs.star_rating_none');
					}
					else {
						return buildRatingLabel(value, scope.scale, i18n);
					}
				}
			}
		}
	}]);

	app.directive('appInputStarRatingItem', function() {
		return {
			restrict: 'A',
			scope: false,
			link: function(scope, elm, attrs) {
				var handlerIn = function handlerIn() {
					if (attrs['appInputStarRatingItem'] === '') {
						scope.scaled.hover = '';
					}
					else {
						scope.scaled.hover = parseInt(attrs['appInputStarRatingItem']);
					}
					scope.$digest();
				};
				var handlerOut = function handlerOut() {
					scope.scaled.hover = -1;
					scope.$digest();
				};
				elm.hover(handlerIn, handlerOut);
			}
		}
	});
})();