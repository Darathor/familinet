/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appBoolean
	 * @function
	 *
	 * @description
	 * Formats a Boolean value with localized <em>yes</em> or <em>no</em>.
	 *
	 * @param {Boolean} value The boolean value to format.
	 */
	app.filter('appBoolean', ['RbsChange.i18n', function(i18n) {
		return function(input) {
			return i18n.trans(input ? 'm.project.familinet.webappjs.yes' : 'm.project.familinet.webappjs.no');
		};
	}]);
})();