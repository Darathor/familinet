(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appDateTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with date and time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appDateTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'medium');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appDate
	 * @function
	 *
	 * @description
	 * Formats a Date object with date, without time.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appDate', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumDate');
		};
	}]);

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appTime
	 * @function
	 *
	 * @description
	 * Formats a Date object with time, without the date.
	 *
	 * @param {Date} date The date to format.
	 */
	app.filter('appTime', ['$filter', function($filter) {
		return function(input) {
			return $filter('date')(input, 'mediumTime');
		};
	}]);
})();