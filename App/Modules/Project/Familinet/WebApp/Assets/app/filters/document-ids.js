/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appDocumentIds
	 * @function
	 *
	 * @description
	 * Returns the ids of a document array.
	 *
	 * @param {array} input Document array.
	 */
	app.filter('appDocumentIds', function() {
		return function(input) {
			var result = [];
			if (angular.isArray(input)) {
				angular.forEach(input, function(document) {
					if (angular.isNumber(document)) {
						result.push(document);
					}
					else if (angular.isObject(document) && document.id) {
						result.push(document.id);
					}
				});
			}
			return result;
		};
	});
})();