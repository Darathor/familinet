(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @id RbsChange.filter:appModelLabel
	 * @name appModelLabel
	 * @function
	 *
	 * @description
	 * Returns the model's label.
	 *
	 * @param {string} string The model's label.
	 */
	app.filter('appModelLabel', ['RbsChange.Models', function(Models) {
		return function(input) {
			return input ? Models.getModelLabel(input) : '';
		};
	}]);
})();