/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appNumber
	 * @function
	 *
	 * @description
	 * Formats an number and remove ending zeros in decimals.
	 *
	 * @param {number} value Number to format.
	 * @param {number} fractionSize Number of decimal places to round the number to.
	 */
	app.filter('appNumber', ['$filter', function($filter) {
		return function(input, fractionSize) {
			return $filter('number')(input, fractionSize).replace(/0+$/, '').replace(/[.,]$/, '');
		};
	}]);
})();