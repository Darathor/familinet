/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appPrice
	 * @function
	 *
	 * @description
	 * Formats an amount with the EUR currency.
	 *
	 * @param {number} value The amount value to format.
	 */
	app.filter('appPrice', ['$filter', function($filter) {
		return function(input) {
			return $filter('pxCorePrice')(input, 'EUR');
		};
	}]);
})();