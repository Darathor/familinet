/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc filter
	 * @name RbsChange.filter:appURL
	 * @function
	 *
	 * @description
	 * Returns the URL for the input Document, Model name or Plugin name.
	 *
	 * Routes are defined in the `Assets/Admin/routes.json` file in each Plugin. Commonly used route names are:
	 *
	 * - `new` (with input = Model name)
	 * - `detail` (with input = Document)
	 * - `edit` (with input = Document)
	 * - `list` (with input = Model name)
	 *
	 * @param {Document|String} input Document, Model name or Plugin name.
	 * @param {String=} routeName Route name (defaults to <em>edit</em>).
	 *
	 * @example
	 * <pre>
	 *   <a ng-href="(= 'Rbs_Catalog_Product' | appURL:'new' =)">Create new product</a>
	 *   <a ng-href="(= product | appURL =)">Edit (= product.label =)</a>
	 * </pre>
	 */
	app.filter('appURL', ['$filter', 'RbsChange.Utils', function($filter, Utils) {
		return function(doc, routeName, params) {
			if (!routeName) {
				if (Utils.isDocument(doc)) {
					routeName = 'detail';
				}
				else if (Utils.isModelName(doc)) {
					routeName = 'list';
				}
				else if (Utils.isModuleName(doc)) {
					routeName = 'home';
				}
			}
			else if (routeName === 'newFrom') {
				routeName = 'createFrom';
			}
			return '#/' + $filter('rbsURL')(doc, routeName, params);
		};
	}]);
})();