(function() {
	"use strict";

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc service
	 * @name RbsChange.service:Document
	 * @description Document service.
	 */
	app.service('RbsChange.Document', ['$routeParams', '$rootScope', '$templateCache', '$location', 'RbsChange.REST', 'RbsChange.Breadcrumb',
		'RbsChange.i18n', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter', 'RbsChange.DocumentCache', documentService]);

	function documentService($routeParams, $rootScope, $templateCache, $location, REST, Breadcrumb, i18n, NotificationCenter, ErrorFormatter, DocumentCache) {
		var pendingNotifications = [];

		/**
		 * @param {Object} scope The controller's scope.
		 * @param {String} modelName The document model name.
		 * @return {Object} The promise of document load.
		 */
		function loadDocument(scope, modelName) {
			var promise = REST.resource(modelName, $routeParams.id);
			promise.then(
				function(doc) {
					scope.modelName = modelName;
					scope.document = doc;
				},
				function(reason) {
					pendingNotifications.push([
						i18n.trans('m.project.familinet.webappjs.load_error'),
						ErrorFormatter.format(reason),
						'LOAD_DOCUMENT'
					]);
					console.warn('Can\'t load the document with id ' + $routeParams.id + ', so redirect to list.\nError message: ' + reason.message);
					Breadcrumb.goParent();
				}
			);
			return promise;
		}

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#initDetailView
		 *
		 * @description Init the scope for a document detail view.
		 *
		 * @param {Object} scope The controller's scope.
		 * @param {String} modelName The document model name.
		 * @return {Object} The promise of document load.
		 */
		this.initDetailView = function(scope, modelName) {
			return loadDocument(scope, modelName);
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#reloadDetailView
		 *
		 * @description Relaod the document detail view.
		 *
		 * @param {Object} scope The controller's scope.
		 * @return {Object} The promise of document load.
		 */
		this.reloadDetailView = function(scope) {
			DocumentCache.removeAll();
			return loadDocument(scope, scope.modelName);
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#initDetailView
		 *
		 * @description Init the scope for a document edition view.
		 *
		 * @param {Object} scope The controller's scope.
		 * @param {String} modelName The document model name.
		 * @return {Object} The promise of document load.
		 */
		this.initEditView = function(scope, modelName) {
			return loadDocument(scope, modelName);
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#getItemTemplateName
		 *
		 * @param {Object} item
		 * @returns {string}
		 */
		this.getItemTemplateName = function(item) {
			if (!item || !item.model) {
				return 'picker-item-default.twig';
			}

			var tplName = 'picker-item-' + item.model + '.twig';
			return $templateCache.get(tplName) ? tplName : 'picker-item-default.twig';
		};

		/**
		 * @ngdoc function
		 * @methodOf RbsChange.service:Document
		 * @name RbsChange.service:document#deleteFromDetailView
		 *
		 * @param {Object} document
		 */
		this.deleteFromDetailView = function(document) {
			if (confirm(i18n.trans('m.project.familinet.webappjs.document_editor_confirm_delete'))) {
				REST.delete(document).then(function() {
					var entries = Breadcrumb.pathEntries();
					var entry = entries[entries.length - 1];
					$location.url(entry.url());
				});
			}
		};

		$rootScope.$on('$routeChangeSuccess', function() {
			for (var i = 0; i < pendingNotifications.length; i++) {
				NotificationCenter.error(pendingNotifications[i][0], pendingNotifications[i][1], pendingNotifications[i][2]);
			}
			pendingNotifications = [];
		});
	}
})();