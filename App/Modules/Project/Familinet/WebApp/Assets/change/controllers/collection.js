/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc controller
	 * @id RbsChange.controller:RbsCollectionController
	 * @name RbsCollectionController
	 *
	 * @description
	 * This controller loads the items of a collection.
	 *
	 * @param {string} collectionCode The collection code.
	 * @param {string=} collectionParams The collection parameters (in form: `param1:value1;param2:value2;...`).
	 * @param {string=} collectionFilter If set, only items with label or value containing the given string will be returned.
	 * @param {string=} variableName The variable name in the scope where the items are set (default: `pxCollection`).
	 *
	 * @example
	 * Minimal example:
	 * ```html
	 * <select class="form-control"
	 *     data-ng-controller="RbsCollectionController"
	 *     data-collection-code="Rbs_Geo_All_Countries_Codes"
	 *     data-ng-options="item.value as item.label for item in pxCollection"
	 *     data-ng-model="country">
	 * </select>
	 * ```
	 *
	 * Full example:
	 * ```html
	 * <select class="form-control"
	 *     data-ng-controller="RbsCollectionController"
	 *     data-collection-code="Rbs_Generic_Typologies"
	 *     data-collection-params="modelName:Rbs_Catalog_Product"
	 *     data-collection-filter="(= filter =)"
	 *     data-variable-name="typologyOptions"
	 *     data-ng-options="item.value as item.label for item in typologyOptions"
	 *     data-ng-model="typology">
	 * </select>
	 * ```
	 */
	app.controller('RbsCollectionController', ['$scope', '$attrs', '$parse', 'RbsChange.REST', 'RbsChange.Utils', RbsCollectionController]);
	function RbsCollectionController(scope, attrs, $parse, REST, Utils) {
		var currentKey, itemsFilter, items = [];
		//noinspection JSUnresolvedVariable
		var variableSetter = $parse(attrs.variableName || 'pxCollection').assign;

		attrs.$observe('collectionCode', loadCollection);
		attrs.$observe('collectionParams', loadCollection);
		attrs.$observe('collectionFilter', function(filter) {
			itemsFilter = filter;
			loadCollection();
		});

		// Load Collection's items.
		function loadCollection() {
			var collectionCode = attrs.collectionCode;
			if (!collectionCode) {
				return;
			}

			//noinspection JSUnresolvedVariable
			var collectionParams = attrs.collectionParams;
			var key = collectionCode + '||' + collectionParams;
			if (currentKey === key) {
				setFilteredItems();
				return;
			}
			currentKey = key;

			var params = parseParamsAttr(collectionParams, collectionCode);
			params.asArray = true;

			REST.action('collectionItems', params).then(
				function(data) {
					items = data.items;
					setFilteredItems();
				},
				function(data) {
					console.error('Unable to load Collection "' + collectionCode + '".', data);
				}
			);
		}

		function parseParamsAttr(paramsAttr, collectionCode) {
			var params = {
				code: collectionCode
			};
			if (paramsAttr) {
				var parts = paramsAttr.split(';');
				angular.forEach(parts, function(value) {
					var values = value.split(':');
					params[values[0].trim()] = values[1].trim();
				});
			}
			return params;
		}

		function setFilteredItems() {
			items = items || [];
			var currentItems = [];
			if (itemsFilter) {
				angular.forEach(items, function(item) {
					if (Utils.containsIgnoreCase(item.label, itemsFilter) || Utils.containsIgnoreCase(item.value, itemsFilter)) {
						currentItems.push(item);
					}
				});
			}
			else {
				currentItems = items;
			}
			variableSetter(scope, currentItems);
		}
	}
})();