if (!('i18n' in __change)) {
	__change.i18n = {};
}

__change.i18n["m.rbs.admin.admin"] = {
	"error_formatter_properties_not_valid": "les propriétés suivantes ne sont pas valides :",
	"save_error": "L'enregistrement a échoué"
};