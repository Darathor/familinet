/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisModal
	 *
	 * @description
	 * The ProximisModal module contains some components for modal handling.
	 *
	 * This module depends on {@link http://angular-ui.github.io/bootstrap/ UI-bootstrap} library.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisModal` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisModal` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxModal`
	 */
	var app = angular.module('proximisModal', ['ui.bootstrap']);

	/**
	 * @ngdoc constant
	 * @id proximisModal.constant:ProximisModalGlobal
	 * @name ProximisModalGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {Object} modalDefinitions The modal definitions.
	 */
	app.constant('ProximisModalGlobal', __change);

	/**
	 * @ngdoc service
	 * @id proximisModal:proximisModalStack
	 * @name proximisModalStack
	 *
	 * @description
	 * This service is used to manage a modal stack where opening a new modal hides the others.
	 * It provides methods to close the last modal and show back the previous or close the full stack.
	 * This is useful for cascading modals, where the child extends the scope of the parent.
	 */
	app.service('proximisModalStack', ['$uibModal', '$timeout', 'ProximisModalGlobal', function($uibModal, $timeout, ProximisModalGlobal) {
		var definitions = ProximisModalGlobal.modalDefinitions || {};
		var className = 'modal-hidden-stack';
		var opened = [];

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name open
		 *
		 * @description
		 * Open a new modal and hide any other modal in the stack.
		 *
		 * @param {Object} options A list of options for the modal.
		 *    * `definitionName` (string|null): a modal definition name.
		 *    * `customClose` (bool): set to `true` to disable the default registration on modal closing showing the last modal.
		 *    * see {@link http://angular-ui.github.io/bootstrap/#/modal UI-bootstrap modal} for the list of available options.
		 * @config {string=} definitionName
		 * @config {bool=} customClose
		 * @config {Object=} resolve
		 *
		 * @returns {Object|null} The modal instance or `null` in error case.
		 */
		function open(options) {
			hideStack();

			if (options.hasOwnProperty('definitionName')) {
				var definitionName = options.definitionName;
				delete options.definitionName;

				if (definitions.hasOwnProperty(definitionName)) {
					/**
					 * @var {Object} definition
					 * @config {Array=} requiredResolve
					 */
					var definition = definitions[definitionName];
					if (definition.requiredResolve) {
						if (!options.resolve) {
							console.error('Missing resolve parameter for modal!');
							return null;
						}

						for (var i = 0; i < definition.requiredResolve.length; i++) {
							if (!options.resolve.hasOwnProperty(definition.requiredResolve[i])) {
								console.error('Missing resolve.' + definition.requiredResolve[i] + ' parameter for modal!');
								return null;
							}
						}
						delete definition.requiredResolve;
					}
					angular.extend(options, definitions[definitionName]);
				}
				else {
					console.error('Modal definition not found: ' + definitionName);
					return null;
				}
			}

			var classNames = className + ' modal-stack-idx-' + (opened.length + 1);
			if (options.windowClass) {
				options.windowClass += ' ' + classNames;
			}
			else {
				options.windowClass = classNames;
			}

			var modal = $uibModal.open(options);

			if (!options.hasOwnProperty('controller')) {
				modal.result.then(closeAll, closeAll);
			}
			else if (!options.customClose) {
				modal.result.then(showPrevious, showPrevious);
			}

			opened.push(modal);
			return modal;
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name close
		 *
		 * @description
		 * Close the last modal (and `parentModalsToClose` parent ones) and show the previous one.
		 *
		 * @param {number=} [parentModalsToClose=0] The number of parent modals that should be closed.
		 */
		function close(parentModalsToClose) {
			if (opened.length) {
				var modal = opened[opened.length - 1];
				var deferredDismiss = function() {
					$timeout(function() {
						modal.dismiss('cancel');
					});
				};
				modal.opened.then(deferredDismiss, deferredDismiss);
			}
			if (parentModalsToClose > 0) {
				opened.pop();
				close(parentModalsToClose - 1);
			}
			else {
				showPrevious();
			}
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name closeAll
		 *
		 * @description
		 * Close each modals in the stack.
		 */
		function closeAll() {
			for (var i = 0; i < opened.length; i++) {
				opened[i].dismiss();
			}
			opened = [];
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisModalStack
		 * @name showPrevious
		 *
		 * @description
		 * Show the previous opened modal. Call it only when the current one is closed.
		 */
		function showPrevious() {
			opened.pop();
			if (opened.length) {
				jQuery('.modal-stack-idx-' + opened.length).show();
			}
		}

		function hideStack() {
			jQuery('.' + className).hide();
		}

		this.open = open;
		this.close = close;
		this.closeAll = closeAll;
		this.showPrevious = showPrevious;
	}]);
})(window.jQuery);