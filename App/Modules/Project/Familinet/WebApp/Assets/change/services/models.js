(function(__change) {
	'use strict';

	var app = angular.module('RbsChange');

	app.provider('RbsChange.Models', function RbsModelsProvider() {
		var allModels = [];

		this.$get = ['$filter', 'RbsChange.REST', 'RbsChange.ArrayUtils', 'RbsChange.i18n',
			function($filter, REST, ArrayUtils, i18n) {
				function ChangeModel() {
					this.META$ = {
						'loaded': false,
						'links': {}
					};
				}

				function getByFilter(filter) {
					var models = [];
					if (allModels.length) {
						applyFilter(models, filter)
					}
					else {
						var loadedModels = [];
						angular.forEach(__change.modelDefinitions, function(result) {
							var model = new ChangeModel();
							angular.extend(model, result);
							model.META$.loaded = true;
							loadedModels.push(model);
						});
						allModels = $filter('orderBy')(loadedModels, ['plugin', 'label']);
						applyFilter(models, filter);
					}
					return models;
				}

				function getAll() {
					return getByFilter();
				}

				function applyFilter(models, filter) {
					if (!angular.isObject(filter)) {
						angular.forEach(allModels, function(testModel) {
							models.push(testModel);
						});
						return;
					}

					angular.forEach(allModels, function(testModel) {
						var valid = true;
						angular.forEach(filter, function(value, attr) {
							if (testModel.hasOwnProperty(attr)) {
								if (angular.isArray(value)) {
									if (angular.isArray(testModel[attr])) {
										if (ArrayUtils.intersect(testModel[attr], value).length === 0) {
											valid = false;
										}
									}
									else if (ArrayUtils.inArray(testModel[attr], value) === -1) {
										valid = false;
									}
								}
								else if (angular.isArray(testModel[attr])) {
									if (ArrayUtils.inArray(value, testModel[attr]) === -1) {
										valid = false;
									}
								}
								else if (testModel[attr] !== value) {
									valid = false;
								}
							}
							else {
								valid = false;
							}
						});
						if (valid) {
							models.push(testModel);
						}
					});
				}

				function getModelLabel(name) {
					if (!name) {
						return '';
					}
					var models = getByFilter({ name: name });
					if (models.length) {
						return models[0].label;
					}
					return name;
				}

				// Public API.
				return {
					getAll: getAll,
					getByFilter: getByFilter,
					getModelLabel: getModelLabel
				};
			}
		]
	});
})(window.__change);