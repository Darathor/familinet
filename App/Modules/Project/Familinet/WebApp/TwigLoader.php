<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Familinet\WebApp;

/**
 * @name \Project\Familinet\WebApp\TwigLoader
 */
class TwigLoader extends \Twig\Loader\FilesystemLoader
{
	/**
	 * @param string $path
	 * @param \Change\Plugins\PluginManager $pluginManager
	 */
	public function __construct(string $path, \Change\Plugins\PluginManager $pluginManager)
	{
		parent::__construct($path);
		foreach ($pluginManager->getModules() as $module)
		{
			if ($module->isAvailable())
			{
				$paths = [];
				$path = $module->getRelativePath() . \DIRECTORY_SEPARATOR . 'WebApp' . \DIRECTORY_SEPARATOR . 'Assets';
				if (\is_dir($path))
				{
					$paths[] = $path;
				}

				if (\count($paths))
				{
					$this->setPaths($paths, $module->getName());
				}
			}
		}
	}
}