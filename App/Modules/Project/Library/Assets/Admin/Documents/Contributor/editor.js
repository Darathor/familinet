(function() {
	"use strict";

	function Edit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.links)) {
						scope.document.links = [];
					}
					if (!angular.isArray(scope.document.aliases)) {
						scope.document.aliases = [];
					}
				};
			}
		};
	}
	angular.module('RbsChange').directive('rbsDocumentEditorRbsProjectLibraryContributorEdit', Edit);
})();