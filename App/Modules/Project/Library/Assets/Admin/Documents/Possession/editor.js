(function() {
	"use strict";

	function rbsDocumentEditorRbsProjectLibraryPossessionEdit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, element, attrs, editorCtrl) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.details)) {
						scope.document.details = [];
					}
				};
			}
		};
	}
	angular.module('RbsChange').directive('rbsDocumentEditorRbsProjectLibraryPossessionEdit', rbsDocumentEditorRbsProjectLibraryPossessionEdit);
})();