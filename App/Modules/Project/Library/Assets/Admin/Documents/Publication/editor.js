(function() {
	"use strict";

	function rbsDocumentEditorRbsProjectLibraryPublicationEdit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, element, attrs, editorCtrl) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.languages)) {
						scope.document.languages = [];
					}
				};
			}
		};
	}
	angular.module('RbsChange').directive('rbsDocumentEditorRbsProjectLibraryPublicationEdit', rbsDocumentEditorRbsProjectLibraryPublicationEdit);
})();