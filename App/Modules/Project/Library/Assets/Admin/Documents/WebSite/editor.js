(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorProjectLibraryWebSiteNew', Editor);
	app.directive('rbsDocumentEditorProjectLibraryWebSiteEdit', Editor);
	function Editor() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.parsers)) {
						scope.document.parsers = [];
					}
				};
			}
		};
	}

	app.directive('projectLibraryWebSiteParsers', projectLibraryWebSiteParsers);
	function projectLibraryWebSiteParsers() {
		return {
			restrict: 'A',
			link: function(scope) {
				var pagination = {
					currentPage: 1,
					totalItems: 0,
					parsersPerPage: null,
					currentParsers: []
				};

				function addNewParser() {
					var inline = {
						model: 'Project_Library_WebSiteParser',
						pattern: null,
						parserClass: null
					};
					scope.document.parsers.push(inline);
				}

				function buildPageParsers() {
					if (angular.isArray(scope.document.parsers)) {
						pagination.offset = (pagination.currentPage - 1) * pagination.parsersPerPage;
						var maxOffset = pagination.offset + pagination.parsersPerPage;
						if (maxOffset > scope.document.parsers.length) {
							maxOffset = scope.document.parsers.length;
						}
						pagination.currentParsers = scope.document.parsers.slice(pagination.offset, maxOffset);
					}
					else {
						pagination.offset = 0;
						pagination.currentParsers = [];
					}
				}

				scope.parsersManager = {
					addNewParser: addNewParser,
					pagination: pagination
				};

				scope.$watchCollection('document.parsers', function (parsers) {
					pagination.totalParsers = angular.isArray(parsers) ? parsers.length : 0;
					buildPageParsers();
				});

				scope.$watch('parsersManager.pagination.currentPage', function () {
					buildPageParsers();
				});

				scope.$watch('parsersManager.pagination.parsersPerPage', function () {
					buildPageParsers();
				});
			}
		};
	}
})();