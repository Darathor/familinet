(function() {
	"use strict";

	function Edit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.aliases)) {
						scope.document.aliases = [];
					}
					if (!angular.isArray(scope.document.links)) {
						scope.document.links = [];
					}
				};
			}
		};
	}
	angular.module('RbsChange').directive('rbsDocumentEditorRbsProjectLibraryWorkGroupEdit', Edit);
})();