<?php
/**
 * Copyright (C) 2014 Laewella
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Collection;

/**
 * @name \Project\Library\Collection\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_COLLECTION,
			static function (\Change\Events\Event $event) {
				switch ($event->getParam('code'))
				{
					case 'Project_Library_Collections':
						(new \Project\Library\Collection\Collections())->addCollections($event);
						break;
					case 'Project_Library_WebSiteParsers':
						(new \Project\Library\Collection\Collections())->addWebSiteParsers($event);
						break;
					case 'Project_Library_ContributionImportance':
						(new \Project\Library\Collection\Collections())->addContributionImportance($event);
						break;
				}
			}, 10);

		$this->listeners[] = $events->attach(\Change\Collection\CollectionManager::EVENT_GET_CODES, static function (\Change\Events\Event $event) {
			$codes = $event->getParam('codes', []);
			$codes = \array_merge($codes, [
				'Project_Library_Collections',
				'Project_Library_WebSiteParsers',
				'Project_Library_ContributionImportance'
			]);
			$event->setParam('codes', $codes);
		});
	}
}