<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Commands;

/**
 * @name \Project\Library\Commands\IndexAliases
 */
class IndexAliases
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$applicationServices = $event->getApplicationServices();
		if (!$applicationServices)
		{
			return;
		}
		$transactionManager = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		$dbProvider = $applicationServices->getDbProvider();

		$services = $event->getServices('Project_FamilinetServices');
		if (!($services instanceof \Project\Familinet\FamilinetServices))
		{
			throw new \RuntimeException('FamilinetServices not set', 999999);
		}
		$attributeManager = $services->getAttributeManager();

		$modelNameInput = $event->getParam('modelName', 'all');

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Work')
		{
			$indexer = \Project\Library\Documents\Work::getAliasesIndexer($dbProvider);
			$modelName = 'Project_Library_Work';
			$modelLabel = 'work';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $attributeManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_WorkGroup')
		{
			$indexer = \Project\Library\Documents\WorkGroup::getAliasesIndexer($dbProvider);
			$modelName = 'Project_Library_WorkGroup';
			$modelLabel = 'work group';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $attributeManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Contributor')
		{
			$indexer = \Project\Library\Documents\Contributor::getAliasesIndexer($dbProvider);
			$modelName = 'Project_Library_Contributor';
			$modelLabel = 'contributor';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $attributeManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Publisher')
		{
			$indexer = \Project\Library\Documents\Publisher::getAliasesIndexer($dbProvider);
			$modelName = 'Project_Library_Publisher';
			$modelLabel = 'publisher';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $attributeManager, $response);
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param string $modelLabel
	 * @param string $modelName
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function reIndexDocuments(\Project\Library\Indexer\AliasesIndexer $indexer, string $modelLabel, string $modelName,
		\Change\Documents\DocumentManager $documentManager, \Change\Transaction\TransactionManager $transactionManager,
		\Project\Familinet\AttributeManager $attributeManager, \Change\Commands\Events\CommandResponseInterface $response)
	{
		$indexed = 0;
		$inserted = 0;
		try
		{
			$transactionManager->begin();

			$indexer->deleteAll();

			$lastId = 0;
			do
			{
				$dqb = $documentManager->getNewQuery($modelName);
				$dqb->andPredicates($dqb->gt('id', $lastId));
				$documents = $dqb->getDocuments(0, 50)->toArray();
				/** @var \Project\Library\Indexer\AliasedDocument $document */
				foreach ($documents as $document)
				{
					$inserted += $document->createAliasesIndex($indexer, $attributeManager);
					$indexed++;
					$lastId = $document->getId();
				}
			}
			while ($documents);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$response->addErrorMessage($e->getMessage());
			throw $transactionManager->rollBack($e);
		}

		$response->addInfoMessage($modelLabel . 's indexed: ' . $indexed);
		$response->addInfoMessage($modelLabel . ' aliases inserted: ' . $inserted);
	}
}