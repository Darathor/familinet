<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Commands;

/**
 * @name \Project\Library\Commands\IndexLinks
 */
class IndexLinks
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$applicationServices = $event->getApplicationServices();
		if (!$applicationServices)
		{
			return;
		}
		$transactionManager = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		$dbProvider = $applicationServices->getDbProvider();

		try
		{
			$transactionManager->begin();

			(new \Project\Library\Indexer\LinksIndexer($dbProvider))->deleteAll();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$response->addErrorMessage($e->getMessage());
			throw $transactionManager->rollBack($e);
		}

		$modelNameInput = $event->getParam('modelName', 'all');

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Work')
		{
			$indexer = \Project\Library\Documents\Work::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_Work';
			$modelLabel = 'work';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Publication')
		{
			$indexer = \Project\Library\Documents\Publication::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_Publication';
			$modelLabel = 'publication';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_WorkGroup')
		{
			$indexer = \Project\Library\Documents\WorkGroup::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_WorkGroup';
			$modelLabel = 'work group';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Contributor')
		{
			$indexer = \Project\Library\Documents\Contributor::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_Contributor';
			$modelLabel = 'contributor';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Publisher')
		{
			$indexer = \Project\Library\Documents\Publisher::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_Publisher';
			$modelLabel = 'publisher';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		if ($modelNameInput === 'all' || $modelNameInput === 'Project_Library_Collection')
		{
			$indexer = \Project\Library\Documents\Collection::getLinksIndexer($dbProvider);
			$modelName = 'Project_Library_Collection';
			$modelLabel = 'collection';
			$this->reIndexDocuments($indexer, $modelLabel, $modelName, $documentManager, $transactionManager, $response);
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @param string $modelLabel
	 * @param string $modelName
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function reIndexDocuments(\Project\Library\Indexer\LinksIndexer $indexer, string $modelLabel, string $modelName,
		\Change\Documents\DocumentManager $documentManager, \Change\Transaction\TransactionManager $transactionManager,
		\Change\Commands\Events\CommandResponseInterface $response)
	{
		$indexed = 0;
		$inserted = 0;
		try
		{
			$transactionManager->begin();

			$lastId = 0;
			do
			{
				$dqb = $documentManager->getNewQuery($modelName);
				$dqb->andPredicates($dqb->gt('id', $lastId));
				$documents = $dqb->getDocuments(0, 50)->toArray();
				/** @var \Project\Library\Indexer\LinkedDocument $document */
				foreach ($documents as $document)
				{
					$inserted += $document->createLinksIndex($indexer);
					$indexed++;
					$lastId = $document->getId();
				}
			}
			while ($documents);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$response->addErrorMessage($e->getMessage());
			throw $transactionManager->rollBack($e);
		}

		$response->addInfoMessage($modelLabel . 's indexed: ' . $indexed);
		$response->addInfoMessage($modelLabel . ' links inserted: ' . $inserted);
	}
}