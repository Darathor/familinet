<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Commands;

/**
 * @name \Project\Familinet\Commands\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('config', static function () {
			$commandConfigPath = __DIR__ . '/Assets/config.json';
			if (\is_file($commandConfigPath))
			{
				return \json_decode(\file_get_contents($commandConfigPath), true, 512, JSON_THROW_ON_ERROR);
			}
			return null;
		});

		$this->listeners[] = $events->attach('project_library:index-aliases', static function ($event) {
			(new \Project\Library\Commands\IndexAliases())->execute($event);
		});

		$this->listeners[] = $events->attach('project_library:index-links', static function ($event) {
			(new \Project\Library\Commands\IndexLinks())->execute($event);
		});
	}
}