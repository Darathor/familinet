<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Collection
 */
class Collection extends \Compilation\Project\Library\Documents\Collection implements \Project\Library\Indexer\LinkedDocument
{
	use \Project\Library\Indexer\LinkedDocumentTrait;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$this->attachLinksIndexEvents($eventManager);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer)
	{
		$links = [];
		foreach ($this->getLinks() as $linkDocument)
		{
			$links[] = $linkDocument->getUrl();
		}

		// Add links to publisher.
		$publisher = $this->getPublisher();
		if ($publisher)
		{
			$indexer->index($publisher->getId(), $publisher->getDocumentModelName(), $links, $this->getId());
		}

		return $indexer->index($this->getId(), $this->getDocumentModelName(), $links);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event); 
		$restResult = $event->getParam('restResult');

		$publisher = $this->getPublisher();
		if ($publisher)
		{
			$restResult->setProperty('publisherId', $publisher->getId());
		}
	}
}
