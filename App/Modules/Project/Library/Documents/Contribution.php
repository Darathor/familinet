<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Contribution
 */
class Contribution extends \Compilation\Project\Library\Documents\Contribution
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$restResult->setProperty('year', $this->getCompiledYear());
		$restResult->setProperty('endYear', $this->getCompiledEndYear());

		$importance = $this->getImportance();
		if ($importance)
		{
			$collection = $event->getApplicationServices()->getCollectionManager()->getCollection('Project_Library_ContributionImportance');
			if ($collection)
			{
				$item = $collection->getItemByValue($importance);
				if ($item)
				{
					$restResult->setProperty('importanceTitle', $item->getTitle());
				}
			}
		}

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('contributionTypes', $this->getContributionTypesInfos());
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($this->getWork())
		{
			$restResult->setProperty('workId', $this->getWorkId());
		}
		if ($this->getContributor())
		{
			$restResult->setProperty('contributorId', $this->getContributorId());
		}
	}

	/**
	 * @return array
	 */
	protected function getContributionTypesInfos()
	{
		$contributionTypesInfos = [];
		foreach ($this->getContributionTypes() as $type)
		{
			$contributionTypesInfos[] = ['label' => $type->getLabel()];
		}
		return $contributionTypesInfos;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->refreshLabel();
		$this->compileDates();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->refreshLabel();
		$this->compileDates();
	}

	/**
	 * Refresh the label property.
	 */
	protected function refreshLabel()
	{
		$contributor = $this->getContributor();
		$work = $this->getWork();
		if ($contributor && $work)
		{
			$this->setLabel($contributor->getLabel() . ' - ' . $work->getLabel());
		}
		else
		{
			throw new \RuntimeException('Missing work or contributor in contribution.');
		}
	}

	protected function compileDates()
	{
		if ($this->getContributionYear())
		{
			$this->setCompiledYear($this->getContributionYear());
			$this->setCompiledEndYear($this->getContributionEndYear());
		}
		else
		{
			$work = $this->getWork();
			if ($work)
			{
				$this->setCompiledYear($work->getYear());
				$this->setCompiledEndYear($work->getEndYear());
			}
			else
			{
				$this->setCompiledYear();
				$this->setCompiledEndYear();
			}
		}
	}
}
