<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Contributor
 */
class Contributor extends \Compilation\Project\Library\Documents\Contributor
	implements \Project\Library\Indexer\AliasedDocument, \Project\Library\Indexer\LinkedDocument
{
	use \Project\Library\Indexer\AliasedDocumentTrait;
	use \Project\Library\Indexer\LinkedDocumentTrait;

	public const ALIASES_INDEX_TABLE = 'project_library_dat_contributor_aliases';
	public const ALIASES_INDEX_PREFIX = 'contributor_';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function () { $this->onFixYears(); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function () { $this->onFixYears(); }, 5);
		$this->attachAliasesIndexEvents($eventManager);
		$this->attachLinksIndexEvents($eventManager);
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager)
	{
		$label = $this->getLabel();
		$aliases = [$label];
		foreach ($this->getAliases() as $aliasDocument)
		{
			$aliases[] = $aliasDocument->getLabel();
		}

		return $indexer->index($this->getId(), $label, $aliases);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer)
	{
		$links = [];
		foreach ($this->getLinks() as $linkDocument)
		{
			$links[] = $linkDocument->getUrl();
		}

		return $indexer->index($this->getId(), $this->getDocumentModelName(), $links);
	}

	protected function onFixYears()
	{
		$birthDate = $this->getBirthDate();
		if ($birthDate)
		{
			$this->setBirthYear($birthDate->format('Y'));
		}

		$deathDate = $this->getDeathDate();
		if ($deathDate)
		{
			$this->setDeathYear($deathDate->format('Y'));
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$restResult->setProperty('contributorType', $this->getContributorType());

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$foundAlias = $this->getFoundAlias();
			if ($foundAlias)
			{
				$restResult->setProperty('foundAlias', $foundAlias);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('aliasesData', $this->getAliasesInfos());
			$contributorsInfos = $this->getContributionsInfos();
			if ($contributorsInfos)
			{
				$restResult->setProperty('contributions', $contributorsInfos);
			}
		}

		$restResult->setProperty('birthYear', $this->getBirthYear());
		$restResult->setProperty('deathYear', $this->getDeathYear());
		$restResult->setProperty('disambiguationSuffix', $this->getDisambiguationSuffix());

		$nationalitiesCount = $this->getNationalitiesCount();
		$restResult->setProperty('nationalitiesCount', $nationalitiesCount);
		if ($nationalitiesCount)
		{
			$restResult->setProperty('nationalitiesData', $this->getNationalitiesInfos());
		}
	}

	/**
	 * @return String|null
	 */
	protected function getContributorType()
	{
		$typology = $this->getDocumentManager()->getTypologyByDocument($this);
		if ($typology)
		{
			return $typology->getTitle();
		}
		return null;
	}

	/**
	 * @return array
	 */
	protected function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}

	/**
	 * @return array
	 */
	protected function getAliasesInfos()
	{
		$aliasesInfos = [];
		foreach ($this->getAliases() as $alias)
		{
			$webSite = $alias->getForWebsiteInstance();
			$aliasesInfos[] = [
				'label' => $alias->getLabel(), 'civilName' => $alias->getCivilName(), 'comment' => $alias->getComment(),
				'forWebsite' => $webSite ? $webSite->getInfos() : null
			];
		}
		return $aliasesInfos;
	}

	/**
	 * @return array
	 */
	protected function getContributionsInfos()
	{
		$query = $this->getDocumentManager()->getNewQuery('Project_Library_Contribution');
		$query->andPredicates($query->getPredicateBuilder()->eq('contributor', $this));
		$contributions = $query->getDocuments();

		$contributionsInfos = [];
		foreach ($contributions as $contribution)
		{
			/* @var $contribution \Project\Library\Documents\Contribution */
			$work = $contribution->getWork();
			if (!$work)
			{
				continue;
			}

			$workData = [
				'id' => $work->getId(),
				'model' => $work->getDocumentModelName(),
				'label' => $work->getLabel(),
				'workType' => $work->getWorkType()
			];
			$key = $workData['label'] . ' ' . $workData['id'];
			$contributionsInfos[$key]['work'] = $workData;

			foreach ($contribution->getContributionTypes() as $contributionType)
			{
				if (!isset($contributionsInfos[$contributionType->getLabel()]))
				{
					$contributionsInfos[$key]['types'][] = [
						'id' => $contributionType->getId(),
						'model' => $contributionType->getDocumentModelName(),
						'label' => $contributionType->getLabel(),
						'description' => $contributionType->getDescription()
					];
				}
			}
		}
		\ksort($contributionsInfos);
		return \array_values($contributionsInfos);
	}
}
