<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Loan
 */
class Loan extends \Compilation\Project\Library\Documents\Loan
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$possession = $this->getPossession();
		if ($possession)
		{
			$restResult->setProperty('possessionId', $possession->getId());
			$restResult->setProperty('possessionLabel', $possession->getLabel());

			if ($possession->getWork())
			{
				$restResult->setProperty('workId', $possession->getWork()->getId());
				$restResult->setProperty('workLabel', $possession->getWork()->getLabel());
			}
		}
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->getBorrowerName();
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less
		return $this;
	}

	/**
	 * @return \Project\Library\Documents\Work|null
	 */
	public function getWork()
	{
	$possession = $this->getPossession();
		return $possession ? $possession->getWork() : null;
	}

	/**
	 * @param \Project\Library\Documents\Work $work
	 * @return $this
	 */
	public function setWork($work)
	{
		// Nothing to do because Work is state less
		return $this;
	}
}
