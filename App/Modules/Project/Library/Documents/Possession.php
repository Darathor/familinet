<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Possession
 */
class Possession extends \Compilation\Project\Library\Documents\Possession
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$work = $this->getWork();
		if ($work)
		{
			$restResult->setProperty('workId', $work->getId());
			$restResult->setProperty('workLabel', $work->getLabel());
		}
		$restResult->setProperty('loanOngoingStatus', $this->getLoanOngoingStatus());
		$restResult->setProperty('volumesLabel', $this->getVolumesLabel());

		$users = \array_map(static function (\Rbs\User\Documents\User $user) { return $user->getDisplayName(); }, $this->getOwners()->toArray());
		$restResult->setProperty('label', \implode(', ', $users));

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$details = $restResult->getProperty('details');
			foreach ($details as &$detail)
			{
				$this->updateDetailRestResult($detail);
			}
			unset($detail);
			$restResult->setProperty('details', $details);
		}
	}

	/**
	 * @param array $detail
	 */
	protected function updateDetailRestResult(&$detail)
	{
		$detail['label'] = $this->getEditionVolumesLabel($detail);
		$publicationLink = $detail['publication'];
		if ($publicationLink instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$publication = $publicationLink->getDocument();
			if ($publication instanceof \Project\Library\Documents\Publication)
			{
				$publicationLink->setProperty('oneShot', $publication->getOneShot());
				$publishers = [];
				foreach ($publication->getPublishers() as $publisher)
				{
					$publishers[] = ['label' => $publisher->getLabel()];
				}
				$publicationLink->setProperty('publishers', $publishers);
				$nationalitiesArray = [];
				foreach ($publication->getNationalities() as $nationality)
				{
					$nationalityArray = [];
					$nationalityArray['label'] = $nationality->getLabel();
					$nationalityArray['code'] = $nationality->getCode();
					$nationalitiesArray[] = $nationalityArray;
				}
				$publicationLink->setProperty('nationalitiesData', $nationalitiesArray);
			}
		}
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->normalizeDescriptions();
		$this->refreshPublications();
		$this->handleOneShot();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->normalizeDescriptions();
		$this->refreshPublications();
		$this->handleOneShot();
	}

	/**
	 * Normalize details descriptions.
	 */
	protected function normalizeDescriptions()
	{
		foreach ($this->getDetails() as $detail)
		{
			if ($detail->getVolumesCount())
			{
				$detail->setDescription();
			}
			else
			{
				/** @var \Project\Library\Tools\VolumesDetailTools $tools */
				$tools = new \Project\Library\Tools\VolumesDetailTools();
				$tools->addDescription($detail->getDescription());
				$detail->setDescription($tools->getLabel());
			}
		}
	}

	/**
	 * Synchronize publications and volumes properties merging the value of all details.
	 */
	protected function refreshPublications()
	{
		$volumes = [];
		$publications = [];
		foreach ($this->getDetails() as $detail)
		{
			if ($detail->getPublication())
			{
				$publications[] = $detail->getPublication();
			}
			foreach ($detail->getVolumes() as $volume)
			{
				$volumes[] = $volume;
			}
		}

		$this->setPublications($publications);
		$this->setVolumes($volumes);
	}

	/**
	 * Handle one shot on publication.
	 */
	protected function handleOneShot()
	{
		foreach ($this->getDetails() as $detail)
		{
			$publication = $detail->getPublication();
			if ($publication && $publication->getOneShot())
			{
				$detail->setDescription('1');
				$detail->setVolumes();
			}
		}
	}

	/**
	 * @return array
	 */
	protected function getLoanOngoingStatus()
	{
		$query = $this->getDocumentManager()->getNewQuery('Project_Library_Loan');
		$query->andPredicates($query->getPredicateBuilder()->eq('possession', $this));
		$loans = $query->getDocuments();

		/** @var \Project\Library\Documents\Loan $loan */
		foreach ($loans as $loan)
		{
			$status = $loan->getStatus();
			if ($status && $status->getCode() === 'ongoing')
			{
				return [
					'label' => $status->getLabel(),
					'iconName' => $status->getIconName(),
					'textClass' => $status->getTextClass()
				];
			}
		}
		return null;
	}

	/**
	 * Calculate volumes label.
	 * @return string
	 */
	public function getVolumesLabel()
	{
		$tools = new \Project\Library\Tools\VolumesDetailTools();

		foreach ($this->getDetails() as $detail)
		{
			if ($detail->getVolumesCount() > 0)
			{
				$tools->addVolumes($detail->getVolumes());
			}
			elseif ($detail->getDescription())
			{
				$tools->addDescription($detail->getDescription());
			}
		}

		return $tools->getLabel();
	}

	/**
	 * Calculate volumes label for a publication (onRestResult).
	 * @param array $detail
	 * @return string
	 */
	protected function getEditionVolumesLabel($detail)
	{
		$description = $detail['description'] ?? null;
		if ($description !== null && $description !== '')
		{
			return $description;
		}

		$tools = new \Project\Library\Tools\VolumesDetailTools();
		/** @var \Change\Http\Rest\V1\Resources\DocumentLink $volume */
		$volumes = $detail['volumes'] ?? [];
		if ($volumes)
		{
			foreach ($detail['volumes'] as $volume)
			{
				$tools->addVolume($volume->getProperty('number'));
			}
		}
		return $tools->getLabel();
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$users = [];
		foreach ($this->getOwners() as $user)
		{
			$users[] = $user->getLabel();
		}
		return \implode(', ', $users);
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less
		return $this;
	}
}
