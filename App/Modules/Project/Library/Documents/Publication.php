<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Publication
 */
class Publication extends \Compilation\Project\Library\Documents\Publication implements \Project\Library\Indexer\LinkedDocument
{
	use \Project\Library\Indexer\LinkedDocumentTrait;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$this->attachLinksIndexEvents($eventManager);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer)
	{
		$links = [];
		foreach ($this->getLinks() as $linkDocument)
		{
			$links[] = $linkDocument->getUrl();
		}

		// Add links to works.
		foreach ($this->getWorks() as $work)
		{
			$indexer->index($work->getId(), $work->getDocumentModelName(), $links, $this->getId());
		}

		return $indexer->index($this->getId(), $this->getDocumentModelName(), $links);
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		$label = '';

		$publisher = $this->getPublisher();
		if ($publisher)
		{
			$label .= $publisher->getLabel();
		}

		$collection = $this->getCollection();
		if ($collection)
		{
			$label .= ' - ' . $collection->getLabel();
		}

		$formatLabel = $this->getFormatLabel();
		if ($formatLabel)
		{
			$label .= ' (' . $formatLabel . ')';
		}

		$name = $this->getName();
		if ($name)
		{
			if ($label)
			{
				$label .= ', ';
			}
			$label .= $name;
		}

		return $label ?: '*';
	}

	/**
	 * @return string
	 */
	protected function getFormatLabel()
	{
		$formatLabel = null;

		$format = $this->documentManager->getAttributeValues($this)->get('format');
		if ($format)
		{
			$typology = $this->documentManager->getTypologyByDocument($this);
			if ($typology)
			{
				$attribute = $typology->getAttributeByName('format');
				if ($attribute)
				{
					$fct = $attribute->getAJAXFormatter();
					if (\is_callable($fct))
					{
						$res = $fct($format, []);
						if (isset($res['common']['title']))
						{
							$formatLabel = $res['common']['title'];
						}
					}
				}
			}
		}

		return $formatLabel;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less.
		return $this;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event);
		$restResult = $event->getParam('restResult');

		$work = $this->getWork();
		if ($work)
		{
			$restResult->setProperty('workId', $work->getId());
		}

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('languagesSelection', $this->getLanguagesSelection());

			$languagesInfos = $this->getLanguagesInfos($event->getApplicationServices()->getCollectionManager());
			if ($languagesInfos)
			{
				$restResult->setProperty('languagesInfos', $languagesInfos);
			}
		}

		$nationalitiesCount = $this->getNationalitiesCount();
		$restResult->setProperty('nationalitiesCount', $nationalitiesCount);
		if ($nationalitiesCount)
		{
			$restResult->setProperty('nationalitiesData', $this->getNationalitiesInfos());
		}
	}

	/**
	 * @return array
	 */
	public function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->normalizeVolumesDetail();
		$this->normalizePublisher();
		$this->normalizeWork();
		$this->refreshLanguageLabel();
		$this->handleOneShot();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->normalizeVolumesDetail();
		$this->normalizePublisher();
		$this->normalizeWork();
		$this->refreshLanguageLabel();
		$this->handleOneShot();
	}

	/**
	 * Normalises possession details descriptions.
	 */
	protected function normalizeVolumesDetail()
	{
		/** @var \Project\Library\Tools\VolumesDetailTools $tools */
		$tools = new \Project\Library\Tools\VolumesDetailTools();
		$tools->addDescription($this->getVolumesDetail());
		$this->setVolumesDetail($tools->getLabel());
	}

	/**
	 * Synchronize the publisher property with the first element of publishers one.
	 */
	protected function normalizePublisher()
	{
		$this->setPublisher($this->getPublishers()[0]);
	}

	/**
	 * Synchronize the work property with the first element of works one.
	 */
	protected function normalizeWork()
	{
		$this->setWork($this->getWorks()[0]);
	}

	/**
	 * Refresh the label property.
	 */
	protected function refreshLanguageLabel()
	{
		foreach ($this->getLanguages() as $language)
		{
			$language->setLabel($language->getLanguage() . ' ' . $language->getLanguageFunction());
		}
	}

	/**
	 * Handle one shot.
	 */
	protected function handleOneShot()
	{
		if ($this->getOneShot())
		{
			$this->setVolumesDetail('1');
			$query = $this->documentManager->getNewQuery('Project_Library_WorkStatus');
			$query->andPredicates($query->eq('code', 'ended'));
			$status = $query->getFirstDocument();
			if ($status instanceof \Project\Library\Documents\WorkStatus)
			{
				$this->setStatus($status);
			}
		}
	}

	/**
	 *
	 * @param \Change\Collection\CollectionManager $cm
	 * @return array
	 */
	protected function getLanguagesInfos($cm)
	{
		$languages = $this->getLanguages();

		$languagesCollection = $cm->getCollection('Project_Library_Languages');
		$functionsCollection = $cm->getCollection('Project_Library_LanguageFunctions');
		if (!$languagesCollection || !$functionsCollection)
		{
			return [];
		}

		$languagesInfos = [];
		foreach ($languages as $language)
		{
			$languageLanguage = $languagesCollection->getItemByValue($language->getLanguage());
			$languageFunction = $functionsCollection->getItemByValue($language->getLanguageFunction());

			if ($languageLanguage !== null && $languageFunction !== null)
			{
				if (!isset($languagesInfos[$languageFunction->getLabel()]))
				{
					$languagesInfos[$languageFunction->getLabel()]['function'] = $languageFunction->getLabel();
				}

				$languagesInfos[$languageFunction->getLabel()]['languages'][] = $languageLanguage->getLabel();
			}
		}

		\ksort($languagesInfos);
		return \array_values($languagesInfos);
	}

	/**
	 * @return array
	 */
	protected function getLanguagesSelection()
	{
		$languages = $this->getLanguages();

		$languagesSelection = ['fr' => []];
		foreach ($languages as $language)
		{
			$languageLanguage = $language->getLanguage();
			$languageFunction = $language->getLanguageFunction();
			if ($languageLanguage && $languageFunction)
			{
				$languagesSelection[$languageLanguage][$languageFunction] = true;
			}
		}

		return $languagesSelection;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'languagesSelection')
		{
			$this->ignoredPropertiesForRestEvents['languages'];

			$collectionManager = $event->getApplicationServices()->getCollectionManager();
			$languagesCollection = $collectionManager->getCollection('Project_Library_Languages');
			if (!$languagesCollection)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Project_Library_Languages collection missing.');
				return false;
			}
			$functionsCollection = $collectionManager->getCollection('Project_Library_LanguageFunctions');
			if (!$functionsCollection)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Project_Library_LanguageFunctions collection missing.');
				return false;
			}

			$languages = $this->getLanguages();
			$languages->removeAll();
			if ($value  && \is_array($value))
			{
				foreach ($value as $lang => $functions)
				{
					if (!$languagesCollection->getItemByValue($lang))
					{
						$event->getApplication()->getLogging()->warn(__METHOD__, 'Unknown language code:', $lang);
						continue;
					}

					foreach (\array_keys(\array_filter($functions)) as $languageFunction)
					{
						if (!$functionsCollection->getItemByValue($languageFunction))
						{
							$event->getApplication()->getLogging()->warn(__METHOD__, 'Unknown language function:', $languageFunction);
							continue;
						}
						$languages->add($this->newPublicationLanguage()->setLanguage($lang)->setLanguageFunction($languageFunction));
					}
				}
			}
			return true;
		}
		return parent::processRestData($name, $value, $event);
	}
}
