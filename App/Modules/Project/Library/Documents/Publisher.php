<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Publisher
 */
class Publisher extends \Compilation\Project\Library\Documents\Publisher
	implements \Project\Library\Indexer\AliasedDocument, \Project\Library\Indexer\LinkedDocument
{
	use \Project\Library\Indexer\AliasedDocumentTrait;
	use \Project\Library\Indexer\LinkedDocumentTrait;

	public const ALIASES_INDEX_TABLE = 'project_library_dat_publisher_aliases';
	public const ALIASES_INDEX_PREFIX = 'publisher_';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$this->attachAliasesIndexEvents($eventManager);
		$this->attachLinksIndexEvents($eventManager);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer)
	{
		$count = 0;
		$links = [];
		foreach ($this->getLinks() as $linkDocument)
		{
			$links[] = $linkDocument->getUrl();
		}
		if ($links)
		{
			$count = $indexer->index($this->getId(), $this->getDocumentModelName(), $links);
		}

		$query = $this->documentManager->getNewQuery('Project_Library_Collection');
		$query->andPredicates($query->eq('publisher', $this));
		/** @var \Project\Library\Documents\Collection $collection */
		foreach ($query->getDocuments() as $collection)
		{
			$links = [];
			foreach ($collection->getLinks() as $linkDocument)
			{
				$links[] = $linkDocument->getUrl();
			}
			$count += $indexer->index($this->getId(), $this->getDocumentModelName(), $links, $collection->getId());
		}

		return $count;
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager)
	{
		$label = $this->getLabel();
		$aliases = [$label];
		foreach ($this->getAliases() as $aliasDocument)
		{
			$aliases[] = $aliasDocument->getLabel();
		}

		return $indexer->index($this->getId(), $label, $aliases);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$foundAlias = $this->getFoundAlias();
			if ($foundAlias)
			{
				$restResult->setProperty('foundAlias', $foundAlias);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('aliasesData', $this->getAliasesInfos());
		}

		$nationalitiesCount = $this->getNationalitiesCount();
		$restResult->setProperty('nationalitiesCount', $nationalitiesCount);
		if ($nationalitiesCount)
		{
			$restResult->setProperty('nationalitiesData', $this->getNationalitiesInfos());
		}
	}

	/**
	 * @return array
	 */
	protected function getAliasesInfos()
	{
		$aliasesInfos = [];
		foreach ($this->getAliases() as $alias)
		{
			$aliasesInfos[] = [
				'label' => $alias->getLabel(), 'comment' => $alias->getComment()
			];
		}
		return $aliasesInfos;
	}

	/**
	 * @return array
	 */
	protected function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}
}
