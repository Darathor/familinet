<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\Reading
 */
class Reading extends \Compilation\Project\Library\Documents\Reading
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$restResult->setProperty('volumesLabel', $this->getVolumesLabel());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultRouteParamsRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultRouteParamsRestResult($event);
		$restResult = $event->getParam('restResult');

		$reader = $this->getReader();
		if ($reader)
		{
			$restResult->setProperty('userId', $reader->getId());
		}
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->normalizeNote();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->normalizeNote();
	}

	protected function normalizeNote()
	{
		if ($this->getRating())
		{
			$this->setRating(\min(100, \max(0, $this->getRating())));
		}
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		if ($this->getWork())
		{
			return $this->getWork()->getLabel();
		}
		return '';
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		// Nothing to do because Label is state less
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getVolumesLabel()
	{
		$detail = $this->getVolumesDetail();
		if ($detail !== null && $detail !== '')
		{
			return $detail;
		}

		$tools = new \Project\Library\Tools\VolumesDetailTools();
		if ($this->getVolumesCount())
		{
			$tools->addVolumes($this->getVolumes());
		}
		return $tools->getLabel();
	}
}
