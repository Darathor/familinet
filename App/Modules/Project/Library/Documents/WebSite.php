<?php
/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WebSite
 */
class WebSite extends \Compilation\Project\Library\Documents\WebSite
{
	/**
	 * @return array
	 */
	public function getInfos()
	{
		$icon = $this->getIcon();
		return [
			'label' => $this->getLabel(),
			'url' => 'https://' . $this->getDomain(),
			'lang' => $this->getLanguage(),
			'icon' => $icon ? $icon->getPublicURL(16, 16) : null
		];
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$image = $this->getIcon();
			if ($image)
			{
				$restResult->setProperty('adminthumbnail', $image->getPublicURL(128, 128));
			}
		}
	}
}
