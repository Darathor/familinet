<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WorkAlias
 */
class WorkAlias extends \Compilation\Project\Library\Documents\WorkAlias
{
	/**
	 * @return array
	 */
	public function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}
}
