<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WorkGroup
 */
class WorkGroup extends \Compilation\Project\Library\Documents\WorkGroup
	implements \Project\Library\Indexer\AliasedDocument, \Project\Library\Indexer\LinkedDocument
{
	use \Project\Library\Indexer\AliasedDocumentTrait;
	use \Project\Library\Indexer\LinkedDocumentTrait;

	public const ALIASES_INDEX_TABLE = 'project_library_dat_work_group_aliases';
	public const ALIASES_INDEX_PREFIX = 'group_';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$this->attachAliasesIndexEvents($eventManager);
		$this->attachLinksIndexEvents($eventManager);
	}

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager)
	{
		$label = $this->getLabel();
		$aliases = [$label];
		foreach ($this->getAliases() as $aliasDocument)
		{
			$aliases[] = $aliasDocument->getLabel();
		}

		return $indexer->index($this->getId(), $label, $aliases);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer)
	{
		$links = [];
		foreach ($this->getLinks() as $linkDocument)
		{
			$links[] = $linkDocument->getUrl();
		}

		return $indexer->index($this->getId(), $this->getDocumentModelName(), $links);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		$restResult->setProperty('groupType', $this->getGroupType());
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$foundAlias = $this->getFoundAlias();
			if ($foundAlias)
			{
				$restResult->setProperty('foundAlias', $foundAlias);
			}
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$restResult->setProperty('aliasesData', $this->getAliasesInfos());
		}
	}

	/**
	 * @return String|null
	 */
	public function getGroupType()
	{
		$typology = $this->getDocumentManager()->getTypologyByDocument($this);
		if ($typology)
		{
			return $typology->getTitle();
		}
		return null;
	}

	/**
	 * @return array
	 */
	protected function getAliasesInfos()
	{
		$aliasesInfos = [];
		foreach ($this->getAliases() as $alias)
		{
			$aliasesInfos[] = [
				'label' => $alias->getLabel(), 'nationalitiesData' => $alias->getNationalitiesInfos(), 'comment' => $alias->getComment()
			];
		}
		return $aliasesInfos;
	}
}
