<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WorkGroupAlias
 */
class WorkGroupAlias extends \Compilation\Project\Library\Documents\WorkGroupAlias
{
	/**
	 * @return array
	 */
	public function getNationalitiesInfos()
	{
		$nationalitiesInfos = [];
		foreach ($this->getNationalities() as $nationality)
		{
			$nationalitiesInfos[] = ['code' => $nationality->getCode(), 'label' => $nationality->getLabel()];
		}
		return $nationalitiesInfos;
	}
}
