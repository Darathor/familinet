<?php
namespace Project\Library\Documents;

/**
 * @name \Project\Library\Documents\WorkStatus
 */
class WorkStatus extends \Compilation\Project\Library\Documents\WorkStatus
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');

		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$restResult->setProperty('iconName', $this->getIconName());
			$restResult->setProperty('textClass', $this->getTextClass());
			$restResult->setProperty('code', $this->getCode());
		}
	}
}
