<?php
/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Events\Http\Rest;

/**
 * @name \Project\Library\Events\Http\Rest\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_REQUEST, static function (\Change\Events\Event $event) {
			$controller = $event->getTarget();
			if ($controller instanceof \Change\Http\Rest\V1\Controller)
			{
				$resolver = $controller->getActionResolver();
				if ($resolver instanceof \Change\Http\Rest\V1\Resolver)
				{
					$resolver->addResolverClasses('projectLibrary', \Project\Library\Http\Rest\V1\Actions\ActionsResolver::class);
				}
			}
		});
	}
}