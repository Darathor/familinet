<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\GetDocumentsByExternalLink
 */
class GetDocumentsByExternalLink
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$link = $request->getQuery('link');
		if (!$link)
		{
			throw new \RuntimeException('No link!', 999999);
		}
		if (!\Change\Stdlib\StringUtils::beginsWith($link, 'http://') && !\Change\Stdlib\StringUtils::beginsWith($link, 'https://'))
		{
			throw new \RuntimeException('Only HTTP and HTTPS protocols are allowed!', 999999);
		}
		$ignoreInherited = $request->getQuery('ignoreInherited') === 'true';

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$dbProvider = $event->getApplicationServices()->getDbProvider();

		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->alias($fb->column('document_id'), 'id'), $fb->alias($fb->column('document_model'), 'model'));
		$qb->from($fb->table(\Project\Library\Indexer\LinksIndexer::TABLE_NAME));
		if ($ignoreInherited)
		{
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->column('link'), $fb->parameter('link')),
					$fb->eq($fb->column('source_id'), 0)
				)
			);
		}
		else
		{
			$qb->where($fb->eq($fb->column('link'), $fb->parameter('link')));
		}
		$qb->orderAsc($fb->column('document_id'));
		$sq = $qb->query();
		$sq->bindParameter('link', $link);
		$collection = new \Change\Documents\DocumentCollection($documentManager, $sq->getResults());
		$collection->preLoad();
		$documents = $collection->toArray();

		$urlManager = $event->getUrlManager();

		$result = new \Change\Http\Rest\V1\CollectionResult();
		$result->setLimit(0);
		$result->setCount(\count($documents));
		$result->setSort('id');
		$result->setDesc(false);

		$selfLink = new \Change\Http\Rest\V1\Link($urlManager, $request->getPath());
		$result->addLink($selfLink);

		$extraColumn = $request->getPost('column', $request->getQuery('column', []));
		foreach ($documents as $document)
		{
			$result->addResource(new \Change\Http\Rest\V1\Resources\DocumentLink($urlManager, $document,
				\Change\Http\Rest\V1\Resources\DocumentLink::MODE_LIST, $extraColumn));
		}

		$event->setResult($result);
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}