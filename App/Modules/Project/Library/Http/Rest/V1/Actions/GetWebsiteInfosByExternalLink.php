<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\GetWebsiteInfosByExternalLink
 */
class GetWebsiteInfosByExternalLink
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$url = $request->getQuery('link');
		if (!$url)
		{
			throw new \RuntimeException('No URL!', 999999);
		}

		$matches = [];
		if (!\preg_match('#^[^/]+://([^/]+)/.*$#', $url, $matches) || !$matches[1])
		{
			throw new \RuntimeException('Invalid URL: ' . $url, 999999);
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();
		$resolver = new \Project\Library\WebSiteParsers\Misc\Resolver($documentManager, $documentCodeManager);
		$infos = $resolver->resolveWebsiteInfos($matches[1]);
		if ($infos)
		{
			$parserFactory = new \Project\Library\WebSiteParsers\Misc\ParserFactory($documentManager, $documentCodeManager);
			$parser = $parserFactory->getByUrl($url);
			if ($parser)
			{
				$normalizedUrl = $parser->getUrl();
				if ($normalizedUrl !== $url)
				{
					$infos['normalizedUrl'] = $normalizedUrl;
				}
			}

			return $event->setResult($this->generateResult($infos));
		}

		throw new \RuntimeException('No matching web site definition!', 999999);
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}