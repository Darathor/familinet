<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\MergeDocuments
 */
class MergeDocuments
{
	/**
	 * @var \Change\Transaction\TransactionManager
	 */
	protected $transactionManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$applicationServices = $event->getApplicationServices();
		$this->documentManager = $applicationServices->getDocumentManager();

		$toDelete = $this->documentManager->getDocumentInstance($request->getPost('toDeleteId'));
		if (!$toDelete)
		{
			throw new \RuntimeException('Invalid toDeleteId', 999999);
		}
		$target = $this->documentManager->getDocumentInstance($request->getPost('targetId'));
		if (!$target)
		{
			throw new \RuntimeException('Invalid targetId', 999999);
		}

		if ($toDelete->getId() === $target->getId())
		{
			throw new \RuntimeException('toDelete and target are the same', 999999);
		}
		if (\get_class($toDelete) !== \get_class($target))
		{
			throw new \RuntimeException('Incompatible toDelete and target types', 999999);
		}

		$this->transactionManager = $applicationServices->getTransactionManager();

		if ($toDelete instanceof \Project\Library\Documents\Contributor && $target instanceof \Project\Library\Documents\Contributor)
		{
			$data = $this->mergeContributor($toDelete, $target);
			$event->setResult($this->generateResult(['message' => 'Contributor merged.', 'data' => $data]));
		}
		if ($toDelete instanceof \Project\Library\Documents\WorkGroup && $target instanceof \Project\Library\Documents\WorkGroup)
		{
			$data = $this->mergeWorkGroup($toDelete, $target);
			$event->setResult($this->generateResult(['message' => 'Work group merged.', 'data' => $data]));
		}
		if ($toDelete instanceof \Project\Library\Documents\Genre && $target instanceof \Project\Library\Documents\Genre)
		{
			$data = $this->mergeGenre($toDelete, $target);
			$event->setResult($this->generateResult(['message' => 'Genre merged.', 'data' => $data]));
		}
		elseif ($toDelete instanceof \Project\Library\Documents\Publisher && $target instanceof \Project\Library\Documents\Publisher)
		{
			$targetCollection = $this->documentManager->getDocumentInstance($request->getPost('targetCollectionId'));
			if ($targetCollection && !($targetCollection instanceof \Project\Library\Documents\Collection))
			{
				throw new \RuntimeException('Invalid targetCollectionId', 999999);
			}
			$data = $this->mergePublisher($toDelete, $target, $targetCollection);
			$event->setResult($this->generateResult(['message' => 'Publisher merged.', 'data' => $data]));
		}
		else
		{
			throw new \RuntimeException('Invalid toDelete type', 999999);
		}
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}

	/**
	 * @param \Project\Library\Documents\Contributor $toDelete
	 * @param \Project\Library\Documents\Contributor $target
	 * @return array
	 */
	protected function mergeContributor(\Project\Library\Documents\Contributor $toDelete, \Project\Library\Documents\Contributor $target)
	{
		$data = [
			'contributions' => 0
		];
		try
		{
			$this->transactionManager->begin();

			// Reassign contributions.
			$query = $this->documentManager->getNewQuery('Project_Library_Contribution');
			$query->andPredicates($query->eq('contributor', $toDelete));
			/** @var \Project\Library\Documents\Contribution $contribution */
			foreach ($query->getDocuments() as $contribution)
			{
				$contribution->setContributor($target);
				$contribution->save();
				$data['contributions']++;
			}

			// Delete contributor.
			$toDelete->delete();

			$this->transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $this->transactionManager->rollBack($e);
		}
		return $data;
	}

	/**
	 * @param \Project\Library\Documents\WorkGroup $toDelete
	 * @param \Project\Library\Documents\WorkGroup $target
	 * @return array
	 */
	protected function mergeWorkGroup(\Project\Library\Documents\WorkGroup $toDelete, \Project\Library\Documents\WorkGroup $target)
	{
		$data = [
			'works' => 0
		];
		try
		{
			$this->transactionManager->begin();

			// Reassign works.
			$query = $this->documentManager->getNewQuery('Project_Library_Work');
			$query->andPredicates($query->eq('workGroups', $toDelete));
			/** @var \Project\Library\Documents\Work $work */
			foreach ($query->getDocuments() as $work)
			{
				$work->getWorkGroups()->remove($toDelete)->add($target);
				$work->save();
				$data['works']++;
			}

			// Delete work group.
			$toDelete->delete();

			$this->transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $this->transactionManager->rollBack($e);
		}
		return $data;
	}

	/**
	 * @param \Project\Library\Documents\Genre $toDelete
	 * @param \Project\Library\Documents\Genre $target
	 * @return array
	 */
	protected function mergeGenre(\Project\Library\Documents\Genre $toDelete, \Project\Library\Documents\Genre $target)
	{
		$data = [
			'works' => 0
		];
		try
		{
			$this->transactionManager->begin();

			// Reassign works.
			$query = $this->documentManager->getNewQuery('Project_Library_Work');
			$query->andPredicates($query->eq('genres', $toDelete));
			/** @var \Project\Library\Documents\Work $work */
			foreach ($query->getDocuments() as $work)
			{
				$work->getGenres()->remove($toDelete)->add($target);
				$work->save();
				$data['works']++;
			}

			// Delete genre.
			$toDelete->delete();

			$this->transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $this->transactionManager->rollBack($e);
		}
		return $data;
	}

	/**
	 * @param \Project\Library\Documents\Publisher $toDelete
	 * @param \Project\Library\Documents\Publisher $target
	 * @param \Project\Library\Documents\Collection|null $targetCollection
	 * @return array
	 */
	protected function mergePublisher(\Project\Library\Documents\Publisher $toDelete, \Project\Library\Documents\Publisher $target,
		?\Project\Library\Documents\Collection $targetCollection)
	{
		$data = [
			'publications' => 0,
			'collections' => 0
		];
		try
		{
			$this->transactionManager->begin();

			// Reassign publications.
			$query = $this->documentManager->getNewQuery('Project_Library_Publication');
			$query->andPredicates($query->eq('publishers', $toDelete));
			/** @var \Project\Library\Documents\Publication $publication */
			foreach ($query->getDocuments() as $publication)
			{
				$publication->getPublishers()->remove($toDelete)->add($target);
				if ($targetCollection && !$publication->getCollection())
				{
					$publication->setCollection($targetCollection);
				}
				$publication->save();
				$data['publications']++;
			}

			// Reassign collections.
			$query = $this->documentManager->getNewQuery('Project_Library_Collection');
			$query->andPredicates($query->eq('publisher', $toDelete));
			/** @var \Project\Library\Documents\Collection $collection */
			foreach ($query->getDocuments() as $collection)
			{
				$collection->setPublisher($target);
				$collection->save();
				$data['collections']++;
			}

			// Delete publisher.
			$toDelete->delete();

			$this->transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $this->transactionManager->rollBack($e);
		}
		return $data;
	}
}