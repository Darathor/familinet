<?php
/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Http\Rest\V1\Actions;

/**
 * @name \Project\Library\Http\Rest\V1\Actions\ParseExternalPage
 */
class ParseExternalPage
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		$url = $request->getQuery('url');
		if (!$url)
		{
			throw new \RuntimeException('No URL!', 999999);
		}

		$type = $request->getQuery('importType');
		if (!$type)
		{
			throw new \RuntimeException('No importType!', 999999);
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$documentCodeManager = $applicationServices->getDocumentCodeManager();

		$parserFactory = new \Project\Library\WebSiteParsers\Misc\ParserFactory($documentManager, $documentCodeManager);
		$parser = $parserFactory->getByUrl($url, $type);
		if (!$parser)
		{
			throw new \RuntimeException('No matching web site definition!', 999999);
		}
		$event->setResult($this->generateResult($parser->parseDataAndNormalize()));
	}

	/**
	 * @param array $data
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($data)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$result->setArray($data);
		return $result;
	}
}