<?php
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\AliasedDocument
 */
interface AliasedDocument
{
	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Project\Library\Indexer\AliasesIndexer
	 */
	public static function getAliasesIndexer(\Change\Db\DbProvider $dbProvider);

	/**
	 * @return int
	 */
	public function getId();

	/**
	 * @return string
	 */
	public function getLabel();

	/**
	 * @return string
	 */
	public function getFoundAlias();

	/**
	 * @param $alias
	 * @return $this
	 */
	public function setFoundAlias($alias);

	/**
	 * @param \Project\Library\Indexer\AliasesIndexer $indexer
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function createAliasesIndex(\Project\Library\Indexer\AliasesIndexer $indexer, \Project\Familinet\AttributeManager $attributeManager);

	/**
	 * @param \Project\Familinet\AttributeManager $attributeManager
	 * @return int
	 */
	public function reIndexAliases(\Project\Familinet\AttributeManager $attributeManager);
}