<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\AliasesIndexer
 */
class AliasesIndexer
{
	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var string
	 */
	protected $tableName;

	/**
	 * @var string
	 */
	protected $fieldPrefix;

	/**
	 * AliasesIndexer constructor.
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param string $tableName
	 * @param string $fieldPrefix
	 */
	public function __construct(\Change\Db\DbProvider $dbProvider, string $tableName, string $fieldPrefix)
	{
		$this->dbProvider = $dbProvider;
		$this->tableName = $tableName;
		$this->fieldPrefix = $fieldPrefix;
	}

	/**
	 * @param int $documentId
	 * @return int
	 */
	public function delete(int $documentId)
	{
		$qb = $this->dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table($this->tableName));
		$qb->where($fb->eq($fb->column($this->fieldPrefix . 'id'), $fb->integerParameter('id')));
		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $documentId);
		return $dq->execute();
	}

	/**
	 * @return int
	 */
	public function deleteAll()
	{
		$qb = $this->dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table($this->tableName));
		return $qb->deleteQuery()->execute();
	}

	/**
	 * @param int $documentId
	 * @param string $label
	 * @param string[] $aliases
	 * @param string[] $exactMatchAliases
	 * @return int
	 */
	public function index(int $documentId, string $label, array $aliases, array $exactMatchAliases = [])
	{
		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->alias($fb->count($fb->allColumns()), 'count'))->from($fb->table($this->tableName))->where(
			$fb->logicAnd(
				$fb->eq($fb->column($this->fieldPrefix . 'id'), $fb->integerParameter('id')),
				$fb->eq($fb->column($this->fieldPrefix . 'alias'), $fb->parameter('alias'))
			)
		);
		$sq = $qb->query();
		$sq->bindParameter('id', $documentId);
		$rc = $sq->getRowsConverter()->addIntCol('count')->singleColumn('count');

		$sb = $this->dbProvider->getNewStatementBuilder();
		$fb = $sb->getFragmentBuilder();
		$sb->insert($fb->table($this->tableName),
			$fb->column($this->fieldPrefix . 'id'), $fb->column($this->fieldPrefix . 'label'),
			$fb->column($this->fieldPrefix . 'alias'), $fb->column($this->fieldPrefix . 'alias_title'),
			$fb->column('priority'), $fb->column('exact_match')
		);
		$sb->addValues(
			$fb->integerParameter('id'), $fb->parameter('label'),
			$fb->parameter('alias'), $fb->parameter('title'),
			$fb->integerParameter('priority'), $fb->booleanParameter('exactMatch')
		);
		$is = $sb->insertQuery();
		$is->bindParameter('id', $documentId);
		$is->bindParameter('label', $label);

		$inserted = 0;
		if ($aliases)
		{
			$aliases = \array_filter($aliases);
			foreach (\array_unique($aliases) as $alias)
			{
				$cleanAlias = self::cleanUpAlias($alias);
				if ($cleanAlias && !$sq->bindParameter('alias', $cleanAlias)->getFirstResult($rc))
				{
					$is->bindParameter('alias', $cleanAlias);
					$is->bindParameter('title', $alias);
					$is->bindParameter('priority', $alias === $label ? 1 : 0);
					$is->bindParameter('exactMatch', false);
					$is->execute();
					$inserted++;
				}
			}
		}

		if ($exactMatchAliases)
		{
			$exactMatchAliases = \array_map(['self', 'cleanUpExactMatchAlias'], \array_filter($exactMatchAliases));
			foreach (\array_unique($exactMatchAliases) as $alias)
			{
				if ($alias && !$sq->bindParameter('alias', $alias)->getFirstResult($rc))
				{
					$is->bindParameter('alias', $alias);
					$is->bindParameter('title', $alias);
					$is->bindParameter('priority', -1);
					$is->bindParameter('exactMatch', true);
					$is->execute();
					$inserted++;
				}
			}
		}

		return $inserted;
	}

	/**
	 * @param string $alias
	 * @return string
	 */
	public static function cleanUpAlias(string $alias)
	{
		return \preg_replace('/[\s\.\-_:!?;]+/', ' ', \Change\Stdlib\StringUtils::stripAccents(\Change\Stdlib\StringUtils::toLower($alias)));
	}

	/**
	 * @param string $alias
	 * @return string
	 */
	public static function cleanUpExactMatchAlias(string $alias)
	{
		return \str_replace([' ', '.', '-'], '', $alias);
	}
}