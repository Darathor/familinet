<?php
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\LinkDocument
 */
interface LinkDocument
{
	/**
	 * @return string
	 */
	public function getUrl();

	/**
	 * @param string $url
	 * @return $this
	 */
	public function setUrl($url);
}