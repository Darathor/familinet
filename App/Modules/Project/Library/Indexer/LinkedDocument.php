<?php
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\LinkedDocument
 */
interface LinkedDocument
{
	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Project\Library\Indexer\LinksIndexer
	 */
	public static function getLinksIndexer(\Change\Db\DbProvider $dbProvider);

	/**
	 * @return int
	 */
	public function getId();

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer);

	/**
	 * @return int
	 */
	public function reIndexLinks();

	/**
	 * @return \Change\Documents\InlineArrayProperty|\Project\Library\Indexer\LinkDocument[]
	 */
	public function getLinks();
}