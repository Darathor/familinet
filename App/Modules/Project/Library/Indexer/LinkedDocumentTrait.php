<?php
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\LinkedDocumentTrait
 */
trait LinkedDocumentTrait
{
	/**
	 * @var \Project\Library\Indexer\LinksIndexer
	 */
	protected static $linksIndexer;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Project\Library\Indexer\LinksIndexer
	 */
	public static function getLinksIndexer(\Change\Db\DbProvider $dbProvider)
	{
		if (!self::$linksIndexer)
		{
			self::$linksIndexer = new \Project\Library\Indexer\LinksIndexer($dbProvider);
		}
		return self::$linksIndexer;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachLinksIndexEvents(\Zend\EventManager\EventManagerInterface $eventManager)
	{
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function () { $this->onCreateLinksIndex(); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function () { $this->onDeleteLinksIndex(); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function () { $this->onCreateLinksIndex(); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, function () { $this->onDeleteLinksIndex(); }, 10);
	}

	protected function onCreateLinksIndex()
	{
		$this->createLinksIndex(self::getLinksIndexer($this->dbProvider));
	}

	protected function onDeleteLinksIndex()
	{
		self::getLinksIndexer($this->dbProvider)->delete($this->getId());
	}

	/**
	 * @return int
	 */
	public function reIndexLinks()
	{
		$indexer = self::getLinksIndexer($this->dbProvider);
		$indexer->delete($this->getId());
		return $this->createLinksIndex($indexer);
	}

	/**
	 * @param \Project\Library\Indexer\LinksIndexer $indexer
	 * @return int
	 */
	abstract public function createLinksIndex(\Project\Library\Indexer\LinksIndexer $indexer);
}