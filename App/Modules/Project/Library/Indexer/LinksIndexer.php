<?php
/**
 * Copyright (C) 2018 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Indexer;

/**
 * @name \Project\Library\Indexer\LinksIndexer
 */
class LinksIndexer
{
	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var string
	 */
	public const TABLE_NAME = 'project_library_dat_external_links';

	/**
	 * LinksIndexer constructor.
	 * @param \Change\Db\DbProvider $dbProvider
	 */
	public function __construct(\Change\Db\DbProvider $dbProvider)
	{
		$this->dbProvider = $dbProvider;
	}

	/**
	 * @param int $documentId
	 * @return int
	 */
	public function delete(int $documentId)
	{
		$qb = $this->dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table(self::TABLE_NAME));
		$qb->where($fb->logicOr(
			$fb->eq($fb->column('document_id'), $fb->integerParameter('id')),
			$fb->eq($fb->column('source_id'), $fb->integerParameter('source'))
		));
		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $documentId);
		$dq->bindParameter('source', $documentId);
		return $dq->execute();
	}

	/**
	 * @return int
	 */
	public function deleteAll()
	{
		$qb = $this->dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table(self::TABLE_NAME));
		return $qb->deleteQuery()->execute();
	}

	/**
	 * @param int $documentId
	 * @param string $modelName
	 * @param string[] $links
	 * @param int $sourceId
	 * @return int
	 */
	public function index(int $documentId, string $modelName, array $links, int $sourceId = 0)
	{
		if ($links)
		{
			$links = \array_filter($links);
		}
		if (!$links)
		{
			return 0;
		}

		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->alias($fb->count($fb->allColumns()), 'count'))->from($fb->table(self::TABLE_NAME))->where(
			$fb->logicAnd(
				$fb->eq($fb->column('document_id'), $fb->integerParameter('id')),
				$fb->eq($fb->column('link'), $fb->parameter('link'))
			)
		);
		$sq = $qb->query();
		$sq->bindParameter('id', $documentId);
		$rc = $sq->getRowsConverter()->addIntCol('count')->singleColumn('count');

		$links = \array_filter($links);
		$sb = $this->dbProvider->getNewStatementBuilder();
		$fb = $sb->getFragmentBuilder();
		$sb->insert($fb->table(self::TABLE_NAME),
			$fb->column('document_id'), $fb->column('document_model'), $fb->column('link'), $fb->column('source_id')
		);
		$sb->addValues(
			$fb->integerParameter('id'), $fb->parameter('model'), $fb->parameter('link'), $fb->integerParameter('sourceId')
		);
		$is = $sb->insertQuery();
		$is->bindParameter('id', $documentId);
		$is->bindParameter('model', $modelName);
		$is->bindParameter('sourceId', $sourceId);

		$inserted = 0;
		foreach (\array_unique($links) as $link)
		{
			if (!$sq->bindParameter('link', $link)->getFirstResult($rc))
			{
				$is->bindParameter('link', $link);
				$is->execute();
				$inserted++;
			}
		}
		return $inserted;
	}
}