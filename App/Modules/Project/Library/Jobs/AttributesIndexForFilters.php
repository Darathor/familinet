<?php
/**
 * Copyright (C) 2018 Datathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Jobs;

/**
 * @name \Project\Library\Jobs\AttributesIndexForFilters
 */
class AttributesIndexForFilters
{
	/**
	 * @param \Change\Job\Event $event
	 */
	public function indexAliases(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();

		$transactionManager = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		/** @var int[] $documentIds */
		$documentIds = $job->getArgument('toIndex');
		try
		{
			$transactionManager->begin();

			foreach ($documentIds as $documentId)
			{
				$document = $documentManager->getDocumentInstance($documentId);
				$work = null;
				if ($document instanceof \Project\Library\Documents\Work)
				{
					$work = $document;
				}
				elseif ($document instanceof \Project\Library\Documents\Publication)
				{
					$work = $document->getWork();
				}
				elseif ($document instanceof \Project\Library\Documents\PublicationVolume)
				{
					$publication = $document->getPublication();
					if ($publication)
					{
						$work = $publication->getWork();
					}
				}

				if ($work)
				{
					$services = $event->getServices('Project_FamilinetServices');
					if (!($services instanceof \Project\Familinet\FamilinetServices))
					{
						throw new \RuntimeException('FamilinetServices not set', 999999);
					}
					$attributeManager = $services->getAttributeManager();
					$work->reIndexAliases($attributeManager);
				}
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}