<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Jobs;

/**
 * @name \Project\Library\Jobs\DocumentCleanUp
 */
class DocumentCleanUp
{
	/**
	 * @param \Change\Job\Event $event
	 */
	public function cleanUp(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$documentId = $job->getArgument('id');
		$modelName = $job->getArgument('model');
		if (!\is_numeric($documentId) || !\is_string($modelName))
		{
			$event->failed('Invalid Arguments ' . $documentId . ', ' . $modelName);
			return;
		}

		switch ($modelName)
		{
			case 'Project_Library_Contributor':
				$this->cleanUpContributor($event, $documentId);
				break;
			case 'Project_Library_Possession':
				$this->cleanUpPossession($event, $documentId);
				break;
			case 'Project_Library_Publication':
				$this->cleanUpPublication($event, $documentId);
				break;
			case 'Rbs_User_User':
				// TODO: Possession à supprimer s'il n'y a plus de owners
				break;
			case 'Project_Library_Work':
				$this->cleanUpWork($event, $documentId);
				break;
		}

		$event->success();
	}
	
	/**
	 * @param \Change\Job\Event $event
	 * @param int $documentId
	 * @throws \Exception
	 */
	protected function cleanUpContributor(\Change\Job\Event $event, int $documentId)
	{
		$applicationServices = $event->getApplicationServices();

		// Suppression des contributions
		
		$queryContribution = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Contribution');
		$queryContribution->andPredicates(
			$queryContribution->eq('contributor', $documentId)
		);

		/** @var \Project\Library\Documents\Contribution $document */
		foreach ($queryContribution->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}
	}
	
	/**
	 * @param \Change\Job\Event $event
	 * @param int $documentId
	 * @throws \Exception
	 */
	protected function cleanUpPossession(\Change\Job\Event $event, int $documentId)
	{
		$applicationServices = $event->getApplicationServices();

		// Suppression des contributions
		
		$queryLoan = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Loan');
		$queryLoan->andPredicates(
			$queryLoan->eq('possession', $documentId)
		);

		/** @var \Project\Library\Documents\Loan $document */
		foreach ($queryLoan->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Job\Event $event
	 * @param int $documentId
	 * @throws \Exception
	 */
	protected function cleanUpPublication(\Change\Job\Event $event, int $documentId)
	{
		$applicationServices = $event->getApplicationServices();

		// Suppression des volumes
		
		$queryPublicationVolume = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_PublicationVolume');
		$queryPublicationVolume->andPredicates(
			$queryPublicationVolume->eq('publication', $documentId)
		);

		/** @var \Project\Library\Documents\PublicationVolume $document */
		foreach ($queryPublicationVolume->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Job\Event $event
	 * @param int $documentId
	 * @throws \Exception
	 */
	protected function cleanUpWork(\Change\Job\Event $event, int $documentId)
	{
		$applicationServices = $event->getApplicationServices();

		// Suppression des contributions
		
		$queryContribution = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Contribution');
		$queryContribution->andPredicates(
			$queryContribution->eq('work', $documentId)
		);

		/** @var \Project\Library\Documents\Contribution $document */
		foreach ($queryContribution->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}

		// Suppression des éditions

		$queryPublication = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Publication');
		$queryPublication->andPredicates(
			$queryPublication->eq('works', $documentId)
		);

		/** @var \Project\Library\Documents\Publication $document */
		foreach ($queryPublication->getDocuments() as $document)
		{
			// Delete only if there is no other work for this publication.
			foreach ($document->getWorksIds() as $workId)
			{
				if ($workId !== $documentId)
				{
					continue 2;
				}
			}

			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}

		// Suppression des possessions

		$queryPossession = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Possession');
		$queryPossession->andPredicates(
			$queryPossession->eq('work', $documentId)
		);

		/** @var \Project\Library\Documents\Possession $document */
		foreach ($queryPossession->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}

		// Suppression des lecture

		$queryReading = $applicationServices->getDocumentManager()->getNewQuery('Project_Library_Reading');
		$queryReading->andPredicates(
			$queryPossession->eq('work', $documentId)
		);

		/** @var \Project\Library\Documents\Reading $document */
		foreach ($queryReading->getDocuments() as $document)
		{
			try
			{
				$applicationServices->getTransactionManager()->begin();
				$document->delete();
				$applicationServices->getTransactionManager()->commit();
			}
			catch (\Exception $e)
			{
				throw $applicationServices->getTransactionManager()->rollBack($e);
			}
		}
	}
}