<?php
/**
 * Copyright (C) 2014 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Jobs;

/**
 * @name \Project\Library\Jobs\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('process_Change_Document_CleanUp',
			static function (\Change\Job\Event $event) { (new \Project\Library\Jobs\DocumentCleanUp())->cleanUp($event); }, 10);

		$this->listeners[] = $events->attach('process_Rbs_Generic_Attributes_IndexForFilters',
			static function (\Change\Job\Event $event) { (new \Project\Library\Jobs\AttributesIndexForFilters())->indexAliases($event); }, 10);
	}
}