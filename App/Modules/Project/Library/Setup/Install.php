<?php
namespace Project\Library\Setup;

/**
 * @name \Project\Library\Setup\Install
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		parent::executeDbSchema($plugin, $schemaManager);
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/JobManager/Project_Library', \Project\Library\Jobs\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/CollectionManager/Project_Library', \Project\Library\Collection\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Commands/Project_Library', \Project\Library\Commands\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/ModelManager/Project_Library', \Project\Library\Events\ModelManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Documents/Project_Library', \Project\Library\Events\Documents\SharedListeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/Rest/Project_Library', \Project\Library\Events\Http\Rest\Listeners::class);
		$configuration->addPersistentEntry('Rbs/Admin/Events/AdminManager/Project_Library', \Project\Library\Admin\Listeners::class);
		$configuration->addPersistentEntry('Project/Familinet/Events/SearchManager/Project_Library',
			\Project\Library\Events\SearchManager\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/PatchManager/Project_Library', \Project\Library\Setup\Patch\Listeners::class);

		$configuration->addPersistentEntry('Project/Familinet/webApps/familinet/modules/Project_Library', true);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \RuntimeException
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$this->installStatuses($applicationServices);
		$this->installGenericAttributes($applicationServices);
		$this->installLanguagesCollection($applicationServices);
		$this->installLanguageFunctionsCollection($applicationServices);
		$this->installAudiencesCollection($applicationServices);
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function installStatuses($applicationServices)
	{
		$json = \Zend\Json\Json::decode(file_get_contents(__DIR__ . '/Assets/statuses.json'),
			\Zend\Json\Json::TYPE_ARRAY);

		$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
		$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$import->addOnly(true);

		try
		{
			$applicationServices->getTransactionManager()->begin();
			$import->fromArray($json);
			$applicationServices->getTransactionManager()->commit();
		}
		catch (\Exception $e)
		{
			throw $applicationServices->getTransactionManager()->rollBack($e);
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function installGenericAttributes($applicationServices)
	{
		$json = \Zend\Json\Json::decode(file_get_contents(__DIR__ . '/Assets/typologies.json'),
			\Zend\Json\Json::TYPE_ARRAY);

		$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
		$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());
		$import->getOptions()->set('resolveInlineDocument', [$import, 'defaultResolveCollectionItem']);
		$import->addOnly(true);

		$dcm = $applicationServices->getDocumentCodeManager();
		$callback = function ($document, $jsonDocument) use ($import, $dcm)
		{
			(new \Rbs\Generic\Attributes\ListenerCallbacks)->preSaveImport($document, $jsonDocument, $import, $dcm);
		};
		$import->getOptions()->set('preSave', $callback);

		try
		{
			$applicationServices->getTransactionManager()->begin();
			$import->fromArray($json);
			$applicationServices->getTransactionManager()->commit();
		}
		catch (\Exception $e)
		{
			throw $applicationServices->getTransactionManager()->rollBack($e);
		}
	}

	/**
	 * http://www.lingoes.net/en/translator/langcode.htm
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function installLanguagesCollection($applicationServices)
	{
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Project_Library_Languages') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Langues');
				$collection->setCode('Project_Library_Languages');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('fr');
				$item->getCurrentLocalization()->setTitle('Français');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('fr_BE');
				$item->getCurrentLocalization()->setTitle('Français (Belgique)');

				$collection->getItems()->add($item);
				
				$item = $collection->newCollectionItem();
				$item->setValue('fr_CA');
				$item->getCurrentLocalization()->setTitle('Français (Canada)');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('fr_CH');
				$item->getCurrentLocalization()->setTitle('Français (Suisse)');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('fr_FR');
				$item->getCurrentLocalization()->setTitle('Français (France)');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en');
				$item->getCurrentLocalization()->setTitle('Anglais');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en_AU');
				$item->getCurrentLocalization()->setTitle('Anglais (Australie)');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en_CA');
				$item->getCurrentLocalization()->setTitle('Anglais (Canada)');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en_GB');
				$item->getCurrentLocalization()->setTitle('Anglais (Angleterre)');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en_NZ');
				$item->getCurrentLocalization()->setTitle('Anglais (Nouvelle Zélande)');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('en_US');
				$item->getCurrentLocalization()->setTitle('Anglais (USA)');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('de');
				$item->getCurrentLocalization()->setTitle('Allemand');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('ko');
				$item->getCurrentLocalization()->setTitle('Coréen');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('es');
				$item->getCurrentLocalization()->setTitle('Espagnol');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('it');
				$item->getCurrentLocalization()->setTitle('Italien');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('ja');
				$item->getCurrentLocalization()->setTitle('Japonnais');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('sv');
				$item->getCurrentLocalization()->setTitle('Suédois');
				
				$collection->getItems()->add($item);

				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function installLanguageFunctionsCollection($applicationServices)
	{
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Project_Library_LanguageFunctions') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Fonctions de langue');
				$collection->setCode('Project_Library_LanguageFunctions');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('langue_principale');
				$item->getCurrentLocalization()->setTitle('Langue principale');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('soustitres');
				$item->getCurrentLocalization()->setTitle('Sous-titres');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('soustitres_sourds');
				$item->getCurrentLocalization()->setTitle('Sous-titres pour malentandants');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('langue_bonus');
				$item->getCurrentLocalization()->setTitle('Langue des bonus');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('soustitres_bonus');
				$item->getCurrentLocalization()->setTitle('Sous-titres des bonus');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('soustitres_bonus_sourds');
				$item->getCurrentLocalization()->setTitle('Sous-titres des bonus pour malentandants');
				
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('langue_aide');
				$item->getCurrentLocalization()->setTitle('Langue de l\'aide');
				
				$collection->getItems()->add($item);

				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function installAudiencesCollection($applicationServices)
	{
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Project_Library_Audiences') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Public');
				$collection->setCode('Project_Library_Audiences');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('audience_10+');
				$item->getCurrentLocalization()->setTitle('+ de 10 ans');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('audience_12+');
				$item->getCurrentLocalization()->setTitle('+ de 12 ans');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('audience_14+');
				$item->getCurrentLocalization()->setTitle('+ de 14 ans');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('audience_16+');
				$item->getCurrentLocalization()->setTitle('+ de 16 ans');

				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('audience_18+');
				$item->getCurrentLocalization()->setTitle('+ de 18 ans');

				$collection->getItems()->add($item);

				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}
}
