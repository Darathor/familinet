<?php
/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Project\Library\Setup\Patch;

/**
 * @name \Project\Library\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		// Patch 001: Fill birth and death years.
		// Patch 002 and 003 cancelled.
		// Patch 004: Fill "publishers" property with the "publisher" one.
		// Patch 005: Fill "works" property with the "work" one.
		// Patch 006: Remove useless query parameters on AlloCiné and IMDB links.
		// Patch 007 cancelled.
		// Patch 008: Normalize external website's URLs.
		// Patch 009: Compile dates on contributions.
		// Patch 010: Convert each AlloCiné links from http to https.
		$this->executePatch('Project_Library_010', 'Convert each AlloCiné links from http to https.', [$this, 'patch0010']);
		// Patch 011: Refresh compiled dates on contributions.
		$this->executePatch('Project_Library_011', 'Refresh compiled dates on contributions.', [$this, 'patch0011']);
		// Patch 012: Update reading rating: 1-3=>1 / 4=>2 / 5=>3 / 6=>4 / 7=>5 / 8=>7.
		$this->executePatch('Project_Library_012', 'Update reading rating: 1-3=>1 / 4=>2 / 5=>3 / 6=>4 / 7=>5 / 8=>7.', [$this, 'patch0012']);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0010(\Change\Plugins\Patch\Patch $patch)
	{
		$transactionManager = $this->applicationServices->getTransactionManager();
		$documentManager = $this->applicationServices->getDocumentManager();

		$updated = 0;
		$ignored = 0;
		$lastId = 0;

		$select = $this->applicationServices->getDbProvider()->getNewQueryBuilder();
		$fb = $select->getFragmentBuilder();
		$select->select($fb->allColumns());
		$select->from($fb->table('project_library_dat_external_links'));
		$select->where($fb->logicAnd(
			$fb->gt($fb->column('document_id'), $fb->integerParameter('lastId')),
			$fb->like($fb->column('link'), $fb->string('http://www.allocine.fr'))
		));
		$select->orderAsc($fb->column('document_id'));

		do
		{
			$found = false;
			try
			{
				$transactionManager->begin();

				$query = $select->query();
				$query->bindParameter('lastId', $lastId);
				$query->setMaxResults(50);
				$rowConverter = $query->getRowsConverter()->addIntCol('document_id')->addStrCol('link')->singleColumn('link')->indexBy('document_id');

				foreach ($query->getResults($rowConverter) as $documentId => $url)
				{
					$document = $documentManager->getDocumentInstance($documentId);
					if ($document instanceof \Project\Library\Indexer\LinkedDocument)
					{
						foreach ($document->getLinks() as $link)
						{
							$linkUrl = $link->getUrl();
							if (\Change\Stdlib\StringUtils::beginsWith($linkUrl, 'http://'))
							{
								$link->setUrl(\str_replace('http://', 'https://', $linkUrl));
							}
						}
						$document->save();
						$updated++;
					}
					elseif ($document)
					{
						$this->sendError('Invalid document type ' . $document->getDocumentModelName());
						$ignored++;
					}

					$lastId = $documentId;
					$found = true;
				}
				echo '.';

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->sendError($e->getMessage());
				$patch->setException($e);
				throw $transactionManager->rollBack($e);
			}
		}
		while ($found);

		echo \PHP_EOL;
		$this->sendInfo('links updated ' . $updated);
		$this->sendInfo('links ignored ' . $ignored);
		$patch->addInstallationData('linksUpdated', $updated);
		$patch->addInstallationData('linksIgnored', $ignored);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0011(\Change\Plugins\Patch\Patch $patch)
	{
		$transactionManager = $this->applicationServices->getTransactionManager();
		$compiled = 0;
		$lastId = 0;

		do
		{
			try
			{
				$transactionManager->begin();
				$query = $this->applicationServices->getDocumentManager()->getNewQuery('Project_Library_Contribution');
				$query->andPredicates(
					$query->gt($query->getColumn('id'), $lastId)
				);
				$query->addOrder($query->getColumn('id'));
				$documents = $query->getDocuments(0, 50)->toArray();

				/** @var \Project\Library\Documents\Publication $document */
				foreach ($documents as $document)
				{
					$document->save();
					$compiled++;
					$lastId = $document->getId();
				}
				echo '.';

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->sendError($e->getMessage());
				$patch->setException($e);
				throw $transactionManager->rollBack($e);
			}
		}
		while ($documents);

		echo \PHP_EOL;
		$this->sendInfo('documents compiled ' . $compiled);
		$patch->addInstallationData('documentsCompiled', $compiled);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0012(\Change\Plugins\Patch\Patch $patch)
	{
		$transactionManager = $this->applicationServices->getTransactionManager();
		$updated = 0;
		$lastId = 0;

		do
		{
			try
			{
				$transactionManager->begin();
				$query = $this->applicationServices->getDocumentManager()->getNewQuery('Project_Library_Reading');
				$query->andPredicates(
					$query->gt($query->getColumn('id'), $lastId)
				);
				$query->addOrder($query->getColumn('id'));
				$documents = $query->getDocuments(0, 50)->toArray();

				/** @var \Project\Library\Documents\Reading $document */
				foreach ($documents as $document)
				{
					switch ($document->getRating())
					{
						case 10:
						case 20:
						case 30:
							$document->setRating(10);
							break;

						case 40:
							$document->setRating(20);
							break;

						case 50:
							$document->setRating(30);
							break;

						case 60:
							$document->setRating(40);
							break;

						case 70:
							$document->setRating(50);
							break;

						case 80:
							$document->setRating(70);
							break;

						default:
							// Nothing to change.
					}
					if ($document->hasModifiedProperties())
					{
						$document->save();
						$updated++;
					}
					$lastId = $document->getId();
				}
				echo '.';

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$this->sendError($e->getMessage());
				$patch->setException($e);
				throw $transactionManager->rollBack($e);
			}
		}
		while ($documents);

		echo \PHP_EOL;
		$this->sendInfo('documents updated ' . $updated);
		$patch->addInstallationData('documentsUpdated', $updated);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}