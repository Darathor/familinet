/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryContributorAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/contributor-aliases.twig',
			scope: { aliases: '=projectLibraryContributorAliases' },
			link: function(scope) {
				scope.$watch('aliases', function() {
					scope.standard = [];
					scope.websiteSpecific = [];
					angular.forEach(scope.aliases, function(alias) {
						if (alias.forWebsite) {
							scope.websiteSpecific.push(alias);
						}
						else {
							scope.standard.push(alias);
						}
					});
				});
			}
		};
	});
})();