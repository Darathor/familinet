/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryWorkAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/work-aliases.twig',
			scope: { aliases: '=projectLibraryWorkAliases' }
		};
	});

	app.directive('projectLibraryWorkGroupAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/work-group-aliases.twig',
			scope: { aliases: '=projectLibraryWorkGroupAliases' }
		};
	});

	app.directive('projectLibraryPublisherAliases', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/publisher-aliases.twig',
			scope: { aliases: '=projectLibraryPublisherAliases' }
		};
	});

	app.directive('projectLibraryNationality', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/nationality.twig',
			scope: { nationality: '=projectLibraryNationality', mode: '@' }
		};
	});

	app.directive('projectLibraryPublication', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/publication.twig',
			scope: { publication: '=projectLibraryPublication', mode: '@' }
		};
	});

	app.directive('projectLibraryLinks', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/links.twig',
			scope: { links: '=projectLibraryLinks' }
		};
	});

	app.directive('projectLibraryUserLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/user-link.twig',
			scope: { user: '=projectLibraryUserLink' }
		};
	});

	app.directive('projectLibraryUserLinks', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/user-links.twig',
			scope: { users: '=projectLibraryUserLinks' }
		};
	});

	app.directive('projectLibraryContributorLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/contributor-link.twig',
			scope: { contributor: '=projectLibraryContributorLink' }
		};
	});

	app.directive('projectLibraryWorkLink', function() {
		return {
			restrict: 'A',
			templateUrl: function (elm, attrs) {
				return attrs.removeButton ? 'Project_Library/directives/work-link.twig' : 'Project_Library/directives/work-link-editable.twig';
			},
			scope: { work: '=projectLibraryWorkLink' }
		};
	});

	app.directive('projectLibraryWorkGroupLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/work-group-link.twig',
			scope: { group: '=projectLibraryWorkGroupLink' }
		};
	});

	app.directive('projectLibraryPublisherLink', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/publisher-link.twig',
			scope: { publisher: '=projectLibraryPublisherLink' }
		};
	});

	app.directive('projectLibraryContributionRoles', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/contribution-roles.twig',
			scope: { contribution: '=projectLibraryContributionRoles' }
		};
	});

	app.directive('projectLibraryPublicationHeader', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/publication-header.twig',
			scope: { publication: '=projectLibraryPublicationHeader' }
		};
	});
})();