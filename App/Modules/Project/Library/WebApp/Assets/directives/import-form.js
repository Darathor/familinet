/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryImportForm', ['RbsChange.REST', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter', 'RbsChange.i18n',
		function(REST, ErrorFormatter, NotificationCenter, i18n) {
			return {
				restrict: 'A',
				templateUrl: 'Project_Library/directives/import-form.twig',
				scope: false,
				link: function(scope, element, attrs) {
					var type = attrs.type;
					scope.importManager.conflictsTemplate = attrs.conflictsTemplate;
					if (type) {
						REST.call(REST.getBaseUrl('projectLibrary/getAvailableWebsites'), { type: type }).then(
							function(data) {
								scope.importManager.websites = data.websites;
							},
							function(data) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(data),
									'EDITOR'
								);
								scope.importManager.importing = false;
							}
						);
					}
					else {
						console.error('Missing type for projectLibraryImportForm');
					}
				}
			};
		}
	]);
})();