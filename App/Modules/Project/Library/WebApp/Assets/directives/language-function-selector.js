/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryLanguageFunctionSelector', ['$timeout', function($timeout) {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/language-function-selector.twig',
			scope: {
				selection: '=projectLibraryLanguageFunctionSelector',
				ready: '='
			},
			link: function(scope) {
				scope.languageToSelect = null;
				scope.collections = {};
				scope.selector = {
					languages: [],
					toAdd: null
				};

				scope.addLanguage = function() {
					if (!scope.selector.toAdd) {
						return;
					}

					var alreadySelected = false;
					angular.forEach(scope.selector.languages, function(item) {
						if (item.value === scope.selector.toAdd.value) {
							alreadySelected = true;
						}
					});

					if (!alreadySelected) {
						scope.selector.languages.push(scope.selector.toAdd);
					}
				};

				scope.$watch('collections.languages', function initSelector() {
					if (!scope.ready() || !angular.isObject(scope.selection)) {
						$timeout(initSelector, 100);
						return;
					}
					scope.selector.languages = [];
					angular.forEach(scope.collections.languages, function(item) {
						if (item.value === 'fr' || item.value === 'en' || item.value === 'ja' || scope.selection[item.value]) {
							scope.selector.languages.push(item);
						}
					});
				});

				scope.$watch('selection', function () {
					angular.forEach(scope.selection, function(value, key) {
						if (angular.isArray(value)) {
							scope.selection[key] = {};
						}
					});
				});
			}
		};
	}]);
})();