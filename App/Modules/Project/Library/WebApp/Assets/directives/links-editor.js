/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryLinksEditor', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/links-editor.twig',
			scope: { modelName: '@', document: '=' }
		};
	});

	app.directive('projectLibraryLinkEditor', ['RbsChange.REST', 'RbsChange.i18n', function(REST, i18n) {
		return {
			restrict: 'A',
			link: function(scope) {
				scope.$watch('link.url', function(url) {
					scope.link.duplicates = [];
					if (!url) {
						return;
					}

					REST.call(REST.getBaseUrl('projectLibrary/getWebsiteInfosByExternalLink'), { link: url }).then(
						function(data) {
							var params = { NEW: data.label, OLD: scope.link.label };
							if (data.normalizedUrl)
							{
								scope.link.url = data.normalizedUrl;
							}
							if (!scope.link.label || (scope.link.label !== data.label &&
								confirm(i18n.trans('m.project.library.webappjs.confirm_replace_link_label|ucf', params)))) {
								scope.link.label = data.label;
							}
						},
						function(data) {
							// Website not found, nothing to do.
						}
					);

					REST.call(REST.getBaseUrl('projectLibrary/getDocumentsByExternalLink'), { link: url, ignoreInherited: true }).then(
						function(data) {
							angular.forEach(data.resources, function(document) {
								if (document.id !== scope.document.id) {
									scope.link.duplicates.push(document);
								}
							})
						},
						function() {
							// Nothing to do...
						}
					);
				});
			}
		};
	}]);
})();