/**
 * Copyright (C) 2015 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryStatus', function() {
		return {
			restrict: 'A',
			templateUrl: 'Project_Library/directives/status.twig',
			scope: { status: '=projectLibraryStatus', mode: '@' },
			link: function(scope, element) {
				element.find('.title-tooltip').tooltip();
			}
		};
	});
})();