/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryModalResolveConflictsController', ['$scope', 'template', 'document', 'conflicts',
		function(scope, templateName, document, conflicts) {
			scope.templateName = templateName;
			scope.document = document;
			scope.conflicts = conflicts;

			// Scalar values.
			scope.replaceScalarValue = function (propertyName) {
				scope.document[propertyName] = scope.conflicts[propertyName];
			};

			scope.appendScalarValue = function (propertyName) {
				scope.document[propertyName] += ' ' + scope.conflicts[propertyName];
			};

			// Richtext values.
			scope.replaceRichtextValue = function (propertyName) {
				scope.document[propertyName].t = scope.conflicts[propertyName];
			};

			scope.appendRichtextValue = function (propertyName) {
				scope.document[propertyName].t += '\n\n' + scope.conflicts[propertyName];
			};
		}
	]);
})();