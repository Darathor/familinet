(function() {
	'use strict';

	var moduleName = 'Project_Library';

	__change.routes['/Library/'] = {
		name: 'home',
		module: 'Project_Library',
		rule: {
			labelKey: 'm.project.library.webappjs.library | ucf',
			icon: 'book',
			templateUrl: moduleName + '/views/home.twig',
			controller: 'EmptyController'
		}
	};

// USER

	__change.routes['/Library/User/'] = {
		name: 'list',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.webappjs.user_list | ucf',
			templateUrl: moduleName + '/views/user/user-list.twig',
			controller: 'LibraryUserListController'
		}
	};

	__change.routes['/Library/User/:id'] = {
		name: 'detail',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.webappjs.user | ucf',
			templateUrl: moduleName + '/views/user/user-detail.twig',
			controller: 'LibraryUserDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/User/:id/Possession/'] = {
		name: 'possessions',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.webappjs.possession_list | ucf',
			templateUrl: moduleName + '/views/user/user-possession-list.twig',
			controller: 'LibraryUserDetailController'
		}
	};

	__change.routes['/Library/User/:id/Reading/'] = {
		name: 'readings',
		model: 'Rbs_User_User',
		rule: {
			labelKey: 'm.project.library.webappjs.reading | ucf',
			templateUrl: moduleName + '/views/user/user-reading-list.twig',
			controller: 'LibraryUserDetailController'
		}
	};

// WORK

	__change.routes['/Library/Work/'] = {
		name: 'list',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.webappjs.work_list | ucf',
			templateUrl: moduleName + '/views/work/work-list.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/new'] = {
		name: 'new',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/work/work-new.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/newDone'] = {
		name: 'newDone',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/work/work-new-done.twig',
			controller: 'LibraryWorkNewDoneController'
		}
	};

	__change.routes['/Library/Work/:id'] = {
		name: 'detail',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.webappjs.work | ucf',
			templateUrl: moduleName + '/views/work/work-detail.twig',
			controller: 'LibraryWorkDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/work/work-edit.twig',
			controller: 'LibraryWorkEditController'
		}
	};

	__change.routes['/Library/Work/:id/Publication/'] = {
		name: 'publications',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_list | ucf',
			templateUrl: moduleName + '/views/work/work-publication-list.twig',
			controller: 'LibraryWorkDetailController'
		}
	};

	__change.routes['/Library/Work/:id/Contribution/'] = {
		name: 'contributions',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.webappjs.contribution_list | ucf',
			templateUrl: moduleName + '/views/work/work-contribution-list.twig',
			controller: 'LibraryWorkDetailController'
		}
	};

	__change.routes['/Library/Work/:id/Possession/'] = {
		name: 'possessions',
		model: 'Project_Library_Work',
		rule: {
			labelKey: 'm.project.library.webappjs.possession_list | ucf',
			templateUrl: moduleName + '/views/work/work-possession-list.twig',
			controller: 'LibraryWorkDetailController'
		}
	};

	__change.routes['/Library/Work/:id/Reading/'] = {
		name: 'readings',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.webappjs.reading_list | ucf',
			templateUrl: moduleName + '/views/work/work-reading-list.twig',
			controller: 'LibraryWorkDetailController'
		}
	};

// GENRE

	__change.routes['/Library/Genre/'] = {
		name: 'list',
		model: 'Project_Library_Genre',
		rule: {
			labelKey: 'm.project.library.webappjs.genre_list | ucf',
			templateUrl: moduleName + '/views/genre/genre-list.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Genre/new'] = {
		name: 'new',
		model: 'Project_Library_Genre',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/genre/genre-new.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Genre/:id'] = {
		name: 'detail',
		model: 'Project_Library_Genre',
		rule: {
			labelKey: 'm.project.library.webappjs.genre | ucf',
			templateUrl: moduleName + '/views/genre/genre-detail.twig',
			controller: 'LibraryGenreDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Genre/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Genre',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/genre/genre-edit.twig',
			controller: 'LibraryGenreEditController'
		}
	};

// WORK GROUP

	__change.routes['/Library/WorkGroup/'] = {
		name: 'list',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.library.webappjs.work_group_list | ucf',
			templateUrl: moduleName + '/views/work-group/work-group-list.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/WorkGroup/new'] = {
		name: 'new',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			templateUrl: moduleName + '/views/work-group/work-group-new.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/WorkGroup/:id'] = {
		name: 'detail',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.library.webappjs.work_group | ucf',
			templateUrl: moduleName + '/views/work-group/work-group-detail.twig',
			controller: 'LibraryWorkGroupDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/WorkGroup/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_WorkGroup',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/work-group/work-group-edit.twig',
			controller: 'LibraryWorkGroupEditController'
		}
	};

// CONTRIBUTOR

	__change.routes['/Library/Contributor/'] = {
		name: 'list',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.webappjs.contributor_list | ucf',
			templateUrl: moduleName + '/views/contributor/contributor-list.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Contributor/new'] = {
		name: 'new',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/contributor/contributor-new.twig',
			controller: 'LibraryContributorNewController'
		}
	};
	__change.routes['/Library/Contributor/:id'] = {
		name: 'detail',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.webappjs.contributor | ucf',
			templateUrl: moduleName + '/views/contributor/contributor-detail.twig',
			controller: 'LibraryContributorDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Contributor/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/contributor/contributor-edit.twig',
			controller: 'LibraryContributorEditController'
		}
	};

	__change.routes['/Library/Contributor/:id/Contribution/'] = {
		name: 'contributions',
		model: 'Project_Library_Contributor',
		rule: {
			labelKey: 'm.project.library.webappjs.contribution_list | ucf',
			templateUrl: moduleName + '/views/contributor/contributor-contribution-list.twig',
			controller: 'LibraryContributorDetailController'
		}
	};

// CONTRIBUTION

	__change.routes['/Library/Work/:workId/Contribution/new'] = {
		name: 'new',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/contribution/contribution-new.twig',
			controller: 'LibraryContributionNewController'
		}
	};
	__change.routes['/Library/Work/:workId/Contribution/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/contribution/contribution-edit.twig',
			controller: 'LibraryContributionEditController'
		}
	};
	__change.routes['/Library/Work/:workId/Contribution/:id'] = {
		name: 'detail',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.library.webappjs.contribution | ucf',
			templateUrl: moduleName + '/views/contribution/contribution-detail.twig',
			controller: 'LibraryContributionDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Contributor/:contributorId/Contribution/new'] = {
		name: 'newForContributor',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/contribution/contribution-new.twig',
			controller: 'LibraryContributionNewForContributorController'
		}
	};
	__change.routes['/Library/Contributor/:contributorId/Contribution/:id'] = {
		name: 'detailForContributor',
		model: 'Project_Library_Contribution',
		rule: {
			labelKey: 'm.project.library.webappjs.contribution | ucf',
			templateUrl: moduleName + '/views/contribution/contribution-detail.twig',
			controller: 'LibraryContributionDetailController',
			labelId: 'id'
		}
	};

// PUBLISHER

	__change.routes['/Library/Publisher/'] = {
		name: 'list',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.webappjs.publisher_list | ucf',
			templateUrl: moduleName + '/views/publisher/publisher-list.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Publisher/new'] = {
		name: 'new',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/publisher/publisher-new.twig',
			controller: 'EmptyController'
		}
	};
	__change.routes['/Library/Publisher/:id'] = {
		name: 'detail',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.webappjs.publisher | ucf',
			templateUrl: moduleName + '/views/publisher/publisher-detail.twig',
			controller: 'LibraryPublisherDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Publisher/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/publisher/publisher-edit.twig',
			controller: 'LibraryPublisherEditController'
		}
	};

	__change.routes['/Library/Publisher/:id/Publication/'] = {
		name: 'publications',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_list | ucf',
			templateUrl: moduleName + '/views/publisher/publisher-publication-list.twig',
			controller: 'LibraryPublisherDetailController'
		}
	};

	__change.routes['/Library/Publisher/:id/Collection/'] = {
		name: 'collections',
		model: 'Project_Library_Publisher',
		rule: {
			labelKey: 'm.project.library.webappjs.collection_list | ucf',
			templateUrl: moduleName + '/views/publisher/publisher-collection-list.twig',
			controller: 'LibraryPublisherDetailController'
		}
	};


// COLLECTION

	__change.routes['/Library/Publisher/:publisherId/Collection/new'] = {
		name: 'new',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/collection/collection-new.twig',
			controller: 'LibraryCollectionNewController'
		}
	};
	__change.routes['/Library/Publisher/:publisherId/Collection/:id'] = {
		name: 'detail',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.library.webappjs.collection | ucf',
			templateUrl: moduleName + '/views/collection/collection-detail.twig',
			controller: 'LibraryCollectionDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Publisher/:publisherId/Collection/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/collection/collection-edit.twig',
			controller: 'LibraryCollectionEditController'
		}
	};
	__change.routes['/Library/Publisher/:publisherId/Collection/:id/Publication/'] = {
		name: 'publications',
		model: 'Project_Library_Collection',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_list | ucf',
			templateUrl: moduleName + '/views/collection/collection-publication-list.twig',
			controller: 'LibraryCollectionDetailController'
		}
	};

// PUBLICATION

	__change.routes['/Library/Work/:workId/Publication/'] = {
		name: 'list',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_list | ucf',
			redirectTo: '/Library/Work/:workId'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/new'] = {
		name: 'new',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/publication/publication-new.twig',
			controller: 'LibraryPublicationNewController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id'] = {
		name: 'detail',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.webappjs.publication | ucf',
			templateUrl: moduleName + '/views/publication/publication-detail.twig',
			controller: 'LibraryPublicationDetailController',
			labelId: 'id'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/publication/publication-edit.twig',
			controller: 'LibraryPublicationEditController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Reading/'] = {
		name: 'readings',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.webappjs.reading_list | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/work/work-reading-list.twig',
			controller: 'LibraryPublicationDetailController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Possession/'] = {
		name: 'possessions',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.webappjs.possession_list | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/publication/publication-possession-list.twig',
			controller: 'LibraryPublicationDetailController'
		}
	};
	__change.routes['/Library/Work/:workId/Publication/:id/Volume/'] = {
		name: 'volumes',
		model: 'Project_Library_Publication',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_volume_list | ucf',
			templateUrl: moduleName + '/views/publication-volume/publication-volume-list.twig',
			controller: 'LibraryPublicationDetailController'
		}
	};

// PUBLICATION VOLUME

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/'] = {
		name: 'list',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_volume_list | ucf',
			redirectTo: '/Library/Work/:workId/Publication/:publicationId/Volume/'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/new'] = {
		name: 'new',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/publication-volume/publication-volume-new.twig',
			controller: 'LibraryPublicationVolumeNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id'] = {
		name: 'detail',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.webappjs.publication_volume | ucf',
			templateUrl: moduleName + '/views/publication-volume/publication-volume-detail.twig',
			controller: 'LibraryPublicationVolumeDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/publication-volume/publication-volume-edit.twig',
			controller: 'LibraryPublicationVolumeEditController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/PublicationVolume/:id/Reading'] = {
		name: 'readings',
		model: 'Project_Library_PublicationVolume',
		rule: {
			labelKey: 'm.project.library.webappjs.reading_list | ucf',
			templateUrl: moduleName + '/views/work/work-reading-list.twig',
			controller: 'LibraryPublicationVolumeDetailController'
		}
	};

// POSSESSION

	__change.routes['/Library/Possession/'] = {
		name: 'list',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.library.webappjs.possession_list | ucf',
			templateUrl: moduleName + '/views/possession/possession-list.twig',
			controller: 'EmptyController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/new'] = {
		name: 'new',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/possession/possession-new.twig',
			controller: 'LibraryPossessionNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Publication/:publicationId/Possession/new'] = {
		name: 'newOnPossession',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/possession/possession-new.twig',
			controller: 'LibraryPossessionNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:id'] = {
		name: 'detail',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.library.webappjs.possession | ucf',
			templateUrl: moduleName + '/views/possession/possession-detail.twig',
			controller: 'LibraryPossessionDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Possession',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/possession/possession-edit.twig',
			controller: 'LibraryPossessionEditController'
		}
	};

// LOAN

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/'] = {
		name: 'list',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.library.webappjs.loan_list | ucf',
			redirectTo: '/Library/Work/:workId/Possession/:possessionId'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/new'] = {
		name: 'new',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/loan/loan-new.twig',
			controller: 'LibraryLoanNewController'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/:id'] = {
		name: 'detail',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.library.webappjs.loan | ucf',
			templateUrl: moduleName + '/views/loan/loan-detail.twig',
			controller: 'LibraryLoanDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/Work/:workId/Possession/:possessionId/Loan/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Loan',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/loan/loan-edit.twig',
			controller: 'LibraryLoanEditController'
		}
	};

// READING

	__change.routes['/Library/Reading/'] = {
		name: 'list',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.webappjs.reading_list | ucf',
			templateUrl: moduleName + '/views/reading/reading-list.twig',
			controller: 'LibraryReadingListController'
		}
	};

	__change.routes['/Library/User/:userId/Reading/new'] = {
		name: 'new',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.familinet.webappjs.creation | ucf',
			icon: 'plus',
			templateUrl: moduleName + '/views/reading/reading-new.twig',
			controller: 'LibraryReadingNewController'
		}
	};

	__change.routes['/Library/User/:userId/Reading/:id'] = {
		name: 'detail',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.library.webappjs.reading | ucf',
			templateUrl: moduleName + '/views/reading/reading-detail.twig',
			controller: 'LibraryReadingDetailController',
			labelId: 'id'
		}
	};

	__change.routes['/Library/User/:userId/Reading/:id/edit'] = {
		name: 'edit',
		model: 'Project_Library_Reading',
		rule: {
			labelKey: 'm.project.familinet.webappjs.edition | ucf',
			icon: 'pencil',
			templateUrl: moduleName + '/views/reading/reading-edit.twig',
			controller: 'LibraryReadingEditController'
		}
	};
})();