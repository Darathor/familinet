/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.service('projectLibraryContributions', function() {
		this.calculateEpisodeCount = function(document) {
			var count = 0;
			if (angular.isString(document.description.t)) {
				var lines = document.description.t.split(/\r\n|\r|\n/);
				angular.forEach(lines, function(line) {
					if (/^\s*[\-*]/.test(line)) {
						count++;
					}
				});
			}
			document.episodeCount = count;
		};

		this.evaluateDescription = function(document, source) {
			if (angular.isString(document.description.t)) {
				if (source === 'IMDB') {
					var count = 0;
					var beginYear = 9999;
					var endYear = 0;
					var detail = [];

					var lines = document.description.t.split(/\r\n|\r|\n/);
					angular.forEach(lines, function(line) {
						if (/^\s*[\-*]/.test(line)) {
							var matches = /\((\d{4})\) ... (.*)/.exec(line);
							if (matches && matches.length === 3) {
								var toAdd = matches[2];

								var suffixMatches = /(.*) \((.*)\)$/.exec(toAdd);
								if (suffixMatches) {
									var suffix = suffixMatches[2];
									if (suffix === 'credit only') {
										return;
									}
									if (suffix === 'uncredited' || suffix.startsWith('as ')) {
										toAdd = suffixMatches[1];
									}
								}

								var year = parseInt(matches[1], 10);
								if (year < beginYear) {
									beginYear = year;
								}
								if (year > endYear) {
									endYear = year;
								}

								for (var i = 0; i < detail.length; i++) {
									if (toAdd === detail[i]) {
										toAdd = null;
										break;
									}
								}
								if (toAdd) {
									detail.push(toAdd);
								}
							}
							count++;
						}
					});

					document.episodeCount = count;
					if (beginYear !== 9999) {
						document.contributionYear = beginYear;
					}
					if (endYear !== 0) {
						document.contributionEndYear = endYear;
					}
					if (detail.length) {
						document.detail = detail.join(' / ');
					}
				}
				else {
					console.error('Unknown source', source);
				}
			}
		}
	});
})();