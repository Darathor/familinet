(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.service('projectLibraryImport', ['RbsChange.REST', 'RbsChange.i18n', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter',
		'proximisModalStack', function(REST, i18n, NotificationCenter, ErrorFormatter, proximisModalStack) {
			var me = this;

			this.initImportManager = function(scope, type, applyImportedData) {
				scope.importManager = {
					url: null,
					importing: false,
					import: function(doc) {
						scope.importManager.importing = true;
						var params = { url: scope.importManager.url, importType: type };
						REST.call(REST.getBaseUrl('projectLibrary/parseExternalPage'), params).then(
							function(data) {
								var conflicts = applyImportedData(doc, data);
								if (conflicts && scope.importManager.conflictsTemplate) {
									proximisModalStack.open({
										templateUrl: 'Project_Library/modals/resolve-conflicts.twig',
										size: 'full',
										controller: 'LibraryModalResolveConflictsController',
										resolve: {
											template: function() { return scope.importManager.conflictsTemplate; },
											document: function() { return doc; },
											conflicts: function() { return conflicts; }
										}
									});
								}
								scope.importManager.importing = false;
							},
							function(data) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(data),
									'EDITOR'
								);
								scope.importManager.importing = false;
							}
						);
					}
				}
			};

			this.isInArray = function(array, value, propertyName) {
				if (!angular.isArray(array)) {
					return false;
				}
				var result = false;
				angular.forEach(array, function(element) {
					if (element.hasOwnProperty(propertyName) && element[propertyName] === value[propertyName]) {
						result = true;
					}
				});
				return result;
			};

			this.addSubDocuments = function(doc, property, subDocuments, checkProperty, single) {
				if (single) {
					var subDocument = angular.isArray(subDocuments) ? subDocuments[0] : subDocuments;
					if (angular.isObject(subDocument)) {
						doc[property] = subDocument;
					}
				}
				else {
					angular.forEach(subDocuments, function(item) {
						if (angular.isObject(item)) {
							if (!angular.isArray(doc[property])) {
								doc[property] = [];
							}
							if (!me.isInArray(doc[property], item, checkProperty)) {
								doc[property].push(item);
							}
						}
					});
				}
			};

			this.setTypology = function(doc, typology, attributes) {
				var conflicts = {};
				if (!angular.isObject(doc.typology$)) {
					doc.typology$ = {};
				}

				if (!doc.typology$.__id) {
					doc.typology$.__id = typology;
				}
				else if (doc.typology$.__id !== typology) {
					conflicts.__id = typology;
				}

				if (attributes) {
					angular.forEach(attributes, function(value, attribute) {
						if (doc.typology$[attribute] === undefined || doc.typology$[attribute] === null) {
							doc.typology$[attribute] = value;
						}
						else {
							conflicts[attribute] = value;
						}
					});
				}
				return angular.equals({}, conflicts) ? null : conflicts;
			};

			this.applyImportedData = function(doc, data, properties) {
				var conflicts = {};
				if (angular.isArray(properties.scalar)) {
					angular.forEach(properties.scalar, function(property) {
						if (data[property]) {
							if (doc[property] === undefined || doc[property] === null) {
								doc[property] = data[property];
							}
							else if (doc[property] !== data[property]) {
								conflicts[property] = data[property];
							}
						}
					});
				}

				if (angular.isArray(properties.richText)) {
					angular.forEach(properties.richText, function(property) {
						if (data[property]) {
							if (!doc[property] || !doc[property].t) {
								doc[property] = { e: 'Markdown', t: data[property], h: null };
							}
							else if (doc[property].t !== data[property]) {
								conflicts[property] = data[property];
							}
						}
					});
				}

				if (angular.isArray(properties.subDoc)) {
					angular.forEach(properties.subDoc, function(propertyData) {
						if (data[propertyData.name]) {
							me.addSubDocuments(doc, propertyData.name, data[propertyData.name], propertyData.check, propertyData.single || false);
						}
					});
				}

				if (data.typology) {
					var typologyConflicts = me.setTypology(doc, data.typology, data.attributes);
					if (typologyConflicts) {
						conflicts.typology = typologyConflicts;
					}
				}
				return angular.equals({}, conflicts) ? null : conflicts;
			}
		}
	]);
})();