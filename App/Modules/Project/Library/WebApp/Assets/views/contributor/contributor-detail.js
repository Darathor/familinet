/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryContributorDetailController', ['$scope', '$routeParams', '$location', 'RbsChange.Document', 'RbsChange.Breadcrumb',
		'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter',
		function(scope, $routeParams, $location, Document, Breadcrumb, i18n, REST, NotificationCenter, ErrorFormatter) {
			Document.initDetailView(scope, 'Project_Library_Contributor');

			scope.contributionStaticFilters = [
				{
					name: 'contribution',
					parameters: { propertyName: 'contributor', value: $routeParams.id, operator: 'eq' }
				}
			];

			scope.newSubDocumentUrlParameters = { contributorId: $routeParams.id };

			// Deletion handling.
			scope.data = {
				contributionList: {},
				target: {
					contributor: null
				}
			};

			scope.confirmDelete = function() {
				if (scope.data.contributionList.totalItems) {
					if (confirm(i18n.trans('m.project.library.webappjs.contributor_confirm_merge'))) {
						var params = {
							toDeleteId: scope.document.id,
							targetId: scope.data.target.contributor.id
						};
						REST.apiPost('projectLibrary/mergeDocuments', params).then(
							function() {
								var entries = Breadcrumb.pathEntries();
								var entry = entries[entries.length - 1];
								$location.url(entry.url());
							},
							function(error) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(error),
									'MERGE_DOCUMENT'
								);
							}
						);
					}
				}
				else {
					Document.deleteFromDetailView(scope.document);
				}
			};
		}
	]);
})();