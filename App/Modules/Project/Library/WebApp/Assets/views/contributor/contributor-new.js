/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryContributorNewController', ['$scope', 'projectLibraryImport', function(scope, projectLibraryImport) {
		projectLibraryImport.initImportManager(scope, 'contributor', function(doc, data) {
			if (angular.isObject(data.contributor)) {
				return projectLibraryImport.applyImportedData(doc, data.contributor, {
						scalar: ['label', 'disambiguationSuffix', 'birthDate', 'birthYear', 'deathDate', 'deathYear'],
						richText: ['description'],
						subDoc: [
							{ name: 'nationalities', check: 'id' },
							{ name: 'aliases', check: 'label' },
							{ name: 'links', check: 'url' }
						]
					}
				);
			}
		});
	}]);
})();