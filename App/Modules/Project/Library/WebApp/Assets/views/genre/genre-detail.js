/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryGenreDetailController', ['$scope', '$routeParams', '$location', 'RbsChange.Document', 'RbsChange.Breadcrumb',
		'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter',
		function(scope, $routeParams, $location, Document, Breadcrumb, i18n, REST, ErrorFormatter, NotificationCenter) {
			Document.initDetailView(scope, 'Project_Library_Genre');

			scope.workStaticFilters = [
				{
					name: 'genres',
					parameters: { propertyName: 'genres', value: $routeParams.id, operator: 'eq' }
				}
			];

			scope.works = {
				busy: false,
				list: [],
				add: function() {
					scope.works.busy = true;

					var params = {
						genreId: scope.document.id,
						workIds: []
					};
					angular.forEach(scope.works.list, function(work) {
						params.workIds.push(work.id);
					});

					REST.apiPut('projectLibrary/addWorksToGenre', params).then(
						function() {
							scope.$broadcast('AppReloadList');
							scope.works.list = [];
							scope.works.busy = false;
						},
						function(data) {
							NotificationCenter.error(
								i18n.trans('m.project.library.webappjs.error_adding_works|ucf'),
								ErrorFormatter.format(data),
								'ADD_WORKS'
							);
							scope.works.busy = false;
						}
					);
				}
			};

			// Deletion handling.
			scope.data = {
				workList: {},
				target: {
					genre: null
				}
			};

			scope.confirmDelete = function() {
				if (scope.data.workList.totalItems) {
					if (confirm(i18n.trans('m.project.library.webappjs.genre_confirm_merge'))) {
						var params = {
							toDeleteId: scope.document.id,
							targetId: scope.data.target.genre.id
						};
						REST.apiPost('projectLibrary/mergeDocuments', params).then(
							function() {
								var entries = Breadcrumb.pathEntries();
								var entry = entries[entries.length - 1];
								$location.url(entry.url());
							},
							function(error) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(error),
									'MERGE_DOCUMENT'
								);
							}
						);
					}
				}
				else {
					Document.deleteFromDetailView(scope.document);
				}
			};
		}
	]);
})();