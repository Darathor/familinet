/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryLoanDetailController', ['$scope', 'RbsChange.Document', 'RbsChange.REST', function(scope, Document, REST) {
		Document.initDetailView(scope, 'Project_Library_Loan').then(function(doc) {
			scope.document = doc;

			REST.resource('Project_Library_Possession', doc.possession.id).then(function(possession) {
				scope.possession = possession;
			});
		});
	}]);
})();