/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryPublicationVolumeDetailController', ['$scope', 'RbsChange.Document', '$routeParams',
		function(scope, Document, $routeParams) {
			Document.initDetailView(scope, 'Project_Library_PublicationVolume');

			scope.readingStaticFilters = [
				{
					name: 'reading',
					parameters: { propertyName: 'volumes', value: $routeParams.id, operator: 'eq' }
				}
			];
		}
	]);
})();