/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryPublicationVolumeNewController', ['$routeParams', '$rootScope', 'RbsChange.Events', 'RbsChange.REST',
		'RbsChange.Breadcrumb', 'RbsChange.i18n', 'RbsChange.ErrorFormatter', 'RbsChange.NotificationCenter',
		function($routeParams, $rootScope, Events, REST, Breadcrumb, i18n, ErrorFormatter, NotificationCenter) {
			$rootScope.$on(Events.EditorReady, function(event, parameters) {
				REST.resource('Project_Library_Publication', $routeParams.publicationId).then(
					function(doc) {
						parameters.document.publication = doc;
					},
					function(reason) {
						NotificationCenter.error(
							i18n.trans('m.project.familinet.webappjs.load_error'),
							ErrorFormatter.format(reason),
							'LOAD_DOCUMENT'
						);
						console.warn('Can\'t load the document with id ' + $routeParams.workId +
							', so redirect to list.\nError message: ' + reason.message);
						Breadcrumb.goParent();
					}
				);
			});
		}
	]);
})();