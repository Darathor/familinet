/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryPublisherDetailController', ['$scope', '$routeParams', '$location', 'RbsChange.Document', 'RbsChange.Breadcrumb',
		'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.NotificationCenter', 'RbsChange.ErrorFormatter',
		function(scope, $routeParams, $location, Document, Breadcrumb, i18n, REST, NotificationCenter, ErrorFormatter) {
			Document.initDetailView(scope, 'Project_Library_Publisher');

			scope.publicationStaticFilters = [
				{
					name: 'publishers',
					parameters: { propertyName: 'publishers', value: $routeParams.id, operator: 'eq' }
				}
			];
			scope.collectionStaticFilters = [
				{
					name: 'publisher',
					parameters: { propertyName: 'publisher', value: $routeParams.id, operator: 'eq' }
				}
			];

			// Url parameter to create children.
			scope.newSubDocumentUrlParameters = { publisherId: $routeParams.id };

			// Deletion handling.
			scope.data = {
				publicationList: {},
				collectionList: {},
				target: {
					publisher: null,
					collectionId: null
				}
			};

			scope.confirmDelete = function() {
				if (scope.data.publicationList.totalItems || scope.data.collectionList.totalItems) {
					if (confirm(i18n.trans('m.project.library.webappjs.publisher_confirm_merge'))) {
						var params = {
							toDeleteId: scope.document.id,
							targetId: scope.data.target.publisher.id,
							targetCollectionId: scope.data.target.collectionId
						};
						REST.apiPost('projectLibrary/mergeDocuments', params).then(
							function() {
								var entries = Breadcrumb.pathEntries();
								var entry = entries[entries.length - 1];
								$location.url(entry.url());
							},
							function(error) {
								NotificationCenter.error(
									i18n.trans('m.project.familinet.webappjs.an_error_occurred|ucf'),
									ErrorFormatter.format(error),
									'MERGE_DOCUMENT'
								);
							}
						);
					}
				}
				else {
					Document.deleteFromDetailView(scope.document);
				}
			};
		}
	]);
})();