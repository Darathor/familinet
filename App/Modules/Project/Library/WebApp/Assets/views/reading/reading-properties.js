/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('projectLibraryReadingProperties', function() {
		return {
			restrict: 'A',
			scope: true,

			link: function(scope) {
				scope.publicationsStaticFilters = [
					{
						name: 'publications',
						parameters: { propertyName: 'works', value: 0, operator: 'eq' }
					}
				];
				scope.volumesStaticFilters = [
					{
						name: 'volumes',
						parameters: { propertyName: 'publication', value: 0, operator: 'eq' }
					}
				];
				scope.$watch('document.work', function(newValue) {
					if (newValue) {
						scope.publicationsStaticFilters[0].parameters.value = newValue.id;
					}
					else {
						scope.publicationsStaticFilters[0].parameters.value = 0;
					}
				});
				scope.$watch('document.publication', function(newValue) {
					if (newValue) {
						scope.volumesStaticFilters[0].parameters.value = newValue.id;
					}
					else {
						scope.volumesStaticFilters[0].parameters.value = 0;
					}
				});
				scope.copyToEndDate = function() {
					scope.document.endDate = scope.document.beginDate;
				}
			}
		};
	});
})();