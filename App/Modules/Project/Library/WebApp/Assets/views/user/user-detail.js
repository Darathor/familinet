/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryUserDetailController', ['$rootScope', '$scope', 'RbsChange.Document', '$routeParams', function($rootScope, scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Rbs_User_User');

		scope.possessionStaticFilters = [
			{
				name: 'owner',
				parameters: { propertyName: 'owners', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.readingStaticFilters = [
			{
				name: 'reading',
				parameters: { propertyName: 'reader', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.newSubDocumentUrlParameters = { userId: $routeParams.id };

		scope.isCurrent = $rootScope.user.id === $routeParams.id;
	}]);
})();