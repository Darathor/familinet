/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryUserListController', ['$scope', function(scope) {
		scope.staticFilters = [
			{
				name: 'groups.realm',
				parameters: { propertyName: 'groups.realm', value: 'familinet', operator: 'eq' }
			}
		];
	}]);
})();