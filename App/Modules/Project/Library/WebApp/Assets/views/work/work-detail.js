/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryWorkDetailController', ['$scope', 'RbsChange.Document', '$routeParams', function(scope, Document, $routeParams) {
		Document.initDetailView(scope, 'Project_Library_Work');

		scope.publicationStaticFilters = [
			{
				name: 'works',
				parameters: { propertyName: 'works', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.contributionStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.possessionStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.readingStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'work', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.containerStaticFilters = [
			{
				name: 'work',
				parameters: { propertyName: 'compiledParts', value: $routeParams.id, operator: 'eq' }
			}
		];

		scope.newSubDocumentUrlParameters = { workId: $routeParams.id };
	}]);
})();