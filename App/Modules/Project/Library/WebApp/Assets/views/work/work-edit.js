/**
 * Copyright (C) 2019 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('LibraryWorkEditController', ['$scope', 'RbsChange.Document', 'projectLibraryImport',
		function(scope, Document, projectLibraryImport) {
			Document.initEditView(scope, 'Project_Library_Work');

			projectLibraryImport.initImportManager(scope, 'work', function(doc, data) {
				if (angular.isObject(data.work)) {
					return projectLibraryImport.applyImportedData(doc, data.work, {
							scalar: ['label', 'subtitle', 'originalLanguage', 'year', 'endYear', 'audience'],
							richText: ['description'],
							subDoc: [
								{ name: 'workGroups', check: 'id' },
								{ name: 'status', check: 'id', single: true },
								{ name: 'genres', check: 'id' },
								{ name: 'nationalities', check: 'id' },
								{ name: 'aliases', check: 'label' },
								{ name: 'links', check: 'url' }
							]
						}
					);
				}
			});
		}
	]);
})();