<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineCastingParser
 */
class AllocineCastingParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \str_replace('http://', 'https://', \explode('?', $url)[0]);
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$season = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'series-center-filter']])
			->node('span', [['class', '~', 'title']])->get()) ?: '';
		$season = \str_replace('Saison ', 'S', $season);

		$contributors = $this->getJsonData();
		foreach ($this->XPathParser->getElements($this->XPath->node('*', [['class', '~', 'section']])->get()) as $section)
		{
			$title = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'titlebar']])->get(), [], $section);
			if ($title)
			{
				foreach ($this->XPathParser->getElements($this->XPath->node('div')->node('div', [['class', '~', 'person-card']])
					->node('div', [['class', '~', 'meta']])->get(), $section) as $contributionNode)
				{
					$contribution = $this->parsePersonCard($contributionNode, $title, $contributors, $season);
					$this->parserData->addContribution($contribution);
				}
				foreach ($this->XPathParser->getElements($this->XPath->node('div', [['class', '~', 'md-table-row']])->get(), $section)
						 as $contributionNode)
				{
					$contribution = $this->parseTableRow($contributionNode, $title, $contributors, $season);
					$this->parserData->addContribution($contribution);
				}
				foreach ($this->XPathParser->getElements($this->XPath->node('div')->node('div', [['class', '~', 'md-table-row']])->get(), $section)
						 as $contributionNode)
				{
					$contribution = $this->parseTableRow($contributionNode, $title, $contributors, $season);
					$this->parserData->addContribution($contribution);
				}
			}
		}

		return true;
	}

	/**
	 * @return array
	 */
	protected function getJsonData()
	{
		$contributors = [];

		foreach ($this->XPathParser->getStrings($this->XPath->node('script', [['type', '=', 'application/ld+json']])->get()) as $jsonLd)
		{
			$object = \Change\Stdlib\JsonUtils::decode(\preg_replace('/([\]}]),\s*([\]}])/', '$1$2', $jsonLd));
			$actors = $object['actor'] ?? [];
			foreach ($actors as $actor)
			{
				if (isset($actor['url']))
				{
					$contributors[$actor['name']] = $actor['url'];
				}
			}
		}

		return $contributors;
	}

	/**
	 * @param \DOMElement $contributionNode
	 * @param string $title
	 * @param array $contributors
	 * @param string $season
	 * @return \Project\Library\WebSiteParsers\Models\ContributionModel|null
	 */
	protected function parsePersonCard(\DOMElement $contributionNode, string $title, array $contributors, string $season)
	{
		$personNode = $this->XPathParser->getElement($this->XPath->node('div', [['class', '~', 'meta-title']])->node('*')->get(), $contributionNode);
		if (!$personNode)
		{
			return null;
		}
		$label = $this->cleanupContent($personNode->nodeValue);
		$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		$contribution->setLabel($label);
		if ($personNode->tagName === 'a')
		{
			$contribution->setLink($personNode->getAttribute('href'));
		}
		elseif (\array_key_exists($label, $contributors))
		{
			$contribution->setLink($contributors[$label]);
		}

		if ($contribution)
		{
			// Role
			$role = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'meta-sub']])->get(), [], $contributionNode);
			if ($role && \preg_match('/Rôle : (.*)/u', $role, $matches))
			{
				$roleDetail = $this->cleanupContent($matches[1]);
				$contribution->addRole('Acteur', $roleDetail, $season);
			}
			else
			{
				$contribution->addRole($role ?: $title, $season);
			}
		}

		return $contribution;
	}

	/**
	 * @param \DOMElement $contributionNode
	 * @param string $title
	 * @param array $contributors
	 * @param string $season
	 * @return \Project\Library\WebSiteParsers\Models\ContributionModel
	 */
	protected function parseTableRow(\DOMElement $contributionNode, string $title, array $contributors, string $season)
	{
		$personNode = $this->XPathParser->getElement($this->XPath->node('*', [['class', '~', 'link']])->get(), $contributionNode);
		if (!$personNode)
		{
			return null;
		}
		$label = $this->cleanupContent($personNode->nodeValue);
		$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		$contribution->setLabel($label);
		if ($personNode->tagName === 'a')
		{
			$contribution->setLink($personNode->getAttribute('href'));
		}
		elseif (\array_key_exists($label, $contributors))
		{
			$contribution->setLink($contributors[$label]);
		}

		if ($title === 'Acteurs et actrices')
		{
			$role = 'Acteur';
			$detail = $this->XPathParser->getString($this->XPath->node('span', [['class', '~', 'light']])->get(), [], $contributionNode);
			$context =
				$season . $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'item-episodes']])->get(), [], $contributionNode);
			$context = \str_replace(['Episodes :', 'Episode :'], ' E', $context);
		}
		else
		{
			$role = $this->XPathParser->getString($this->XPath->node('span', [['class', '~', 'light']])->get(), [], $contributionNode) ?: $title;
			$detail = '';
			$context = $season;
		}
		$contribution->addRole($role, $detail, $context);

		return $contribution;
	}
}