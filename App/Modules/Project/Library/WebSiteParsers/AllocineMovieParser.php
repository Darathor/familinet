<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineMovieParser
 */
class AllocineMovieParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \str_replace('http://', 'https://', \explode('?', $url)[0]);
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Multiple languages: Les Orphelins de Huang Shi https://www.allocine.fr/film/fichefilm_gen_cfilm=123255.html
		// - Multiple nationalities: Pluie d'enfer https://www.allocine.fr/film/fichefilm_gen_cfilm=17509.html

		// JSON LD
		$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument));

		// Links.
		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);

		// Properties.
		$work->setStatus('ended');
		$work->setYear($this->getNumberInfo('Année de production'));
		$work->addNationalities($this->XPathParser->getStrings($this->XPath->node('*', [['class', '~', 'nationality']])->get()));
		$work->addAlias($this->getMainInfo('Titre original'));
		$this->setOriginalLanguage();

		// Attributes.
		$work->setAttribute('type_film', $this->getInfo('Type de film'));
		$work->setAttribute('budget', $this->getInfo('Budget'));
		$work->setAttribute('format_production', $this->getInfo('Format production'));
		$work->setAttribute('mode_couleur', $this->getInfo('Couleur'));
		$work->setAttribute('format_audio', $this->getInfo('Format audio'));
		$work->setAttribute('format_projection', $this->getInfo('Format de projection'));
		$work->setAttribute('visa', $this->getInfo('N° de Visa'));
		$work->setAttribute('date_sortie_VOD', $this->getDateInfo('Date de sortie VOD'));
		$work->setAttribute('date_sortie_DVD', $this->getDateInfo('Date de sortie DVD'));
		$work->setAttribute('date_sortie_BR', $this->getDateInfo('Date de sortie Blu-ray'));
		$work->setAttribute('date_reprise', $this->getDateInfo('Date de reprise'));

		$infos = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'meta-body-info']])->get());
		if ($infos)
		{
			[$date, $duree,] = \explode('/', $infos);
			$date = \trim(\str_replace('en salle', '', $date));
			$work->setAttribute('date_sortie_cinema', $this->XPathParser::applyFormat($date, $this->XPathParser::FORMAT_DATE, 'j F Y'));
			$work->setAttribute('duree_cinema', \trim($duree));
		}

		// Contributions.
		$this->setContributions();

		return true;
	}

	/**
	 * @param string $label
	 * @return string|null
	 */
	protected function getMainInfo(string $label)
	{
		return $this->XPathParser->getString($this->XPath
			->node('span', [['class', '~', 'light'], ['text', '=', $label]])
			->parent()->textNodes()->get(), ['-']);
	}

	/**
	 * @param string $label
	 * @return string|null
	 */
	protected function getInfo(string $label)
	{
		return $this->XPathParser->getString($this->XPath
			->node('span', [['class', '~', 'what'], ['text', '=', $label]])
			->parent()->node('*', [['class', '=', 'that']])->get(), ['-']);
	}

	/**
	 * @param string $label
	 * @return int
	 */
	protected function getNumberInfo(string $label)
	{
		return $this->XPathParser->getNumber($this->XPath
			->node('span', [['class', '~', 'what'], ['text', '=', $label]])
			->parent()->node('*', [['class', '=', 'that']])->get());
	}

	/**
	 * @param string $label
	 * @return string
	 */
	protected function getDateInfo(string $label)
	{
		return $this->XPathParser->getDate($this->XPath
			->node('span', [['class', '~', 'what'], ['text', '=', $label]])
			->parent()->node('*', [['class', '=', 'that']])->get());
	}

	protected function setOriginalLanguage()
	{
		$languages = $this->XPathParser->getString($this->XPath
			->node('span', [['class', '~', 'what'], ['text', '=', 'Langues']])
			->parent()->node('*', [['class', '=', 'that']])->get(), ['-']) ?: '';
		$this->parserData->getWork()->addOriginalLanguages(explode(', ', $languages));
	}

	protected function setContributions()
	{
		if (\preg_match('/.*\/film\/fichefilm_gen_cfilm=(\d+).html/', $this->url, $matches))
		{
			$url = $this->httpUri->getScheme() . '://' . $this->httpUri->getHost() . '/film/fichefilm-' . $matches[1] . '/casting/';

			$parserObject = new AllocineCastingParser($this->resolver, $url);

			/** @var \Project\Library\WebSiteParsers\Misc\ParserData $data */
			$data = $parserObject->parseData();
			$this->parserData->addContribution($data->getContributions());
		}
	}
}