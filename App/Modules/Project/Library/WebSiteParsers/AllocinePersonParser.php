<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocinePersonParser
 */
class AllocinePersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \str_replace('http://', 'https://', \explode('?', $url)[0]);
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Multiple nationalities: Alejandro Amenábar https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=24989.html
		// - Birth name: Alejandro Amenábar https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=24989.html
		// - Pseudo: Wood Harris https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=57115.html
		// - Death date: Bill McKinney https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=51054.html

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology('person');
		$contributor->setBirthDate($this->XPathParser->getDate($this->XPath
			->node('span', [['text', '=', 'Naissance']])->parent()->node('strong')->get(), null, 'j F Y'));
		$contributor->setDeathDate($this->XPathParser->getDate($this->XPath
			->node('span', [['text', '=', 'Décès']])->parent()->node('strong')->get(), null, 'j F Y'));

		$description = $this->XPathParser->getString(
			$this->XPath->node('div', [['text', '=', 'Biographie']])->parent()->node('div', [['class', '~', 'content-txt']])->get()
		);
		if ($description && !\Change\Stdlib\StringUtils::endsWith($description, 'Lire plus')) // Ignore truncated descriptions.
		{
			$contributor->appendToDescription($description);
		}

		$this->setDates();
		$this->setLabel();
		$this->setNationalities();
		$this->setAliases();

		return true;
	}

	protected function setDates()
	{
		$birthFilter = $this->XPath->node('span', [['text', '=', 'Naissance']])->parent()->node('strong')->get();
		$birthDate = $this->XPathParser->getDate($birthFilter, null, 'j F Y');
		if ($birthDate)
		{
			$this->parserData->getContributor()->setBirthDate($birthDate);
		}
		else
		{
			$birth = $this->XPathParser->getString($birthFilter);
			if ($birth && \preg_match('/\d{1,4}/', $birth))
			{
				$this->parserData->getContributor()->setBirthYear((int)$birth);
			}
		}

		$deathFilter = $this->XPath->node('span', [['text', '=', 'Naissance']])->parent()->node('strong')->get();
		$deathDate = $this->XPathParser->getDate($deathFilter, null, 'j F Y');
		if ($deathDate)
		{
			$this->parserData->getContributor()->setBirthDate($deathDate);
		}
		else
		{
			$death = $this->XPathParser->getString($deathFilter);
			if ($death && \preg_match('/\d{1,4}/', $death))
			{
				$this->parserData->getContributor()->setBirthYear((int)$death);
			}
		}
	}

	protected function setLabel()
	{
		$contributor = $this->parserData->getContributor();
		$label = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'titlebar-page']])
			->node('div', [['class', '~', 'titlebar-title']])->get());
		if (\preg_match('/(.+) \([IVX]+\)/', $label, $matches))
		{
			$contributor->setLabel($matches[1]);
			$contributor->addAlias($label, false, $this->httpUri->getHost());
		}
		else
		{
			$contributor->setLabel($label);
		}
	}

	protected function setNationalities()
	{
		$nationalities = $this->XPathParser->getString($this->XPath->node('span', [['text', '=', 'Nationalité']])->parent()->textNodes()->get());
		if (!$nationalities)
		{
			$nationalities = $this->XPathParser->getString($this->XPath->node('span', [['text', '=', 'Nationalités']])->parent()->textNodes()->get());
		}
		if ($nationalities)
		{
			$contributor = $this->parserData->getContributor();
			foreach (\explode(',', $nationalities) as $nationality)
			{
				$nationality = $this->cleanupContent($nationality);
				if ($nationality !== 'Indéfini' && $nationality !== 'Indéfinie')
				{
					$contributor->addNationalities($nationality);
				}
			}
		}
	}

	protected function setAliases()
	{
		$contributor = $this->parserData->getContributor();

		// Birth Name.
		$birthName = $this->XPathParser->getString($this->XPath->node('span', [['text', '=', 'Nom de naissance']])->parent()->node('h2')->get());
		$contributor->addAlias($birthName);

		// Pseudos.
		$pseudo = $this->XPathParser->getString($this->XPath->node('span', [['text', '=', 'Pseudo']])->parent()->node('h2')->get());
		$contributor->addAlias($pseudo, false);
		$pseudos = $this->XPathParser->getStrings($this->XPath->node('span', [['text', '=', 'Pseudos']])->parent()->node('h2')->get());
		foreach ($pseudos as $pseudo)
		{
			$contributor->addAlias($pseudo, false);
		}
	}
}