<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\AllocineSerieParser
 */
class AllocineSerieParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \str_replace('http://', 'https://', \explode('?', $url)[0]);
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Ended + multiple nationalities: Tru Calling https://www.allocine.fr/series/ficheserie_gen_cserie=144.html
		// - Canceled: Frequency https://www.allocine.fr/series/ficheserie_gen_cserie=18468.html

		// JSON LD
		$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument));

		// Links.
		$this->parserData->getWork()->addLinkByUrl($this->url);

		$this->setStatus();
		$this->setMetaInfos();
		$this->setNationalities();
		$this->setAliases();
		$this->setContributions();

		return true;
	}

	protected function setMetaInfos()
	{
		$work = $this->parserData->getWork();
		$value = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'meta-body-info']])->get()) ?: '';
		[$dates, $episodeDuration,] = \explode('/', $value);
		if (\preg_match('/(\d{4}) - (\d{4})/', $dates, $matches))
		{
			$work->setYear($matches[1]);
			$work->setEndYear($matches[2]);
		}
		elseif (\preg_match('/(\d{4})/', $dates, $matches))
		{
			$work->setYear($matches[1]);
		}
		if ($episodeDuration)
		{
			$work->setAttribute('duree_episode', \trim($episodeDuration));
		}
	}

	protected function setStatus()
	{
		$work = $this->parserData->getWork();
		$value = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'label-status']])->get());
		switch ($value)
		{
			case 'En cours':
				$work->setStatus('ongoing');
				break;
			case 'Terminée':
				$work->setStatus('ended');
				break;
			case 'Annulée':
				$work->setStatus('canceled');
				break;
		}
	}

	protected function setNationalities()
	{
		foreach ($this->XPathParser->getStrings($this->XPath
			->node('*', [['class', '~', 'meta-body-nationality']])
			->node('*', [['class', '~', 'nationality']])->get()) as $nationality)
		{
			if ($nationality !== 'Indéfini')
			{
				$this->parserData->getWork()->addNationalities($nationality);
			}
		}
	}

	protected function setAliases()
	{
		$alias = $this->XPathParser->getString($this->XPath
			->node('div', [['class', '~', 'meta-body-original-title']])
			->node('strong')->get());
		$this->parserData->getWork()->addAlias($alias);
	}

	protected function setContributions()
	{
		$urls = [];
		/** @var \DOMElement $node */
		foreach ($this->XPathParser->getElements($this->XPath
			->node('section', [['id', '=', 'synopsis-details']])
			->node('a', [['class', '~', 'end-section-link']])->get()) as $node)
		{
			$url = $node->getAttribute('href');
			if (\preg_match('/(\/series\/ficheserie-\d*\/)(saison-\d*\/)/', $url, $matches))
			{
				$urls[] = $this->httpUri->getScheme() . '://' . $this->httpUri->getHost() . $matches[1] . 'casting/' . $matches[2];
			}
		}
		foreach ($urls as $url)
		{
			$parserObject = new AllocineCastingParser($this->resolver, $url);

			/** @var \Project\Library\WebSiteParsers\Misc\ParserData $data */
			$data = $parserObject->parseData();
			$this->parserData->addContribution($data->getContributions());
		}
	}
}