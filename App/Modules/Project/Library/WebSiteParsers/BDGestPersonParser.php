<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\BDGestPersonParser
 */
class BDGestPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \explode('?', $url)[0];
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Example: https://www.bedetheque.com/auteur-16339-BD-Bit.html
		// With death date, alias with a coma: https://www.bedetheque.com/auteur-2132-BD-Lee-Stan.html

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology('person');
		$this->setLabelAndAliases();
		$this->setDates();

		// Nationality.
		$country = $this->XPathParser->getString($this->XPath->node('span', [['class', '=', 'pays-auteur']])->get());
		if ($country)
		{
			$country = \mb_convert_case(\Change\StdLib\StringUtils::toLower(\str_replace(['(', ')'], '', $country)), \MB_CASE_TITLE, 'UTF-8');
			$contributor->addNationalities($country);
		}

		// Website.
		$url = $this->XPathParser->getHref($this->XPath->node('label', [['text', '=', 'Site internet :']])->parent()->node('a')->get());
		if ($url)
		{
			$contributor->addLink('Site officiel', $url);
		}

		return true;
	}

	protected function fixName($name)
	{
		$name = \explode("\n", $name)[0];
		if ($name && \strpos($name, ', '))
		{
			$name = \implode(' ', \array_map('\trim', \array_reverse(\explode(', ', $name))));
		}
		return $name;
	}

	protected function setLabelAndAliases()
	{
		$label = $this->fixName($this->XPathParser->getString($this->XPath->node('*', [['class', '=', 'auteur-nom']])->get()));
		$this->parserData->getContributor()->setLabel($label);

		$lastName = $this->XPathParser->getString($this->XPath->node('label', [['text', '=', 'Nom :']])->parent()->node('span')->get());
		$firstName = $this->XPathParser->getString($this->XPath->node('label', [['text', '=', 'Prénom :']])->parent()->node('span')->get());
		if ($lastName)
		{
			$fullName = ($firstName ? ($firstName . ' ') : '') . $lastName;
			if ($fullName !== $label)
			{
				$this->parserData->getContributor()->addAlias($this->fixName($fullName));
			}
		}

		$alias = $this->XPathParser->getString($this->XPath->node('label', [['text', '=', 'Pseudo :']])->parent()->textNodes()->get());
		if ($alias)
		{
			$alias = $this->fixName($alias);
			if ($alias !== $label)
			{
				$this->parserData->getContributor()->addAlias($this->fixName($alias), false);
			}
		}
	}

	protected function setDates()
	{
		$birthFilter = $this->XPath->node('label', [['text', '=', 'Naissance :']])->parent()->textNodes()->get();
		$birth = $this->XPathParser->getString($birthFilter);
		if ($birth && \preg_match('/^\d{1,4}$/', $birth))
		{
			$this->parserData->getContributor()->setBirthYear((int)$birth);
		}
		elseif ($birth)
		{
			$this->parserData->getContributor()->setBirthDate($this->XPathParser->getDate($birthFilter, null, '\l\e j/m/Y'));
		}

		$deathFilter = $this->XPath->node('label', [['text', '=', 'Décès :']])->parent()->textNodes()->get();
		$death = $this->XPathParser->getString($deathFilter);
		if ($death && \preg_match('/^\d{1,4}$/', $death))
		{
			$this->parserData->getContributor()->setDeathYear((int)$death);
		}
		elseif ($death)
		{
			$this->parserData->getContributor()->setDeathDate($this->XPathParser->getDate($deathFilter, null, '\l\e j/m/Y'));
		}
	}
}