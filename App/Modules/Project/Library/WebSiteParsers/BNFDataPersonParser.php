<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\BNFDataPersonParser
 */
class BNFDataPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	protected function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Birth/death dates: https://data.bnf.fr/fr/11922661/suzanne_rosset/
		// - One alias: https://data.bnf.fr/fr/16612931/marie_favereau_doumenjou/
		// - Several aliases: https://data.bnf.fr/fr/15093103/hye-ri_baek/

		$this->parseJSONLD();
		$this->parseJSON();

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);

		return true;
	}

	private function parseJSONLD()
	{
		$jsonLdHref = $this->XPathParser->getString($this->XPath->node('a', [['id', '=', 'download-rdf-jsonld']])->attribute('href')->get());
		$jsonLdContent = $this->loadContent($jsonLdHref);
		if (!$jsonLdContent)
		{
			return;
		}

		$jsonData = \Change\Stdlib\JsonUtils::decode($jsonLdContent);

		$aliases = [];
		foreach ($jsonData['@graph'] ?? [] as $item)
		{
			$type = $item['@type'] ?? null;
			if ($type === 'foaf:Person')
			{
				$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseArray($item));
			}
			elseif ($type === 'skos:Concept')
			{
				$aliasesData = $item['skos:altLabel'] ?? [];
				if (\array_key_exists('@value', $aliasesData))
				{
					$aliases[] = $aliasesData['@value'];
				}
				else
				{
					foreach ($aliasesData as $aliasData)
					{
						$aliases[] = $aliasData['@value'];
					}
				}
			}
		}

		$contributor = $this->parserData->getContributor();
		foreach ($aliases as $alias)
		{
			$contributor->addAlias($alias, false);
		}
	}

	private function parseJSON()
	{
		$jsonHref = $this->XPathParser->getString($this->XPath->node('a', [['id', '=', 'download-json']])->attribute('href')->get());
		$jsonContent = $this->loadContent($jsonHref);
		if (!$jsonContent)
		{
			return;
		}

		$jsonData = \Change\Stdlib\JsonUtils::decode($jsonContent)[0] ?? [];

		$contributor = $this->parserData->getContributor();
		$contributor->addNationalities($jsonData['nationality'] ?? []);
		$contributor->appendToDescription(\implode("\n\n", $jsonData['notes'] ?? []));

		if (!$contributor->getLabel())
		{
			$contributor->setLabel(\trim(\preg_replace('/\(.*\)/', '', $jsonData['label'])));
		}

		if (!$contributor->getBirthDate())
		{
			$birthDate = $jsonData['birthdate'] ?? null;
			if ($birthDate)
			{
				if (\preg_match('/^\d{1,4}$/', $birthDate))
				{
					$contributor->setBirthYear((int)$birthDate);
				}
				else
				{
					$contributor->setBirthDate($birthDate);
				}
			}
		}

		if (!$contributor->getDeathDate())
		{
			$deathDate = $jsonData['deathdate'] ?? null;
			if ($deathDate)
			{
				if (\preg_match('/^\d{1,4}$/', $deathDate))
				{
					$contributor->setDeathYear((int)$deathDate);
				}
				else
				{
					$contributor->setDeathDate($deathDate);
				}
			}
		}
	}
}