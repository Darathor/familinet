<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\BabelioPersonParser
 */
class BabelioPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $content
	 * @return string
	 */
	protected function fixFileContent(string $content)
	{
		$content = \str_replace(
			['<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />', '<meta charset="iso-8859-1">'], '', $content
		);
		return \iconv('WINDOWS-1252', 'UTF-8', $content);
	}

	/**
	 * @return boolean
	 */
	protected function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Birth/death dates + special characters: https://www.babelio.com/auteur/Charles-Baudelaire/2184
		// - Nationality "États-Unis": https://www.babelio.com/auteur/Joss-Whedon/46800

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology('person');
		$contributor->setLabel($this->XPathParser->getString($this->XPath->node('*', [['itemprop', '=', 'name']])->node('a')->get()));

		$description = $this->XPathParser->getString($this->XPath->node('*', [['id', '=', 'd_bio']])->get());
		if ($description)
		{
			$description = \preg_replace('/^Biographie :/', '', $description, 1);
			$contributor->appendToDescription($description);
		}

		$this->setDates();
		$this->setNationalities();

		return true;
	}

	/**
	 * @param string $dateText
	 * @return string
	 */
	protected function replaceMonth(string $dateText): string
	{
		$frMonths = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
		$replacements = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
		return \str_replace($frMonths, $replacements, $dateText);
	}

	protected function setDates()
	{
		$div = $this->XPathParser->getNode($this->XPath->node('div', [['class', '=', 'livre_resume']])->get());
		if ($div)
		{
			foreach ($div->childNodes as $node)
			{
				if ($node instanceof \DOMElement)
				{
					continue;
				}

				if ($node instanceof \DOMText)
				{
					$value = $this->cleanupContent($node->nodeValue);
					if (\preg_match('/Né\(e\)\s*(à|le)?\s*:/u', $value, $matches))
					{
						$dateNode = $node->nextSibling;
						if ($dateNode instanceof \DOMElement && $dateNode->tagName === 'span')
						{
							$dateText = $this->replaceMonth(\trim($dateNode->textContent));
							if (\preg_match('/\d{1,2} [a-zA-Z]{3,4} \d{4}/', $dateText))
							{
								// The "!" in the format forces hours, minutes and seconds to 0...
								$this->parserData->getContributor()->setBirthDate(\DateTime::createFromFormat('!j M Y', $dateText));
							}
							elseif (\preg_match('/\d{1,2}\/\d{1,2}\/\d{4}/', $dateText))
							{
								// The "!" in the format forces hours, minutes and seconds to 0...
								$this->parserData->getContributor()->setBirthDate(\DateTime::createFromFormat('!j/m/Y', $dateText));
							}
							elseif (\preg_match('/\d{4}/', $dateText, $matches))
							{
								$this->parserData->getContributor()->setBirthYear((int)$matches[0]);
							}
						}
					}
					elseif (\preg_match('/Mort\(e\)\s*(à|le)?\s*:/u', $value, $matches))
					{
						$dateNode = $node->nextSibling;
						if ($dateNode instanceof \DOMElement && $dateNode->tagName === 'span')
						{
							$dateText = $this->replaceMonth(\trim($dateNode->textContent));
							if (\preg_match('/\d{1,2} [a-zA-Z]{3,4} \d{4}/', $dateText))
							{
								// The "!" in the format forces hours, minutes and seconds to 0...
								$this->parserData->getContributor()->setDeathDate(\DateTime::createFromFormat('!j M Y', $dateText));
							}
							elseif (\preg_match('/\d{1,2}\/\d{1,2}\/\d{4}/', $dateText))
							{
								// The "!" in the format forces hours, minutes and seconds to 0...
								$this->parserData->getContributor()->setDeathDate(\DateTime::createFromFormat('!j/m/Y', $dateText));
							}
							elseif (\preg_match('/\d{4}/', $dateText, $matches))
							{
								$this->parserData->getContributor()->setDeathYear((int)$matches[0]);
							}
						}
					}
				}
			}
		}
	}

	protected function setNationalities()
	{
		$div = $this->XPathParser->getNode($this->XPath->node('div', [['class', '=', 'livre_resume']])->get());
		if ($div)
		{
			foreach ($div->childNodes as $node)
			{
				if ($node instanceof \DOMElement)
				{
					continue;
				}

				$value = $this->cleanupContent($node->nodeValue);
				if (\preg_match('/Nationalité\s*:\s*([\w \-]+)/u', $value, $matches))
				{
					$this->parserData->getContributor()->addNationalities(\Change\Stdlib\StringUtils::ucfirst($this->cleanupContent($matches[1])));
				}
			}
		}
	}
}