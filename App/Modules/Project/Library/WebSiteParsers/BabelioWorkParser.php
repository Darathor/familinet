<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\BabelioWorkParser
 */
class BabelioWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $content
	 * @return string
	 */
	protected function fixFileContent(string $content)
	{
		$content = \str_replace(
			['<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />', '<meta charset="iso-8859-1">'], '', $content
		);
		return \iconv('WINDOWS-1252', 'UTF-8', $content);
	}

	/**
	 * @return boolean
	 */
	protected function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Multiple authors + EAN + pages + publisher + date: https://www.babelio.com/livres/Daudet-Ma-ville-interdite/1306332
		// - Translator: https://www.babelio.com/livres/Martin-Le-Trone-de-fer-tome-11--Les-sables-de-Dorne/4016

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setLabel($this->XPathParser->getString($this->XPath->node('*', [['itemprop', '=', 'name']])->node('a')->get()));

		$publication = $this->parserData->getPublication();
		$publication->setTypology('livre');

		$this->setContributors();
		$this->setMetaData();
		return true;
	}

	protected function setMetaData()
	{
		$div = $this->XPathParser->getNode($this->XPath->node('div', [['class', '~', 'livre_refs']])->get());
		if ($div)
		{
			foreach ($div->childNodes as $node)
			{
				if ($node instanceof \DOMElement)
				{
					// Publisher.
					if ($node->localName === 'a')
					{
						$this->parserData->getPublication()->setPublisher($node->textContent);
					}
					continue;
				}

				if ($node instanceof \DOMText)
				{
					$value = $this->cleanupContent($node->nodeValue);

					// Publication date.
					if (\preg_match('/\((\d{1,2}\/\d{1,2}\/\d{4})\)/u', $value, $matches))
					{
						// The "!" in the format forces hours, minutes and seconds to 0...
						$date = \DateTime::createFromFormat('!j/m/Y', $matches[1]);
						$this->parserData->getPublication()->setYear($date->format('Y'));
						$this->parserData->getPublication()->setAttribute('date_publication', $date->format(\DateTimeInterface::ATOM));
					}
					// EAN.
					elseif (\preg_match('/EAN : (\d+)/u', $value, $matches))
					{
						$this->parserData->getPublication()->setAttribute('ean', $matches[1]);
					}
					// Pages.
					elseif (\preg_match('/(\d+) pages/u', $value, $matches))
					{
						$this->parserData->getPublication()->setAttribute('nb_pages', $matches[1]);
					}
					// Publisher.
					elseif (\preg_match('/Éditeur :/u', $value, $matches))
					{
						$this->parserData->getPublication()->setPublisher($node->nextSibling->textContent);
					}
				}
			}
		}
	}

	protected function setContributors()
	{
		foreach ($this->XPathParser->getElements($this->XPath->node('*', [['itemprop', '=', 'author']])->node('a')->get()) as $element)
		{
			$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
			$contribution->setLabel($this->cleanupContent($element->textContent));
			$contribution->setLink($element->getAttribute('href'));
			$contribution->addRole('Auteur');

			$this->parserData->addContribution($contribution);
		}

		foreach ($this->XPathParser->getElements($this->XPath->node('a', [['class', '~', 'livre_collabs']])->get()) as $element)
		{
			$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
			$contribution->setLabel($this->cleanupContent($element->childNodes[0]->textContent));
			$contribution->setLink($element->getAttribute('href'));

			$roleElement = $element->childNodes[1];
			if ($roleElement instanceof \DOMElement && \preg_match('/\((\w+)\)/u', $roleElement->textContent, $matches))
			{
				$contribution->addRole($matches[1]);
			}

			$this->parserData->addContribution($contribution);
		}
	}
}