<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\ComicVinePersonParser
 */
class ComicVinePersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	protected function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Birth/death dates: https://comicvine.gamespot.com/justin-ponsor/4040-2066/
		// - Alias: https://comicvine.gamespot.com/stan-lee/4040-40467/

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology('person');
		$contributor->setLabel($this->XPathParser->getString($this->XPath->node('*', [['data-field', '=', 'name']])->get()));
		$contributor
			->addNationalities(\explode(',', $this->XPathParser->getString($this->XPath->node('*', [['data-field', '=', 'country']])->get())));

		$this->setDates();
		$this->setSex();

		foreach (\explode("\n", $this->XPathParser->getString($this->XPath->node('*', [['data-field', '=', 'aliases']])->get())) as $alias)
		{
			$contributor->addAlias(\trim($alias), false);
		}

		$this->setLinks();

		return true;
	}

	protected function setDates()
	{
		$birthFilter = $this->XPath->node('*', [['data-field', '=', 'birthday']])->get();
		$birth = $this->XPathParser->getString($birthFilter);
		if ($birth && \preg_match('/^\d{1,4}$/', $birth))
		{
			$this->parserData->getContributor()->setBirthYear((int)$birth);
		}
		elseif ($birth)
		{
			$this->parserData->getContributor()->setBirthDate($this->XPathParser->getDate($birthFilter, null, 'M j, Y'));
		}

		$deathFilter = $this->XPath->node('*', [['data-field', '=', 'death']])->get();
		$death = $this->XPathParser->getString($deathFilter);
		if ($death && \preg_match('/^\d{1,4}$/', $death))
		{
			$this->parserData->getContributor()->setDeathYear((int)$death);
		}
		elseif ($death)
		{
			$this->parserData->getContributor()->setDeathDate($this->XPathParser->getDate($deathFilter, null, 'M j, Y'));
		}
	}

	protected function setSex()
	{
		$contributor = $this->parserData->getContributor();
		$sex = $this->XPathParser->getString($this->XPath->node('*', [['data-field', '=', 'gender']])->get());
		switch ($sex)
		{
			case 'Female':
				$sex = 'f';
				break;
			case 'Male':
				$sex = 'h';
				break;
			default:
				$sex = null;
				break;
		}
		$contributor->setAttribute('sex', $sex);
	}

	protected function setLinks()
	{
		$contributor = $this->parserData->getContributor();

		$website = $this->XPathParser->getHref($this->XPath->node('*', [['data-field', '=', 'website']])->node('a')->get());
		if ($website)
		{
			$contributor->addLink('Site web', $website);
		}
	}
}