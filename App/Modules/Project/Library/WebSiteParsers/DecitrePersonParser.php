<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\DecitrePersonParser
 */
class DecitrePersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	protected function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Biography: Musso https://www.decitre.fr/auteur/308433/Guillaume+Musso

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology('person');
		$contributor->setLabel($this->XPathParser->getString($this->XPath
			->node('div', [['class', '~', 'nbr_result']])->node('h2')->get()));
		$contributor->appendToDescription($this->XPathParser->getString($this->XPath
			->node('div', [['class', '~', 'resume_result_search']])->node('p')->get()));

		return true;
	}
}