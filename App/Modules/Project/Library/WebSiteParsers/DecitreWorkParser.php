<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\DecitreWorkParser
 */
class DecitreWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	protected $isSerie = false;

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - Multiple authors: https://www.decitre.fr/livres/ma-ville-interdite-9782862666143.html
		// - Translator: https://www.decitre.fr/livres/surprends-moi-9782714479228.html
		// - The 2 + colorist: https://www.decitre.fr/livres/infamous-iron-man-tome-1-redemption-9782809474923.html
		// - Multiple annotators: https://www.decitre.fr/livres/cyrano-de-bergerac-9782081408630.html
		// - Serie: https://www.decitre.fr/livres/la-reine-faucon-tome-1-reine-des-batailles-9791028110727.html

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$this->setLabels();
		$work->appendToDescription($this->XPathParser->getString($this->XPath
			->node('div', [['id', '=', 'resume']])->node('div', [['class', '~', 'content']])->get()), 'Résumé');

		$publication = $this->parserData->getPublication();
		$publication->setPublisher($this->getInfo('Editeur'));
		$publication->setCollection($this->getInfo('Collection'));
		$publication->setAttribute('date_publication', $this->getDateInfo('Date de parution'));
		$publication->setAttribute('isbn', $this->getInfo('ISBN'));
		$publication->setAttribute('ean', $this->getInfo('EAN'));
		$publication->setAttribute('nb_pages', $this->getNumberInfo('Nb. de pages'));
		$publication->setAttribute('poids', $this->getInfo('Poids'));
		$publication->setAttribute('dimensions', $this->getInfo('Dimensions'));
		$publication->setAttribute('format', $this->getInfo('Format'));

		if ($this->isSerie)
		{
			$publication->setTypology('serie_livres');
		}
		else
		{
			$work->setStatus('ended');
			$publication->setTypology('livre');
			$publication->setStatus('ended');
			$publication->setOneShot(true);
		}

		$this->setContributions();

		return true;
	}

	protected function setLabels()
	{
		$title = $this->XPathParser->getString($this->XPath->node('h1', [['class', '~', 'product-title']])->textNodes()->get());
		if ($title)
		{
			if (\preg_match('/.*( Tome \d+)/', $title, $matches))
			{
				$this->isSerie = true;
				$title = \str_replace($matches[1], '', $title);
			}
			else
			{
				$this->parserData->getWork()->setSubtitle($this->XPathParser->getString($this->XPath
					->node('h2', [['class', '~', 'product-subtitle']])->get()));
			}
			$this->parserData->getWork()->setLabel($title);
		}
	}

	/**
	 * @param string $label
	 * @return string
	 */
	protected function getInfo(string $label)
	{
		return $this->XPathParser->getString($this->XPath
			->node('div', [['class', '~', 'name'], ['text', '=', $label]])
			->parent()->node('*', [['class', '=', 'value']])->get());
	}

	/**
	 * @param string $label
	 * @return int|null
	 */
	protected function getNumberInfo(string $label)
	{
		$value = $this->getInfo($label);
		$nb = \preg_replace('/\D/', '', $value);
		return $nb ? (int)$nb : null;
	}

	/**
	 * @param string $label
	 * @return string
	 */
	protected function getDateInfo(string $label)
	{
		return $this->XPathParser->getDate($this->XPath
			->node('div', [['class', '~', 'name'], ['text', '=', $label]])
			->parent()->node('*', [['class', '=', 'value']])->get());
	}

	protected function setContributions()
	{
		// Authors.
		foreach ($this->XPathParser->getElements($this->XPath->node('div', [['class', '~', 'authors-container']])
			->node('div', [['class', '~', 'authors--main']])->node('a', [['class', '~', 'author--main']])->get()) as $a)
		{
			$contribution = $this->parserData->initContribution($this->cleanupContent($a->nodeValue), $a->getAttribute('href'));
			if ($contribution)
			{
				$contribution->addRole('Auteur');
			}
		}

		// Other contributions.
		$contribution = null;
		foreach ($this->XPathParser->getElements($this->XPath->node('div', [['class', '~', 'authors-container']])
			->node('div', [['class', '~', 'authors'], ['class', '!~', 'authors--main']])->get()) as $div)
		{
			/** @var \DOMNode $child */
			foreach ($div->childNodes as $child)
			{
				if ($child instanceof \DOMElement)
				{
					switch ($child->tagName)
					{
						case 'h3':
							$a = $this->XPathParser->getElement($this->XPath->node('a')->get(), $child);
							if ($a instanceof \DOMElement)
							{
								$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
								$contribution->setLabel($this->cleanupContent($a->nodeValue));
								$contribution->setLink($a->getAttribute('href'));
							}
							break;
						case 'h4':
							if ($contribution instanceof \Project\Library\WebSiteParsers\Models\ContributionModel)
							{
								$contribution->addRole(\trim($this->cleanupContent($child->nodeValue), '()'));
							}
							break;
					}
				}
				elseif ($this->cleanupContent($child->nodeValue) === ',')
				{
					$this->parserData->addContribution($contribution);
					$contribution = null;
				}
			}
		}
		// Add the last contribution.
		$this->parserData->addContribution($contribution);
	}
}