<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\GogWorkParser
 */
class GogWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $content
	 * @return string
	 */
	protected function fixFileContent(string $content)
	{
		return \str_replace(['&#039;'], ['\''], $content);
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - https://www.gog.com/fr/game/baldurs_gate_enhanced_edition

		// JSON LD
		$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument));

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setTypology('jeu_video');
		$work->setStatus('ended');
		$work->setEndYear($work->getYear());

		$publication = $this->parserData->getPublication();
		$publication->setStatus('ended');
		$publication->setOneShot(true);
		$publication->setYear($work->getYear());
		$publication->setEndYear($work->getEndYear());
		$publication->setTypology('support_logiciel');
		$publication->setAttribute('type_support_logiciel', 'Numérique');

		$this->setPublicationLanguages();

		return true;
	}

	protected function setPublicationLanguages()
	{
		// TODO
//		foreach ($this->XPathParser->getNodes($this->XPath->node('div', [['class', '~', 'details__languages-row']])->get()) as $node)
//		{
//			$languageName = $this->XPathParser->getString(
//				$this->XPath->node('div', [['class', '~', 'details__languages-row--language-name']])->textNodes()->get(), [], $node
//			);
//			$this->parseData()->getPublication()->
//		}
	}
}