<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\ImdbPersonParser
 */
class ImdbPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \explode('?', $url)[0];
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// JSON LD
		$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument));

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->unsetDescription(); // Incomplete and in english...

		$this->setDates();

		// Disambiguation suffix.
		// Ex: https://www.imdb.com/name/nm4449702/
		$parts = $this->XPathParser->getStrings($this->XPath->node('h1')->node('span')->get());
		$suffix = $parts[1] ?? null;
		if ($suffix && $suffix !== '(I)' && \preg_match('/\([A-Z]+\)/u', $suffix))
		{
			$contributor->addAlias($contributor->getLabel() . ' ' . $suffix, false, $this->httpUri->getHost());
		}

		return true;
	}

	protected function setDates()
	{
		$contributor = $this->parserData->getContributor();

		if (!$contributor->getBirthDate())
		{
			$birthFilter = $this->XPath->node('span', [['text', '=', 'Born']])->followings()->get();
			$birthDate = $this->XPathParser->getDate($birthFilter, null, 'F j, Y');
			if ($birthDate)
			{
				$contributor->setBirthDate($birthDate);
			}
			else
			{
				$birth = $this->XPathParser->getString($birthFilter);
				if ($birth && \preg_match('/^\d{1,4}$/', $birth))
				{
					$contributor->setBirthYear((int)$birth);
				}
			}
		}

		if (!$contributor->getDeathDate())
		{
			$deathFilter = $this->XPath->node('span', [['text', '=', 'Died']])->followings()->get();
			$deathDate = $this->XPathParser->getDate($deathFilter, null, 'F j, Y');
			if ($deathDate)
			{
				$contributor->setDeathDate($deathDate);
			}
			else
			{
				$death = $this->XPathParser->getString($deathFilter);
				if ($death && \preg_match('/^\d{1,4}$/', $death))
				{
					$contributor->setDeathYear((int)$death);
				}
			}
		}
	}
}