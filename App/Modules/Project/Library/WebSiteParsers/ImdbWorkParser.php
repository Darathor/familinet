<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\ImdbWorkParser
 */
class ImdbWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		return \explode('?', $url)[0];
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// JSON LD
		$this->parserData->init(\Project\Library\WebSiteParsers\JsonLd\SchemaParser::parseAll($this->DOMDocument));

		$work = $this->parserData->getWork();
		$work->unsetDescription(); // Incomplete and in english...

		// Links
		$work->addLinkByUrl($this->url);

		// Nationalities
		$work->addNationalities($this->XPathParser->getStrings($this->XPath
			->node('*', [['text', '=', 'Country of origin']])->parent()->node('a')->get()));
		$work->addNationalities($this->XPathParser->getStrings($this->XPath
			->node('*', [['text', '=', 'Countries of origin']])->parent()->node('a')->get()));

		// Original label VS label (to do after nationalities initialization
		$label1 = $work->getLabel();
		$label2 = $this->XPathParser->getString($this->XPath->node('h1')->textNodes()->get());
		if ($label2 && $label2 !== $label1)
		{
			$work->addAlias($label1);
			$work->setLabel($label2);
		}

		// Original language
		$work->addOriginalLanguages($this->XPathParser->getStrings($this->XPath
			->node('*', [['text', '=', 'Language']])->parent()->node('a')->get()));
		$work->addOriginalLanguages($this->XPathParser->getStrings($this->XPath
			->node('*', [['text', '=', 'Languages']])->parent()->node('a')->get()));

		if ($work->getTypology() === 'serietv')
		{
			$this->parseTVSeries();
		}
		else
		{
			$this->parseMovie();
		}

		// TODO: parse contributors

		return true;
	}

	protected function parseTVSeries()
	{
		$work = $this->parserData->getWork();

		// Begin and end years and status
		$release = $this->XPathParser->getString($this->XPath->node('a', [['href', '~', 'releaseinfo']])->get()) ?: '';
		if (\preg_match('/(\d*)–(\d*)/u', $release, $matches))
		{
			if (\is_numeric($matches[1]))
			{
				$work->setYear($matches[1]);
			}
			if (\is_numeric($matches[2]))
			{
				$work->setEndYear($matches[2]);
				$work->setStatus('ended');
			}
			else
			{
				$work->setStatus('ongoing');
			}
		}

		// Episode duration
		$work->setAttribute('duree_episode', $this->resolveDuration());
	}

	protected function parseMovie()
	{
		$work = $this->parserData->getWork();

		// Status
		$work->setStatus('ended');

		// Duration
		$work->setAttribute('duree_cinema', $this->resolveDuration());
	}

	protected function resolveDuration(): ?string
	{
		$duration = null;

		foreach ($this->XPathParser->getStrings($this->XPath->node('li', [['role', '=', 'presentation']])->get()) as $value)
		{
			$value = \trim($value);
			if (\preg_match('/^\d ?(h|min)/i', $value))
			{
				$duration = $value;
			}
		}

		if (!$duration)
		{
			$duration = $this->XPathParser->getString(
				$this->XPath->node('*', [['text', '=', 'Runtime']])->parent()->node('*', [['role', '=', 'presentation']])->get()
			);
		}

		if (!$duration)
		{
			$duration = $this->XPathParser->getString(
				$this->XPath->node('*', [['data-testid', '=', 'title-techspec_runtime']])->node('div')->get()
			);
		}

		// Normalize format to 'Xh Xmin'.
		if ($duration)
		{
			$duration = \preg_replace('/(\d) ?heures?/', '$1h', $duration);
			$duration = \preg_replace('/(\d) h/', '$1h', $duration);
			$duration = \preg_replace('/(\d) ?minutes?/', '$1m', $duration);
			$duration = \preg_replace('/(\d) ?min/', '$1m', $duration);
			$duration = \preg_replace('/(\d) ?m/', '$1min', $duration);
		}
		return $duration;
	}
}