<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\CreativeWorkParser
 */
class CreativeWorkParser extends \Project\Library\WebSiteParsers\JsonLd\SchemaParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\AbstractModel[]
	 */
	protected function doParse(array $jsonData)
	{
		$models = [];

		$work = $this->doParseWork($jsonData);
		if ($work)
		{
			$models[] = $work;
		}

		// 'actor' is not a property of CreativeWork but is of both Movie and TVSeries
		if (isset($jsonData['actor']))
		{
			foreach ($jsonData['actor'] as $actor)
			{
				$contribution = \Project\Library\WebSiteParsers\Models\ContributionModel::init($actor['name'] ?? null, $actor['url'] ?? null);
				if ($contribution)
				{
					$contribution->addRole('Acteur');
					$models[] = $contribution;
				}
			}
		}

		return $models;
	}

	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\WorkModel $model
	 */
	protected function doParseWork($jsonData)
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();

		$model->setLabel($jsonData['name'] ?? null);
		$model->addGenres($jsonData['genre'] ?? null);
		$model->appendToDescription($jsonData['description'] ?? null, 'Synopsis');

		if (\is_string($jsonData['image']))
		{
			$model->setImage($jsonData['image'] ?? null);
		}
		elseif (\is_array($jsonData['image']))
		{
			$model->setImage($jsonData['image']['url'] ?? null);
		}

		return $model;
	}
}