<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\FoafPersonParser
 */
class FoafPersonParser extends \Project\Library\WebSiteParsers\JsonLd\SchemaParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\ContributorModel[]
	 */
	protected function doParse(array $jsonData)
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();

		$model->setTypology('person');
		$model->setLabel($jsonData['foaf:name'] ?? null);
		$model->setBirthDate($jsonData['bio:birth'] ?? null);
		$model->setBirthYear($jsonData['bnf-onto:firstYear'] ?? null);
		$model->setDeathDate($jsonData['bio:death'] ?? null);
		$model->setDeathYear($jsonData['bnf-onto:lastYear'] ?? null);

		$description = $jsonData['rdagroup2elements:biographicalInformation'] ?? null;
		if ($description)
		{
			while (\strpos($description, '&nbsp;') !== false || \strpos($description, '&amp;') !== false)
			{
				$description = \html_entity_decode($description);
			}
			$model->appendToDescription($description);
		}

		switch ($jsonData['foaf:gender'] ?? null)
		{
			case 'female':
				$gender = 'f';
				break;
			case 'male':
				$gender = 'h';
				break;
			default:
				$gender = null;
				break;
		}
		$model->setAttribute('sex', $gender);

		return [$model];
	}
}