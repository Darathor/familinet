<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\MovieParser
 */
class MovieParser extends \Project\Library\WebSiteParsers\JsonLd\CreativeWorkParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\WorkModel $model
	 */
	protected function doParseWork($jsonData)
	{
		$model = parent::doParseWork($jsonData);

		$model->setTypology('film');
		$model->setYear($this->parseYearFromDate($jsonData['datePublished'] ?? null));

		return $model;
	}
}