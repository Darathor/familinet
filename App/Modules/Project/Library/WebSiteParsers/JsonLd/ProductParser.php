<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\ProductParser
 */
class ProductParser extends \Project\Library\WebSiteParsers\JsonLd\SchemaParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\WorkModel[]
	 */
	protected function doParse(array $jsonData)
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();

		$model->setLabel($jsonData['name'] ?? null);
		$model->setImage($jsonData['image'] ?? null);

		if (isset($jsonData['releaseDate']))
		{
			$releaseDate = new \DateTime($jsonData['releaseDate']);
			$model->setYear($releaseDate->format('Y'));
		}

		return [$model];
	}
}