<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\SchemaParser
 */
abstract class SchemaParser
{
	/**
	 * @param string $json
	 * @return \Project\Library\WebSiteParsers\Models\AbstractModel[]
	 */
	public static function parse(string $json)
	{
		return self::parseArray(\Change\Stdlib\JsonUtils::decode($json));
	}

	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\AbstractModel[]
	 */
	public static function parseArray(array $jsonData)
	{
		switch ($jsonData['@type'] ?? null)
		{
			case 'TVSeries' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\TVSeriesParser();
				return $parser->doParse($jsonData);

			case 'Movie' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\MovieParser();
				return $parser->doParse($jsonData);

			case 'Person' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\PersonParser();
				return $parser->doParse($jsonData);

			case 'foaf:Person' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\FoafPersonParser();
				return $parser->doParse($jsonData);

			case 'Product' :
				$parser = new \Project\Library\WebSiteParsers\JsonLd\ProductParser();
				return $parser->doParse($jsonData);

			default :
				if (isset($jsonData['mainEntity']))
				{
					return self::parseArray($jsonData['mainEntity']);
				}
				return [];
		}
	}

	/**
	 * @param \DOMDocument $DOMDocument
	 * @return \Project\Library\WebSiteParsers\Models\AbstractModel[]
	 */
	public static function parseAll(\DOMDocument $DOMDocument): array
	{
		$data = [];
		/** @var \DOMElement $script */
		foreach ($DOMDocument->getElementsByTagName('script') as $script)
		{
			if ($script->getAttribute('type') === 'application/ld+json')
			{
				foreach (self::parse($script->textContent) as $value)
				{
					$data[] = $value;
				}
			}
		}
		return $data;
	}

	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\AbstractModel[]
	 */
	abstract protected function doParse(array $jsonData);

	/**
	 * @param string|null $dateString
	 * @return string|null
	 * @throws \Exception
	 */
	protected function parseYearFromDate(?string $dateString): ?string
	{
		if ($dateString)
		{
			return (new \DateTime($dateString))->format('Y');
		}
		return null;
	}
}