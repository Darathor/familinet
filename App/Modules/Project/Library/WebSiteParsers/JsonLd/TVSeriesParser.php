<?php
namespace Project\Library\WebSiteParsers\JsonLd;

/**
 * @name \Project\Library\WebSiteParsers\JsonLd\TVSeriesParser
 */
class TVSeriesParser extends \Project\Library\WebSiteParsers\JsonLd\CreativeWorkParser
{
	/**
	 * @param array $jsonData
	 * @return \Project\Library\WebSiteParsers\Models\WorkModel $model
	 */
	protected function doParseWork($jsonData)
	{
		$model = parent::doParseWork($jsonData);

		$model->setTypology('serietv');
		$model->setYear($this->parseYearFromDate($jsonData['startYear'] ?? $jsonData['datePublished'] ?? null));
		$model->setEndYear($this->parseYearFromDate($jsonData['endYear'] ?? null));
		$model->setAttribute('nb_episodes', !isset($jsonData['numberOfEpisodes']) ? null : (int)$jsonData['numberOfEpisodes']);
		$model->setAttribute('nb_saisons', !isset($jsonData['numberOfSeasons']) ? null : (int)$jsonData['numberOfSeasons']);

		return $model;
	}
}