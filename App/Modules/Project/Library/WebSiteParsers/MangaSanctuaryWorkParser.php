<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MangaSanctuaryWorkParser
 */
class MangaSanctuaryWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setLabel($this->XPathParser->getString($this->XPath->node('h1')->get()));

		$this->setInfos();
		return true;
	}

	protected function setInfos()
	{
		$work = $this->parserData->getWork();
		$infosNode = $this->XPathParser->getElement($this->XPath->node('ul', [['class', '=', 'fiche-infos']])->get());

		$inTitles = false;
		$dt = null;
		foreach ($infosNode->childNodes as $node)
		{
			if (!($node instanceof \DOMElement))
			{
				continue;
			}

			$spanNodes = [];
			foreach ($node->childNodes as $childNode)
			{
				if ($childNode instanceof \DOMElement && $childNode->nodeName === 'span')
				{
					$spanNodes[] = $childNode;
				}
			}

			if (\count($spanNodes) < 2)
			{
				continue;
			}

			$dt = \trim($spanNodes[0]->nodeValue);
			$contentNode = $spanNodes[1];

			if ($dt === 'Autres titres')
			{
				$inTitles = true;
			}
			elseif (!$dt && $inTitles)
			{
				$dt = 'Autres titres';
			}
			else
			{
				$inTitles = false;
			}

			switch ($dt)
			{
				case 'Mag. prépub':
				case 'Mag. prépub.':
				case 'Tags':
					break;

				case 'Autres titres':
					$content = $this->cleanupContent($contentNode->nodeValue);
					if ($content)
					{
						$countryCode = null;
						foreach ($node->getElementsByTagName('img') as $imgNode)
						{
							/** @var \DOMElement $imgNode */
							$countryCode = $this->getCountryCode($imgNode->getAttribute('src'));
						}
						$this->parserData->getWork()->addAlias($content, false, $countryCode);
					}
					break;

				case 'Nb. épisodes':
					$work->setAttribute('nb_episodes', $this->XPathParser->getNumber('.', $contentNode));
					break;

				case 'Type':
					$this->setType(\ucfirst($this->XPathParser->getString('.', [], $contentNode)));
					break;

				case 'Année':
					$work->setYear($this->XPathParser->getNumber('.', $contentNode));
					break;

				case 'Catégorie':
				case 'Catégories':
				case 'Genres':
				case 'Thématiques':
					foreach ( $this->XPathParser->getStrings('./a', [], $contentNode) as $value)
					{
						$value = \trim($value);
						if ($value && $value !== 'Inconnue' && $value !== 'Genre inconnu')
						{
							$work->addGenres([$value]);
						}
					}
					break;

				case 'Site officiel':
					$work->addLink('Site officiel', $contentNode->getAttribute('href'));
					break;

				default: // Contributions.
					foreach ($this->XPathParser->getElements($this->XPath->node('a')->get(), $contentNode) as $a)
					{
						// Name.
						$label = $this->cleanupContent($a->nodeValue);
						$parts = \explode(' ', $label);
						foreach ($parts as &$part)
						{
							$part = \Change\Stdlib\StringUtils::ucfirst(\Change\Stdlib\StringUtils::toLower($part));
						}
						unset($part);
						$label = \implode(' ', $parts);

						$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
						$contribution->setLabel($label);
						$contribution->setLink($a->getAttribute('href'));
						$contribution->addRole($dt);

						$this->parserData->addContribution($contribution);
					}
					break;
			}
		}
	}

	/**
	 * @param string $type
	 */
	protected function setType(string $type)
	{
		$work = $this->parserData->getWork();
		$publication = $this->parserData->getPublication();
		switch ($type)
		{
			case 'Manga': // https://www.manga-sanctuary.com/bdd/manga/2027-death-note/
				$work->setTypology('manga');
				$work->addOriginalLanguages('ja');
				$work->addNationalities('Japon');
				break;

			case 'Manhwa': // https://www.manga-sanctuary.com/bdd/manhwa/1625-cafe-occult/
				$work->setTypology('manga');
				$work->addOriginalLanguages('ko');
				$work->addNationalities('Corée du Sud');
				break;

			case 'Manhua': // https://www.manga-sanctuary.com/bdd/manhua/5167-vampire/
				$work->setTypology('manga');
				$work->addOriginalLanguages('zh');
				$work->addNationalities('Chine');
				break;

			case 'Global manga': // https://www.manga-sanctuary.com/bdd/globalmanga/5138-kairi/
				$work->setTypology('manga');
				break;

			case 'Dôjinshi': // https://www.manga-sanctuary.com/bdd/dojinshi/40273-dragon-ball-afterthefuture/
				$work->setTypology('manga');
				$work->addGenres('Fanfiction');
				break;

			case 'Anime comics': // https://www.manga-sanctuary.com/bdd/animecomics/60036-dragon-ball-super-broly/
				$work->setTypology('manga');
				$work->addGenres('Anime comics');
				break;

			case 'Light novel': // https://www.manga-sanctuary.com/bdd/light-novel/49024-death-note-light-up-the-world/
				$work->setTypology('roman');
				$work->addGenres('Light novel');
				break;

			case 'Roman': // https://www.manga-sanctuary.com/bdd/roman/6604-dragon-ball-evolution/
				$work->setTypology('roman');
				break;

			case 'Guide': // https://www.manga-sanctuary.com/bdd/guide/8726-dragon-ball-guide/
			case 'Fanbook': // https://www.manga-sanctuary.com/bdd/fanbook/9093-dragon-ball-landmark/
			case 'Artbook': // https://www.manga-sanctuary.com/bdd/artbook/15323-dragon-ball-cho-gashu/
				$work->setTypology('livre_divers');
				$work->setStatus('ended');
				$publication->setOneShot(true);
				$publication->setTypology('livre');
				break;

			case 'Série TV animée': // https://www.manga-sanctuary.com/bdd/serietvanimee/12455-dragon-ball-z-kai/
				$work->setTypology('serietv');
				$work->addGenres('Animation');
				$publication->setTypology('support_video');
				break;

			case 'Drama': // https://www.manga-sanctuary.com/bdd/drama/49073-death-note/
				$work->setTypology('serietv');
				$work->addGenres('Drama');
				$publication->setTypology('support_video');
				break;

			case 'OAV': // https://www.manga-sanctuary.com/bdd/oav/2081-bleach-memories-in-the-rain/
			case 'TV Special': // https://www.manga-sanctuary.com/bdd/tvspecial/1673-one-piece-jango-dance-carnival/
				$work->setTypology('oav');
				$publication->setTypology('support_video');
				break;

			case 'Film': // https://www.manga-sanctuary.com/bdd/film/6713-death-note-film-3/
				$work->setTypology('film');
				$work->setStatus('ended');
				$publication->setOneShot(true);
				$publication->setTypology('support_video');
				break;

			case 'OST': // https://www.manga-sanctuary.com/bdd/ost/48790-death-note-music-note/
				$work->setTypology('musique');
				$work->setStatus('ended');
				$publication->setOneShot(true);
				$publication->setTypology('support_audio');
				break;
		}
	}

	/**
	 * @param string $src
	 * @return string|null
	 */
	protected function getCountryCode(string $src)
	{
		switch (\substr($src, \strpos($src, 'img/flags')))
		{
			case 'img/flags/24.png':
				return 'BE';
			case 'img/flags/45.png':
				return 'CN';
			case 'img/flags/54.png':
				return 'KR';
			case 'img/flags/71.png':
				return 'US';
			case 'img/flags/77.png':
				return 'FR';
			case 'img/flags/112.png':
				return 'JP';
		}
		return null;
	}
}