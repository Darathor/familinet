<?php
namespace Project\Library\WebSiteParsers\Misc;

/**
 * @name \Project\Library\WebSiteParsers\Misc\AbstractParser
 */
abstract class AbstractParser
{
	/** @var \DOMDocument */
	protected $DOMDocument;

	/** @var \DOMXPath */
	protected $DOMXPath;

	/** @var \Project\Library\WebSiteParsers\XPath\XPathParser */
	protected $XPathParser;

	/** @var \Project\Library\WebSiteParsers\XPath\XPathFilter */
	protected $XPath;

	/** @var \Project\Library\WebSiteParsers\Misc\ParserData */
	protected $parserData;

	/** @var string */
	protected $url;

	/** @var \Zend\Uri\Http */
	protected $httpUri;

	/** @var \Project\Library\WebSiteParsers\Misc\Resolver */
	protected $resolver;

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @param string $url
	 */
	public function __construct(\Project\Library\WebSiteParsers\Misc\Resolver $resolver, string $url)
	{
		$this->resolver = $resolver;
		$this->url = $this->normalizeUrl($url);
		$this->httpUri = new \Zend\Uri\Http($this->url);
	}

	/**
	 * @param string $content
	 * @return string
	 */
	protected function fixFileContent(string $content)
	{
		return $content;
	}

	/**
	 * @return string
	 */
	abstract public static function getType();

	/**
	 * @return bool true if the parser only adds the link but don't parse anything in the page.
	 */
	abstract public static function isMinimal();

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return string
	 */
	protected function normalizeUrl(string $url)
	{
		// Nothing to do by default, this method should be overridden for websites with normalization rules.
		return $url;
	}

	/**
	 * @return array|null
	 */
	public function parseDataAndNormalize()
	{
		$this->preParse();
		if (!$this->doParseData())
		{
			return null;
		}
		return $this->parserData->normalize($this->url);
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Misc\ParserData|null
	 */
	public function parseData()
	{
		$this->preParse();
		if (!$this->doParseData())
		{
			return null;
		}
		return $this->parserData;
	}

	protected function preParse()
	{
		$content = \file_get_contents($this->httpUri->toString()); // In this way, the special chars are replaced by %XX in the URL.
		if ($content)
		{
			$content = $this->fixFileContent($content);
			$this->DOMDocument = new \DOMDocument();
			@$this->DOMDocument->loadHTML('<?xml encoding="UTF-8">' . $content);
			$this->DOMXPath = new \DOMXPath($this->DOMDocument);
			$this->XPathParser = new \Project\Library\WebSiteParsers\XPath\XPathParser($this->DOMDocument);
			$this->XPath = new \Project\Library\WebSiteParsers\XPath\XPathFilter();
			$this->parserData = new \Project\Library\WebSiteParsers\Misc\ParserData($this->resolver);
		}
	}

	/**
	 * @return boolean
	 */
	abstract protected function doParseData();

	/**
	 * @param string $content
	 * @return string
	 */
	protected function cleanupContent($content)
	{
		return \trim($content);
	}

	protected function loadContent(?string $jsonHref): ?string
	{
		try
		{
			$jsonContent = \file_get_contents($jsonHref);
		}
		catch (\Throwable $t)
		{
			$jsonContent = null;
		}
		return $jsonContent ?: null;
	}
}