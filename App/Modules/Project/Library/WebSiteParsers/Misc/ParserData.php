<?php
namespace Project\Library\WebSiteParsers\Misc;

/**
 * @name \Project\Library\WebSiteParsers\Misc\ParserData
 */
class ParserData
{
	/** @var \Project\Library\WebSiteParsers\Misc\Resolver */
	protected $resolver;

	/** @var array */
	protected $data = [];

	/**
	 * ParserData constructor.
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 */
	public function __construct(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Models\AbstractModel[] $data
	 */
	public function init(array $data)
	{
		foreach ($data as $model)
		{
			if ($model instanceof \Project\Library\WebSiteParsers\Models\WorkModel)
			{
				$this->data['work'] = $model;
			}
			elseif ($model instanceof \Project\Library\WebSiteParsers\Models\ContributorModel)
			{
				$this->data['contributor'] = $model;
			}
			elseif ($model instanceof \Project\Library\WebSiteParsers\Models\ContributionModel)
			{
				if (!isset($this->data['contributions']))
				{
					$this->data['contributions'] = [];
				}
				$this->data['contributions'][] = $model;
			}
		}
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\WorkModel
	 */
	public function getWork()
	{
		if (!isset($this->data['work']))
		{
			$this->data['work'] = new \Project\Library\WebSiteParsers\Models\WorkModel();
		}
		return $this->data['work'];
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\ContributorModel
	 */
	public function getContributor()
	{
		if (!isset($this->data['contributor']))
		{
			$this->data['contributor'] = new \Project\Library\WebSiteParsers\Models\ContributorModel();
		}
		return $this->data['contributor'];
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\ContributionModel[]
	 */
	public function getContributions()
	{
		return $this->data['contributions'] ?? [];
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\PublicationModel
	 */
	public function getPublication()
	{
		if (!isset($this->data['publication']))
		{
			$this->data['publication'] = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		}
		return $this->data['publication'];
	}

	/**
	 * @param string $contributorName
	 * @param string $contributorUrl
	 * @return \Project\Library\WebSiteParsers\Models\ContributionModel
	 */
	public function initContribution($contributorName, $contributorUrl)
	{
		if (!isset($this->data['contributions']))
		{
			$this->data['contributions'] = [];
		}

		$contribution = null;
		if ($contributorName && \is_string($contributorName))
		{
			// Search by link or name.
			/** @var \Project\Library\WebSiteParsers\Models\ContributionModel $c */
			foreach ($this->data['contributions'] as $c)
			{
				if ($c->getLabel() === $contributorName || ($contributorUrl && $c->getLink()->getUrl() === $contributorUrl))
				{
					$contribution = $c;
					break;
				}
			}

			if (!$contribution)
			{
				$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
				$contribution->setLabel($contributorName);
				$this->data['contributions'][] = $contribution;
			}

			if ($contributorUrl && \is_string($contributorUrl))
			{
				$contribution->setLink($contributorUrl);
			}
		}
		return $contribution;
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Models\ContributionModel|\Project\Library\WebSiteParsers\Models\ContributionModel[] $contributions
	 * @return $this
	 */
	public function addContribution($contributions)
	{
		if (!\is_array($contributions))
		{
			$contributions = $contributions ? [$contributions] : [];
		}
		foreach ($contributions as $c)
		{
			if ($c instanceof \Project\Library\WebSiteParsers\Models\ContributionModel)
			{
				$contribution = $this->initContribution($c->getLabel(), $c->getLink()->getUrl());
				if ($contribution)
				{
					$contribution->addRoles($c->getRoles());
				}
			}
		}
		return $this;
	}

	/**
	 * Transform data to array
	 * @param string $baseUrl Used to normalize partial URLs
	 * @return array
	 */
	public function normalize(string $baseUrl)
	{
		$uriReference = new \Zend\Uri\Http($baseUrl);
		return $this->normalizeArray($this->data, $uriReference);
	}

	/**
	 * @param array $array
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	private function normalizeArray(array $array, \Zend\Uri\Http $uriReference)
	{
		$data = [];

		foreach ($array as $key => $value)
		{
			if (\is_array($value))
			{
				$data[$key] = $this->normalizeArray($value, $uriReference);
			}
			elseif (!\is_object($value))
			{
				$data[$key] = $value;
			}
			if ($value instanceof \Project\Library\WebSiteParsers\Models\AbstractModel)
			{
				$normalizer = $value->getNormalizer($this->resolver);
				$normalized = $normalizer->normalize($uriReference);
				if ($normalized)
				{
					$data[$key] = $normalized;
				}
			}
		}

		return $data;
	}
}