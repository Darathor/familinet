<?php
namespace Project\Library\WebSiteParsers\Misc;

/**
 * @name \Project\Library\WebSiteParsers\Misc\ParserFactory
 */
class ParserFactory
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	public $documentManager;

	/**
	 * @var \Change\Documents\DocumentCodeManager
	 */
	public $documentCodeManager;

	/**
	 * ParserFactory constructor.
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Documents\DocumentCodeManager $documentCodeManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Documents\DocumentCodeManager $documentCodeManager)
	{
		$this->documentManager = $documentManager;
		$this->documentCodeManager = $documentCodeManager;
	}

	/**
	 * @param string $url
	 * @param string|null $type
	 * @return \Project\Library\WebSiteParsers\Misc\AbstractParser|null
	 */
	public function getByUrl(?string $url, string $type = null): ?\Project\Library\WebSiteParsers\Misc\AbstractParser
	{
		$matches = [];
		if (!$url || !\preg_match('#^([^/]+)://([^/]+)/(.*)$#', $url, $matches) || !$matches[2])
		{
			return null;
		}

		$domain = $matches[2];
		if ($matches[1] !== 'http' && $matches[1] !== 'https')
		{
			$url = 'https://' . $domain . '/' . $matches[3];
		}

		$query = $this->documentManager->getNewQuery('Project_Library_WebSite');
		$query->andPredicates($query->eq('domain', $domain));
		foreach ($query->getDocuments() as $webSite)
		{
			/** @var \Project\Library\Documents\WebSite $webSite */
			foreach ($webSite->getParsers() as $parser)
			{
				$pattern = $parser->getPattern();
				if ($pattern && \preg_match('/' . \str_replace('/', '\/', $pattern) . '/', $url))
				{
					$parserClass = $parser->getParserClass();
					if (!\class_exists($parserClass))
					{
						throw new \RuntimeException('Invalid parser class: ' . $parserClass, 999999);
					}

					$documentResolver = new \Project\Library\WebSiteParsers\Misc\Resolver($this->documentManager, $this->documentCodeManager);
					$parserObject = new $parserClass($documentResolver, $url, $webSite);
					if (!($parserObject instanceof \Project\Library\WebSiteParsers\Misc\AbstractParser))
					{
						throw new \RuntimeException('Invalid parser class: ' . $parserClass, 999999);
					}

					if ($type && $parserObject::getType() !== $type)
					{
						continue;
					}

					return $parserObject;
				}
			}
		}
		return null;
	}
}