<?php
namespace Project\Library\WebSiteParsers\Misc;

/**
 * @name \Project\Library\WebSiteParsers\Misc\Resolver
 */
class Resolver
{
	public const CODE_CONTEXT = 'External sites aliases';

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\Documents\DocumentCodeManager
	 */
	protected $documentCodeManager;

	/**
	 * Resolver constructor.
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Documents\DocumentCodeManager $documentCodeManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager, \Change\Documents\DocumentCodeManager $documentCodeManager)
	{
		$this->documentManager = $documentManager;
		$this->documentCodeManager = $documentCodeManager;
	}

	/**
	 * @param string $type
	 * @param string $class
	 * @param array $properties
	 * @param string|null $code
	 * @param string $operator
	 * @return \Change\Documents\AbstractDocument|null
	 */
	protected function resolveDocument(string $type, string $class, array $properties, string $code = null, $operator = 'or')
	{
		$query = $this->documentManager->getNewQuery($type);
		foreach ($properties as $property => $value)
		{
			if ($operator === 'and')
			{
				$query->andPredicates($query->eq($property, $value));
			}
			else
			{
				$query->orPredicates($query->eq($property, $value));
			}
		}
		$result = $query->getFirstDocument();
		if (!($result instanceof $class) && $code)
		{
			foreach ($this->documentCodeManager->getDocumentsByCode($code, self::CODE_CONTEXT) as $document)
			{
				if ($document instanceof $class)
				{
					$result = $document;
					break;
				}
			}
		}

		if ($result instanceof $class && $result instanceof \Change\Documents\AbstractDocument
			&& $result instanceof \Change\Documents\Interfaces\Editable
		)
		{
			return $result;
		}
		return null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function getDocumentInfos(\Change\Documents\AbstractDocument $document)
	{
		return [
			'model' => $document->getDocumentModelName(),
			'id' => $document->getId(),
			'label' => $document->getLabel()
		];
	}

	/**
	 * @param string $type
	 * @param string $class
	 * @param array $properties
	 * @param string|null $code
	 * @param string $operator
	 * @return array|null
	 */
	protected function resolveDocumentInfos(string $type, string $class, array $properties, string $code = null, $operator = 'or')
	{
		$document = $this->resolveDocument($type, $class, $properties, $code, $operator);
		return $document ? $this->getDocumentInfos($document) : null;
	}

	/**
	 * @param string $label
	 * @param string $url
	 * @return array|null
	 */
	public function resolveContributorInfos(string $label, string $url)
	{
		/** @var \Project\Library\Documents\Contributor|null $contributor */
		$contributor = $this->resolveDocument('Project_Library_Contributor', \Project\Library\Documents\Contributor::class, ['label' => $label]);
		if ($contributor)
		{
			foreach ($contributor->getLinks() as $link)
			{
				$url1 = $link->getUrl();
				if (\preg_match('/https?:\/\/(.+)/', $url1, $matches))
				{
					$url1 = $matches[1];
				}
				if (\preg_match('/https?:\/\/(.+)/', $url, $matches))
				{
					$url = $matches[1];
				}
				if ($url1 === $url)
				{
					return $this->getDocumentInfos($contributor);
				}
			}
		}
		return null;
	}

	/**
	 * @param string $labelOrCode
	 * @return array
	 */
	public function resolveCountryInfos(string $labelOrCode)
	{
		return $this->resolveDocumentInfos('Rbs_Geo_Country', \Rbs\Geo\Documents\Country::class,
			['label' => $labelOrCode, 'code' => $labelOrCode, 'isoCode' => $labelOrCode], $labelOrCode);
	}

	/**
	 * @param string $label
	 * @return array|null
	 */
	public function resolveWorkStatusData(string $label)
	{
		return $this->resolveDocumentInfos('Project_Library_WorkStatus', \Project\Library\Documents\WorkStatus::class, ['code' => $label], $label);
	}

	/**
	 * @param string $label
	 * @return array|null
	 */
	public function resolveGenreInfos(string $label)
	{
		return $this->resolveDocumentInfos('Project_Library_Genre', \Project\Library\Documents\Genre::class, ['label' => $label], $label);
	}

	/**
	 * @param string $label
	 * @return array|null
	 */
	public function resolveContributionTypeInfos(string $label)
	{
		return $this->resolveDocumentInfos('Project_Library_ContributionType', \Project\Library\Documents\ContributionType::class,
			['label' => $label], $label);
	}

	/**
	 * @param string $label
	 * @return array|null
	 */
	public function resolvePublisherInfos(string $label)
	{
		return $this->resolveDocumentInfos('Project_Library_Publisher', \Project\Library\Documents\Publisher::class, ['label' => $label], $label);
	}

	/**
	 * @param string $label
	 * @param int $publisherId
	 * @return array|null
	 */
	public function resolvePublisherCollectionInfos(string $label, int $publisherId)
	{
		return $this->resolveDocumentInfos('Project_Library_Collection', \Project\Library\Documents\Collection::class,
			['label' => $label, 'publisher' => $publisherId], null, 'and');
	}

	/**
	 * @param string $name
	 * @return \Rbs\Generic\Documents\Typology|null
	 */
	public function resolveTypology(string $name)
	{
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $this->resolveDocument('Rbs_Generic_Typology', \Rbs\Generic\Documents\Typology::class, ['name' => $name], $name);
	}

	/**
	 * @param string $code
	 * @param string $value
	 * @return string
	 */
	public function resolveValueFromCollection(string $code, string $value)
	{
		$convertedValue = $value;
		if ($code)
		{
			$query = $this->documentManager->getNewQuery('Rbs_Collection_Collection');
			$query->andPredicates($query->eq('code', $code));
			$collection = $query->getFirstDocument();
			if ($collection instanceof \Rbs\Collection\Documents\Collection && !$collection->getItemByValue($value))
			{
				$converterId = $this->documentManager->getAttributeValues($collection)->get('converter');
				$converter = $this->documentManager->getDocumentInstance($converterId);
				if ($converter instanceof \Project\Familinet\Documents\Mapping)
				{
					$convertedValue = $converter->getMappedValue($value);
				}
			}
		}
		return $convertedValue;
	}

	/**
	 * @param string $host
	 * @return \Project\Library\Documents\WebSite|null
	 */
	protected function resolveWebsite(string $host)
	{
		$query = $this->documentManager->getNewQuery('Project_Library_WebSite');
		$query->andPredicates($query->eq('domain', $host));
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $query->getFirstDocument();
	}

	/**
	 * @param string $host
	 * @return string|null
	 */
	public function resolveWebsiteLabel(string $host)
	{
		$website = $this->resolveWebsite($host);
		return $website ? $website->getLabel() : null;
	}

	/**
	 * @param string $host
	 * @return int|null
	 */
	public function resolveWebsiteId(string $host)
	{
		$website = $this->resolveWebsite($host);
		return $website ? $website->getId() : null;
	}

	/**
	 * @param string $host
	 * @return array|null
	 */
	public function resolveWebsiteInfos(string $host)
	{
		$website = $this->resolveWebsite($host);
		return $website ? $this->getDocumentInfos($website) : null;
	}
}