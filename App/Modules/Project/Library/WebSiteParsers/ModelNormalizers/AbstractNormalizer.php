<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
 */
abstract class AbstractNormalizer
{
	/** @var \Project\Library\WebSiteParsers\Misc\Resolver */
	protected $resolver;

	/** @var \Project\Library\WebSiteParsers\Models\AbstractModel */
	protected $model;

	/** @var array */
	protected $modelType;

	/**
	 * AbstractModelNormalizer constructor.
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @param \Project\Library\WebSiteParsers\Models\AbstractModel $model
	 */
	public function __construct(\Project\Library\WebSiteParsers\Misc\Resolver $resolver,
		\Project\Library\WebSiteParsers\Models\AbstractModel $model)
	{
		$this->resolver = $resolver;
		$this->model = $model;
		$this->modelType = $model->getModelType();
	}

	/**
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	public function normalize(\Zend\Uri\Http $uriReference)
	{
		$data = $this->model->toArray();
		$data = $this->basicNormalize($data, $uriReference);
		$data = $this->specificNormalize($data, $uriReference);
		foreach ($data as $key => $value)
		{
			if (!$value)
			{
				unset($data[$key]);
			}
		}
		return $data;
	}

	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function basicNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		foreach ($data as $key => $value)
		{
			if ($value instanceof \Project\Library\WebSiteParsers\Models\AbstractModel)
			{
				$normalizer = $value->getNormalizer($this->resolver);
				$normalized = $normalizer->normalize($uriReference);
				if ($normalized)
				{
					$data[$key] = $normalized;
				}
			}
		}

		// Model.
		$data['model'] = $this->model->getModelType();

		// Typology.
		$typology = null;
		if (isset($data['typology']))
		{
			$typology = $this->resolver->resolveTypology($data['typology']);
			if ($typology)
			{
				$data['typology'] = $typology->getId();
			}
			else
			{
				unset($data['typology']);
			}
		}

		// Nationalities.
		if (isset($data['nationalities']))
		{
			/** @var string[] $countryCodes */
			$nationalities = $data['nationalities'];
			$data['nationalities'] = [];
			$notFound = [];
			foreach ($nationalities as $nationality)
			{
				$infos = $this->resolver->resolveCountryInfos($nationality);
				if ($infos)
				{
					$data['nationalities'][] = $infos;
				}
				else
				{
					$notFound[] = $nationality;
				}
			}
			if ($notFound)
			{
				$this->appendToDescription($data, 'Autres nationalités : ' . \implode(', ', $notFound));
			}
		}

		// Status.
		if (isset($data['status']))
		{
			$infos = $this->resolveStatusData($data['status']);
			if ($infos)
			{
				$data['status'] = $infos;
			}
			else
			{
				$this->appendToDescription($data, 'Statut : ' . $data['status']);
				unset($data['status']);
			}
		}

		// Aliases.
		if (isset($data['aliases']))
		{
			/** @var \Project\Library\WebSiteParsers\Models\AbstractAliasModel[] $aliases */
			$aliases = $data['aliases'];
			unset($data['aliases']);
			foreach ($aliases as $alias)
			{
				$normalizer = $alias->getNormalizer($this->resolver);
				$normalized = $normalizer->normalize($uriReference);
				if ($normalized)
				{
					$data['aliases'][] = $normalized;
				}
			}
		}

		// Links.
		if (isset($data['links']))
		{
			/** @var \Project\Library\WebSiteParsers\Models\AbstractLinkModel[] $links */
			$links = $data['links'];
			unset($data['links']);
			foreach ($links as $key => $link)
			{
				$normalizer = $link->getNormalizer($this->resolver);
				$normalized = $normalizer->normalize($uriReference);
				if ($normalized)
				{
					$data['links'][] = $normalized;
				}
			}
		}

		// Attributes.
		if ($typology && isset($data['attributes']))
		{
			/** @var array $attributes */
			$attributes = $data['attributes'];
			foreach ($attributes as $code => $value)
			{
				$value = $this->getAttributeValue($typology, $code, $value);
				if ($value)
				{
					$data['attributes'][$code] = $value;
				}
			}
		}

		return $data;
	}

	/**
	 * @param string $status
	 * @return array|null
	 */
	protected function resolveStatusData(string $status)
	{
		return null;
	}

	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		return $data;
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology|null $typology
	 * @param string $attributeCode
	 * @param mixed $value
	 * @return mixed
	 */
	protected function getAttributeValue($typology, string $attributeCode, $value)
	{
		if (!($typology instanceof \Rbs\Generic\Documents\Typology))
		{
			return $value;
		}

		$collectionCode = null;
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				if ($attribute->getName() === $attributeCode)
				{
					$collectionCode = $attribute->getCollectionCode();
					break;
				}
			}
		}

		if (!$collectionCode)
		{
			return null;
		}
		return $this->resolver->resolveValueFromCollection($collectionCode, $value);
	}

	/**
	 * @param array $data
	 * @param string $text
	 */
	protected function appendToDescription(array &$data, string $text)
	{
		if (!$text)
		{
			return;
		}

		if (!isset($data['description']) || !$data['description'])
		{
			$data['description'] = '';
		}
		elseif (!\Change\Stdlib\StringUtils::endsWith($data['description'], "\n\n"))
		{
			$data['description'] .= "\n\n";
		}
		$data['description'] .= $text;
	}
}