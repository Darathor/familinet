<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\AliasNormalizer
 */
class AliasNormalizer extends \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
{
	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		if (isset($data['forWebsite']))
		{
			$data['forWebsite'] = $this->resolver->resolveWebsiteId($data['forWebsite']);
		}
		return $data;
	}
}