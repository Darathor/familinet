<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\ContributionNormalizer
 */
class ContributionNormalizer extends \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
{
	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		// Roles.
		if (isset($data['rolesData']))
		{
			$roles = [];
			foreach ($data['rolesData'] as $roleCode => $d)
			{
				$role = $this->resolver->resolveContributionTypeInfos($roleCode);
				if ($role)
				{
					if (!\array_key_exists($role['label'], $roles))
					{
						$roles[$role['label']] = ['role' => $role, 'details' => []];
					}
					foreach ($d as $detail => $contexts)
					{
						if (!\array_key_exists($detail, $roles[$role['label']]['details']))
						{
							$roles[$role['label']]['details'][$detail] = [];
						}
						foreach ($contexts as $context)
						{
							if (!\in_array($context, $roles[$role['label']]['details'][$detail], true))
							{
								$roles[$role['label']]['details'][$detail][] = $context;
							}
						}
					}
				}
			}
			$data['roles'] = [];
			$data['roleDetail'] = [];
			foreach ($roles as $role)
			{
				$data['roles'][] = $role['role'];
				foreach ($role['details'] as $detail => $contexts)
				{
					$data['roleDetail'][] = $detail . ($contexts ? ' (' . \implode(', ', $contexts) . ')' : '');
				}
			}
			$data['roleDetail'] = \implode(', ', $data['roleDetail']);
			unset($data['rolesData']);
		}

		// Find contributor by name and check the link
		if (isset($data['link']))
		{
			$link = $data['link'];
			if ($link instanceof \Project\Library\WebSiteParsers\Models\AbstractLinkModel)
			{
				$data['link'] = $link->getNormalizer($this->resolver)->normalize($uriReference) ?: null;
			}

			$linkUrl = $data['link']['url'] ?? null;
			if ($linkUrl)
			{
				$infos = $this->resolver->resolveContributorInfos($data['label'], $linkUrl);
				if ($infos)
				{
					$data['contributor'] = $infos;
				}
			}
		}

		return $data;
	}
}