<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\LinkNormalizer
 */
class LinkNormalizer extends \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
{
	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		if (!isset($data['url']))
		{
			return [];
		}

		// Complete relative URL.
		$url = new \Zend\Uri\Http($data['url']);
		if (!$url->getScheme())
		{
			$url->setScheme($uriReference->getScheme());
		}
		if (!$url->getHost())
		{
			$url->setHost($uriReference->getHost());
		}
		if (!\Change\Stdlib\StringUtils::beginsWith($url->getPath(), '/'))
		{
			$url->setPath('/' . $url->getPath());
		}

		if (!$url->isValid() || !$url->isAbsolute())
		{
			return [];
		}

		// If no label, search by website.
		$data['url'] = (string)$url;
		if (!empty($data['label']))
		{
			return $data;
		}
		$label = $this->resolver->resolveWebsiteLabel($url->getHost());
		if (!$label)
		{
			return [];
		}

		$data['label'] = $label;
		return $data;
	}
}