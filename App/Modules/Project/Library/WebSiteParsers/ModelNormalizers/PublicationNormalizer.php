<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\PublicationNormalizer
 */
class PublicationNormalizer extends \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
{
	/**
	 * @param string $status
	 * @return array|null
	 */
	protected function resolveStatusData(string $status)
	{
		return $this->resolver->resolveWorkStatusData($status);
	}

	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		// Publisher.
		if (isset($data['publisher']))
		{
			$data['publishers'][] = $data['publisher'];
			unset($data['publisher']);
		}
		if (isset($data['publishers']))
		{
			$publishers = [];
			foreach ($data['publishers'] as $publisherValue)
			{
				$publisher = $this->resolver->resolvePublisherInfos($publisherValue);
				if ($publisher)
				{
					$publishers[] = $publisher;
				}
				elseif ($publisherValue)
				{
					$this->appendToDescription($data, 'Éditeur : ' . $publisherValue);
				}
			}
			$data['publishers'] = $publishers;

			// Collection.
			if (isset($data['collection']))
			{
				$collectionValue = $data['collection'];
				unset($data['collection']);

				$collection = null;
				foreach ($data['publishers'] as $publisher)
				{
					$collection = $this->resolver->resolvePublisherCollectionInfos($collectionValue, $publisher['id']);
					if ($collection)
					{
						break;
					}
				}
				if ($collection)
				{
					$data['collection'] = $collection;
				}
				elseif ($collectionValue)
				{
					$this->appendToDescription($data, 'Collection : ' . $collectionValue);
				}
			}
		}

		return $data;
	}
}