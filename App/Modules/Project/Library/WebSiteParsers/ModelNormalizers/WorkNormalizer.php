<?php
namespace Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \Project\Library\WebSiteParsers\ModelNormalizers\WorkNormalizer
 */
class WorkNormalizer extends \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
{
	/**
	 * @param string $status
	 * @return array|null
	 */
	protected function resolveStatusData(string $status)
	{
		return $this->resolver->resolveWorkStatusData($status);
	}

	/**
	 * @param array $data
	 * @param \Zend\Uri\Http $uriReference Used to normalize partial URLs
	 * @return array
	 */
	protected function specificNormalize(array $data, \Zend\Uri\Http $uriReference)
	{
		// Original language
		if (isset($data['originalLanguages']))
		{
			$languages = [];
			foreach ($data['originalLanguages'] as $language)
			{
				$value = $this->resolver->resolveValueFromCollection('Project_Library_Languages', $language);
				if ($value && !isset($data['originalLanguage']))
				{
					$data['originalLanguage'] = $value;
				}
				else
				{
					$languages[] = $language;
				}
			}
			if ($languages)
			{
				$this->appendToDescription($data, '**Langues origniales :** ' . \implode(', ', $languages));
			}
			unset($data['originalLanguages']);
		}

		// Genres
		if (isset($data['genres']))
		{
			/** @var string[] $genreLabels */
			$genreLabels = $data['genres'];
			$notFound = [];
			$data['genres'] = [];
			foreach ($genreLabels as $genreLabel)
			{
				$infos = $this->resolver->resolveGenreInfos($genreLabel);
				if ($infos)
				{
					$data['genres'][] = $infos;
				}
				else
				{
					$notFound[] = $genreLabel;
				}
			}
			if ($notFound)
			{
				$this->appendToDescription($data, 'Autres genres : ' . \implode(', ', $notFound));
			}
		}

		return $data;
	}
}