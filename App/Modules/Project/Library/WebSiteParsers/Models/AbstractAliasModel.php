<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\AbstractAliasModel
 */
abstract class AbstractAliasModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	/** @var bool */
	protected $original = false;

	/**
	 * @return bool
	 */
	public function getOriginal()
	{
		return $this->original;
	}

	/**
	 * @param bool $original
	 * @return $this
	 */
	public function setOriginal($original)
	{
		$this->original = $original;
		return $this;
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\AliasNormalizer($resolver, $this);
	}
}