<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\AbstractLinkModel
 */
abstract class AbstractLinkModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	/** @var string */
	protected $url;

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return $this
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\LinkNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\LinkNormalizer($resolver, $this);
	}
}