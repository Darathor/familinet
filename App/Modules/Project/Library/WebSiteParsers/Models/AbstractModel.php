<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\AbstractModel
 */
abstract class AbstractModel
{
	/**
	 * @return array
	 */
	public function toArray()
	{
		return \get_object_vars($this);
	}

	/**
	 * @return string
	 */
	abstract public function getModelType();

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
	 */
	abstract public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver);

	/**
	 * @param string|string[] $values
	 * @param string[] $array
	 * @param array $options
	 */
	public static function addTextValuesToArray($values, &$array, $options = [])
	{
		if (!\is_array($values))
		{
			$values = $values && \is_string($values) ? [$values] : [];
		}
		foreach ($values as $value)
		{
			if (\is_string($value))
			{
				$value = \trim($value);
				if (in_array('ucfirst', $options, true))
				{
					$value = \Change\Stdlib\StringUtils::ucfirst($value);
				}
				if ($value && !\in_array($value, $array, true))
				{
					$array[] = $value;
				}
			}
		}
	}
}