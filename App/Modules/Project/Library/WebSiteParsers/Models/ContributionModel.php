<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\ContributionModel
 */
class ContributionModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\DocumentModel;
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;

	/** @var \Project\Library\WebSiteParsers\Models\AbstractLinkModel Contributor's link */
	protected $link;

	/** @var array Role's code with details and context */
	protected $rolesData = [];

	/**
	 * @return \Project\Library\WebSiteParsers\Models\AbstractLinkModel
	 */
	public function getLink()
	{
		if (!($this->link instanceof \Project\Library\WebSiteParsers\Models\ContributorLinkModel))
		{
			return new \Project\Library\WebSiteParsers\Models\ContributorLinkModel();
		}
		return $this->link;
	}

	/**
	 * @param string|\Project\Library\WebSiteParsers\Models\AbstractLinkModel $url
	 * @return $this
	 */
	public function setLink($url)
	{
		if ($url && \is_string($url))
		{
			$link = new \Project\Library\WebSiteParsers\Models\ContributorLinkModel();
			$link->setUrl($url);
			$this->link = $link;
		}
		elseif ($url instanceof \Project\Library\WebSiteParsers\Models\ContributorLinkModel)
		{
			$this->link = $url;
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getRoles()
	{
		return $this->rolesData;
	}

	/**
	 * @param string $roleCode
	 * @param string $roleDetail
	 * @param string $context
	 * @return $this
	 */
	public function addRole($roleCode, $roleDetail = '', $context = '')
	{
		if ($roleCode && !\array_key_exists($roleCode, $this->rolesData))
		{
			$this->rolesData[$roleCode] = [];
		}
		if ($roleCode && $roleDetail && !\array_key_exists($roleDetail, $this->rolesData[$roleCode]))
		{
			$this->rolesData[$roleCode][$roleDetail] = [];
		}
		if ($roleCode && $roleDetail && $context && !\in_array($context, $this->rolesData[$roleCode][$roleDetail], true))
		{
			$this->rolesData[$roleCode][$roleDetail][] = $context;
		}
		return $this;
	}

	/**
	 * @param array $roles
	 * @return $this
	 */
	public function addRoles($roles)
	{
		foreach ($roles as $role => $d)
		{
			if ($d)
			{
				foreach ($d as $detail => $c)
				{
					if ($c)
					{
						foreach ($c as $context)
						{
							$this->addRole($role, $detail, $context);
						}
					}
					else
					{
						$this->addRole($role, $detail);
					}
				}
			}
			else
			{
				$this->addRole($role);
			}
		}
		return $this;
	}

	/**
	 * @param string $contributorName
	 * @param string $contributorUrl
	 * @return ContributionModel
	 */
	public static function init($contributorName, $contributorUrl)
	{
		$contribution = null;
		if ($contributorName && \is_string($contributorName))
		{
			$contribution = new self();
			$contribution->setLabel($contributorName);
			if ($contributorUrl && \is_string($contributorUrl))
			{
				$contribution->setLink($contributorUrl);
			}
		}
		return $contribution;
	}

	/**
	 * @return string
	 */
	public function getModelType()
	{
		return 'Project_Library_Contribution';
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\ContributionNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\ContributionNormalizer($resolver, $this);
	}
}