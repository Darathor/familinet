<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\ContributorAliasModel
 */
class ContributorAliasModel extends \Project\Library\WebSiteParsers\Models\AbstractAliasModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;

	/** @var string|null */
	protected $forWebsite;

	/**
	 * @return string|null
	 */
	public function getForWebsite()
	{
		return $this->forWebsite;
	}

	/**
	 * @param string|null $forWebsite
	 * @return $this
	 */
	public function setForWebsite(?string $forWebsite)
	{
		$this->forWebsite = $forWebsite;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getModelType()
	{
		return 'Project_Library_ContributorAlias';
	}
}