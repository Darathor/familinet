<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\ContributorModel
 * @property \Project\Library\WebSiteParsers\Models\ContributorLinkModel[] $links
 */
class ContributorModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\DescribedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\DocumentModel;
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;
	use \Project\Library\WebSiteParsers\Models\Traits\LinkedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel;

	/** @var string */
	protected $birthDate;

	/** @var int */
	protected $birthYear;

	/** @var string */
	protected $deathDate;

	/** @var int */
	protected $deathYear;

	/** @var string */
	protected $image;

	/** @var \Project\Library\WebSiteParsers\Models\ContributorAliasModel[] */
	protected $aliases = [];

	/**
	 * @return string
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

	/**
	 * @param string|\DateTime $birthDate
	 * @return $this
	 */
	public function setBirthDate($birthDate)
	{
		if ($birthDate && \is_string($birthDate))
		{
			$birthDate = new \DateTime($birthDate);
		}
		if ($birthDate instanceof \DateTime)
		{
			// FIX#142: ensure to have 0h00 UTC without modifying the day.
			$birthDate->setTime(12, 0);
			$birthDate->setTimezone(new \DateTimeZone('UTC'));
			$birthDate->setTime(0, 0);

			$this->birthDate = $birthDate->format(\DateTimeInterface::ATOM);
			$this->birthYear = (int)$birthDate->format('Y');
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getBirthYear()
	{
		return $this->birthYear;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setBirthYear($year)
	{
		if ($year !== null)
		{
			$this->birthYear = (int)$year;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDeathDate()
	{
		return $this->deathDate;
	}

	/**
	 * @param string|\DateTime $deathDate
	 * @return $this
	 */
	public function setDeathDate($deathDate)
	{
		if ($deathDate && \is_string($deathDate))
		{
			$deathDate = new \DateTime($deathDate);
		}
		if ($deathDate instanceof \DateTime)
		{
			// FIX#142: ensure to have 0h00 UTC without modifying the day.
			$deathDate->setTime(12, 0);
			$deathDate->setTimezone(new \DateTimeZone('UTC'));
			$deathDate->setTime(0, 0);

			$this->deathDate = $deathDate->format(\DateTimeInterface::ATOM);
			$this->deathYear = (int)$deathDate->format('Y');
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getDeathYear()
	{
		return $this->deathYear;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setDeathYear($year)
	{
		if ($year !== null)
		{
			$this->deathYear = (int)$year;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 * @return $this
	 */
	public function setImage($image)
	{
		if ($image)
		{
			$this->image = (string)$image;
		}
		return $this;
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\AbstractAliasModel[]
	 */
	public function getAliases()
	{
		return $this->aliases;
	}

	/**
	 * @param string|null $label
	 * @param bool $original
	 * @param string|null $forWebsiteHost
	 * @return $this
	 */
	public function addAlias($label, bool $original = true, string $forWebsiteHost = null)
	{
		if ($label)
		{
			$alias = new \Project\Library\WebSiteParsers\Models\ContributorAliasModel();
			$alias->setLabel($label);
			$alias->setOriginal($original);
			$alias->setForWebsite($forWebsiteHost);
			$this->aliases[] = $alias;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getLinkClass()
	{
		return ContributorLinkModel::class;
	}

	/**
	 * @return string
	 */
	public function getModelType()
	{
		return 'Project_Library_Contributor';
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\ContributorNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\ContributorNormalizer($resolver, $this);
	}
}