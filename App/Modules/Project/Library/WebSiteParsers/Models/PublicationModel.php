<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\PublicationModel
 */
class PublicationModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\DatedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\DescribedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\DocumentModel;
	use \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel;

	/** @var string */
	protected $status;

	/** @var boolean */
	protected $oneShot = false;

	/** @var string */
	protected $volumesDetail;

	/** @var string */
	protected $publisher;

	/** @var string */
	protected $collection;

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return $this
	 */
	public function setStatus($status)
	{
		if ($status)
		{
			$this->status = (string)$status;
		}
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getOneShot()
	{
		return $this->oneShot;
	}

	/**
	 * @param bool $oneShot
	 * @return $this
	 */
	public function setOneShot($oneShot)
	{
		$this->oneShot = $oneShot;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVolumesDetail()
	{
		return $this->volumesDetail;
	}

	/**
	 * @param string $volumesDetail
	 * @return $this
	 */
	public function setVolumesDetail($volumesDetail)
	{
		$this->volumesDetail = $volumesDetail;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPublisher()
	{
		return $this->publisher;
	}

	/**
	 * @param string $publisher
	 * @return $this
	 */
	public function setPublisher($publisher)
	{
		$this->publisher = $publisher;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCollection()
	{
		return $this->collection;
	}

	/**
	 * @param string $collection
	 * @return $this
	 */
	public function setCollection($collection)
	{
		$this->collection = $collection;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getModelType()
	{
		return 'Project_Library_Publication';
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\PublicationNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\PublicationNormalizer($resolver, $this);
	}
}