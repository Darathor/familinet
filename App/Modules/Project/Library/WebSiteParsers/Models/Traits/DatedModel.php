<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\DatedModel
 */
trait DatedModel
{
	/** @var int */
	protected $year;

	/** @var int */
	protected $endYear;

	/**
	 * @return int
	 */
	public function getYear()
	{
		return $this->year;
	}

	/**
	 * @param int $year
	 * @return $this
	 */
	public function setYear($year)
	{
		if ($year !== null)
		{
			$this->year = (int)$year;
		}
		return $this;
	}

	/**
	 * @return int
	 */
	public function getEndYear()
	{
		return $this->endYear;
	}

	/**
	 * @param int $endYear
	 * @return $this
	 */
	public function setEndYear($endYear)
	{
		if ($endYear !== null)
		{
			$this->endYear = (int)$endYear;
		}
		return $this;
	}

}