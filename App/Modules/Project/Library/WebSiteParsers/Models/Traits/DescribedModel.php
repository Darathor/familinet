<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\DescribedModel
 */
trait DescribedModel
{
	/** @var string */
	protected $description;

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return $this
	 */
	public function unsetDescription()
	{
		$this->description = null;
		return $this;
	}

	/**
	 * @param string|null $description
	 * @param string $title
	 * @return $this
	 */
	public function appendToDescription($description, string $title = '')
	{
		$description = \trim($description ?: '');
		$description = \preg_replace("/ *\n */", "\n", $description);
		$description = \preg_replace("/\n{2,}/", "\n\n", $description);
		$description = \str_replace('’', '\'', $description);
		$title = \trim($title);
		if ($description)
		{
			while (\strpos($description, '&nbsp;') !== false || \strpos($description, '&amp;') !== false)
			{
				$description = \html_entity_decode($description);
			}

			if ($this->description && !\Change\Stdlib\StringUtils::endsWith($this->description, "\n\n"))
			{
				$this->description .= "\n\n";
			}
			if ($title)
			{
				$this->description .= '**' . $title . ' :** ';
			}
			$this->description .= $description;
		}

		return $this;
	}
}