<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\DocumentModel
 */
trait DocumentModel
{
	/**
	 * @var string
	 */
	protected $typology;

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * @return string
	 */
	public function getTypology()
	{
		return $this->typology;
	}

	/**
	 * @param string $typology
	 * @return $this
	 */
	public function setTypology(string $typology)
	{
		$this->typology = $typology;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * @return mixed
	 */
	public function getAttribute(string $name)
	{
		return $this->attributes[$name] ?? null;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function setAttribute(string $name, $value)
	{
		if ($value)
		{
			$this->attributes[$name] = $value;
		}
		return $this;
	}
}