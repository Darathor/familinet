<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\LabeledModel
 */
trait LabeledModel
{
	/** @var string */
	protected $label;

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		$this->label = $label;
		return $this;
	}
}