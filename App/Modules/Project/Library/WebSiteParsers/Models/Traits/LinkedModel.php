<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\LinkedModel
 */
trait LinkedModel
{
	/** @var \Project\Library\WebSiteParsers\Models\AbstractLinkModel[] */
	protected $links = [];

	abstract protected function getLinkClass();

	/**
	 * @return \Project\Library\WebSiteParsers\Models\AbstractLinkModel[]
	 */
	public function getLinks()
	{
		return $this->links;
	}

	/**
	 * @param string $label
	 * @param string $url
	 * @return $this
	 */
	public function addLink(string $label, string $url)
	{
		if ($label && $url)
		{
			$classname = $this->getLinkClass();
			$link = new $classname();
			$link->setLabel($label);
			$link->setUrl($url);
			$this->links[] = $link;
		}
		return $this;
	}

	/**
	 * @param string $url
	 * @return $this
	 */
	public function addLinkByUrl($url)
	{
		if ($url)
		{
			$classname = $this->getLinkClass();
			$this->links[] = (new $classname())->setUrl($url);
		}
		return $this;
	}
}