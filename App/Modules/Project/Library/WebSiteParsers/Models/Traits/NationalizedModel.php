<?php
namespace Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel
 */
trait NationalizedModel
{
	/** @var string[] */
	protected $nationalities = [];

	/**
	 * @return string[]
	 */
	public function getNationalities()
	{
		return $this->nationalities;
	}

	/**
	 * @param string|string[]|null $nationalities
	 * @return $this
	 */
	public function addNationalities($nationalities)
	{
		\Project\Library\WebSiteParsers\Models\AbstractModel::addTextValuesToArray($nationalities, $this->nationalities);
		return $this;
	}
}