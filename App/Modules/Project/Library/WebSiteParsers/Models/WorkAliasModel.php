<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\WorkAliasModel
 */
class WorkAliasModel extends \Project\Library\WebSiteParsers\Models\AbstractAliasModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;
	use \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel;

	/**
	 * @inheritDoc
	 */
	public function getModelType()
	{
		return 'Project_Library_WorkAlias';
	}
}