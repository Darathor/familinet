<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\WorkLinkModel
 */
class WorkLinkModel extends \Project\Library\WebSiteParsers\Models\AbstractLinkModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;

	/**
	 * @inheritDoc
	 */
	public function getModelType()
	{
		return 'Project_Library_WorkLink';
	}
}