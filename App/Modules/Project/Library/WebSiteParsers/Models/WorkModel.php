<?php
namespace Project\Library\WebSiteParsers\Models;

/**
 * @name \Project\Library\WebSiteParsers\Models\WorkModel
 * @property \Project\Library\WebSiteParsers\Models\WorkLinkModel[] $links
 */
class WorkModel extends \Project\Library\WebSiteParsers\Models\AbstractModel
{
	use \Project\Library\WebSiteParsers\Models\Traits\DatedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\DescribedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\DocumentModel;
	use \Project\Library\WebSiteParsers\Models\Traits\LabeledModel;
	use \Project\Library\WebSiteParsers\Models\Traits\LinkedModel;
	use \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel;

	/** @var string */
	protected $subtitle;

	/** @var string */
	protected $status;

	/** @var string[] */
	protected $genres = [];

	/** @var string */
	protected $image;

	/** @var string[] */
	protected $originalLanguages = [];

	/** @var string */
	protected $audience;

	/** @var \Project\Library\WebSiteParsers\Models\WorkAliasModel[] */
	protected $aliases = [];

	/**
	 * @return string
	 */
	public function getSubtitle()
	{
		return $this->subtitle;
	}

	/**
	 * @param string $subtitle
	 * @return $this
	 */
	public function setSubtitle($subtitle)
	{
		$this->subtitle = $subtitle;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return $this
	 */
	public function setStatus($status)
	{
		if ($status)
		{
			$this->status = (string)$status;
		}
		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getGenres()
	{
		return $this->genres;
	}

	/**
	 * @param string|string[] $genres
	 * @return $this
	 */
	public function addGenres($genres)
	{
		self::addTextValuesToArray($genres, $this->genres, ['ucfirst']);
		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getOriginalLanguages()
	{
		return $this->originalLanguages;
	}

	/**
	 * @param string|string[]|null $originalLanguages
	 * @return $this
	 */
	public function addOriginalLanguages($originalLanguages)
	{
		self::addTextValuesToArray($originalLanguages, $this->originalLanguages);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 * @return $this
	 */
	public function setImage($image)
	{
		if ($image)
		{
			$this->image = (string)$image;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAudience()
	{
		return $this->audience;
	}

	/**
	 * @param string $audience
	 * @return $this
	 */
	public function setAudience($audience)
	{
		$this->audience = $audience;
		return $this;
	}

	/**
	 * @return \Project\Library\WebSiteParsers\Models\WorkAliasModel[]
	 */
	public function getAliases()
	{
		return $this->aliases;
	}

	/**
	 * @param string|null $label
	 * @param string|string[] $nationalities
	 * @param bool $original
	 * @return $this
	 */
	public function addAlias($label, bool $original = true, $nationalities = [])
	{
		if ($label)
		{
			if (!$nationalities && $original)
			{
				$originalNationalities = $this->getNationalities();
				if (\count($originalNationalities) === 1)
				{
					$nationalities = $originalNationalities;
				}
			}

			$alias = new \Project\Library\WebSiteParsers\Models\WorkAliasModel();
			$alias->setLabel($label);
			$alias->addNationalities($nationalities);
			$alias->setOriginal($original);
			$this->aliases[] = $alias;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getLinkClass()
	{
		return WorkLinkModel::class;
	}

	/**
	 * @return string
	 */
	public function getModelType()
	{
		return 'Project_Library_Work';
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Misc\Resolver $resolver
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\WorkNormalizer
	 */
	public function getNormalizer(\Project\Library\WebSiteParsers\Misc\Resolver $resolver)
	{
		return new \Project\Library\WebSiteParsers\ModelNormalizers\WorkNormalizer($resolver, $this);
	}
}