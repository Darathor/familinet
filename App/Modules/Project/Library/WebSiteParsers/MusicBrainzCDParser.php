<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MusicBrainzCDParser
 */
class MusicBrainzCDParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - With 2 authors: https://musicbrainz.org/release/6a661fb2-a950-491d-bcbd-308aa6e24ca1

		$work = $this->parserData->getWork();

		$work->addLinkByUrl($this->url);
		$work->setTypology('musique');
		$work->setStatus('ended');
		$work->setLabel($this->XPathParser->getString($this->XPath->node('h1')->get()));
		$work->addOriginalLanguages($this->XPathParser->getStrings($this->XPath->node('dd', [['class', '=', 'language']])->get()));
		$this->setLinks();

		$publication = $this->parserData->getPublication();
		$publication->setTypology('support_audio');
		$publication->setStatus('ended');
		$publication->setOneShot(true);
		$publication->setAttribute('detail_format', $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'format']])->get()));
		$publication->setAttribute('duree_totale', $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'length']])->get()));
		$publication->setAttribute('code_barre', $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'barcode']])->get()));
		$publication->setAttribute('type_album', $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'type']])->get()));
		$publication->setAttribute('id_amazon', $this->XPathParser->getString($this->XPath->node('li', [['class', '=', 'amazon-favicon']])->get()));
		$this->setReleaseEvent();
		$this->setPublisher();
		$this->setTypeSupport();
		$this->setDescription();

		$this->setContributions();

		return true;
	}

	public function setReleaseEvent()
	{
		$publication = $this->parserData->getPublication();
		foreach ($this->XPathParser->getElements($this->XPath->node('ul', [['class', '~', 'release-events']])->node('li')->get()) as $li)
		{
			$date = $this->XPathParser->getString($this->XPath->node('span', [['class', '~', 'release-date']])->get(), [], $li);
			if ($date)
			{
				$fullDate = $this->XPathParser::applyFormat($date, $this->XPathParser::FORMAT_DATE, 'Y-m-j');
				if ($fullDate)
				{
					$publication->setYear(\date('Y', \strtotime($fullDate)));
					$publication->setAttribute('date_publication', $fullDate);
				}
				else
				{
					$fullDate = $this->XPathParser::applyFormat($date, $this->XPathParser::FORMAT_DATE, 'Y-m');
					if ($fullDate)
					{
						$publication->setYear(\date('Y', \strtotime($fullDate)));
					}
					else
					{
						$year = $this->XPathParser::applyFormat($date, $this->XPathParser::FORMAT_NUMBER, '');
						$publication->setYear($year);
					}
				}

				$publication->addNationalities($this->XPathParser->getString($this->XPath->node('span', [['class', '~', 'flag']])->get(), [], $li));

				break;
			}
		}
	}

	private function setPublisher()
	{
		$publication = $this->parserData->getPublication();

		foreach ($this->XPathParser->getElements($this->XPath->node('h2', [['class', '=', 'labels']])->followings('ul', [['position', '=', 1]])
			->node('li')->get()) as $li)
		{
			$publisher = $this->XPathParser->getString('a', [], $li);
			if ($publisher)
			{
				$publication->setPublisher($publisher);
				$publication->setAttribute('numero_catalogue', $this->XPathParser->getString($this->XPath
					->node('span', [['class', '=', 'catalog-number']])->get(), [], $li));
				break;
			}
		}
	}

	private function setTypeSupport()
	{
		$publication = $this->parserData->getPublication();
		$value = $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'format']])->get());
		if ($value)
		{
			if (\preg_match('/(.*)×(.*)/u', $value, $matches))
			{
				$publication->setAttribute('type_support_audio', $matches[2]);
			}
			else
			{
				$publication->setAttribute('type_support_audio', $value);
			}
		}
	}

	private function setDescription()
	{
		$playlist = $this->readPlaylist();
		$description = $this->compilePlaylist($playlist);

		$publication = $this->parserData->getPublication();
		$publication->appendToDescription($description);
	}

	private function readPlaylist()
	{
		$playlist = [];
		foreach ($this->XPathParser->getElements($this->XPath->node('div', [['id', '=', 'content']])->node('table', [['class', '=', 'tbl medium']])
			->get()) as $table)
		{
			// Title
			$volume = $this->XPathParser->getString('thead//th', [], $table);
			$volume = \trim(\str_replace('▼', '', $volume));

			// List
			foreach ($this->XPathParser->getElements($this->XPath->node('tbody')->node('tr', [['class', '!=', 'subh']])->get(), $table) as $tr)
			{
				$position = $this->XPathParser->getNumber($this->XPath->node('td', [['class', '=', 'pos t']])->get(), $tr);
				$title = $this->XPathParser->getString($this->XPath->node('td', [['class', '=', 'pos t']])->followings('td//a')->get(), [], $tr);

				if ($position && $title)
				{
					$playlist[$volume][$position] = $title;
				}
			}
		}
		return $playlist;
	}

	/**
	 * @param array $playlist
	 * @return string
	 */
	private function compilePlaylist($playlist)
	{
		$text = '';
		foreach ($playlist as $volume => $tracks)
		{
			if ($text)
			{
				$text .= "\n";
			}

			$text .= $volume . "\n\n";
			foreach ($tracks as $position => $title)
			{
				$text .= $position . '. ' . $title . "\n";
			}
		}
		return $text;
	}

	protected function setLinks()
	{
		$work = $this->parserData->getWork();
		/** @var \DOMElement $a */
		foreach ($this->XPathParser->getStrings($this->XPath->node('ul', [['class', '~', 'external_links']])
			->node('li')->node('a')->attribute('href')->get()) as $href)
		{
			$work->addLinkByUrl($href);
		}
	}

	protected function setContributions()
	{
		foreach ($this->XPathParser->getElements($this->XPath->node('p', [['class', '~', 'subheader']])->node('a', [['title', '!=', '']])->get()) as
				 $a)
		{
			$contribution = new \Project\Library\WebSiteParsers\Models\ContributionModel();
			$contribution->setLink($a->getAttribute('href'));
			$contribution->setLabel($this->cleanupContent($a->nodeValue));
			$contribution->addRole('Auteur');
			$this->parserData->addContribution($contribution);
		}
	}
}