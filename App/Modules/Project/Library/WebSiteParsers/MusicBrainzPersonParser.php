<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\MusicBrainzPersonParser
 */
class MusicBrainzPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - With death date: https://musicbrainz.org/artist/5a6e934b-a203-456c-812e-6c5c3efd673e
		// - Group: https://musicbrainz.org/artist/3c18062e-27bd-4532-8a91-9889477a3533
		// - Multiple area: https://musicbrainz.org/artist/505604ee-556c-402a-b5eb-0441fd153037

		// Notes:
		// - Description is not available because it is dynamic

		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setTypology($this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'type']])->get()));
		$contributor->addNationalities(
			$this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'area']])->node('span', [['class', '~', 'flag']])->get())
		);
		$this->setLabel();
		$this->setSex();
		$this->setDates();
		$this->setLinks();

		return true;
	}

	protected function setLabel()
	{
		$contributor = $this->parserData->getContributor();
		$label = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'artistheader']])->node('h1')->node('a')->get());
		$contributor->setLabel($label);
		$comment = $this->XPathParser->getString($this->XPath->node('div', [['class', '~', 'artistheader']])->node('h1')->node('span')->get());
		if ($comment)
		{
			$contributor->addAlias($label . ' ' . $comment, false, $this->httpUri->getHost());
		}
	}

	protected function setSex()
	{
		$contributor = $this->parserData->getContributor();
		$sex = $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'gender']])->get());
		switch ($sex)
		{
			case 'Female':
				$sex = 'f';
				break;
			case 'Male':
				$sex = 'h';
				break;
			default:
				$sex = null;
				break;
		}
		$contributor->setAttribute('sex', $sex);
	}

	protected function setDates()
	{
		$contributor = $this->parserData->getContributor();
		$beginDate = $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'begin-date']])->get());
		if (\preg_match('/.*(\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])).*/', $beginDate, $matches))
		{
			$beginDate = $matches[1];
			$beginDate = $this->XPathParser::applyFormat($beginDate, $this->XPathParser::FORMAT_DATE, 'Y-m-j');
			$contributor->setBirthDate($beginDate);
			$contributor->setBirthYear(\date('Y', \strtotime($beginDate)));
		}
		elseif (\preg_match('/.*(\d{4}).*/', $beginDate, $matches))
		{
			$contributor->setBirthYear($matches[1]);
		}

		$endDate = $this->XPathParser->getString($this->XPath->node('dd', [['class', '=', 'end-date']])->get());
		if (\preg_match('/.*(\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])).*/', $endDate, $matches))
		{
			$endDate = $matches[1];
			$endDate = $this->XPathParser::applyFormat($endDate, $this->XPathParser::FORMAT_DATE, 'Y-m-j');
			$contributor->setDeathDate($endDate);
			$contributor->setDeathYear(\date('Y', \strtotime($endDate)));
		}
		elseif (\preg_match('/.*(\d{4}).*/', $endDate, $matches))
		{
			$contributor->setDeathYear($matches[1]);
		}
	}

	protected function setLinks()
	{
		$contributor = $this->parserData->getContributor();
		foreach ($this->XPathParser->getElements($this->XPath->node('ul', [['class', '~', 'external_links']])
			->node('li', [['class', '!=', 'all-relationships']])->node('a')->get()) as $li)
		{
			if ($li->nodeValue === 'Official homepage')
			{
				$contributor->addLink('Site officiel', $li->getAttribute('href'));
			}
			else
			{
				$contributor->addLinkByUrl($li->getAttribute('href'));
			}
		}

		foreach ($this->XPathParser->getElements($this->XPath->node('dd', [['class', '~', 'isni-code']])->node('a')->get()) as $a)
		{
			$contributor->addLinkByUrl($a->getAttribute('href'));
		}

		foreach ($this->XPathParser->getElements($this->XPath->node('div', [['class', '~', 'wikipedia-extract']])->node('a')->get()) as $a)
		{
			$contributor->addLinkByUrl($a->getAttribute('href'));
		}
	}
}