<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\SimpleContributorParser
 */
class SimpleContributorParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return true;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		$this->parserData->getContributor()->addLinkByUrl($this->url);

		return true;
	}
}