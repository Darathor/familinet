<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\SimpleWorkParser
 */
class SimpleWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return true;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		$this->parserData->getWork()->addLinkByUrl($this->url);

		return true;
	}
}