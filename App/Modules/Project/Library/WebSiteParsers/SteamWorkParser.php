<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\SteamWorkParser
 */
class SteamWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		// Examples:
		// - https://store.steampowered.com/app/228280/Baldurs_Gate_Enhanced_Edition/

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setTypology('jeu_video');
		$work->setStatus('ended');
		$work->setLabel($this->XPathParser->getString($this->XPath->node('*', [['itemprop', '~', 'name']])->textNodes()->get()));

		$releaseDate = $this->XPathParser->getString($this->XPath
			->node('*', [['class', '~', 'release_date']])->node('*', [['class', '~', 'date']])->get());
		if (\preg_match('/\d{4}$/', $releaseDate, $matches))
		{
			$work->setYear($matches[0]);
			$work->setEndYear($work->getYear());
		}

		$publication = $this->parserData->getPublication();
		$publication->setStatus('ended');
		$publication->setOneShot(true);
		$publication->setYear($work->getYear());
		$publication->setEndYear($work->getEndYear());
		$publication->setTypology('support_logiciel');
		$publication->setAttribute('type_support_logiciel', 'Numérique');

		return true;
	}
}