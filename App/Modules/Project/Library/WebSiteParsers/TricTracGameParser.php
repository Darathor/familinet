<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\TricTracGameParser
 */
class TricTracGameParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		if (!$this->DOMDocument)
		{
			return false;
		}

		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setTypology('jeu_societe');
		$work->setStatus('ended');
		$work->setLabel($this->XPathParser->getString($this->XPath->node('h1', [['itemprop', '=', 'name']])->get()));

		// TODO: to finish

		return true;
	}
}