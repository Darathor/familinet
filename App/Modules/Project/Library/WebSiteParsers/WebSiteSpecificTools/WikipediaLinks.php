<?php
namespace Project\Library\WebSiteParsers\WebSiteSpecificTools;

/**
 * @name \Project\Library\WebSiteParsers\WebSiteSpecificTools\WikipediaLinks
 * @property \Project\Library\WebSiteParsers\XPath\XPathParser $XPathParser
 * @property \Project\Library\WebSiteParsers\XPath\XPathFilter $XPath
 * @property \Project\Library\WebSiteParsers\Misc\Resolver $resolver
 */
trait WikipediaLinks
{
	protected function resolveInterLanguageLinks(callable $callback): void
	{
		foreach ($this->XPathParser->getElements($this->XPath->node('a', [['class', '=', 'interlanguage-link-target']])->get()) as $link)
		{
			$url = $link->getAttribute('href');
			$zendURI = new \Zend\Uri\Http($url);
			$label = $this->resolver->resolveWebsiteLabel($zendURI->getHost());
			if ($label)
			{
				$callback($label, $url);
			}
		}
	}

	protected function resolveExternalLinks(callable $callback): void
	{
		foreach ($this->XPathParser->getElements($this->XPath->node('*', [['id', '=', 'Liens_externes']])->parent()->followings('ul')
			->node('a', [['class', '~', 'external']])->get()) as $link)
		{
			$url = $link->getAttribute('href');
			if (\Change\Stdlib\StringUtils::beginsWith($url, 'https://tools.wmflabs.org'))
			{
				if (\preg_match('#https://www\.imdb\.com/&id=(tt[a-z0-9]+)#', $url, $matches))
				{
					$url = 'https://www.imdb.com/title/' . $matches[1] . '/';
				}
				if (\preg_match('#https://www\.imdb\.com/&id=(nm[a-z0-9]+)#', $url, $matches))
				{
					$url = 'https://www.imdb.com/name/' . $matches[1] . '/';
				}
			}
			$zendURI = new \Zend\Uri\Http($url);
			$label = $this->resolver->resolveWebsiteLabel($zendURI->getHost());
			if ($label)
			{
				$callback($label, $url);
			}
		}
	}
}