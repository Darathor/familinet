<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\WikipediaWorkParser
 */
class WikipediaPersonParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	use \Project\Library\WebSiteParsers\WebSiteSpecificTools\WikipediaLinks;

	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'contributor';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		$contributor = $this->parserData->getContributor();
		$contributor->addLinkByUrl($this->url);
		$contributor->setLabel($this->XPathParser->getString($this->XPath->node('h1', [['id', '=', 'firstHeading']])->get()));

		$this->resolveInterLanguageLinks(function (string $label, string $url) { $this->parserData->getContributor()->addLink($label, $url); });
		$this->resolveExternalLinks(function (string $label, string $url) { $this->parserData->getContributor()->addLink($label, $url); });

		return true;
	}
}