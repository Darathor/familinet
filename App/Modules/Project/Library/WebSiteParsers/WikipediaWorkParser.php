<?php
namespace Project\Library\WebSiteParsers;

/**
 * @name \Project\Library\WebSiteParsers\WikipediaWorkParser
 */
class WikipediaWorkParser extends \Project\Library\WebSiteParsers\Misc\AbstractParser
{
	use \Project\Library\WebSiteParsers\WebSiteSpecificTools\WikipediaLinks;

	/**
	 * @return string
	 */
	public static function getType()
	{
		return 'work';
	}

	/**
	 * @return bool
	 */
	public static function isMinimal()
	{
		return false;
	}

	/**
	 * @return boolean
	 */
	public function doParseData()
	{
		$work = $this->parserData->getWork();
		$work->addLinkByUrl($this->url);
		$work->setLabel($this->XPathParser->getString($this->XPath->node('h1', [['id', '=', 'firstHeading']])->get()));

		$this->resolveInterLanguageLinks(function (string $label, string $url) { $this->parserData->getWork()->addLink($label, $url); });
		$this->resolveExternalLinks(function (string $label, string $url) { $this->parserData->getWork()->addLink($label, $url); });

		return true;
	}
}