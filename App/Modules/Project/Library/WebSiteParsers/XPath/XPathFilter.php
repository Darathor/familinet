<?php
namespace Project\Library\WebSiteParsers\XPath;

/**
 * @name \Project\Library\WebSiteParsers\XPath\XPathFilter
 */
class XPathFilter
{
	/**
	 * @var string
	 */
	protected $path = '';

	/**
	 * @param array $filters
	 * @return XPathFilter
	 */
	protected function addFilter(array $filters)
	{
		if ($filters)
		{
			$this->path .= '[';
			$firstFilter = true;
			/**
			 * @var string $attr
			 * @var string $value
			 */
			foreach ($filters as [$attr, $op, $value])
			{
				if ($firstFilter)
				{
					$firstFilter = false;
				}
				else
				{
					$this->path .= ' and ';
				}

				if ($attr === 'text')
				{
					$toCheck = 'normalize-space(.)';
				}
				elseif ($attr === 'position')
				{
					$toCheck = 'position()';
				}
				else
				{
					$toCheck = '@' . $attr;
				}

				switch ($op)
				{
					case '=':
						if (\is_int($value))
						{
							$this->path .= $toCheck . '=' . $value;
						}
						else
						{
							$this->path .= $toCheck . "='" . \addslashes($value) . "'";
						}
						break;
					case '!=':
						if (\is_int($value))
						{
							$this->path .= 'not(' . $toCheck . '=' . $value . ')';
						}
						elseif ($value)
						{
							$this->path .= 'not(' . $toCheck . "='" . \addslashes($value) . "')";
						}
						else
						{
							$this->path .= 'attribute::' . $attr;
						}
						break;
					case '~':
						$this->path .= 'contains(' . $toCheck . ", '" . \addslashes($value) . "')";
						break;
					case '!~':
						$this->path .= 'not(contains(' . $toCheck . ", '" . \addslashes($value) . "'))";
						break;
				}
			}
			$this->path .= ']';
		}
		return $this;
	}

	/**
	 * @param string $tag
	 * @param array $filters
	 * @return XPathFilter
	 */
	public function node(string $tag, array $filters = [])
	{
		if ($this->path)
		{
			$this->path .= '//';
		}
		$this->path .= $tag;
		return $this->addFilter($filters);
	}

	/**
	 * @param string $attribute
	 * @param array $filters
	 * @return XPathFilter
	 */
	public function attribute(string $attribute, array $filters = [])
	{
		$this->path .= '/@' . $attribute;
		return $this->addFilter($filters);
	}

	/**
	 * Complete filter to return text nodes
	 * @return XPathFilter
	 */
	public function textNodes()
	{
		$this->path .= '/text()';
		return $this;
	}

	/**
	 * Complete filter to go back parent node
	 * @param string $tag
	 * @param array $filters
	 * @return XPathFilter
	 */
	public function parent(string $tag = '*', array $filters = [])
	{
		$this->path .= '/parent::' . $tag;
		return $this->addFilter($filters);
	}

	/**
	 * Complete filter to select brother
	 * @param string $tag
	 * @param array $filters
	 * @return XPathFilter
	 */
	public function followings(string $tag = '*', array $filters = [])
	{
		$this->path .= '/following-sibling::' . $tag;
		return $this->addFilter($filters);
	}

	/**
	 * Complete filter to select brother
	 * @param string $tag
	 * @param array $filters
	 * @return XPathFilter
	 */
	public function precedings(string $tag = '*', array $filters = [])
	{
		$this->path .= '/preceding-sibling::' . $tag;
		return $this->addFilter($filters);
	}

	/**
	 * Return the filter and re init it
	 * @return string
	 */
	public function get()
	{
		$value = $this->path;
		$this->path = '';
		return $value;
	}
}