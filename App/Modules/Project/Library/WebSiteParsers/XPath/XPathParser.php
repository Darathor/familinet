<?php
namespace Project\Library\WebSiteParsers\XPath;

/**
 * @name \Project\Library\WebSiteParsers\XPath\XPathParser
 */
class XPathParser
{
	public const FORMAT_STRING = 'string';
	public const FORMAT_NUMBER = 'number';
	public const FORMAT_DATE = 'date';

	/**
	 * @var \DOMXPath
	 */
	protected $DOMXPath;

	/**
	 * XPathParser constructor.
	 * @param \DOMDocument $domDocument
	 */
	public function __construct(\DOMDocument $domDocument)
	{
		$this->DOMXPath = new \DOMXPath($domDocument);
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return \DOMElement[]
	 */
	public function getElements(string $xPath, \DOMNode $contextNode = null)
	{
		if (!$contextNode)
		{
			$xPath = '//' . $xPath;
		}

		$elements = [];
		foreach ($this->DOMXPath->query($xPath, $contextNode) as $node)
		{
			if ($node instanceof \DOMElement)
			{
				$elements[] = $node;
			}
		}

		return $elements;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return \DOMElement|null
	 */
	public function getElement(string $xPath, \DOMNode $contextNode = null): ?\DOMElement
	{
		if (!$contextNode)
		{
			$xPath = '//' . $xPath;
		}

		$elements = $this->DOMXPath->query($xPath, $contextNode);
		if ($elements->count() === 0)
		{
			return null;
		}

		$item = $elements->item(0);
		if ($item instanceof \DOMElement)
		{
			return $item;
		}

		return null;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return \DOMNode[]
	 */
	public function getNodes(string $xPath, \DOMNode $contextNode = null): array
	{
		if (!$contextNode)
		{
			$xPath = '//' . $xPath;
		}

		$nodes = [];
		foreach ($this->DOMXPath->query($xPath, $contextNode) as $node)
		{
			$nodes[] = $node;
		}
		return $nodes;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return \DOMNode|null
	 */
	public function getNode(string $xPath, \DOMNode $contextNode = null): ?\DomNode
	{
		if (!$contextNode)
		{
			$xPath = '//' . $xPath;
		}

		$nodes = $this->DOMXPath->query($xPath, $contextNode);
		if ($nodes->count() === 0)
		{
			return null;
		}

		return $nodes->item(0);
	}

	/**
	 * Return all formatted values matching the XPath
	 * @param string $xPath
	 * @param array $ignoredValues
	 * @param \DOMNode|null $contextNode [optional]
	 * @param string $format [optional]
	 * @param string $dateFormat [optional]
	 * @return string[]|int[]
	 */
	protected function getValues(string $xPath, array $ignoredValues = [], ?\DOMNode $contextNode = null, string $format = self::FORMAT_STRING,
		string $dateFormat = 'j/m/Y')
	{
		$values = [];
		foreach ($this->getNodes($xPath, $contextNode) as $node)
		{
			$value = null;
			if (!$node->childNodes || ($node->childNodes->length ?: 0) === 0)
			{
				$value = $this->cleanupContent($node->nodeValue);
			}
			else
			{
				foreach ($node->childNodes as $element)
				{
					if ($element instanceof \DOMElement && $element->tagName === 'br')
					{
						$value .= "\n\n";
					}
					else
					{
						$value .= $element->nodeValue;
					}
				}
				$value = $this->cleanupContent($value);
			}
			$values = $this->normalizeAndAddValue($values, $value, $ignoredValues, $format, $dateFormat);
		}

		return $values;
	}

	/**
	 * Return all formatted attribute values matching the XPath
	 * @param string $xPath
	 * @param string $attribute
	 * @param array $ignoredValues
	 * @param \DOMNode|null $contextNode [optional]
	 * @param string $format [optional]
	 * @param string $dateFormat [optional]
	 * @return string[]
	 */
	protected function getAttributeValues(string $xPath, string $attribute, array $ignoredValues = [], ?\DOMNode $contextNode = null,
		string $format = self::FORMAT_STRING, string $dateFormat = 'j/m/Y'): array
	{
		$values = [];

		foreach ($this->getNodes($xPath, $contextNode) as $node)
		{
			$value = null;
			if ($node instanceof \DOMElement)
			{
				$value = $node->getAttribute($attribute);
			}

			$values = $this->normalizeAndAddValue($values, $value, $ignoredValues, $format, $dateFormat);
		}

		return $values;
	}

	/**
	 * @param array $values
	 * @param string|int|null $value
	 * @param array $ignoredValues
	 * @param string $format
	 * @param string $dateFormat
	 * @return array
	 */
	protected function normalizeAndAddValue(array $values, ?string $value, array $ignoredValues, string $format, string $dateFormat): array
	{
		$value = \preg_replace('/ +/', ' ', $value);
		$value = self::applyFormat($value, $format, $dateFormat);
		if ($value)
		{
			if ($ignoredValues)
			{
				if (!\in_array($value, $ignoredValues, true))
				{
					$values[] = $value;
				}
			}
			else
			{
				$values[] = $value;
			}
		}
		return $values;
	}

	/**
	 * Apply specified format to the value
	 * @param string $value
	 * @param string $format
	 * @param string $dateFormat
	 * @return int|string|null
	 */
	public static function applyFormat(string $value, string $format, string $dateFormat = 'j/m/Y')
	{
		if (!$value)
		{
			return null;
		}

		switch ($format)
		{
			case self::FORMAT_STRING:
				return $value;

			case self::FORMAT_DATE:
				if (false !== \strpos($dateFormat, 'F'))
				{
					$value = self::replaceMonths($value);
				}
				$date = \DateTime::createFromFormat($dateFormat, $value);
				if ($date)
				{
					$date->setTimezone(new \DateTimeZone('UTC'));
					$date->setTime(0, 0);
					return $date->format(\DateTimeInterface::ATOM);
				}
				return null;

			case self::FORMAT_NUMBER:
				$nb = \preg_replace('/\D/', '', $value);
				return $nb ? (int)$nb : null;

			default:
				return null;
		}
	}

	/**
	 * Clean up content
	 * @param string $content
	 * @return string
	 */
	protected function cleanupContent(string $content)
	{
		return \trim($content);
	}

	/**
	 * Replace french months with english months in the given value
	 * @param string $value
	 * @return string
	 */
	protected static function replaceMonths(string $value)
	{
		return \str_replace(
			['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
			['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			$value
		);
	}

	/**
	 * @param string $xPath
	 * @param array $ignoredValues
	 * @param \DOMNode|null $contextNode [optional]
	 * @return string[]
	 */
	public function getStrings(string $xPath, array $ignoredValues = [], ?\DOMNode $contextNode = null)
	{
		return $this->getValues($xPath, $ignoredValues, $contextNode);
	}

	/**
	 * @param string $xPath
	 * @param array $ignoredValues
	 * @param \DOMNode|null $contextNode [optional]
	 * @return string|null
	 */
	public function getString(string $xPath, array $ignoredValues = [], ?\DOMNode $contextNode = null)
	{
		$values = $this->getStrings($xPath, $ignoredValues, $contextNode);
		return $values ? \trim($values[0]) : null;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode [optional]
	 * @return int[]
	 */
	public function getNumbers(string $xPath, ?\DOMNode $contextNode = null)
	{
		return $this->getValues($xPath, [], $contextNode, self::FORMAT_NUMBER);
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode [optional]
	 * @return int|null
	 */
	public function getNumber(string $xPath, ?\DOMNode $contextNode = null)
	{
		$values = $this->getNumbers($xPath, $contextNode);
		return $values ? $values[0] : null;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode [optional]
	 * @param string $dateFormat [optional]
	 * @return string[]
	 */
	public function getDates(string $xPath, ?\DOMNode $contextNode = null, string $dateFormat = 'j/m/Y')
	{
		return $this->getValues($xPath, [], $contextNode, self::FORMAT_DATE, $dateFormat);
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode [optional]
	 * @param string $dateFormat [optional]
	 * @return string|null
	 */
	public function getDate(string $xPath, ?\DOMNode $contextNode = null, string $dateFormat = 'j/m/Y')
	{
		$values = $this->getDates($xPath, $contextNode, $dateFormat);
		return $values ? $values[0] : null;
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return string[]
	 */
	public function getHrefs(string $xPath, \DOMNode $contextNode = null)
	{
		return $this->getAttributeValues($xPath, 'href', [], $contextNode);
	}

	/**
	 * @param string $xPath
	 * @param \DOMNode|null $contextNode
	 * @return string|null
	 */
	public function getHref(string $xPath, \DOMNode $contextNode = null)
	{
		$values = $this->getHrefs($xPath, $contextNode);
		return $values ? $values[0] : null;
	}
}