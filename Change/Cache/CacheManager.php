<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Cache;

/**
 * @name \Change\Cache\CacheManager
 */
class CacheManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'CacheManager';

	const MEMORY_NAMESPACE_PREFIX = 'Memory';

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @var array[]
	 */
	protected $namespaces;

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/CacheManager');
	}

	/**
	 * @api
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('buildCacheAdapter', function ($event) { $this->onBuildCacheAdapter($event); }, 5);
	}

	/**
	 * @return array
	 */
	protected function loadNamespaces()
	{
		$namespaces = [];
		$application = $this->getApplication();
		$cacheConfiguration = (array)($application->getConfiguration('Change/Cache') ?? []);

		$ns = $cacheConfiguration['Namespaces'] ?? [];
		foreach ($ns as $namespace => $args)
		{
			if ($namespace === 'BlockManager' || $namespace === 'PageManager' || !$args)
			{
				continue;
			}
			$namespaces[$namespace] = null;
		}

		if ($cacheConfiguration['block'] ?? true)
		{
			$namespaces['BlockManager'] = null;
		}
		if ($cacheConfiguration['page'] ?? true)
		{
			$namespaces['PageManager'] = null;
		}

		return $namespaces;
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getNamespaces()
	{
		if ($this->namespaces === null)
		{
			$this->namespaces = $this->loadNamespaces();
		}
		return array_keys($this->namespaces);
	}

	/**
	 * @param string $namespace
	 * @return \Zend\Cache\Storage\StorageInterface|null
	 */
	protected function buildCacheAdapter($namespace)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['namespace' => $namespace]);
		$eventManager->trigger('buildCacheAdapter', $this, $args);
		if (isset($args['adapter']) && $args['adapter'] instanceof \Zend\Cache\Storage\StorageInterface)
		{
			return $args['adapter'];
		}
		return null;
	}

	/**
	 * @var \Redis|null
	 */
	protected $redis;
	
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildCacheAdapter($event)
	{
		if ($event->getParam('adapter') !== null)
		{
			return;
		}

		$namespace = $event->getParam('namespace');
		$application = $this->getApplication();
		$adapter = $application->getConfiguration('Change/Cache/Adapter');
		if (isset($adapter['redis']) && is_array($adapter['redis']))
		{
			$options = $adapter['redis'];
			$options['namespace'] = ($options['namespace'] ?? '') . $namespace;
			$cacheAdapter = new SimpleRedisAdapter($options, $this->redis);
			$this->redis = $cacheAdapter->getRedisAdapter();
			$this->namespaces[$namespace] = $cacheAdapter;
		}
		elseif (isset($adapter['memcached']) && is_array($adapter['memcached']))
		{
			$options = $adapter['memcached'];
			$options['namespace'] = $namespace;
			$cacheAdapter = new \Zend\Cache\Storage\Adapter\Memcached($options);
			$cachePlugin = new \Zend\Cache\Storage\Plugin\Serializer();
			$cachePluginOptions = new \Zend\Cache\Storage\Plugin\PluginOptions(
				['serializer' => new \Zend\Serializer\Adapter\PhpSerialize()]
			);
			$cachePlugin->setOptions($cachePluginOptions);
			$cacheAdapter->addPlugin($cachePlugin);
			$this->namespaces[$namespace] = $cacheAdapter;
		}
		else
		{
			$options = isset($adapter['filesystem']) && is_array($adapter['filesystem']) ? $adapter['filesystem'] :
				['cache_dir' => null, 'umask' => false, 'dirPermission' => false, 'filePermission' => false];

			$workspace = $application->getWorkspace();
			if (!isset($options['cache_dir']))
			{
				$cacheDir = $workspace->cachePath();
			}
			else
			{
				$cacheDir = $workspace->composeAbsolutePath($options['cache_dir']);
			}
			$options['cache_dir'] = rtrim($cacheDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR .'Zend' . DIRECTORY_SEPARATOR ;
			$options['namespace'] = $namespace;

			\Change\Stdlib\FileUtils::mkdir($options['cache_dir']);
			$cacheAdapter = new \Zend\Cache\Storage\Adapter\Filesystem($options);
			$cachePlugin = new \Zend\Cache\Storage\Plugin\Serializer();
			$cachePluginOptions = new \Zend\Cache\Storage\Plugin\PluginOptions(
				['serializer' => new \Zend\Serializer\Adapter\PhpSerialize()]
			);
			$cachePlugin->setOptions($cachePluginOptions);
			$cacheAdapter->addPlugin($cachePlugin);
			$this->namespaces[$namespace] = $cacheAdapter;
		}

		$event->setParam('adapter', $cacheAdapter);
	}

	/**
	 * @api
	 * @param string $memoryNamespace
	 * @return string
	 */
	public function buildMemoryNamespace($memoryNamespace = 'Default')
	{
		return static::MEMORY_NAMESPACE_PREFIX . ucfirst((string)$memoryNamespace);
	}

	/**
	 * @param string $namespace
	 * @return null|\Zend\Cache\Storage\StorageInterface
	 */
	protected function getCacheAdapter($namespace)
	{
		if ($this->namespaces === null)
		{
			$this->namespaces = $this->loadNamespaces();
		}

		$namespaces = $this->namespaces;
		if (array_key_exists($namespace, $namespaces))
		{
			$adapter = $namespaces[$namespace];
			if ($adapter !== false && !($adapter instanceof \Zend\Cache\Storage\StorageInterface))
			{
				$adapter = $this->buildCacheAdapter($namespace);
				if ($adapter)
				{
					$this->namespaces[$namespace] = $adapter;
				}
				else
				{
					$this->namespaces[$namespace] = false;
				}
			}
			return $adapter ?: null;
		}
		if (strpos($namespace, static::MEMORY_NAMESPACE_PREFIX) === 0)
		{
			$adapter = new SimpleMemoryAdapter([]);
			$this->namespaces[$namespace] = $adapter;
			return $adapter;
		}
		return null;
	}

	/**
	 * @param \Zend\Cache\Storage\StorageInterface $adapter
	 * @param array $options
	 */
	protected function setAdapterOptions(\Zend\Cache\Storage\StorageInterface $adapter, array $options)
	{
		if (isset($options['ttl']))
		{
			$adapter->getOptions()->setTtl($options['ttl']);
		}
	}

	/**
	 * @api
	 * @return string
	 */
	public function getLocalCacheDir()
	{
		return $this->getApplication()->getWorkspace()->cachePath() . DIRECTORY_SEPARATOR;
	}

	/**
	 * @api
	 * @param string $namespace
	 * @return bool
	 */
	public function isValidNamespace($namespace)
	{
		if ($namespace && is_string($namespace))
		{
			return $this->getCacheAdapter($namespace) !== null;
		}
		return false;
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param string $key
	 * @param array $options
	 * @return bool
	 */
	public function hasEntry($namespace, $key, array $options = [])
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			if ($options)
			{
				$this->setAdapterOptions($adapter, $options);
			}
			return $adapter->hasItem($key);
		}
		return false;
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param string $key
	 * @param array $options
	 * @return mixed|null
	 */
	public function getEntry($namespace, $key, array $options = [])
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			if ($options)
			{
				$this->setAdapterOptions($adapter, $options);
			}
			return $adapter->getItem($key);
		}
		return null;
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param string[] $keys
	 * @param array $options
	 * @return array Associative array of keys and values
	 */
	public function getEntries($namespace, $keys, array $options = [])
	{
		if ($keys && ($adapter = $this->getCacheAdapter($namespace)))
		{
			if ($options)
			{
				$this->setAdapterOptions($adapter, $options);
			}
			return $adapter->getItems($keys);
		}
		return [];
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param string $key
	 * @param mixed $entry
	 * @param array $options
	 */
	public function setEntry($namespace, $key, $entry, array $options = [])
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			if ($options)
			{
				$this->setAdapterOptions($adapter, $options);
			}
			$adapter->setItem($key, $entry);
		}
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param $key
	 */
	public function removeEntry($namespace, $key)
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			$adapter->removeItem($key);
		}
	}

	/**
	 * @api
	 * @param string $namespace
	 * @return boolean
	 */
	public function canClearKeyPrefix($namespace)
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			return ($adapter instanceof \Zend\Cache\Storage\ClearByPrefixInterface);
		}
		return false;
	}

	/**
	 * @api
	 * @param string $namespace
	 * @param string $keyPrefix
	 */
	public function clearNameSpace($namespace, $keyPrefix = null)
	{
		$adapter = $this->getCacheAdapter($namespace);
		if ($adapter)
		{
			if ($keyPrefix)
			{
				if ($adapter instanceof \Zend\Cache\Storage\ClearByPrefixInterface)
				{
					$adapter->clearByPrefix($keyPrefix);
				}
				elseif ($adapter instanceof \Zend\Cache\Storage\ClearByNamespaceInterface)
				{
					$adapter->clearByNamespace($namespace);
				}
				elseif ($adapter instanceof \Zend\Cache\Storage\FlushableInterface)
				{
					$adapter->flush();
				}
			}
			else
			{
				if ($adapter instanceof \Zend\Cache\Storage\ClearByNamespaceInterface)
				{
					$adapter->clearByNamespace($namespace);
				}
				elseif ($adapter instanceof \Zend\Cache\Storage\FlushableInterface)
				{
					$adapter->flush();
				}
			}
		}
	}

	/**
	 * @api
	 */
	public function clearLocalCache()
	{
		\Change\Stdlib\FileUtils::rmdir($this->getLocalCacheDir());
	}

	/**
	 * @api
	 */
	public function clearMemoryCache()
	{
		foreach ($this->getNamespaces() as $namespace)
		{
			$adapter = $this->getCacheAdapter($namespace);
			if ($adapter instanceof SimpleMemoryAdapter)
			{
				$adapter->flush();
			}
		}
	}

	/**
	 * @api
	 */
	public function clearPersistentCache()
	{
		foreach ($this->getNamespaces() as $namespace)
		{
			$adapter = $this->getCacheAdapter($namespace);
			if ($adapter instanceof SimpleMemoryAdapter)
			{
				continue;
			}
			if ($adapter instanceof \Zend\Cache\Storage\ClearByNamespaceInterface)
			{
				$adapter->clearByNamespace($namespace);
			}
			elseif ($adapter instanceof \Zend\Cache\Storage\FlushableInterface)
			{
				$adapter->flush();
			}
		}
	}

	/**
	 * @api
	 */
	public function clearAll()
	{
		$this->clearLocalCache();
		foreach ($this->getNamespaces() as $name)
		{
			$this->clearNameSpace($name);
		}
	}
}