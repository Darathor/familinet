<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Cache;

use Zend\Cache\Storage\Adapter;

/**
 * @name \Change\Cache\SimpleMemoryAdapter
 */
class SimpleMemoryAdapter implements \Zend\Cache\Storage\StorageInterface, \Zend\Cache\Storage\FlushableInterface
{

	/**
	 * @var \Zend\Cache\Storage\Capabilities
	 */
	protected $capabilities;

	/**
	 * @var \Change\Cache\SimpleMemoryOptions
	 */
	protected $options;

	/**
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var string
	 */
	protected $namespacePrefix = '';

	/**
	 * @param array $options
	 */
	public function __construct(array $options)
	{
		$this->setOptions($options);
		$this->data = [];
	}

	/**
	 * @return string
	 */
	public function getNamespacePrefix()
	{
		return $this->namespacePrefix;
	}

	/**
	 * @param string $namespacePrefix
	 * @return $this
	 */
	public function setNamespacePrefix($namespacePrefix)
	{
		$this->namespacePrefix = $namespacePrefix;
		return $this;
	}

	/**
	 * @param array $options
	 * @return $this
	 */
	public function setOptions($options)
	{
		/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		$options = new \Change\Cache\SimpleMemoryOptions($options, $this);
		if ($this->options)
		{
			$this->options->setAdapter(null);
		}
		$this->options = $options;
		return $this;
	}

	/**
	 * @return \Change\Cache\SimpleMemoryOptions
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Get an item.
	 *
	 * @param  string $key
	 * @param  bool $success
	 * @param  mixed $casToken
	 * @return mixed Data on success, null on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getItem($key, &$success = null, &$casToken = null)
	{
		if (!array_key_exists($key, $this->data))
		{
			$success = false;
			return null;
		}
		$success = true;
		$value = $this->data[$key];
		$casToken = $value;
		return $value;
	}

	/**
	 * Get multiple items.
	 *
	 * @param  array $keys
	 * @return array Associative array of keys and values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getItems(array $keys)
	{
		$results = [];
		if (!$keys)
		{
			return $results;
		}
		foreach ($keys as $key)
		{
			if (array_key_exists($key, $this->data))
			{
				$results[$key] = $this->data[$key];
			}
		}
		return $results;
	}

	/**
	 * Test if an item exists.
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function hasItem($key)
	{
		return array_key_exists($key, $this->data);
	}

	/**
	 * Test multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of found keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function hasItems(array $keys)
	{
		$result = [];
		foreach ($keys as $key)
		{
			if (array_key_exists($key, $this->data))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Get metadata of an item.
	 *
	 * @param  string $key
	 * @return array|bool Metadata on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getMetadata($key)
	{
		return [];
	}

	/**
	 * Get multiple metadata
	 *
	 * @param  array $keys
	 * @return array Associative array of keys and metadata
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getMetadatas(array $keys)
	{
		return [];
	}

	/**
	 * Store an item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function setItem($key, $value)
	{
		$this->data[$key] = $value;
		return true;
	}

	/**
	 * Store multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function setItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			$this->data[$key] = $value;
		}
		return $result;
	}

	/**
	 * Add an item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function addItem($key, $value)
	{
		if (!$this->hasItem($key))
		{
			$this->data[$key] = $value;
			return true;
		}
		return false;
	}

	/**
	 * Add multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function addItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			if (!$this->hasItem($key))
			{
				$this->data[$key] = $value;
			}
			else
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Replace an existing item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function replaceItem($key, $value)
	{
		if ($this->hasItem($key))
		{
			return $this->setItem($key, $value);
		}
		return false;
	}

	/**
	 * Replace multiple existing items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function replaceItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			if (!$this->replaceItem($key, $value))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Set an item only if token matches
	 *
	 * It uses the token received from getItem() to check if the item has
	 * changed before overwriting it.
	 *
	 * @param  mixed $token
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 * @see    getItem()
	 * @see    setItem()
	 */
	public function checkAndSetItem($token, $key, $value)
	{
		return false;
	}

	/**
	 * Reset lifetime of an item
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function touchItem($key)
	{
		return true;
	}

	/**
	 * Reset lifetime of multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of not updated keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function touchItems(array $keys)
	{
		$result = [];
		return $result;
	}

	/**
	 * Remove an item.
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function removeItem($key)
	{
		if ($this->hasItem($key))
		{
			unset($this->data[$key]);
			return true;
		}
		return false;
	}

	/**
	 * Remove multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of not removed keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function removeItems(array $keys)
	{
		$result = [];
		foreach ($keys as $key)
		{
			if ($this->hasItem($key))
			{
				unset($this->data[$key]);
			}
		}
		return $result;
	}

	/**
	 * Increment an item.
	 *
	 * @param  string $key
	 * @param  int $value
	 * @return int|bool The new value on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function incrementItem($key, $value)
	{
		if ($this->hasItem($key) && is_numeric($oldValue = $this->data[$key]))
		{
			$nv = $oldValue + $value;
			$this->data[$key] = $nv;
			return $nv;
		}
		return false;
	}

	/**
	 * Increment multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Associative array of keys and new values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function incrementItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			$nv = $this->incrementItem($key, $value);
			if ($nv !== false)
			{
				$result[$key] = $nv;
			}
		}
		return $result;
	}

	/**
	 * Decrement an item.
	 *
	 * @param  string $key
	 * @param  int $value
	 * @return int|bool The new value on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function decrementItem($key, $value)
	{
		if ($this->hasItem($key) && is_numeric($oldValue = $this->data[$key]))
		{
			$nv = $oldValue - $value;
			$this->data[$key] = $nv;
			return $nv;
		}
		return false;
	}

	/**
	 * Decrement multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Associative array of keys and new values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function decrementItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			$nv = $this->decrementItem($key, $value);
			if ($nv !== false)
			{
				$result[$key] = $nv;
			}
		}
		return $result;
	}

	/**
	 * @return \Zend\Cache\Storage\Capabilities
	 */
	public function getCapabilities()
	{
		if ($this->capabilities === null)
		{
			$capabilityMarker = new \stdClass();
			$this->capabilities = new \Zend\Cache\Storage\Capabilities(
				$this, $capabilityMarker,
				[
					'supportedDatatypes' => [
						'NULL' => true,
						'boolean' => true,
						'integer' => true,
						'double' => true,
						'string' => true,
						'array' => true,
						'object' => true,
						'resource' => true,
					],
					'supportedMetadata' => [],
					'minTtl' => 1,
					'maxTtl' => 0,
					'staticTtl' => true,
					'ttlPrecision' => 1,
					'useRequestTime' => false,
					'expiredRead' => false,
					'maxKeyLength' => 255,
					'namespaceIsPrefix' => false,
				]
			);
		}
	}

	/**
	 * Flush the whole storage
	 *
	 * @return bool
	 */
	public function flush()
	{
		$this->data = [];
	}
}