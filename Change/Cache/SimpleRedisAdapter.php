<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Cache;

/**
 * @name \Change\Cache\SimpleRedisAdapter
 */
class SimpleRedisAdapter implements \Zend\Cache\Storage\StorageInterface, \Zend\Cache\Storage\FlushableInterface,
	\Zend\Cache\Storage\ClearByNamespaceInterface, \Zend\Cache\Storage\ClearByPrefixInterface
{

	/**
	 * @var \Zend\Cache\Storage\Capabilities
	 */
	protected $capabilities;

	/**
	 * @var \Change\Cache\SimpleRedisOptions
	 */
	protected $options;

	/**
	 * @var \Redis
	 */
	protected $redis;

	protected $namespacePrefix = '';

	/**
	 * @param array $options
	 * @param \Redis|null $redis
	 */
	public function __construct(array $options, \Redis $redis = null)
	{
		$this->setOptions($options);
		if ($redis)
		{
			$this->redis = $redis;
		}
		else
		{
			$this->redis = new \Redis();
			$server = $this->getOptions()->getServer();
			if ($server['timeout'])
			{
				$ok = $this->redis->connect($server['host'], $server['port'], $server['timeout']);
			}
			elseif ($server['port'] ?? false)
			{
				$ok = $this->redis->connect($server['host'], $server['port']);
			}
			else
			{
				$ok = $this->redis->connect($server['host']);
			}

			if (!$ok)
			{
				$this->redis = null;
				throw new \RuntimeException('Unable to connect on redis server', 999999);
			}

			// If no database specified, use the default one (0). Explicit selection kills performances.
			$database = $this->getOptions()->getDatabase();
			if ($database !== false)
			{
				$this->redis->select($database);
			}
			$this->redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
		}
	}

	/**
	 * @return string
	 */
	public function getNamespacePrefix()
	{
		return $this->namespacePrefix;
	}

	/**
	 * @param string $namespacePrefix
	 * @return $this
	 */
	public function setNamespacePrefix($namespacePrefix)
	{
		$this->namespacePrefix = $namespacePrefix;
		return $this;
	}

	/**
	 * @param array $options
	 * @return $this
	 */
	public function setOptions($options)
	{
		/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		$options = new \Change\Cache\SimpleRedisOptions($options, $this);
		if ($this->options)
		{
			$this->options->setAdapter(null);
		}
		$this->options = $options;
		return $this;
	}

	/**
	 * @return \Change\Cache\SimpleRedisOptions
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Get an item.
	 *
	 * @param  string $key
	 * @param  bool $success
	 * @param  mixed $casToken
	 * @return mixed Data on success, null on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getItem($key, &$success = null, &$casToken = null)
	{
		$value = $this->redis->get($this->namespacePrefix . $key);
		if ($value === false)
		{
			$success = false;
			return null;
		}
		$success = true;
		$casToken = $value;
		return $value;
	}

	/**
	 * Get multiple items.
	 *
	 * @param  array $keys
	 * @return array Associative array of keys and values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getItems(array $keys)
	{
		if (!$keys)
		{
			return [];
		}
		$nmKeys = [];
		foreach ($keys as $key)
		{
			$nmKeys[] = $this->namespacePrefix . $key;
		}
		$results = $this->redis->mget($nmKeys);

		return array_filter(array_combine($keys, $results), function ($value) { return $value !== false; });
	}

	/**
	 * Test if an item exists.
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function hasItem($key)
	{
		return $this->redis->exists($this->namespacePrefix . $key);
	}

	/**
	 * Test multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of found keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function hasItems(array $keys)
	{
		$result = [];
		$redis = $this->redis;
		foreach ($keys as $key)
		{
			if ($redis->exists($this->namespacePrefix . $key))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Get metadata of an item.
	 *
	 * @param  string $key
	 * @return array|bool Metadata on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getMetadata($key)
	{
		$ttl = $this->redis->ttl($this->namespacePrefix . $key);
		if ($ttl == -2)
		{
			return false;
		}
		return ['ttl' => $ttl];
	}

	/**
	 * Get multiple metadata
	 *
	 * @param  array $keys
	 * @return array Associative array of keys and metadata
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function getMetadatas(array $keys)
	{
		$result = [];
		foreach ($keys as $key)
		{
			if ($metadata = $this->getMetadata($key))
			{
				$result[$key] = $metadata;
			}
		}
		return $result;
	}

	/**
	 * Store an item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function setItem($key, $value)
	{
		$ttl = $this->getOptions()->getTtl();
		if ($ttl)
		{
			$success = $this->redis->setex($this->namespacePrefix . $key, $ttl, $value);
		}
		else
		{
			$success = $this->redis->set($this->namespacePrefix . $key, $value);
		}
		return $success;
	}

	/**
	 * Store multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function setItems(array $keyValuePairs)
	{
		$result = [];
		$ttl = $this->getOptions()->getTtl();
		$redis = $this->redis;
		foreach ($keyValuePairs as $key => $value)
		{
			if ($ttl)
			{
				$success = $redis->setex($this->namespacePrefix . $key, $ttl, $value);
			}
			else
			{
				$success = $redis->set($this->namespacePrefix . $key, $value);
			}
			if (!$success)
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Add an item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function addItem($key, $value)
	{
		return $this->redis->setnx($this->namespacePrefix . $key, $value);
	}

	/**
	 * Add multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function addItems(array $keyValuePairs)
	{
		$result = [];
		$redis = $this->redis;
		foreach ($keyValuePairs as $key => $value)
		{
			if (!$redis->setnx($this->namespacePrefix . $key, $value))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Replace an existing item.
	 *
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function replaceItem($key, $value)
	{
		if ($this->hasItem($key))
		{
			return $this->setItem($key, $value);
		}
		return false;
	}

	/**
	 * Replace multiple existing items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Array of not stored keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function replaceItems(array $keyValuePairs)
	{
		$result = [];
		foreach ($keyValuePairs as $key => $value)
		{
			if (!$this->replaceItem($key, $value))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Set an item only if token matches
	 *
	 * It uses the token received from getItem() to check if the item has
	 * changed before overwriting it.
	 *
	 * @param  mixed $token
	 * @param  string $key
	 * @param  mixed $value
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 * @see    getItem()
	 * @see    setItem()
	 */
	public function checkAndSetItem($token, $key, $value)
	{
		return false;
	}

	/**
	 * Reset lifetime of an item
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function touchItem($key)
	{
		$ttl = $this->getOptions()->getTtl();
		return (bool)$this->redis->expire($this->namespacePrefix . $key, $ttl);
	}

	/**
	 * Reset lifetime of multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of not updated keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function touchItems(array $keys)
	{
		$result = [];
		$redis = $this->redis;
		$ttl = $this->getOptions()->getTtl();
		foreach ($keys as $key)
		{
			if (!(bool)$redis->expire($this->namespacePrefix . $key, $ttl))
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Remove an item.
	 *
	 * @param  string $key
	 * @return bool
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function removeItem($key)
	{
		return $this->redis->del($this->namespacePrefix . $key) == 1;
	}

	/**
	 * Remove multiple items.
	 *
	 * @param  array $keys
	 * @return array Array of not removed keys
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function removeItems(array $keys)
	{
		$result = [];
		$redis = $this->redis;
		foreach ($keys as $key)
		{
			if ($redis->del($this->namespacePrefix . $key) != 1)
			{
				$result[] = $key;
			}
		}
		return $result;
	}

	/**
	 * Increment an item.
	 *
	 * @param  string $key
	 * @param  int $value
	 * @return int|bool The new value on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function incrementItem($key, $value)
	{
		return $this->redis->incrBy($this->namespacePrefix . $key, $value);
	}

	/**
	 * Increment multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Associative array of keys and new values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function incrementItems(array $keyValuePairs)
	{
		$result = [];
		$redis = $this->redis;
		foreach ($keyValuePairs as $key => $value)
		{
			$nv = $redis->incrBy($this->namespacePrefix . $key, $value);
			if ($nv !== false)
			{
				$result[$key] = $nv;
			}
		}
		return $result;
	}

	/**
	 * Decrement an item.
	 *
	 * @param  string $key
	 * @param  int $value
	 * @return int|bool The new value on success, false on failure
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function decrementItem($key, $value)
	{
		return $this->redis->decrBy($this->namespacePrefix . $key, $value);
	}

	/**
	 * Decrement multiple items.
	 *
	 * @param  array $keyValuePairs
	 * @return array Associative array of keys and new values
	 * @throws \Zend\Cache\Exception\ExceptionInterface
	 */
	public function decrementItems(array $keyValuePairs)
	{
		$result = [];
		$redis = $this->redis;
		foreach ($keyValuePairs as $key => $value)
		{
			$nv = $redis->decrBy($this->namespacePrefix . $key, $value);
			if ($nv !== false)
			{
				$result[$key] = $nv;
			}
		}
		return $result;
	}

	/**
	 * @return \Zend\Cache\Storage\Capabilities
	 */
	public function getCapabilities()
	{
		if ($this->capabilities === null)
		{
			$capabilityMarker = new \stdClass();
			$this->capabilities = new \Zend\Cache\Storage\Capabilities(
				$this, $capabilityMarker,
				[
					'supportedDatatypes' => [
						'NULL' => 'string',
						'boolean' => 'string',
						'integer' => 'string',
						'double' => 'string',
						'string' => true,
						'array' => true,
						'object' => true,
						'resource' => false,
					],
					'supportedMetadata' => ['ttl'],
					'minTtl' => 1,
					'maxTtl' => 0,
					'staticTtl' => true,
					'ttlPrecision' => 1,
					'useRequestTime' => false,
					'expiredRead' => false,
					'maxKeyLength' => 255,
					'namespaceIsPrefix' => true,
				]
			);
		}
		return $this->capabilities;
	}

	/**
	 * Remove items of given namespace
	 *
	 * @param string $namespace
	 * @return bool
	 */
	public function clearByNamespace($namespace)
	{
		$redis = $this->redis;
		$it = null;
		$pattern = $this->namespacePrefix . '*';
		do
		{
			if ($arrKeys = $redis->scan($it, $pattern))
			{
				$redis->del($arrKeys);
			}
		}
		while ($it);
		return true;
	}

	/**
	 * Remove items matching given prefix
	 *
	 * @param string $prefix
	 * @return bool
	 */
	public function clearByPrefix($prefix)
	{
		$redis = $this->redis;
		$it = null;
		$pattern = $this->namespacePrefix . $prefix . '*';
		do
		{
			if ($arrKeys = $redis->scan($it, $pattern))
			{
				$redis->del($arrKeys);
			}
		}
		while ($it);
		return true;
	}

	/**
	 * Flush the whole storage
	 *
	 * @return bool
	 */
	public function flush()
	{
		return $this->redis->flushDB();
	}

	/**
	 * @return null|\Redis
	 */
	public function getRedisAdapter()
	{
		return $this->redis;
	}
}