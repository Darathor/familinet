<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Cache;

/**
 * @name \Change\Cache\SimpleRedisOptions
 */
class SimpleRedisOptions
{
	/**
	 * @var \Change\Cache\SimpleRedisAdapter
	 */
	protected $adapter;
	/**
	 * @var array
	 */
	protected $server;

	/**
	 * @var integer|false
	 */
	protected $database = false;

	/**
	 * @var integer
	 */
	protected $ttl = 0;

	/**
	 * @var string
	 */
	protected $namespace = '';

	/**
	 * @param array $options
	 * @param \Change\Cache\SimpleRedisAdapter $adapter
	 */
	public function __construct(array $options, \Change\Cache\SimpleRedisAdapter $adapter)
	{
		$this->adapter = $adapter;

		if (!isset($options['server']) || !is_array($options['server']))
		{
			$options['server'] = [];
		}
		$this->server = array_merge(['host' => '127.0.0.1', 'port' => 6379, 'timeout' => 0], $options['server']);
		if (isset($options['database']))
		{
			$this->setDatabase($options['database']);
		}

		if (isset($options['namespace']) && $options['namespace'] && is_string($options['namespace']))
		{
			$this->setNamespace(trim($options['namespace']));
		}

		if (isset($options['ttl']) && $options['ttl'])
		{
			$this->setTtl((int)$options['ttl']);
		}
	}

	/**
	 * @return SimpleRedisAdapter
	 */
	public function getAdapter()
	{
		return $this->adapter;
	}

	/**
	 * @param SimpleRedisAdapter $adapter
	 * @return $this
	 */
	public function setAdapter($adapter)
	{
		$this->adapter = $adapter;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getServer()
	{
		return $this->server;
	}

	/**
	 * @param array $server
	 * @return $this
	 */
	public function setServer(array $server)
	{
		$this->server = $server;
		return $this;
	}

	/**
	 * @return integer|false
	 */
	public function getDatabase()
	{
		return $this->database;
	}

	/**
	 * @param integer $database
	 * @return $this
	 */
	public function setDatabase($database)
	{
		$this->database = $database === false ? $database: (int)$database;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTtl()
	{
		return $this->ttl;
	}

	/**
	 * @param int $ttl
	 * @return $this
	 */
	public function setTtl($ttl)
	{
		$this->ttl = $ttl;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->namespace;
	}

	/**
	 * @param string $namespace
	 * @return $this
	 */
	public function setNamespace($namespace)
	{
		$this->namespace = $namespace;
		if ($this->adapter)
		{
			$this->adapter->setNamespacePrefix($namespace ? $namespace . ':' : '');
		}
		return $this;
	}
}