<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Change\Commands\ClearCache
 */
class ClearCache
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$response = $event->getCommandResponse();

		$cacheManager = $event->getApplicationServices()->getCacheManager();

		$clearAll = true;
		if ($event->getParam('files'))
		{
			$clearAll = false;
			$response->addInfoMessage('Clear local path: ' . $cacheManager->getLocalCacheDir());
			$cacheManager->clearLocalCache();
		}

		if ($event->getParam('namespace-list'))
		{
			$clearAll = false;
			$response->addInfoMessage('Valid namespaces: ' . implode(', ', $cacheManager->getNamespaces()));
		}

		if ($nameSpace = $event->getParam('namespace'))
		{
			$clearAll = false;
			if (!$cacheManager->isValidNamespace($nameSpace))
			{
				$response->addWarningMessage('Invalid namespace: ' . $nameSpace);
			}
			else
			{
				$response->addInfoMessage('Clear namespace: ' . $nameSpace);
				$cacheManager->clearNameSpace($nameSpace);
			}
		}

		if ($clearAll)
		{
			$response->addInfoMessage('Clear all cache');
			$cacheManager->clearAll();
		}
		$response->addInfoMessage('Done.');
	}
}