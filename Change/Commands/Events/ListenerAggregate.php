<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands\Events;

/**
 * @name \Change\Commands\Events\ListenerAggregate
 * @ignore
 */
class ListenerAggregate extends \Zend\EventManager\AbstractListenerAggregate
{

	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('config', function (Event $event)
		{
			$changeCommandConfigPath = $event->getApplication()->getWorkspace()->changePath('Commands', 'Assets', 'config.json');
			if (is_file($changeCommandConfigPath))
			{
				return json_decode(file_get_contents($changeCommandConfigPath), true);
			}
			return null;
		});

		$this->listeners[] = $events->attach('change:clear-cache', function ($event)
		{
			(new \Change\Commands\ClearCache())->execute($event);
		});

		$this->listeners[] = $events->attach('change:compile-documents', function ($event)
		{
			(new \Change\Commands\CompileDocuments())->execute($event);
		});


		$this->listeners[] = $events->attach('change:generate-db-schema', function ($event)
		{
			(new \Change\Commands\GenerateDbSchema())->execute($event);
		});
		$this->listeners[] = $events->attach('change:generate-db-indexes', function ($event)
		{
			(new \Change\Commands\GenerateDbIndexes())->execute($event);
		});

		$this->listeners[] = $events->attach('change:set-document-root', function ($event)
		{
			(new \Change\Commands\SetDocumentRoot())->execute($event);
		});


		$this->listeners[] = $events->attach('change:install-package', function ($event)
		{
			(new \Change\Commands\InstallPackage())->execute($event);
		}, 5);


		$this->listeners[] = $events->attach('change:compile-plugins-registration', function ($event)
		{
			(new \Change\Commands\CompilePluginsRegistration())->execute($event);
		}, 5);


		$this->listeners[] = $events->attach('change:install-plugin', function ($event)
		{
			(new \Change\Commands\InstallPlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:disable-plugin', function ($event)
		{
			(new \Change\Commands\DisablePlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:enable-plugin', function ($event)
		{
			(new \Change\Commands\EnablePlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:deinstall-plugin', function ($event)
		{
			(new \Change\Commands\DeinstallPlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:deregister-plugin', function ($event)
		{
			(new \Change\Commands\DeregisterPlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:register-plugin', function ($event)
		{
			(new \Change\Commands\RegisterPlugin())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:manage-cache', function ($event)
		{
			(new \Change\Commands\ManageCache())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:refresh-i18n', function ($event)
		{
			(new \Change\Commands\RefreshI18n())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:refresh-document-cache', function ($event)
		{
			(new \Change\Commands\RefreshDocumentCache())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:patch', function ($event)
		{
			(new \Change\Commands\Patch())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:job', function ($event)
		{
			(new \Change\Commands\Job())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:import', function ($event)
		{
			(new \Change\Commands\Import())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:export', function ($event)
		{
			(new \Change\Commands\Export())->execute($event);
		}, 5);

		$this->listeners[] = $events->attach('change:install-plugins', function ($event) {
			(new \Change\Commands\InstallPlugins())->execute($event);
		});
	}
}