<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

/**
 * @name \Change\Commands\Export
 */
class Export
{
	/**
	 * input param: context baseDirectory model offset ids chunk
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		//Enable Change storage system
		$event->getApplicationServices()->getStorageManager();
		$response = $event->getCommandResponse();

		$baseDirectory = $event->getParam('baseDirectory');
		if ($baseDirectory && is_string($baseDirectory))
		{
			$baseDirectory = rtrim($baseDirectory, '/');
			if ($baseDirectory && substr($baseDirectory, 0, 9) !== 'change://')
			{
				$baseDirectory = $event->getApplication()->getWorkspace()->composeAbsolutePath($baseDirectory);
				\Change\Stdlib\FileUtils::mkdir($baseDirectory);
			}
		}

		$applicationServices = $event->getApplicationServices();

		$exportEngine = new \Change\Synchronization\ExportEngine($event->getApplication(),
			['contextCode' => $event->getParam('context')]);

		$verbose = $event->getParam('verbose');

		$model = $event->getParam('model');
		$ids = $event->getParam('ids');
		$chunk = (int)$event->getParam('chunk');
		$offset = $event->getParam('offset');

		$limit = $event->getParam('limit');
		if ($limit === null || is_string($ids))
		{
			$limit = -1;
		}
		else
		{
			$limit = (int)$limit;
		}
		$start = (new \DateTime())->format('_Ymd_His_');

		$encodeOption = JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE;
		$chunkIndex = 0;
		$filter = ['offset' => $offset, 'ids' => $ids];
		while (true)
		{
			$items = $exportEngine->getItems($model, $chunk, $filter);
			$json = [];
			foreach ($items as $item)
			{
				$applicationServices->getDocumentManager()->reset();
				$json[] = $exportEngine->exportItem($item);
			}
			if (!$json)
			{
				break;
			}

			$fileName = $model . $start . str_pad((string)$chunkIndex, 5, '0', STR_PAD_LEFT) . '.json';

			if ($baseDirectory)
			{
				$filePath = $baseDirectory . DIRECTORY_SEPARATOR . $fileName;
				$response->addCommentMessage($filePath);
				file_put_contents($filePath, json_encode($json, $encodeOption));
			}
			else
			{
				if ($verbose)
				{
					$response->addCommentMessage($fileName);
				}
				$response->addInfoMessage(json_encode($json, $verbose ? $encodeOption + JSON_PRETTY_PRINT : $encodeOption));
			}

			$chunkIndex++;
			if (($limit > 0 && (($chunk * $chunkIndex) >= $limit)) || count($items) !== $chunk)
			{
				break;
			}
		}

		$exportEngine->finalize();
	}
}