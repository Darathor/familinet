<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

/**
 * @name \Change\Commands\GenerateDbIndexes
 */
class GenerateDbIndexes
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$verbose = $event->getParam('verbose');
		$run = $event->getParam('run');
		$cleanUp = $event->getParam('clean-up');

		$application = $event->getApplication();
		$workspace = $application->getWorkspace();
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();
		$pluginManager = $applicationServices->getPluginManager();

		$indexes = $this->buildIndexesDefinition($workspace, $pluginManager, $response, $verbose);

		$schemaManager = $applicationServices->getDbProvider()->getSchemaManager();
		$totalChange = $this->applyIndexes($indexes, $cleanUp, $run, $schemaManager, $response, $verbose);

		if ($totalChange)
		{
			if ($run)
			{
				$response->addInfoMessage('Done with ' . $totalChange . ' indexes modified.');
			}
			else
			{
				$response->addInfoMessage('Done with ' . $totalChange . ' indexes to modify. Use --run option.');
			}
		}
		else
		{
			$response->addInfoMessage('Done with no modification.');
		}
	}

	/**
	 * @param \Change\Db\Schema\KeyDefinition $definition
	 * @return string
	 */
	protected function buildDigest(\Change\Db\Schema\KeyDefinition $definition)
	{
		$p = [$definition->getName(), $definition->getType()];
		foreach ($definition->getFields() as $field)
		{
			$p[] = $field->getName();
		}
		return implode(',', $p);
	}

	protected function mergeIndexes(array $indexes, array $toAddIndexes)
	{
		foreach ($toAddIndexes as $table => $data)
		{
			$tableIndexes = $data['indexes'] ?? [];
			if ($tableIndexes && is_array($tableIndexes))
			{
				foreach ($tableIndexes as $indexName => $index)
				{
					if ($index && isset($index['fields'][0], $index['type']))
					{
						$indexes[$table]['indexes'][$indexName] = $index;
					}
					else
					{
						unset($indexes[$table]['indexes'][$indexName]);
					}
				}
			}
		}
		return $indexes;
	}

	/**
	 * @param \Change\Workspace $workspace
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @param boolean $verbose
	 * @return array
	 */
	protected function buildIndexesDefinition($workspace, $pluginManager, $response, $verbose)
	{
		$filePath = $workspace->changePath('Assets', 'Db', 'tablesIndexes.json');
		if ($verbose)
		{
			$response->addCommentMessage('Add: ' . $filePath);
		}
		/** @var array $indexes */
		$indexes = json_decode(file_get_contents($filePath), true);

		foreach ($pluginManager->getModules() as $module)
		{
			$filePath = $module->getAbsolutePath() . '/Setup/Assets/Db/tablesIndexes.json';
			if (file_exists($filePath))
			{
				$json = json_decode(file_get_contents($filePath), true);
				if ($json && is_array($json))
				{
					$indexes = $this->mergeIndexes($indexes, $json);
					if ($verbose)
					{
						$response->addCommentMessage('Add: ' . $filePath);
					}
				}
				else
				{
					$response->addErrorMessage('Invalid file: ' . $filePath);
				}
			}
		}

		$filePath = $workspace->appPath('Override', 'Db', 'tablesIndexes.json');
		if (file_exists($filePath))
		{
			$json = json_decode(file_get_contents($filePath), true);
			if ($json && is_array($json))
			{
				$indexes = $this->mergeIndexes($indexes, $json);
				if ($verbose)
				{
					$response->addCommentMessage('Add: ' . $filePath);
				}
			}
			else
			{
				$response->addErrorMessage('Invalid file: ' . $filePath);
			}
		}

		$nbTable = count($indexes);
		$nbIndex = 0;
		foreach ($indexes as $data)
		{
			$nbIndex += count($data['indexes']);
		}
		$response->addInfoMessage('Check ' . $nbTable . ' tables and ' . $nbIndex . ' indexes');

		return $indexes;
	}

	/**
	 * @param array $indexes
	 * @param boolean $cleanUp
	 * @param boolean $run
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @param boolean $verbose
	 * @return integer
	 */
	protected function applyIndexes($indexes, $cleanUp, $run, $schemaManager, $response, $verbose)
	{
		if ($cleanUp)
		{
			$schemaManager->getOptions()->set('dropUndefinedKeys', true);
		}

		$totalChange = 0;
		foreach ($indexes as $table => $tableIndexes)
		{
			$currentTable = $schemaManager->getTableDefinition($table);
			if (!$currentTable)
			{
				$response->addWarningMessage('Table ' . $table . ' not found');
				continue;
			}
			$newTable = clone $currentTable;
			$keys = $newTable->getKeys();
			$oldKeys = [];
			$newTable->setKeys([]);
			foreach ($keys as $definition)
			{
				if ($definition->isPrimary())
				{
					$newTable->addKey($definition);
				}
				else
				{
					$oldKeys[$definition->getName()] = $definition;
				}
			}

			$nbChange = 0;

			/** @noinspection ForeachSourceInspection */
			foreach ($tableIndexes['indexes'] as $indexName => $indexData)
			{
				$definition = new \Change\Db\Schema\KeyDefinition();
				$definition->setName($indexName);
				$definition->setType($indexData['type'] === 'UNIQUE' ? \Change\Db\Schema\KeyDefinition::UNIQUE :
					\Change\Db\Schema\KeyDefinition::INDEX);
				$fields = [];
				foreach ($indexData['fields'] as $fieldName)
				{
					$field = $newTable->getField($fieldName);
					if (!$field)
					{
						$response->addErrorMessage('Field ' . $fieldName . ' not found on table ' . $table);
						$fields = [];
						break;
					}
					$fields[] = $field;
				}
				if ($fields)
				{
					$definition->setFields($fields);
					$newTable->addKey($definition);
					$oldDefinition = $oldKeys[$indexName] ?? null;
					if ($oldDefinition)
					{
						if ($this->buildDigest($oldDefinition) === $this->buildDigest($definition))
						{
							if ($verbose)
							{
								$response->addCommentMessage('Unmodified index: ' . $table . '::' . $indexName);
							}
						}
						else
						{
							$nbChange++;
							$response->addInfoMessage('Updated index: ' . $table . '::' . $indexName);
						}
						unset($oldKeys[$indexName]);
					}
					else
					{
						$nbChange++;
						$response->addInfoMessage('Added index: ' . $table . '::' . $indexName);
					}
				}
			}
			if ($cleanUp)
			{
				foreach ($oldKeys as $oldIndexName => $oldDefinition)
				{
					$nbChange++;
					$response->addInfoMessage('Dropped index: ' . $table . '::' . $oldIndexName);
				}
			}

			$totalChange += $nbChange;
			if ($nbChange && $run)
			{
				$schemaManager->alterTable($newTable, $currentTable);
			}
		}

		return $totalChange;
	}
}