<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;


/**
 * @name \Change\Commands\InstallPackage
 */
class InstallPackage
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		$pluginManager = $applicationServices->getPluginManager();
		$vendor = $pluginManager->normalizeVendorName($event->getParam('vendor'));
		$shortName = $event->getParam('name');

		$allSteps = $pluginManager->getInstallSteps();
		$inputSteps = array_count_values(array_filter(array_map(function ($s)
		{
			return strtolower(trim($s));
		}, explode(',', $event->getParam('steps')))));

		if (isset($inputSteps['all']))
		{
			$steps = $allSteps;
		}
		else
		{
			$steps = [];
			foreach ($allSteps as $step)
			{
				if (isset($inputSteps[strtolower($step)]))
				{
					$steps[] = $step;
				}
			}
			if (!$steps)
			{
				$response->addErrorMessage('No valid step defined!');
				return;
			}
		}
		$response->addInfoMessage('Installation of package ' . $vendor . '_' . $shortName . ', steps: ' . implode(', ', $steps));

		$plugins = [];
		foreach ($pluginManager->getPlugins() as $plugin)
		{
			if ($plugin->getVendor() === $vendor && $plugin->getPackage() === $shortName)
			{
				$plugins[] = $plugin;
			}
		}
		if (!$plugins)
		{
			$response->addErrorMessage('Package ' . $vendor .'_'. $shortName . ' not found.');
			return;
		}

		$rc1 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
			$event->setParam('_start', null);
			if (\in_array($en = $event->getName(), ['setupApplication', 'setupDbSchema', 'setupServices'], true))
			{
				$en = $en === 'setupApplication' ? ($event->getParam('configuration') ? 'setupConfiguration' : 'setupAssets') : $en;
				$event->setParam('_start', microtime(true));
				$event->setParam('_name', $en);
				$pn = $event->getParam('plugin');
				$response->addCommentMessage(($pn ? $pn->getName() : '') . ' START ' . $en);
			}
		}, 1000);

		$rc2 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
			if ($last = $event->getParam('_start'))
			{
				$current = microtime(true);
				$pn = $event->getParam('plugin');
				$response->addCommentMessage(($pn ? $pn->getName() : '') . ' END ' . $event->getParam('_name') . ' (' . round($current - $last, 3) . 's)');
			}
		}, -1000);

		$plugins = $pluginManager->installPlugins($plugins, ['steps' => $steps]);
		$response->addInfoMessage(\count($plugins) . ' plugin(s) installed.');

		$pluginManager->getEventManager()->detach($rc1);
		$pluginManager->getEventManager()->detach($rc2);
	}
}