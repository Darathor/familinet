<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Change\Commands\InstallPlugin
 */
class InstallPlugin extends AbstractPluginCommand
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$this->initWithEvent($event);
		$type = $this->getType();
		$vendor = $this->getVendor();
		$shortName = $this->getShortName();

		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		$pluginManager = $applicationServices->getPluginManager();

		$toInstall = $pluginManager->getPlugin($type, $vendor, $shortName);

		if ($toInstall && $toInstall->getRegistrationDate())
		{
			$allSteps = $pluginManager->getInstallSteps();
			$inputSteps = array_count_values(array_filter(array_map(function ($s) {
				return strtolower(trim($s));
			}, explode(',', $event->getParam('steps')))));

			if (isset($inputSteps['all']))
			{
				$steps = $allSteps;
			}
			else
			{
				$steps = [];
				foreach ($allSteps as $step)
				{
					if (isset($inputSteps[strtolower($step)]))
					{
						$steps[] = $step;
					}
				}
				if (!$steps)
				{
					$response->addErrorMessage('No valid step defined!');
					return;
				}
			}

			$rc1 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
				$event->setParam('_start', null);
				if (\in_array($en = $event->getName(), ['setupApplication', 'setupDbSchema', 'setupServices'], true))
				{
					$en = $en === 'setupApplication' ? ($event->getParam('configuration') ? 'setupConfiguration' : 'setupAssets') : $en;
					$event->setParam('_start', microtime(true));
					$event->setParam('_name', $en);
					$pn = $event->getParam('plugin');
					$response->addCommentMessage(($pn ? $pn->getType() . ' ' . $pn->getName() : '') . ' START ' . $en);
				}
			}, 1000);

			$rc2 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
				if ($last = $event->getParam('_start'))
				{
					$current = microtime(true);
					$pn = $event->getParam('plugin');
					$response->addCommentMessage(($pn ? $pn->getType() . ' ' . $pn->getName() : '') . ' END ' . $event->getParam('_name') . ' ('
						. round($current - $last, 3) . 's)');
				}
			}, -1000);

			$plugins = $pluginManager->installPlugin($toInstall, ['steps' => $steps]);

			$response->addInfoMessage(\count($plugins) . ' plugin(s) installed');

			$pluginManager->getEventManager()->detach($rc1);
			$pluginManager->getEventManager()->detach($rc2);
		}
		else
		{
			$response->addErrorMessage('Plugin does not exist');
		}
	}
}