<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

/**
 * @name \Change\Commands\InstallPlugins
 */
class InstallPlugins
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		$pluginManager = $applicationServices->getPluginManager();
		$pluginsToInstall = $event->getParam('plugins');

		$allSteps = $pluginManager->getInstallSteps();
		$inputSteps = array_count_values(array_filter(array_map(function ($s) {
			return strtolower(trim($s));
		}, explode(',', $event->getParam('steps')))));

		if (isset($inputSteps['all']))
		{
			$steps = $allSteps;
		}
		else
		{
			$steps = [];
			foreach ($allSteps as $step)
			{
				if (isset($inputSteps[strtolower($step)]))
				{
					$steps[] = $step;
				}
			}
			if (!$steps)
			{
				$response->addErrorMessage('No valid step defined!');
				return;
			}
		}
		$plugins = [];
		if ($pluginsToInstall === 'All')
		{
			$plugins = $pluginManager->getPlugins();
		}
		elseif ($pluginsToInstall === 'Modules')
		{
			$plugins = $pluginManager->getModules();
		}
		elseif ($pluginsToInstall === 'Themes')
		{
			$plugins = $pluginManager->getThemes();
		}
		else
		{
			$names = array_map('trim', explode(',', $pluginsToInstall));
			foreach ($names as $name)
			{
				$p = explode('_', $name);
				if (\count($p) === 2)
				{
					$vendor = $pluginManager->normalizeVendorName($p[0]);
					$shortName = $pluginManager->normalizePluginName($p[1]);

					foreach ($pluginManager->getPlugins() as $plugin)
					{
						if ($plugin->getShortName() === $shortName && $plugin->getVendor() === $vendor)
						{
							$plugins[] = $plugin;
							continue 2;
						}
					}
				}
				$response->addErrorMessage('Plugin "' . $name . '" not found.');
			}
		}

		if (!$plugins)
		{
			$response->addWarningMessage('No plugin to install.');
			return;
		}

		$rc1 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
			$event->setParam('_start', null);
			if (\in_array($en = $event->getName(), ['setupApplication', 'setupDbSchema', 'setupServices'], true))
			{
				$en = $en === 'setupApplication' ? ($event->getParam('configuration') ? 'setupConfiguration' : 'setupAssets') : $en;
				$event->setParam('_start', microtime(true));
				$event->setParam('_name', $en);
				$pn = $event->getParam('plugin');
				$response->addCommentMessage(($pn ? $pn->getType() . ' ' .$pn->getName() : '') . ' START ' . $en);
			}
		}, 1000);

		$rc2 = $pluginManager->getEventManager()->attach('*', function (\Change\Events\Event $event) use ($response) {
			if ($last = $event->getParam('_start'))
			{
				$current = microtime(true);
				$pn = $event->getParam('plugin');
				$response->addCommentMessage(($pn ? $pn->getType() . ' ' . $pn->getName() : '') . ' END ' . $event->getParam('_name') . ' (' . round($current - $last, 3) . 's)');
			}
		}, -1000);

		$plugins = $pluginManager->installPlugins($plugins, ['steps' => $steps]);
		$response->addInfoMessage(\count($plugins) . ' plugin(s) installed.');

		$pluginManager->getEventManager()->detach($rc1);
		$pluginManager->getEventManager()->detach($rc2);
	}
}