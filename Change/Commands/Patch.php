<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Change\Commands\Patch
 */
class Patch
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$run = $event->getParam('run');
		$patchManager = new \Change\Plugins\Patch\PatchManager();
		$patchManager->setApplication($event->getApplication());

		$commandResponse = $event->getCommandResponse();
		$verbose = $event->getParam('verbose');

		$patchManager->getEventManager()->attach(\Change\Plugins\Patch\PatchManager::EVENT_APPLY,
			function (\Change\Events\Event $event) use ($commandResponse, $verbose)
			{
				$event->setParam('verbose', $verbose);
				$event->setParam('commandResponse', $commandResponse);
			}, 100);

		$patches = $patchManager->apply($run);
		$response = $event->getCommandResponse();
		if (!$run)
		{
			if (!count($patches))
			{
				$response->addInfoMessage('No patch to install');
				return;
			}
			$response->addInfoMessage('Patch to install: ' . count($patches));
		}
	}
}