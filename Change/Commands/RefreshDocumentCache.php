<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Change\Commands\RefreshDocumentCache
 */
class RefreshDocumentCache
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$cacheManager = $applicationServices->getCacheManager();
		$response = $event->getCommandResponse();
		if (!$cacheManager->isValidNamespace('prefetch'))
		{
			$response->addWarningMessage('Cache configuration entry "Change/Cache/Namespaces/prefetch" is disabled!');
			return;
		}


		$models = explode(',', (string)$event->getParam('models'));
		$modelNames = array_filter(array_map('trim', $models));
		if (!$modelNames)
		{
			$filters = [
				'abstract' => false,
				'stateless' => false,
				'inline' => false,
				'onlyInstalled' => true
			];
			$modelNames = $applicationServices->getModelManager()->getFilteredModelsNames($filters);
		}

		$documentCount = 0;
		$cachedCount = 0;

		$forceRefresh = $event->getParam('force');
		$documentManager->usePersistentCache(true);

		$serializer = new \Change\Documents\Serializer($documentManager);
		foreach ($modelNames as $modelName)
		{
			$model = $applicationServices->getModelManager()->getModelByName($modelName);
			if (!$model)
			{
				$response->addWarningMessage('model "'. $modelName .'" not found!"');
				continue;
			}
			$response->addInfoMessage('Refresh cache for' . $model . ' model...');

			$id = 0;
			$cacheOptions = ['ttl' => 36000];
			while (true)
			{
				$from = $id;
				$documentManager->reset();
				$q = $applicationServices->getDocumentManager()->getNewQuery($model);
				$q->andPredicates($q->gt('id', $id));
				$q->addOrder('id');
				$docs = $q->getDocuments(0, 50);
				foreach ($docs as $doc)
				{
					$id = $doc->getId();
					$documentCount++;
					if ($forceRefresh || !$doc->isLoaded())
					{
						$cachedCount++;
						$str = $serializer->serialize($doc);
						$cacheManager->setEntry('prefetch', (string)$id, $str, $cacheOptions);
					}
				}
				$response->addInfoMessage($from . ' .. ' . $id);
				if ($docs->count() < 50)
				{
					break;
				}
			}
		}
		$response->addInfoMessage($cachedCount . ' / ' .$documentCount .' documents cached.');
	}
}