<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Change\Commands\CompileI18n
 */
class RefreshI18n
{
	/**
	 * @param Event $event
	 */
	public function execute(Event $event)
	{
		if ($event->getName() === 'change:compile-i18n')
		{
			$event->getCommandResponse()->addWarningMessage('Use "refresh-i18n" instead');
		}

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();

		$i18nManager->refreshCoreI18n();
		foreach($applicationServices->getPluginManager()->getInstalledPlugins() as $plugin)
		{
			if ($plugin->isAvailable())
			{
				$i18nManager->refreshPluginI18n($plugin);
			}
		}
		$event->getCommandResponse()->addInfoMessage('Done.');
	}
}