<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Commands;

use Change\Commands\Events\Event;


/**
 * @name \Change\Commands\RegisterPlugins
 */
class RegisterPlugin extends AbstractPluginCommand
{
	/**
	 * @param Event $event
	 * @throws \Exception
	 */
	public function execute(Event $event)
	{
		$this->initWithEvent($event);
		$type = $this->getType();
		$vendor = $this->getVendor();
		$shortName = $this->getShortName();

		$applicationServices = $event->getApplicationServices();

		$response = $event->getCommandResponse();

		$pluginManager = $applicationServices->getPluginManager();

		if ($event->getParam('force'))
		{
			$plugins =  $pluginManager->scanPlugins();
			$pluginManager->loadRegistration($plugins);
		}
		else
		{
			$plugins = $pluginManager->getUnregisteredPlugins();
		}

		if ($event->getParam('all'))
		{
			foreach ($plugins as $plugin)
			{
				$pluginManager->register($plugin);
				$response->addInfoMessage($plugin . ' registered');
			}

			$nbPlugins = count($plugins);
			$response->addInfoMessage($nbPlugins. ' new plugins registered');

			$plugins = $pluginManager->compile();
			$nbPlugins = count($plugins);
			$response->addInfoMessage($nbPlugins. ' plugins registered.');
		}
		else if (!$event->getParam('name'))
		{
			$response->addErrorMessage('You must at least specify a plugin name');
		}
		else
		{
			$found = false;
			foreach ($plugins as $plugin)
			{
				if ($plugin->getType() === $type && $plugin->getVendor() === $vendor && $plugin->getShortName() === $shortName)
				{
					$found = true;
					$pluginManager->register($plugin);
					$pluginManager->compile();
					$response->addInfoMessage('Done!');
				}
			}
			if (!$found)
			{
				$response->addErrorMessage('No such unregistered plugin!');
			}
		}
	}
}