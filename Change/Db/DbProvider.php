<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db;

use Change\Db\Query\AbstractQuery;
use Change\Db\Query\Builder;
use Change\Db\Query\StatementBuilder;
use Change\Logging\Logging;

/**
 * @name \Change\Db\DbProvider
 * @api
 */
abstract class DbProvider
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_SQL_FRAGMENT_STRING = 'SQLFragmentString';
	public const EVENT_SQL_FRAGMENT = 'SQLFragment';

	/**
	 * @var array
	 */
	protected $connectionInfos;

	/**
	 * @var array|null
	 */
	protected $readOnlyConnectionInfos;

	/**
	 * @var array
	 */
	protected $timers;

	/**
	 * @var \Change\Db\SqlMapping
	 */
	protected $sqlMapping;

	/**
	 * @var AbstractQuery[]
	 */
	protected $builderQueries;

	/**
	 * @var AbstractQuery[]
	 */
	protected $statementBuilderQueries;

	/**
	 * @var string[]
	 */
	protected $listenerAggregateClassNames;

	/**
	 * @var bool
	 */
	protected $readOnly = false;

	/**
	 * @var bool
	 */
	protected $disabled = false;

	/**
	 * @var bool
	 */
	protected $checkTransactionBeforeWriting = true;

	/**
	 * @var \Change\Db\Filters\Storage|null
	 */
	protected $filterStorage;

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Db'];
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		$classNames = [];
		foreach ($this->getEventManagerIdentifier() as $identifier)
		{
			$entry = $this->getApplication()->getConfiguredListenerClassNames('Change/Events/' . \str_replace('.', '/', $identifier));
			if (\is_array($entry))
			{
				foreach ($entry as $className)
				{
					if (\is_string($className))
					{
						$classNames[] = $className;
					}
				}
			}
		}
		return \array_unique($classNames);
	}

	/**
	 * @return string
	 */
	abstract public function getType();

	/**
	 * @param \Change\Application $application
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		if ($this->connectionInfos === null)
		{
			$this->connectionInfos = [];
			$dbConfig = $application->getConfiguration()->getEntry('Change/Db');
			$section = $dbConfig['use'] ?? 'default';
			$connectionInfos = $dbConfig[$section] ?? [];
			$this->setConnectionInfos($connectionInfos);
			if ($connectionInfos)
			{
				$readOnlyConnectionInfos = $dbConfig[$section . 'ReadOnly'] ?? null;
				if ($readOnlyConnectionInfos['enabled'] ?? false)
				{
					$this->setReadOnlyConnectionInfos($readOnlyConnectionInfos);
				}
			}
			else
			{
				$this->setDisabled(true);
			}
		}
	}

	public function __construct()
	{
		$this->timers = ['init' => \microtime(true), 'select' => 0, 'exec' => 0, 'longTransaction' => 0.2, 'roSelect' => 0];
	}

	/**
	 * @param bool $readOnly
	 * @return $this
	 */
	public function setReadOnly(bool $readOnly)
	{
		$this->readOnly = $readOnly;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getReadOnly()
	{
		return $this->readOnly;
	}

	/**
	 * @param bool $disabled
	 * @return $this
	 */
	public function setDisabled(bool $disabled)
	{
		$this->disabled = $disabled;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getDisabled()
	{
		return $this->disabled;
	}

	/**
	 * @return array
	 */
	public function getTimers()
	{
		return $this->timers ?: [];
	}

	/**
	 * @param bool $checkTransactionBeforeWriting
	 * @return $this
	 */
	public function setCheckTransactionBeforeWriting($checkTransactionBeforeWriting)
	{
		$this->checkTransactionBeforeWriting = $checkTransactionBeforeWriting;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getCheckTransactionBeforeWriting()
	{
		return $this->checkTransactionBeforeWriting;
	}

	/**
	 * @return array
	 */
	public function getConnectionInfos()
	{
		return $this->connectionInfos;
	}

	/**
	 * @param array $connectionInfos
	 */
	public function setConnectionInfos(array $connectionInfos)
	{
		$this->connectionInfos = $connectionInfos;
		if (isset($connectionInfos['longTransaction']))
		{
			$this->timers['longTransaction'] = (float)$connectionInfos['longTransaction'];
		}
	}

	/**
	 * @return array|null
	 */
	public function getReadOnlyConnectionInfos()
	{
		return $this->readOnlyConnectionInfos;
	}

	/**
	 * @param array|null $readOnlyConnectionInfos
	 * @return $this
	 */
	public function setReadOnlyConnectionInfos(array $readOnlyConnectionInfos = null)
	{
		$this->readOnlyConnectionInfos = $readOnlyConnectionInfos;
		return $this;
	}

	/**
	 * @param AbstractQuery $query
	 */
	public function addBuilderQuery(AbstractQuery $query)
	{
		if ($query->getCachedKey() !== null)
		{
			$this->builderQueries[$query->getCachedKey()] = $query;
		}
	}

	/**
	 * @param null $cacheKey
	 * @return Builder
	 */
	public function getNewQueryBuilder($cacheKey = null)
	{
		$query = ($cacheKey !== null && isset($this->builderQueries[$cacheKey])) ? $this->builderQueries[$cacheKey] : null;
		return new Builder($this, $cacheKey, $query);
	}

	/**
	 * @param AbstractQuery $query
	 */
	public function addStatementBuilderQuery(AbstractQuery $query)
	{
		if ($query->getCachedKey() !== null)
		{
			$this->statementBuilderQueries[$query->getCachedKey()] = $query;
		}
	}

	/**
	 * @param string $cacheKey
	 * @throws \RuntimeException
	 * @return StatementBuilder
	 */
	public function getNewStatementBuilder($cacheKey = null)
	{
		$query = ($cacheKey !== null
			&& isset($this->statementBuilderQueries[$cacheKey])) ? $this->statementBuilderQueries[$cacheKey] : null;
		return new StatementBuilder($this, $cacheKey, $query);
	}

	public function __destruct()
	{
		unset($this->builderQueries, $this->statementBuilderQueries);
	}

	/**
	 * @return void
	 */
	abstract public function closeConnection();

	/**
	 * @return \Change\Db\InterfaceSchemaManager
	 */
	abstract public function getSchemaManager();

	/**
	 * @return \Change\Db\SqlMapping
	 */
	public function getSqlMapping()
	{
		if ($this->sqlMapping === null)
		{
			$this->sqlMapping = new SqlMapping();
		}
		return $this->sqlMapping;
	}

	/**
	 * @return Logging
	 */
	public function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	abstract public function beginTransaction($event = null);

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	abstract public function commit($event);

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	abstract public function rollBack($event);

	/**
	 * @param string $tableName
	 * @return int
	 */
	abstract public function getLastInsertId(string $tableName);

	/**
	 * @api
	 * @param Query\InterfaceSQLFragment $fragment
	 * @return string
	 */
	abstract public function buildSQLFragment(Query\InterfaceSQLFragment $fragment);

	/**
	 * @param Query\InterfaceSQLFragment $fragment
	 * @return string
	 */
	public function buildCustomSQLFragment(Query\InterfaceSQLFragment $fragment)
	{
		$event = new \Change\Events\Event(static::EVENT_SQL_FRAGMENT_STRING, $this, ['fragment' => $fragment]);
		$this->getEventManager()->triggerEvent($event);
		$sql = $event->getParam('sql');
		if (\is_string($sql))
		{
			return $sql;
		}
		$this->getLogging()->warn(__METHOD__ . '(' . \get_class($fragment) . ') not implemented');
		return $fragment->toSQL92String();
	}

	/**
	 * @param array $argument
	 * @return Query\InterfaceSQLFragment|null
	 */
	public function getCustomSQLFragment(array $argument = [])
	{
		$event = new \Change\Events\Event(static::EVENT_SQL_FRAGMENT, $this, $argument);
		$this->getEventManager()->triggerEvent($event);
		$fragment = $event->getParam('SQLFragment');
		if ($fragment instanceof Query\InterfaceSQLFragment)
		{
			return $fragment;
		}
		return null;
	}

	/**
	 * @param \Change\Db\Query\SelectQuery $selectQuery
	 * @return array
	 */
	abstract public function getQueryResultsArray(\Change\Db\Query\SelectQuery $selectQuery);

	/**
	 * @param AbstractQuery $query
	 * @return int
	 */
	abstract public function executeQuery(AbstractQuery $query);

	/**
	 * @param mixed $value
	 * @param int $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	abstract public function phpToDB($value, $scalarType);

	/**
	 * @param mixed $value
	 * @param int $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	abstract public function dbToPhp($value, $scalarType);

	/**
	 * @return \Change\Db\Filters\Storage
	 */
	public function getFilterStorage()
	{
		if (!$this->filterStorage)
		{
			$this->filterStorage = new \Change\Db\Filters\Storage($this, $this->getLogging());
		}
		return $this->filterStorage;
	}

	/**
	 * @param string|null $data
	 * @return string|null
	 */
	public function encrypt(string $data = null)
	{
		return $data;
	}

	/**
	 * @param string|null $data
	 * @return string|null
	 */
	public function decrypt(string $data = null)
	{
		return $data;
	}

	/**
	 * @param string|null $data
	 * @return string|null
	 */
	public function hash(string $data = null)
	{
		return $data === null ? $data : \hash('sha256', 'px_' . $data . '_ms');
	}
}