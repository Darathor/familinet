<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Filters;

/**
 * @name \Change\Db\Filters\Filter
 */
class Filter
{
	/**
	 * @var int
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $label;

	/**
	 * @var string
	 */
	protected $contextName;

	/**
	 * @var int
	 */
	protected $userId;

	/**
	 * @var array
	 */
	protected $content;

	/**
	 * @param string $contextName
	 * @param int $userId
	 * @param array $content
	 * @param string $label
	 * @param int $id
	 */
	public function __construct(string $contextName, int $userId, array $content, string $label, int $id = 0)
	{
		$this->id = $id;
		$this->label = $label;
		$this->contextName = $contextName;
		$this->userId = $userId;
		$this->content = $content;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId(int $id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param string $label
	 * @return $this
	 */
	public function setLabel(string $label)
	{
		$this->label = $label;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContextName()
	{
		return $this->contextName;
	}

	/**
	 * @param string $contextName
	 * @return $this
	 */
	public function setContextName(string $contextName)
	{
		$this->contextName = $contextName;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * @param int $userId
	 * @return $this
	 */
	public function setUserId(int $userId)
	{
		$this->userId = $userId;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param array $content
	 * @return $this
	 */
	public function setContent(array $content)
	{
		$this->content = $content;
		return $this;
	}
}