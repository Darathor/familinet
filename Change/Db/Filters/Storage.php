<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Filters;

/**
 * @name \Change\Db\Filters\Storage
 */
class Storage
{
	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Logging\Logging
	 */
	protected $logging;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param \Change\Logging\Logging $logging
	 */
	public function __construct(\Change\Db\DbProvider $dbProvider, \Change\Logging\Logging $logging)
	{
		$this->dbProvider = $dbProvider;
		$this->logging = $logging;
	}

	/**
	 * @param array $row Filter data from DB.
	 * @return \Change\Db\Filters\Filter
	 */
	protected function getFilterInstance($row)
	{
		$content = \json_decode($row['content'], true, 512, JSON_THROW_ON_ERROR);
		return new \Change\Db\Filters\Filter($row['context_name'], $row['user_id'], $content, $row['label'], $row['filter_id']);
	}

	/**
	 * @param int $id
	 * @return \Change\Db\Filters\Filter|null
	 */
	public function load($id)
	{
		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('filter_id'), $fb->column('context_name'), $fb->column('user_id'), $fb->column('content'), $fb->column('label'));
		$qb->from($fb->table('change_filters'));
		$qb->where($fb->eq($fb->column('filter_id'), $fb->parameter('id')));
		$sc = $qb->query();
		$sc->bindParameter('id', $id);

		return $sc->getFirstResult($this->getRowConverter());
	}

	/**
	 * @param string $contextName
	 * @return \Change\Db\Filters\Filter[]
	 */
	public function getByContext($contextName)
	{
		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('filter_id'), $fb->column('context_name'), $fb->column('user_id'), $fb->column('content'), $fb->column('label'));
		$qb->from($fb->table('change_filters'));
		$qb->where($fb->eq($fb->column('context_name'), $fb->parameter('contextName')));
		$sc = $qb->query();
		$sc->bindParameter('contextName', $contextName);

		return $sc->getResults($this->getRowsConverter());
	}

	/**
	 * @param string $contextName
	 * @param array $userIds
	 * @return \Change\Db\Filters\Filter[]
	 */
	public function getByContextForUserIds($contextName, array $userIds)
	{
		if (!$userIds)
		{
			throw new \RuntimeException('Invalid user ids');
		}

		$qb = $this->dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('filter_id'), $fb->column('context_name'), $fb->column('user_id'), $fb->column('content'), $fb->column('label'));
		$qb->from($fb->table('change_filters'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('context_name'), $fb->parameter('contextName')),
			$fb->in($fb->column('user_id'), $userIds)
		));
		$sc = $qb->query();
		$sc->bindParameter('contextName', $contextName);

		return $sc->getResults($this->getRowsConverter());
	}

	/**
	 * @param int|\Change\Db\Filters\Filter $filter
	 */
	public function delete($filter)
	{
		if ($filter instanceof \Change\Db\Filters\Filter)
		{
			$filter = $filter->getId();
		}

		if (!$filter || !\is_numeric($filter))
		{
			throw new \LogicException('Invalid filter id: ' . $filter);
		}

		$stmt = $this->dbProvider->getNewStatementBuilder();
		$fb = $stmt->getFragmentBuilder();
		$stmt->delete($fb->table('change_filters'));
		$stmt->where($fb->eq($fb->column('filter_id'), $fb->integerParameter('filter_id')));
		$query = $stmt->deleteQuery();
		$query->bindParameter('filter_id', $filter);
		$query->execute();
	}

	/**
	 * @param \Change\Db\Filters\Filter $filter
	 */
	public function insert(\Change\Db\Filters\Filter $filter)
	{
		$stmt = $this->dbProvider->getNewStatementBuilder();
		$fb = $stmt->getFragmentBuilder();

		$stmt->insert($fb->table('change_filters'), 'context_name', 'label', 'content', 'user_id');
		$stmt->addValues($fb->parameter('context_name'), $fb->parameter('label'), $fb->lobParameter('content'), $fb->integerParameter('user_id'));
		$query = $stmt->insertQuery();
		$query->bindParameter('context_name', $filter->getContextName());
		$query->bindParameter('label', $filter->getLabel());
		$query->bindParameter('content', \json_encode($filter->getContent(), JSON_THROW_ON_ERROR));
		$query->bindParameter('user_id', $filter->getUserId());
		$query->execute();

		$filter->setId($this->dbProvider->getLastInsertId('change_filters'));
	}

	/**
	 * @param \Change\Db\Filters\Filter $filter
	 */
	public function update(\Change\Db\Filters\Filter $filter)
	{
		$stmt = $this->dbProvider->getNewStatementBuilder();
		$fb = $stmt->getFragmentBuilder();

		$stmt->update($fb->table('change_filters'));
		$stmt->assign($fb->column('context_name'), $fb->parameter('context_name'));
		$stmt->assign($fb->column('label'), $fb->parameter('label'));
		$stmt->assign($fb->column('content'), $fb->lobParameter('content'));
		$stmt->where($fb->eq($fb->column('filter_id'), $fb->integerParameter('filter_id')));
		$query = $stmt->updateQuery();
		$query->bindParameter('filter_id', $filter->getId());
		$query->bindParameter('context_name', $filter->getContextName());
		$query->bindParameter('label', $filter->getLabel());
		$query->bindParameter('content', \json_encode($filter->getContent(), JSON_THROW_ON_ERROR));
		$query->execute();
	}

	/**
	 * @return callable
	 */
	protected function getRowConverter()
	{
		return static function ($row)
		{
			$content = \json_decode($row['content'], true, 512, JSON_THROW_ON_ERROR);
			return new \Change\Db\Filters\Filter($row['context_name'], $row['user_id'], $content, $row['label'], $row['filter_id']);
		};
	}

	/**
	 * @return callable
	 */
	protected function getRowsConverter()
	{
		return function ($rows)
		{
			$rowConverter = $this->getRowConverter();
			$filters = [];
			foreach ($rows as $row)
			{
				$filters[] = $rowConverter($row);
			}
			return $filters;
		};
	}
}