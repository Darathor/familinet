<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db;

/**
 * @name \Change\Db\InterfaceSchemaManager
 */
interface InterfaceSchemaManager
{
	/**
	 * @return string|NULL
	 */
	public function getName();

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getOptions();
	
	/**
	 * @return bool
	 */
	public function check();

	/**
	 * @return void
	 */
	public function closeConnection();

	/**
	 * @param string $sql
	 * @return int the number of affected rows
	 * @throws \Exception on error
	 */
	public function execute(string $sql);
	
	/**
	 * @param string $script
	 * @param bool $throwOnError
	 * @throws \Exception on error
	 */
	public function executeBatch(string $script, bool $throwOnError = false);
	
	/**
	 * @throws \Exception on error
	 */
	public function clearDB();
	
	/**
	 * @return string[]
	 */
	public function getTableNames();
	
	/**
	 * @param string $tableName
	 * @return \Change\Db\Schema\TableDefinition|null
	 */
	public function getTableDefinition(string $tableName);

	/**
	 * @param int $scalarType
	 * @param array $fieldDbOptions
	 * @param array $defaultDbOptions
	 * @throws \InvalidArgumentException
	 * @return array
	 */
	public function getFieldDbOptions(int $scalarType, array $fieldDbOptions = null, array $defaultDbOptions = null);
	
	
	/**
	 * @param string $tableName
	 * @return \Change\Db\Schema\TableDefinition
	 */
	public function newTableDefinition(string $tableName);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @throws \InvalidArgumentException
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newEnumFieldDefinition(string $name, array $dbOptions);
	
	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newCharFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newVarCharFieldDefinition(string $name, array $dbOptions = null);
	
	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newNumericFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newBooleanFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newIntegerFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newFloatFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newTextFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newLobFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newDateFieldDefinition(string $name, array $dbOptions = null);

	/**
	 * @param string $name
	 * @param array $dbOptions
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function newTimeStampFieldDefinition(string $name, array $dbOptions = null);
			
	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @return string SQL definition
	 */
	public function createOrAlterTable(\Change\Db\Schema\TableDefinition $tableDefinition);
	
	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @return string SQL definition
	 */
	public function createTable(\Change\Db\Schema\TableDefinition $tableDefinition);
	
	/**
	 * @param \Change\Db\Schema\TableDefinition $tableDefinition
	 * @param \Change\Db\Schema\TableDefinition $oldDef
	 * @return string SQL definition
	 */
	public function alterTable(\Change\Db\Schema\TableDefinition $tableDefinition, \Change\Db\Schema\TableDefinition $oldDef);

	/**
	 * @return \Change\Db\Schema\SchemaDefinition
	 */
	public function getSystemSchema();
}