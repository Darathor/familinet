<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Mysql;

use Change\Db\Query\AbstractQuery;
use Change\Db\Query\InterfaceSQLFragment;
use Change\Db\Query\SelectQuery;
use Change\Db\ScalarType;

/**
 * @name \Change\Db\Mysql\DbProvider
 * @api
 */
class DbProvider extends \Change\Db\DbProvider
{
	/**
	 * @var \Change\Db\Mysql\SchemaManager
	 */
	protected $schemaManager;
	
	/**
	 * @var \PDO instance provided by PDODatabase
	 */
	private $pdo;

	/**
	 * @var \PDO instance provided by PDODatabase
	 */
	private $pdoReadOnly = false;

	/**
	 * @var bool
	 */
	protected $inTransaction = false;

	/**
	 * @var null|array
	 */
	protected $registerShutDown;

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'mysql';
	}

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Db', 'Db.Mysql'];
	}

	/**
	 * @param \PDO|null $driver
	 * @return $this
	 */
	public function setDriver($driver)
	{
		if ($this->pdo && $this->inTransaction)
		{
			$this->pdo->commit();
		}

		$this->pdo = $driver;
		if ($driver === null)
		{
			$duration = \microtime(true) - $this->timers['init'];
			$this->timers['duration'] = $duration;
		}
		else
		{
			if ($this->inTransaction)
			{
				$this->pdo->beginTransaction();
			}

			if ($this->registerShutDown === null)
			{
				$this->registerShutDown = [$this, 'closeConnection'];
				\register_shutdown_function($this->registerShutDown);
			}
		}
		return $this;
	}
	
	/**
	 * @return \PDO
	 */
	public function getDriver()
	{
		if ($this->getReadOnly())
		{
			if ($this->pdoReadOnly === false)
			{
				$this->pdoReadOnly = null;
				$connectionInfos = $this->getReadOnlyConnectionInfos();
				if ($connectionInfos)
				{
					$this->pdoReadOnly = $this->getConnection($connectionInfos);
					if ($this->registerShutDown === null)
					{
						$this->registerShutDown = [$this, 'closeConnection'];
						\register_shutdown_function($this->registerShutDown);
					}
				}
			}
			if ($this->pdoReadOnly)
			{
				return $this->pdoReadOnly;
			}
		}
		if ($this->pdo === null)
		{
			$this->setDriver($this->getConnection($this->connectionInfos));
		}
		return $this->pdo;
	}
	
	/**
	 * @return string
	 */
	protected function errorCode()
	{
		return $this->getDriver()->errorCode();
	}
	
	/**
	 * @return array("sqlstate" => ..., "errorcode" => ..., "errormessage" => ...)
	 */
	protected function getErrorParameters()
	{
		$errorInfo = $this->getDriver()->errorInfo();
		return ['sqlstate' => $errorInfo[0], 'errorcode' => $errorInfo[1], 'errormessage' => $errorInfo[2]];
	}
	
	/**
	 * @return string
	 */
	protected function errorInfo()
	{
		return \print_r($this->getDriver()->errorInfo(), true);
	}

	/**
	 * @param array $connectionInfo
	 * @throws \RuntimeException
	 * @return \PDO
	 */
	public function getConnection(array $connectionInfo)
	{
		$protocol = 'mysql';
		$dsnOptions = [];

		if (isset($connectionInfo['url']))
		{
			$url = $connectionInfo['url'];

			// If the field starts with 'ENV:', the following environment variable is used to configure MySQL
			if (\strpos($url, 'ENV:') === 0)
			{
				$mysql_url_var_name = \substr($url, 4);
				$envUrl = \getenv($mysql_url_var_name);
				if ($envUrl === false)
				{
					throw new \RuntimeException('Environment variable defined in database configuration is not set.', 999999);
				}
				$url = $envUrl;
			}

			$parsed_url = \parse_url($url);
			if ($parsed_url === false)
			{
				throw new \RuntimeException('Database URL is not valid, use mysql://user:password@host:port/dbname.', 999999);
			}

			// Remove the initial '/ or the URL path'
			$database = isset($parsed_url['path']) ? \substr($parsed_url['path'], 1) : null;
			$password = $parsed_url['pass'] ?? null;
			$username = $parsed_url['user'] ?? null;
			$port = $parsed_url['port'] ?? 3306;
			$host = $parsed_url['host'] ?? 'localhost';
			$dsnOptions[] = 'host=' . $host;
			$dsnOptions[] = 'port=' . $port;
			// Set connection infos properly
			$connectionInfo['database'] = $database;
			$connectionInfo['password'] = $password;
			$connectionInfo['user'] = $username;
			$connectionInfo['port'] = $port;
			$connectionInfo['host'] = $host;
		}
		else
		{
			$database = $connectionInfo['database'] ?? null;
			$password = $connectionInfo['password'] ?? null;
			$username = $connectionInfo['user'] ?? null;

			$unix_socket = $connectionInfo['unix_socket'] ?? null;
			if ($unix_socket !== null)
			{
				$dsnOptions[] = 'unix_socket=' . $unix_socket;
			}
			else
			{
				$host = $connectionInfo['host'] ?? 'localhost';
				$dsnOptions[] = 'host=' . $host;
				$port = $connectionInfo['port'] ?? 3306;
				$dsnOptions[] = 'port=' . $port;
			}
		}

		if ($database !== null)
		{
			$dsnOptions[] = 'dbname=' . $database;
		}
		else
		{
			throw new \RuntimeException('Database not defined', 31001);
		}

		$options = [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'utf8\' COLLATE \'utf8_unicode_ci\', TIME_ZONE=\'+00:00\''];
		if (isset($connectionInfo['emulatePrepare']) && !$connectionInfo['emulatePrepare'])
		{
			$options[\PDO::ATTR_EMULATE_PREPARES] = false;
		}

		$dsn = $protocol . ':' . \implode(';', $dsnOptions);
		$pdo = new \PDO($dsn, $username, $password, $options);
		$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(\PDO::ATTR_AUTOCOMMIT, 0);
		return $pdo;
	}

	/**
	 * @return void
	 */
	public function closeConnection()
	{
		if ($this->schemaManager)
		{
			$this->schemaManager->closeConnection();
		}
		$this->setDriver(null);
		$this->pdoReadOnly = null;
		$this->getLogging()->info('Close Connection: (ROS: ' . $this->timers['roSelect'] . ', S: ' . $this->timers['select'] . ', IUD: ' . $this->timers['exec'] . ')');
		$this->timers['exec'] = $this->timers['select'] = $this->timers['roSelect'] = 0;
	}

	/**
	 * @throws \RuntimeException
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function getSchemaManager()
	{
		if ($this->schemaManager === null)
		{
			if ($this->inTransaction)
			{
				throw new \RuntimeException('SchemaManager is not available during transaction', 999999);
			}
			$this->closeConnection();
			$this->schemaManager = new SchemaManager($this, $this->getLogging());
		}
		return $this->schemaManager;
	}
	
	/**
	 * @return bool
	 */
	public function inTransaction()
	{
		return $this->inTransaction;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function beginTransaction($event = null)
	{
		if ($event === null || $event->getParam('primary'))
		{
			if ($this->getReadOnly())
			{
				throw new \LogicException('Unable to start a transaction in read only database', 999999);
			}
			if ($this->inTransaction())
			{
				$this->getLogging()->warn(\get_class($this), ' while already in transaction');
				if ($this->getApplication()->inDevelopmentMode())
				{
					\ob_start();
					/** @noinspection ForgottenDebugOutputInspection */
					\debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
					$this->getLogging()->info(\ob_get_clean());
				}
			}
			else
			{
				$this->timers['bt'] = \microtime(true);
				$this->inTransaction = true;
				if ($this->pdo)
				{
					$this->pdo->beginTransaction();
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function commit($event)
	{
		if ($event && $event->getParam('primary'))
		{
			if (!$this->inTransaction())
			{
				$this->getLogging()->warn(__METHOD__, ' called while not in transaction');
			}
			else
			{
				$this->inTransaction = false;
				if ($this->pdo)
				{
					$this->pdo->commit();
				}
				$duration = \round(\microtime(true) - $this->timers['bt'], 4);
				$this->getLogging()->info('commit: ' . \number_format($duration, 3) . 's');
				if ($duration > $this->timers['longTransaction'])
				{
					$this->getLogging()->warn('Long Transaction detected > ', $this->timers['longTransaction']);
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function rollBack($event)
	{
		if ($event && $event->getParam('primary'))
		{
			if (!$this->inTransaction())
			{
				$this->getLogging()->warn(__METHOD__, ' called while not in transaction');
			}
			else
			{
				$this->inTransaction = false;
				if ($this->pdo)
				{
					$this->pdo->rollBack();
				}
				$duration = \round(\microtime(true) - $this->timers['bt'], 4);
				$this->getLogging()->info('rollBack: ' . \number_format($duration, 3) . 's');
				if ($duration > $this->timers['longTransaction'])
				{
					$this->getLogging()->warn('Long Transaction detected > ', $this->timers['longTransaction']);
				}
			}
		}
	}

	/**
	 * @param bool $readOnly
	 * @return $this
	 */
	public function setReadOnly($readOnly)
	{
		if ($readOnly && $this->inTransaction())
		{
			throw new \LogicException('Transaction started, unable to set connection as read only!');
		}
		parent::setReadOnly($readOnly);
		return $this;
	}

	/**
	 * @param string $tableName
	 * @return int
	 */
	public function getLastInsertId(string $tableName)
	{
		return (int)$this->getDriver()->lastInsertId($tableName);
	}

	/**
	 * @api
	 * @param InterfaceSQLFragment $fragment
	 * @return string
	 */
	public function buildSQLFragment(InterfaceSQLFragment $fragment)
	{
		$fragmentBuilder = new FragmentBuilder($this);
		return $fragmentBuilder->buildSQLFragment($fragment);
	}

	/**
	 * @param AbstractQuery $query
	 * @return string
	 */
	protected function buildQuery(AbstractQuery $query)
	{
		$fragmentBuilder = new FragmentBuilder($this);
		return $fragmentBuilder->buildQuery($query);
	}

	/**
	 * @param SelectQuery $selectQuery
	 * @throws \RuntimeException
	 * @throws \PDOException
	 * @return array
	 */
	public function getQueryResultsArray(SelectQuery $selectQuery)
	{
		if ($this->getDisabled())
		{
			throw new \RuntimeException('This DB provider is disabled!', 999999);
		}
		if ($selectQuery->getCachedSql() === null)
		{
			$selectQuery->setCachedSql($this->buildQuery($selectQuery));
		}

		$statement = $this->prepareStatement($selectQuery->getCachedSql());
		foreach ($selectQuery->getParameters() as $parameter)
		{
			/* @var $parameter \Change\Db\Query\Expressions\Parameter */
			$value = $this->phpToDB($selectQuery->getParameterValue($parameter->getName()), $parameter->getType());
			$statement->bindValue($this->buildSQLFragment($parameter), $value);
		}
		$select = ($this->pdoReadOnly && $this->getReadOnly()) ? 'roSelect' : 'select';
		$this->timers[$select]++;
		try
		{
			$statement->execute();
		}
		catch (\PDOException $e)
		{
			$paramValues = [];
			foreach ($selectQuery->getParameters() as $parameter)
			{
				$paramValues[] = $parameter->getName() . ': ' . $this->phpToDB($selectQuery->getParameterValue($parameter->getName()), $parameter->getType());
			}
			$this->getLogging()->error($selectQuery->getCachedSql(), \implode(', ', $paramValues));
			throw $e;
		}
		return $statement->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param AbstractQuery $query
	 * @throws \RuntimeException
	 * @throws \PDOException
	 * @return int
	 */
	public function executeQuery(AbstractQuery $query)
	{
		if ($this->getDisabled())
		{
			throw new \RuntimeException('This DB provider is disabled!', 999999);
		}
		if ($this->getReadOnly())
		{
			throw new \RuntimeException('This DB provider is read only!', 999999);
		}
		if (!$this->inTransaction && $this->getCheckTransactionBeforeWriting())
		{
			throw new \RuntimeException('No transaction started!', 999999);
		}

		if ($query->getCachedSql() === null)
		{
			$query->setCachedSql($this->buildQuery($query));
		}

		$statement = $this->prepareStatement($query->getCachedSql(), false);
		foreach ($query->getParameters() as $parameter)
		{
			/* @var $parameter \Change\Db\Query\Expressions\Parameter */
			$value = $this->phpToDB($query->getParameterValue($parameter->getName()), $parameter->getType());
			$statement->bindValue($this->buildSQLFragment($parameter), $value);
		}
		$this->timers['exec']++;
		try
		{
			$statement->execute();
		}
		catch (\PDOException $e)
		{
			$paramValues = [];
			foreach ($query->getParameters() as $parameter)
			{
				$paramValues[] = $parameter->getName()  . ': ' . $this->phpToDB($query->getParameterValue($parameter->getName()), $parameter->getType());
			}
			$this->getLogging()->error($query->getCachedSql(), \implode(', ', $paramValues));
			throw $e;
		}
		return $statement->rowCount();
	}
	
	/**
	 * @param string $sql
	 * @param bool $cursorFwdOnly
	 * @return \PDOStatement
	 */
	private function prepareStatement($sql, $cursorFwdOnly = true)
	{
		return $cursorFwdOnly ? $this->getDriver()->prepare($sql, [\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY]) :
			$this->getDriver()->prepare($sql);
	}

	/**
	 * @param mixed $value
	 * @param int $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public function phpToDB($value, $scalarType)
	{
		switch ($scalarType)
		{
			case ScalarType::BOOLEAN :
				return $value ? 1 : 0;
			case ScalarType::INTEGER :
				if ($value !== null)
				{
					if (\is_object($value) && \is_callable([$value, 'getId']))
					{
						$value = $value->getId();
					}
					return (int)$value;
				}
				break;
			case ScalarType::DECIMAL :
				if ($value !== null)
				{
					return (float)$value;
				}
				break;
			case ScalarType::DATETIME :
				if ($value instanceof \DateTime)
				{
					$value->setTimezone(new \DateTimeZone('UTC'));
					return $value->format('Y-m-d H:i:s');
				}
				break;
		}
		return $value;
	}
	
	/**
	 * @param mixed $value
	 * @param int $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public function dbToPhp($value, $scalarType)
	{
		switch ($scalarType)
		{
			case ScalarType::BOOLEAN :
				return ($value === '1');
			case ScalarType::INTEGER :
				if ($value !== null)
				{
					return (int)$value;
				}
				break;
			case ScalarType::DECIMAL :
				if ($value !== null)
				{
					return (float)$value;
				}
				break;
			case ScalarType::DATETIME :
				if ($value !== null)
				{
					return \DateTime::createFromFormat('Y-m-d H:i:s', $value, new \DateTimeZone('UTC'));
				}
				break;
		}
		return $value;
	}
}
