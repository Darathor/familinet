<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Mysql;

use Change\Db\Query;
use Change\Db\ScalarType;

/**
 * @name \Change\Db\Mysql\FragmentBuilder
 */
class FragmentBuilder
{
	/**
	 * @var DbProvider
	 */
	protected $dbProvider;

	/**
	 * @param DbProvider $dbProvider
	 */
	public function __construct($dbProvider)
	{
		$this->dbProvider = $dbProvider;
	}

	/**
	 * @api
	 * @param Query\InterfaceSQLFragment $fragment
	 * @throws \RuntimeException
	 * @return string
	 */
	public function buildSQLFragment(Query\InterfaceSQLFragment $fragment)
	{
		$className = \get_class($fragment);
		switch ($className)
		{
			case \Change\Db\Query\Expressions\Table::class:
				/* @var $fragment \Change\Db\Query\Expressions\Table */
				$identifierParts = [];
				$dbName = $fragment->getDatabase();
				$tableName = $fragment->getName();
				if (!empty($dbName))
				{
					$identifierParts[] = '`' . $dbName . '`';
				}
				$identifierParts[] = '`' . $tableName . '`';
				return \implode('.', $identifierParts);

			case \Change\Db\Query\Expressions\Column::class:
				/* @var $fragment \Change\Db\Query\Expressions\Column */
				$columnName = $this->buildSQLFragment($fragment->getColumnName());
				$tableOrIdentifier = $fragment->getTableOrIdentifier();
				$table = ($tableOrIdentifier) ? $this->buildSQLFragment($tableOrIdentifier) : null;
				return empty($table) ? $columnName : $table . '.' . $columnName;

			case \Change\Db\Query\Expressions\Parentheses::class:
				/* @var $fragment \Change\Db\Query\Expressions\Parentheses */
				return '(' . $this->buildSQLFragment($fragment->getExpression()) . ')';

			case \Change\Db\Query\Expressions\Identifier::class:
				/* @var $fragment \Change\Db\Query\Expressions\Identifier */
				return implode('.', array_map(function ($part) {
					return '`' . $part . '`';
				}, $fragment->getParts()));

			case \Change\Db\Query\Expressions\Concat::class:
				/* @var $fragment \Change\Db\Query\Expressions\Concat */
				return 'CONCAT(' . implode(', ', $this->buildSQLFragmentArray($fragment->getList())) . ')';

			case \Change\Db\Query\Expressions\ExpressionList::class:
				/* @var $fragment \Change\Db\Query\Expressions\ExpressionList */
				return implode(', ', $this->buildSQLFragmentArray($fragment->getList()));

			case \Change\Db\Query\Expressions\Raw::class:
			case \Change\Db\Query\Expressions\AllColumns::class:
				return $fragment->toSQL92String();

			case \Change\Db\Query\Predicates\Conjunction::class:
				/* @var $fragment \Change\Db\Query\Predicates\Conjunction */
				return '(' . implode(' AND ', $this->buildSQLFragmentArray($fragment->getArguments())) . ')';

			case \Change\Db\Query\Predicates\Disjunction::class:
				/* @var $fragment \Change\Db\Query\Predicates\Disjunction */
				return '(' . implode(' OR ', $this->buildSQLFragmentArray($fragment->getArguments())) . ')';

			case \Change\Db\Query\Predicates\Like::class:
			case \Change\Db\Query\Predicates\In::class:
				/* @var $fragment \Change\Db\Query\Predicates\Like|\Change\Db\Query\Predicates\In */
				$fragment->checkCompile();
				$rhe = $fragment->getCompletedRightHandExpression();
				return $this->buildSQLFragment($fragment->getLeftHandExpression()) . ' ' . $fragment->getOperator() . ' '
					. $this->buildSQLFragment($rhe);

			case \Change\Db\Query\Predicates\Exists::class:
				/* @var $fragment \Change\Db\Query\Predicates\Exists */
				$fragment->checkCompile();
				return $fragment->getOperator() . ' ' . $this->buildSQLFragment($fragment->getExpression());

			case \Change\Db\Query\Predicates\HasPermission::class:
				/* @var $fragment \Change\Db\Query\Predicates\HasPermission */
				return $this->buildSQLFragment($fragment->getPredicate());

			case \Change\Db\Query\Expressions\BinaryOperation::class:
			case \Change\Db\Query\Expressions\Alias::class:
			case \Change\Db\Query\Expressions\Assignment::class:
			case \Change\Db\Query\Predicates\BinaryPredicate::class:
				/* @var $fragment \Change\Db\Query\Expressions\BinaryOperation */
				return $this->buildSQLFragment($fragment->getLeftHandExpression()) . ' ' . $fragment->getOperator() . ' '
					. $this->buildSQLFragment($fragment->getRightHandExpression());

			case \Change\Db\Query\Predicates\UnaryPredicate::class:
				/* @var $fragment \Change\Db\Query\Predicates\UnaryPredicate */
				return $this->buildSQLFragment($fragment->getExpression()) . ' ' . $fragment->getOperator();

			case \Change\Db\Query\Expressions\OrderingSpecification::class:
				/* @var $fragment \Change\Db\Query\Expressions\OrderingSpecification */
				return $this->buildSQLFragment($fragment->getExpression()) . ' ' . $fragment->getOperator();

			case \Change\Db\Query\Expressions\UnaryOperation::class:
				/* @var $fragment \Change\Db\Query\Expressions\UnaryOperation */
				return $fragment->getOperator() . ' ' . $this->buildSQLFragment($fragment->getExpression());

			case \Change\Db\Query\Expressions\Join::class:
				/* @var $fragment \Change\Db\Query\Expressions\Join */
				$joinedTable = $fragment->getTableExpression();
				if (!$joinedTable)
				{
					throw new \RuntimeException('A joined table is required', 42002);
				}
				$parts = [];
				if ($fragment->isNatural())
				{
					$parts[] = 'NATURAL';
				}
				if ($fragment->isQualified())
				{
					switch ($fragment->getType())
					{
						case Query\Expressions\Join::LEFT_OUTER_JOIN :
							$parts[] = 'LEFT OUTER JOIN';
							break;
						case Query\Expressions\Join::RIGHT_OUTER_JOIN :
							$parts[] = 'RIGHT OUTER JOIN';
							break;
						case Query\Expressions\Join::FULL_OUTER_JOIN :
							$parts[] = 'FULL OUTER JOIN';
							break;
						case Query\Expressions\Join::INNER_JOIN :
						default :
							$parts[] = 'INNER JOIN';
							break;
					}
				}
				else
				{
					$parts[] = 'CROSS JOIN';
				}
				$parts[] = $this->buildSQLFragment($joinedTable);
				if (!$fragment->isNatural())
				{
					$joinSpecification = $fragment->getSpecification();
					$parts[] = $this->buildSQLFragment($joinSpecification);
				}
				return \implode(' ', $parts);
			case \Change\Db\Query\Expressions\Value::class:
			case \Change\Db\Query\Expressions\StringValue::class:
			case \Change\Db\Query\Expressions\Numeric::class:
			case \Change\Db\Query\Expressions\Boolean::class:
				/* @var $fragment \Change\Db\Query\Expressions\Value */
				$v = $fragment->getValue();
				if ($v === null)
				{
					return 'NULL';
				}
				switch ($fragment->getScalarType())
				{
					case ScalarType::BOOLEAN :
						return $v ? '1' : '0';
					case ScalarType::INTEGER :
						return (string)(int)$v;
					case ScalarType::DECIMAL :
						return (string)(float)$v;
					case ScalarType::DATETIME :
						if ($v instanceof \DateTime)
						{
							$v->setTimezone(new \DateTimeZone('UTC'));
							$v = $v->format('Y-m-d H:i:s');
						}
				}
				return $this->dbProvider->getDriver()->quote($v);

			case \Change\Db\Query\Expressions\Parameter::class:
				/* @var $fragment \Change\Db\Query\Expressions\Parameter */
				return ':' . $fragment->getName();

			case \Change\Db\Query\Expressions\SubQuery::class:
				/* @var $fragment \Change\Db\Query\Expressions\SubQuery */
				return '(' . $this->buildQuery($fragment->getSubQuery()) . ')';

			case \Change\Db\Query\Expressions\Func::class:
				/* @var $fragment \Change\Db\Query\Expressions\Func */
				return $this->buildSQLFunc($fragment);

			case \Change\Db\Query\SelectQuery::class:
			case \Change\Db\Query\InsertQuery::class:
			case \Change\Db\Query\UpdateQuery::class:
			case \Change\Db\Query\DeleteQuery::class:
				/* @var $fragment \Change\Db\Query\AbstractQuery */
				return $this->buildQuery($fragment);

			case \Change\Db\Query\Clauses\WhereClause::class:
			case \Change\Db\Query\Clauses\CollateClause::class:
			case \Change\Db\Query\Clauses\DeleteClause::class:
			case \Change\Db\Query\Clauses\FromClause::class:
			case \Change\Db\Query\Clauses\GroupByClause::class:
			case \Change\Db\Query\Clauses\HavingClause::class:
			case \Change\Db\Query\Clauses\InsertClause::class:
			case \Change\Db\Query\Clauses\OrderByClause::class:
			case \Change\Db\Query\Clauses\SelectClause::class:
			case \Change\Db\Query\Clauses\SetClause::class:
			case \Change\Db\Query\Clauses\UpdateClause::class:
			case \Change\Db\Query\Clauses\ValuesClause::class:
				/* @var $fragment \Change\Db\Query\Clauses\AbstractClause */
				return $this->buildAbstractClause($fragment);
			default:
				return $this->dbProvider->buildCustomSQLFragment($fragment);
		}
	}

	/**
	 * @param Query\InterfaceSQLFragment[]|Query\Expressions\AbstractExpression[] $fragments
	 * @return string[]
	 */
	protected function buildSQLFragmentArray($fragments)
	{
		$strings = [];
		foreach ($fragments as $fragment)
		{
			$strings[] = $this->buildSQLFragment($fragment);
		}
		return $strings;
	}

	/**
	 * @param Query\Expressions\Func $func
	 * @return string
	 */
	protected function buildSQLFunc($func)
	{
		$distinct = $func->getDistinct() ? 'DISTINCT ' : '';
		return $func->getFunctionName() . '(' . $distinct . \implode(',', $this->buildSQLFragmentArray($func->getArguments())) . ')';
	}

	/**
	 * @param Query\AbstractQuery $query
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function buildQuery(Query\AbstractQuery $query)
	{
		$className = \get_class($query);
		switch ($className)
		{
			case \Change\Db\Query\SelectQuery::class:
				/* @var $query \Change\Db\Query\SelectQuery */
				$query->checkCompile();
				$parts = [$this->buildAbstractClause($query->getSelectClause())];
				$fromClause = $query->getFromClause();
				if ($fromClause)
				{
					$parts[] = $this->buildAbstractClause($fromClause);
				}
				$whereClause = $query->getWhereClause();
				if ($whereClause)
				{
					$parts[] = $this->buildAbstractClause($whereClause);
				}

				$groupByClause = $query->getGroupByClause();
				if ($groupByClause)
				{
					$parts[] = $this->buildAbstractClause($groupByClause);
				}

				$havingClause = $query->getHavingClause();
				if ($havingClause)
				{
					$parts[] = $this->buildAbstractClause($havingClause);
				}

				$orderByClause = $query->getOrderByClause();
				if ($orderByClause)
				{
					$parts[] = $this->buildAbstractClause($orderByClause);
				}

				if ($query->getMaxResults())
				{
					$parts[] = 'LIMIT';
					if ($query->getStartIndex())
					{
						$parts[] = \max(0, $query->getStartIndex()) . ',';
					}
					$parts[] = (string)\max(1, $query->getMaxResults());
				}

				return \implode(' ', $parts);

			case \Change\Db\Query\InsertQuery::class:
				/* @var $query \Change\Db\Query\InsertQuery */
				$query->checkCompile();
				$parts = [$this->buildAbstractClause($query->getInsertClause())];
				if ($query->getValuesClause() !== null)
				{
					$parts[] = $this->buildAbstractClause($query->getValuesClause());
				}
				elseif ($query->getSelectQuery() !== null)
				{
					$parts[] = $this->buildQuery($query->getSelectQuery());
				}
				return \implode(' ', $parts);

			case \Change\Db\Query\UpdateQuery::class:
				/* @var $query \Change\Db\Query\UpdateQuery */
				$query->checkCompile();
				$parts = [$this->buildAbstractClause($query->getUpdateClause()),
					$this->buildAbstractClause($query->getSetClause())];
				if ($query->getWhereClause() !== null)
				{
					$parts[] = $this->buildAbstractClause($query->getWhereClause());
				}
				return \implode(' ', $parts);

			case \Change\Db\Query\DeleteQuery::class:
				/* @var $query \Change\Db\Query\DeleteQuery */

				$query->checkCompile();
				$parts = [$this->buildAbstractClause($query->getDeleteClause()),
					$this->buildAbstractClause($query->getFromClause())];
				if ($query->getWhereClause() !== null)
				{
					$parts[] = $this->buildAbstractClause($query->getWhereClause());
				}
				return \implode(' ', $parts);

			default:
				throw new \InvalidArgumentException('Argument 1 must be a Select, Insert, Update or Delete query', 999999);
		}
	}

	/**
	 * @param Query\Clauses\AbstractClause $clause
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	protected function buildAbstractClause(Query\Clauses\AbstractClause $clause)
	{
		$className = \get_class($clause);
		switch ($className)
		{
			case \Change\Db\Query\Clauses\SelectClause::class:
				/* @var $clause \Change\Db\Query\Clauses\SelectClause */
				$parts = [$clause->getName()];
				if ($clause->getQuantifier() === Query\Clauses\SelectClause::QUANTIFIER_DISTINCT)
				{
					$parts[] = Query\Clauses\SelectClause::QUANTIFIER_DISTINCT;
				}
				$selectList = $clause->getSelectList();
				if ($selectList === null || !$selectList->count())
				{
					$selectList = new Query\Expressions\AllColumns();
				}

				$parts[] = $this->buildSQLFragment($selectList);
				return \implode(' ', $parts);

			case \Change\Db\Query\Clauses\FromClause::class:
				/* @var $clause \Change\Db\Query\Clauses\FromClause */
				$clause->checkCompile();
				$parts = [$clause->getName(), $this->buildSQLFragment($clause->getTableExpression())];
				if ($options = $clause->getOptions())
				{
					foreach ($options as $option)
					{
						if (\is_array($option) && ($option[0] === 'USE INDEX' || $option[0] === 'FORCE INDEX'))
						{
							$parts[] = $option[0] . ' (`' . $option[1] . '`)';
						}
					}
				}
				$parts[] = \implode(' ', $this->buildSQLFragmentArray($clause->getJoins()));
				return \implode(' ', $parts);

			case \Change\Db\Query\Clauses\WhereClause::class:
				/* @var $clause \Change\Db\Query\Clauses\WhereClause */
				$parts = [$clause->getName(), $this->buildSQLFragment($clause->getPredicate())];
				return implode(' ', $parts);

			case \Change\Db\Query\Clauses\OrderByClause::class:
			case \Change\Db\Query\Clauses\GroupByClause::class:
				/* @var $clause \Change\Db\Query\Clauses\OrderByClause|\Change\Db\Query\Clauses\GroupByClause */
				$clause->checkCompile();
				$parts = [$clause->getName(), $this->buildSQLFragment($clause->getExpressionList())];
				return \implode(' ', $parts);

			case \Change\Db\Query\Clauses\HavingClause::class:
				/* @var $clause \Change\Db\Query\Clauses\HavingClause */
				return 'HAVING ' . $this->buildSQLFragment($clause->getPredicate());

			case \Change\Db\Query\Clauses\InsertClause::class:
				/* @var $clause \Change\Db\Query\Clauses\InsertClause */
				$clause->checkCompile();
				$insert = 'INSERT INTO ' . $this->buildSQLFragment($clause->getTable());
				$columns = $clause->getColumns();
				if ($columns)
				{
					$compiler = $this;
					$insert .= ' (' . \implode(', ', \array_map(static function ($column) use ($compiler) {
							return $compiler->buildSQLFragment($column);
						}, $columns)) . ')';
				}
				return $insert;

			case \Change\Db\Query\Clauses\ValuesClause::class:
				/* @var $clause \Change\Db\Query\Clauses\ValuesClause */
				$clause->checkCompile();
				return 'VALUES (' . $this->buildSQLFragment($clause->getValuesList()) . ')';

			case \Change\Db\Query\Clauses\UpdateClause::class:
				/* @var $clause \Change\Db\Query\Clauses\UpdateClause */
				$clause->checkCompile();
				return 'UPDATE ' . $this->buildSQLFragment($clause->getTable());

			case \Change\Db\Query\Clauses\SetClause::class:
				/* @var $clause \Change\Db\Query\Clauses\SetClause */
				$clause->checkCompile();
				return 'SET ' . $this->buildSQLFragment($clause->getSetList());

			case \Change\Db\Query\Clauses\DeleteClause::class:
				/* @var $clause \Change\Db\Query\Clauses\DeleteClause */
				return 'DELETE';

			default:
				throw new \InvalidArgumentException('Argument 1 must be a valid clause', 999999);
		}
	}
}