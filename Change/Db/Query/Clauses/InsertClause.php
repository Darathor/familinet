<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Clauses;

/**
 * @name \Change\Db\Query\Clauses\InsertClause
 * @api
 */
class InsertClause extends AbstractClause
{
	/**
	 * @var \Change\Db\Query\Expressions\Table
	 */
	protected $table;

	/**
	 * @var \Change\Db\Query\Expressions\Column[]
	 */
	protected $columns = [];

	/**
	 * @param \Change\Db\Query\Expressions\Table $table
	 */
	public function __construct(\Change\Db\Query\Expressions\Table $table = null)
	{
		$this->name = 'INSERT';
		$this->table = $table;
	}

	/**
	 * @return \Change\Db\Query\Expressions\Table|null
	 */
	public function getTable()
	{
		return $this->table;
	}

	/**
	 * @param \Change\Db\Query\Expressions\Table $table
	 */
	public function setTable(\Change\Db\Query\Expressions\Table $table)
	{
		$this->table = $table;
	}

	/**
	 * @return \Change\Db\Query\Expressions\Column[]
	 */
	public function getColumns()
	{
		return $this->columns;
	}

	/**
	 * @param \Change\Db\Query\Expressions\Column[] $columns
	 */
	public function setColumns(array $columns)
	{
		$this->columns = [];
		foreach ($columns as $column)
		{
			$this->addColumn($column);
		}
	}

	/**
	 * @param \Change\Db\Query\Expressions\Column $column
	 * @return \Change\Db\Query\Clauses\InsertClause
	 */
	public function addColumn(\Change\Db\Query\Expressions\Column $column)
	{
		$this->columns[] = $column;
		return $this;
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 */
	public function checkCompile()
	{
		if ($this->table === null)
		{
			throw new \RuntimeException('Table can not be null', 42026);
		}
	}

	/**
	 * @throws \RuntimeException
	 * @return string
	 */
	public function toSQL92String()
	{
		$this->checkCompile();
		$insert = 'INSERT ' . $this->table->toSQL92String();
		if ($this->columns)
		{
			$insert .= ' (' . \implode(', ', \array_map(static function (\Change\Db\Query\Expressions\Column $column) {
					return $column->toSQL92String();
				}, $this->columns)) . ')';
		}
		return $insert;
	}
}
