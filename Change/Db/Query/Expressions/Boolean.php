<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Expressions;

/**
 * @name \Change\Db\Query\Expressions\Boolean
 */
class Boolean extends \Change\Db\Query\Expressions\Value
{
	/**
	 * @param int|float $value
	 */
	public function __construct($value = null)
	{
		parent::__construct($value, \Change\Db\ScalarType::BOOLEAN);
	}
	
	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		if ($this->value === null)
		{
			return 'NULL';
		}
		return $this->value ? '1' : '0';
	}
}