<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Expressions;

/**
 * @name \Change\Db\Query\Expressions\Func
 * @api
 */
class Func extends AbstractExpression
{
	/**
	 * @var bool
	 */
	protected $distinct;

	/**
	 * @var string
	 */
	protected $functionName;

	/**
	 * @var array
	 */
	protected $arguments;

	/**
	 * @param string $functionName
	 * @param \Change\Db\Query\Expressions\AbstractExpression[] $arguments
	 * @param bool $distinct
	 */
	public function __construct(string $functionName = null, array $arguments = [], bool $distinct = false)
	{
		$this->functionName = $functionName;
		$this->distinct = $distinct;
		$this->setArguments($arguments);

	}

	/**
	 * @api
	 * @return string
	 */
	public function getFunctionName()
	{
		return $this->functionName;
	}

	/**
	 * @api
	 * @param string $functionName
	 */
	public function setFunctionName(string $functionName = null)
	{
		$this->functionName = $functionName;
	}

	/**
	 * @api
	 * @return \Change\Db\Query\Expressions\AbstractExpression[]
	 */
	public function getArguments()
	{
		return $this->arguments;
	}

	/**
	 * @api
	 * @throws \InvalidArgumentException
	 * @param \Change\Db\Query\Expressions\AbstractExpression[] $arguments
	 */
	public function setArguments(array $arguments)
	{
		$this->arguments = [];
		if ($arguments)
		{
			$this->arguments = \array_map(static function (AbstractExpression $item) { return $item; }, $arguments);
		}
	}

	/**
	 * @api
	 * @param \Change\Db\Query\Expressions\AbstractExpression $argument
	 * @return \Change\Db\Query\Expressions\Func
	 */
	public function addArgument(\Change\Db\Query\Expressions\AbstractExpression $argument)
	{
		$this->arguments[] = $argument;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getDistinct()
	{
		return $this->distinct;
	}

	/**
	 * @param bool $distinct
	 * @return $this
	 */
	public function setDistinct(bool $distinct)
	{
		$this->distinct = $distinct;
		return $this;
	}

	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		$argsString = \array_map(static function (AbstractExpression $element)
		{
			return $element->toSQL92String();
		}, $this->arguments);
		$distinct = $this->distinct ? 'DISTINCT ' : '';
		return $this->functionName . '(' . $distinct . \implode(', ', $argsString) . ')';
	}
}