<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Expressions;

use Change\Db\ScalarType;

/**
 * @name \Change\Db\Query\Expressions\Parameter
 */
class Parameter extends AbstractExpression
{		
	/**
	 * @var string
	 */
	protected $name;
	
	/**
	 * @var int as \Change\Db\ScalarType::*
	 */
	protected $type;
	
	/**
	 * @param string $name
	 * @param int $type
	 */
	public function __construct(string $name, int $type = ScalarType::STRING)
	{
		$this->name = $name;
		$this->type = $type;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}
	
	/**
	 * @return int as \Change\Db\ScalarType::*
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @throws \InvalidArgumentException
	 * @param int $type as \Change\Db\ScalarType::*
	 */
	public function setType(int $type)
	{
		switch ($type)
		{
			case ScalarType::STRING:
			case ScalarType::BOOLEAN:
			case ScalarType::DATETIME:
			case ScalarType::DECIMAL:
			case ScalarType::INTEGER:
			case ScalarType::LOB:
			case ScalarType::TEXT:
				$this->type = $type;
				return;
		}
		throw new \InvalidArgumentException('Argument 1 must be a valid const', 42027);
	}

	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		return ':' . $this->name;
	}
}