<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Predicates;

/**
 * @name \Change\Db\Query\Predicates\Exists
 * @api
 */
class Exists extends UnaryPredicate
{
	/**
	 * @var bool
	 */
	protected $not = false;

	/**
	 * @param \Change\Db\Query\Expressions\SubQuery $subQuery
	 * @param bool $not
	 */
	public function __construct(\Change\Db\Query\Expressions\SubQuery $subQuery = null, bool $not = false)
	{
		parent::__construct($subQuery, 'EXISTS');
		$this->setNot($not);
	}

	/**
	 * @return bool
	 */
	public function getNot()
	{
		return $this->not;
	}

	/**
	 * @param bool $not
	 */
	public function setNot(bool $not)
	{
		$this->not = $not;
		$this->operator = $not ? 'NOT EXISTS' : 'EXISTS';
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 */
	public function checkCompile()
	{
		$subQuery = $this->expression;
		if (!($subQuery instanceof \Change\Db\Query\Expressions\SubQuery))
		{
			throw new \RuntimeException('Expression must be a SubQuery', 999999);
		}
	}

	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		$this->checkCompile();
		return $this->operator . $this->expression->toSQL92String();
	}
}