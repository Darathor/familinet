<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Query\Predicates;

use Change\Db\Query\Expressions\StringValue;
use Change\Db\Query\Expressions\AbstractExpression;
use Change\Db\Query\Expressions\Concat;

/**
 * @name \Change\Db\Query\Predicates\Like
 */
class Like extends BinaryPredicate
{
	public const ANYWHERE = 0;
	public const BEGIN = 1;
	public const END = 2;
	public const EXACT = 3;

	/**
	 * @var int
	 */
	protected $matchMode;

	/**
	 * @var bool
	 */
	protected $caseSensitive;

	/**
	 * @var string
	 */
	protected $wildCard = '%';

	/**
	 * @var bool
	 */
	protected $not = false;

	/**
	 * @param AbstractExpression $lhs
	 * @param AbstractExpression $rhs
	 * @param int $matchMode
	 * @param bool $caseSensitive
	 * @param bool $not
	 */
	public function __construct(AbstractExpression $lhs = null, AbstractExpression $rhs = null, int $matchMode = self::ANYWHERE,
		bool $caseSensitive = false, bool $not = false)
	{
		parent::__construct($lhs, $rhs);
		$this->not = $not;
		$this->setMatchMode($matchMode);
		//This method update operator
		$this->setCaseSensitive($caseSensitive);
	}

	/**
	 * @param bool $caseSensitive
	 */
	public function setCaseSensitive(bool $caseSensitive)
	{
		$this->caseSensitive = $caseSensitive;
		$this->evaluateOperator();
	}

	protected function evaluateOperator()
	{
		$this->operator = ($this->not ? 'NOT ' : '') . 'LIKE' . ($this->caseSensitive ? ' BINARY' : '');
	}

	/**
	 * @return bool
	 */
	public function getCaseSensitive()
	{
		return $this->caseSensitive;
	}

	/**
	 * @throws \InvalidArgumentException
	 * @param int $matchMode
	 */
	public function setMatchMode($matchMode)
	{
		switch ($matchMode)
		{
			case self::ANYWHERE:
			case self::BEGIN:
			case self::END:
			case self::EXACT:
				$this->matchMode = $matchMode;
				return;
		}
		throw new \InvalidArgumentException('Argument 1 must be a valid const', 42027);
	}

	/**
	 * @return int
	 */
	public function getMatchMode()
	{
		return $this->matchMode;
	}

	/**
	 * @param string $wildCard
	 */
	public function setWildCard($wildCard)
	{
		$this->wildCard = $wildCard;
	}

	/**
	 * @return string
	 */
	public function getWildCard()
	{
		return $this->wildCard;
	}

	/**
	 * @return bool
	 */
	public function getNot()
	{
		return $this->not;
	}

	/**
	 * @param bool $not
	 */
	public function setNot(bool $not)
	{
		$this->not = $not;
		$this->evaluateOperator();
	}

	/**
	 * @return \Change\Db\Query\Expressions\AbstractExpression|\Change\Db\Query\Expressions\Concat
	 */
	public function getCompletedRightHandExpression()
	{
		$arguments = [$this->rightHandExpression];
		switch ($this->matchMode)
		{
			case self::BEGIN :
				$arguments[] = new StringValue($this->wildCard);
				break;
			case self::END :
				\array_unshift($arguments, new StringValue($this->wildCard));
				break;
			case self::ANYWHERE :
				$arguments[] = new StringValue($this->wildCard);
				\array_unshift($arguments, new StringValue($this->wildCard));
				break;
			default:
				return $this->rightHandExpression;
		}
		return new Concat($arguments);
	}

	/**
	 * @return string
	 */
	public function toSQL92String()
	{
		$rhe = $this->getCompletedRightHandExpression();
		return $this->leftHandExpression->toSQL92String() . ' ' . $this->operator . ' ' . $rhe->toSQL92String();
	}
}