<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db;

/**
 * @name \Change\Db\ScalarType
 */
class ScalarType
{
	public const STRING = 0;
	public const BOOLEAN = 1;
	public const INTEGER = 2;
	public const DECIMAL = 3;
	public const DATETIME = 4;
	public const TEXT = 5;
	public const LOB = 6;
}