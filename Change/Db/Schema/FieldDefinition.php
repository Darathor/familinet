<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Schema;

/**
 * @name \Change\Db\Schema\FieldDefinition
 */
class FieldDefinition
{
	public const CHAR = 0;
	public const VARCHAR = 1;

	public const INTEGER = 2;
	public const SMALLINT = 3;

	public const DECIMAL = 4;
	public const FLOAT = 5;

	public const DATE = 6;
	public const TIMESTAMP = 7;

	public const ENUM = 8;

	public const LOB = 9;

	public const TEXT = 10;

	/**
	 * @var array
	 */
	protected $options = [];

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var int
	 */
	protected $type = self::VARCHAR;

	/**
	 * @var string
	 */
	protected $defaultValue;

	/**
	 * @var bool
	 */
	protected $nullable = true;

	/**
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setName(string $name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return int FieldDefinition::*
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param int $type FieldDefinition::*
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setType(int $type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDefaultValue()
	{
		return $this->defaultValue;
	}

	/**
	 * @param string|null $defaultValue
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setDefaultValue(?string $defaultValue)
	{
		$this->defaultValue = $defaultValue;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getNullable()
	{
		return $this->nullable;
	}

	/**
	 * @param bool $nullable
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setNullable(bool $nullable)
	{
		$this->nullable = $nullable;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 * @return $this
	 */
	public function setOptions(array $options)
	{
		$this->options = $options;
		return $this;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function setOption(string $name, $value)
	{
		if ($value === null)
		{
			unset($this->options[$name]);
		}
		else
		{
			$this->options[$name] = $value;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getOption(string $name)
	{
		return $this->options[$name] ?? null;
	}

	/**
	 * @param int $length
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setLength(int $length)
	{
		$this->setOption('LENGTH', $length);
		return $this;
	}

	/**
	 * @return int
	 */
	public function getLength()
	{
		return (int)$this->getOption('LENGTH');
	}

	/**
	 * @param string $charset
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setCharset(string $charset)
	{
		$this->setOption('CHARSET', $charset);
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCharset()
	{
		return $this->getOption('CHARSET');
	}

	/**
	 * @param string $collation
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setCollation(string $collation)
	{
		$this->setOption('COLLATION', $collation);
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCollation()
	{
		return $this->getOption('COLLATION');
	}

	/**
	 * @param bool $autoNumber
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setAutoNumber(bool $autoNumber)
	{
		$this->setOption('AUTONUMBER', $autoNumber);
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getAutoNumber()
	{
		return $this->getOption('AUTONUMBER') === true;
	}

	/**
	 * @param int $precision
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setPrecision(int $precision)
	{
		$this->setOption('PRECISION', $precision);
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPrecision()
	{
		return (int)$this->getOption('PRECISION');
	}

	/**
	 * @param int $scale
	 * @return \Change\Db\Schema\FieldDefinition
	 */
	public function setScale(int $scale)
	{
		$this->setOption('SCALE', $scale);
		return $this;
	}

	/**
	 * @return int
	 */
	public function getScale()
	{
		return (int)$this->getOption('SCALE');
	}
}