<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Db\Schema;

/**
 * @name \Change\Db\Schema\KeyDefinition
 */
class KeyDefinition
{
	public const PRIMARY = 'PRIMARY';
	public const UNIQUE = 'UNIQUE';
	public const INDEX = 'INDEX';

	/**
	 * @var string
	 */
	protected $type = self::INDEX;

	/**
	 * @var array
	 */
	protected $options = [];

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var \Change\Db\Schema\FieldDefinition[]
	 */
	protected $fields;

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function setType(string $type)
	{
		if ($type === static::PRIMARY || $type === static::UNIQUE)
		{
			$this->type = $type;
		}
		else
		{
			$this->type = static::INDEX;
		}
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function setOptions(array $options)
	{
		$this->options = is_array($options) ? $options : [];
		return $this;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function setOption(string $name, $value)
	{
		if ($value === null)
		{
			unset($this->options[$name]);
		}
		else
		{
			$this->options[$name] = $value;
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getOption(string $name)
	{
		return $this->options[$name] ?? null;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function setName(string $name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPrimary()
	{
		return $this->type === static::PRIMARY;
	}

	/**
	 * @return bool
	 */
	public function isUnique()
	{
		return $this->type === static::UNIQUE;
	}

	/**
	 * @return bool
	 */
	public function isIndex()
	{
		return $this->type === static::INDEX;
	}

	/**
	 * @return \Change\Db\Schema\FieldDefinition[]
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @param \Change\Db\Schema\FieldDefinition[] $fields
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function setFields(array $fields)
	{
		$this->fields = [];
		foreach ($fields as $field)
		{
			if ($field instanceof FieldDefinition)
			{
				$this->fields[$field->getName()] = $field;
			}
		}
		return $this;
	}

	/**
	 * @param \Change\Db\Schema\FieldDefinition $field
	 * @return \Change\Db\Schema\KeyDefinition
	 */
	public function addField(\Change\Db\Schema\FieldDefinition $field)
	{
		$this->fields[$field->getName()] = $field;
		return $this;
	}
}