<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;

/**
 * @name \Change\Documents\AbstractLocalizedDocument
 * @api
 */
abstract class AbstractLocalizedDocument implements \Serializable
{
	/**
	 * @var AbstractModel
	 */
	protected $documentModel;

	/**
	 * @var integer
	 */
	private $id;

	/**
	 * @var string
	 */
	private $LCID;

	/**
	 * @var integer
	 */
	private $persistentState = AbstractDocument::STATE_NEW;

	/**
	 * @var array
	 */
	protected $modifiedProperties = [];

	/**
	 * @param AbstractModel $documentModel
	 */
	public function __construct(AbstractModel $documentModel)
	{
		$this->setDocumentModel($documentModel);
	}

	/**
	 * This class is not serializable
	 * @return null
	 */
	public function serialize()
	{
		return null;
	}

	/**
	 * @param string $serialized
	 * @return void
	 */
	public function unserialize($serialized)
	{
	}

	/**
	 * @param integer $id
	 * @param string $lcid
	 * @param integer $persistentState \Change\Documents\AbstractDocument::STATE_*
	 */
	public function initialize($id, $lcid, $persistentState = null)
	{
		$this->id = (int)$id;
		$this->setLCID($lcid);
		if ($persistentState !== null)
		{
			$this->persistentState = $this->setPersistentState($persistentState);
		}
	}

	/**
	 * @param \Change\Documents\AbstractModel $documentModel
	 */
	public final function setDocumentModel(\Change\Documents\AbstractModel $documentModel)
	{
		$this->documentModel = $documentModel;
	}

	/**
	 * @return \Change\Documents\AbstractModel
	 */
	public final function getDocumentModel()
	{
		return $this->documentModel;
	}

	/**
	 * Return \Change\Documents\AbstractDocument::STATE_*
	 * @return integer
	 */
	public final function getPersistentState()
	{
		return $this->persistentState;
	}

	/**
	 * @return boolean
	 */
	public function isNew()
	{
		return $this->getPersistentState() === AbstractDocument::STATE_NEW;
	}

	/**
	 * @param integer $newValue \Change\Documents\AbstractDocument::STATE_*
	 * @return integer
	 */
	public final function setPersistentState($newValue)
	{
		$oldState = $this->persistentState;
		switch ($newValue)
		{
			case AbstractDocument::STATE_LOADED:
				$this->clearModifiedProperties();
				$this->persistentState = $newValue;
				break;
			case AbstractDocument::STATE_NEW:
			case AbstractDocument::STATE_LOADING:
			case AbstractDocument::STATE_DELETED:
			case AbstractDocument::STATE_DELETING:
			case AbstractDocument::STATE_SAVING:
				$this->persistentState = $newValue;
				break;
		}
		return $oldState;
	}

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return $this
	 */
	public function setDefaultValues()
	{
		$this->persistentState = AbstractDocument::STATE_NEW;
		return $this;
	}

	/**
	 * @ignore
	 * @return array
	 */
	public function toDb()
	{
		return [];
	}

	/**
	 * @ignore
	 * @param array $data
	 * @return $this
	 */
	public function fromDb(array $data)
	{
		return $this;
	}

	/**
	 * @api
	 * @param string $propertyName
	 */
	public function removeOldPropertyValue($propertyName)
	{
		unset($this->modifiedProperties[$propertyName]);
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getModifiedPropertyNames()
	{
		return array_keys($this->modifiedProperties);
	}

	/**
	 * @api
	 * @return boolean
	 */
	public function hasModifiedProperties()
	{
		return count($this->getModifiedPropertyNames()) !== 0;
	}

	/**
	 * @return void
	 */
	protected function clearModifiedProperties()
	{
		$this->modifiedProperties = [];
	}

	/**
	 * @api
	 * @param string $propertyName
	 * @return boolean
	 */
	public final function isPropertyModified($propertyName)
	{
		return in_array($propertyName, $this->getModifiedPropertyNames());
	}

	/**
	 * @param string $propertyName
	 * @param mixed $value
	 */
	protected function setOldPropertyValue($propertyName, $value)
	{
		if (!array_key_exists($propertyName, $this->modifiedProperties))
		{
			$this->modifiedProperties[$propertyName] = $value;
		}
	}

	/**
	 * @param string $propertyName
	 * @return mixed
	 */
	public final function getOldPropertyValue($propertyName)
	{
		if (array_key_exists($propertyName, $this->modifiedProperties))
		{
			return $this->modifiedProperties[$propertyName];
		}
		return null;
	}

	// Generic Method

	/**
	 * @api
	 * @return string
	 */
	public function getLCID()
	{
		return $this->LCID;
	}

	/**
	 * @api
	 * @param string $LCID
	 * @return $this
	 */
	public function setLCID($LCID)
	{
		$this->LCID = $LCID;
		return $this;
	}

	/**
	 * @api
	 * @return \DateTime
	 */
	abstract public function getCreationDate();

	/**
	 * @api
	 * @param \DateTime $creationDate
	 */
	abstract public function setCreationDate(\DateTime $creationDate = null);

	/**
	 * @api
	 * @return \DateTime
	 */
	abstract public function getModificationDate();

	/**
	 * @api
	 * @param \DateTime $modificationDate
	 */
	abstract public function setModificationDate(\DateTime $modificationDate = null);
}