<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;

/**
 * @api
 * @name \Change\Documents\AbstractLocalizedInline
 */
abstract class AbstractLocalizedInline implements \Serializable
{
	/**
	 * @var \Change\Documents\AbstractModel
	 */
	private $documentModel;

	/**
	 * @var \Callable|null
	 */
	private $updateCallback;

	/**
	 * @var boolean
	 */
	private $isNew = true;

	/**
	 * @var boolean
	 */
	private $isModified = false;

	/**
	 * @var string
	 */
	private $LCID;

	/**
	 * @param \Change\Documents\AbstractModel $documentModel
	 */
	public function __construct(AbstractModel $documentModel)
	{
		$this->setDocumentModel($documentModel);
	}

	/**
	 * This class is not serializable
	 * @return null
	 */
	public function serialize()
	{
		return null;
	}

	/**
	 * @param string $serialized
	 * @return void
	 */
	public function unserialize($serialized)
	{
	}

	/**
	 * @param \Change\Documents\AbstractModel $documentModel
	 */
	public final function setDocumentModel(\Change\Documents\AbstractModel $documentModel)
	{
		$this->documentModel = $documentModel;
	}

	/**
	 * @return \Change\Documents\AbstractModel
	 */
	public final function getDocumentModel()
	{
		return $this->documentModel;
	}

	/**
	 * @param \Callable|null $updateCallback
	 * @return $this
	 */
	public function link($updateCallback)
	{
		$this->updateCallback = null;
		if ($updateCallback && is_callable($updateCallback))
		{
			$this->updateCallback = $updateCallback;
		}
		return $this;
	}

	public function cleanUp()
	{
		$this->updateCallback = null;
	}

	/**
	 * @param boolean $isNew
	 * @return boolean
	 */
	public function isNew($isNew = null)
	{
		if (is_bool($isNew))
		{
			$this->isNew = $isNew;
		}
		return $this->isNew;
	}

	/**
	 * @param boolean $isModified
	 * @return boolean
	 */
	public function isModified($isModified = null)
	{
		if (is_bool($isModified))
		{
			$this->isModified = $isModified;
		}
		return $this->isModified;
	}

	/**
	 * @return boolean
	 */
	public function isEmpty()
	{
		return $this->isNew && !$this->isModified;
	}

	/**
	 * @return $this
	 */
	protected function onPropertyUpdate()
	{
		$this->isModified(true);
		if ($this->updateCallback !== null)
		{
			call_user_func($this->updateCallback);
		}
		return $this;
	}

	public function __clone()
	{
		$this->cleanUp();
		$this->updateCallback = null;
	}

	/**
	 * @param array|bool $dbData
	 * @return $this|array
	 */
	public function dbData($dbData = false)
	{
		if ($dbData === false)
		{
			return $this->toDbData();
		}
		if (\is_array($dbData))
		{
			$this->fromDbData($dbData);
		}
		else
		{
			$this->unsetProperties();
		}
		return $this;
	}

	/**
	 * @return array
	 */
	protected function toDbData()
	{
		$this->isNew(false);
		$this->isModified(false);
		return ['LCID' => $this->getLCID()];
	}

	/**
	 * @api
	 * @return $this
	 */
	public function setDefaultValues()
	{
		$this->isNew(true);
		return $this;
	}

	/**
	 * @api
	 * @param array $dbData
	 */
	protected function fromDbData(array $dbData)
	{
		$this->isNew(false);
		$this->isModified(false);
		$this->setLCID($dbData['LCID']);
	}

	/**
	 * @api
	 */
	public function unsetProperties()
	{
		$this->isModified(false);
	}

	// Generic Method

	/**
	 * @api
	 * @return string
	 */
	public function getLCID()
	{
		return $this->LCID;
	}

	/**
	 * @api
	 * @param string $LCID
	 */
	public function setLCID($LCID)
	{
		if ($this->LCID === null)
		{
			$this->LCID = $LCID;
		}
	}
}