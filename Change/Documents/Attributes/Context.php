<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes;

/**
 * @name \Change\Documents\Attributes\Context
 */
class Context implements \Change\Documents\Attributes\Interfaces\Context
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var boolean
	 */
	protected $exposedInAjaxAPI;

	/**
	 * @param array|null $context
	 */
	public function __construct($context = null)
	{
		if (is_array($context))
		{
			$this->fromArray($context);
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getExposedInAjaxAPI()
	{
		return $this->exposedInAjaxAPI;
	}

	/**
	 * @param boolean $exposedInAjaxAPI
	 * @return $this
	 */
	public function setExposedInAjaxAPI($exposedInAjaxAPI)
	{
		$this->exposedInAjaxAPI = $exposedInAjaxAPI ? true : false;
		return $this;
	}

	/**
	 * @param array $group
	 */
	protected function fromArray(array $group)
	{
		$this->name = isset($group['name']) ? $group['name'] : null;
		$this->title = isset($group['title']) ? $group['title'] : null;
		$this->exposedInAjaxAPI = isset($group['exposedInAjaxAPI']) ? $group['exposedInAjaxAPI'] : false;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$result = [
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'exposedInAjaxAPI' => $this->getExposedInAjaxAPI()
		];
		return $result;
	}
}