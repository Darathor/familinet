<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes\Interfaces;

/**
 * @name \Change\Documents\Attributes\Interfaces\Attribute
 */
interface Attribute
{
	const TYPE_BOOLEAN = 'Boolean';
	const TYPE_INTEGER = 'Integer';
	const TYPE_FLOAT = 'Float';
	const TYPE_DATETIME = 'DateTime';
	const TYPE_STRING = 'String';
	const TYPE_RICHTEXT = 'RichText';
	const TYPE_DOCUMENT_ID = 'DocumentId';
	const TYPE_DOCUMENT_ID_ARRAY = 'DocumentIdArray';
	const TYPE_JSON = 'JSON';

	/**
	 * @api
	 * @return string
	 */
	public function getName();

	/**
	 * @api
	 * @return string
	 */
	public function getType();

	/**
	 * @api
	 * @return string
	 */
	public function getTitle();

	/**
	 * @api
	 * @return \Change\Documents\RichtextProperty
	 */
	public function getDescription();

	/**
	 * @api
	 * @return string|null
	 */
	public function getRenderingMode();

	/**
	 * @api
	 * @return boolean
	 */
	public function getLocalized();

	/**
	 * @api
	 * @return boolean
	 */
	public function getIndexed();

	/**
	 * @api
	 * @return callable|null If callable, it should have two arguments: mixed $value and array $context
	 */
	public function getAJAXFormatter();

	/**
	 * @api
	 * @return array
	 */
	public function toArray();
}