<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes\Interfaces;

/**
 * @name \Change\Documents\Attributes\Interfaces\Typology
 */
interface Typology
{
	/**
	 * @api
	 * @return integer
	 */
	public function getId();

	/**
	 * @api
	 * @return string
	 */
	public function getName();

	/**
	 * @api
	 * @return string
	 */
	public function getTitle();

	/**
	 * @api
	 * @return string
	 */
	public function getModelName();

	/**
	 * @api
	 * @return \Change\Documents\Attributes\Interfaces\Group[]
	 */
	public function getGroups();

	/**
	 * @api
	 * @return \Change\Documents\Attributes\Interfaces\Context[]
	 */
	public function getContexts();

	/**
	 * @api
	 * @return array
	 */
	public function getVisibilities();

	/**
	 * @api
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Group|null
	 */
	public function getGroupByName($name);

	/**
	 * @api
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Attribute|null
	 */
	public function getAttributeByName($name);

	/**
	 * @api
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Context|null
	 */
	public function getContextByName($name);

	/**
	 * @api
	 * @return array
	 */
	public function toArray();
}