<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes;

/**
 * @name \Change\Documents\Attributes\ListenerCallbacks
 */
class ListenerCallbacks
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDocumentDelete(\Change\Documents\Events\Event $event)
	{
		$document = $document = $event->getDocument();;
		$event->getApplicationServices()->getDocumentManager()->saveAttributeValues($document, null, null);
	}
}