<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Attributes;

/**
 * @name \Change\Documents\Attributes\Typology
 */
class Typology implements \Change\Documents\Attributes\Interfaces\Typology
{
	/**
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var \Change\Documents\Attributes\Interfaces\Group[]
	 */
	protected $groups = [];

	/**
	 * @var array
	 */
	protected $contexts = [];

	/**
	 * @var array
	 */
	protected $visibilities = [];

	/**
	 * @var string
	 */
	protected $modelName;

	/**
	 * @param array|null $typology
	 */
	public function __construct($typology = null)
	{
		if (is_array($typology))
		{
			$this->fromArray($typology);
		}
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return \Rbs\Generic\Attributes\Group[]
	 */
	public function getGroups()
	{
		return $this->groups;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Group[]|array $groups
	 * @return $this
	 */
	public function setGroups($groups)
	{
		$this->groups = [];
		foreach ($groups as $group)
		{
			$group = $this->getNewGroup($group);
			if ($group)
			{
				$this->groups[] = $group;
			}
		}
		return $this;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Group|array $group
	 * @return \Rbs\Generic\Attributes\Group|null
	 */
	public function getNewGroup($group)
	{
		if ($group instanceof \Change\Documents\Attributes\Group)
		{
			return $group;
		}
		elseif ($group instanceof \Change\Documents\Attributes\Interfaces\Group)
		{
			return new \Change\Documents\Attributes\Group($group->toArray());
		}
		elseif (is_array($group))
		{
			return new \Change\Documents\Attributes\Group($group);
		}
		return null;
	}

	/**
	 * @return \Rbs\Generic\Attributes\Context[]
	 */
	public function getContexts()
	{
		return $this->contexts;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Context[]|array $contexts
	 * @return $this
	 */
	public function setContexts($contexts)
	{
		$this->contexts = [];
		foreach ($contexts as $context)
		{
			$context = $this->getNewContext($context);
			if ($context)
			{
				$this->contexts[] = $context;
			}
		}
		return $this;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Context|array $context
	 * @return \Rbs\Generic\Attributes\Context|null
	 */
	public function getNewContext($context)
	{
		if ($context instanceof \Change\Documents\Attributes\Context)
		{
			return $context;
		}
		elseif ($context instanceof \Change\Documents\Attributes\Interfaces\Context)
		{
			return new \Change\Documents\Attributes\Context($context->toArray());
		}
		elseif (is_array($context))
		{
			return new \Change\Documents\Attributes\Context($context);
		}
		return null;
	}

	/**
	 * @return array
	 */
	public function getVisibilities()
	{
		return $this->visibilities;
	}

	/**
	 * @param array $visibilities
	 * @return $this
	 */
	public function setVisibilities(array $visibilities)
	{
		$this->visibilities = $visibilities;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getModelName()
	{
		return $this->modelName;
	}

	/**
	 * @param string $modelName
	 * @return $this
	 */
	public function setModelName($modelName)
	{
		$this->modelName = $modelName;
		return $this;
	}

	/**
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Group|null
	 */
	public function getGroupByName($name)
	{
		foreach ($this->getGroups() as $group)
		{
			if ($group->getName() == $name)
			{
				return $group;
			}
		}
		return null;
	}

	/**
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Attribute|null
	 */
	public function getAttributeByName($name)
	{
		foreach ($this->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				if ($attribute->getName() == $name)
				{
					return $attribute;
				}
			}
		}
		return null;
	}

	/**
	 * @param string $name
	 * @return \Change\Documents\Attributes\Interfaces\Context|null
	 */
	public function getContextByName($name)
	{
		foreach ($this->getContexts() as $context)
		{
			if ($context->getName() == $name)
			{
				return $context;
			}
		}
		return null;
	}

	/**
	 * @param array $typology
	 */
	protected function fromArray(array $typology)
	{
		$this->id = isset($typology['id']) ? (int)$typology['id'] : null;
		$this->name = $typology['name'] ?? null;
		$this->title = $typology['title'] ?? null;
		$this->modelName = $typology['modelName'] ?? null;
		$this->groups = [];
		if (isset($typology['groups']) && is_array($typology['groups']))
		{
			$this->setGroups($typology['groups']);
		}
		$this->contexts = [];
		if (isset($typology['contexts']) && is_array($typology['contexts']))
		{
			$this->setContexts($typology['contexts']);
		}
		$this->visibilities = [];
		if (isset($typology['visibilities']) && is_array($typology['visibilities']))
		{
			$this->setVisibilities($typology['visibilities']);
		}
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$result = [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'modelName' => $this->getModelName(),
			'groups' => [],
			'visibilities' => $this->getVisibilities()
		];
		foreach ($this->getGroups() as $group)
		{
			$result['group'][] = $group->toArray();
		}
		return $result;
	}
}