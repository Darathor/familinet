<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;

/**
 * @name \Change\Documents\DocumentCollection
 */
class DocumentCollection implements \Iterator, \Countable, \ArrayAccess
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var integer
	 */
	protected $index = 0;

	/**
	 * @var array
	 */
	protected $entries = [];

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param array $array
	 */
	public function __construct(DocumentManager $documentManager, array $array = null)
	{
		$this->documentManager = $documentManager;
		if ($array)
		{
			foreach ($array as $rawValue)
			{
				if ($rawValue instanceof AbstractDocument)
				{
					$this->entries[] = $this->convertToEntry($rawValue);
				}
				elseif (is_array($rawValue))
				{
					if (count($rawValue) == 2 && isset($rawValue[0], $rawValue[1]))
					{
						$this->entries[] = [(int)$rawValue[0], (string)$rawValue[1]];
					}
					elseif (isset($rawValue['id'], $rawValue['model']))
					{
						$this->entries[] = [(int)$rawValue['id'], (string)$rawValue['model']];
					}
				}
				elseif (is_numeric($rawValue))
				{
					$this->entries[] = [(int)$rawValue, null];
				}
			}
		}
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	public function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
	}

	/**
	 * @param integer $offset
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
		return isset($this->entries[$offset]);
	}

	/**
	 * @param integer $offset
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function offsetGet($offset)
	{
		if (isset($this->entries[$offset]))
		{
			return $this->convertToDocument($this->entries[$offset]);
		}
		return null;
	}

	/**
	 * @param integer $offset
	 * @param \Change\Documents\AbstractDocument $value
	 * @throws \InvalidArgumentException
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		if ($value === null)
		{
			if ($offset !== null)
			{
				$this->offsetUnset($offset);
			}
		}
		elseif ($value instanceof AbstractDocument)
		{
			if ($offset === null)
			{
				$this->entries[] = $this->convertToEntry($value);
			}
			else
			{
				$index = (int)$offset;
				if ($index > $this->count() || $index < 0)
				{
					$this->entries[] = $this->convertToEntry($value);
				}
				else
				{
					$this->entries[$offset] = $this->convertToEntry($value);
				}
			}
		}
		else
		{
			throw new \InvalidArgumentException('Argument 1 must be null or \Change\Documents\AbstractDocument', 50001);
		}
	}

	/**
	 * @param integer $offset
	 */
	public function offsetUnset($offset)
	{
		unset($this->entries[$offset]);
		$this->entries = array_values($this->entries);
	}

	/**
	 * @param array $entry
	 * @return \Change\Documents\AbstractDocument|null
	 */
	protected function convertToDocument($entry)
	{
		return $this->documentManager->getDocumentInstance($entry[0], $entry[1]);
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function convertToEntry(AbstractDocument $document)
	{
		return [$document->getId(), $document->getDocumentModelName()];
	}

	/**
	 * @return\Change\Documents\AbstractDocument|null
	 */
	public function current()
	{
		return $this->convertToDocument($this->entries[$this->index]);
	}

	/**
	 * @return integer
	 */
	public function key()
	{
		return $this->index;
	}

	public function next()
	{
		++$this->index;
	}

	public function rewind()
	{
		$this->index = 0;
	}

	/**
	 * @return boolean
	 */
	public function valid()
	{
		return isset($this->entries[$this->index]);
	}

	/**
	 * @return integer
	 */
	public function count()
	{
		return count($this->entries);
	}

	/**
	 * @api
	 * return integer[]
	 */
	public function ids()
	{
		return array_map(function ($row)
		{
			return $row[0];
		}, $this->entries);
	}

	/**
	 * @api
	 * @return \Change\Documents\AbstractDocument[]
	 */
	public function toArray()
	{
		$documents = [];
		foreach ($this->ids() as $id)
		{
			$document = $this->documentManager->getDocumentInstance($id);
			if ($document instanceof \Change\Documents\AbstractDocument)
			{
				$documents[] = $document;
			}
		}
		return $documents;
	}

	/**
	 * @param string|\Change\Documents\AbstractModel|null $defaultModel
	 * @return $this
	 */
	public function preLoad($defaultModel = null)
	{
		if ($this->entries)
		{
			$ids = [];
			$modelName = null;
			if ($defaultModel)
			{
				$modelName = ($defaultModel instanceof \Change\Documents\AbstractModel) ? $defaultModel->getName() : (string)$defaultModel;
			}
			foreach ($this->entries as list($id, $m))
			{
				$ids[] = [$id, $m?:$modelName];
			}
			$this->documentManager->preLoad($ids);
		}
		return $this;
	}
}