<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Events;

/**
 * @name \Change\Documents\Events\CorrectionListener
 */
class CorrectionListener
{
	public function onMergeContentAt(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$document = $documentManager->getDocumentInstance($job->getArgument('documentId'));
		if ($document instanceof \Change\Documents\Interfaces\Correction)
		{
			$LCID = $job->getArgument('LCID');
			if ($LCID)
			{
				$documentManager->pushLCID($LCID);
			}
			$correction = $document->getCurrentCorrection();
			if ($correction && $correction->getId() == $job->getArgument('correctionId'))
			{
				$transactionManager = $applicationServices->getTransactionManager();
				try
				{
					$transactionManager->begin();
					if (!$document->mergeCurrentCorrection())
					{
						throw new \RuntimeException('Unable to merge correction :' . $correction, 999999);
					}
					$transactionManager->commit();
				}
				catch (\Exception $e)
				{
					throw $transactionManager->rollBack($e);
				}
				finally
				{
					if ($LCID)
					{
						$documentManager->popLCID();
					}
				}
			}
			elseif ($LCID)
			{
				$documentManager->popLCID();
			}
		}
	}
}