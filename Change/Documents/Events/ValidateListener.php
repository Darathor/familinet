<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Events;

use Change\Documents\AbstractDocument;
use Change\Documents\Events\Event as DocumentEvent;
use Change\Documents\Interfaces\Editable;
use Change\Documents\Interfaces\Localizable;
use Change\Documents\Property;
use Change\I18n\PreparedKey;

/**
 * @name \Change\Documents\Events\ValidateListener
 */
class ValidateListener
{
	/**
	 * @var array
	 */
	protected $propertiesErrors;

	/**
	 * @param DocumentEvent $event
	 */
	public function onValidate($event)
	{
		if ($event instanceof DocumentEvent)
		{
			$doc = $event->getDocument();
			$this->propertiesErrors = $event->getParam('propertiesErrors');
			if (!is_array($this->propertiesErrors))
			{
				$this->propertiesErrors = [];
			}
			$this->updateSystemProperties($doc);
			$this->validateProperties($doc, $event);
			/* @var $doc AbstractDocument|Editable */
			if ($event->getName() === DocumentEvent::EVENT_UPDATE && $doc instanceof Editable && $doc->isPropertyModified('documentVersion'))
			{
				$this->addPropertyError('documentVersion', new PreparedKey('c.constraints.isinvalidfield', ['ucf']));
			}
			$event->setParam('propertiesErrors', count($this->propertiesErrors) ? $this->propertiesErrors : null);
		}
	}

	/**
	 * @param AbstractDocument $document
	 */
	protected function updateSystemProperties($document)
	{
		$p = $document->getDocumentModel()->getProperty('creationDate');
		if ($p && $p->getValue($document) === null)
		{
			$p->setValue($document, new \DateTime());
		}

		$p = $document->getDocumentModel()->getProperty('modificationDate');
		if ($p && $p->getValue($document) === null)
		{
			$p->setValue($document, new \DateTime());
		}

		if ($document->getPersistentState() === AbstractDocument::STATE_NEW && $document instanceof Localizable)
		{
			if ($document->getRefLCID() === null)
			{
				$document->setRefLCID($document->getCurrentLCID());
			}
			elseif ($document->getRefLCID() !== $document->getCurrentLCID())
			{
				$this->addPropertyError('refLCID', new PreparedKey('c.constraints.isinvalidfield', ['ucf']));
			}
		}
	}

	/**
	 * @param AbstractDocument $document
	 * @param DocumentEvent $event
	 */
	protected function validateProperties($document, $event)
	{
		$modifiedPropertyNames = $document->getModifiedPropertyNames();
		foreach ($document->getDocumentModel()->getProperties() as $propertyName => $property)
		{
			/* @var $property Property */
			if ($document->isNew() || in_array($propertyName, $modifiedPropertyNames))
			{
				$this->validatePropertyValue($property, $document, $event);
			}
		}
	}

	/**
	 * @param Property $property
	 * @param AbstractDocument $document
	 * @param DocumentEvent $event
	 * @return boolean
	 */
	protected function validatePropertyValue($property, $document, $event)
	{
		$value = $property->getValue($document);
		if ($property->getType() === Property::TYPE_DOCUMENTARRAY)
		{
			$nbValue = count($value);
			if ($nbValue === 0)
			{
				if (!$property->isRequired())
				{
					return true;
				}
				$this->addPropertyError($property->getName(), new PreparedKey('c.constraints.isempty', ['ucf']));
				return false;
			}
			if ($nbValue > $property->getMaxOccurs())
			{
				$args = ['maxOccurs' => $property->getMaxOccurs()];
				$this->addPropertyError($property->getName(),
					new PreparedKey('c.constraints.maxoccurs', ['ucf'], [$args]));
				return false;
			}
			if ($nbValue < $property->getMinOccurs())
			{
				$args = ['minOccurs' => $property->getMinOccurs()];
				$this->addPropertyError($property->getName(),
					new PreparedKey('c.constraints.minoccurs', ['ucf'], [$args]));
				return false;
			}
		}
		elseif ($value === null || $value === '')
		{
			if (!$property->isRequired())
			{
				return true;
			}
			$this->addPropertyError($property->getName(), new PreparedKey('c.constraints.isempty', ['ucf']));
			return false;
		}
		elseif ($property->hasConstraints())
		{
			$constraintManager = $event->getApplicationServices()->getConstraintsManager();
			$defaultParams = ['document' => $document, 'property' => $property, 'documentEvent' => $event];
			foreach ($property->getConstraintArray() as $name => $params)
			{
				$params += $defaultParams;
				$c = $constraintManager->getByName($name, $params);
				if (!$c->isValid($value))
				{
					$this->addPropertyErrors($property->getName(), $c->getMessages());
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @param string $propertyName
	 * @param string[] $errors
	 */
	protected function addPropertyErrors($propertyName, $errors)
	{
		if (is_array($errors) && count($errors))
		{
			foreach ($errors as $error)
			{
				/* @var $error string */
				$this->addPropertyError($propertyName, $error);
			}
		}
	}

	/**
	 * @param string $propertyName
	 * @param string $error
	 */
	protected function addPropertyError($propertyName, $error)
	{
		if ($error !== null)
		{
			$this->propertiesErrors[$propertyName][] = $error;
		}
	}
}
