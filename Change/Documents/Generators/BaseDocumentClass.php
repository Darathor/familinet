<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Generators;

/**
 * @name \Change\Documents\Generators\BaseDocumentClass
 * @api
 */
class BaseDocumentClass
{
	/**
	 * @var \Change\Documents\Generators\Compiler
	 */
	protected $compiler;

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @param string $compilationPath
	 * @return boolean
	 */
	public function savePHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model,
		$compilationPath)
	{
		$code = $this->getPHPCode($compiler, $model);
		$nsParts = explode('\\', $model->getNameSpace());
		$nsParts[] = $model->getShortBaseDocumentClassName() . '.php';
		array_unshift($nsParts, $compilationPath);
		\Change\Stdlib\FileUtils::write(implode(DIRECTORY_SEPARATOR, $nsParts), $code);
		return true;
	}

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @return string
	 */
	public function getPHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model)
	{
		$this->compiler = $compiler;
		$code = '<' . '?php' . PHP_EOL . 'namespace ' . $model->getCompilationNameSpace() . ';' . PHP_EOL;
		$code .= '
/**
 * @name ' . $model->getBaseDocumentClassName() . '
 * @method ' . $model->getModelClassName() . ' getDocumentModel()' . PHP_EOL .
			($model->rootLocalized() ? ' * @method ' . $model->getDocumentLocalizedClassName() . ' getCurrentLocalization()' . PHP_EOL : '') .
			($model->rootLocalized() ? ' * @method ' . $model->getDocumentLocalizedClassName() . ' getRefLocalization()' . PHP_EOL : '') .
			' */' . PHP_EOL;

		$extend = $model->getParentDocumentClassName();

		$interfaces = [];
		$uses = [];

		if ($model->getExtends() === null)
		{
			$uses[] = '\Change\Documents\Traits\DbStorage';
			if ($model->implementCorrection())
			{
				/** @noinspection ClassConstantCanBeUsedInspection */
				$interfaces[] = '\Change\Documents\Interfaces\Correction';
				$uses[] = '\Change\Documents\Traits\Correction';
			}
		}

		// implements , 
		if ($model->getLocalized())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Localizable';
			$uses[] = '\Change\Documents\Traits\Localized';
		}
		if ($model->getEditable())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Editable';
		}
		if ($model->getPublishable())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Publishable';
			$uses[] = '\Change\Documents\Traits\Publication';
		}
		if ($model->getActivable())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Activable';
			$uses[] = '\Change\Documents\Traits\Activation';
		}

		if (count($interfaces))
		{
			$extend .= ' implements ' . implode(', ', $interfaces);
		}

		$code .= 'abstract class ' . $model->getShortBaseDocumentClassName() . ' extends ' . $extend . PHP_EOL;
		$code .= '{' . PHP_EOL;
		if (count($uses))
		{
			$code .= '	use ' . implode(', ', $uses) . ';' . PHP_EOL;
		}

		$properties = $this->getMemberProperties($model);

		if (count($properties))
		{
			$code .= $this->getMembers($model, $properties);

			foreach ($properties as $property)
			{
				if ($property->getLocalized())
				{
					continue;
				}

				/* @var $property \Change\Documents\Generators\Property */
				if ($property->getStateless())
				{
					$code .= $this->getPropertyExtraGetters($property);
					$code .= $this->getPropertyStatelessCode($property);
					continue;
				}
				switch ($property->getType())
				{
					case 'JSON':
						$code .= $this->getPropertyJSONAccessors($property);
						break;
					case 'RichText':
						$code .= $this->getPropertyRichTextAccessors($property);
						break;
					case 'Inline':
						$code .= $this->getPropertyInlineAccessors($property);
						break;
					case 'InlineArray':
						$code .= $this->getPropertyInlineArrayAccessors($property);
						break;
					case 'DocumentArray':
						$code .= $this->getPropertyDocumentArrayAccessors($property);
						break;
					case 'Document':
						$code .= $this->getPropertyDocumentAccessors($model, $property);
						break;
					case 'Boolean':
						$code .= $this->getPropertyBooleanAccessors($property);
						break;
					/** @noinspection PhpMissingBreakStatementInspection */
					case 'DocumentId':
						$code .= $this->getPropertyExtraGetters($property);
					case 'Integer':
						$code .= $this->getPropertyIntegerAccessors($property);
						break;
					case 'Float':
					case 'Decimal':
						$code .= $this->getPropertyFloatAccessors($property);
						break;
					case 'DateTime':
					case 'Date':
						$code .= $this->getPropertyDateTimeAccessors($property);
						break;
					/** @noinspection PhpMissingBreakStatementInspection */
					case 'StorageUri':
						$code .= $this->getPropertyExtraGetters($property);
					default:
						$code .= $this->getPropertyStringAccessors($property);
						break;

				}
			}
		}

		if ($model->getEditable())
		{
			$code .= $this->getEditableInterface($model);
		}

		$code .= '}' . PHP_EOL;
		$this->compiler = null;
		return $code;
	}

	/**
	 * @param mixed $value
	 * @param boolean $removeSpace
	 * @return string
	 */
	protected function escapePHPValue($value, $removeSpace = true)
	{
		if ($removeSpace)
		{
			return str_replace([PHP_EOL, ' ', "\t"], '', var_export($value, true));
		}
		return var_export($value, true);
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @return string
	 */
	protected function getEditableInterface($model)
	{
		$code = '';

		if ($model->getLocalized())
		{
			$code .= '
	/**
	 * @param \Change\User\UserInterface $user
	 * @return $this
	 */
	public function setOwnerUser(\Change\User\UserInterface $user)
	{
		$cl = $this->getCurrentLocalization();
		if (!$cl->getAuthorId())
		{
			$cl->setAuthorId($user->getId());
		}
		return $this;
	}' . PHP_EOL;
		}
		else
		{
			$code .= '
	/**
	 * @param \Change\User\UserInterface $user
	 * @return $this
	 */
	public function setOwnerUser(\Change\User\UserInterface $user)
	{
		if (!$this->getAuthorId())
		{
			$this->setAuthorId($user->getId());
		}
		return $this;
	}' . PHP_EOL;
		}
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @return \Change\Documents\Generators\Property[]
	 */
	protected function getMemberProperties($model)
	{
		$properties = [];
		foreach ($model->getProperties() as $property)
		{
			/* @var $property \Change\Documents\Generators\Property */
			if (!$property->getParent())
			{
				$properties[$property->getName()] = $property;
			}
		}
		return $properties;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembers($model, $properties)
	{
		$resetProperties = [];
		$modifiedProperties = [];
		$removeOldPropertiesValue = [];
		$clearModifiedProperties = [];
		$cleanUp = [];
		if ($model->getLocalized())
		{
			$resetProperties[] = '$this->resetCurrentLocalized();';
			$modifiedProperties[] = '$names = array_merge($names, $this->getCurrentLocalization()->getModifiedPropertyNames());';
		}
		$code = '';

		$toDb = [];
		$fromDb = [];
		$relationsToDb = [];

		$relationsFromDb = [];

		$defaultValues = [];

		foreach ($properties as $property)
		{
			/* @var $property \Change\Documents\Generators\Property */
			$propertyName = $property->getName();
			if ($property->getStateless())
			{
				continue;
			}
			$constName = var_export($propertyName, true);
			if ($property->getLocalized())
			{
				$removeOldPropertiesValue[] =
					'case ' . $constName . ': $this->getCurrentLocalization()->removeOldPropertyValue($propertyName); return;';
				continue;
			}
			$memberValue = ';';
			$mn = '$this->' . $propertyName;
			$methodPart = ucfirst($propertyName);

			switch ($property->getType())
			{
				case 'DocumentArray':
					$memberValue = ' = 0;';
					$modifiedProperties[] =
						'if (' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty && '
						. $mn. '->isModified()) {$names[] = ' . $constName . ';}';
					$removeOldPropertiesValue[] =
						'case ' . $constName . ': if (' . $mn
						. ' instanceof \Change\Documents\DocumentArrayProperty) {' . $mn . '->setAsDefault();} return;';
					$clearModifiedProperties[] = '$this->removeOldPropertyValue(' . $constName . ');';
					$relationsToDb[] = '$relationsToDb[' . $constName . '] = ' . $mn
						. ' instanceof \Change\Documents\DocumentArrayProperty ? ' . $mn . '->getIds() : null;';

					$modelName = $this->escapePHPValue($property->getDocumentType(), false);
					$relationsFromDb[] = $mn . ' = new \Change\Documents\DocumentArrayProperty($this->documentManager, ' . $modelName . ');';
					$relationsFromDb[] = $mn . '->setDefaultIds($dbRelations[' . $constName . ']);';

					$fromDb[] = $mn . ' = (int)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty ? ' . $mn . '->count() : (int)' . $mn . ';';
					break;
				case 'Inline':
					$code .= '
	/**
	 * @var ' . $this->getCommentaryMemberType($property) . '|boolean
	 */
	private $' . $propertyName . 'Old = false;' . PHP_EOL;
					$resetProperties[] = 'if (' . $mn . ') {' . $mn . '->link(null);}';
					$resetProperties[] = $mn . 'Old = false;';
					$modifiedProperties[] = 'if ($this->checkModified' . $methodPart . '()) {$names[] = ' . $constName . ';}';
					$removeOldPropertiesValue[] = 'case ' . $constName . ': ' . $mn . 'Old = false; return;';
					$clearModifiedProperties[] = '$this->removeOldPropertyValue(' . $constName . ');';
					$cleanUp[] = 'if (' . $mn . ' instanceof \Change\Documents\AbstractInline) {' . $mn . '->cleanUp();}';

					$fromDb[] = '/** @noinspection UnserializeExploitsInspection */
		if ($d = ($data[' . $constName . '] ? unserialize($data[' . $constName . ']) : null)) 
		{
			$dm = $this->documentManager; $m = $dm->getModelManager()->getModelByName($d[\'model\']);
			' . $mn . ' = $dm->getNewInlineInstanceByModel($m, false);
			' . $mn . '->dbData($d)->link(function () { $this->checkModified' . $methodPart . '(true); });
		}';
					$toDb[] =
						'$toDb[' . $constName . '] = ' . $mn . ' ? serialize(' . $mn . '->link(function () {$this->checkModified'
						. $methodPart . '(true);})->dbData()) : null;';
					break;
				case 'InlineArray':
					$modifiedProperties[] =
						'if (' . $mn . ' instanceof \Change\Documents\InlineArrayProperty && '
						. $mn . '->isModified()) {$names[] = ' . $constName . ';}';
					$removeOldPropertiesValue[] =
						'case ' . $constName . ': if (' . $mn . ' instanceof \Change\Documents\InlineArrayProperty) {'
						. $mn . '->setAsDefault();} return;';
					$clearModifiedProperties[] = '$this->removeOldPropertyValue(' . $constName . ');';
					$cleanUp[] = 'if (' . $mn . ' instanceof \Change\Documents\InlineArrayProperty) {' . $mn . '->cleanUp();}';

					$fromDb[] = '/** @noinspection UnserializeExploitsInspection */
		if ($d = ($data[' . $constName . '] ? unserialize($data[' . $constName . ']) : null)) 
		{
			$this->check' . $methodPart . 'Initialized();
			' . $mn . '->dbData($d);
		}';
					$toDb[] =
						'$toDb[' . $constName . '] = ' . $mn . ' && ' . $mn . '->count() ? serialize(' . $mn . '->dbData()) : null;';
					break;
				case 'RichText':
					$modifiedProperties[] = 'if (' . $mn . ' !== null && ' . $mn . '->isModified()) {$names[] = ' . $constName . ';}';
					$removeOldPropertiesValue[] = 'case ' . $constName . ': if (' . $mn . ' !== null) {' . $mn . '->setAsDefault();} return;';
					$clearModifiedProperties[] = '$this->removeOldPropertyValue(' . $constName . ');';

					$fromDb[] = $mn . ' = new \Change\Documents\RichtextProperty($data[' . $constName . ']);';
					$toDb[] = '$toDb[' . $constName . '] = (' . $mn . ' !== null) ? ' . $mn . '->toJSONString() : null;';
					break;
				case 'Document':
					$memberValue = ' = 0;';

					$fromDb[] = $mn . ' = (int)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = (int)' . $mn . ';';
					break;
				case 'DocumentId':
					$memberValue = ' = 0;';

					$fromDb[] = $mn . ' = (int)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = (int)' . $mn . ';';
					break;
				case 'Boolean':
					$memberValue = ' = false;';
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = $dv  === \'true\';}';

					$fromDb[] = $mn . ' = (bool)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = (bool)' . $mn . ';';
					break;
				case 'Integer':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (int)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (int)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' !== null ? (int)' . $mn . ' : null;';
					break;
				case 'Float':
				case 'Decimal':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (float)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (float)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' !== null ? (float)' . $mn . ' : null;';
					break;
				case 'DateTime':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = new \DateTime($dv);}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] instanceof \DateTime ? $data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' ? (new \DateTime(' . $mn
						. '->format(\DateTime::ATOM)))->setTimezone(new \DateTimeZone(\'UTC\')) : null;';
					break;
				case 'Date':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (new \DateTime($dv))->setTime(0, 0);}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] instanceof \DateTime ? $data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = '. $mn .' ? new \DateTime(' . $mn . '->format(\'Y-m-d\T00:00:00\'), new \DateTimeZone(\'UTC\')) : null;';
					break;
				case 'String':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'LongString':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'StorageUri':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'JSON':
					$fromDb[] = $mn . ' = $data[' . $constName . '] ? json_decode($data[' . $constName . '], true) : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' ? json_encode(' . $mn . ') : null;';
					break;
				default:
					throw new \LogicException('Invalid type ' . $property->getType() . ' on ' . $model->getName() . ' / ' . $propertyName);
			}

			$resetProperties[] = $mn . ($memberValue !== ';' ? $memberValue : ' = null;');
			$code .= '
	/**
	 * @var ' . $this->getCommentaryMemberType($property) . '
	 */	
	private $' . $propertyName . $memberValue . PHP_EOL;
		}

		$code .= '
	/**
	 * @api
	 */
	public function unsetProperties()
	{
		parent::unsetProperties();
		' . implode(PHP_EOL . '		', $resetProperties) . '
	}' . PHP_EOL;

		if (count($modifiedProperties))
		{
			$code .= '
	/**
	 * @api
	 * @return string[]
	 */
	public function getModifiedPropertyNames()
	{
		$names =  parent::getModifiedPropertyNames();
		' . implode(PHP_EOL . '		', $modifiedProperties) . '
		return $names;
	}' . PHP_EOL;
		}

		if ($defaultValues)
		{
			$code .= '
	/**
	 * @return $this
	 */
	public function setDefaultValues()
	{
		parent::setDefaultValues();
		' . implode(PHP_EOL . '		', $defaultValues) . '
		return $this;
	}' . PHP_EOL;
		}

		if ($cleanUp)
		{
			$code .= '
	public function cleanUp()
	{
		parent::cleanUp();
		' . implode(PHP_EOL . '		', $cleanUp) . '
	}' . PHP_EOL;
		}

		if ($removeOldPropertiesValue)
		{
			$code .= '
	/**
	 * @api
	 * @param string $propertyName
	 */
	public function removeOldPropertyValue($propertyName)
	{
		switch ($propertyName)
		{
			' . implode(PHP_EOL . '			', $removeOldPropertiesValue) . '
			default:
				parent::removeOldPropertyValue($propertyName);
		}
	}' . PHP_EOL;
		}

		if ($clearModifiedProperties)
		{
			$code .= '
	protected function clearModifiedProperties()
	{
		parent::clearModifiedProperties();
		' . implode(PHP_EOL . '		', $clearModifiedProperties) . '
	}' . PHP_EOL;
		}

		$extends = $model->getExtends();
		if ($toDb || !$extends)
		{
			$code .= '
	/**
	 * @ignore
	 * @return array
	 */
	public function toDb()
	{
		$toDb = ' . ($extends ? 'parent::toDb()' : '[]') . ';
		' . implode(PHP_EOL . '		', $toDb) . '
		return $toDb;
	}

	/**
	 * @ignore
	 * @return array
	 */
	public function relationsToDb()
	{
		$relationsToDb = ' . ($extends ? 'parent::relationsToDb()' : '[]') . ';
		' . implode(PHP_EOL . '		', $relationsToDb) . '
		return $relationsToDb;
	}

	/**
	 * @ignore
	 * @param array $data
	 * @return $this
	 */
	public function fromDb(array $data)
	{
		' . implode(PHP_EOL . '		', $fromDb) . '
		return ' . ($extends ? 'parent::fromDb($data)' : '$this') . ';
	}

	/**
	 * @ignore
	 * @param array $dbRelations
	 * @return $this
	 */
	public function relationsFromDb(array $dbRelations)
	{
		' . implode(PHP_EOL . '		', $relationsFromDb) . '
		return ' . ($extends ? 'parent::relationsFromDb($dbRelations)' : '$this') . ';
	}' . PHP_EOL;
		}
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryType($property)
	{
		switch ($property->getComputedType())
		{
			case 'Boolean' :
				return 'bool';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
				return 'int';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'Document' :
			case 'DocumentArray' :
				if ($property->getDocumentType() === null)
				{
					/** @noinspection ClassConstantCanBeUsedInspection */
					return '\Change\Documents\AbstractDocument';
				}
				return $this->compiler->getModelByName($property->getDocumentType())->getDocumentClassName();
			case 'Inline' :
			case 'InlineArray' :
				return $this->compiler->getModelByName($property->getInlineType())->getDocumentClassName();
			case 'JSON' :
				return 'array';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryMemberType($property)
	{
		switch ($property->getType())
		{
			case 'Boolean' :
				return 'bool';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
			case 'Document' :
				return 'int';
			case 'DocumentArray' :
				return 'int|\Change\Documents\DocumentArrayProperty';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			case 'Inline' :
				return $this->compiler->getModelByName($property->getInlineType())->getDocumentClassName();
			case 'InlineArray' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\InlineArrayProperty';
			case 'JSON' :
				return 'array';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyBooleanAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return bool|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return bool
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @param bool ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(bool ' . $var . ')
	{
		$this->load();
		if (' .  $mn .' !== ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . ']' . ' === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyIntegerAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return int|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return int|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @param int|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(int ' . $var . ' = null)
	{
		'. ($property->getType() === 'DocumentId' ? $var . ' = (int)' . $var .'; ' : '') . '$this->load();
		if (' . $var . ' !== ' . $mn . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyFloatAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return float|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return float|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @param float|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(float ' . $var . ' = null)
	{
		$this->load();
		if (!\Change\Stdlib\FloatUtils::equals(' . $mn . ', ' . $var . '))
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if (\Change\Stdlib\FloatUtils::equals($this->modifiedProperties[' . $en . '], ' . $var . '))
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDateTimeAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$setConverter = '';
		if ($property->getType() === 'Date')
		{
			$setConverter = 'if (' . $var . ') {' . $var . '->setTime(0, 0);} ';
		}
		elseif ($name === 'startPublication' || $name === 'startActivation')
		{
			$setConverter = $var . ' = ' . $var . ' ?? new \DateTime(\Change\Documents\Property::DEFAULT_START_DATE); ';
		}
		elseif ($name === 'endPublication' || $name === 'endActivation')
		{
			$setConverter = $var . ' = ' . $var . ' ?? new \DateTime(\Change\Documents\Property::DEFAULT_END_DATE); ';
		}

		$code = '
	/**
	 * @return \DateTime|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return \DateTime|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}
	
	/**
	 * @return string|null
	 */
	public function get' . $un . 'AsString()
	{
		$this->load();
		return ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format('
			. ($property->getType() === 'Date' ? '\'Y-m-d\'' : '\DateTime::ATOM') . ') : null;
	}

	/**
	 * @param \DateTime|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(\DateTime ' . $var . ' = null)
	{
		'. $setConverter . '$this->load();
		if (' . $mn . ' != ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				/** @noinspection TypeUnsafeComparisonInspection */
				if ($this->modifiedProperties[' . $en . '] == ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyStringAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = \ucfirst($name);

		$code = '
	/**
	 * @return string|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return string|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @param string|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		' . $var . ' = ' . $var . ' === null ? null : (string)' . $var . ';
		$this->load();
		if (' . $var . ' !== ' . $mn  . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyExtraGetters($property)
	{
		$code = '';
		$name = $property->getName();
		$un = ucfirst($name);
		if ($property->getType() === 'DocumentId')
		{
			if ($property->getDocumentType() === null)
			{
				$ct = \Change\Documents\AbstractDocument::class;
			}
			else
			{
				$ct = $this->compiler->getModelByName($property->getDocumentType())->getDocumentClassName();
			}

			$code .= '
	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . 'Instance()
	{
		return $this->getDocumentManager()->getDocumentInstance($this->get' . $un . '());
	}' . PHP_EOL;
		}
		elseif ($property->getType() === 'StorageUri')
		{
			$code .= '
	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return \Change\Storage\ItemInfo|null
	 */
	public function get' . $un . 'ItemInfo(\Change\Storage\StorageManager $storageManager)
	{
		$uri = $this->get' . $un . '();
		if ($uri)
		{
			return $storageManager->getItemInfo($uri);
		}
		return null;
	}' . PHP_EOL;
		}
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyStatelessCode($property)
	{
		$name = $property->getName();
		if ($name === 'creationDate' || $name ==='modificationDate')
		{
			return '';
		}
		$code = [];

		$var = '$' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		if ($name === 'publicationSections')
		{
			$code[] = '
	/**
	 * @return \Change\Presentation\Interfaces\Section[]
	 */
	public function get' . $un . '()
	{
		return [];
	}

	/**
	 * @param \Change\Presentation\Interfaces\Section[] ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		return $this;
	}';
		}
		else
		{
			$code[] = '
	/**
	 * @return ' . $ct . '
	 */
	abstract public function get' . $un . '();

	/**
	 * @param ' . $ct . ' ' . $var . '
	 * @return $this
	 */
	abstract public function set' . $un . '(' . $var . ');';
		}
		if ($property->getType() === 'JSON')
		{
			$code[] = '
	/**
	 * @return string|null
	 */
	public function get' . $un . 'String()
	{
		' . $var . ' = $this->get' . $un . '();
		return (' . $var . ' === null) ? null : json_encode(' . $var . ');
	}';
		}
		elseif ($property->getType() === 'Document')
		{
			$code[] = '
	/**
	 * @return integer|null
	 */
	public function get' . $un . 'Id()
	{
		' . $var . ' = $this->get' . $un . '();
		return ' . $var . ' instanceof \Change\Documents\AbstractDocument ? ' . $var . '->getId() : null;
	}';
		}
		elseif ($property->getType() === 'DocumentArray')
		{
			$code[] = '
	/**
	 * @return integer[]
	 */
	public function get' . $un . 'Ids()
	{
		$result = [];
		' . $var . ' = $this->get' . $un . '();
		if (is_array(' . $var . '))
		{
			foreach (' . $var . ' as $o)
			{
				if ($o instanceof \Change\Documents\AbstractDocument) {$result[] = $o->getId();}
			}
		}
		return $result;
	}';
		}
		return implode(PHP_EOL, $code);
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyJSONAccessors($property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return string|null
	 */
	public function get' . $un . 'OldStringValue()
	{
		' . $var . ' = $this->getOldPropertyValue(' . $en . ');
		return ' . $var . ' ? json_encode(' . $var . ') : null;
	}

	/**
	 * @return array|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @param array|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(array ' . $var . ' = null)
	{
		$this->load();
		' . $var . ' = ' . $var . ' ?: null;
		if (' . $mn . ' !== ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function get' . $un . 'String()
	{
		$this->load(); return ' . $mn . ' ? json_encode(' . $mn . ') : null;
	}

	/**
	 * @return array|null
	 */
	public function get' . $un . '()
	{
		$this->load(); return ' . $mn . ';
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyRichTextAccessors($property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$un = ucfirst($name);

		$code = '
	protected function checkLoaded' . $un . '()
	{
		$this->load();
		if (' . $mn . ' === null) {' . $mn . ' = new \Change\Documents\RichtextProperty();}
	}

	/**
	 * @return \Change\Documents\RichtextProperty
	 */
	public function get' . $un . 'OldValue()
	{
		return new \Change\Documents\RichtextProperty((' . $mn . ' !== null) ? ' . $mn . '->getDefaultJSONString() : null);
	}

	/**
	 * @param string|array|\Change\Documents\RichtextProperty|null ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		$this->checkLoaded' . $un . '();
		if (is_string(' . $var . '))
		{
			' . $mn . '->fromJSONString(' . $var . ');
		}
		elseif (' . $var . ' === null || is_array(' . $var . '))
		{
			' . $mn . '->fromArray(' . $var . ');
		}
		elseif (' . $var . ' instanceof \Change\Documents\RichtextProperty)
		{
			' . $mn . '->fromRichtextProperty(' . $var . ');
		}
		else
		{
			throw new \InvalidArgumentException(\'Argument 1 must be an array, string, \Change\Documents\RichtextProperty or null\', 52005);
		}
		return $this;
	}

	/**
	 * @return \Change\Documents\RichtextProperty
	 */
	public function get' . $un . '()
	{
		$this->checkLoaded' . $un . '();
		return ' . $mn . ';
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyInlineAccessors($property)
	{
		$inlineType = $property->getInlineType();
		$inlineModel = $this->compiler->getModelByName($inlineType);
		$inlineModelShortName = $inlineModel->getShortName();
		$inlineModelClassName = $inlineModel->getDocumentClassName();
		$name = $property->getName();
		$mn = '$this->' . $name;
		$mno = $mn . 'Old';
		$var = '$' . $name;
		$un = ucfirst($name);

		$code = '
	/**
	 * @param boolean|null $modified
	 * @return boolean
	 */
	protected function checkModified' . $un . '($modified = null)
	{
		if (is_bool($modified))
		{
			if ($modified)
			{
				if (' . $mno . ' === false)
				{
					' . $mno . ' = ' . $mn . ' ? clone ' . $mn . ' : null;
				}
			}
			else
			{
				' . $mno . ' = false;
			}
			return $modified;
		}
		$modified = false;
		if (' . $mno . ' !== false && ' . $mn . ' !== ' . $mno . ')
		{
			$modified = ' . $mn . ' ? !' . $mn . '->isEquals(' . $mno . ') : true;
			if (!$modified)
			{
				' . $mno . ' = false;
			}
		}
		return $modified;
	}

	/**
	 * @return ' . $inlineModelClassName . '
	 */
	public function new' . $inlineModelShortName . '()
	{
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $this->getDocumentManager()->getNewInlineInstanceByModelName(\'' . $inlineType . '\');
	}

	/**
	 * @param ' . $inlineModelClassName . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '('. $inlineModelClassName . ' ' . $var . ' = null)
	{
		$this->load();
		$this->checkModified' . $un . '(true);
		' . $mn . ' = ' . $var . ';
		if (' . $var . ')
		{
			' . $var . '->link(function () {$this->checkModified' . $un . '(true);});
		}
		return $this;
	}

	/**
	 * @return ' . $inlineModelClassName . '|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @return ' . $inlineModelClassName . '|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->checkModified' . $un . '() ? ' . $mno . ' : null;
	}' . PHP_EOL;

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyInlineArrayAccessors($property)
	{
		$inlineType = $property->getInlineType();
		$inlineModel = $this->compiler->getModelByName($inlineType);
		$inlineModelShortName = $inlineModel->getShortName();
		$inlineModelClassName = $inlineModel->getDocumentClassName();
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$un = ucfirst($name);

		$code = '

	protected function check' . $un . 'Initialized()
	{
		if (' . $mn . ' === null)
		{
			$elements = new \Change\Documents\InlineArrayProperty($this->documentManager, \'' . $inlineType . '\');
			' . $mn . ' = $elements;
		}
	}

	/**
	 * @return ' . $inlineModelClassName . '
	 */
	public function new' . $inlineModelShortName . '()
	{
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $this->documentManager->getNewInlineInstanceByModelName(\'' . $inlineType . '\');
	}

	/**
	 * @param ' . $inlineModelClassName . '[]|iterable ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(iterable ' . $var . ' = [])
	{
		$this->load();
		$this->check' . $un . 'Initialized();
		' . $mn . '->setAll('.$var.');
		return $this;
	}

	/**
	 * @return \Change\Documents\InlineArrayProperty|' . $inlineModelClassName . '[]
	 */
	public function get' . $un . '()
	{
		$this->load();
		$this->check' . $un . 'Initialized();
		return ' . $mn . ';
	}

	/**
	 * @return ' . $inlineModelClassName . '[]
	 */
	public function get' . $un . 'OldValue()
	{
		if (' . $mn . ' instanceof \Change\Documents\InlineArrayProperty && ' . $mn . '->isModified())
		{
			/** @noinspection PhpIncompatibleReturnTypeInspection */
			return ' . $mn . '->getDefaultDocuments();
		}
		return [];
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDocumentAccessors($model, $property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$en = $this->escapePHPValue($name);
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return integer|null
	 */
	public function get' . $un . 'OldValueId()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . 'OldValue()
	{
		$oldId = $this->get' . $un . 'OldValueId();
		$doc = ($oldId !== null) ? $this->documentManager->getDocumentInstance($oldId) : null;
		return $doc instanceof ' . $ct . ' ? $doc : null;
	}

	/**
	 * @param ' . $ct . ' ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(' . $ct . ' ' . $var . ' = null)
	{
		if (' . $var . ' && '. $var . '->getId() <= 0)
		{
			throw new \InvalidArgumentException(\'Argument 1 must be a saved document\', 52005);
		}
		$this->load();
		$newId = ' . $var . ' ? ' . $var . '->getId() : 0;
		if (' . $mn . ' !== $newId)
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === $newId)
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = $newId;
		}
		return $this;
	}

	/**
	 * @return integer
	 */
	public function get' . $un . 'Id()
	{
		$this->load();
		return ' . $mn . ';
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		$this->load();
		$doc = ' . $mn . ' ? $this->documentManager->getDocumentInstance(' . $mn . ') : null;
		return $doc instanceof ' . $ct . ' ? $doc : null;
	}' . PHP_EOL;

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDocumentArrayAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$ct = $this->getCommentaryType($property);
		$modelName = $this->escapePHPValue($property->getDocumentType(), false);
		$un = ucfirst($name);
		$code = '
	protected function checkLoaded' . $un . '()
	{
		$this->load();
		if (!(' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty))
		{
			' . $mn . ' = new \Change\Documents\DocumentArrayProperty($this->documentManager, ' . $modelName . ');
			if ($this->getPersistentState() === static::STATE_LOADED)
			{
				$ids = $this->getPropertyDocumentIds(' . $en . ');
				' . $mn . '->setDefaultIds($ids);
			}
		}
	}

	/**
	 * @param ' . $ct . '[]|iterable ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(iterable ' . $var . ' = [])
	{
		$this->checkLoaded' . $un . '();
		' . $mn . '->fromArray(' . $var . ');
		return $this;
	}

	/**
	 * @return ' . $ct . '[]|\Change\Documents\DocumentArrayProperty
	 */
	public function get' . $un . '()
	{
		$this->checkLoaded' . $un . '();
		return ' . $mn . ';
	}

	/**
	 * @return integer
	 */
	public function get' . $un . 'Count()
	{
		$this->load();
		return (' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty) ? ' . $mn . '->count() : ' . $mn . ';
	}

	/**
	 * @return ' . $ct . '[]
	 */
	public function get' . $un . 'OldValue()
	{
		if (' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty && ' . $mn . '->isModified())
		{
			/** @noinspection PhpIncompatibleReturnTypeInspection */
			return ' . $mn . '->getDefaultDocuments();
		}
		return [];
	}

	/**
	 * @return integer[]
	 */
	public function get' . $un . 'Ids()
	{
		$this->checkLoaded' . $un . '();
		return ' . $mn . '->getIds();
	}

	/**
	 * @return integer[]
	 */
	public function get' . $un . 'OldValueIds()
	{
		if (' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty && ' . $mn . '->isModified())
		{
			return ' . $mn . '->getDefaultIds();
		}
		return [];
	}' . PHP_EOL;
		return $code;
	}
}