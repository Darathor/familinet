<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Generators;

/**
 * @name \Change\Documents\Generators\BaseInlineClass
 * @api
 */
class BaseInlineClass
{
	/**
	 * @var \Change\Documents\Generators\Compiler
	 */
	protected $compiler;

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @param string $compilationPath
	 * @return boolean
	 */
	public function savePHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model,
		$compilationPath)
	{
		$code = $this->getPHPCode($compiler, $model);
		$nsParts = explode('\\', $model->getNameSpace());
		$nsParts[] = $model->getShortBaseDocumentClassName() . '.php';
		array_unshift($nsParts, $compilationPath);
		\Change\Stdlib\FileUtils::write(implode(DIRECTORY_SEPARATOR, $nsParts), $code);
		return true;
	}

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @return string
	 */
	public function getPHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model)
	{
		$this->compiler = $compiler;
		$code = '<' . '?php' . PHP_EOL . 'namespace ' . $model->getCompilationNameSpace() . ';' . PHP_EOL;
		$code .= '
/**
 * @name ' . $model->getBaseDocumentClassName() . '
 * @method ' . $model->getModelClassName() . ' getDocumentModel()' . PHP_EOL .
			($model->rootLocalized() ? ' * @method ' . $model->getDocumentLocalizedClassName() . ' getCurrentLocalization()' . PHP_EOL : '') .
			($model->rootLocalized() ? ' * @method ' . $model->getDocumentLocalizedClassName() . ' getRefLocalization()' . PHP_EOL : '') .
			' */' . PHP_EOL;

		$extend = $model->getParentDocumentClassName();

		$interfaces = [];
		$uses = [];

		// implements , 
		if ($model->getLocalized())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Localizable';
			$uses[] = '\Change\Documents\Traits\InlineLocalized';
		}
		if ($model->getActivable())
		{
			/** @noinspection ClassConstantCanBeUsedInspection */
			$interfaces[] = '\Change\Documents\Interfaces\Activable';
			$uses[] = '\Change\Documents\Traits\InlineActivation';
		}

		if (count($interfaces))
		{
			$extend .= ' implements ' . implode(', ', $interfaces);
		}

		$code .= 'abstract class ' . $model->getShortBaseDocumentClassName() . ' extends ' . $extend . PHP_EOL;
		$code .= '{' . PHP_EOL;
		if (count($uses))
		{
			$code .= '	use ' . implode(', ', $uses) . ';' . PHP_EOL;
		}

		$properties = $this->getMemberProperties($model);

		if (count($properties))
		{
			$code .= $this->getMembers($model, $properties);

			foreach ($properties as $property)
			{
				if ($property->getLocalized())
				{
					continue;
				}

				/* @var $property \Change\Documents\Generators\Property */
				if ($property->getStateless())
				{
					$code .= $this->getPropertyStatelessCode($property);
				}
				elseif ($property->getType() === 'JSON')
				{
					$code .= $this->getPropertyJSONAccessors($property);
				}
				elseif ($property->getType() === 'RichText')
				{
					$code .= $this->getPropertyRichTextAccessors($property);
				}
				elseif ($property->getType() === 'DocumentArray')
				{
					$code .= $this->getPropertyDocumentArrayAccessors($property);
				}
				elseif ($property->getType() === 'Document')
				{
					$code .= $this->getPropertyDocumentAccessors($property);
				}
				else
				{
					$code .= $this->getPropertyAccessors($property);
				}
			}
		}

		$code .= '}' . PHP_EOL;
		$this->compiler = null;
		return $code;
	}

	/**
	 * @param mixed $value
	 * @param boolean $removeSpace
	 * @return string
	 */
	protected function escapePHPValue($value, $removeSpace = true)
	{
		if ($removeSpace)
		{
			return str_replace([PHP_EOL, ' ', "\t"], '', var_export($value, true));
		}
		return var_export($value, true);
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @return \Change\Documents\Generators\Property[]
	 */
	protected function getMemberProperties($model)
	{
		$properties = [];
		foreach ($model->getProperties() as $property)
		{
			/* @var $property \Change\Documents\Generators\Property */
			if (!$property->getParent())
			{
				$properties[$property->getName()] = $property;
			}
		}
		return $properties;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembers($model, $properties)
	{
		$unsetProperties = [];
		$fromDbData = [];
		$toDbData = [];
		$defaultValues = [];

		if ($model->getLocalized())
		{
			$unsetProperties[] = '$this->resetCurrentLocalized();';
			$fromDbData[] = '$this->localizedDbData($dbData[\'_LCID\'] ?? null);';
			$toDbData[] = '$dbData[\'_LCID\'] = $this->localizedDbData();';
		}

		$code = '';
		foreach ($properties as $property)
		{
			if ($property->getStateless() || $property->getLocalized())
			{
				continue;
			}
			$propertyName = $property->getName();
			$mn = '$this->' . $propertyName;
			$en = var_export($propertyName, true);
			$propertyType = $property->getType();
			$memberValue = ';';
			if ($propertyType === 'DocumentId' || $propertyType === 'Document')
			{
				$memberValue = ' = 0;';
			}
			elseif ($propertyType === 'Boolean')
			{
				$memberValue = ' = false;';
			}

			switch ($propertyType)
			{
				case 'Date':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = '$v = $dbData[' . $en . '] ?? null; ' . $mn . ' = $v ? \DateTime::createFromFormat(\'Y-m-d\', $v, new \DateTimeZone(\'UTC\'))->setTime(0, 0) : null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format(\'Y-m-d\') : null;';
					break;
				case 'DateTime':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = '$v = $dbData[' . $en . '] ?? null; ' . $mn . ' = $v ? new \DateTime($v) : null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format(\DateTime::ATOM) : null;';
					break;
				case 'JSON':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = $mn . ' = $dbData[' . $en . '] ?? null;';
					$toDbData[] = '$dbData[' . $en . '] = (' . $mn . ' && is_array(' . $mn . ')) ? ' . $mn . ' : null;';
					break;
				case 'RichText':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = $mn . ' = new \Change\Documents\RichtextProperty($dbData[' . $en . '] ?? null);';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . '  instanceof \Change\Documents\RichtextProperty && !' . $mn . '->isEmpty() ? ' . $mn
						. '->toArray() : null;';
					break;
				case 'Document':
					$unsetProperties[] = '$this->' . $propertyName . ' = 0;';
					$fromDbData[] = $mn . ' = (int)($dbData[' . $en . '] ?? 0);';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ';';
					break;
				case 'DocumentArray':
					$modelName = $this->escapePHPValue($property->getDocumentType(), false);
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = $mn . ' = new \Change\Documents\DocumentArrayProperty($this->getDocumentManager(), ' . $modelName . ');
		' . $mn . '->setDefaultIds($dbData[' . $en . '] ?? []);';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ' instanceof \Change\Documents\DocumentArrayProperty ? ' . $mn
						. '->getIds() : [];';
					break;
				default:
					$unsetProperties[] = '$this->' . $propertyName . ($memberValue === ';' ? ' = null;' : $memberValue);
					$fromDbData[] = $mn . ' = $dbData[' . $en . '] ?? null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ';';
			}

			switch ($propertyType)
			{
				case 'Boolean':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' .$mn . ' = $dv  === \'true\';}';
					break;
				case 'Integer':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (int)$dv;}';
					break;
				case 'Float':
				case 'Decimal':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (float)$dv;}';
					break;
				case 'DateTime':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = new \DateTime($dv);}';
					break;
				case 'Date':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (new \DateTime($dv))->setTime(0, 0);}';
					break;
				case 'String':
				case 'LongString':
				case 'StorageUri':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';
					break;
			}

			$code .= '
	/**
	 * @var ' . $this->getCommentaryMemberType($property) . '
	 */	
	private $' . $propertyName . $memberValue . PHP_EOL;
		}

		if ($model->getLocalized())
		{
			$code .= '
	public function cleanUp()
	{
		parent::cleanUp();
		$this->cleanUpLocalized();
	}' . PHP_EOL;
		}

		$code .= '
	/**
	 * @api
	 */
	public function unsetProperties()
	{
		parent::unsetProperties();
		' . implode(PHP_EOL . '		', $unsetProperties) . '
	}' . PHP_EOL;

		if ($defaultValues)
		{
			$code .= '
	/**
	 * @return $this
	 */
	public function setDefaultValues()
	{
		parent::setDefaultValues();
		' . implode(PHP_EOL . '		', $defaultValues) . '
		return $this;
	}' . PHP_EOL;
		}

		if (count($fromDbData))
		{
			$code .= '
	/**
	 * @api
	 * @param array $dbData
	 */
	public function fromDbData(array $dbData)
	{
		parent::fromDbData($dbData);
		' . implode(PHP_EOL . '		', $fromDbData) . '
	}' . PHP_EOL;
		}

		if (count($toDbData))
		{
			$code .= '
	/**
	 * @return array
	 */
	protected function toDbData()
	{
		$dbData = parent::toDbData();
		' . implode(PHP_EOL . '		', $toDbData) . '
		return $dbData;
	}' . PHP_EOL;
		}

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryType($property)
	{
		switch ($property->getComputedType())
		{
			case 'Boolean' :
				return 'bool';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
				return 'int';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'Document' :
			case 'DocumentArray' :
				if ($property->getDocumentType() === null)
				{
					/** @noinspection ClassConstantCanBeUsedInspection */
					return '\Change\Documents\AbstractDocument';
				}
				/** @noinspection NullPointerExceptionInspection */
				return $this->compiler->getModelByName($property->getDocumentType())->getDocumentClassName();
			case 'JSON' :
				return 'array';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryMemberType($property)
	{
		switch ($property->getType())
		{
			case 'Boolean' :
				return 'boolean';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
			case 'Document' :
				return 'integer';
			case 'DocumentArray' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\DocumentArrayProperty';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			case 'JSON' :
				return 'array';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @param string $varName
	 * @return string
	 */
	protected function buildValConverter($property, $varName)
	{
		switch ($property->getType())
		{
			case 'DocumentId';
				return $varName . ' = (int)' . $varName . ';';
			case 'String';
			case 'LongString';
			case 'StorageUri';
				return $varName . ' = ' . $varName . ' === null ? null : (string)' . $varName .';';
			case 'Boolean';
				return $varName . ' = (bool)' . $varName . ';';
			case 'Integer';
				return $varName . ' = ' . $varName . ' === null ? null : (int)' . $varName . ';';
			case 'Float';
			case 'Decimal';
				return $varName . ' = ' . $varName . ' === null ? null : (float)' . $varName . ';';
			case 'Date';
				return '/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		' . $varName . ' =  ' . $varName . ' instanceof \DateTime ? \DateTime::createFromFormat(\'Y-m-d\', ' . $varName
					. '->format(\'Y-m-d\'), new \DateTimeZone(\'UTC\'))->setTime(0, 0) : null;';
			case 'DateTime';
				return $varName . ' = ' . $varName . ' instanceof \DateTime ? ' . $varName . ' : null;';
			default:
				throw new \LogicException('Invalid converter for ' . $property->getType());

		}
	}

	/**
	 * @param string $oldVarName
	 * @param string $newVarName
	 * @param string $type
	 * @return string
	 */
	protected function buildEqualsProperty($oldVarName, $newVarName, $type)
	{
		if ($type === 'Float' || $type === 'Decimal')
		{
			return '\Change\Stdlib\FloatUtils::equals(' . $oldVarName . ', ' . $newVarName . ')';
		}
		if ($type === 'Date' || $type === 'DateTime')
		{
			return $oldVarName . ' == ' . $newVarName;
		}
		return $oldVarName . ' === ' . $newVarName;
	}

	/**
	 * @param string $oldVarName
	 * @param string $newVarName
	 * @param string $type
	 * @return string
	 */
	protected function buildNotEqualsProperty($oldVarName, $newVarName, $type)
	{
		if ($type === 'Float' || $type === 'Decimal')
		{
			return '!\Change\Stdlib\FloatUtils::equals(' . $oldVarName . ', ' . $newVarName . ')';
		}
		if ($type === 'Date' || $type === 'DateTime')
		{
			return $oldVarName . ' != ' . $newVarName;
		}
		return $oldVarName . ' !== ' . $newVarName;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$propertyType = $property->getType();
		$hintType = $propertyType === 'String' ? '' : $ct . ' ';

		$code = '
	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param ' . $ct . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $hintType . $var . ' = null)
	{
		' . $this->buildValConverter($property, $var) . '
		if (' . $this->buildNotEqualsProperty($mn, $var, $propertyType) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;

		$code .= $this->getPropertyExtraGetters($property);
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyStatelessCode($property)
	{
		if (in_array($property->getName(), ['creationDate', 'modificationDate'], true))
		{
			return '';
		}
		$code = [];
		$name = $property->getName();
		$var = '$' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$code[] = '
	/**
	 * @return ' . $ct . '
	 */
	abstract public function get' . $un . '();

	/**
	 * @param ' . $ct . ' ' . $var . '
	 * @return $this
	 */
	abstract public function set' . $un . '(' . $var . ');';

		if ($property->getType() === 'JSON')
		{
			$code[] = '
	/**
	 * @return string|null
	 */
	public function get' . $un . 'String()
	{
		' . $var . ' = $this->get' . $un . '();
		return (' . $var . ' === null) ? null : json_encode(' . $var . ');
	}';
		}
		elseif ($property->getType() === 'Document')
		{
			$code[] = '
	/**
	 * @return integer|null
	 */
	public function get' . $un . 'Id()
	{
		' . $var . ' = $this->get' . $un . '();
		return ' . $var . ' instanceof \Change\Documents\AbstractDocument ? ' . $var . '->getId() : null;
	}';
		}
		elseif ($property->getType() === 'DocumentArray')
		{
			$code[] = '
	/**
	 * @return integer[]
	 */
	public function get' . $un . 'Ids()
	{
		$result = [];
		' . $var . ' = $this->get' . $un . '();
		if (is_array(' . $var . '))
		{
			foreach (' . $var . ' as $o)
			{
				if ($o instanceof \Change\Documents\AbstractDocument) {$result[] = $o->getId();}
			}
		}
		return $result;
	}';
		}

		$code[] = $this->getPropertyExtraGetters($property);
		return implode(PHP_EOL, $code);
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyExtraGetters($property)
	{
		$code = '';
		$name = $property->getName();
		$un = ucfirst($name);
		if ($property->getType() === 'DocumentId')
		{
			if ($property->getDocumentType() === null)
			{
				/** @noinspection ClassConstantCanBeUsedInspection */
				$ct = '\Change\Documents\AbstractDocument';
			}
			else
			{
				/** @noinspection NullPointerExceptionInspection */
				$ct = $this->compiler->getModelByName($property->getDocumentType())->getDocumentClassName();
			}

			$code .= '
	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . 'Instance()
	{
		 $v = $this->getDocumentManager()->getDocumentInstance($this->get' . $un . '());
		 return $v instanceof ' . $ct . ' ? $v : null;
	}' . PHP_EOL;
		}
		elseif ($property->getType() === 'StorageUri')
		{
			$code .= '
	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return \Change\Storage\ItemInfo|null
	 */
	public function get' . $un . 'ItemInfo(\Change\Storage\StorageManager $storageManager)
	{
		$uri = $this->get' . $un . '();
		if ($uri)
		{
			return $storageManager->getItemInfo($uri);
		}
		return null;
	}' . PHP_EOL;
		}
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyJSONAccessors($property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param ' . $ct . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		' . $var . ' = ' . $var . ' && is_array(' . $var . ') ? ' . $var . ' : null;
		if (' . $this->buildNotEqualsProperty($mn, $var, $property->getType()) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyRichTextAccessors($property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$code = '
	/**
	 * @param string|array|' . $ct . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		if (!(' . $mn . '  instanceof ' . $ct . '))
		{
			' . $mn . ' = new ' . $ct . '(null);
		}
		if (!(' . $var . '  instanceof ' . $ct . '))
		{
			' . $var . ' = new ' . $ct . '(' . $var . ');
		}
		if (' . $this->buildNotEqualsProperty($mn . '->toArray()', $var . '->toArray()', $property->getType()) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return ' . $ct . '
	 */
	public function get' . $un . '()
	{
		if (!(' . $mn . '  instanceof ' . $ct . '))
		{
			' . $mn . ' = new ' . $ct . '(null);
		}
		return ' . $mn . ';
	}' . PHP_EOL;

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDocumentAccessors($property)
	{
		$name = $property->getName();
		$mn = '$this->' . $name;
		$var = '$' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);

		$code = '
	/**
	 * @param ' . $ct . '|null ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(' . $ct . ' ' . $var . ' = null)
	{
		if (' . $var . ' && ' . $var . '->getId() <= 0)
		{
			throw new \InvalidArgumentException(\'Argument 1 must be a saved document\', 52005);
		}
		$newId = ' . $var . ' ? ' . $var . '->getId() : 0;
		if (' . $mn . ' !== $newId)
		{
			$this->onPropertyUpdate();
			' . $mn . ' = $newId;
		}
		return $this;
	}

	/**
	 * @return integer
	 */
	public function get' . $un . 'Id()
	{
		return ' . $mn . ';
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		$v = ' . $mn . ' ? $this->getDocumentManager()->getDocumentInstance(' . $mn . ') : null;
		return $v instanceof ' . $ct . ' ? $v : null;
	}' . PHP_EOL;

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDocumentArrayAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$ct = $this->getCommentaryType($property);
		$modelName = $this->escapePHPValue($property->getDocumentType(), false);
		$un = ucfirst($name);
		$code = '
	/**
	 * @param ' . $ct . '[]|iterable ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(iterable ' . $var . ' = [])
	{
		$this->onPropertyUpdate();
		' . $mn . ' = new \Change\Documents\DocumentArrayProperty($this->getDocumentManager(), ' . $modelName . ');
		' . $mn . '->fromArray(' . $var . ');
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentArrayProperty|' . $ct . '[]
	 */
	public function get' . $un . '()
	{
		' . $mn . ' = ' . $mn . ' ?: new \Change\Documents\DocumentArrayProperty($this->getDocumentManager(), ' . $modelName . ');
		return ' . $mn . ';
	}

	/**
	 * @return integer
	 */
	public function get' . $un . 'Count()
	{
		return ' . $mn . ' ? ' . $mn . '->count() : 0;
	}

	/**
	 * @return integer[]
	 */
	public function get' . $un . 'Ids()
	{
		return ' . $mn . ' ? ' . $mn . '->getIds() : [];
	}' . PHP_EOL;

		return $code;
	}
}