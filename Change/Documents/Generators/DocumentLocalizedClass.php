<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Generators;

/**
 * @name \Change\Documents\Generators\DocumentLocalizedClass
 * @api
 */
class DocumentLocalizedClass
{
	/**
	 * @var \Change\Documents\Generators\Compiler
	 */
	protected $compiler;

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @param string $compilationPath
	 * @return boolean
	 */
	public function savePHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model,
		$compilationPath)
	{
		$code = $this->getPHPCode($compiler, $model);
		$nsParts = explode('\\', $model->getNameSpace());
		$nsParts[] = $model->getShortDocumentLocalizedClassName() . '.php';
		array_unshift($nsParts, $compilationPath);
		\Change\Stdlib\FileUtils::write(implode(DIRECTORY_SEPARATOR, $nsParts), $code);
		return true;
	}

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @return string
	 */
	public function getPHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model)
	{
		if (!$model->rootLocalized())
		{
			return null;
		}

		$this->compiler = $compiler;
		$code = '<' . '?php' . PHP_EOL . 'namespace ' . $model->getCompilationNameSpace() . ';' . PHP_EOL;

		$parentModel = $model->getParent();
		/** @noinspection ClassConstantCanBeUsedInspection */
		$extend = ($parentModel !== null) ? $parentModel->getDocumentLocalizedClassName() : '\Change\Documents\AbstractLocalizedDocument';

		$code .= 'class ' . $model->getShortDocumentLocalizedClassName() . ' extends ' . $extend . PHP_EOL;
		$code .= '{' . PHP_EOL;
		$properties = $this->getLocalizedProperties($model);
		if ($properties)
		{
			$code .= $this->getMembers($model, $properties);
			$code .= $this->getMembersAccessors($properties);
		}
		$code .= '}' . PHP_EOL;
		$this->compiler = null;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @return \Change\Documents\Generators\Property[]
	 */
	protected function getLocalizedProperties($model)
	{
		$properties = [];
		foreach ($model->getProperties() as $property)
		{
			/* @var $property \Change\Documents\Generators\Property */
			if ($property->getParent() === null && $property->getLocalized() && $property->getName() !== 'LCID')
			{
				$properties[$property->getName()] = $property;
			}
		}
		return $properties;
	}

	/**
	 * @param mixed $value
	 * @param boolean $removeSpace
	 * @return string
	 */
	protected function escapePHPValue($value, $removeSpace = true)
	{
		if ($removeSpace)
		{
			return str_replace([PHP_EOL, ' ', "\t"], '', var_export($value, true));
		}
		return var_export($value, true);
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembers($model, $properties)
	{
		$modifiedProperties = [];
		$removeOldPropertiesValue = [];
		$clearModifiedProperties = [];

		$toDb = [];
		$fromDb = [];
		$defaultValues = [];

		$code = '';
		foreach ($properties as $property)
		{
			$memberValue = ';';
			$propertyName = $property->getName();
			$mn = '$this->' . $propertyName;
			$constName = var_export($propertyName, true);

			switch ($property->getType())
			{
				case 'RichText':
					$removeOldPropertiesValue[] =
						'case ' . $constName . ': if (' . $mn . ' !== null) {' . $mn . '->setAsDefault();} return;';
					$clearModifiedProperties[] = '$this->removeOldPropertyValue(' . $constName . ');';
					$modifiedProperties[] = 'if (' . $mn . ' !== null && ' . $mn . '->isModified()) {$names[] = ' . $constName . ';}';

					$fromDb[] = $mn . ' = new \Change\Documents\RichtextProperty($data[' . $constName . ']);';
					$toDb[] = '$toDb[' . $constName . '] = (' . $mn . ' !== null) ? ' . $mn . '->toJSONString() : null;';
					break;
				case 'DocumentId':
					$memberValue = ' = 0;';

					$fromDb[] = $mn . ' = (int)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = (int)' . $mn . ';';
					break;
				case 'Boolean':
					$memberValue = ' = false;';
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = $dv  === \'true\';}';

					$fromDb[] = $mn . ' = (bool)$data[' . $constName . '];';
					$toDb[] = '$toDb[' . $constName . '] = (bool)' . $mn . ';';
					break;
				case 'Integer':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (int)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (int)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' !== null ? (int)' . $mn . ' : null;';
					break;
				case 'Float':
				case 'Decimal':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (float)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (float)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' !== null ? (float)' . $mn . ' : null;';
					break;
				case 'DateTime':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = new \DateTime($dv);}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] instanceof \DateTime ? $data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' ? (new \DateTime(' . $mn
						. '->format(\DateTime::ATOM)))->setTimezone(new \DateTimeZone(\'UTC\')) : null;';
					break;
				case 'Date':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (new \DateTime($dv))->setTime(0, 0);}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] instanceof \DateTime ? $data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' ? new \DateTime(' . $mn
						. '->format(\'Y-m-d\T00:00:00\'), new \DateTimeZone(\'UTC\')) : null;';
					break;
				case 'String':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'LongString':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'StorageUri':
					$defaultValues[] = '$dv = $this->documentModel->getProperty(' . $constName . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';

					$fromDb[] = $mn . ' = $data[' . $constName . '] !== null ? (string)$data[' . $constName . '] : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ';';
					break;
				case 'JSON':
					$fromDb[] = $mn . ' = $data[' . $constName . '] ? json_decode($data[' . $constName . '], true) : null;';
					$toDb[] = '$toDb[' . $constName . '] = ' . $mn . ' ? json_encode(' . $mn . ') : null;';
					break;
				default:
					throw new \LogicException('Invalid type ' . $property->getType() . ' on ' . $model->getName() . ' / ' . $propertyName);
			}

			$code .= '
	/**
	 * @var ' . $this->getCommentaryMemberType($property) . '
	 */
	private $' . $property->getName() . $memberValue . PHP_EOL;
		}

		if ($modifiedProperties)
		{
			$code .= '
	/**
	 * @api
	 * @return string[]
	 */
	public function getModifiedPropertyNames()
	{
		$names =  parent::getModifiedPropertyNames();
		' . implode(PHP_EOL . '		', $modifiedProperties) . '
		return $names;
	}' . PHP_EOL;
		}

		if ($removeOldPropertiesValue)
		{
			$code .= '
	/**
	 * @api
	 * @param string $propertyName
	 */
	public function removeOldPropertyValue($propertyName)
	{
		switch ($propertyName)
		{
			' . implode(PHP_EOL . '			', $removeOldPropertiesValue) . '
			default:
				parent::removeOldPropertyValue($propertyName);
		}
	}' . PHP_EOL;
		}

		if ($clearModifiedProperties)
		{
			$code .= '
	protected function clearModifiedProperties()
	{
		parent::clearModifiedProperties();
		' . implode(PHP_EOL . '		', $clearModifiedProperties) . '
	}' . PHP_EOL;
		}

		if ($defaultValues)
		{
			$code .= '
	/**
	 * @return $this
	 */
	public function setDefaultValues()
	{
		parent::setDefaultValues();
		' . implode(PHP_EOL . '		', $defaultValues) . '
		return $this;
	}' . PHP_EOL;
		}

		if ($toDb)
		{
			$code .= '
	/**
	 * @ignore
	 * @return array
	 */
	public function toDb()
	{
		$toDb = parent::toDb();
		' . implode(PHP_EOL . '		', $toDb) . '
		return $toDb;
	}

	/**
	 * @ignore
	 * @param array $data
	 * @return $this
	 */
	public function fromDb(array $data)
	{
		parent::fromDb($data);
		' . implode(PHP_EOL . '		', $fromDb) . '
		return $this;
	}' . PHP_EOL;
		}

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembersAccessors($properties)
	{
		$code = '';
		foreach ($properties as $property)
		{
			switch ($property->getType())
			{
				case 'JSON':
					$code .= $this->getPropertyJSONAccessors($property);
					break;
				case 'RichText':
					$code .= $this->getPropertyRichTextAccessors($property);
					break;
				case 'Boolean':
					$code .= $this->getPropertyBooleanAccessors($property);
					break;
				case 'DocumentId':
				case 'Integer':
					$code .= $this->getPropertyIntegerAccessors($property);
					break;
				case 'Float':
				case 'Decimal':
					$code .= $this->getPropertyFloatAccessors($property);
					break;
				case 'DateTime':
				case 'Date':
					$code .= $this->getPropertyDateTimeAccessors($property);
					break;
				default:
					$code .= $this->getPropertyStringAccessors($property);
					break;
			}
		}
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyBooleanAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return bool|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return bool
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param bool ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(bool ' . $var . ')
	{
		if (' . $mn . ' !== ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . ']' . ' === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyIntegerAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return int|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return int|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param int|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(int ' . $var . ' = null)
	{
		' . ($property->getType() === 'DocumentId' ? $var . ' = (int)' . $var . '; ' : '') . '
		if (' . $var . ' !== ' . $mn . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyFloatAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return float|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return float|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param float|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(float ' . $var . ' = null)
	{
		if (!\Change\Stdlib\FloatUtils::equals(' . $mn . ', ' . $var . '))
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if (\Change\Stdlib\FloatUtils::equals($this->modifiedProperties[' . $en . '], ' . $var . '))
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyDateTimeAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$setConverter = '';
		if ($property->getType() === 'Date')
		{
			$setConverter = 'if (' . $var . ') {' . $var . '->setTime(0, 0);}';
		}
		elseif ($name === 'startPublication' || $name === 'startActivation')
		{
			$setConverter = $var . ' = ' . $var . ' ?? new \DateTime(\Change\Documents\Property::DEFAULT_START_DATE);';
		}
		elseif ($name === 'endPublication' || $name === 'endActivation')
		{
			$setConverter = $var . ' = ' . $var . ' ?? new \DateTime(\Change\Documents\Property::DEFAULT_END_DATE);';
		}

		$code = '
	/**
	 * @return \DateTime|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return \DateTime|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}
	
	/**
	 * @return string|null
	 */
	public function get' . $un . 'AsString()
	{
		return ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format('
			. ($property->getType() === 'Date' ? '\'Y-m-d\'' : '\DateTime::ATOM') . ') : null;
	}

	/**
	 * @param \DateTime|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(\DateTime ' . $var . ' = null)
	{
		' . $setConverter . '
		if (' . $mn . ' != ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				/** @noinspection TypeUnsafeComparisonInspection */
				if ($this->modifiedProperties[' . $en . '] == ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyStringAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @return string|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}

	/**
	 * @return string|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @param string|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		' . $var . ' = ' . $var . ' === null ? null : (string)' . $var . ';
		if (' . $var . ' !== ' . $mn . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyJSONAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$en = $this->escapePHPValue($name);
		$un = ucfirst($name);

		$code = '
	/**
	 * @param array|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(array ' . $var . ' = null)
	{
		' . $var . ' = ' . $var . ' ?: null;
		if (' . $mn . ' !== ' . $var . ')
		{
			if (array_key_exists(' . $en . ', $this->modifiedProperties))
			{
				if ($this->modifiedProperties[' . $en . '] === ' . $var . ')
				{
					unset($this->modifiedProperties[' . $en . ']);
				}
			}
			else
			{
				$this->modifiedProperties[' . $en . '] = ' . $mn . ';
			}
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}

	/**
	 * @return array|null
	 */
	public function get' . $un . 'OldValue()
	{
		return $this->getOldPropertyValue(' . $en . ');
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyRichTextAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$un = ucfirst($name);
		$code = '
	/**
	 * @param string|array|\Change\Documents\RichtextProperty|null ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		if (' . $mn . ' === null)
		{
			' . $mn . ' = new \Change\Documents\RichtextProperty();
		}

		if (is_string(' . $var . '))
		{
			' . $mn . '->fromJSONString(' . $var . ');
		}
		elseif (' . $var . ' === null || is_array(' . $var . '))
		{
			' . $mn . '->fromArray(' . $var . ');
		}
		elseif (' . $var . '  instanceof \Change\Documents\RichtextProperty)
		{
			' . $mn . '->fromRichtextProperty(' . $var . ');
		}
		elseif (' . $var . ' !== null)
		{
			throw new \InvalidArgumentException(\'Argument 1 must be an array, string, \Change\Documents\RichtextProperty or null\', 52005);
		}
		return $this;
	}

	/**
	 * @return \Change\Documents\RichtextProperty
	 */
	public function get' . $un . '()
	{
		if (' . $mn . ' === null)
		{
			' . $mn . ' = new \Change\Documents\RichtextProperty();
		}
		return ' . $mn . ';
	}

	/**
	 * @return \Change\Documents\RichtextProperty
	 */
	public function get' . $un . 'OldValue()
	{
		return new \Change\Documents\RichtextProperty((' . $mn . ' !== null) ? ' . $mn . '->getDefaultJSONString() : null);
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryType($property)
	{
		switch ($property->getComputedType())
		{
			case 'Boolean' :
				return 'boolean';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'JSON' :
				return 'array';
			case 'Integer' :
			case 'DocumentId' :
				return 'integer';
			case 'Date' :
			case 'DateTime' :
				return '\DateTime';
			case 'RichText' :
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryMemberType($property)
	{
		switch ($property->getType())
		{
			case 'Boolean' :
				return 'boolean';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
				return 'integer';
			case 'Date' :
			case 'DateTime' :
				return '\DateTime';
			case 'RichText' :
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}
}