<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Generators;

/**
 * @name \Change\Documents\Generators\InlineLocalizedClass
 * @api
 */
class InlineLocalizedClass
{
	/**
	 * @var \Change\Documents\Generators\Compiler
	 */
	protected $compiler;

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @param string $compilationPath
	 * @return boolean
	 */
	public function savePHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model,
		$compilationPath)
	{
		$code = $this->getPHPCode($compiler, $model);
		$nsParts = explode('\\', $model->getNameSpace());
		$nsParts[] = $model->getShortDocumentLocalizedClassName() . '.php';
		array_unshift($nsParts, $compilationPath);
		\Change\Stdlib\FileUtils::write(implode(DIRECTORY_SEPARATOR, $nsParts), $code);
		return true;
	}

	/**
	 * @param \Change\Documents\Generators\Compiler $compiler
	 * @param \Change\Documents\Generators\Model $model
	 * @return string
	 */
	public function getPHPCode(\Change\Documents\Generators\Compiler $compiler, \Change\Documents\Generators\Model $model)
	{
		if (!$model->rootLocalized())
		{
			return null;
		}

		$this->compiler = $compiler;
		$code = '<' . '?php' . PHP_EOL . 'namespace ' . $model->getCompilationNameSpace() . ';' . PHP_EOL;

		$extend = $model->getParentDocumentLocalizedClassName();
		$code .= 'class ' . $model->getShortDocumentLocalizedClassName() . ' extends ' . $extend . PHP_EOL;
		$code .= '{' . PHP_EOL;
		$properties = $this->getLocalizedProperties($model);
		if (count($properties))
		{
			$code .= $this->getMembers($properties);
			$code .= $this->getMembersAccessors($properties);
		}
		$code .= '}' . PHP_EOL;
		$this->compiler = null;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Model $model
	 * @return \Change\Documents\Generators\Property[]
	 */
	protected function getLocalizedProperties($model)
	{
		$properties = [];
		foreach ($model->getProperties() as $property)
		{
			/* @var $property \Change\Documents\Generators\Property */
			if ($property->getParent() === null && !$property->getStateless()
				&& $property->getLocalized()
				&& $property->getName() !== 'LCID'
			)
			{
				$properties[$property->getName()] = $property;
			}
		}
		return $properties;
	}

	/**
	 * @param mixed $value
	 * @param boolean $removeSpace
	 * @return string
	 */
	protected function escapePHPValue($value, $removeSpace = true)
	{
		if ($removeSpace)
		{
			return str_replace([PHP_EOL, ' ', "\t"], '', var_export($value, true));
		}
		return var_export($value, true);
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @param string $varName
	 * @return string
	 */
	protected function buildValConverter($property, $varName)
	{
		switch ($property->getType())
		{
			case 'DocumentId';
				return $varName . ' = (int)' . $varName . ';';
			case 'String';
			case 'LongString';
			case 'StorageUri';
				return $varName . ' = ' . $varName . ' === null ? null : (string)' . $varName . ';';
			case 'Boolean';
				return $varName . ' = (bool)' . $varName . ';';
			case 'Integer';
				return $varName . ' = ' . $varName . ' === null ? null : (int)' . $varName . ';';
			case 'Float';
			case 'Decimal';
				return $varName . ' = ' . $varName . ' === null ? null : (float)' . $varName . ';';
			case 'Date';
				return '/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		' . $varName . ' =  ' . $varName . ' instanceof \DateTime ? \DateTime::createFromFormat(\'Y-m-d\', ' . $varName
					. '->format(\'Y-m-d\'), new \DateTimeZone(\'UTC\'))->setTime(0, 0) : null;';
			case 'DateTime';
				return $varName . ' = ' . $varName . ' instanceof \DateTime ? ' . $varName . ' : null;';
			default:
				throw new \LogicException('Invalid converter for ' . $property->getType());
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembers($properties)
	{
		$unsetProperties = [];
		$fromDbData = [];
		$toDbData = [];
		$defaultValues = [];

		$code = '';
		foreach ($properties as $property)
		{
			$propertyName = $property->getName();

			$propertyType = $property->getType();
			$memberValue = ';';
			if ($propertyType === 'DocumentId')
			{
				$memberValue = ' = 0;';
			}
			elseif ($propertyType === 'Boolean')
			{
				$memberValue = ' = false;';
			}

			$mn = '$this->' . $propertyName;
			$en = var_export($propertyName, true);

			switch ($propertyType)
			{
				case 'Date':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = '$v = $dbData[' . $en . '] ?? null; ' . $mn
						. ' = $v ? \DateTime::createFromFormat(\'Y-m-d\', $v, new \DateTimeZone(\'UTC\'))->setTime(0, 0) : null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format(\'Y-m-d\') : null;';
					break;
				case 'DateTime':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = '$v = $dbData[' . $en . '] ?? null; ' . $mn . ' = $v ? new \DateTime($v) : null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ' instanceof \DateTime ? ' . $mn . '->format(\DateTime::ATOM) : null;';
					break;
				case 'JSON':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = $mn . ' = $dbData[' . $en . '] ?? null;';
					$toDbData[] = '$dbData[' . $en . '] = (' . $mn . ' && is_array(' . $mn . ')) ? ' . $mn . ' : null;';
					break;
				case 'RichText':
					$unsetProperties[] = '$this->' . $propertyName . ' = null;';
					$fromDbData[] = $mn . ' = new \Change\Documents\RichtextProperty($dbData[' . $en . '] ?? null);';
					$toDbData[] =
						'$dbData[' . $en . '] = ' . $mn . '  instanceof \Change\Documents\RichtextProperty && !' . $mn . '->isEmpty() ? ' . $mn
						. '->toArray() : null;';
					break;
				default:
					$unsetProperties[] = '$this->' . $propertyName . ($memberValue === ';' ? ' = null;' : $memberValue);
					$fromDbData[] = $mn . ' = $dbData[' . $en . '] ?? null;';
					$toDbData[] = '$dbData[' . $en . '] = ' . $mn . ';';
			}

			switch ($propertyType)
			{
				case 'Boolean':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = $dv  === \'true\';}';
					break;
				case 'Integer':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (int)$dv;}';
					break;
				case 'Float':
				case 'Decimal':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (float)$dv;}';
					break;
				case 'DateTime':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = new \DateTime($dv);}';
					break;
				case 'Date':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (new \DateTime($dv))->setTime(0, 0);}';
					break;
				case 'String':
				case 'LongString':
				case 'StorageUri':
					$defaultValues[] = '$dv = $this->getDocumentModel()->getProperty(' . $en . ')->getDefaultValue();';
					$defaultValues[] = 'if ($dv !== null) {' . $mn . ' = (string)$dv;}';
					break;
			}

			$code .= '
	/**
	 * @var ' . $this->getCommentaryMemberType($property) . '
	 */
	private $' . $property->getName() . $memberValue . PHP_EOL;
		}

		$code .= '
	/**
	 * @api
	 */
	public function unsetProperties()
	{
		parent::unsetProperties();
		' . implode(PHP_EOL . '		', $unsetProperties) . '
	}' . PHP_EOL;

		if ($defaultValues)
		{
			$code .= '
	/**
	 * @return $this
	 */
	public function setDefaultValues()
	{
		parent::setDefaultValues();
		' . implode(PHP_EOL . '		', $defaultValues) . '
		return $this;
	}' . PHP_EOL;
		}


		if (count($fromDbData))
		{
			$code .= '
	/**
	 * @api
	 * @param array $dbData
	 */
	public function fromDbData(array $dbData)
	{
		parent::fromDbData($dbData);
		' . implode(PHP_EOL . '		', $fromDbData) . '
	}' . PHP_EOL;
		}

		if (count($toDbData))
		{
			$code .= '
	/**
	 * @return array
	 */
	protected function toDbData()
	{
		$dbData = parent::toDbData();
		' . implode(PHP_EOL . '		', $toDbData) . '
		return $dbData;
	}' . PHP_EOL;
		}

		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property[] $properties
	 * @return string
	 */
	protected function getMembersAccessors($properties)
	{
		$code = '';
		foreach ($properties as $property)
		{
			if ($property->getType() === 'JSON')
			{
				$code .= $this->getJSONPropertyAccessors($property);
			}
			elseif ($property->getType() === 'RichText')
			{
				$code .= $this->getRichTextPropertyAccessors($property);
			}
			else
			{
				$code .= $this->getPropertyAccessors($property);
			}
		}
		return $code;
	}

	/**
	 * @param string $oldVarName
	 * @param string $newVarName
	 * @param string $type
	 * @return string
	 */
	protected function buildEqualsProperty($oldVarName, $newVarName, $type)
	{
		if ($type === 'Float' || $type === 'Decimal')
		{
			return '\Change\Stdlib\FloatUtils::equals(' . $oldVarName . ', ' . $newVarName . ')';
		}
		if ($type === 'Date' || $type === 'DateTime')
		{
			return $oldVarName . ' == ' . $newVarName;
		}
		return $oldVarName . ' === ' . $newVarName;
	}

	/**
	 * @param string $oldVarName
	 * @param string $newVarName
	 * @param string $type
	 * @return string
	 */
	protected function buildNotEqualsProperty($oldVarName, $newVarName, $type)
	{
		if ($type === 'Float' || $type === 'Decimal')
		{
			return '!\Change\Stdlib\FloatUtils::equals(' . $oldVarName . ', ' . $newVarName . ')';
		}
		if ($type === 'Date' || $type === 'DateTime')
		{
			return $oldVarName . ' != ' . $newVarName;
		}
		return $oldVarName . ' !== ' . $newVarName;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getPropertyAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);
		$propertyType = $property->getType();
		$hintType = $propertyType === 'String' ? '' : $ct . ' ';

		$code = '
	/**
	 * @param ' . $ct . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $hintType . $var . ' = null)
	{
		' . $this->buildValConverter($property, $var) . '
		if (' . $this->buildNotEqualsProperty($mn, $var, $property->getType()) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getJSONPropertyAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);
		$code = '
	/**
	 * @param ' . $ct . '|null ' . $var . '
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		' . $var . ' = (is_array(' . $var . ') && count(' . $var . ')) ? ' . $var . ' : null;
		if (' . $this->buildNotEqualsProperty($mn, $var, $property->getType()) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		return ' . $mn . ';
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	protected function getRichTextPropertyAccessors($property)
	{
		$name = $property->getName();
		$var = '$' . $name;
		$mn = '$this->' . $name;
		$ct = $this->getCommentaryType($property);
		$un = ucfirst($name);
		$code = '
	/**
	 * @param string|array|' . $ct . '|null ' . $var . '
	 * @throws \InvalidArgumentException
	 * @return $this
	 */
	public function set' . $un . '(' . $var . ')
	{
		if (!(' . $mn . '  instanceof ' . $ct . '))
		{
			' . $mn . ' = new ' . $ct . '(null);
		}
		if (!(' . $var . '  instanceof ' . $ct . '))
		{
			' . $var . ' = new ' . $ct . '(' . $var . ');
		}
		if (' . $this->buildNotEqualsProperty($mn . '->toArray()', $var . '->toArray()', $property->getType()) . ')
		{
			$this->onPropertyUpdate();
			' . $mn . ' = ' . $var . ';
		}
		return $this;
	}

	/**
	 * @return ' . $ct . '|null
	 */
	public function get' . $un . '()
	{
		if (!(' . $mn . '  instanceof ' . $ct . '))
		{
			' . $mn . ' = new ' . $ct . '(null);
		}
		return ' . $mn . ';
	}' . PHP_EOL;
		return $code;
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryType($property)
	{
		switch ($property->getComputedType())
		{
			case 'Boolean' :
				return 'bool';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'JSON' :
				return 'array';
			case 'Integer' :
			case 'DocumentId' :
				return 'int';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}

	/**
	 * @param \Change\Documents\Generators\Property $property
	 * @return string
	 */
	public function getCommentaryMemberType($property)
	{
		switch ($property->getType())
		{
			case 'Boolean' :
				return 'bool';
			case 'Float' :
			case 'Decimal' :
				return 'float';
			case 'Integer' :
			case 'DocumentId' :
				return 'int';
			case 'JSON' :
				return 'array';
			case 'Date' :
			case 'DateTime' :
			/** @noinspection ClassConstantCanBeUsedInspection */
				return '\DateTime';
			case 'RichText' :
				/** @noinspection ClassConstantCanBeUsedInspection */
				return '\Change\Documents\RichtextProperty';
			default:
				return 'string';
		}
	}
}