<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Generators;

/**
 * @name \Change\Documents\Generators\Model
 */
class Model
{
	/**
	 * @var string
	 */
	protected $vendor;

	/**
	 * @var string
	 */
	protected $shortModuleName;

	/**
	 * @var string
	 */
	protected $shortName;

	/**
	 * @var boolean
	 */
	protected $inline;

	/**
	 * @var boolean
	 */
	protected $abstract;

	/**
	 * @var string
	 */
	protected $treeName;

	/**
	 * @var \Change\Documents\Generators\Model
	 */
	protected $parent;

	/**
	 * @var Property[]
	 */
	protected $properties = [];

	/**
	 * @var InverseProperty[]
	 */
	protected $inverseProperties = [];

	/**
	 * @var string
	 */
	protected $extends;

	/**
	 * @var boolean
	 */
	protected $localized;

	/**
	 * @var boolean
	 */
	protected $publishable;

	/**
	 * @var boolean
	 */
	protected $activable;

	/**
	 * @var boolean
	 */
	protected $editable;

	/**
	 * @var boolean
	 */
	protected $implementCorrection = false;

	/**
	 * @var boolean
	 */
	protected $workflow = true;

	/**
	 * @param string $vendor
	 * @param string $shortModuleName
	 * @param string $shortName
	 */
	public function __construct($vendor, $shortModuleName, $shortName)
	{
		$this->vendor = $vendor;
		$this->shortModuleName = $shortModuleName;
		$this->shortName = $shortName;
	}

	/**
	 * @return string
	 */
	public function getVendor()
	{
		return $this->vendor;
	}

	/**
	 * @return string
	 */
	public function getShortModuleName()
	{
		return $this->shortModuleName;
	}

	/**
	 * @return string
	 */
	public function getShortName()
	{
		return $this->shortName;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->vendor . '_' . $this->shortModuleName . '_' . $this->shortName;
	}

	/**
	 * @param \DOMDocument $domDocument
	 * @param Compiler $compiler
	 * @throws \RuntimeException
	 */
	public function setXmlDocument($domDocument, Compiler $compiler)
	{
		if ($domDocument->documentElement)
		{
			$this->setXmlElement($domDocument->documentElement, $compiler);
		}
		else
		{
			throw new \RuntimeException('Invalid XML document', 54008);
		}
	}

	/**
	 * @param \DOMElement $element
	 * @param Compiler $compiler
	 * @throws \RuntimeException
	 */
	public function setXmlElement($element, Compiler $compiler)
	{
		if ($element)
		{
			$this->importAttributes($element);
			foreach ($element->childNodes as $xmlSectionNode)
			{
				/* @var $xmlSectionNode \DOMElement */
				if ($xmlSectionNode->nodeType === XML_ELEMENT_NODE)
				{
					$localName = $xmlSectionNode->localName;
					if ($localName === 'properties')
					{
						$this->importProperties($xmlSectionNode, $compiler);
					}
					else
					{
						throw new \RuntimeException('Invalid properties node name ' . $this . ' ' . $localName, 54008);
					}
				}
			}
			$this->addStandardProperties();
		}
	}

	/**
	 * @param \DOMElement $xmlElement
	 * @throws \RuntimeException
	 */
	protected function importAttributes($xmlElement)
	{
		if ($xmlElement->localName !== 'document')
		{
			throw new \RuntimeException('Invalid document element name ' . $this, 54009);
		}

		foreach ($xmlElement->attributes as $attribute)
		{
			$name = $attribute->nodeName;
			$value = $attribute->nodeValue;
			$tv = \trim($value);
			if ($tv === '' || $tv !== $value)
			{
				throw new \RuntimeException('Invalid empty attribute value for ' . $this . ' ' . $name, 54010);
			}
			switch ($name)
			{
				case 'abstract':
					$this->abstract = ($value === 'true');
					if ($this->abstract === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'extends':
					$this->extends = $value;
					if (\substr_count($value, '_') + 1 !== 3)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'editable':
					$this->editable = ($value === 'true');
					if ($this->editable === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'publishable':
					$this->publishable = ($value === 'true');
					if ($this->publishable === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'activable':
					$this->activable = ($value === 'true');
					if ($this->activable === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'localized':
					$this->localized = ($value === 'true');
					if ($this->localized === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'tree-name':
					if ($value === 'false')
					{
						$this->treeName = false;
					}
					elseif ($value === 'true')
					{
						$this->treeName = true;
					}
					else
					{
						if (!\preg_match('/^[A-Z][A-Za-z0-9]+_[A-Z][A-Za-z0-9]+$/', $value))
						{
							throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
						}
						$this->treeName = $value;
					}
					break;
				case 'xsi:schemaLocation':
					// just ignore it
					break;
				default:
					throw new \RuntimeException('Invalid attribute name ' . $this . ' ' . $name . ' = ' . $value, 54011);
					break;
			}
		}

		if ($this->localized === false || $this->editable === false || $this->publishable === false || $this->activable === false)
		{
			throw new \RuntimeException('Invalid attribute value true expected', 54012);
		}

		if ($this->publishable && $this->activable)
		{
			throw new \RuntimeException('Property publishable and activable can not be applicable', 54024);
		}

		if (\strlen($this->getName()) > 50)
		{
			throw new \RuntimeException('Invalid document element name ' . $this . ' too long', 54009);
		}

		if ($this->extends && $this->localized)
		{
			throw new \RuntimeException('Invalid localized attribute ' . $this, 54015);
		}
	}

	/**
	 * @throws \RuntimeException
	 */
	public function addStandardProperties()
	{
		if (!$this->extends)
		{
			$property = new Property($this, 'creationDate', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'modificationDate', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}

		if ($this->localized)
		{
			$property = new Property($this, 'refLCID', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'LCID', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}

		if ($this->editable)
		{
			if (!isset($this->properties['label']))
			{
				$property = new Property($this, 'label', 'String');
				$property->applyDefaultProperties();
				$this->properties[$property->getName()] = $property;
			}

			/** @noinspection SuspiciousAssignmentsInspection */
			$property = new Property($this, 'authorId', 'DocumentId');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'documentVersion', 'Integer');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}

		if ($this->publishable)
		{
			if (!isset($this->properties['title']))
			{
				$property = new Property($this, 'title', 'String');
				$property->applyDefaultProperties();
				$this->properties[$property->getName()] = $property;
			}

			if (!isset($this->properties['publicationSections']))
			{
				$property = new Property($this, 'publicationSections', 'DocumentArray');
				$property->setStateless(true);
				$property->applyDefaultProperties();
				$this->properties[$property->getName()] = $property;
			}

			/** @noinspection SuspiciousAssignmentsInspection */
			$property = new Property($this, 'publicationStatus', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'publicationData', 'JSON');
			$property->applyDefaultProperties();
			$property->setInternal(true);
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'startPublication', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'endPublication', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}
		elseif ($this->activable)
		{
			$property = new Property($this, 'active', 'Boolean');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'startActivation', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'endActivation', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}
	}

	/**
	 * @param \DOMElement $element
	 * @param Compiler $compiler
	 * @throws \RuntimeException
	 */
	public function setXmlInlineElement($element, Compiler $compiler)
	{
		if ($element)
		{
			$this->importInlineAttributes($element);
			foreach ($element->childNodes as $xmlSectionNode)
			{
				/* @var $xmlSectionNode \DOMElement */
				if ($xmlSectionNode->nodeType === XML_ELEMENT_NODE)
				{
					$localName = $xmlSectionNode->localName;
					if ($localName && 'properties')
					{
						$this->importProperties($xmlSectionNode, $compiler);
					}
					else
					{
						throw new \RuntimeException('Invalid properties node name ' . $this . ' ' . $localName, 54008);
					}
				}
			}

			$this->addStandardInlineProperties();
		}
	}

	/**
	 * @param \DOMElement $xmlElement
	 * @throws \RuntimeException
	 */
	protected function importInlineAttributes($xmlElement)
	{
		if ($xmlElement->localName !== 'inline')
		{
			throw new \RuntimeException('Invalid document element name ' . $this, 54009);
		}
		$this->inline = true;
		foreach ($xmlElement->attributes as $attribute)
		{
			$name = $attribute->nodeName;
			$value = $attribute->nodeValue;
			$tv = \trim($value);
			if ($tv === '' || $tv !== $value)
			{
				throw new \RuntimeException('Invalid empty attribute value for ' . $this . ' ' . $name, 54010);
			}
			switch ($name)
			{
				case 'name':
					if (!preg_match('/^[A-Z][A-Za-z0-9]+$/', $value))
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					$this->shortName = $value;
					break;
				case 'editable':
					$this->editable = ($value === 'true');
					if ($this->editable === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'activable':
					$this->activable = ($value === 'true');
					if ($this->activable === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'localized':
					$this->localized = ($value === 'true');
					if ($this->localized === false)
					{
						throw new \RuntimeException('Invalid ' . $name . ' attribute value: ' . $value, 54022);
					}
					break;
				case 'xsi:schemaLocation':
					// just ignore it
					break;
				default:
					throw new \RuntimeException('Invalid attribute name ' . $this . ' ' . $name . ' = ' . $value, 54011);
					break;
			}
		}

		if (strlen($this->getName()) > 50)
		{
			throw new \RuntimeException('Invalid document element name ' . $this . ' too long', 54009);
		}
	}

	/**
	 * @throws \RuntimeException
	 */
	public function addStandardInlineProperties()
	{
		if ($this->localized)
		{
			$property = new Property($this, 'refLCID', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'LCID', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}

		if ($this->editable && !isset($this->properties['label']))
		{
			$property = new Property($this, 'label', 'String');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}

		if ($this->activable)
		{
			$property = new Property($this, 'active', 'Boolean');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'startActivation', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;

			$property = new Property($this, 'endActivation', 'DateTime');
			$property->applyDefaultProperties();
			$this->properties[$property->getName()] = $property;
		}
	}

	/**
	 * @param \DOMElement $propertiesElement
	 * @param Compiler $compiler
	 * @throws \RuntimeException
	 */
	protected function importProperties($propertiesElement, Compiler $compiler)
	{
		foreach ($propertiesElement->childNodes as $xmlProperty)
		{
			if ($xmlProperty->nodeType === XML_ELEMENT_NODE)
			{
				if ($xmlProperty->nodeName === 'property')
				{
					$property = new Property($this);
					$property->initialize($xmlProperty, $compiler);
					$this->addProperty($property);
				}
				else
				{
					throw new \RuntimeException('Invalid property node name ' . $this . ' ' . $xmlProperty->nodeName, 54013);
				}
			}
		}
	}

	/**
	 * @param Property $property
	 * @throws \RuntimeException
	 */
	public function addProperty(Property $property)
	{
		if (isset($this->properties[$property->getName()]))
		{
			throw new \RuntimeException('Duplicate property name ' . $this . '::' . $property->getName(), 54014);
		}
		$this->properties[$property->getName()] = $property;
	}

	/**
	 * @throws \Exception
	 */
	public function validateInheritance()
	{
		if ($this->getPublishable() && $this->checkAncestorPublishable())
		{
			throw new \RuntimeException('Duplicate publishable attribute on ' . $this, 54020);
		}

		if ($this->getActivable() && $this->checkAncestorActivable())
		{
			throw new \RuntimeException('Duplicate activable attribute on ' . $this, 54020);
		}

		if ($this->getInline() && $this->getParent() && !$this->getRoot()->getInline())
		{
			throw new \RuntimeException('Invalid inline attribute on ' . $this, 54020);
		}

		if ($this->getTreeName())
		{
			$addProperty = true;
			foreach ($this->getAncestors() as $am)
			{
				/* @var $am \Change\Documents\Generators\Model */
				if ($am->getPropertyByName('treeName'))
				{
					$addProperty = false;
					break;
				}
			}

			if ($addProperty)
			{
				$property = new Property($this, 'treeName', 'String');
				$property->applyDefaultProperties();
				$this->properties[$property->getName()] = $property;
			}
		}

		if ($this->getAbstract() === null && $this->getParent() && $this->getParent()->getAbstract())
		{
			$this->abstract = false;
		}

		foreach ($this->properties as $property)
		{

			/* @var $property Property */
			$property->validateInheritance();
		}
	}

	/**
	 * @return \Change\Documents\Generators\Model
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param \Change\Documents\Generators\Model $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @return Property[]
	 */
	public function getProperties()
	{
		return $this->properties;
	}

	/**
	 * @param string $name
	 * @return Property|null
	 */
	public function getPropertyByName($name)
	{
		return $this->properties[$name] ?? null;
	}

	/**
	 * @return InverseProperty[]
	 */
	public function getInverseProperties()
	{
		return $this->inverseProperties;
	}

	/**
	 * @param string $name
	 * @return InverseProperty
	 */
	public function getInversePropertyByName($name)
	{
		return $this->inverseProperties[$name] ?? null;
	}

	/**
	 * @param InverseProperty $inverseProperty
	 * @return \Change\Documents\Generators\InverseProperty
	 */
	public function addInverseProperty($inverseProperty)
	{
		return $this->inverseProperties[$inverseProperty->getName()] = $inverseProperty;
	}

	/**
	 * @return boolean
	 */
	public function getInline()
	{
		return $this->inline;
	}

	/**
	 * @return boolean
	 */
	public function getAbstract()
	{
		return $this->abstract;
	}

	/**
	 * @return string
	 */
	public function getTreeName()
	{
		return $this->treeName;
	}

	/**
	 * @return string
	 */
	public function getExtends()
	{
		return $this->extends;
	}

	/**
	 * @return boolean
	 */
	public function getLocalized()
	{
		return $this->localized;
	}

	/**
	 * @return boolean
	 */
	public function getPublishable()
	{
		return $this->publishable;
	}

	/**
	 * @return boolean
	 */
	public function getActivable()
	{
		return $this->activable;
	}

	/**
	 * @return boolean
	 */
	public function getEditable()
	{
		return $this->editable;
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->getName();
	}

	/**
	 * @param boolean $localized
	 */
	public function setLocalized($localized)
	{
		$this->localized = (bool)$localized;
	}

	/**
	 * @return \Change\Documents\Generators\Model[]
	 */
	public function getAncestors()
	{
		if ($this->parent)
		{
			$ancestors = $this->parent->getAncestors();
			$ancestors[] = $this->parent;
			return $ancestors;
		}
		return [];
	}

	/**
	 * @return \Change\Documents\Generators\Model
	 */
	public function getRoot()
	{
		return $this->parent ? $this->parent->getRoot() : $this;
	}

	/**
	 * @return boolean
	 */
	public function rootLocalized()
	{
		return $this->getRoot()->getLocalized();
	}

	/**
	 * @return boolean
	 */
	public function checkHasCorrection()
	{
		foreach ($this->properties as $property)
		{
			if ($property->getHasCorrection())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @return boolean
	 */
	public function checkAncestorPublishable()
	{
		foreach ($this->getAncestors() as $model)
		{
			/* @var $model \Change\Documents\Generators\Model */
			if ($model->getPublishable())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @return boolean
	 */
	public function checkAncestorActivable()
	{
		foreach ($this->getAncestors() as $model)
		{
			/* @var $model \Change\Documents\Generators\Model */
			if ($model->getActivable())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @param bool $workflow
	 * @return $this
	 */
	public function setWorkflow($workflow)
	{
		$this->workflow = $workflow;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function checkUseWorkflow()
	{
		return ($this->workflow && ($this->getPublishable() || $this->checkAncestorPublishable()));
	}

	/**
	 * @return string
	 */
	public function getNameSpace()
	{
		return \implode('\\', [$this->getVendor(), $this->getShortModuleName(), 'Documents']);
	}

	/**
	 * @return string
	 */
	public function getCompilationNameSpace()
	{
		return \implode('\\', ['Compilation', $this->getVendor(), $this->getShortModuleName(), 'Documents']);
	}

	/**
	 * @return string
	 */
	public function getShortModelClassName()
	{
		return $this->getShortName() . 'Model';
	}

	/**
	 * @return string
	 */
	public function getModelClassName()
	{
		return '\\' . $this->getCompilationNameSpace() . '\\' . $this->getShortModelClassName();
	}

	/**
	 * @return string
	 */
	public function getShortBaseDocumentClassName()
	{
		return $this->getShortName();
	}

	/**
	 * @return string
	 */
	public function getParentDocumentClassName()
	{
		if ($this->parent)
		{
			return $this->parent->getDocumentClassName();
		}
		if ($this->inline)
		{
			return '\\' . \Change\Documents\AbstractInline::class;
		}
		return '\\' . \Change\Documents\AbstractDocument::class;
	}

	/**
	 * @return string
	 */
	public function getBaseDocumentClassName()
	{
		return '\\' . $this->getCompilationNameSpace() . '\\' . $this->getShortBaseDocumentClassName();
	}

	/**
	 * @return string
	 */
	public function getShortDocumentClassName()
	{
		return $this->getShortName();
	}

	/**
	 * @return string
	 */
	public function getDocumentClassName()
	{
		return '\\' . $this->getNameSpace() . '\\' . $this->getShortDocumentClassName();
	}

	/**
	 * @return string
	 */
	public function getShortDocumentLocalizedClassName()
	{
		return 'Localized' . $this->getShortName();
	}

	/**
	 * @return string
	 */
	public function getDocumentLocalizedClassName()
	{
		return '\\' . $this->getCompilationNameSpace() . '\\' . $this->getShortDocumentLocalizedClassName();
	}

	/**
	 * @return string
	 */
	public function getParentDocumentLocalizedClassName()
	{
		if ($this->parent)
		{
			return $this->parent->getDocumentLocalizedClassName();
		}
		if ($this->inline)
		{
			return '\\' . \Change\Documents\AbstractLocalizedInline::class;
		}
		return '\\' . \Change\Documents\AbstractLocalizedDocument::class;
	}

	/**
	 * @param boolean $implementCorrection
	 * @return boolean
	 */
	public function implementCorrection($implementCorrection = null)
	{
		if (is_bool($implementCorrection))
		{
			$this->implementCorrection = $implementCorrection;
		}
		return $this->implementCorrection;
	}
}