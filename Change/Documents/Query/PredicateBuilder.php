<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Query;

use Change\Db\Query\Expressions\AbstractExpression;
use Change\Db\Query\Expressions\ExpressionList;
use Change\Db\Query\Expressions\Subquery;
use Change\Db\Query\InterfaceSQLFragment;
use Change\Db\Query\Predicates\HasPermission;
use Change\Db\Query\Predicates\In;
use Change\Db\Query\Predicates\InterfacePredicate;
use Change\Db\Query\Predicates\Like;
use Change\Db\Query\Predicates\UnaryPredicate;
use Change\Db\Query\SelectQuery;

use Change\Documents\Property;

/**
 * @name \Change\Documents\Query\PredicateBuilder
 */
class PredicateBuilder // NOSONAR
{
	/**
	 * @var AbstractBuilder
	 */
	protected $builder;

	/**
	 * @param AbstractBuilder $builder
	 */
	function __construct(AbstractBuilder $builder)
	{
		$this->builder = $builder;
	}

	/**
	 * @return \Change\Documents\Query\AbstractBuilder
	 */
	public function getBuilder()
	{
		return $this->builder;
	}

	/**
	 * @return \Change\Db\Query\SQLFragmentBuilder
	 */
	protected function getFragmentBuilder()
	{
		return $this->builder->getFragmentBuilder();
	}

	/**
	 * @param string|Property $propertyName
	 * @return array<InterfaceSQLFragment, Property>
	 * @throws \InvalidArgumentException
	 */
	protected function convertPropertyArgument($propertyName)
	{
		if ($propertyName instanceof InterfaceSQLFragment)
		{
			return [$propertyName, null];
		}
		$lhs = $this->builder->getColumn($propertyName);
		return [$lhs, $this->builder->getValidProperty($propertyName)];
	}

	/**
	 * @param \Change\Documents\Property $property
	 * @param mixed $value
	 * @return InterfaceSQLFragment
	 */
	protected function convertValueArgument(Property $property, $value)
	{
		if ($value instanceof InterfaceSQLFragment)
		{
			return $value;
		}
		return $this->builder->getValueAsParameter($value, $property->getType());
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return array<$lhs, $rhs>
	 * @throws \InvalidArgumentException
	 */
	protected function convertPropertyValueArgument($propertyName, $value)
	{
		[$lhs, $property] = $this->convertPropertyArgument($propertyName);

		if ($value instanceof InterfaceSQLFragment)
		{
			$rhs = $value;
		}
		elseif ($property instanceof Property)
		{
			$rhs = $this->builder->getValueAsParameter($value, $property->getType());
		}
		else
		{
			$rhs = $this->builder->getValueAsParameter($value);
		}
		return [$lhs, $rhs];
	}

	/**
	 * @param Property|string $propertyName
	 * @return \Change\Db\Query\Expressions\Column
	 */
	public function columnProperty($propertyName)
	{
		if ($propertyName instanceof InterfaceSQLFragment)
		{
			return $propertyName;
		}
		return $this->builder->getColumn($propertyName);
	}

	/**
	 * @param InterfacePredicate|InterfacePredicate[] $predicate1
	 * @param InterfacePredicate $_ [optional]
	 * @return InterfacePredicate|\Change\Db\Query\Predicates\Conjunction
	 * @throws \InvalidArgumentException
	 * @api
	 */
	public function logicAnd($predicate1, $_ = null)
	{
		$args = [];
		foreach (func_get_args() as $idx => $arg)
		{
			if (is_array($arg))
			{
				/* @var $conjunction \Change\Db\Query\Predicates\Conjunction */
				$conjunction = call_user_func_array([$this, 'logicAnd'], $arg);
				$args = array_merge($args, $conjunction->getArguments());
			}
			elseif ($arg instanceof InterfaceSQLFragment)
			{
				$args[] = $arg;
			}
			else
			{
				throw new \InvalidArgumentException('Argument ' . ($idx + 1) . ' must be a valid InterfaceSQLFragment', 999999);
			}
		}

		return call_user_func_array([$this->getFragmentBuilder(), 'logicAnd'], $args);
	}

	/**
	 * @param InterfacePredicate|InterfacePredicate[] $predicate1
	 * @param InterfacePredicate $_ [optional]
	 * @return InterfacePredicate|\Change\Db\Query\Predicates\Disjunction
	 * @throws \InvalidArgumentException
	 * @api
	 */
	public function logicOr($predicate1, $_ = null)
	{
		$args = [];
		foreach (func_get_args() as $idx => $arg)
		{
			if (is_array($arg))
			{
				/* @var $disjunction \Change\Db\Query\Predicates\Disjunction */
				$disjunction = call_user_func_array([$this, 'logicOr'], $arg);
				$args = array_merge($args, $disjunction->getArguments());
			}
			elseif ($arg instanceof InterfaceSQLFragment)
			{
				$args[] = $arg;
			}
			else
			{
				throw new \InvalidArgumentException('Argument ' . ($idx + 1) . ' must be a valid InterfaceSQLFragment', 999999);
			}
		}

		return call_user_func_array([$this->getFragmentBuilder(), 'logicOr'], $args);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function eq($propertyName, $value)
	{
		$fb = $this->getFragmentBuilder();
		$property = $this->builder->getValidProperty($propertyName);
		if ($property && $property->getType() === Property::TYPE_DOCUMENTARRAY)
		{
			$abstractBuilder = $this->builder;
			$sq = new \Change\Db\Query\SelectQuery();
			$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
			$fromClause = new \Change\Db\Query\Clauses\FromClause();
			$fromTable = $fb->getDocumentRelationTable($abstractBuilder->getModel()->getRootName());
			$fromClause->setTableExpression($fromTable);
			$sq->setFromClause($fromClause);

			$docEq = $fb->eq($fb->getDocumentColumn('id', $fromTable), $abstractBuilder->getColumn('id'));
			$relnamePredicate = $fb->eq($fb->column('relname', $fromTable), $fb->string($property->getName()));
			$idPredicate = $fb->eq($fb->column('relatedid', $fromTable),
				$abstractBuilder->getValueAsParameter($value, Property::TYPE_INTEGER));

			$and = new \Change\Db\Query\Predicates\Conjunction($docEq, $relnamePredicate, $idPredicate);
			$where = new \Change\Db\Query\Clauses\WhereClause($and);
			$sq->setWhereClause($where);
			return new \Change\Db\Query\Predicates\Exists(new \Change\Db\Query\Expressions\SubQuery($sq));
		}
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $fb->eq($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function neq($propertyName, $value)
	{
		$fb = $this->getFragmentBuilder();
		$property = $this->builder->getValidProperty($propertyName);
		if ($property && ($property->getType() === Property::TYPE_DOCUMENT || $property->getType() === Property::TYPE_DOCUMENTID))
		{
			[$lhsNull, $rhsNull] = $this->convertPropertyValueArgument($propertyName, 0);
			[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
			return new \Change\Db\Query\Predicates\Conjunction($fb->neq($lhsNull, $rhsNull), $fb->neq($lhs, $rhs));
		}
		if ($property && $property->getType() === Property::TYPE_DOCUMENTARRAY)
		{
			$abstractBuilder = $this->builder;
			$sq = new \Change\Db\Query\SelectQuery();
			$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
			$fromClause = new \Change\Db\Query\Clauses\FromClause();
			$fromTable = $fb->getDocumentRelationTable($abstractBuilder->getModel()->getRootName());
			$fromClause->setTableExpression($fromTable);
			$sq->setFromClause($fromClause);

			$docEq = $fb->eq($fb->getDocumentColumn('id', $fromTable), $abstractBuilder->getColumn('id'));
			$relNamePredicate = $fb->eq($fb->column('relname', $fromTable), $fb->string($property->getName()));
			$idPredicate = $fb->eq($fb->column('relatedid', $fromTable),
				$abstractBuilder->getValueAsParameter($value, Property::TYPE_INTEGER));

			$and = new \Change\Db\Query\Predicates\Conjunction($docEq, $relNamePredicate, $idPredicate);
			$where = new \Change\Db\Query\Clauses\WhereClause($and);
			$sq->setWhereClause($where);
			return new \Change\Db\Query\Predicates\Exists(new \Change\Db\Query\Expressions\SubQuery($sq), true);
		}
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $fb->neq($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function gt($propertyName, $value)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->gt($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function lt($propertyName, $value)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->lt($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function gte($propertyName, $value)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->gte($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @return InterfacePredicate
	 * @api
	 */
	public function lte($propertyName, $value)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->lte($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @param integer $matchMode
	 * @param boolean $caseSensitive
	 * @return InterfacePredicate
	 * @api
	 */
	public function like($propertyName, $value, $matchMode = Like::ANYWHERE, bool $caseSensitive = false)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->like($lhs, $rhs, $matchMode, $caseSensitive);
	}

	/**
	 * @param string|Property $propertyName
	 * @param mixed $value
	 * @param integer $matchMode
	 * @param boolean $caseSensitive
	 * @return InterfacePredicate
	 * @api
	 */
	public function notLike($propertyName, $value, $matchMode = Like::ANYWHERE, bool $caseSensitive = false)
	{
		[$lhs, $rhs] = $this->convertPropertyValueArgument($propertyName, $value);
		return $this->getFragmentBuilder()->notLike($lhs, $rhs, $matchMode, $caseSensitive);
	}

	/**
	 * @param string|Property $propertyName
	 * @param string|array|\Change\Db\Query\Expressions\AbstractExpression $rhs1
	 * @return InterfacePredicate
	 * @api
	 */
	public function in($propertyName, $rhs1)
	{
		[$lhs, $property] = $this->convertPropertyArgument($propertyName);
		if ($rhs1 instanceof SelectQuery)
		{
			$rhs = $this->getFragmentBuilder()->subQuery($rhs1);
		}
		elseif ($rhs1 instanceof SubQuery || $rhs1 instanceof ExpressionList)
		{
			$rhs = $rhs1;
		}
		else
		{
			$rhs = new ExpressionList();
			$arguments = func_get_args();
			\array_shift($arguments);
			if ($arguments)
			{
				$builder = $this->builder;
				$converterType = ($property && $property->getType() === Property::TYPE_DOCUMENTARRAY) ? Property::TYPE_INTEGER : $property;
				$converter = static function ($item) use ($builder, $converterType) {
					return ($item instanceof InterfaceSQLFragment) ? $item : $builder->getValueAsParameter($item, $converterType);
				};

				foreach ($arguments as $argument)
				{
					if (\is_array($argument))
					{
						foreach ($argument as $subArgument)
						{
							$rhs->add($converter($subArgument));
						}
					}
					else
					{
						$rhs->add($converter($argument));
					}
				}
			}
		}

		if ($property && $property->getType() === Property::TYPE_DOCUMENTARRAY)
		{
			$fb = $this->getFragmentBuilder();
			$abstractBuilder = $this->builder;

			$sq = new \Change\Db\Query\SelectQuery();
			$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
			$fromClause = new \Change\Db\Query\Clauses\FromClause();
			$fromTable = $fb->getDocumentRelationTable($abstractBuilder->getModel()->getRootName());
			$fromClause->setTableExpression($fromTable);
			$sq->setFromClause($fromClause);

			$docEq = $fb->eq($fb->getDocumentColumn('id', $fromTable), $abstractBuilder->getColumn('id'));
			$relnamePredicate = $fb->eq($fb->column('relname', $fromTable), $fb->string($property->getName()));
			$idPredicate = $fb->in(
				$fb->column('relatedid', $fromTable),
				$rhs
			);

			$and = new \Change\Db\Query\Predicates\Conjunction($docEq, $relnamePredicate, $idPredicate);
			$where = new \Change\Db\Query\Clauses\WhereClause($and);
			$sq->setWhereClause($where);
			return new \Change\Db\Query\Predicates\Exists(new \Change\Db\Query\Expressions\SubQuery($sq));
		}

		return new In($lhs, $rhs);
	}

	/**
	 * @param string|Property $propertyName
	 * @param string | \Change\Db\Query\Expressions\AbstractExpression $rhs
	 * @return InterfacePredicate
	 * @api
	 */
	public function notIn($propertyName, $rhs)
	{
		$pre = \call_user_func_array([$this, 'in'], \func_get_args());
		$pre->setNot(true);
		return $pre;
	}

	/**
	 * @param string|Property $propertyName
	 * @return InterfacePredicate
	 * @api
	 */
	public function isNull($propertyName)
	{
		[$expression, $property] = $this->convertPropertyArgument($propertyName);

		/* @var $property Property */
		if ($property !== null
			&& \in_array($property->getType(), [Property::TYPE_DOCUMENTARRAY, Property::TYPE_DOCUMENT, Property::TYPE_DOCUMENTID], true)
		)
		{
			return $this->getFragmentBuilder()->eq($expression, $this->builder->getValueAsParameter(0, Property::TYPE_INTEGER));
		}
		return new UnaryPredicate($expression, UnaryPredicate::ISNULL);
	}

	/**
	 * @param string|Property $propertyName
	 * @return InterfacePredicate
	 * @api
	 */
	public function isNotNull($propertyName)
	{
		[$expression, $property] = $this->convertPropertyArgument($propertyName);

		/* @var $property Property */
		if ($property !== null
			&& \in_array($property->getType(), [Property::TYPE_DOCUMENTARRAY, Property::TYPE_DOCUMENT, Property::TYPE_DOCUMENTID], true)
		)
		{
			return $this->getFragmentBuilder()->neq($expression, $this->builder->getValueAsParameter(0, Property::TYPE_INTEGER));
		}
		return new UnaryPredicate($expression, UnaryPredicate::ISNOTNULL);
	}

	/**
	 * @param string|Property $propertyName
	 * @param \Change\Documents\AbstractModel|string $model
	 * @param \Change\Documents\Property|string $modelProperty
	 * @param boolean $notExist
	 * @return InterfacePredicate
	 * @throws \InvalidArgumentException
	 * @api
	 */
	protected function buildExists($propertyName, $model, $modelProperty, $notExist)
	{
		[$expression, $property] = $this->convertPropertyArgument($propertyName);

		/* @var $property Property */
		if ($property !== null && $model !== null && $modelProperty !== null)
		{
			$fragmentBuilder = $this->getFragmentBuilder();

			if (!$model instanceof \Change\Documents\AbstractModel)
			{
				$model = $this->getBuilder()->getDocumentManager()->getModelManager()->getModelByName($model);
			}

			if ($model)
			{
				$modelPropertyName = $modelProperty instanceof \Change\Documents\Property ? $modelProperty->getName() : $modelProperty;

				$modelProperty = $model->getProperty($modelPropertyName);

				if ($modelProperty)
				{

					if ($modelProperty->getLocalized())
					{
						$fromTable = $fragmentBuilder->getDocumentI18nTable($model->getRootName());
						$eq = $fragmentBuilder->eq($expression, $fragmentBuilder->getDocumentColumn($modelPropertyName, $fromTable));
					}
					elseif ($modelProperty->getType() === \Change\Documents\Property::TYPE_DOCUMENTARRAY)
					{
						$fromTable = $fragmentBuilder->getDocumentRelationTable($model->getRootName());
						$id = $fragmentBuilder->eq($expression, $fragmentBuilder->getDocumentColumn('relatedid', $fromTable));
						$rel = $fragmentBuilder->eq($fragmentBuilder->string($modelPropertyName),
							$fragmentBuilder->getDocumentColumn('relname', $fromTable));
						$eq = $fragmentBuilder->logicAnd($id, $rel);
					}
					else
					{
						$fromTable = $fragmentBuilder->getDocumentTable($model->getRootName());
						$eq = $fragmentBuilder->eq($expression, $fragmentBuilder->getDocumentColumn($modelPropertyName, $fromTable));
					}

					$sq = new \Change\Db\Query\SelectQuery();
					$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
					$fromClause = new \Change\Db\Query\Clauses\FromClause();
					$fromClause->setTableExpression($fromTable);
					$sq->setFromClause($fromClause);

					$where = new \Change\Db\Query\Clauses\WhereClause($eq);
					$sq->setWhereClause($where);

					if ($notExist)
					{
						return $fragmentBuilder->notExists(new \Change\Db\Query\Expressions\SubQuery($sq));
					}
					return $fragmentBuilder->exists(new \Change\Db\Query\Expressions\SubQuery($sq));
				}
			}
		}
		throw new \InvalidArgumentException('Invalid exists predicate arguments', 999999);
	}

	/**
	 * @param string|Property $propertyName
	 * @param \Change\Documents\AbstractModel|string $model
	 * @param \Change\Documents\Property|string $modelProperty
	 * @return InterfacePredicate
	 * @throws \InvalidArgumentException
	 * @api
	 */
	public function exists($propertyName, $model, $modelProperty)
	{
		return $this->buildExists($propertyName, $model, $modelProperty, false);
	}

	/**
	 * @param string|Property $propertyName
	 * @param \Change\Documents\AbstractModel|string $model
	 * @param \Change\Documents\Property|string $modelProperty
	 * @return InterfacePredicate
	 * @throws \InvalidArgumentException
	 * @api
	 */
	public function notExists($propertyName, $model, $modelProperty)
	{
		return $this->buildExists($propertyName, $model, $modelProperty, true);
	}

	/**
	 * @param \DateTime $at
	 * @param \DateTime $to
	 * @return InterfacePredicate
	 * @throws \RuntimeException
	 * @api
	 */
	public function published($at = null, $to = null)
	{
		if (!$this->builder->getModel()->isPublishable())
		{
			throw new \RuntimeException('Model is not publishable: ' . $this->builder->getModel(), 999999);
		}
		$fb = $this->getFragmentBuilder();

		if (!($at instanceof \DateTime))
		{
			$at = new \DateTime();
		}
		$this->roundDateTime($at);

		if ($to instanceof \DateTime)
		{
			$this->roundDateTime($to);
		}
		else
		{
			$to = $at;
		}

		return $fb->logicAnd(
			$this->eq('publicationStatus', \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE),
			$this->lte('startPublication', $at), $this->gt('endPublication', $to)
		);
	}

	/**
	 * @param \DateTime $at
	 * @param \DateTime $to
	 * @return InterfacePredicate
	 * @throws \RuntimeException
	 * @api
	 */
	public function notPublished($at = null, $to = null)
	{
		if (!$this->builder->getModel()->isPublishable())
		{
			throw new \RuntimeException('Model is not publishable: ' . $this->builder->getModel(), 999999);
		}
		$fb = $this->getFragmentBuilder();
		if (!($at instanceof \DateTime))
		{
			$at = new \DateTime();
		}
		$at->sub(new \DateInterval('PT' . $at->format('s') . 'S'));

		if ($to instanceof \DateTime)
		{
			$to->sub(new \DateInterval('PT' . $to->format('s') . 'S'));
		}
		else
		{
			$to = $at;
		}

		return $fb->logicOr(
			$this->neq('publicationStatus', \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE),
			$this->gt('startPublication', $at), $this->lte('endPublication', $to)
		);
	}

	/**
	 * @param \DateTime $at
	 * @param \DateTime $to
	 * @return InterfacePredicate
	 * @throws \RuntimeException
	 * @api
	 */
	public function activated($at = null, $to = null)
	{
		if (!$this->builder->getModel()->isActivable())
		{
			throw new \RuntimeException('Model is not activable: ' . $this->builder->getModel(), 999999);
		}
		$fb = $this->getFragmentBuilder();

		if (!($at instanceof \DateTime))
		{
			$at = new \DateTime();
		}
		$this->roundDateTime($at);

		if ($to instanceof \DateTime)
		{
			$this->roundDateTime($to);
		}
		else
		{
			$to = $at;
		}

		return $fb->logicAnd(
			$this->eq('active', true),
			$this->lte('startActivation', $at), $this->gt('endActivation', $to)
		);
	}

	/**
	 * @param \DateTime $at
	 * @param \DateTime $to
	 * @return InterfacePredicate
	 * @throws \RuntimeException
	 * @api
	 */
	public function notActivated($at = null, $to = null)
	{
		if (!$this->builder->getModel()->isActivable())
		{
			throw new \RuntimeException('Model is not activable: ' . $this->builder->getModel(), 999999);
		}
		$fb = $this->getFragmentBuilder();
		if (!($at instanceof \DateTime))
		{
			$at = new \DateTime();
		}
		$this->roundDateTime($at);

		if ($to instanceof \DateTime)
		{
			$this->roundDateTime($to);
		}
		else
		{
			$to = $at;
		}

		return $fb->logicOr(
			$this->neq('active', true),
			$this->gt('startActivation', $at), $this->lte('endActivation', $to)
		);
	}

	/**
	 * @param \DateTime $dateTime
	 */
	protected function roundDateTime(\DateTime $dateTime)
	{
		$seconds = (int)$dateTime->format('s');
		if ($seconds)
		{
			$dateTime->add(new \DateInterval('PT' . (60 - $seconds) . 'S'));
		}
	}

	/**
	 * @param AbstractExpression|\Change\User\UserInterface|integer|null $accessor
	 * @param AbstractExpression|string|null $role
	 * @param AbstractExpression|integer|null $resource
	 * @param AbstractExpression|string|null $privilege
	 * @return HasPermission
	 * @api
	 */
	public function hasPermission($accessor = null, $role = null, $resource = null, $privilege = null)
	{
		if ($resource === null)
		{
			$resource = $this->builder->getColumn('id');
		}
		return $this->getFragmentBuilder()->hasPermission($accessor, $role, $resource, $privilege);
	}
}