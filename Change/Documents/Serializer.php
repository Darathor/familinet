<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents;


/**
 * @name \Change\Documents\Serializer
 */
class Serializer
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function __construct(\Change\Documents\DocumentManager $documentManager)
	{
		$this->documentManager = $documentManager;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return string|null
	 */
	public function serialize(\Change\Documents\AbstractDocument $document)
	{
		$document->load();
		$data = $document->toDb();
		$data['id'] = $document->getId();
		$data['model'] = $document->getDocumentModelName();

		if ($document instanceof \Change\Documents\Interfaces\Localizable)
		{
			$data['__LCID'] = [];
			foreach ($document->getLCIDArray() as $LCID)
			{
				$this->documentManager->pushLCID($LCID);
				$localizedDoc = $document->getCurrentLocalization();
				$data['__LCID'][$LCID] = $localizedDoc->toDb();
				/** @noinspection DisconnectedForeachInstructionInspection */
				$this->documentManager->popLCID();
			}
		}
		$data['__relations'] = $document->getDbRelations();
		$data['__attributesValues'] = $document->getCachedAttributesValue();
		$data['__typologyId'] = $document->getCachedTypologyId();
		$data['__pathRules'] = $document->getCachedPathRules();
		return $data ? \serialize($data) : null;
	}

	/**
	 * @param $string
	 * @return \Change\Documents\AbstractDocument|null
	 */
	public function unserialize($string)
	{
		if (!$string || !\is_string($string))
		{
			return null;
		}

		/** @noinspection UnserializeExploitsInspection */
		$data = \unserialize($string);
		$document = null;
		if (isset($data['id'], $data['model']))
		{
			$model = $this->documentManager->getModelManager()->getModelByName($data['model']);
			if (!$model)
			{
				return $document;
			}
			$document = $this->documentManager->getNewDocumentInstanceByModel($model, false);
			$document->initialize((int)$data['id'], \Change\Documents\AbstractDocument::STATE_LOADING);

			$document->setCachedAttributesValue($data['__attributesValues']);
			$document->setCachedTypologyId($data['__typologyId']);
			$document->setCachedPathRules($data['__pathRules']);
			$document->fromDb($data);
			$document->relationsFromDb($data['__relations']);
			$document->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);

			if ($document instanceof \Change\Documents\Interfaces\Localizable)
			{
				/** @noinspection ForeachSourceInspection */
				foreach ($data['__LCID'] as $LCID => $i18nData)
				{
					$this->documentManager->pushLCID($LCID);
					$localizedDoc = $document->getNewLocalizationByLCID($LCID);
					$localizedDoc->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADING);
					$localizedDoc->fromDb($i18nData);
					$localizedDoc->setPersistentState(\Change\Documents\AbstractDocument::STATE_LOADED);
					$this->documentManager->popLCID();
				}
				/** @noinspection PhpUndefinedMethodInspection */
				$document->setLCIDArray(array_keys($data['__LCID']));
			}
		}
		return $document;
	}

}