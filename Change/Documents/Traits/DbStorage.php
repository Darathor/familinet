<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Traits;

use Change\Documents\AbstractDocument;
use Change\Documents\AbstractModel;
use Change\Documents\Events\Event as DocumentEvent;
use Change\Documents\Interfaces\Localizable;
use Change\Documents\PropertiesValidationException;

/**
 * @name \Change\Documents\Traits\DbStorage
 *
 * From \Change\Documents\AbstractDocument
 * @method integer getId()
 * @method initialize($id, $persistentState = null)
 * @method integer getPersistentState()
 * @method integer setPersistentState(integer $persistentState)
 * @method boolean useCorrection(boolean $useCorrection = null)
 * @method \Change\Db\DbProvider getDbProvider()
 * @method \Change\Documents\DocumentManager getDocumentManager()
 * @method \Change\Documents\AbstractModel getDocumentModel()
 * @method string getDocumentModelName()
 * @method \Change\Events\EventManager getEventManager()
 * @method string[] getModifiedPropertyNames()
 * @method boolean hasModifiedProperties()
 * @method array toDb()
 * @method array relationsToDb()
 * @method fromDb(array $data)
 *
 * From \Change\Documents\Traits\Correction
 * @method saveCorrection()
 * @method populateCorrection()
 *
 * From \Change\Documents\Traits\Localized
 * @method deleteAllLocalizedPart()
 *
 * From \Change\Documents\Traits\Publication
 * @method string[] getValidPublicationStatusForCorrection()
 *
 */
trait DbStorage
{
	/**
	 * Load properties
	 * @api
	 * @return $this
	 */
	public function load()
	{
		if ($this->getPersistentState() === AbstractDocument::STATE_INITIALIZED)
		{
			$this->doLoad();

			$event = new DocumentEvent(DocumentEvent::EVENT_LOADED, $this);
			$this->getEventManager()->triggerEvent($event);
		}
		return $this;
	}

	/**
	 * @throws \Exception
	 * @return void
	 */
	protected function doLoad()
	{
		$this->loadDocument();
		$callable = [$this, 'onLoad'];
		if (is_callable($callable))
		{
			$callable();
		}
	}

	protected function loadDocument()
	{
		$this->setPersistentState(AbstractDocument::STATE_LOADING);

		$model = $this->getDocumentModel();

		/** @var \Change\Db\DbProvider $dbp */
		$dbp = $this->getDbProvider();
		$qb = $dbp->getNewQueryBuilder(__METHOD__ . $model->getName());

		$dbTypes = $model->getDbTypes();
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select()
				->from($fb->getDocumentTable($model->getRootName()))
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
			foreach ($dbTypes as $name => $type)
			{
				$qb->addColumn($fb->alias($fb->getDocumentColumn($name), $name));
			}
		}

		$sq = $qb->query();
		$sq->bindParameter('id', $this->getId());
		/** @var array $propertyBag */
		$propertyBag = $sq->getFirstResult(function (array $row) use ($dbTypes, $dbp) {
			foreach ($dbTypes as $name => $type)
			{
				$row[$name] = $dbp->dbToPhp($row[$name], $type);
			}
			return $row;
		});

		if ($propertyBag)
		{
			$this->fromDb($propertyBag);
			$this->setPersistentState(AbstractDocument::STATE_LOADED);
		}
		else
		{
			$this->setPersistentState(AbstractDocument::STATE_DELETED);
		}
	}

	/**
	 * @param string $propertyName
	 * @return integer[]
	 */
	protected function getPropertyDocumentIds($propertyName)
	{
		$model = $this->getDocumentModel();
		$qb = $this->getDbProvider()->getNewQueryBuilder(__METHOD__ . $model->getRootName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->column('relatedid'), 'id'))->from($fb->getDocumentRelationTable($model->getRootName()))
				->where(
					$fb->logicAnd(
						$fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')),
						$fb->eq($fb->column('relname'), $fb->parameter('relname'))
					))
				->orderAsc($fb->column('relorder'));
		}

		$query = $qb->query();
		$query->bindParameter('id', $this->getId());
		$query->bindParameter('relname', $propertyName);
		return $query->getResults($query->getRowsConverter()->addIntCol('id'));
	}

	/**
	 * @return array
	 */
	protected function getPropertiesDocumentIds()
	{
		$model = $this->getDocumentModel();
		$qb = $this->getDbProvider()->getNewQueryBuilder(__METHOD__ . $model->getRootName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->column('relatedid'), 'id'),
				$fb->alias($fb->column('relname'), 'name'))

				->from($fb->getDocumentRelationTable($model->getRootName()))
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')))
				->orderAsc($fb->column('relorder'));
		}
		$query = $qb->query();
		$query->bindParameter('id', $this->getId());
		return $query->getResults($query->getRowsConverter()->addIntCol('id')->addStrCol('name'));
	}

	/**
	 * @return array
	 */
	public function getDbRelations()
	{
		$dbRelations = [];
		$model = $this->getDocumentModel();
		foreach ($model->getProperties() as $property)
		{
			if ($property->getType() === \Change\Documents\Property::TYPE_DOCUMENTARRAY)
			{
				$dbRelations[$property->getName()] = [];
			}
		}
		if ($dbRelations)
		{
			foreach ($this->getPropertiesDocumentIds() as $r)
			{
				$dbRelations[$r['name']][] = $r['id'];
			}
		}
		return $dbRelations;
	}

	/**
	 * @api
	 */
	public function save()
	{
		if ($this->getPersistentState() === AbstractDocument::STATE_NEW)
		{
			$this->create();
		}
		else
		{
			$this->update();
		}
	}

	/**
	 * @api
	 */
	public function create()
	{
		if ($this->getPersistentState() !== AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is not new', 51001);
		}

		$callable = [$this, 'onCreate'];
		if (is_callable($callable))
		{
			$callable();
		}
		$event = new DocumentEvent(DocumentEvent::EVENT_CREATE, $this);
		$this->getEventManager()->triggerEvent($event);

		$propertiesErrors = $event->getParam('propertiesErrors');
		if (is_array($propertiesErrors) && count($propertiesErrors))
		{
			$e = new PropertiesValidationException('Invalid document properties.', 52000);
			$e->setPropertiesErrors($propertiesErrors);
			throw $e;
		}

		$this->assignId();
		$this->doCreate();

		$event->setName(DocumentEvent::EVENT_CREATED);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @throws \Exception
	 * @return void
	 */
	protected function doCreate()
	{
		$this->insertDocument();
		if ($this instanceof Localizable)
		{
			$this->saveCurrentLocalization(true);
		}
	}

	/**
	 * @api
	 * @throws \RuntimeException
	 * @return integer
	 */
	protected function assignId()
	{
		$dbp = $this->getDbProvider();
		$qb = $dbp->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$dt = $fb->getDocumentIndexTable();
		$qb->insert($dt);
		$iq = $qb->insertQuery();

		if ($this->getId() > 0)
		{
			$qb->addColumn($fb->getDocumentColumn('id'));
			$qb->addValue($fb->integerParameter('id'));
			$iq->bindParameter('id', $this->getId());
		}

		$qb->addColumn($fb->getDocumentColumn('model'));
		$qb->addValue($fb->parameter('model'));
		$iq->bindParameter('model', $this->getDocumentModelName());

		$iq->execute();
		if ($this->getId() > 0)
		{
			$id = $this->getId();
		}
		else
		{
			$id = $dbp->getLastInsertId($dt->getName());
			$this->initialize($id);
		}
		return $id;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	protected function insertDocument()
	{
		$id = $this->getId();
		if ($id <= 0)
		{
			throw new \InvalidArgumentException('Invalid Document Id: ' . $id, 51008);
		}
		$this->setPersistentState(AbstractDocument::STATE_SAVING);
		$model = $this->getDocumentModel();
		$typesDb = $model->getDbTypes();

		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__ . $model->getName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->getDocumentTable($model->getRootName()));
			$qb->addColumn($fb->getDocumentColumn('id'))->addValue($fb->integerParameter('id'));
			$qb->addColumn($fb->getDocumentColumn('model'))->addValue($fb->parameter('model'));
			foreach ($typesDb as $name => $dbType)
			{
				$qb->addColumn($fb->getDocumentColumn($name))->addValue($fb->typedParameter($name, $dbType));
			}
		}
		$iq = $qb->insertQuery();
		$iq->bindParameter('id', $id);
		$iq->bindParameter('model', $this->getDocumentModelName());

		$values = $this->toDb();
		foreach ($typesDb as $name => $dbType)
		{
			$iq->bindParameter($name, $values[$name]);
		}

		$iq->execute();
		foreach ($this->relationsToDb() as $name => $ids)
		{
			if ($ids)
			{
				$this->insertRelation($model, $name, $ids);
			}
		}
		$this->setPersistentState(AbstractDocument::STATE_LOADED);
	}

	/**
	 * @param AbstractModel $model
	 * @param string $name
	 * @param integer[] $ids
	 * @throws \RuntimeException
	 */
	protected function insertRelation($model, $name, $ids)
	{
		$idsToSave = [];
		foreach ($ids as $id)
		{
			if ($id <= 0)
			{
				throw new \RuntimeException('Invalid relation document id: ' . $id, 50003);
			}
			$idsToSave[] = $id;
		}

		if ($idsToSave)
		{
			$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__ . $model->getRootName());
			if (!$qb->isCached())
			{
				$fb = $qb->getFragmentBuilder();
				$qb->insert($fb->getDocumentRelationTable($model->getRootName()), $fb->getDocumentColumn('id'), 'relname',
					'relorder', 'relatedid');
				$qb->addValues($fb->integerParameter('id'), $fb->parameter('relname'),
					$fb->integerParameter('order'), $fb->integerParameter('relatedid'));
			}

			$query = $qb->insertQuery();
			$query->bindParameter('id', $this->getId());
			$query->bindParameter('relname', $name);
			foreach ($idsToSave as $order => $relId)
			{
				$query->bindParameter('order', $order);
				$query->bindParameter('relatedid', $relId);
				/** @noinspection DisconnectedForeachInstructionInspection */
				$query->execute();
			}
		}
	}

	/**
	 * @api
	 */
	public function update()
	{
		if ($this->getPersistentState() === AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is new', 51002);
		}

		$callable = [$this, 'onUpdate'];
		if (is_callable($callable))
		{
			$callable();
		}

		$event = new DocumentEvent(DocumentEvent::EVENT_UPDATE, $this);
		$this->getEventManager()->triggerEvent($event);

		$propertiesErrors = $event->getParam('propertiesErrors');
		if (is_array($propertiesErrors) && count($propertiesErrors))
		{
			$e = new PropertiesValidationException('Invalid document properties.', 52000);
			$e->setPropertiesErrors($propertiesErrors);
			throw $e;
		}

		if ($this->useCorrection())
		{
			$this->populateCorrection();
			$modifiedCorrection = $this->saveCorrection();
		}
		else
		{
			$modifiedCorrection = false;
		}

		$modifiedPropertyNames = $this->getModifiedPropertyNames();
		$this->doUpdate($modifiedPropertyNames, $modifiedCorrection);

		$event->setName(DocumentEvent::EVENT_UPDATED);
		$event->setParam('modifiedPropertyNames', $modifiedPropertyNames);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @param string[] $modifiedPropertyNames
	 * @param boolean $modifiedCorrection
	 * @return void
	 */
	protected function doUpdate($modifiedPropertyNames, $modifiedCorrection)
	{
		if ($modifiedCorrection || $modifiedPropertyNames)
		{
			if (!in_array('modificationDate', $modifiedPropertyNames))
			{
				$modifiedPropertyNames[] = 'modificationDate';
			}
			/** @noinspection PhpParamsInspection */
			/** @noinspection NullPointerExceptionInspection */
			$this->getDocumentModel()->getProperty('modificationDate')->setValue($this, new \DateTime());
			if ($this instanceof \Change\Documents\Interfaces\Editable)
			{
				if (!in_array('documentVersion', $modifiedPropertyNames, true))
				{
					$modifiedPropertyNames[] = 'documentVersion';
				}
				$p = $this->getDocumentModel()->getProperty('documentVersion');
				/** @noinspection PhpParamsInspection */
				$p->setValue($this, max(0, $p->getValue($this)) + 1);
			}

			if ($this->getPersistentState() === AbstractDocument::STATE_LOADED)
			{
				$this->updateDocument($modifiedPropertyNames);
			}

			if ($this instanceof Localizable)
			{
				$this->saveCurrentLocalization(false);
			}
		}
	}

	/**
	 * @param string[] $modifiedPropertyNames
	 * @return boolean
	 */
	protected function updateDocument($modifiedPropertyNames)
	{
		$dbp = $this->getDbProvider();
		$this->setPersistentState(AbstractDocument::STATE_SAVING);

		$model = $this->getDocumentModel();
		$typesDb = $model->getDbTypes();

		$qb = $dbp->getNewStatementBuilder(__METHOD__. $model->getName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->getDocumentTable($model->getRootName()));
			foreach ($typesDb as $name => $dbType)
			{
				$qb->assign($fb->getDocumentColumn($name), $fb->typedParameter($name, $dbType));
			}
			$qb->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}
		$values = $this->toDb();
		$uq = $qb->updateQuery();
		foreach ($typesDb as $name => $dbType)
		{
			$uq->bindParameter($name, $values[$name]);
		}
		$uq->bindParameter('id', $this->getId());
		$uq->execute();

		foreach ($this->relationsToDb() as $name => $ids)
		{
			if ($ids !== null && in_array($name, $modifiedPropertyNames, true))
			{
				$this->deleteRelation($model, $name);
				if ($ids)
				{
					$this->insertRelation($model, $name, $ids);
				}
			}
		}

		$this->setPersistentState(AbstractDocument::STATE_LOADED);
		return true;
	}

	/**
	 * @param AbstractModel $model
	 * @param string $name
	 */
	protected function deleteRelation($model, $name)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__ . $model->getRootName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentRelationTable($model->getRootName()));
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')),
					$fb->eq($fb->column('relname'), $fb->parameter('relname'))
				)
			);
		}
		$query = $qb->deleteQuery();
		$query->bindParameter('id', $this->getId());
		$query->bindParameter('relname', $name);
		$query->execute();
	}

	/**
	 * @api
	 */
	public function delete()
	{
		$persistentState = $this->getPersistentState();
		//Already deleted
		if ($persistentState === AbstractDocument::STATE_DELETED || $persistentState === AbstractDocument::STATE_DELETING)
		{
			return;
		}

		if ($persistentState === AbstractDocument::STATE_NEW)
		{
			throw new \RuntimeException('Document is new', 51002);
		}

		$callable = [$this, 'onDelete'];
		if (is_callable($callable))
		{
			$callable();
		}
		$event = new DocumentEvent(DocumentEvent::EVENT_DELETE, $this);
		$this->getEventManager()->triggerEvent($event);

		$this->doDelete();

		$event->setName(DocumentEvent::EVENT_DELETED);
		$this->getEventManager()->triggerEvent($event);
	}

	/**
	 * @throws \Exception
	 */
	protected function doDelete()
	{
		$this->deleteDocument();
		$this->saveDocumentMetas(null);
		if ($this->getDocumentModel()->isLocalized())
		{
			$this->deleteAllLocalizedPart();
		}
	}

	/**
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 * @return integer
	 */
	public function deleteDocument()
	{
		$dbp = $this->getDbProvider();
		if ($this->getPersistentState() !== AbstractDocument::STATE_LOADED)
		{
			throw new \InvalidArgumentException('Invalid Document persistent state: ' . $this->getPersistentState(), 51009);
		}
		$this->setPersistentState(AbstractDocument::STATE_DELETING);

		$model = $this->getDocumentModel();
		$qb = $dbp->getNewStatementBuilder(__METHOD__ . $model->getRootName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentTable($model->getRootName()))
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}

		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $this->getId());
		$rowCount = $dq->execute();

		$qb = $dbp->getNewStatementBuilder(__METHOD__ . 'documentIndex');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentIndexTable())
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}

		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $this->getId());
		$dq->execute();

		$this->setPersistentState(AbstractDocument::STATE_DELETED);
		return $rowCount;
	}

	// Metadata management

	/**
	 * @var array<String,String|String[]>
	 */
	private $metas;

	/**
	 * @var boolean
	 */
	private $modifiedMetas = false;

	/**
	 * @api
	 */
	public function saveMetas()
	{
		if ($this->modifiedMetas)
		{
			$this->saveDocumentMetas($this->metas);
			$this->modifiedMetas = false;
		}
	}

	/**
	 * @param array $metas
	 * @throws \RuntimeException
	 */
	protected function saveDocumentMetas($metas)
	{
		$dbp = $this->getDbProvider();
		$qb = $dbp->getNewStatementBuilder(__METHOD__ . 'Delete');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentMetasTable())
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}

		$deleteQuery = $qb->deleteQuery();
		$deleteQuery->bindParameter('id', $this->getId());
		$deleteQuery->execute();
		if (!$metas || !is_array($metas))
		{
			return;
		}

		$qb = $dbp->getNewStatementBuilder(__METHOD__ . 'Insert');
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->getDocumentMetasTable(), $fb->getDocumentColumn('id'), 'metas', 'lastupdate')
				->addValues($fb->integerParameter('id'), $fb->lobParameter('metas'), $fb->dateTimeParameter('lastupdate'));
		}

		$insertQuery = $qb->insertQuery();
		$insertQuery->bindParameter('id', $this->getId());
		$insertQuery->bindParameter('metas', json_encode($metas));
		$insertQuery->bindParameter('lastupdate', new \DateTime());
		$insertQuery->execute();
	}

	/**
	 * @api
	 */
	public function resetMetas()
	{
		$this->metas = null;
		$this->modifiedMetas = false;
	}

	/**
	 * @return void
	 */
	protected function checkMetasLoaded()
	{
		if ($this->metas === null)
		{
			$this->metas = [];
			$qb = $this->getDbProvider()->getNewQueryBuilder(__METHOD__);
			if (!$qb->isCached())
			{
				$fb = $qb->getFragmentBuilder();
				$qb->select('metas')->from($fb->getDocumentMetasTable())
					->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')))
					->query();
			}
			$query = $qb->query();
			$query->bindParameter('id', $this->getId());
			$row = $query->getFirstResult();
			if ($row !== null && $row['metas'])
			{
				$this->metas = json_decode($row['metas'], true);
			}
			$this->modifiedMetas = false;
		}
	}

	/**
	 * @api
	 * @return boolean
	 */
	public function hasModifiedMetas()
	{
		return $this->modifiedMetas;
	}

	/**
	 * @api
	 * @return array
	 */
	public function getMetas()
	{
		$this->checkMetasLoaded();
		return $this->metas;
	}

	/**
	 * @api
	 * @param array $metas
	 */
	public function setMetas($metas)
	{
		$this->checkMetasLoaded();
		if (count($this->metas))
		{
			$this->metas = [];
			$this->modifiedMetas = true;
		}
		if (is_array($metas))
		{
			foreach ($metas as $name => $value)
			{
				$this->metas[$name] = $value;
			}
			$this->modifiedMetas = true;
		}
	}

	/**
	 * @api
	 * @param string $name
	 * @return boolean
	 */
	public function hasMeta($name)
	{
		$this->checkMetasLoaded();
		return isset($this->metas[$name]);
	}

	/**
	 * @api
	 * @param string $name
	 * @return mixed
	 */
	public function getMeta($name)
	{
		$this->checkMetasLoaded();
		return $this->metas[$name] ?? null;
	}

	/**
	 * @api
	 * @param string $name
	 * @param mixed|null $value
	 */
	public function setMeta($name, $value)
	{
		$this->checkMetasLoaded();
		if ($value === null)
		{
			if (isset($this->metas[$name]))
			{
				unset($this->metas[$name]);
				$this->modifiedMetas = true;
			}
		}
		elseif (!isset($this->metas[$name]) || $this->differentValues($this->metas[$name], $value))
		{
			$this->metas[$name] = $value;
			$this->modifiedMetas = true;
		}
	}

	/**
	 * compare two values, especially if their are two array
	 * because try to compare two arrays with != return something strange
	 * like 0 == 'abcd' return true...
	 * @param $a
	 * @param $b
	 * @return bool
	 */
	protected function differentValues($a, $b)
	{
		/** @noinspection TypeUnsafeComparisonInspection */
		if ($a != $b)
		{
			return true;
		}
		if (is_numeric($a) || is_numeric($b))
		{
			return (string)$a !== (string)$b;
		}
		if (is_array($a) && is_array($b))
		{
			foreach ($a as $key => $value)
			{
				if (!isset($b[$key]) || $this->differentValues($value, $b[$key]))
				{
					return true;
				}
			}
		}
		return false;
	}
}