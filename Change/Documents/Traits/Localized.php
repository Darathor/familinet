<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Documents\Traits;

use Change\Documents\AbstractDocument;
use Change\Documents\Events\Event as DocumentEvent;

/**
 * @name \Change\Documents\Traits\Localized
 *
 * From \Change\Documents\AbstractDocument
 * @method integer getPersistentState()
 * @method integer getId()
 * @method \Change\Documents\DocumentManager getDocumentManager()
 * @method \Change\Documents\AbstractModel getDocumentModel()
 * @method \Change\Db\DbProvider getDbProvider()
 * @method \Change\Events\EventManager getEventManager()
 * @method string[] getModifiedPropertyNames()
 * @method setOldPropertyValue($propertyName, $value)
 */
trait Localized
{
	/**
	 * @var \Change\Documents\AbstractLocalizedDocument[]
	 */
	protected $localizedPartArray = [];

	/**
	 * @var string[]
	 */
	protected $LCIDArray;

	/**
	 * @var string|null
	 */
	protected $currentLCID;

	/**
	 * @api
	 * @param string $val
	 */
	abstract public function setRefLCID($val);

	/**
	 * @api
	 * @return string
	 */
	abstract public function getRefLCID();

	/**
	 * @api
	 * @return string
	 */
	public function getCurrentLCID()
	{
		return $this->getDocumentManager()->getLCID();
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getLCIDArray()
	{
		if ($this->LCIDArray === null)
		{
			if ($this->getId() <= 0)
			{
				$this->LCIDArray = [];
			}
			else
			{
				$model = $this->getDocumentModel();
				$qb = $this->getDbProvider()->getNewQueryBuilder('Localized::getLCIDArray' . $model->getRootName());
				if (!$qb->isCached())
				{
					$fb = $qb->getFragmentBuilder();
					$qb->select($fb->alias($fb->getDocumentColumn('LCID'), 'lc'))
						->from($fb->getDocumentI18nTable($model->getRootName()))
						->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
				}

				$q = $qb->query();
				$q->bindParameter('id', $this->getId());
				$this->LCIDArray = $q->getResults($q->getRowsConverter()->addStrCol('lc'));
			}
		}

		foreach ($this->localizedPartArray as $LCID => $localizedPart)
		{
			if (!in_array($LCID, $this->LCIDArray, true) && $localizedPart->getPersistentState() === AbstractDocument::STATE_LOADED)
			{
				$this->LCIDArray[] = $LCID;
			}
		}
		return $this->LCIDArray;
	}

	/**
	 * @param string[]|null $LCIDArray
	 * @return $this
	 */
	public function setLCIDArray(array $LCIDArray = null)
	{
		$this->LCIDArray = $LCIDArray;
		return $this;
	}

	/**
	 * @param \Change\Documents\AbstractLocalizedDocument $currentLocalizedPart
	 */
	protected function loadCurrentLocalizedPart($currentLocalizedPart)
	{
		$LCID = $currentLocalizedPart->getLCID();
		$model = $this->getDocumentModel();
		$typesDb = $model->getDbLocalizedTypes();

		$currentLocalizedPart->setPersistentState(AbstractDocument::STATE_LOADING);
		$qb = $this->getDbProvider()->getNewQueryBuilder('Localized::loadLocalizedPart' . $model->getName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select()->from($fb->getDocumentI18nTable($model->getRootName()));
			foreach ($typesDb as $name => $dbType)
			{
				$qb->addColumn($fb->alias($fb->getDocumentColumn($name), $name));
			}
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')),
					$fb->eq($fb->getDocumentColumn('LCID'), $fb->parameter('lcid')
					)
				)
			);
		}

		$q = $qb->query();
		$q->bindParameter('id', $this->getId())->bindParameter('lcid', $LCID);

		$dbp = $q->getDbProvider();
		$propertyBag = $q->getFirstResult(function (array $row) use ($typesDb, $dbp) {
			foreach ($typesDb as $name => $type)
			{
				$row[$name] = $dbp->dbToPhp($row[$name], $type);
			}
			return $row;
		});

		if ($propertyBag)
		{
			$currentLocalizedPart->fromDb($propertyBag);
			$currentLocalizedPart->setPersistentState(AbstractDocument::STATE_LOADED);
		}
		elseif ($this->getPersistentState() === AbstractDocument::STATE_DELETED)
		{
			$currentLocalizedPart->setPersistentState(AbstractDocument::STATE_DELETED);
		}
		else
		{
			$currentLocalizedPart->setPersistentState(AbstractDocument::STATE_NEW);
		}
	}

	/**
	 * @return \Change\Documents\AbstractLocalizedDocument[]
	 */
	protected function getLocalizedPartArray()
	{
		return $this->localizedPartArray;
	}

	/**
	 * @api
	 * @param string $LCID
	 * @return boolean
	 */
	public function hasLocalizedPartArray($LCID)
	{
		return isset($this->localizedPartArray[$LCID]);
	}

	/**
	 * @api
	 * @param string $LCID
	 * @return \Change\Documents\AbstractLocalizedDocument
	 */
	public function getNewLocalizationByLCID($LCID)
	{
		$model = $this->getDocumentModel();
		$className = $model->getLocalizedDocumentClassName();
		/* @var $localizedPart \Change\Documents\AbstractLocalizedDocument */
		$localizedPart = new $className($model);
		$localizedPart->initialize($this->getId(), $LCID, AbstractDocument::STATE_NEW);
		$this->localizedPartArray[$LCID] = $localizedPart;
		return $localizedPart;
	}

	/**
	 * @api
	 * @return \Change\Documents\AbstractLocalizedDocument
	 */
	public function getCurrentLocalization()
	{
		$LCID = $this->getCurrentLCID();
		if (!isset($this->localizedPartArray[$LCID]))
		{
			$localizedPart = $this->getNewLocalizationByLCID($LCID);
			if ($this->getPersistentState() !== AbstractDocument::STATE_NEW)
			{
				$this->loadCurrentLocalizedPart($localizedPart);
				$event = new DocumentEvent(DocumentEvent::EVENT_LOCALIZED_LOADED, $this, ['LCID' => $LCID]);
				$this->getEventManager()->triggerEvent($event);
			}

			if ($localizedPart->getPersistentState() === AbstractDocument::STATE_NEW)
			{
				$localizedPart->setDefaultValues();
			}
		}
		else
		{
			$localizedPart = $this->localizedPartArray[$LCID];
		}

		if ($this->currentLCID !== $LCID)
		{
			$this->currentLCID = $LCID;
		}
		return $localizedPart;
	}

	/**
	 * @api
	 * @return \Change\Documents\AbstractLocalizedDocument
	 */
	public function getRefLocalization()
	{
		if ($this->getRefLCID() === null)
		{
			$this->setRefLCID($this->getCurrentLCID());
		}

		$LCID = $this->getRefLCID();
		if (!isset($this->localizedPartArray[$LCID]))
		{
			$localizedPart = $this->getNewLocalizationByLCID($LCID);
			if ($this->getPersistentState() !== AbstractDocument::STATE_NEW)
			{
				$this->loadCurrentLocalizedPart($localizedPart);
			}
			if ($localizedPart->getPersistentState() === AbstractDocument::STATE_NEW)
			{
				$localizedPart->setDefaultValues();
			}
		}
		else
		{
			$localizedPart = $this->localizedPartArray[$LCID];
		}

		if ($this->currentLCID === null)
		{
			$this->currentLCID = $LCID;
		}

		return $localizedPart;
	}

	/**
	 * @api
	 * @throws \RuntimeException if current LCID = refLCID
	 */
	public function deleteCurrentLocalization()
	{
		$localizedPart = $this->getCurrentLocalization();
		if ($localizedPart->getLCID() === $this->getRefLCID())
		{
			throw new \RuntimeException('Unable to delete refLCID: ' . $this->getRefLCID(), 51014);
		}

		if ($localizedPart->getPersistentState() === AbstractDocument::STATE_LOADED)
		{
			$this->deleteLocalizedPart($localizedPart);

			$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_LOCALIZED_DELETED, $this);
			$this->getEventManager()->triggerEvent($event);
		}
	}

	/**
	 * @api
	 * @param boolean $newDocument
	 */
	public function saveCurrentLocalization($newDocument = false)
	{
		$localizedPart = $this->getCurrentLocalization();
		if ($localizedPart->getPersistentState() === AbstractDocument::STATE_NEW)
		{
			$this->insertLocalizedPart($localizedPart);
			if (!$newDocument)
			{
				$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_LOCALIZED_CREATED, $this);
				$this->getEventManager()->triggerEvent($event);
			}
		}
		else
		{
			$this->updateLocalizedPart($localizedPart);
		}
	}

	/**
	 * @param \Change\Documents\AbstractLocalizedDocument $localizedPart
	 */
	public function deleteLocalizedPart(\Change\Documents\AbstractLocalizedDocument $localizedPart)
	{
		$model = $this->getDocumentModel();
		$qb = $this->getDbProvider()
			->getNewStatementBuilder('Localized::deleteLocalizedPart' . $model->getRootName());
		if (!$qb->isCached())
		{

			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentI18nTable($model->getRootName()))
				->where(
					$fb->logicAnd(
						$fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')),
						$fb->eq($fb->getDocumentColumn('LCID'), $fb->parameter('LCID')))
				);
		}

		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $this->getId());
		$dq->bindParameter('LCID', $localizedPart->getLCID());
		$dq->execute();
		$this->unsetLocalizedPart($localizedPart);
	}

	/**
	 * @param \Change\Documents\AbstractLocalizedDocument $localizedPart
	 * @throws \InvalidArgumentException
	 */
	public function insertLocalizedPart(\Change\Documents\AbstractLocalizedDocument $localizedPart)
	{
		if ($localizedPart->getPersistentState() !== AbstractDocument::STATE_NEW)
		{
			throw new \InvalidArgumentException(
				'Invalid I18n Document persistent state: ' . $localizedPart->getPersistentState(), 51010);
		}

		if ($this->getId() <= 0)
		{
			throw new \InvalidArgumentException('Invalid Document Id: ' . $this->getId(), 51008);
		}

		if ($localizedPart->getId() !== $this->getId())
		{
			$localizedPart->initialize($this->getId(), $localizedPart->getLCID());
		}
		$localizedPart->setPersistentState(AbstractDocument::STATE_SAVING);
		$model = $this->getDocumentModel();
		$typesDb = $model->getDbLocalizedTypes();

		$qb = $this->getDbProvider()->getNewStatementBuilder('Localized::insertLocalizedPart'. $model->getName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->getDocumentI18nTable($model->getRootName()));
			$qb->addColumn($fb->getDocumentColumn('id'))->addValue($fb->integerParameter('id'));
			$qb->addColumn($fb->getDocumentColumn('LCID'))->addValue($fb->parameter('LCID'));
			foreach ($typesDb as $name => $dbType)
			{
				$qb->addColumn($fb->getDocumentColumn($name))->addValue($fb->typedParameter($name, $dbType));
			}
		}
		$iq = $qb->insertQuery();
		$iq->bindParameter('id', $localizedPart->getId());
		$iq->bindParameter('LCID', $localizedPart->getLCID());
		$values = $localizedPart->toDb();
		foreach ($typesDb as $name => $dbType)
		{
			$iq->bindParameter($name, $values[$name]);
		}
		$iq->execute();
		$localizedPart->setPersistentState(AbstractDocument::STATE_LOADED);
	}

	/**
	 * @param \Change\Documents\AbstractLocalizedDocument $localizedPart
	 * @throws \InvalidArgumentException
	 */
	public function updateLocalizedPart(\Change\Documents\AbstractLocalizedDocument $localizedPart)
	{
		if ($localizedPart->getPersistentState() !== AbstractDocument::STATE_LOADED)
		{
			throw new \InvalidArgumentException(
				'Invalid I18n Document persistent state: ' . $localizedPart->getPersistentState(), 51010);
		}
		if ($localizedPart->getId() !== $this->getId())
		{
			$localizedPart->initialize($this->getId(), $localizedPart->getLCID());
		}

		$localizedPart->setPersistentState(AbstractDocument::STATE_SAVING);
		$model = $this->getDocumentModel();
		$typesDb = $model->getDbLocalizedTypes();

		$qb = $this->getDbProvider()->getNewStatementBuilder('Localized::updateLocalizedPart'. $model->getName());
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->getDocumentI18nTable($model->getRootName()));
			foreach ($typesDb as $name => $dbType)
			{
				$qb->assign($fb->getDocumentColumn($name), $fb->typedParameter($name, $dbType));
			}
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')),
					$fb->eq($fb->getDocumentColumn('LCID'), $fb->parameter('LCID'))
				)
			);
		}

		$values = $localizedPart->toDb();
		$uq = $qb->updateQuery();

		foreach ($typesDb as $name => $dbType)
		{
			$uq->bindParameter($name, $values[$name]);
		}
		$uq->bindParameter('id', $localizedPart->getId());
		$uq->bindParameter('LCID', $localizedPart->getLCID());
		$uq->execute();

		$localizedPart->setPersistentState(AbstractDocument::STATE_LOADED);
	}

	/**
	 *
	 */
	protected function deleteAllLocalizedPart()
	{
		$model = $this->getDocumentModel();
		$qb = $this->getDbProvider()
			->getNewStatementBuilder('Localized::deleteAllLocalizedPart' . $model->getRootName());
		if (!$qb->isCached())
		{

			$fb = $qb->getFragmentBuilder();
			$qb->delete($fb->getDocumentI18nTable($model->getRootName()))
				->where($fb->eq($fb->getDocumentColumn('id'), $fb->integerParameter('id')));
		}

		$dq = $qb->deleteQuery();
		$dq->bindParameter('id', $this->getId());
		$dq->execute();
		$this->unsetLocalizedPart();
	}

	/**
	 * @param \Change\Documents\AbstractLocalizedDocument|null $localizedPart
	 */
	protected function unsetLocalizedPart(\Change\Documents\AbstractLocalizedDocument $localizedPart = null)
	{
		if ($localizedPart === null)
		{
			/** @noinspection SuspiciousLoopInspection */
			foreach ($this->localizedPartArray as $localizedPart)
			{
				$localizedPart->setPersistentState(AbstractDocument::STATE_DELETED);
			}
			$this->LCIDArray = [];
		}
		else
		{
			$LCID = $localizedPart->getLCID();
			if ($this->localizedPartArray[$LCID] === $localizedPart)
			{
				$localizedPart->setPersistentState(AbstractDocument::STATE_DELETED);
				if ($this->LCIDArray !== null)
				{
					$this->LCIDArray = array_values(array_diff($this->LCIDArray, [$LCID]));
				}
			}
		}
	}

	public function resetCurrentLocalized()
	{
		unset($this->localizedPartArray[$this->getCurrentLCID()]);
	}
}