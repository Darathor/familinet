<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Events;

use Change\Documents\Events\Event as DocumentEvent;
use Zend\EventManager\SharedEventManagerInterface;

/**
 * @name \Change\Events\DefaultSharedListenerAggregate
 */
class DefaultSharedListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param SharedEventManagerInterface $events
	 */
	public function attachShared(SharedEventManagerInterface $events)
	{
		$events->attach('Application', 'setServices', function ($event)
		{
			if ($event instanceof \Change\Events\Event)
			{
				$event->getServices()->set('applicationServices', new \Change\Services\ApplicationServices($event->getApplication()));
			}
			return true;
		}, 9999);

		$callBack = function ($event)
		{
			(new \Change\Documents\Events\ValidateListener())->onValidate($event);
		};
		$events->attach('Documents', DocumentEvent::EVENT_CREATE, $callBack, 5);
		$events->attach('Documents', DocumentEvent::EVENT_UPDATE, $callBack, 5);

		$callBack = function ($event)
		{
			(new \Change\Documents\Events\PublishListener())->onUpdated($event);
		};
		$events->attach('Documents', DocumentEvent::EVENT_UPDATED, $callBack, 5);

		$callBack = function ($event)
		{
			(new \Change\Documents\Events\DeleteListener())->onDelete($event);
			(new \Change\Documents\Attributes\ListenerCallbacks())->onDocumentDelete($event);
		};
		$events->attach('Documents', DocumentEvent::EVENT_DELETE, $callBack, 5);

		$callBack = function ($event)
		{
			(new \Change\Documents\Events\DeleteListener())->onDeleted($event);
		};
		$events->attach('Documents', DocumentEvent::EVENT_DELETED, $callBack, 5);

		$callBack = function ($event)
		{
			(new \Change\Documents\Events\DeleteListener())->onLocalizedDeleted($event);
		};
		$events->attach('Documents', DocumentEvent::EVENT_LOCALIZED_DELETED, $callBack, 5);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				$event->getDocument()->onDefaultUpdateRestResult($event);
			}
		};
		$events->attach('Documents', 'updateRestResult', $callBack, 10);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				$event->getDocument()->onDefaultRouteParamsRestResult($event);
			}
		};
		$events->attach('Documents', 'updateRestResult', $callBack, 5);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				$event->getDocument()->onDefaultCorrectionFiled($event);
			}
		};
		$events->attach('Documents', 'correctionFiled', $callBack, 5);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\Event)
			{
				if ($event->getParam('data') !== null)
				{
					return;
				}

				$document = $event->getDocument();
				if (($document instanceof \Change\Documents\Interfaces\Publishable && !$document->published())
					|| ($document instanceof \Change\Documents\Interfaces\Activable && !$document->activated())
				)
				{
					$event->setParam('data', []);
				}
			}
		};
		$events->attach('Documents', 'getAJAXData', $callBack, 100);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\InlineEvent)
			{
				$event->getDocument()->onDefaultGetRestValue($event);
			}
		};
		$events->attach('Inline', 'getRestValue', $callBack, 5);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\InlineEvent)
			{
				$event->getDocument()->onDefaultProcessRestValue($event);
			}
		};
		$events->attach('Inline', 'processRestValue', $callBack, 5);

		$callBack = function ($event)
		{
			if ($event instanceof \Change\Documents\Events\InlineEvent)
			{
				if ($event->getParam('data') !== null)
				{
					return;
				}

				$document = $event->getDocument();
				if ($document instanceof \Change\Documents\Interfaces\Activable && !$document->activated())
				{
					$event->setParam('data', []);
				}
			}
		};
		$events->attach('Inline', 'getAJAXData', $callBack, 100);

		$callBack = function ($event)
		{
			/** @var $event \Change\Events\Event */
			$predicateJSON = $event->getParam('predicateJSON');
			if (is_array($predicateJSON) && isset($predicateJSON['op']) && ucfirst($predicateJSON['op']) === 'HasCode')
			{
				$predicateBuilder = $event->getParam('predicateBuilder');
				$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
				$hasTag = new \Change\Db\Query\Predicates\HasCode();
				$fragment = $hasTag->populate($predicateJSON, $predicateBuilder, $documentCodeManager);
				if ($fragment)
				{
					$event->setParam('SQLFragment', $fragment);
					$event->stopPropagation();
				}
			}
		};
		$events->attach('Db', 'SQLFragment', $callBack, 5);

		$callback = function ($event)
		{
			/** @var $event \Change\Events\Event */
			$fragment = $event->getParam('fragment');
			if ($fragment instanceof \Change\Db\Query\Predicates\HasCode)
			{
				/** @var $dbProvider \Change\Db\DbProvider */
				$dbProvider = $event->getTarget();
				$event->setParam('sql', $fragment->toSQLString($dbProvider));
				$event->stopPropagation();
			}
		};
		$events->attach('Db', 'SQLFragmentString', $callback, 5);

		$callBack = function ($event)
		{
			/** @var $event \Change\Events\Event */
			$event->getApplication()->getStatistics()->onTransactionBegin($event);
		};
		$events->attach('TransactionManager', 'begin', $callBack, 1);

		$callBack = function ($event)
		{
			/** @var $event \Change\Events\Event */
			$event->getApplication()->getStatistics()->onTransactionCommit($event);
		};
		$events->attach('TransactionManager', 'commit', $callBack, 1);

		$callBack = function ($event)
		{
			/** @var $event \Change\Events\Event */
			$event->getApplication()->getStatistics()->onTransactionRollBack($event);
		};
		$events->attach('TransactionManager', 'rollback', $callBack, 1);
	}
}
