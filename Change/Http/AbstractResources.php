<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http;

/**
 * @name \Change\Http\AbstractResources
 */
abstract class AbstractResources
{
	use \Change\Events\EventsCapableTrait
	{
		\Change\Events\EventsCapableTrait::setApplication as setDefaultApplication;
	}

	public const EVENT_MANAGER_IDENTIFIER = 'UaResources';

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var boolean
	 */
	protected $devMode = false;

	/**
	 * @var string
	 */
	protected $webBaseDirectory;

	/**
	 * @var string
	 */
	protected $assetsVersion;

	/**
	 * @var string
	 */
	protected $assetsBasePath;

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Ua/Events/UaResources');
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->setDefaultApplication($application);
		$configuration = $application->getConfiguration();
		$this->devMode = $configuration->inDevelopmentMode();
		$config = $configuration->getEntry('Change/Install');
		$this->webBaseDirectory = $config['webBaseDirectory'] ?? '';
		$this->assetsVersion = $config['assetsVersion'] ?? '';
		$this->assetsBasePath = '/Assets/' . ($this->assetsVersion ? $this->assetsVersion . '/' : '');
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return \Assetic\AssetManager
	 */
	public function getNewAssetManager()
	{
		return new \Assetic\AssetManager();
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $cssAssets
	 * @param string $pathWithoutExtension
	 */
	protected function addCSSAsset($cssAssets, $pathWithoutExtension)
	{
		$path = $pathWithoutExtension . 'css';
		if (\file_exists($path))
		{
			$cssAssets->add(new \Assetic\Asset\FileAsset($path));
		}
		foreach (['scss', 'less'] as $extension)
		{
			$path = $pathWithoutExtension . $extension;
			if (\file_exists($path))
			{
				$asset = $this->getPreprocessedAsset($path, $extension);
				$cssAssets->add($asset);
			}
		}
	}

	/**
	 * @param string $path
	 * @param string $extension
	 * @return \Assetic\Asset\FileAsset
	 */
	protected function getPreprocessedAsset($path, $extension)
	{
		$asset = new \Assetic\Asset\FileAsset($path);
		$importPaths = $this->getStyleImportPaths($path);

		if ($extension === 'less')
		{
			$cacheDir = $this->getApplication()->getWorkspace()->cachePath('less');
			\Change\Stdlib\FileUtils::mkdir($cacheDir);

			$filter = new \Change\Presentation\Themes\AsseticLessFilter($cacheDir, $importPaths, $this->assetsBasePath . 'Theme/');
			$filter->setFormatter($this->devMode ? 'classic' : 'compressed');
		}
		else
		{
			$filter = new \Change\Presentation\Themes\AsseticScssFilter(null, $importPaths, $this->assetsBasePath . 'Theme/');
			$filter->setFormatter($this->devMode ? 'expanded' : 'crunched');
		}

		$asset->ensureFilter($filter);
		return $asset;
	}

	/**
	 * @param string $path
	 * @return array
	 */
	protected function getStyleImportPaths($path)
	{
		return [\dirname($path)];
	}

	/**
	 * @return string
	 */
	public function getAssetsBasePath()
	{
		return $this->assetsBasePath;
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function write(\Assetic\AssetManager $assetsManager)
	{
		$assetsBaseDirectory = $this->application->getWorkspace()->composeAbsolutePath($this->webBaseDirectory, 'Assets', $this->assetsVersion);
		$writer = new \Assetic\AssetWriter($assetsBaseDirectory);
		$writer->writeManagerAssets($assetsManager);
	}
}