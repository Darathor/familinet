<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Ajax\V1;

use Change\Http\Event;
use Change\Http\Result;
use Zend\Http\Response as HttpResponse;

/**
 * @name \Change\Http\Ajax\V1\Controller
 */
class Controller extends \Change\Http\Controller
{
	/**
	 * @api
	 * @return string
	 */
	public function getApiVersion()
	{
		return 'V1';
	}

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http', 'Http.Ajax', 'Http.Ajax.V1'];
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(Event::EVENT_REQUEST, [$this, 'onDefaultRequest'], 10);
		$eventManager->attach(Event::EVENT_REQUEST, [$this, 'onCacheRequest'], 5);
		$eventManager->attach(Event::EVENT_EXCEPTION, [$this, 'onException'], 5);
		$eventManager->attach(Event::EVENT_RESPONSE, [$this, 'onDefaultJsonResponse'], 5);
		$eventManager->attach(Event::EVENT_RESPONSE, [$this, 'onCacheResponse']);
	}

	public function onDefaultRequest(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$applicationServices->getPermissionsManager()->allow(false);

		/** @var \Change\Http\Ajax\Request $request */
		$request = $event->getRequest();

		$logger = $event->getApplication()->getLogging();
		if (!$request->isPost())
		{
			$logger->debug('Method ' . $request->getMethod() . ' not allowed. URL : ' . $request->getPath());
			$event->setResult($this->notAllowedError($request->getMethod(), ['POST']));
			return;
		}

		$h = $request->getHeaders('X-HTTP-Method-Override');
		if ($h)
		{
			$method = $h->getFieldValue();
			$errorMsg = 'Value ' . $method . ' not allowed for header X-HTTP-Method-Override.';
			switch ($method)
			{
				case \Change\Http\Request::METHOD_GET:
				case \Change\Http\Request::METHOD_PUT:
				case \Change\Http\Request::METHOD_DELETE:
				case \Change\Http\Request::METHOD_POST:
					// Beware: if request is built so that custom HTTP methods are not allowed anymore, this line could trigger an exception.
					$request->setMethod($method);
					break;
				default:
					$logger->debug($errorMsg);
					$event->setResult(new ErrorResult('BAD-REQUEST', $errorMsg, \Zend\Http\Response::STATUS_CODE_400));
					return;
			}
		}
		else
		{
			$errorMsg = 'No header X-HTTP-Method-Override.';
			$logger->debug($errorMsg);
			$event->setResult(new ErrorResult('BAD-REQUEST', 'Bad request', \Zend\Http\Response::STATUS_CODE_400));
			return;
		}

		$i18nManager = $applicationServices->getI18nManager();
		$path = $request->getPath();
		$pathPart = explode('/', $path);
		if (\count($pathPart) > 2)
		{
			$LCID = $pathPart[1];
			if ($i18nManager->isSupportedLCID($LCID))
			{
				$i18nManager->setLCID($LCID);
				$request->setLCID($LCID);
				$actionPath = implode('/', \array_slice($pathPart, 2));
				$method = $request->getMethod();
				$event->setParam('actionPath', $actionPath);
				$event->setParam('method', $method);
				$event->setParam('LCID', $LCID);
				$logger->info($method, $LCID, $actionPath);
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function onCacheRequest(Event $event)
	{
		/** @var \Change\Http\Ajax\Request $request */
		$request = $event->getRequest();
		$method = $request->getMethod();
		if ($method === 'GET' && ($json = $request->getJSON()))
		{
			$applicationServices = $event->getApplicationServices();
			$cacheTTL = (int)($json['cacheTTL'] ?? 0);
			if ($cacheTTL > 5)
			{
				$cacheManager = $applicationServices->getCacheManager();
				if ($cacheManager->isValidNamespace('WebsiteResolver'))
				{
					$cacheKey = 'Ajax-' . md5($method . $event->getParam('LCID') . $event->getParam('actionPath') . serialize($json));
					$cacheValue = $cacheManager->getEntry('WebsiteResolver', $cacheKey, ['ttl' => $cacheTTL]);
					if ($cacheValue !== null)
					{
						$event->getApplication()->getLogging()->debug('GET', $cacheKey, 'from cache');
						$event->setParam('cacheValue', $cacheValue);
					}
					else
					{
						$event->setParam('cacheTTL', $cacheTTL);
						$event->setParam('cacheKey', $cacheKey);
					}
				}
			}
			$applicationServices->getDocumentManager()->usePersistentCache(true);
		}
	}

	/**
	 * @api
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function createResponse()
	{
		$response = parent::createResponse();
		$response->getHeaders()->addHeaderLine('Content-Type: application/json');
		return $response;
	}

	/**
	 * @param Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	protected function getDefaultResponse($event)
	{
		$response = $this->createResponse();
		$response->setStatusCode(HttpResponse::STATUS_CODE_500);
		$content = ['code' => 'ERROR-GENERIC', 'message' => 'Generic error'];
		$response->setContent(json_encode($content));
		return $response;
	}

	/**
	 * @param Event $event
	 */
	public function onException($event)
	{
		/* @var $exception \Exception */
		$exception = $event->getParam('Exception');
		$result = $event->getResult();

		if (!($result instanceof ErrorResult))
		{
			$error = new ErrorResult($exception);
			if ($event->getResult() instanceof Result)
			{
				$result = $event->getResult();
				if ($result->getHttpStatusCode() && $result->getHttpStatusCode() !== HttpResponse::STATUS_CODE_200)
				{
					$error->setHttpStatusCode($result->getHttpStatusCode());
					if ($result->getHttpStatusCode() === HttpResponse::STATUS_CODE_404)
					{
						$error->setErrorCode('PATH-NOT-FOUND');
						$error->setErrorMessage('Unable to resolve path');
						$error->addDataValue('path', $event->getRequest()->getPath());
					}
				}
			}

			$event->setResult($error);
			$event->setResponse(null);
		}
	}

	/**
	 * @param Event $event
	 */
	public function onCacheResponse($event)
	{
		if (($cacheKey = $event->getParam('cacheKey'))
			&& ($cacheTTL = $event->getParam('cacheTTL'))
			&& (($cacheValue = $event->getParam('cacheValue')) !== null))
		{
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$event->getApplication()->getLogging()->debug('SET', $cacheKey, 'to cache');
			$cacheManager->setEntry('WebsiteResolver', $cacheKey, $cacheValue, ['ttl' => $cacheTTL]);
		}
	}

	/**
	 * @param Event $event
	 */
	public function onDefaultJsonResponse($event)
	{
		if ($event->getResponse())
		{
			return;
		}

		$result = $event->getResult();
		if ($result instanceof Result)
		{
			$acceptHeader = $event->getRequest()->getHeader('Accept');
			if ($acceptHeader instanceof \Zend\Http\Header\Accept && $acceptHeader->hasMediaType('application/json'))
			{
				$response = $this->createResponse();
				$response->getHeaders()->addHeaders($result->getHeaders());
				$monitoring = $this->getApplication()->getConfiguration('Change/Http/Ajax/Monitoring');
				if ($monitoring)
				{
					$response->getHeaders()->addHeaderLine('Change-Memory-Usage: ' . number_format(memory_get_usage()));
				}

				$response->setStatusCode($result->getHttpStatusCode());
				$event->setResponse($response);

				if ($this->resultNotModified($event->getRequest(), $result))
				{
					$response->setStatusCode(HttpResponse::STATUS_CODE_304);
				}

				$callable = [$result, 'toArray'];
				if (is_callable($callable))
				{
					$data = call_user_func($callable);
					$response->setContent(json_encode($data));
				}
				elseif ($result->getHttpStatusCode() === HttpResponse::STATUS_CODE_404)
				{
					$error = new ErrorResult('PATH-NOT-FOUND', 'Unable to resolve path', HttpResponse::STATUS_CODE_404);
					$error->addDataValue('path', $event->getRequest()->getPath());
					$response->setContent(json_encode($error->toArray()));
				}
			}
		}
	}
}