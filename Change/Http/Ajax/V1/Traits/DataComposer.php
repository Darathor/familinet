<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Ajax\V1\Traits;

/**
 * @name \Change\Http\Ajax\V1\Traits\DataComposer
 */
trait DataComposer
{
	/**
	 * @var array
	 */
	protected $dataSetNames;

	/**
	 * @var array
	 */
	protected $visualFormats;

	/**
	 * @var array
	 */
	protected $URLFormats;

	/**
	 * @var boolean
	 */
	protected $detailed;

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @var boolean
	 */
	protected $showEmptyAttributes;

	/**
	 * @var \Rbs\Website\Documents\Website
	 */
	protected $website;

	/**
	 * @var \Change\Http\Web\UrlManager
	 */
	protected $websiteUrlManager;

	/**
	 * @var \Rbs\Website\Documents\Section
	 */
	protected $section;

	/**
	 * @var \Rbs\Website\Documents\Page
	 */
	protected $page;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Presentation\RichText\RichTextManager
	 */
	protected $richTextManager;

	/**
	 * @var \Change\Collection\CollectionManager
	 */
	protected $collectionManager;

	/**
	 * @var integer
	 */
	protected $level;

	/**
	 * @var \Change\Documents\AbstractDocument|null
	 */
	protected $referrer;

	/**
	 * @var null|array
	 */
	protected $dataSets;

	/**
	 * @param array $context
	 * @return $this
	 */
	protected function setContext(array $context)
	{
		// Set default context values.
		$context += ['visualFormats' => [], 'URLFormats' => [], 'dataSetNames' => [], 'data' => [], 'website' => null,
			'websiteUrlManager' => null, 'section' => null, 'page' => null, 'detailed' => false, 'level' => 0,
			'referrer' => null];

		$this->visualFormats = $context['visualFormats'];
		$this->URLFormats = $context['URLFormats'];
		$this->dataSetNames = $context['dataSetNames'];
		$this->detailed = $context['detailed'];
		$this->website = $context['website'];
		$this->websiteUrlManager = $context['websiteUrlManager'];
		$this->section = $context['section'];
		$this->page = $context['page'];
		$this->level = $context['level'];
		$this->referrer = $context['referrer'];
		$this->data = $context['data'];
		$this->showEmptyAttributes = (isset($this->data['showEmptyAttributes']) && $this->data['showEmptyAttributes'] === true);

		return $this;
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	protected function setServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->documentManager = $applicationServices->getDocumentManager();
		$this->i18nManager = $applicationServices->getI18nManager();
		$this->richTextManager = $applicationServices->getRichTextManager();
		$this->collectionManager = $applicationServices->getCollectionManager();
		return $this;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return mixed|null
	 */
	protected function getPublishedData($document)
	{
		$publishedData = new \Change\Http\Ajax\V1\PublishedData($document);
		$common = $publishedData->getCommonData();
		if (count($common))
		{
			$section = $this->section ?: $this->website;
			$common['URL'] = $publishedData->getURLData($this->URLFormats, $section);
			return $common;
		}
		return null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 */
	protected function generateTypologyDataSet(\Change\Documents\AbstractDocument $document)
	{
		if ($this->shouldRenderTypology())
		{
			$typology = $this->getTypologyData($document);
			if ($typology)
			{
				$this->dataSets['typology'] = $typology;
			}
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument|null $referrer
	 * @return array
	 */
	protected function getAttributeContext(\Change\Documents\AbstractDocument $referrer = null)
	{
		return ['visualFormats' => $this->visualFormats, 'URLFormats' => $this->URLFormats, 'dataSetNames' => $this->dataSetNames,
			'data' => $this->data, 'website' => $this->website, 'websiteUrlManager' => $this->websiteUrlManager,
			'section' => $this->section, 'page' => $this->page, 'detailed' => false, 'level' => $this->level + 1,
			'referrer' => $referrer];
	}

	/**
	 * @return boolean
	 */
	protected function shouldRenderTypology()
	{
		return $this->level <= 1;
	}

	/**
	 * Result format:
	 *    [
	 *        'groups' => [
	 *            'group1' => ['title' => ...]
	 *        ],
	 *        'attributes' => [
	 *            'toto' => [...],
	 *            '45646' => [...]
	 *        ],
	 *        'visibilities' => [
	 *            'list' => [
	 *                [
	 *                    'key' => 'group1',
	 *                    'items' => [
	 *                        ['key' => '45646']
	 *                    ]
	 *                ]
	 *            ]
	 *        ]
	 *    ]
	 *
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function getTypologyData(\Change\Documents\AbstractDocument $document)
	{
		$typology = $this->documentManager->getTypologyByDocument($document);
		if (!$typology)
		{
			return [];
		}

		$typologyData = [
			'id' => $typology->getId(),
			'name' => $typology->getName(),
			'title' => $typology->getTitle(),
			'groups' => [],
			'attributes' => [],
			'contexts' => [],
			'visibilities' => []
		];

		foreach ($typology->getContexts() as $context)
		{
			if ($context->getExposedInAjaxAPI())
			{
				$typologyData['contexts'][$context->getName()] = ['title' => $context->getTitle()];
			}
		}

		$attributeContext = $this->getAttributeContext();
		$attributeValues = $this->documentManager->getAttributeValues($document);
		foreach ($typology->getGroups() as $group)
		{
			$groupVisibilities = [];
			foreach ($group->getAttributes() as $attribute)
			{
				$contextNames = [];
				$name = $attribute->getName();
				foreach ($typology->getVisibilities() as $contextName => $context)
				{
					if (!isset($typologyData['contexts'][$contextName], $context['attributes'][$name]))
					{
						continue;
					}
					if ($context['attributes'][$name])
					{
						$contextNames[] = $contextName;
					}
				}

				// Show only attributes having at least one visibility context.
				// If we are not in detail mode, ignore all attributes that are not visible in 'list' context.
				if (!count($contextNames) || (!$this->detailed && !in_array('list', $contextNames)))
				{
					continue;
				}

				$value = $this->getAttributeValue($attribute, $attributeValues, $attributeContext);

				// Ignore attributes with null value?
				if (!$this->showEmptyAttributes)
				{
					if ($value !== null)
					{
						foreach ($contextNames as $contextName)
						{
							$groupVisibilities[$contextName][] = ['key' => $name];
						}
					}
					else
					{
						continue;
					}
				}

				$typologyData['attributes'][$name] = [
					'title' => $attribute->getTitle(),
					'type' => $attribute->getType(),
					'renderingMode' => $attribute->getRenderingMode(),
					'value' => $value
				];

				$formattedDescription = $this->formatRichText($attribute->getDescription());
				if ($formattedDescription)
				{
					$typologyData['attributes'][$name]['description'] = $formattedDescription;
				}

				$formatter = $attribute->getAJAXFormatter();
				if (is_callable($formatter))
				{
					$skin = $formatter($value, $attributeContext);
					if ($skin)
					{
						$typologyData['attributes'][$name]['formattedValue'] = $skin;
						// Deprecated since 1.1.0, use formattedValue.
						$typologyData['attributes'][$name]['valueTitle'] = $skin['common']['title'];
					}
				}
			}

			if (!$this->detailed && (!isset($groupVisibilities['list']) || !count($groupVisibilities['list'])))
			{
				continue;
			}

			$typologyData['groups'][$group->getName()] = [
				'title' => $group->getTitle()
			];

			foreach ($groupVisibilities as $contextName => $items)
			{
				if (($this->detailed || $contextName == 'list') && is_array($items) && count($items))
				{
					$typologyData['visibilities'][$contextName][] = ['key' => $group->getName(), 'items' => $items];
				}
			}
		}

		return $typologyData;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Attribute $attribute
	 * @param \Change\Documents\Attributes\AttributeValues|null $attributeValues
	 * @param array $context
	 * @return mixed
	 */
	protected function getAttributeValue($attribute, $attributeValues, $context)
	{
		$value = $attributeValues->get($attribute->getName());

		switch ($attribute->getType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
				$value = $this->documentManager->getDocumentInstance($value);
				if ($value)
				{
					$data = $value->getAJAXData($context);
					return $data ?: null;
				}
				return null;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				if (is_array($value) && count($value))
				{
					$values = [];
					foreach ($value as $id)
					{
						$v = $this->documentManager->getDocumentInstance($id);
						if ($v)
						{
							$data = $v->getAJAXData($context);
							if ($data)
							{
								$values[] = $data;
							}
						}
					}
					return $values ?: null;
				}
				return null;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT:
				if (is_array($value))
				{
					$value = new \Change\Documents\RichtextProperty($value);
				}

				if ($value instanceof \Change\Documents\RichtextProperty && !$value->isEmpty())
				{
					return $this->formatRichText($value);
				}
				return null;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				if (is_array($value) && isset($value['date'], $value['timezone']))
				{
					$value = new \DateTime($value['date'], $value['timezone']);
				}
				elseif (is_string($value))
				{
					$value = new \DateTime($value);
				}

				if ($value instanceof \DateTime)
				{
					return $this->formatDate($value);
				}
				return null;
		}
		return $value;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array|mixed|null
	 */
	protected function getDocumentData($document)
	{
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			if ($document->published())
			{
				/* @var \Change\Documents\AbstractDocument $document */
				return $this->getPublishedData($document);
			}
		}
		elseif ($document instanceof \Change\Documents\Interfaces\Activable)
		{
			if ($document->activated())
			{
				/* @var \Change\Documents\AbstractDocument $document */
				return ['common' => ['id' => $document->getId()]];
			}
		}
		else
		{
			/* @var \Change\Documents\AbstractDocument $document */
			return ['common' => ['id' => $document->getId()]];
		}
		return null;
	}

	/**
	 * @param \Change\Documents\RichtextProperty $richTextProperty
	 * @return null|string
	 */
	protected function formatRichText($richTextProperty)
	{
		if ($this->website && $richTextProperty instanceof \Change\Documents\RichtextProperty)
		{
			return $this->richTextManager->render($richTextProperty, 'Website',
				['website' => $this->website]);
		}
		return null;
	}

	/**
	 * @param \DateTime $dateTime
	 * @return null|string
	 */
	protected function formatDate($dateTime)
	{
		if ($dateTime instanceof \DateTime)
		{
			return $dateTime->format(\DateTime::ATOM);
		}
		return null;
	}

	/**
	 * @param string $dataSetName
	 * @return boolean
	 */
	protected function hasDataSet($dataSetName)
	{
		if ($dataSetName && $this->dataSetNames)
		{
			return array_key_exists($dataSetName, $this->dataSetNames);
		}
		return false;
	}

	/**
	 * This method should fill $this->dataSet.
	 */
	protected abstract function generateDataSets();

	/**
	 * @return array
	 */
	public function toArray()
	{
		if ($this->dataSets === null)
		{
			$this->generateDataSets();
		}
		return $this->dataSets;
	}
} 