<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Actions;

/**
 * @name \Change\Http\Rest\V1\Actions\DocumentFilters
 */
class DocumentFilters
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();

		if ($request->isGet())
		{
			$this->getFiltersList($event);
		}
		elseif ($request->isPost())
		{
			$this->createFilter($event);
		}
		elseif ($request->isPut())
		{
			$this->updateFilter($event);
		}
		elseif ($request->isDelete())
		{
			$this->deleteFilter($event);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getFiltersList(\Change\Http\Event $event)
	{
		$modelName = $event->getRequest()->getQuery('model');
		if ($modelName)
		{
			$filters = [];
			foreach ($event->getApplicationServices()->getDbProvider()->getFilterStorage()->getByContext($modelName) as $filter)
			{
				$filters[] = $this->filterToArray($filter);
			}

			$result = new \Change\Http\Rest\V1\ArrayResult();
			$result->setArray($filters);
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		else
		{
			$result = new \Change\Http\Rest\V1\ErrorResult(999999, 'invalid model name');
		}

		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function createFilter(\Change\Http\Event $event)
	{
		$params = $event->getRequest()->getPost();

		$modelName = $params->get('model_name');
		$content = $params->get('content');
		$label = $params->get('label');

		if ($modelName && $content && $label)
		{
			$result = new \Change\Http\Rest\V1\ArrayResult();

			$applicationServices = $event->getApplicationServices();
			$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();
			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();

				$filter = new \Change\Db\Filters\Filter($modelName, $userId, $content, $label);
				$applicationServices->getDbProvider()->getFilterStorage()->insert($filter);

				$transactionManager->commit();

				$result->setArray($this->filterToArray($filter));
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}

			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		else
		{
			$result = new \Change\Http\Rest\V1\ErrorResult(999999, 'invalid model name');
		}

		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function updateFilter(\Change\Http\Event $event)
	{
		$params = $event->getRequest()->getPost();

		$modelName = $params->get('model_name');
		$content = $params->get('content');
		$id = $params->get('filter_id');
		$label = $params->get('label');

		if ($id && $modelName && $content && $label)
		{
			$result = new \Change\Http\Rest\V1\ArrayResult();
			$applicationServices = $event->getApplicationServices();

			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();

				$storage = $applicationServices->getDbProvider()->getFilterStorage();
				$filter = $storage->load($id);
				$filter->setContent($content);
				$filter->setLabel($label);
				$storage->update($filter);

				$transactionManager->commit();

				$result->setArray($this->filterToArray($filter));
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}

			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		else
		{
			$result = new \Change\Http\Rest\V1\ErrorResult(999999, 'invalid model name and/or filter id');
		}

		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Exception
	 */
	public function deleteFilter(\Change\Http\Event $event)
	{
		$id = $event->getRequest()->getQuery('filter_id');

		if ($id)
		{
			$applicationServices = $event->getApplicationServices();

			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();

				$applicationServices->getDbProvider()->getFilterStorage()->delete($id);

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}

			$result = new \Change\Http\Rest\V1\ArrayResult();
			$result->setArray([]);
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		else
		{
			$result = new \Change\Http\Rest\V1\ErrorResult(999999, 'invalid filter id');
		}

		$event->setResult($result);
	}

	/**
	 * @param \Change\Db\Filters\Filter $filter
	 * @return array
	 */
	protected function filterToArray(\Change\Db\Filters\Filter $filter)
	{
		return [
			'filter_id' => $filter->getId(),
			'user_id' => $filter->getUserId(),
			'label' => $filter->getLabel(),
			'content' => $filter->getContent(),
			'model_name' => $filter->getContextName()
		];
	}
}