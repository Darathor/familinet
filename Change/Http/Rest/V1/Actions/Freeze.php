<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Actions;

/**
 * @name \Change\Http\Rest\V1\Actions\Freeze
 */
class Freeze extends \Change\Http\Rest\V1\Actions\Publish
{
	/**
	 * @return boolean
	 */
	protected function getNewStatus()
	{
		return \Change\Documents\Interfaces\Publishable::STATUS_FROZEN;
	}

	/**
	 * @param $urlManager
	 * @return \Change\Http\Rest\V1\Link
	 */
	protected function getSwitchLink($urlManager)
	{
		return new \Change\Http\Rest\V1\Link($urlManager, 'actions/publish', 'switchStatus');
	}
}