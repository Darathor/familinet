<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Actions;

/**
 * @name \Change\Http\Rest\V1\Actions\MergeCorrection
 */
class MergeCorrection
{
	/**
	 * Request Query Params: documentId, correctionId, LCID
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$queryParameters = $event->getRequest()->getQuery()->toArray();
		$documentId = $queryParameters['documentId'] ?? 0;
		$correctionId = $queryParameters['correctionId'] ?? 0;
		$LCID = $queryParameters['LCID'] ?? null;
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$document = $documentManager->getDocumentInstance($documentId);

		if (!$correctionId || !($document instanceof \Change\Documents\Interfaces\Correction))
		{
			return;
		}

		if ($document instanceof \Change\Documents\Interfaces\Publishable && $document->useWorkflow())
		{
			return;
		}

		if ($LCID)
		{
			$documentManager->pushLCID($LCID);
		}

		$correction = $document->getCurrentCorrection();
		if ($correction && $correction->getId() == $correctionId)
		{
			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();
				$applyCorrectionWhen = $queryParameters['applyCorrectionWhen'] ?? 'now';
				$plannedCorrectionDate = $queryParameters['plannedCorrectionDate'] ?? null;
				if ($plannedCorrectionDate && $applyCorrectionWhen === 'planned')
				{
					$startDate = new \DateTime($plannedCorrectionDate);
					$applicationServices->getJobManager()->createNewJob('Change_Document_MergeCorrection', $queryParameters, $startDate);
					$correction->setStatus(\Change\Documents\Correction::STATUS_PUBLISHABLE);
					$correction->updateStatus();
				}
				elseif (!$document->mergeCurrentCorrection())
				{
					throw new \RuntimeException('Unable to merge correction :' . $correction, 999999);
				}

				$result = new \Change\Http\Rest\V1\ArrayResult();
				$result->setArray([
					'documentId' => $document->getId(),
					'modelName' => $document->getDocumentModelName(),
					'LCID' => $LCID,
					'publicationStatus' => $document->getDocumentModel()->getPropertyValue($document, 'publicationStatus'),
					'correctionId' => $correctionId,
					'status' => $correction->getStatus(),
				]);
				$event->setResult($result);

				$event->setParam('documentId', $document->getId());
				$event->setParam('modelName', $document->getDocumentModelName());
				if ($LCID)
				{
					$event->setParam('LCID', $LCID);
					(new \Change\Http\Rest\V1\Resources\GetLocalizedDocument())->execute($event);
				}
				else
				{
					(new \Change\Http\Rest\V1\Resources\GetDocument())->execute($event);
				}
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				if ($LCID)
				{
					$documentManager->popLCID();
				}
				throw $transactionManager->rollBack($e);
			}
		}

		if ($LCID)
		{
			$documentManager->popLCID();
		}
	}
}