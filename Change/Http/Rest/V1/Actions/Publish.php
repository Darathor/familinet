<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Actions;

/**
 * @name \Change\Http\Rest\V1\Actions\Publish
 */
class Publish
{
	/**
	 * Use Required Event Params: documentId
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	public function execute($event)
	{
		$queryParameters = $event->getRequest()->getQuery()->toArray();
		$LCID = $queryParameters['LCID'] ?? null;
		$documentId = $queryParameters['documentId'] ?? 0;
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($documentId);

		if ($document instanceof \Change\Documents\Interfaces\Publishable && !$document->useWorkflow())
		{
			if ($document instanceof \Change\Documents\Interfaces\Localizable)
			{
				$LCID = $LCID ?? $document->getRefLCID();
				try
				{
					$documentManager->pushLCID($LCID);
					$this->updateStatus($event, $document, $this->getNewStatus(), $LCID);
					$documentManager->popLCID();
				}
				catch (\Exception $e)
				{
					$documentManager->popLCID($e);
				}
			}
			else
			{
				$this->updateStatus($event, $document, $this->getNewStatus());
			}
		}
	}

	/**
	 * @return boolean
	 */
	protected function getNewStatus()
	{
		return \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE;
	}

	/**
	 * @param $urlManager
	 * @return \Change\Http\Rest\V1\Link
	 */
	protected function getSwitchLink($urlManager)
	{
		return new \Change\Http\Rest\V1\Link($urlManager, 'actions/freeze', 'switchStatus');
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument $document
	 * @param boolean $newStatus
	 * @param string $LCID
	 * @throws \Exception
	 */
	protected function updateStatus($event, $document, $newStatus, $LCID = null)
	{
		$transactionManager = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$document->updatePublicationStatus($newStatus);
			$event->setParam('documentId', $document->getId());
			$event->setParam('modelName', $document->getDocumentModelName());

			$result = new \Change\Http\Rest\V1\ArrayResult();
			$result->setArray([
				'documentId' => $document->getId(),
				'modelName' => $document->getDocumentModelName(),
				'LCID' => $LCID,
				'publicationStatus' => $document->getDocumentModel()->getPropertyValue($document, 'publicationStatus')
			]);
			$event->setResult($result);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}


} 