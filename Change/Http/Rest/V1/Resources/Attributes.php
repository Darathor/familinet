<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Resources;

/**
 * @name \Change\Http\Rest\V1\Resources\Attributes
 */
class Attributes
{
	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array $properties
	 * @throws \Exception
	 */
	public function update($event, $document, $properties)
	{
		$data = $properties['typology$'] ?? null;
		if (is_array($data))
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$typology = null;
			if (isset($data['__id']) && $data['__id'] > 0)
			{
				$typology = $documentManager->getTypology($data['__id']);
			}
			unset($data['__id']);

			if (!($typology instanceof \Change\Documents\Attributes\Interfaces\Typology))
			{
				$documentManager->saveAttributeValues($document, null, null);
			}
			else
			{
				$values = new \Change\Documents\Attributes\AttributeValues($documentManager->getLCID(), $data);
				$documentManager->saveAttributeValues($document, $typology, $values);
			}
		}
	}
}