<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Resources;

/**
 * @name \Change\Http\Rest\V1\Resources\GetDocumentCollection
 */
class GetDocumentCollection
{
	/**
	 * Use Event Params: documentId, modelName, LCID
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		$modelName = $event->getParam('modelName');
		$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
		if (!$model)
		{
			throw new \RuntimeException('Invalid Parameter: modelName', 71000);
		}
		$this->generateResult($event, $model);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function executeFiltered($event)
	{
		$modelName = $event->getParam('modelName');
		$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
		if (!$model)
		{
			throw new \RuntimeException('Invalid Parameter: modelName', 71000);
		}
		$this->generateResult($event, $model);
	}

	/**
	 * @param \Change\Http\Rest\V1\CollectionResult $result
	 * @return array
	 */
	protected function buildQueryArray($result)
	{
		$array = ['limit' => $result->getLimit(), 'offset' => $result->getOffset()];
		if ($result->getSort())
		{
			$array['sort'] = $result->getSort();
			$array['desc'] = $result->getDesc() ? 'true' : 'false';
		}
		return $array;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractModel $model
	 * @return \Change\Http\Rest\V1\CollectionResult
	 */
	protected function generateResult($event, $model)
	{
		$urlManager = $event->getUrlManager();
		$request = $event->getRequest();
		$params = \array_merge($request->getQuery()->toArray(), $request->getPost()->toArray());
		$result = $this->getNewResult($params);

		$selfLink = new \Change\Http\Rest\V1\Link($urlManager, $event->getRequest()->getPath());
		$selfLink->setQuery($this->buildQueryArray($result));
		$result->addLink($selfLink);

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$docQuery = $documentManager->getNewQuery($model);
		if (isset($params['filter']) && \is_array($params['filter']))
		{
			$event->getApplicationServices()->getModelManager()->applyDocumentFilter($docQuery, $params['filter']);
		}
		if ($event->getParam('hasPermission'))
		{
			$predicateBuilder = $docQuery->getPredicateBuilder();
			$perm = $predicateBuilder->hasPermission($event->getAuthenticationManager()->getCurrentUser(), 'Consumer', null, $event->getParam('modelName'));
			$docQuery->andPredicates($perm);
		}

		$result->setCount($docQuery->getCountDocuments());

		if ($result->getOffset())
		{
			$prevLink = clone $selfLink;
			$prevOffset = \max(0, $result->getOffset() - $result->getLimit());
			$query = $this->buildQueryArray($result);
			$query['offset'] = $prevOffset;
			$prevLink->setQuery($query);
			$prevLink->setRel('prev');
			$result->addLink($prevLink);
		}

		if ($result->getCount())
		{
			if ($result->getCount() > $result->getOffset() + $result->getLimit())
			{
				$nextLink = clone $selfLink;
				$nextOffset = \min($result->getOffset() + $result->getLimit(), $result->getCount() - 1);
				$query = $this->buildQueryArray($result);
				$query['offset'] = $nextOffset;
				$nextLink->setQuery($query);
				$nextLink->setRel('next');
				$result->addLink($nextLink);
			}

			$sortInfo = \explode('.', $result->getSort());

			$childBuilder = null;
			$property = $model->getProperty(\array_shift($sortInfo));
			if ($property && !$property->getStateless())
			{
				$sortProperty = null;
				if ($property->getType() === \Change\Documents\Property::TYPE_DOCUMENT)
				{
					$childBuilder = $docQuery->getPropertyBuilder($property);
					$childBuilder->setJoinType(\Change\Db\Query\Expressions\Join::LEFT_OUTER_JOIN);
					$sortModel = $childBuilder->getModel();
					$sortPropertyName = \array_shift($sortInfo);
					if ($sortModel && $sortModel->hasProperty($sortPropertyName))
					{
						$sortProperty = $sortModel->getProperty($sortPropertyName);
						if ($sortProperty->getStateless())
						{
							$sortProperty = null;
						}
					}
				}
				else
				{
					$sortProperty = $property;
				}

				if ($sortProperty)
				{
					$docQuery->addOrder($sortProperty->getName(), !$result->getDesc(), $childBuilder);
					$docQuery->addOrder('id', !$result->getDesc(), $childBuilder); // For duplicates, use the id with the same sort direction.
				}
			}


			$qb = $docQuery->dbQueryBuilder();

			$fb = $qb->getFragmentBuilder();
			$qb->addColumn($fb->alias($fb->getDocumentColumn('id', $docQuery->getTableAliasName()), 'id'))
				->addColumn($fb->alias($fb->getDocumentColumn('model', $docQuery->getTableAliasName()), 'model'));

			$extraColumn = $request->getPost('column', $request->getQuery('column', []));
			$sc = $qb->query();
			$sc->setMaxResults($result->getLimit());
			$sc->setStartIndex($result->getOffset());
			$collection = new \Change\Documents\DocumentCollection($documentManager, $sc->getResults());
			$collection->preLoad();
			$documents = $collection->toArray();

			foreach ($documents as $document)
			{
				$result->addResource(new DocumentLink($urlManager, $document, DocumentLink::MODE_LIST, $extraColumn));
			}
		}

		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
		return $result;
	}

	/**
	 * @param array $params
	 * @return \Change\Http\Rest\V1\CollectionResult
	 */
	protected function getNewResult($params)
	{
		$result = new \Change\Http\Rest\V1\CollectionResult();
		if (isset($params['offset']) && (($offset = (int)$params['offset']) >= 0))
		{
			$result->setOffset($offset);
		}
		if (isset($params['limit']) && (($limit = (int)$params['limit']) > 0))
		{
			$result->setLimit($limit);
		}

		if (isset($params['sort']))
		{
			$result->setSort($params['sort']);
		}

		if (isset($params['desc']))
		{
			$result->setDesc($params['desc']);
			return $result;
		}
		return $result;
	}
}
