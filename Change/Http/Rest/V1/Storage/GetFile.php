<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Storage;

/**
 * @name \Change\Http\Rest\V1\Storage\GetFile
 */
class GetFile
{
	/**
	 * Use Required Event Params: path
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		$content = $event->getRequest()->getQuery('content');
		$fileResponse = false;
		$temporaryDownload = $event->getParam('temporaryDownload');
		if ($temporaryDownload)
		{
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$options = ['ttl' => 120];
			if ($cacheManager->hasEntry('temporaryDownload', $temporaryDownload, $options))
			{
				$storageURI = $cacheManager->getEntry('temporaryDownload', $temporaryDownload);
				$event->setParam('path', $storageURI);
				$event->getController()->getEventManager()
					->attach(\Change\Http\Event::EVENT_RESPONSE, [$this, 'onDownloadContent'], 10);
				$content = 0;
				$fileResponse = true;
			}
		}

		$storagePath = $event->getParam('path');
		if (!$storagePath)
		{
			//Document Not Found
			return;
		}

		$storageManager = $event->getApplicationServices()->getStorageManager();
		$itemInfo = $storageManager->getItemInfo($storagePath);
		if ($itemInfo)
		{
			$infos = parse_url($storagePath);
			if (!isset($infos['path']))
			{
				$infos['path'] = '/';
			}

			$findPath = [DIRECTORY_SEPARATOR, $infos['scheme'] . '://'];
			$replacePath = ['/', 'storage/'];

			$path = 'storage/' . $infos['host'] . $infos['path'];
			if ($itemInfo->isDir())
			{
				if (substr($storagePath, -1) !== '/')
				{
					$storagePath .= '/';
				}
				$result = new \Change\Http\Rest\V1\CollectionResult();
				$result->addLink(new \Change\Http\Rest\V1\Link($event->getUrlManager(), $path));
				$dirHandle = opendir($storagePath);
				$childNames = [];
				while (($name = readdir($dirHandle)) !== false)
				{
					if ($name !== '.' && $name !== '..')
					{
						$childNames[] = $name;
					}
				}
				closedir($dirHandle);
				$count = 0;
				foreach ($childNames as $name)
				{
					$childPath = $storagePath . $name;
					$fileInfo = $storageManager->getItemInfo($childPath);
					if ($fileInfo)
					{
						$count++;
						$restPath = str_replace($findPath, $replacePath, $fileInfo->getPathname());
						if ($fileInfo->isDir())
						{
							$restPath .= '/';
						}
						$link = new \Change\Http\Rest\V1\Link($event->getUrlManager(), $restPath);
						$res = ['name' => $fileInfo->getFilename(), 'link' => $link->toArray()];
						$result->addResource($res);
					}
				}
				$result->setCount($count);
			}
			else
			{
				$temporaryDownload = null;
				if ($itemInfo->isReadable())
				{
					$mTime =  \DateTime::createFromFormat('U', $itemInfo->getMTime());
					$size = $itemInfo->getSize();
				}
				else
				{
					$mTime = null;
					$size = -1;
					$temporaryDownload = $itemInfo->getPublicURL();
				}

				$link = new \Change\Http\Rest\V1\Link($event->getUrlManager(), $path);
				$hrefContent = $event->getUrlManager()->getByPathInfo($path)->setQuery(['content' => 1])->normalize()->toString();
				$result = new \Change\Http\Rest\V1\ArrayResult();
				$vc = null;
				if ($content)
				{
					if ($mTime)
					{
						$result->setHeaderLastModified($mTime);
					}
					$event->setParam('itemInfo', $itemInfo);
					$event->getController()->getEventManager()
						->attach(\Change\Http\Event::EVENT_RESPONSE, [$this, 'onResultContent'], 10);
				}
				elseif (!$fileResponse)
				{
					$vc = new \Change\Http\Rest\V1\ValueConverter($event->getUrlManager(),
						$event->getApplicationServices()->getDocumentManager());
					$vc->setCacheManager($event->getApplicationServices()->getCacheManager());
					if (!$temporaryDownload)
					{
						$temporaryDownload = $vc->getTemporaryDownloadUrl($storagePath);
					}
				}

				$result->setArray([
					'link' => $link->toArray(),
					'storageURI' => $storagePath,
					'size' => $size,
					'data' => $hrefContent,
					'temporaryDownload' => $temporaryDownload,
					'mTime' => $mTime ? $mTime->format(\DateTime::ATOM) : null,
					'mimeType' => $itemInfo->getMimeType(),
					'publicURL' => $itemInfo->getPublicURL()]);
			}
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onResultContent($event)
	{
		$path = $event->getParam('path');
		if ($path != null)
		{
			/* @var $result \Change\Http\Rest\V1\ArrayResult */
			$result = $event->getResult();
			$ra = $result->toArray();

			$response = new \Change\Http\StreamResponse();
			if (!$event->getController()->resultNotModified($event->getRequest(), $result))
			{
				$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
				$response->getHeaders()->clearHeaders();

				if (isset($ra['mimeType']))
				{
					$result->getHeaders()->addHeaderLine('Content-Type: ' . $ra['mimeType']);
				}
				else
				{
					$result->getHeaders()->addHeaderLine('Content-Type:  application/octet-stream');
				}
				$response->getHeaders()->addHeaders($result->getHeaders());
				$response->setUri($path);
			}
			else
			{
				$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
			}
			return $response;
		}
		return null;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onDownloadContent($event)
	{
		$path = $event->getParam('path');
		if ($path != null)
		{
			/* @var $result \Change\Http\Rest\V1\ArrayResult */
			$result = $event->getResult();
			if ($result instanceof \Change\Http\Rest\V1\ArrayResult)
			{
				$ra = $result->toArray();
				$response = new \Change\Http\StreamResponse();
				if (!$event->getController()->resultNotModified($event->getRequest(), $result))
				{
					$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
					$response->getHeaders()->clearHeaders();
					$result->getHeaders()
						->addHeaderLine('Content-Type', 'application/force-download; name="' . basename($path) . '"');
					$result->getHeaders()->addHeaderLine('Content-Transfer-Encoding', 'binary');
					$result->getHeaders()->addHeaderLine('Content-Length', $ra['size']);
					$result->getHeaders()->addHeaderLine('Content-Disposition', 'attachment; filename="' . basename($path) . '"');
					$offset = 48 * 60 * 60;
					$result->getHeaders()->addHeaderLine('Expires', gmdate('D, d M Y H:i:s', time() + $offset) . ' GMT');
					$result->getHeaders()->addHeaderLine('Cache-Control', 'no-cache, must-revalidate');
					$result->getHeaders()->addHeaderLine('Pragma', 'no-cache');
					$response->getHeaders()->addHeaders($result->getHeaders());
					$response->setUri($path);
				}
				else
				{
					$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
				}
				return $response;
			}
		}
		return null;
	}
}
