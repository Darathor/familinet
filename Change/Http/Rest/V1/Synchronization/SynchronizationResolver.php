<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Rest\V1\Synchronization;

use Change\Http\Rest\Request;
use Change\Http\Rest\V1\DiscoverNameSpace;
use Change\Http\Rest\V1\Resolver;

/**
 * @name \Change\Http\Rest\V1\Synchronization\SynchronizationResolver
 */
class SynchronizationResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		$namespaces = [];
		if (!isset($namespaceParts[1]))
		{
			$names = ['import'];
		}
		else
		{
			return $namespaces;
		}

		$base = implode('.', $namespaceParts);
		foreach ($names as $name)
		{
			$namespaces[] = $base . '.' . $name;
		}
		return $namespaces;
	}

	/**
	 * Set Event params: query
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @throws \RuntimeException
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$isDirectory = (bool)$event->getParam('isDirectory');
		$nbParts = count($resourceParts);
		if ($nbParts == 0 && $method === Request::METHOD_GET)
		{
			array_unshift($resourceParts, 'synchronization');
			$event->setParam('namespace', implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = function ($event)
			{
				$action = new DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		elseif ($method === Request::METHOD_POST)
		{
			if ($nbParts === 1 && $resourceParts[0] === 'import')
			{
				$action = function ($event)
				{
					$action = new Import();
					$action->execute($event);
				};
				$event->setAction($action);
				$event->setAuthorization(null);
				return;
			}
		}
	}
}