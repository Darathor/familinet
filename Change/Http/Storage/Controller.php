<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Storage;

/**
 * @name \Change\Http\Storage\Controller
 */
class Controller extends \Change\Http\Controller
{

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http', 'Http.Storage'];
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Http\Event::EVENT_REQUEST, [$this, 'onDefaultRequest'], 10);
		$eventManager->attach(\Change\Http\Event::EVENT_EXCEPTION, [$this, 'onException'], 5);
		$eventManager->attach(\Change\Http\Event::EVENT_RESPONSE, [$this, 'onDefaultResponse'], 5);
	}

	public function onDefaultRequest(\Change\Http\Event $event)
	{
		if (!$event->getApplication()->getConfiguration()->getEntry('Change/Http/Storage/enabled'))
		{
			throw new \RuntimeException('Storage controller disabled', 503);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	protected function getDefaultResponse($event)
	{
		$response = $this->createResponse();
		$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		return $response;
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function onDefaultResponse($event)
	{
		if ($event->getResponse())
		{
			return;
		}
		$result = $event->getResult();
		if ($result instanceof Result)
		{
			$acceptHeader = $event->getRequest()->getHeader('Accept');
			if ($acceptHeader instanceof \Zend\Http\Header\Accept && $acceptHeader->hasMediaType('application/json'))
			{
				$response = $this->createResponse();
				$response->getHeaders()->addHeaders($result->getHeaders());
				$response->setStatusCode($result->getHttpStatusCode());
				$event->setResponse($response);
				$response->setContent($result->getString());
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onException($event)
	{
		/* @var $exception \Exception */
		$exception = $event->getParam('Exception');
		$code = (int)$exception->getCode();
		if ($code < 500 || $code > 511)
		{
			$code = 500;
		}
		$result = new Result($exception->getMessage(), $code);
		$event->setResult($result);
		$event->setResponse(null);
	}
}