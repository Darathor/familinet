<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Storage;

/**
 * @name \Change\Http\Storage\Resolver
 */
class Resolver extends \Change\Http\BaseResolver
{
	public function resolve($event)
	{
		$request = $event->getRequest();
		$file = $request->getQuery('file');
		if (is_string($file) && $file)
		{
			$storageManager = $event->getApplicationServices()->getStorageManager();
			$storageURI = \Change\Storage\StorageManager::DEFAULT_SCHEME . '://' . $file;
			$parsed = @parse_url($storageURI);
			if ($parsed && isset($parsed['host'], $parsed['path']) && $storageManager->getStorageByName($parsed['host']))
			{
				$event->setParam('storageURI', $storageURI);
			}
			else
			{
				return;
			}
		}
		else
		{
			return;
		}

		$storageManager = $event->getApplicationServices()->getStorageManager();
		$itemInfo = $storageManager->getItemInfo($storageURI);
		if ($itemInfo && $itemInfo->getStorageEngine() instanceof \Change\Storage\Engines\LocalStorage)
		{
			$event->setParam('storageURI', $storageURI);
			if ($request->isPost())
			{
				$fileData = $request->getFiles('data');
				if ($fileData)
				{
					$result = new Result(var_export($fileData, true));
					$ok = @move_uploaded_file($fileData['tmp_name'], $storageURI);
					if ($ok)
					{
						$result->setString($storageURI . ' uploaded');
					}
					else
					{
						$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_500);
						$result->setString($storageURI . ' not uploaded');
					}
					$event->setResult($result);
				}
			}
			elseif ($request->isGet() && file_exists($storageURI))
			{
				$meta = $request->getQuery('meta');
				if ($meta)
				{
					switch ($meta)
					{
						case 'MimeType' :
							$mimeType = $itemInfo->getMimeType();
							$result = new Result((string)$mimeType);
							$result->setHeaderContentType('text/plain; charset=UTF-8');
							$event->setResult($result);
							break;
						case 'stat' :
							$stat = @stat($storageURI);
							$result = new Result(serialize($stat));
							$result->setHeaderContentType('application/octet-stream');
							$event->setResult($result);
							break;
						case 'unlink' :
							@unlink($storageURI);
							$result = new Result($storageURI . ' deleted');
							$result->setHeaderContentType('text/plain; charset=UTF-8');
							$event->setResult($result);
							break;
						case 'touch' :
							$time = $request->getQuery('time');
							$time = $time ? (int)$time : null;
							$aTime = $request->getQuery('atime');
							$aTime = $aTime ? (int)$aTime : null;
							touch($storageURI, $time, $aTime);
							$result = new Result($storageURI . ' deleted');
							$result->setHeaderContentType('text/plain; charset=UTF-8');
							$event->setResult($result);
							break;
					}
				}
				else
				{
					$event->getController()->getEventManager()
						->attach(\Change\Http\Event::EVENT_RESPONSE, [$this, 'onFileContentResponse'], 10);
				}
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onFileContentResponse($event)
	{
		$storageURI = $event->getParam('storageURI');
		$response = new \Change\Http\StreamResponse();
		$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$response->getHeaders()->clearHeaders();
		$response->getHeaders()->addHeaderLine('Content-Type', 'application/octet-stream');
		$response->getHeaders()->addHeaderLine('X-File-Modification-Time', filemtime($storageURI));
		$response->setUri($storageURI);
		$event->setResponse($response);
		return $response;
	}
}