<?php
namespace Change\Http;

/**
 * @name \Change\Http\StreamResponse
 */
class StreamResponse extends \Zend\Http\PhpEnvironment\Response
{
	/**
	 * @var string
	 */
	protected $uri;

	/**
	 * @param string $uri
	 * @return $this
	 */
	public function setUri($uri)
	{
		$this->uri = $uri;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUri()
	{
		return $this->uri;
	}

	/**
	 * Send content
	 *
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function sendContent()
	{
		$file = $this->getUri();
		$fp = @fopen($file, 'rb');
		if (!is_resource($fp))
		{
			header('HTTP/1.1 404 Not Found');
			header('Content-Length: 0');
			return $this;
		}

		$size = filesize($file); // File size
		$length = $size; // Content length
		$start = 0; // Start byte
		$end = $size - 1; // End byte

		header('Accept-Ranges: bytes');
		if (isset($_SERVER['HTTP_RANGE']))
		{
			$c_end = $end;

			list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
			if (strpos($range, ',') !== false)
			{
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				header("Content-Range: bytes $start-$end/$size");
				return $this;
			}
			if ($range == '-')
			{
				$c_start = $size - substr($range, 1);
			}
			else
			{
				$range = explode('-', $range);
				$c_start = $range[0];
				$c_end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
			}
			$c_end = ($c_end > $end) ? $end : $c_end;
			if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size)
			{
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				header("Content-Range: bytes $start-$end/$size");
				return $this;
			}
			$start = $c_start;
			$end = $c_end;
			$length = $end - $start + 1;
			fseek($fp, $start);
			header('HTTP/1.1 206 Partial Content');
		}
		header("Content-Range: bytes $start-$end/$size");
		header('Content-Length: ' . $length);

		$buffer = 1024 * 8;
		while (!feof($fp) && ($p = ftell($fp)) <= $end)
		{

			if ($p + $buffer > $end)
			{
				$buffer = $end - $p + 1;
			}
			set_time_limit(0);
			echo fread($fp, $buffer);
			flush();
		}

		fclose($fp);

		return $this;
	}
}