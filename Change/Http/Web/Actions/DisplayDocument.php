<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Web\Actions;

/**
 * @name \Change\Http\Web\Actions\DisplayDocument
 */
class DisplayDocument
{
	/**
	 * Use Required Event Params: pathRule
	 * @param \Change\Http\Web\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		/* @var $pathRule \Change\Http\Web\PathRule */
		$pathRule = $event->getPathRule();
		if (!($pathRule instanceof \Change\Http\Web\PathRule))
		{
			throw new \RuntimeException('Invalid Parameter: pathRule', 71000);
		}

		$requestQuery = $event->getRequest()->getQuery();
		if ($pathRule->getQuery())
		{
			$requestQuery->fromArray(\Zend\Stdlib\ArrayUtils::merge($pathRule->getQueryParameters(), $requestQuery->toArray()));
		}
		$pathRule->setQueryParameters($requestQuery->toArray());

		$document = $event->getDocument();
		if ($document instanceof \Change\Documents\AbstractDocument)
		{
			$eventName = \Change\Documents\Events\Event::EVENT_DISPLAY_PAGE;
			$documentEvent = new \Change\Documents\Events\Event($eventName, $document, $event->getParams());
			$document->getEventManager()->triggerEvent($documentEvent);
			$page = $documentEvent->getParam('page');
			if ($page instanceof \Change\Presentation\Interfaces\Page)
			{
				$event->setParam('page', $page);
				$section = $page->getSection();
				if ($section instanceof \Change\Presentation\Interfaces\Website)
				{
					$section = null;
				}
				$event->getUrlManager()->setSection($section);
				$event->setParam('section', $section);
			}
		}
	}
}
