<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Web\Actions;

/**
 * @name \Change\Http\Web\Actions\GetImageStorageItemContent
 */
class GetImageStorageItemContent
{
	/**
	 * Use Required Event Params: changeURI
	 * @param \Change\Http\Web\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		/* @var $changeURI \Zend\Uri\Uri */
		$changeURI = $event->getParam('changeURI');
		/* @var $originalURI \Zend\Uri\Uri */
		$originalURI = $event->getParam('originalURI');
		$maxWidth = $event->getParam('maxWidth', 0);
		$maxHeight = $event->getParam('maxHeight', 0);
		if (!($changeURI instanceof \Zend\Uri\Uri) || !($originalURI instanceof \Zend\Uri\Uri))
		{
			$event->setResult(new \Change\Http\Result(\Zend\Http\Response::STATUS_CODE_404));
			return;
		}

		$storageManager = $event->getApplicationServices()->getStorageManager();

		$originalPath = $originalURI->toString();
		$storageEngine = $storageManager->getStorageByStorageURI($originalPath);

		if (!$storageEngine || !is_callable([$storageEngine, 'getCompression']))
		{
			$event->setResult(new \Change\Http\Result(\Zend\Http\Response::STATUS_CODE_404));
			return;
		}

		$path = $changeURI->toString();
		if (!file_exists($originalPath))
		{
			$event->setResult(new \Change\Http\Result(\Zend\Http\Response::STATUS_CODE_404));
			return;
		}

		if (!file_exists($path) || filemtime($path) < filemtime($originalPath))
		{
			$event->getApplication()->getLogging()->info(__METHOD__ . ' resize to ' . $path);

			$resizer = new \Change\Presentation\Images\Resizer();
			$resizer->resize($originalPath, $path, $maxWidth, $maxHeight, $storageEngine->{'getCompression'}());
		}

		$forwardedAction = new GetStorageItemContent();
		$forwardedAction->execute($event);
	}
}