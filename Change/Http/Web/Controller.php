<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Web;

/**
 * @name \Change\Http\Web\Controller
 */
class Controller extends \Change\Http\Controller
{
	/**
	 * @var integer
	 */
	protected $errorCount = 0;

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['Http', 'Http.Web'];
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Http\Web\Event::EVENT_REQUEST, function($event) { $this->onDefaultRequest($event); }, 10);
		$eventManager->attach(\Change\Http\Web\Event::EVENT_ACTION, function($event) { $this->onDefaultAction($event); }, -10);
		$eventManager->attach(\Change\Http\Web\Event::EVENT_RESULT, function($event) { $this->onDefaultResult($event); }, 5);
		$eventManager->attach(\Change\Http\Web\Event::EVENT_RESPONSE, function($event) { $this->onDefaultResponse($event); }, 5);
		$eventManager->attach(\Change\Http\Web\Event::EVENT_EXCEPTION, function ($event) { $this->onException($event); }, 5);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	protected function onDefaultRequest(\Change\Http\Web\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$event->getUrlManager()
			->setDocumentManager($applicationServices->getDocumentManager())
			->setPathRuleManager($applicationServices->getPathRuleManager());
		$applicationServices->getDbProvider()->setReadOnly(true);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	protected function onDefaultAction(\Change\Http\Web\Event $event)
	{
		if ($event->getAction() === null && ($relativePath = $event->getParam('relativePath')))
		{
			$parts = explode('.', $relativePath);
			$ext = array_pop($parts);
			if (in_array($ext, ['css', 'js', 'json', 'xml', 'png', 'jpeg', 'jpg', 'gif', 'svg', 'eot', 'ttf', 'woff', 'woff2'])
				|| strpos($relativePath, 'Assets/Theme') === 0
			)
			{
				$event->getApplication()->getLogging()->info('resource not found:', $relativePath);
				$result = parent::notFound($event);
				$event->setAction(function (\Change\Http\Web\Event $event) use ($result)
				{
					$event->setResult($result);
				});
			}
		}
	}

	/**
	 * @param $request
	 * @return \Change\Http\Web\Event
	 */
	protected function createEvent($request)
	{
		$event = new \Change\Http\Web\Event();
		$event->setRequest($request);
		$script = $request->getServer('SCRIPT_NAME');
		if (strpos($request->getRequestUri(), $script) !== 0)
		{
			$script = null;
		}

		$urlManager = new UrlManager($request->getUri(), $script);
		$event->setUrlManager($urlManager);
		return $event;
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	protected function onDefaultResult(\Change\Http\Web\Event $event)
	{
		$page = $event->getParam('page');
		if ($page instanceof \Change\Presentation\Interfaces\Page)
		{
			$applicationServices = $event->getApplicationServices();
			$pageManager = $applicationServices->getPageManager();
			$result = $pageManager->setHttpWebEvent($event)->getPageResult($page);
			if ($result)
			{
				$event->setResult($result);
			}
		}
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	protected function onDefaultResponse($event)
	{
		$result = $event->getResult();
		if ($result instanceof \Change\Http\Result)
		{
			$response = $event->getController()->createResponse();
			$response->setStatusCode($result->getHttpStatusCode());
			$responseHeaders = $response->getHeaders();
			$responseHeaders->addHeaders($result->getHeaders());
			if (!$responseHeaders->has('X-Frame-Options'))
			{
				$responseHeaders->addHeaderLine('X-Frame-Options: SAMEORIGIN');
			}

			$monitoring = $event->getApplication()->getConfiguration()->getEntry('Change/Http/Web/Monitoring');
			if ($monitoring)
			{
				$responseHeaders->addHeaderLine('Change-Memory-Usage: ' . number_format(memory_get_usage()));
			}

			if ($result instanceof \Change\Http\Web\Result\Page)
			{
				$acceptHeader = $event->getRequest()->getHeader('Accept');
				if ($acceptHeader instanceof \Zend\Http\Header\Accept && $acceptHeader->hasMediaType('text/html'))
				{
					$response->setContent($result->toHtml());
				}
			}
			elseif ($result instanceof \Change\Http\Web\Result\Resource)
			{
				$response->setContent($result->getContent());
			}
			elseif ($result instanceof \Change\Http\Web\Result\AjaxResult)
			{
				$response->setContent(json_encode($result->toArray()));
			}
			else
			{
				$callable = [$result, 'toHtml'];
				if (is_callable($callable))
				{
					$response->setContent(call_user_func($callable));
				}
				else
				{
					$response->setContent((string)$result);
				}
			}
			$event->setResponse($response);
		}
	}

	/**
	 * @param \Change\Http\Request $request
	 * @return boolean
	 */
	protected function acceptHtml($request)
	{
		$accept = $request->getHeader('Accept');
		if ($accept instanceof \Zend\Http\Header\Accept)
		{
			/* @var $acceptFieldValuePart \Zend\Http\Header\Accept\FieldValuePart\AcceptFieldValuePart */
			foreach ($accept->getPrioritized() as $acceptFieldValuePart)
			{
				if ($acceptFieldValuePart->getSubtype() === 'html')
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @return \Change\Http\Result
	 */
	public function notFound($event)
	{
		if ($this->acceptHtml($event->getRequest()))
		{
			$authorization = $event->getAuthorization();
			if (!is_callable($authorization))
			{
				$this->doSendAuthenticate($event);
			}
			$result = $this->getFunctionalResult($event, 'Error_404');
			if ($result !== null)
			{
				$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
				return $result;
			}
		}
		return parent::notFound($event);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @return \Change\Http\Result
	 */
	public function unauthorized($event)
	{
		if ($this->acceptHtml($event->getRequest()))
		{
			$result = $this->getFunctionalResult($event, 'Error_401');
			if ($result !== null)
			{
				$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_401);
				return $result;
			}
		}
		return parent::unauthorized($event);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @return \Change\Http\Result
	 */
	public function forbidden($event)
	{
		if ($this->acceptHtml($event->getRequest()))
		{
			$result = $this->getFunctionalResult($event, 'Error_403');
			if ($result !== null)
			{
				$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_403);
				return $result;
			}
		}
		return parent::forbidden($event);
	}


	protected function onException(\Change\Http\Web\Event $event)
	{
		$this->errorCount++;
		$accept = $event->getRequest()->getHeader('Accept');
		if ($accept instanceof \Zend\Http\Header\Accept)
		{
			/* @var $acceptFieldValuePart \Zend\Http\Header\Accept\FieldValuePart\AcceptFieldValuePart */
			foreach ($accept->getPrioritized() as $acceptFieldValuePart)
			{
				if ($acceptFieldValuePart->getSubtype() === 'html')
				{
					$result = $this->getFunctionalResult($event, 'Error_500');
					if ($result !== null)
					{
						$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_500);
						$event->setResult($result);
					}
					break;
				}
			}
		}
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param string $functionCode
	 * @return \Change\Http\Result
	 */
	protected function getFunctionalResult($event, $functionCode)
	{
		try
		{
			$page = null;
			if ($this->errorCount < 2)
			{
				$target = null;
				$pathRule = $event->getPathRule();
				if($pathRule)
				{
					$sectionId = $pathRule->getSectionId();
					if ($sectionId)
					{
						$target = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($sectionId);
					}
				}

				if (!$target)
				{
					$website = $event->getWebsite();
					if ($website instanceof \Change\Documents\AbstractDocument)
					{
						$target = $website;
					}
				}

				if ($target)
				{
					$em = $target->getEventManager();
					$args = ['functionCode' => $functionCode];
					$docEvent = new \Change\Documents\Events\Event('getPageByFunction', $target, $args);
					$em->triggerEvent($docEvent);
					$page = $docEvent->getParam('page');
				}
			}

			if ($this->errorCount < 3)
			{
				if (!($page instanceof \Change\Presentation\Interfaces\Page))
				{
					$page = new \Change\Presentation\Themes\DefaultPage($event->getApplicationServices()->getThemeManager(), $functionCode);
				}
				$event->setParam('errorFunction', $functionCode);
				$event->setParam('page', $page);
				$this->doSendResult($event);
				$event->setParam('page', null);
			}
		}
		catch (\Exception $e)
		{
			$this->doSendException($event, $e);
		}
		catch (\Throwable $throwable)
		{
			$this->doSendThrowable($event, $throwable);
		}
		return $event->getResult();
	}
}