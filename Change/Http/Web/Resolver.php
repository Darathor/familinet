<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Http\Web;

/**
 * @name \Change\Http\Web\Resolver
 */
class Resolver extends \Change\Http\BaseResolver
{
	/**
	 * @param Event $event
	 * @return void
	 */
	public function resolve($event)
	{
		$request = $event->getRequest();
		$path = $request->getPath();
		if ($request->isGet())
		{
			if (\preg_match('/^\/Imagestorage\/([A-Za-z0-9]+)\/([\d]+)\/([\d]+)(\/.+)$/', $path, $matches))
			{
				$event->getApplication()->getLogging()->info('GET', $path);

				$storageName = $matches [1];
				$maxWidth = (int)$matches[2];
				$maxHeight = (int)$matches[3];
				$path = $matches[4];
				$originalURI = $event->getApplicationServices()->getStorageManager()->buildChangeURI($storageName, $path);
				$changeURI = $event->getApplicationServices()->getStorageManager()
					->buildChangeURI($storageName, $path, ['max-width' => $maxWidth, 'max-height' => $maxHeight]);
				$event->setParam('originalURI', $originalURI);
				$event->setParam('changeURI', $changeURI);
				$event->setParam('maxWidth', $maxWidth);
				$event->setParam('maxHeight', $maxHeight);
				$action = static function ($event) {
					(new Actions\GetImageStorageItemContent())->execute($event);
				};
				$event->setAction($action);
				$event->stopPropagation();
				return;
			}

			if (\preg_match('/^\/Storage\/([\w]+)\/(.+)$/', $path, $matches))
			{
				$event->getApplication()->getLogging()->info('GET', $path);

				$storageName = $matches[1];
				$changeURI = $event->getApplicationServices()->getStorageManager()
					->buildChangeURI($storageName, '/' . $matches[2]);
				$event->setParam('changeURI', $changeURI);
				$action = static function ($event) {
					$action = new \Change\Http\Web\Actions\GetStorageItemContent();
					$action->execute($event);
				};
				$event->setAction($action);
				return;
			}
		}

		if (!($website = $event->getWebsite()))
		{
			return;
		}

		$relativePath = $this->getRelativePath($path, $website->getRelativePath());
		$event->setParam('relativePath', $relativePath);

		if (\preg_match('/^Theme\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)$/', $relativePath, $matches))
		{
			$themeName = $matches[1] . '_' . $matches[2];
			$themeResourcePath = $matches[3];
			$themeManager = $event->getApplicationServices()->getThemeManager();
			$theme = $themeManager->getByName($themeName);
			if ($theme)
			{
				$event->setParam('theme', $theme);
				$event->setParam('themeResourcePath', $themeResourcePath);
				if ($themeResourcePath === 'ng-templates.js')
				{
					$action = static function ($event) {
						$action = new \Change\Http\Web\Actions\GetThemeResource();
						$action->getNgTemplates($event);
					};
					$event->setAction($action);
				}
				$event->getApplication()->getLogging()->info($request->getMethod(), $website->getLCID(), '/' . $relativePath);
			}
			return;
		}

		if (\preg_match('/^Action\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9\/]+)$/', $relativePath, $matches))
		{
			$event->setParam('action', [$matches[1], $matches[2], $matches[3]]);
			$action = static function ($event) {
				$action = new \Change\Http\Web\Actions\ExecuteByName();
				$action->execute($event);
			};
			$event->getApplicationServices()->getDbProvider()->setReadOnly(false);
			$event->setAction($action);
			if (!$request->getPost('anonymous', $request->getQuery('anonymous')))
			{
				$event->setAuthorization(static function () {
					return true;
				});
			}
			$event->getApplication()->getLogging()->info($request->getMethod(), $website->getLCID(), '/' . $relativePath);
			return;
		}

		$pathRule = $this->findRule($event, $website);
		if (!$pathRule)
		{
			return;
		}
		$event->getApplication()->getLogging()->info($request->getMethod(), $pathRule->getWebsiteId()
			, $pathRule->getLCID(), '/' . $relativePath, $pathRule->getHttpStatus());

		$event->setParam('pathRule', $pathRule);
		$event->setParam('relativePath', $pathRule->getRelativePath());
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = null;
		if ($pathRule->getDocumentId())
		{
			$eventManager = $documentManager->getEventManager();
			$args = $eventManager->prepareArgs(['documentId' => $pathRule->getDocumentId(), 'httpEvent' => $event]);
			$eventManager->trigger('getDisplayableDocument', $documentManager, $args);
			if (isset($args['displayableDocument'])
				&& $args['displayableDocument'] instanceof \Change\Documents\AbstractDocument
			)
			{
				$document = $args['displayableDocument'];
				$event->setParam('document', $document);
			}
		}

		$urlManager = $event->getUrlManager();
		if ($pathRule->getHttpStatus() !== \Zend\Http\Response::STATUS_CODE_200 && $pathRule->getLocation() === null)
		{
			// Generic document URL
			if (!$document)
			{
				return;
			}
			$queryParameters = \array_merge_recursive($request->getQuery()->toArray(), $pathRule->getQueryParameters());
			$pathRule->setQueryParameters($queryParameters);
			$validPathRule = $urlManager->getValidDocumentRule($document, $pathRule);
			$absoluteUrl = $urlManager->absoluteUrl(true);
			if ($validPathRule instanceof PathRule)
			{
				foreach ($validPathRule->getQueryParameters() as $key => $param)
				{
					unset($queryParameters[$key]);
				}
				$location = $urlManager->getByPathInfo($validPathRule->getRelativePath(), $queryParameters);
				$pathRule->setLocation($location->normalize()->toString());
				$urlManager->absoluteUrl($absoluteUrl);
			}
			elseif ($pathRule->getRuleId())
			{
				$location = $urlManager->getCanonicalByDocument($document, $queryParameters);
				$pathRule->setLocation($location->normalize()->toString());
				$urlManager->absoluteUrl($absoluteUrl);
			}
			else
			{
				$pathRule->setHttpStatus(\Zend\Http\Response::STATUS_CODE_200);
			}
		}

		if ($pathRule->getLocation())
		{
			$action = static function ($event) {
				$action = new \Change\Http\Web\Actions\RedirectPathRule();
				$action->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if ($document && $pathRule->getHttpStatus() === \Zend\Http\Response::STATUS_CODE_200)
		{
			if ($document instanceof \Change\Documents\Interfaces\Publishable && !$pathRule->getSectionId())
			{
				$section = $document->getCanonicalSection($website);
				if ($section && !($section instanceof \Change\Presentation\Interfaces\Website))
				{
					$pathRule->setSectionId($section->getId());
				}
			}

			$action = static function ($event) {
				(new \Change\Http\Web\Actions\DisplayDocument())->execute($event);
			};
			$event->setAction($action);
			$authorizedSectionId = $pathRule->getSectionId() ?: $pathRule->getWebsiteId();
			$this->setPathRuleAuthorization($event, $authorizedSectionId, $pathRule->getWebsiteId());
			return;
		}
	}

	/**
	 * @param string $path
	 * @param string $websitePathPart
	 * @return boolean
	 */
	protected function isBasePath($path, $websitePathPart)
	{
		if ($websitePathPart && $path)
		{
			$path = \ltrim($path, '/');
			return $websitePathPart === $path || $websitePathPart . '/' === $path || \strpos($path, $websitePathPart . '/') === 0;
		}
		return true;
	}

	/**
	 * @param string $path
	 * @param string $websitePathPart
	 * @return string
	 */
	protected function getRelativePath($path, $websitePathPart)
	{
		if ($websitePathPart)
		{
			$websitePathPart = '/' . $websitePathPart . '/';
			if (\strpos($path, $websitePathPart) === 0)
			{
				return \substr($path, \strlen($websitePathPart));
			}
		}
		elseif ($path)
		{
			$path = \ltrim($path, '/');
		}
		return $path;
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 * @param \Change\Presentation\Interfaces\Website $website
	 * @return \Change\Http\Web\PathRule|null
	 */
	protected function findRule($event, $website)
	{
		if ($website instanceof \Change\Presentation\Interfaces\Website)
		{
			$websiteRelativePath = $website->getRelativePath();
			$pathInfo = $event->getRequest()->getPath();
			if ($pathInfo === $website->getScriptName())
			{
				$pathInfo = '/';
			}

			if ($this->isBasePath($pathInfo, $websiteRelativePath))
			{
				$relativePath = $this->getRelativePath($pathInfo, $websiteRelativePath);
			}
			else
			{
				return null;
			}

			$websiteId = $website->getId();
			$LCID = $website->getLCID();

			if (!$relativePath)
			{
				// Home.
				$pathRule = new \Change\Http\Web\PathRule();
				$pathRule->setWebsiteId($websiteId)->setLCID($LCID);
				$pathRule->setRelativePath(null);
				$pathRule->setDocumentId($website->getId());
				if ($event->getRequest()->isGet() && !$event->getRequest()->getQuery()->count())
				{
					$pathRule->setHttpStatus(\Zend\Http\Response::STATUS_CODE_200);
				}
				else
				{
					$pathRule->setHttpStatus(\Zend\Http\Response::STATUS_CODE_303);
				}
				return $pathRule;
			}

			$applicationServices = $event->getApplicationServices();
			$pathRule = new \Change\Http\Web\PathRule();
			$pathRule->setWebsiteId($websiteId)->setLCID($LCID)->setRelativePath($relativePath);

			$cacheManager = $applicationServices->getCacheManager();
			$key = null;
			if ($cacheManager->isValidNamespace('WebsiteResolver'))
			{
				$key = 'url-' . $websiteId . '-' . $LCID . '-' . $pathRule->getHash();
				$data = $cacheManager->getEntry('WebsiteResolver', $key, ['ttl' => 3600]);
				if (\is_array($data))
				{
					foreach ($data as $k => $v)
					{
						switch ($k)
						{
							case 'ruleId':
								$pathRule->setRuleId($v);
								break;
							case 'documentId':
								$pathRule->setDocumentId($v);
								break;
							case 'documentAliasId':
								$pathRule->setDocumentAliasId($v);
								break;
							case 'sectionId':
								$pathRule->setSectionId($v);
								break;
							case 'httpStatus':
								$pathRule->setHttpStatus($v);
								break;
							case 'query':
								$pathRule->setQuery($v);
								break;
							case 'userEdited':
								$pathRule->setUserEdited($v);
								break;
						}
					}
					return $pathRule;
				}
			}

			$pathRuleManager = $applicationServices->getPathRuleManager();
			if ($dbRule = $pathRuleManager->getPathRule($websiteId, $LCID, $relativePath))
			{
				if ($key)
				{
					$cacheManager->setEntry('WebsiteResolver', $key, $dbRule->toArray(), ['ttl' => 3600]);
				}
				return $dbRule;
			}

			if ($this->findDefaultRule($pathRule))
			{
				if ($key)
				{
					$cacheManager->setEntry('WebsiteResolver', $key, $pathRule->toArray(), ['ttl' => 3600]);
				}
				return $pathRule;
			}
		}
		return null;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param PathRule $pathRule
	 * @return bool
	 */
	protected function findDbRule($dbProvider, $pathRule)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select($fb->alias($fb->column('rule_id'), 'ruleId'),
			$fb->alias($fb->column('document_id'), 'documentId'),
			$fb->alias($fb->column('section_id'), 'sectionId'),
			$fb->alias($fb->column('relative_path'), 'relativePath'),
			$fb->alias($fb->column('http_status'), 'httpStatus'),
			$fb->alias($fb->column('query'), 'query'),
			$fb->alias($fb->column('user_edited'), 'userEdited'));

		$qb->from($qb->getSqlMapping()->getPathRuleTable());

		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('website_id'), $fb->integerParameter('websiteId')),
			$fb->eq($fb->column('lcid'), $fb->parameter('LCID')),
			$fb->eq($fb->column('hash'), $fb->parameter('hash'))
		));

		$sq = $qb->query();
		$sq->bindParameter('websiteId', $pathRule->getWebsiteId());
		$sq->bindParameter('LCID', $pathRule->getLCID());
		$sq->bindParameter('hash', $pathRule->getHash());

		$row = $sq->getFirstResult($sq->getRowsConverter()
			->addIntCol('ruleId', 'documentId', 'sectionId', 'httpStatus')
			->addTxtCol('relativePath', 'query')
			->addBoolCol('userEdited'));

		if ($row)
		{
			$pathRule->setRuleId($row['ruleId']);
			$pathRule->setRelativePath($row['relativePath']);
			$pathRule->setHttpStatus($row['httpStatus']);
			$pathRule->setQuery($row['query']);
			$pathRule->setUserEdited($row['userEdited']);

			if ($row['documentId'])
			{
				$pathRule->setDocumentId((int)$row['documentId']);
			}

			if ($row['sectionId'])
			{
				$pathRule->setSectionId((int)$row['sectionId']);
			}
			return true;
		}
		return false;
	}

	/**
	 * @param PathRule $pathRule
	 * @return boolean
	 */
	protected function findDefaultRule($pathRule)
	{
		if (\preg_match('/^document(?:\/(\d{4,10}))?\/(\d{4,10})(\.html|\/)$/', $pathRule->getRelativePath(), $matches))
		{
			$pathRule->setDocumentId((int)$matches[2]);
			if ($matches[1] !== '')
			{
				$pathRule->setSectionId((int)$matches[1]);
			}
			$pathRule->setHttpStatus(\Zend\Http\Response::STATUS_CODE_303);
			return true;
		}
		return false;
	}

	/**
	 * @param Event $event
	 * @param integer $sectionId
	 * @param integer $websiteId
	 */
	protected function setPathRuleAuthorization($event, $sectionId, $websiteId)
	{
		if ($sectionId)
		{
			$authorisation = static function (Event $event) use ($sectionId, $websiteId) {
				$applicationServices = $event->getApplicationServices();
				$permissionManager = $applicationServices->getPermissionsManager();
				$webSectionIds = $permissionManager->getWebSectionIds();
				if (!isset($webSectionIds[$websiteId]))
				{
					$cacheManager = $applicationServices->getCacheManager();
					$cacheOptions = ['ttl' => 3600];
					$cacheKey = 'web_permissions_' . $websiteId;
					$cachedData = $cacheManager->getEntry('user', $cacheKey, $cacheOptions);
					if ($cachedData === null)
					{
						$cachedData = ['a' => true, 's' => []];
						$qb = $applicationServices->getDbProvider()->getNewQueryBuilder();
						$fb = $qb->getFragmentBuilder();
						$qb->select($fb->alias($fb->column('section_id'), 'sid'),
							$fb->alias($fb->column('accessor_id'), 'uid'));

						$qb->from($fb->getSqlMapping()->getWebPermissionRuleTable());
						$qb->where($fb->eq($fb->column('website_id'), $fb->number($websiteId)));
						$select = $qb->query();
						$rows = $select->getResults($select->getRowsConverter()->addIntCol('sid', 'uid'));
						$anonymous = true;
						foreach ($rows as $row)
						{
							if ($row['uid'])
							{
								$anonymous = false;
							}
							$cachedData['s'][$row['sid']][] = $row['uid'];
						}
						$cachedData['a'] = $anonymous;
						$cacheManager->setEntry('user', $cacheKey, $cachedData, $cacheOptions);
					}

					if ($cachedData['a'])
					{
						$webSectionIds[$websiteId] = true;
					}
					else
					{
						$allowedAccessorIds = $permissionManager->getAllowedAccessorIds();
						/** @noinspection ForeachSourceInspection */
						foreach ($cachedData['s'] as $sid => $accessorIds)
						{
							if (\array_intersect($allowedAccessorIds, $accessorIds))
							{
								$webSectionIds[$websiteId][] = $sid;
							}
						}
					}
					$permissionManager->setWebSectionIds($webSectionIds);
				}
				return $event->getPermissionsManager()->isWebAllowed($sectionId, $websiteId);
			};
			$event->setAuthorization($authorisation);
		}
	}
}