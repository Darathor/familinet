<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\I18n;

/**
 * @api
 * @name \Change\I18n\I18nManager
 */
class I18nManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_KEY_NOT_FOUND = 'key-not-found';
	const EVENT_FORMATTING = 'formatting';
	const EVENT_MANAGER_IDENTIFIER = 'I18n';

	/**
	 * @var array
	 */
	protected $langMap;

	/**
	 * @var string ex: "fr_FR"
	 */
	protected $uiLCID;

	/**
	 * @var string[] ex: "fr_FR"
	 */
	protected $supportedLCIDs = [];

	/**
	 * @var string
	 */
	protected $uiDateFormat;

	/**
	 * @var string
	 */
	protected $uiDateTimeFormat;

	/**
	 * @var \DateTimeZone
	 */
	protected $uiTimeZone;

	/**
	 * @var array
	 */
	protected $i18nDocumentsSynchro;

	/**
	 * @var array
	 */
	protected $i18nSynchro;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var array<string, string>
	 */
	protected $packageList;

	/**
	 * @var array
	 */
	protected $loadedPackages = [];

	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		$this->supportedLCIDs = $application->getConfiguration()->getEntry('Change/I18n/supported-lcids');
		if (!is_array($this->supportedLCIDs) || count($this->supportedLCIDs) === 0)
		{
			$this->supportedLCIDs = ['fr_FR'];
		}
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return \Change\Workspace
	 */
	protected function getWorkspace()
	{
		return $this->getApplication()->getWorkspace();
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	protected function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager(\Change\Plugins\PluginManager $pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param string $LCID
	 * @return boolean
	 * @api
	 */
	public function isValidLCID($LCID)
	{
		return is_string($LCID) && preg_match('/^[a-z]{2}_[A-Z]{2}$/', $LCID);
	}

	/**
	 * Get all supported LCIDs.
	 * @return string[] , ex: "fr_FR"
	 * @api
	 */
	public function getSupportedLCIDs()
	{
		return $this->supportedLCIDs;
	}

	/**
	 * @param string $LCID
	 * @return boolean
	 * @api
	 */
	public function isSupportedLCID($LCID)
	{
		return ($this->isValidLCID($LCID) && in_array($LCID, $this->getSupportedLCIDs()));
	}

	/**
	 * @return boolean
	 * @api
	 */
	public function supportsMultipleLCIDs()
	{
		return count($this->supportedLCIDs) > 1;
	}

	/**
	 * Get the default LCID.
	 * @return string two lower-cased letters code, ex: "fr_FR"
	 * @api
	 */
	public function getDefaultLCID()
	{
		return $this->supportedLCIDs[0];
	}

	/**
	 * Get the UI LCID.
	 * @return string two lower-cased letters code, ex: "fr_FR"
	 * @api
	 */
	public function getLCID()
	{
		if ($this->uiLCID === null)
		{
			$this->setLCID($this->getDefaultLCID());
		}
		return $this->uiLCID;
	}

	/**
	 * Set the UI LCID.
	 * @param string $LCID ex: "fr_FR"
	 * @throws \InvalidArgumentException if the lang is not supported
	 * @api
	 */
	public function setLCID($LCID)
	{
		if (!in_array($LCID, $this->getSupportedLCIDs()))
		{
			throw new \InvalidArgumentException('Not supported LCID: ' . $LCID, 80000);
		}
		$this->uiLCID = $LCID;
	}

	/**
	 * Loads the i18n synchro configuration.
	 */
	protected function loadI18nSynchroConfiguration()
	{
		$data = $this->getConfiguration()->getEntry('Change/I18n/synchro/keys', null);
		$this->i18nSynchro = $this->cleanI18nSynchroConfiguration($data);
	}

	/**
	 * Clean i18n synchro configuration.
	 * @param array $data
	 * @return array|bool
	 * @see loadI18nSynchroConfiguration()
	 */
	protected function cleanI18nSynchroConfiguration($data)
	{
		if ($data && \is_array($data))
		{
			$result = [];
			$LCIDs = $this->getSupportedLCIDs();
			foreach ($data as $LCID => $fromLCID)
			{
				if (\in_array($LCID, $LCIDs, true) && \in_array($fromLCID, $LCIDs, true))
				{
					$result[$LCID] = $fromLCID;
				}
			}
			if ($result)
			{
				return $result;
			}
		}
		return false;
	}

	/**
	 * @return boolean
	 */
	public function hasI18nSynchro()
	{
		if ($this->i18nSynchro === null)
		{
			$this->loadI18nSynchroConfiguration();
		}
		return $this->i18nSynchro !== false;
	}

	/**
	 * @return array string : string[]
	 */
	public function getI18nSynchro()
	{
		return $this->hasI18nSynchro() ? $this->i18nSynchro : [];
	}

	/**
	 * Converts a LCID to a two characters lang code.
	 * @param string $LCID
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function getLangByLCID($LCID)
	{
		if ($this->langMap === null)
		{
			$this->langMap = $this->getConfiguration()->getEntry('Change/I18n/langs', []);
		}

		if (!isset($this->langMap[$LCID]))
		{
			if (strlen($LCID) === 5)
			{
				$this->langMap[$LCID] = strtolower(substr($LCID, 0, 2));
			}
			else
			{
				throw new \InvalidArgumentException('Not supported LCID: ' . $LCID, 80000);
			}
		}
		return $this->langMap[$LCID];
	}

	/**
	 * @param string $LCID
	 * @param array $formatters value in array lab, lc, uc, ucf, js, html, attr, short
	 * @return string
	 * @api
	 */
	public function transLCID($LCID, $formatters = [])
	{
		$currentLCID = $this->getLCID();
		if (in_array('short', $formatters))
		{
			$label = \Locale::getDisplayLanguage($LCID, $currentLCID);
		}
		else
		{
			$label = \Locale::getDisplayName($LCID, $currentLCID);
		}
		return $formatters ? $this->formatText($currentLCID, $label, $formatters) : $label;
	}

	/**
	 * @param string $LCID
	 * @param array $formatters value in array lab, lc, uc, ucf, js, html, attr, short
	 * @return string
	 * @api
	 */
	public function transLCIDInSelf($LCID, $formatters = [])
	{
		if (in_array('short', $formatters))
		{
			$label = \Locale::getDisplayLanguage($LCID, $LCID);
		}
		else
		{
			$label = \Locale::getDisplayName($LCID, $LCID);
		}
		return $formatters ? $this->formatText($LCID, $label, $formatters) : $label;
	}

	/**
	 * For example: trans('c.date.default_date_format')
	 * @param string | \Change\I18n\PreparedKey $cleanKey
	 * @param array $formatters value in array lab, lc, uc, ucf, js, html, attr
	 * @param array $replacements
	 * @return string | $cleanKey
	 * @api
	 */
	public function trans($cleanKey, $formatters = [], $replacements = [])
	{
		return $this->transForLCID($this->getLCID(), $cleanKey, $formatters, $replacements);
	}

	/**
	 * For example: transForLCID('fr_FR', 'c.date.default_date_format')
	 * @param string $LCID
	 * @param string | \Change\I18n\PreparedKey $cleanKey
	 * @param array $formatters value in array lab, lc, uc, ucf, js, attr, raw, text, html
	 * @param array $replacements
	 * @return string
	 * @api
	 */
	public function transForLCID($LCID, $cleanKey, $formatters = [], $replacements = [])
	{
		if ($cleanKey instanceof \Change\I18n\PreparedKey)
		{
			$preparedKey = $cleanKey;
			$preparedKey->mergeFormatters($formatters);
			$preparedKey->mergeReplacements($replacements);
		}
		else
		{
			$preparedKey = new \Change\I18n\PreparedKey($cleanKey, $formatters, $replacements);
		}

		$textKey = $preparedKey->getKey();
		if ($preparedKey->isValid())
		{
			$parts = explode('.', $textKey);
			$id = array_pop($parts);
			$packageName = implode('.', $parts);

			$translations = $this->getTranslationsForPackage($packageName, $LCID);
			if (is_array($translations) && isset($translations[$id]))
			{
				return $this->formatText($LCID, $translations[$id], $preparedKey->getFormatters(),
					$preparedKey->getReplacements());
			}
			return $this->dispatchKeyNotFound($preparedKey, $LCID);
		}
		return $textKey;
	}

	/**
	 * For example: formatText('fr_FR', 'My text.')
	 * @param string $LCID
	 * @param string $text
	 * @param array $formatters value in array lab, lc, uc, ucf, js, attr, raw, text, html
	 * @param array $replacements
	 * @return string
	 * @api
	 */
	public function formatText($LCID, $text, $formatters = [], $replacements = [])
	{
		if (count($replacements))
		{
			$search = [];
			$replace = [];
			foreach ($replacements as $key => $value)
			{
				$search[] = '$' . $key . '$';
				$replace[] = $value;
			}
			$text = str_ireplace($search, $replace, $text);
		}

		if (count($formatters))
		{
			$text = $this->dispatchFormatting($text, $formatters, $LCID);
		}
		return $text;
	}

	/**
	 * @param string $transString
	 * @return \Change\I18n\PreparedKey
	 */
	public function prepareKeyFromTransString($transString)
	{
		$formatters = [];
		$replacements = [];
		$parts = explode(',', $transString);
		$count = count($parts);
		for ($i = 1; $i < $count; $i++)
		{
			$data = trim($parts[$i]);
			if ($data == '')
			{
				continue;
			}
			if (strpos($data, '='))
			{
				$subParts = explode('=', $data);
				if (count($subParts) == 2)
				{
					list ($name, $value) = $subParts;
					$replacements[trim($name)] = trim($value);
				}
			}
			else
			{
				$data = strtolower($data);
				$formatters[] = $data;
			}
		}
		return new \Change\I18n\PreparedKey(trim($parts[0]), $formatters, $replacements);
	}

	/**
	 * @param $packageName
	 * @param $LCID
	 * @return string[]
	 */
	public function buildCacheInfo($packageName, $LCID)
	{
		$key = 'package_' . $LCID . '_' . str_replace('.', '_', $packageName);
		return ['I18n', $key];
	}

	// Events.

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/I18n');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(static::EVENT_KEY_NOT_FOUND, [$this, 'onKeyNotFound'], 5);
		$eventManager->attach(static::EVENT_KEY_NOT_FOUND, [$this, 'onKeyDayNameNotFound'], 6);
		$eventManager->attach(static::EVENT_FORMATTING, [$this, 'onFormatting'], 5);

		$eventManager->attach('getTranslationsForPackage', [$this, 'onGetTranslationsPackageFromCache'], 50);
		$eventManager->attach('getTranslationsForPackage', [$this, 'onGetTranslationsForPackage'], 5);
		$eventManager->attach('getTranslationsForPackage', [$this, 'onSetTranslationsPackageInCache'], -50);
	}

	/**
	 * @param \Change\I18n\PreparedKey $preparedKey
	 * @param string $LCID
	 * @return string
	 */
	protected function dispatchKeyNotFound($preparedKey, $LCID)
	{
		$args = ['preparedKey' => $preparedKey, 'LCID' => $LCID];
		$event = new \Change\Events\Event(static::EVENT_KEY_NOT_FOUND, $this, $args);
		$callback = function ($result) {
			return is_string($result);
		};
		$results = $this->getEventManager()->triggerEventUntil($callback, $event);
		return ($results->stopped() && is_string($results->last())) ? $results->last() : $event->getParam('text');
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onKeyNotFound($event)
	{
		if (!$event->getParam('text'))
		{
			$key = $event->getParam('preparedKey')->getKey();
			$this->getLogging()->info('I18n key not found:', $key, $event->getParam('LCID'));
			$event->setParam('text', $key);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onKeyDayNameNotFound($event)
	{
		$key = $event->getParam('preparedKey')->getKey();
		$lcid = $event->getParam('LCID');

		$date = null;
		if ($key === 'c.date.long_day_name_monday')
		{
			$date = new \DateTime('2013-07-01');
		}
		elseif ($key === 'c.date.long_day_name_tuesday')
		{
			$date = new \DateTime('2013-07-02');
		}
		elseif ($key === 'c.date.long_day_name_wednesday')
		{
			$date = new \DateTime('2013-07-03');
		}
		elseif ($key === 'c.date.long_day_name_thursday')
		{
			$date = new \DateTime('2013-07-04');
		}
		elseif ($key === 'c.date.long_day_name_friday')
		{
			$date = new \DateTime('2013-07-05');
		}
		elseif ($key === 'c.date.long_day_name_saturday')
		{
			$date = new \DateTime('2013-07-06');
		}
		elseif ($key === 'c.date.long_day_name_sunday')
		{
			$date = new \DateTime('2013-07-07');
		}

		if ($date != null)
		{
			$event->setParam('text', $this->formatDate($lcid, $date, 'cccc'));
		}
	}

	/**
	 * @param string $text
	 * @param string[] $formatters
	 * @param string $LCID
	 * @return string
	 */
	protected function dispatchFormatting($text, $formatters, $LCID)
	{
		$args = ['text' => $text, 'formatters' => $formatters, 'LCID' => $LCID];
		$event = new \Change\Events\Event(static::EVENT_FORMATTING, $this, $args);
		$callback = function ($result) {
			return is_string($result);
		};
		$results = $this->getEventManager()->triggerEventUntil($callback, $event);
		return ($results->stopped() && is_string($results->last())) ? $results->last() : $event->getParam('text');
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onFormatting($event)
	{
		$text = $event->getParam('text');
		$LCID = $event->getParam('LCID');
		foreach ($event->getParam('formatters') as $formatter)
		{
			$callable = [$this, 'transform' . ucfirst($formatter)];
			if (is_callable($callable))
			{
				$text = call_user_func($callable, $text, $LCID);
			}
			else
			{
				$this->getLogging()->info(__METHOD__ . ' Unknown formatter ' . $formatter);
			}
		}
		$event->setParam('text', $text);
	}

	// Dates.

	/**
	 * @param string $LCID
	 * @return string
	 * @api
	 */
	public function getDateFormat($LCID)
	{
		if ($this->uiDateFormat)
		{
			return $this->uiDateFormat;
		}
		return $this->transForLCID($LCID, 'c.date.default_date_format');
	}

	/**
	 * @param string $dateFormat
	 * @api
	 */
	public function setDateFormat($dateFormat)
	{
		$this->uiDateFormat = $dateFormat;
	}

	/**
	 * @param string $LCID
	 * @return string
	 * @api
	 */
	public function getDateTimeFormat($LCID)
	{
		if ($this->uiDateTimeFormat)
		{
			return $this->uiDateTimeFormat;
		}
		return $this->transForLCID($LCID, 'c.date.default_datetime_format');
	}

	/**
	 * @param string $dateTimeFormat
	 * @api
	 */
	public function setDateTimeFormat($dateTimeFormat)
	{
		$this->uiDateTimeFormat = $dateTimeFormat;
	}

	/**
	 * @return \DateTimeZone
	 * @api
	 */
	public function getTimeZone()
	{
		if ($this->uiTimeZone)
		{
			return $this->uiTimeZone;
		}
		return new \DateTimeZone($this->getConfiguration()->getEntry('Change/I18n/default-timezone'));
	}

	/**
	 * @param \DateTimeZone|string $timeZone
	 * @api
	 */
	public function setTimeZone($timeZone)
	{
		if (!($timeZone instanceof \DateTimeZone))
		{
			$timeZone = new \DateTimeZone($timeZone);
		}
		$this->uiTimeZone = $timeZone;
	}

	/**
	 * @param \DateTime $gmtDate
	 * @return string
	 * @api
	 */
	public function transDate(\DateTime $gmtDate)
	{
		$LCID = $this->getLCID();
		return $this->formatDate($LCID, $gmtDate, $this->getDateFormat($LCID));
	}

	/**
	 * @param \DateTime $gmtDate
	 * @return string
	 * @api
	 */
	public function transDateTime(\DateTime $gmtDate)
	{
		$LCID = $this->getLCID();
		return $this->formatDate($LCID, $gmtDate, $this->getDateTimeFormat($LCID));
	}

	/**
	 * Format a date.
	 * @param string $LCID
	 * @param \DateTime $gmtDate
	 * @param string $format using this syntax: http://userguide.icu-project.org/formatparse/datetime
	 * @param \DateTimeZone $timeZone
	 * @return string
	 * @api
	 */
	public function formatDate($LCID, \DateTime $gmtDate, $format, $timeZone = null)
	{
		if (!$timeZone)
		{
			$timeZone = $this->getTimeZone();
		}
		$tmpDate = clone $gmtDate; // To not alter $gmtDate.
		$dateFormatter = new \IntlDateFormatter($LCID, null, null, $timeZone->getName(), \IntlDateFormatter::GREGORIAN, $format);
		return $dateFormatter->format($this->toLocalDateTime($tmpDate));
	}

	/**
	 * @param string $date
	 * @return \DateTime
	 * @api
	 */
	public function getGMTDateTime($date)
	{
		return new \DateTime($date, new \DateTimeZone('UTC'));
	}

	/**
	 * @param \DateTime $localDate
	 * @return \DateTime
	 * @api
	 */
	public function toGMTDateTime($localDate)
	{
		return $localDate->setTimezone(new \DateTimeZone('UTC'));
	}

	/**
	 * @param string $date
	 * @return \DateTime
	 * @api
	 */
	public function getLocalDateTime($date)
	{
		return new \DateTime($date, $this->getTimeZone());
	}

	/**
	 * @param \DateTime $localDate
	 * @return \DateTime
	 * @api
	 */
	public function toLocalDateTime($localDate)
	{
		return $localDate->setTimezone($this->getTimeZone());
	}

	// Number

	/**
	 * @param string $LCID
	 * @param integer|float $value
	 * @param integer $style
	 * @return string
	 */
	public function formatNumber($LCID, $value, $style = \NumberFormatter::DEFAULT_STYLE)
	{
		return (new \NumberFormatter($LCID, $style))->format($value);
	}

	/**
	 * @param string $LCID
	 * @param float $amount
	 * @param string $currency
	 * @return string
	 */
	public function formatCurrency($LCID, $amount, $currency)
	{
		return (new \NumberFormatter($LCID, \NumberFormatter::CURRENCY))->formatCurrency($amount, $currency);
	}

	/**
	 * @param integer|float $value
	 * @return string
	 * @api
	 */
	public function transNumber($value)
	{
		return $this->formatNumber($this->getLCID(), $value);
	}

	/**
	 * @param integer $value
	 * @return string
	 * @api
	 */
	public function transInteger($value)
	{
		return $this->formatNumber($this->getLCID(), (int)$value);
	}

	/**
	 * @param float $value
	 * @return string
	 * @api
	 */
	public function transFloat($value)
	{
		return $this->formatNumber($this->getLCID(), (float)$value, \NumberFormatter::DECIMAL);
	}

	/**
	 * @param float $amount
	 * @param string $currency
	 * @return string
	 * @api
	 */
	public function transCurrency($amount, $currency)
	{
		return $this->formatCurrency($this->getLCID(), $amount, $currency);
	}

	// File size.

	/**
	 * @param string $LCID
	 * @param string $fileSize
	 * @param string[] $formatters
	 * @return string
	 */
	public function formatFileSize($LCID, $fileSize, $formatters = [])
	{
		$units = ['bytes', 'kilobytes', 'megabytes', 'gigabytes', 'terabytes'];
		$value = $fileSize;
		$unitIndex = 0;
		while ($value >= 1024 && $unitIndex < count($units))
		{
			$unitIndex++;
			$value /= 1024.0;
		}

		$unitLabel = $this->transForLCID($LCID, 'c.filesize.' . $units[$unitIndex]);
		$unitAbbr = $this->transForLCID($LCID, 'c.filesize.' . $units[$unitIndex] . '_abbr');
		if ($unitLabel == $unitAbbr)
		{
			$text = (int)$value . ' ' . $unitAbbr;
		}
		else
		{
			$text = (int)$value . ' <abbr title="' . $this->transformUcf($unitLabel, $LCID) . '">' . $unitAbbr . '</abbr>';
		}

		if (count($formatters))
		{
			$text = $this->dispatchFormatting($text, $formatters, $LCID);
		}
		return $text;
	}

	/**
	 * @param string $fileSize
	 * @param string[] $formatters
	 * @return string
	 */
	public function transFileSize($fileSize, $formatters = [])
	{
		return $this->formatFileSize($this->getLCID(), $fileSize, $formatters);
	}

	// Transformers.

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformLab($text, $LCID)
	{
		return $text . (strpos($LCID, 'fr') === 0 ? ' :' : ':');
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformUc($text, $LCID)
	{
		return \Change\Stdlib\StringUtils::toUpper($text);
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformUcf($text, $LCID)
	{
		return \Change\Stdlib\StringUtils::ucfirst($text);
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformUcw($text, $LCID)
	{
		return mb_convert_case($text, MB_CASE_TITLE, 'UTF-8');
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformLc($text, $LCID)
	{
		return \Change\Stdlib\StringUtils::toLower($text);
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformJs($text, $LCID)
	{
		return str_replace(["\\", "\t", "\n", "\"", "'"], ["\\\\", "\\t", "\\n", "\\\"", "\\'"], $text);
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformHtml($text, $LCID)
	{
		return nl2br(htmlspecialchars($text, ENT_COMPAT, 'UTF-8'));
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformText($text, $LCID)
	{
		if ($text === null)
		{
			return '';
		}
		$text = str_replace(['</div>', '</p>', '</li>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>', '</tr>'],
			PHP_EOL, $text);
		/** @noinspection CascadeStringReplacementInspection */
		$text = str_replace(['</td>', '</th>'], "\t", $text);
		$text = preg_replace('/<li[^>]*>/', ' * ', $text);
		$text = preg_replace('/<br[^>]*>/', PHP_EOL, $text);
		$text = preg_replace('/<hr[^>]*>/', '------' . PHP_EOL, $text);
		$text = preg_replace(['/<a[^>]+href="([^"]+)"[^>]*>([^<]+)<\/a>/i', '/<img' . '[^>]+alt="([^"]+)"[^>]*\/>/i'],
			['$2 [$1]', PHP_EOL . "[$1]" . PHP_EOL], $text);
		$text = trim(html_entity_decode(strip_tags($text), ENT_QUOTES, 'UTF-8'));
		return $text;
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformAttr($text, $LCID)
	{
		return htmlspecialchars(str_replace(["\t", "\n"], ['&#09;', '&#10;'], $text), ENT_COMPAT, 'UTF-8');
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformSpace($text, $LCID)
	{
		return ' ' . $text . ' ';
	}

	/**
	 * @param string $text
	 * @param string $LCID
	 * @return string
	 */
	public function transformEtc($text, $LCID)
	{
		return $text . '...';
	}

	public function refreshCoreI18n()
	{
		$packageNames = $this->processCoreI18nFilesForLCID('fr_FR');
		foreach ($this->getSupportedLCIDs() as $LCID)
		{
			if ($LCID === 'fr_FR')
			{
				continue;
			}
			foreach ($packageNames as $packageName)
			{
				$this->getTranslationsForPackage($packageName, $LCID, true);
			}
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function refreshPluginI18n(\Change\Plugins\Plugin $plugin)
	{
		$defaultLCID = $plugin->getDefaultLCID();
		$packageNames = $this->processPluginI18nFilesForLCID($plugin, $defaultLCID);
		foreach ($this->getSupportedLCIDs() as $LCID)
		{
			if ($LCID === $defaultLCID)
			{
				continue;
			}
			foreach ($packageNames as $packageName)
			{
				$this->getTranslationsForPackage($packageName, $LCID, true);
			}
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string $locale
	 * @return array
	 */
	protected function getI18nFilePathsForPlugin(\Change\Plugins\Plugin $plugin, $locale)
	{
		$localeFilePattern = implode(DIRECTORY_SEPARATOR, [$plugin->getAssetsPath(), 'I18n', $locale, '*.json']);
		return \Zend\Stdlib\Glob::glob($localeFilePattern, \Zend\Stdlib\Glob::GLOB_NOESCAPE + \Zend\Stdlib\Glob::GLOB_NOSORT);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string $fileName
	 * @return string
	 */
	protected function getPluginI18nPackageNameByFilename(\Change\Plugins\Plugin $plugin, $fileName)
	{
		$fInfo = new \SplFileInfo($fileName);
		return implode('.', [
			$plugin->getType() == \Change\Plugins\Plugin::TYPE_MODULE ? 'm' : 't',
			\Change\Stdlib\StringUtils::toLower($plugin->getVendor()),
			\Change\Stdlib\StringUtils::toLower($plugin->getShortName()),
			substr(\Change\Stdlib\StringUtils::toLower($fInfo->getFilename()), 0, -5)
		]);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string $LCID
	 * @return array
	 */
	protected function processPluginI18nFilesForLCID(\Change\Plugins\Plugin $plugin, $LCID)
	{
		$packageNames = [];
		foreach ($this->getI18nFilePathsForPlugin($plugin, $LCID) as $filePath)
		{
			$fInfo = new \SplFileInfo($filePath);
			$packageName = $this->getPluginI18nPackageNameByFilename($plugin, $fInfo->getFilename());
			$this->getTranslationsForPackage($packageName, $LCID, true);
			$packageNames[] = $packageName;
		}
		return $packageNames;
	}

	/**
	 * @param string $LCID
	 * @return array
	 */
	protected function processCoreI18nFilesForLCID($LCID)
	{
		$packageNames = [];
		$localeFilePattern = $this->getWorkspace()->changePath('Assets', 'I18n', $LCID, '*.json');
		$glob = \Zend\Stdlib\Glob::glob($localeFilePattern, \Zend\Stdlib\Glob::GLOB_NOESCAPE + \Zend\Stdlib\Glob::GLOB_NOSORT);
		foreach ($glob as $filePath)
		{
			$fInfo = new \SplFileInfo($filePath);
			$packageName = 'c.' . substr($fInfo->getFilename(), 0, -5);
			$this->getTranslationsForPackage($packageName, $LCID, true);
			$packageNames[] = $packageName;
		}
		return $packageNames;
	}

	/**
	 * @param string $packageName
	 * @param string $LCID
	 * @param bool $refresh
	 * @return array|bool
	 */
	public function getTranslationsForPackage($packageName, $LCID, $refresh = false)
	{
		if (!in_array($LCID, $this->getSupportedLCIDs()))
		{
			return false;
		}

		if (!isset($this->loadedPackages[$LCID][$packageName]))
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['packageName' => $packageName, 'LCID' => $LCID, 'refresh' => $refresh]);
			$eventManager->trigger('getTranslationsForPackage', $this, $args);
			$package = $args['package'] ?? false;
			$this->loadedPackages[$LCID][$packageName] = $package;
		}
		return $this->loadedPackages[$LCID][$packageName];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetTranslationsPackageFromCache($event)
	{
		$packageName = $event->getParam('packageName');
		$LCID = $event->getParam('LCID');
		$refresh = $event->getParam('refresh');

		$dataCacheInfo = $this->buildCacheInfo($packageName, $LCID);
		$event->setParam('dataCacheInfo', $dataCacheInfo);
		if (!$refresh)
		{
			list($namespace, $key) = $dataCacheInfo;
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$package = $cacheManager->getEntry($namespace, $key);
			if ($package !== null)
			{
				if (is_array($package) && $event->getApplication()->getConfiguration()->inDevelopmentMode()
					&& $package['mTime'] != $this->getPackageModifiedTimeStamp($packageName, $LCID)
				)
				{
					return;
				}
				$event->setParam('cachedPackage', $package);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetTranslationsForPackage($event)
	{
		$cachedPackage = $event->getParam('cachedPackage');
		if ($cachedPackage !== null)
		{
			$event->setParam('package', $cachedPackage);
			return;
		}

		$packageName = $event->getParam('packageName');
		$LCID = $event->getParam('LCID');

		$parts = explode('.', $packageName);
		$originalFilePath = null;
		$defaultLCID = null;
		if ($parts[0] === 'c')
		{
			$originalFilePath = $this->getWorkspace()->changePath('Assets', 'I18n', $LCID, $parts[1] . '.json');
			$defaultLCID = 'fr_FR';
		}
		else
		{
			$plugin = $this->getPluginManager()->getPlugin($parts[0] === 'm' ? \Change\Plugins\Plugin::TYPE_MODULE :
				\Change\Plugins\Plugin::TYPE_THEME, $parts[1], $parts[2]);
			if ($plugin)
			{
				$defaultLCID = $plugin->getDefaultLCID() ?: null;
				$originalFilePath = implode(DIRECTORY_SEPARATOR, [$plugin->getAssetsPath(), 'I18n', $LCID, $parts[3] . '.json']);
			}
		}

		if (!$defaultLCID)
		{
			$event->setParam('package', false);
			return;
		}

		/** @var array $content */
		$content = [];
		if ($defaultLCID !== $LCID)
		{
			$synchro = $this->getI18nSynchro();
			if (isset($synchro[$LCID]))
			{
				$referenceLCID = is_array($synchro[$LCID]) ? $synchro[$LCID][0] : $synchro[$LCID];
				$content = $this->getTranslationsForPackage($packageName, $referenceLCID);
			}
			else
			{
				$content = $this->getTranslationsForPackage($packageName, $defaultLCID);
			}
			if (!is_array($content))
			{
				$event->setParam('package', false);
				return;
			}
		}

		$decoded = null;
		$mTime = $content['mTime'] ?? 0;
		if (file_exists($originalFilePath) && is_readable($originalFilePath))
		{
			$mTime = max(filemtime($originalFilePath), $mTime);
			$decoded = json_decode(file_get_contents($originalFilePath), JSON_OBJECT_AS_ARRAY);
			if (!is_array($decoded))
			{
				$this->getLogging()->error('Invalid I18n JSON file: ' . $originalFilePath);
			}
		}

		if (is_array($decoded))
		{
			foreach ($decoded as $key => $data)
			{
				$content[strtolower($key)] = $data['message'] ?? '';
			}
		}

		$overrideFilePath = $this->getWorkspace()->appPath('Override', 'I18n', $LCID, $packageName . '.json');
		if (file_exists($overrideFilePath) && is_readable($overrideFilePath))
		{
			$mTime = max(filemtime($overrideFilePath), $mTime);

			$decoded = json_decode(file_get_contents($overrideFilePath), JSON_OBJECT_AS_ARRAY);
			if (is_array($decoded))
			{
				foreach ($decoded as $key => $data)
				{
					$content[strtolower($key)] = $data['message'] ?? '';
				}
			}
			else
			{
				$this->getLogging()->error('Invalid I18n JSON file: ' . $overrideFilePath);
			}
		}
		$content['mTime'] = $mTime;
		$event->setParam('package', $content);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetTranslationsPackageInCache($event)
	{
		$package = $event->getParam('package');
		$cachedPackage = $event->getParam('cachedPackage');
		if ($package !== $cachedPackage)
		{
			$dataCacheInfo = $event->getParam('dataCacheInfo');
			list($namespace, $cacheKey) = $dataCacheInfo;
			$cacheManager = $event->getApplicationServices()->getCacheManager();
			$cacheManager->setEntry($namespace, $cacheKey, $package);
		}
	}

	/**
	 * @param string $packageName
	 * @param string $LCID
	 * @return int
	 */
	protected function getPackageModifiedTimeStamp($packageName, $LCID)
	{
		$parts = explode('.', $packageName);

		$filePath = null;
		$mTime = 0;

		if ($parts[0] === 'c')
		{
			$filePath = $this->getWorkspace()->changePath('Assets', 'I18n', $LCID, $parts[1] . '.json');
			if ($LCID !== 'fr_FR')
			{
				$mTime = $this->getPackageModifiedTimeStamp($packageName, 'fr_FR');
			}
		}
		else
		{
			$plugin = $this->getPluginManager()->getPlugin($parts[0] === 'm' ? \Change\Plugins\Plugin::TYPE_MODULE :
				\Change\Plugins\Plugin::TYPE_THEME, $parts[1], $parts[2]);
			if ($plugin)
			{
				$filePath = implode(DIRECTORY_SEPARATOR, [$plugin->getAssetsPath(), 'I18n', $LCID, $parts[3] . '.json']);
				$defaultLCID = $plugin->getDefaultLCID();
				if ($LCID !== $defaultLCID)
				{
					$mTime = $this->getPackageModifiedTimeStamp($packageName, $defaultLCID);
				}
			}
		}
		if (file_exists($filePath))
		{
			$mTime = max(filemtime($filePath), $mTime);
		}

		$overrideFilePath = $this->getWorkspace()->appPath('Override', 'I18n', $LCID, $packageName . '.json');

		if (file_exists($overrideFilePath))
		{
			$mTime = max(filemtime($overrideFilePath), $mTime);
		}

		return $mTime;
	}
}