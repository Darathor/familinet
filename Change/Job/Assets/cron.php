<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
require_once dirname(__DIR__, 3) . '/Change/Application.php';

$application = new \Change\Application();
$application->useSession(false);
$application->start();

class cron
{
	/**
	 * @var \Change\Events\EventManager
	 */
	protected $eventManager;

	/**
	 * @var \Zend\Log\Logger|null
	 */
	protected $logger;

	/**
	 * @var bool
	 */
	protected $logDebug = false;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct($application)
	{
		$this->eventManager = $application->getNewEventManager(['cron']);
		if ($application->getConfiguration('Change/Job/stop'))
		{
			return;
		}
		$logging = $application->getLogging();
		$priority = $logging->getPriority();
		if ($priority >= 5) //  >= NOTICE
		{
			$this->logDebug = $priority === 7;
			$this->logger = $logging->getLoggerByName('job');
		}

		$this->eventManager->attach('execute',  function($event) {$this->onExecutePrioritized($event);}, 10);
		$this->eventManager->attach('execute',  function($event) {$this->onExecute($event); }, 5);

		$this->eventManager->attach('executeJob', function ($event) { $this->onExecuteJob($event); }, 5);
	}

	public function execute($argv)
	{
		if ($argv && count($argv) === 2 && is_numeric($argv[1]))
		{
			$this->eventManager->trigger('executeJob', $this, ['jobId' => $argv[1]]);
			return;
		}
		$startDate = new \DateTime();
		$this->eventManager->trigger('execute', $this, ['startDate' => $startDate]);
	}

	protected function onExecutePrioritized(\Change\Events\Event $event)
	{
		$prioritizedJobNames = $event->getApplication()->getConfiguration('Change/Job/prioritized');
		if ($prioritizedJobNames && is_array($prioritizedJobNames))
		{
			if ($this->logger && $this->logDebug)
			{
				$this->logger->debug('Cron check (' . implode(', ', $prioritizedJobNames) . ') runnable jobs...');
			}
			$startDate = $event->getParam('startDate');
			$startTimestamp = $startDate->getTimestamp();
			$applicationServices = $event->getApplicationServices();
			$jobManager = $applicationServices->getJobManager();
			$runnableJobIds = $jobManager->getRunnableNamedJobIds($startDate, array_values($prioritizedJobNames));

			$maxExecutionTime = max(60, (int)$event->getApplication()->getConfiguration('Change/Job/maxExecutionTime'));
			if (count($runnableJobIds))
			{
				foreach($runnableJobIds as $jobId)
				{
					$this->executeJobId($jobManager, $jobId);

					/** @noinspection DisconnectedForeachInstructionInspection */
					$workingTime = (new \DateTime())->getTimestamp() - $startTimestamp;
					if ($workingTime >= $maxExecutionTime)
					{
						if ($this->logger)
						{
							$this->logger->info('Max Cron Working time: ' . $workingTime . 's');
						}
						$event->stopPropagation();
						break;
					}
				}
			}
		}
	}

	protected function onExecute(\Change\Events\Event $event)
	{
		$startDate = $event->getParam('startDate');
		$startTimestamp = $startDate->getTimestamp();

		if ($this->logger && $this->logDebug)
		{
			$this->logger->debug('Cron check runnable jobs...');
		}
		$applicationServices = $event->getApplicationServices();

		$jobManager = $applicationServices->getJobManager();
		$runnableJobIds = $jobManager->getRunnableJobIds($startDate);
		if (count($runnableJobIds))
		{
			$maxExecutionTime = max(60, (int)$event->getApplication()->getConfiguration('Change/Job/maxExecutionTime'));
			foreach($runnableJobIds as $jobId)
			{
				$this->executeJobId($jobManager, $jobId);

				/** @noinspection DisconnectedForeachInstructionInspection */
				$workingTime = (new \DateTime())->getTimestamp() - $startTimestamp;
				if ($workingTime >= $maxExecutionTime)
				{
					if ($this->logger)
					{
						$this->logger->info('Max Cron Working time: ' . $workingTime . 's');
					}
					break;
				}
			}
		}
	}

	protected function onExecuteJob(\Change\Events\Event $event)
	{
		$jobId = $event->getParam('jobId');

		$applicationServices = $event->getApplicationServices();
		$jobManager = $applicationServices->getJobManager();
		$this->executeJobId($jobManager, $jobId);
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @param int $jobId
	 */
	public function executeJobId(\Change\Job\JobManager $jobManager, $jobId)
	{
		$job = $jobManager->getJob($jobId);
		if ($job)
		{
			$startJobTimestamp = (new \DateTime())->getTimestamp();
			$startJobMemory = memory_get_usage(true);

			if ($this->logger)
			{
				$this->logger->info('Run: ' . $job->getName() . ' ' . $job->getId(), ['userId' => 0]);
			}


			$jobManager->run($job);

			$jobDuration = (new \DateTime())->getTimestamp() - $startJobTimestamp;
			$jobMemory = memory_get_usage(true) - $startJobMemory;
			$fmt = \NumberFormatter::create('en_US', \NumberFormatter::DECIMAL);
			$msg = implode(' ', ['Stats:', $job->getName(), $jobId, '-- PID:', getmypid(), 'Status:', $job->getStatus(), 'Duration:', $jobDuration . 's', 'Memory:',
				$fmt->format($jobMemory)]);
			if ($this->logger)
			{
				$this->logger->info($msg, ['userId' => 0]);
			}

		}
		else
		{
			if ($this->logger)
			{
				$this->logger->info('Invalid job: ' . $jobId);
			}
		}
	}

}
$cron = new cron($application);
$cron->execute($argv);
