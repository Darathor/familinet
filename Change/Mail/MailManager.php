<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Mail;

/**
 * @name \Change\Mail\MailManager
 */
class MailManager
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'MailManager';

	/**
	 * @var \Zend\Mail\Transport\TransportInterface
	 */
	protected $transport;

	/**
	 * @var \Change\Storage\StorageManager
	 */
	protected $storageManager;

	/**
	 * @return \Change\Storage\StorageManager
	 */
	protected function getStorageManager()
	{
		return $this->storageManager;
	}

	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return $this
	 */
	public function setStorageManager($storageManager)
	{
		$this->storageManager = $storageManager;
		return $this;
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	protected function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/MailManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('buildHtmlContent', function ($event) { $this->onBuildHtmlContent($event); }, 5);
	}

	/**
	 * @api
	 * @param array $from ['email@email.com', ['email'=>'email3@email.com'], 'email2@email.com', ['email'=>'email3@email.com', 'name' => 'My name']]
	 * @param array $to ['email@email.com', ['email'=>'email3@email.com'], 'email2@email.com', ['email'=>'email3@email.com', 'name' => 'My name']]
	 * @param string $subject
	 * @param string $HTMLBody
	 * @param string|null $txtBody
	 * @param array $cc ['email@email.com', ['email'=>'email3@email.com'], 'email2@email.com', ['email'=>'email3@email.com', 'name' => 'My
	 *     name']]
	 * @param array $bcc ['email@email.com', ['email'=>'email3@email.com'], 'email2@email.com', ['email'=>'email3@email.com', 'name' => 'My
	 *     name']]
	 * @param array $replyTo ['email@email.com', ['email'=>'email3@email.com'], 'email2@email.com', ['email'=>'email3@email.com', 'name' => 'My
	 *     name']]
	 * @param string $encoding
	 * @param array $attachFiles
	 * @return \Zend\Mail\Message
	 */
	public function prepareMessage($from, $to, $subject, $HTMLBody, $txtBody = null, array $cc = [], array $bcc = [],
		array $replyTo = [], $encoding = null, array $attachFiles = [])
	{
		$message = new \Zend\Mail\Message();

		foreach ($from as $value)
		{
			if (is_array($value))
			{
				if (array_key_exists('email', $value))
				{
					if (array_key_exists('name', $value))
					{
						$message->addFrom($value['email'], $value['name']);
					}
					else
					{
						$message->addFrom($value['email']);
					}
				}
			}
			else
			{
				$message->addFrom($value);
			}
		}

		foreach ($to as $value)
		{
			if (is_array($value))
			{
				if (array_key_exists('email', $value))
				{
					if (array_key_exists('name', $value))
					{
						$message->addTo($value['email'], $value['name']);
					}
					else
					{
						$message->addTo($value['email']);
					}
				}
			}
			else
			{
				$message->addTo($value);
			}
		}

		foreach ($cc as $value)
		{
			if (is_array($value))
			{
				if (array_key_exists('email', $value))
				{
					if (array_key_exists('name', $value))
					{
						$message->addCc($value['email'], $value['name']);
					}
					else
					{
						$message->addCc($value['email']);
					}
				}
			}
			else
			{
				$message->addCc($value);
			}
		}

		foreach ($bcc as $value)
		{
			if (is_array($value))
			{
				if (array_key_exists('email', $value))
				{
					if (array_key_exists('name', $value))
					{
						$message->addBcc($value['email'], $value['name']);
					}
					else
					{
						$message->addBcc($value['email']);
					}
				}
			}
			else
			{
				$message->addBcc($value);
			}
		}

		foreach ($replyTo as $value)
		{
			if (is_array($value))
			{
				if (array_key_exists('email', $value))
				{
					if (array_key_exists('name', $value))
					{
						$message->addReplyTo($value['email'], $value['name']);
					}
					else
					{
						$message->addReplyTo($value['email']);
					}
				}
			}
			else
			{
				$message->addReplyTo($value);
			}
		}

		if ($encoding !== null)
		{
			$message->setEncoding($encoding);
		}

		$message->setSubject($subject);
		$parts = [];

		$htmlPart = new \Zend\Mime\Part($HTMLBody);
		$htmlPart->type = \Zend\Mime\Mime::TYPE_HTML;
		$htmlPart->charset = 'utf-8';
		$htmlPart->setEncoding(\Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE);

		if ($txtBody)
		{
			$textPart = new \Zend\Mime\Part($txtBody);
			$textPart->type = \Zend\Mime\Mime::TYPE_TEXT;
			$textPart->charset = 'utf-8';

			$alternatives = new \Zend\Mime\Message();
			$alternatives->setParts([$textPart, $htmlPart]);
			$alternativesPart = new \Zend\Mime\Part($alternatives->generateMessage());
			$alternativesPart->type = \Zend\Mime\Mime::MULTIPART_ALTERNATIVE;
			$alternativesPart->setBoundary($alternatives->getMime()->boundary());
			$parts[] = $alternativesPart;
		}
		else
		{
			$parts[] = $htmlPart;
		}

		if ($attachFiles)
		{
			foreach ($attachFiles as $file => $mimeType)
			{
				if (file_exists($file))
				{
					$filePart = new \Zend\Mime\Part(fopen($file, 'r'));
					$filePart->type = $mimeType;
					$filePart->filename = basename($file);
					$filePart->setEncoding(\Zend\Mime\Mime::ENCODING_BASE64);
					$filePart->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
					$parts[] = $filePart;
				}
				else
				{
					$this->getLogging()->warn(get_class($this) . ' try to send not existing file (' . $file . ')');
				}
			}
		}
		// then add them to a MIME message
		$mimeMessage = new \Zend\Mime\Message();
		$mimeMessage->setParts($parts);

		$message->setBody($mimeMessage);

		return $message;
	}

	/**
	 * @api
	 * @param \Zend\Mail\Message $message
	 */
	public function send($message)
	{
		$this->prepareFakeMail($message);

		$transport = $this->getTransport();
		$transport->send($message);
	}

	/**
	 * @param \Zend\Mail\Transport\TransportInterface $transport
	 * @return $this
	 */
	public function setTransport(\Zend\Mail\Transport\TransportInterface $transport)
	{
		$this->transport = $transport;
		return $this;
	}

	/**
	 * @return \Zend\Mail\Transport\TransportInterface
	 */
	protected function getTransport()
	{
		if ($this->transport === null)
		{
			$configuration = $this->getConfiguration();
			$type = strtolower($configuration->getEntry('Change/Mail/type'));
			switch ($type)
			{
				case 'smtp' :
					$transport = new \Zend\Mail\Transport\Smtp();
					$options = [
						'name' => $configuration->getEntry('Change/Mail/host', 'localhost'),
						'host' => $configuration->getEntry('Change/Mail/host', '127.0.0.1'),
						'port' => $configuration->getEntry('Change/Mail/port', 25)
					];

					if ($configuration->getEntry('Change/Mail/auth') == 'true')
					{
						$options['connection_class'] = $configuration->getEntry('Change/Mail/connection', 'login');
						$options['connection_config'] = [
							'username' => $configuration->getEntry('Change/Mail/username'),
							'password' => $configuration->getEntry('Change/Mail/password'),
						];
						$ssl = $configuration->getEntry('Change/Mail/ssl');
						if ($ssl !== null)
						{
							$options['connection_config']['ssl'] = $ssl;
						}
					}

					$transport->setOptions(new \Zend\Mail\Transport\SmtpOptions($options));
					break;
				case 'sendmail' :
					$transport = new \Zend\Mail\Transport\Sendmail($configuration->getEntry('Change/Mail/parameters'));
					break;

				case 'proximis' :
					$transport = new ProximisTransport($configuration->getEntry('Change/Mail'), $this->getApplication());
					break;

				default:
					$transport = new \Zend\Mail\Transport\InMemory();
			}
			$this->transport = $transport;
		}
		return $this->transport;
	}

	/**
	 * @param \Zend\Mail\Message $message
	 * @return \Zend\Mail\Message
	 */
	protected function prepareFakeMail($message)
	{
		$configuration = $this->getConfiguration();
		$fakeMail = $this->getApplication()->checkDevValue($configuration->getEntry('Change/Mail/fakemail'));

		if ($fakeMail !== null)
		{
			$currentCc = $message->getCc();
			$currentBcc = $message->getBcc();
			$currentTo = $message->getTo();
			$currentSubject = $message->getSubject();

			$newSubject = '[FAKE] ' . $currentSubject . ' [To : ';
			$i = 0;
			foreach ($currentTo as $to)
			{
				if ($i > 0)
				{
					$newSubject .= ', ';
				}
				$newSubject .= $to->getEmail();
				++$i;
			}
			$newSubject .= ']';

			$i = 0;
			$newCc = '';
			foreach ($currentCc as $cc)
			{
				if ($i > 0)
				{
					$newCc .= ', ';
				}
				$newCc .= $cc->getEmail();
				++$i;
			}
			if ($newCc != '')
			{
				$newSubject .= '[Cc : ' . $newCc . ']';
			}

			$i = 0;
			$newBcc = '';
			foreach ($currentBcc as $bcc)
			{
				if ($i > 0)
				{
					$newBcc .= ', ';
				}
				$newBcc .= $bcc->getEmail();
				++$i;
			}
			if ($newBcc != '')
			{
				$newSubject .= '[Bcc : ' . $newBcc . ']';
			}

			$message->setSubject($newSubject);
			$message->setTo($fakeMail);
			$message->setCc([]);
			$message->setBcc([]);
		}

		return $message;
	}

	/**
	 * @api
	 * @param \Change\Mail\MailInterface $mail
	 * @param \Change\Presentation\Interfaces\Website|null $website
	 * @param string $LCID
	 * @param array $substitutions
	 * @return string
	 */
	public function buildHtmlContent($mail, $website, $LCID, $substitutions)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['mail' => $mail, 'website' => $website, 'LCID' => $LCID, 'substitutions' => $substitutions,
			'html' => null
		]);
		$eventManager->trigger('buildHtmlContent', $this, $args);
		return $args['html'] ?? '';
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildHtmlContent($event)
	{
		$mail = $event->getParam('mail');
		$LCID = $event->getParam('LCID');
		$website = $event->getParam('website');
		$substitutions = $event->getParam('substitutions');
		if ($mail instanceof \Change\Mail\MailInterface && $website instanceof \Change\Presentation\Interfaces\Website
			&& is_string($LCID) && is_array($substitutions))
		{
			$mailTemplate = $mail->getTemplate();
			if (!($mailTemplate instanceof \Change\Presentation\Interfaces\Template))
			{
				return;
			}

			$applicationServices = $event->getApplicationServices();
			$application = $event->getApplication();
			$templateManager = $applicationServices->getTemplateManager();
			$result = new \Change\Http\Web\Result\Page($mail->getIdentifier());


			$templateLayout = $mailTemplate->getContentLayout($website->getId());

			$themeManager = $applicationServices->getThemeManager();
			$themeManager->setCurrent($mailTemplate->getTheme());

			$mailLayout = $mail->getContentLayout();
			$containers = [];
			foreach ($templateLayout->getItems() as $item)
			{
				if ($item instanceof \Change\Presentation\Layout\Container)
				{
					$container = $mailLayout->getById($item->getId());
					if ($container)
					{
						$containers[] = $container;
					}
				}
			}
			$mailLayout->setItems($containers);

			$blocks = array_merge($templateLayout->getBlocks(), $mailLayout->getBlocks());

			if (count($blocks))
			{
				$blockManager = $applicationServices->getBlockManager();

				$httpWebEvent = new \Change\Http\Web\Event(null, null, $event->getParams());
				$httpWebEvent->setUrlManager($website->getUrlManager($LCID));

				$blockInputs = [];
				foreach ($blocks as $block)
				{
					/* @var $block \Change\Presentation\Layout\Block */
					$information = $blockManager->getBlockInformation($block->getName());
					if ($information && $information->isMailSuitable())
					{
						$blockParameter = $blockManager->getParameters($block, $httpWebEvent);
						$blockInputs[] = [$block, $blockParameter];
					}
				}

				$blockResults = [];
				foreach ($blockInputs as $infos)
				{
					list($blockLayout, $parameters) = $infos;

					/* @var $blockLayout \Change\Presentation\Layout\Block */
					$blockResult = $blockManager->getResult($blockLayout, $parameters, $httpWebEvent);
					if (isset($blockResult))
					{
						$blockResults[$blockLayout->getId()] = $blockResult;
					}
				}
				$result->setBlockResults($blockResults);
			}

			$cacheTime = 0;
			if ($modificationDate = $mail->getModificationDate())
			{
				$cacheTime = $modificationDate->getTimestamp();
			}
			if (($modificationDate = $mailTemplate->getModificationDate()) && $modificationDate->getTimestamp() > $cacheTime)
			{
				$cacheTime = $modificationDate->getTimestamp();
			}
			$cacheTime = $cacheTime ?: (new \DateTime())->getTimestamp();

			$cachePath = $application->getWorkspace()->cachePath('twig', 'mail', $result->getIdentifier() . ',' . $cacheTime . '.twig');
			if ($application->inDevelopmentMode() && file_exists($cachePath))
			{
				unlink($cachePath);
			}

			if (!file_exists($cachePath))
			{
				$mailHtml = new \Change\Presentation\Layout\MailHtml();
				$callableTwigBlock = function (\Change\Presentation\Layout\Block $item) use ($mailHtml)
				{
					return '{{ pageResult.htmlBlock(\'' . $item->getId() . '\', ' . var_export($mailHtml->getBlockClass($item), true) . ')|raw }}';
				};
				$twigLayout = $mailHtml->getHtmlParts($templateLayout, $mailLayout, $callableTwigBlock);

				$htmlTemplate = str_replace(array_keys($twigLayout), array_values($twigLayout), $mailTemplate->getHtml());

				\Change\Stdlib\FileUtils::write($cachePath, $htmlTemplate);
				touch($cachePath, $cacheTime);
			}

			$html = $templateManager->renderTemplateFile($cachePath, ['pageResult' => $result]);
			$html = preg_replace('/[\s]+/', ' ', $html);
			$event->setParam('html', \Change\Stdlib\StringUtils::getSubstitutedString($html, $substitutions));
		}
	}
}