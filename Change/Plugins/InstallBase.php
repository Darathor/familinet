<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins;

/**
 * @name \Change\Plugins\InstallBase
 */
class InstallBase
{
	/**
	 * @var \Change\Plugins\Plugin
	 */
	protected $plugin;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var \Zend\Stdlib\Parameters|null
	 */
	protected $services;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param \Change\Plugins\Plugin $plugin
	 * @return callable[]
	 */
	public function attach($events, $plugin)
	{
		$this->setPlugin($plugin);
		$callable = [];
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_INITIALIZE, [$this, 'onSetupInitialize'], 5);
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_APPLICATION, [$this, 'onSetupApplication'], 5);
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_DB_SCHEMA, [$this, 'onSetupDbSchema'], 5);
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_SERVICES, [$this, 'onSetupServices'], 5);
		$callable[] = $events->attach(PluginManager::EVENT_SETUP_FINALIZE, [$this, 'onSetupFinalize'], 5);
		return $callable;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param Plugin $plugin
	 * @return boolean
	 */
	public function isValid(\Change\Events\Event $event, \Change\Plugins\Plugin $plugin)
	{
		return $event->getParam('plugin') === $plugin;
	}

	/**
	 * @return \Change\Plugins\Plugin
	 */
	public function getPlugin()
	{
		return $this->plugin;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @return $this
	 */
	public function setPlugin($plugin)
	{
		$this->plugin = $plugin;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	public function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return null|\Zend\Stdlib\Parameters
	 */
	public function getServices()
	{
		return $this->services;
	}

	/**
	 * @param null|\Zend\Stdlib\Parameters $services
	 * @return $this
	 */
	public function setServices($services)
	{
		$this->services = $services;
		return $this;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupInitialize(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			$this->initialize($plugin);
			if ($plugin->isModule() && \in_array('Application', $event->getParam('steps'), true) && \is_dir($plugin->getThemeAssetsPath()))
			{
				$pluginManager = $event->getTarget();
				if (($pluginManager instanceof \Change\Plugins\PluginManager) && $themes = $pluginManager->getThemes())
				{
					$extraPlugins = $event->getParam('extraPlugins');
					foreach ($themes as $theme)
					{
						if (!\in_array($theme, $extraPlugins, true))
						{
							$extraPlugins[] = $theme;
						}
					}
					$event->setParam('extraPlugins', $extraPlugins);
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupApplication(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);

			/* @var $app \Change\Application */
			$app = $event->getApplication();
			if ($event->getParam('configuration'))
			{
				$this->executeApplication($plugin, $app, $app->getConfiguration());
			}
			else
			{
				$this->executeThemeAssets($plugin, $pluginManager, $app);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupDbSchema(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);

			$this->executeDbSchema($plugin, $event->getApplicationServices()->getDbProvider()->getSchemaManager());
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupServices(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			/** @var \Change\Plugins\PluginManager $pluginManager */
			$pluginManager = $event->getTarget();
			$this->setPluginManager($pluginManager);
			$this->setServices($event->getServices());
			$this->executeServices($plugin, $event->getApplicationServices());
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupFinalize(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			$this->finalize($plugin);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function initialize($plugin)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{

	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function executeServices($plugin, $applicationServices)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function finalize($plugin)
	{
	}
}