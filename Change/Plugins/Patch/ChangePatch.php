<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins\Patch;

/**
 * @name \Change\Plugins\Patch\ChangePatch
 */
class ChangePatch extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onApply(\Change\Events\Event $event)
	{
		//Change_0001 v1.1 Update publication and activation default dates.

		//Change_0002 v2.0 Migrate filters.
		$this->executePatch('Change_0002', 'Migrate filters.', [$this, 'patch0002']);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function patch0002(\Change\Plugins\Patch\Patch $patch)
	{
		$dbProvider = $this->applicationServices->getDbProvider();
		if (!$dbProvider->getSchemaManager()->getTableDefinition('change_document_filters'))
		{
			$this->sendInfo('No table change_document_filters found, so no filter to migrate.');
			$patch->addInstallationData('success', 'No table change_document_filters found, so no filter to migrate.');
			// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
			$this->patchManager->installedPatch($patch);
			return;
		}

		if (!$dbProvider->getSchemaManager()->getTableDefinition('change_filters'))
		{
			throw new \RuntimeException('Database schema is not properly installed.', 99999);
		}

		$migratedRows = 0;
		$transactionManager = $this->applicationServices->getTransactionManager();

		try
		{
			$transactionManager->begin();

			$qb = $dbProvider->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select()->from($fb->table('change_document_filters'));
			$qb->addColumn($fb->column('filter_id'));
			$qb->addColumn($fb->column('model_name'));
			$qb->addColumn($fb->column('label'));
			$qb->addColumn($fb->column('content'));
			$qb->addColumn($fb->column('user_id'));
			$select = $qb->query();

			$stmt = $dbProvider->getNewStatementBuilder();
			$fb = $stmt->getFragmentBuilder();
			$stmt->insert($fb->table('change_filters'), 'filter_id', 'context_name', 'label', 'content', 'user_id');
			$insert = $stmt->insertQuery();
			$insert->setSelectQuery($select);
			$migratedRows = $insert->execute();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$patch->setException($e);
			$transactionManager->rollBack($e);
		}

		$patch->addInstallationData('migratedFilters', $migratedRows);
		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}