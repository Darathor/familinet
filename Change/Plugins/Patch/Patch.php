<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins\Patch;

/**
 * @name \Change\Plugins\Patch\Patch
 */
class Patch
{
	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var \DateTime|null
	 */
	protected $installDate;

	/**
	 * @var array
	 */
	protected $installationData = [];

	/**
	 * @param $code
	 */
	public function __construct($code)
	{
		$this->code = $code;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param string $code
	 * @return $this
	 */
	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}

	/**
	 * @return \DateTime|null
	 */
	public function getInstallDate()
	{
		return $this->installDate;
	}

	/**
	 * @return boolean
	 */
	public function installed()
	{
		return $this->installDate !== null;
	}

	/**
	 * @param \DateTime|null $installDate
	 * @return $this
	 */
	public function setInstallDate(\DateTime $installDate)
	{
		$this->installDate = $installDate;
		return $this;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->installationData['description'] = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->installationData['description'] ?? null;
	}

	/**
	 * @param array $installationData
	 * @return $this
	 */
	public function setInstallationData(array $installationData)
	{
		$this->installationData = $installationData;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getInstallationData()
	{
		return $this->installationData;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @return $this
	 */
	public function addInstallationData($key, $value)
	{
		if ($key && is_string($key))
		{
			if ($value === null)
			{
				unset($this->installationData[$key]);
			}
			else
			{
				$this->installationData[$key] = $value;
			}
		}
		return $this;
	}

	/**
	 * @param \Exception $exception
	 * @return $this
	 */
	public function setException(\Exception $exception = null)
	{
		return $this->addInstallationData('exception', $exception);
	}

	/**
	 * @param string $error
	 * @return $this
	 */
	public function setError(string $error = null)
	{
		return $this->addInstallationData('error', $error);
	}
}