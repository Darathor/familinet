<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins\Patch;

/**
 * @api Patch manager
 * @name \Change\Plugins\Patch\PatchManager
 */
class PatchManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'PatchManager';

	const EVENT_APPLY = 'apply';

	/**
	 * @var \Change\Plugins\Patch\Patch[]
	 */
	protected $patchCollection = [];


	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/' . static::EVENT_MANAGER_IDENTIFIER);
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getPatch', function (\Change\Events\Event $event)
		{
			$this->onDefaultGetPatch($event);
		}, 5);
		$eventManager->attach('setPatch', function (\Change\Events\Event $event)
		{
			$this->onDefaultSetPatch($event);
		}, 5);

		(new \Change\Plugins\Patch\ChangePatch())->attach($eventManager, 1);
	}

	/**
	 * @api apply all registered patch
	 * @param boolean $run
	 * @return \Change\Plugins\Patch\Patch[]
	 */
	public function apply($run = false)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['run' => $run, 'patchInstalled' => new \ArrayObject()]);
		$eventManager->trigger(static::EVENT_APPLY, $this, $args);

		$patchInstalled = [];
		foreach ($args['patchInstalled'] as $patch)
		{
			if ($patch instanceof \Change\Plugins\Patch\Patch)
			{
				$patchInstalled[] = $patch;
			}
		}
		return $patchInstalled;
	}

	/**
	 * @api
	 * @param string $code
	 * @throw \RuntimeException
	 * @return \Change\Plugins\Patch\Patch
	 */
	public function getPatch($code)
	{
		if (!$code || !is_string($code))
		{
			throw new \RuntimeException('Invalid patch code', 999999);
		}
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['code' => $code]);
		$eventManager->trigger('getPatch', $this, $args);
		$patch = $args['patch'] ?? null;
		if (!($patch instanceof \Change\Plugins\Patch\Patch))
		{
			$patch = new \Change\Plugins\Patch\Patch($code);
		}
		return $patch;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultGetPatch(\Change\Events\Event $event)
	{
		$code = $event->getParam('code');
		if (array_key_exists($code, $this->patchCollection))
		{
			if ($this->patchCollection[$code])
			{
				$event->setParam('patch', $this->patchCollection[$code]);
			}
			return;
		}

		$patch = null;
		$applicationServices = $event->getApplicationServices();
		$queryBuilder = $applicationServices->getDbProvider()->getNewQueryBuilder('PatchManager::onDefaultGetPatch');
		if (!$queryBuilder->isCached())
		{
			$fb = $queryBuilder->getFragmentBuilder();
			$queryBuilder->select($fb->column('install_date'), $fb->column('installation_data'))
				->from($fb->table('change_patch'))
				->where($fb->eq($fb->column('code'), $fb->parameter('code')));
		}
		$select = $queryBuilder->query();
		$select->bindParameter('code', $code);
		$patchData = $select->getFirstResult($select->getRowsConverter()->addDtCol('install_date')
			->addTxtCol('installation_data'));
		if ($patchData)
		{
			$patch = new \Change\Plugins\Patch\Patch($code);
			$patch->setInstallDate($patchData['install_date']);
			if ($patchData['installation_data'])
			{
				$installationData = json_decode($patchData['installation_data'], true);
				if (is_array($installationData))
				{
					$patch->setInstallationData($installationData);
				}
			}
			$event->setParam('patch', $patch);
		}
		$this->patchCollection[$code] = $patch;
	}

	/**
	 * @api insert patch in Db
	 * @param \Change\Plugins\Patch\Patch $patch
	 */
	public function installedPatch(\Change\Plugins\Patch\Patch $patch)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['patch' => $patch]);
		$eventManager->trigger('setPatch', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onDefaultSetPatch(\Change\Events\Event $event)
	{
		/** @var \Change\Plugins\Patch\Patch $patch */
		$patch = $event->getParam('patch');
		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$queryBuilder = $applicationServices->getDbProvider()->getNewStatementBuilder();

			$fb = $queryBuilder->getFragmentBuilder();
			$queryBuilder->insert($fb->table('change_patch'))
				->addColumns($fb->column('code'), $fb->column('install_date'), $fb->column('installation_data'))
				->addValues($fb->parameter('code'), $fb->dateTimeParameter('installDate'), $fb->lobParameter('installationData'));

			$insert = $queryBuilder->insertQuery();
			$code = $patch->getCode();
			$insert->bindParameter('code', $code);

			$installDate = $patch->getInstallDate();
			if ($installDate === null)
			{
				$installDate = new \DateTime();
			}
			$insert->bindParameter('installDate', $installDate);

			$installationData = $patch->getInstallationData();
			if ($installationData)
			{
				$installationData = json_encode($installationData);
			}
			else
			{
				$installationData = null;
			}

			$insert->bindParameter('installationData', $installationData);
			$event->setParam('result', $insert->execute());

			$patch->setInstallDate($installDate);
			$this->patchCollection[$code] = $patch;

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}