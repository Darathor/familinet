<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change\Plugins;

use Change\Stdlib\FileUtils;
use Zend\Stdlib\Glob;

/**
 * @api
 * @name \Change\Plugins\PluginManager
 */
class PluginManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'Plugin';

	const EVENT_SETUP_INITIALIZE = 'setupInitialize';
	const EVENT_SETUP_APPLICATION = 'setupApplication';
	const EVENT_SETUP_DB_SCHEMA = 'setupDbSchema';
	const EVENT_SETUP_SERVICES = 'setupServices';
	const EVENT_SETUP_FINALIZE = 'setupFinalize';
	const EVENT_SETUP_SUCCESS = 'setupSuccess';

	const EVENT_TYPE_PACKAGE = 'package';
	const EVENT_TYPE_MODULE = 'module';
	const EVENT_TYPE_THEME = 'theme';

	/**
	 * @var Plugin[]
	 */
	protected $plugins;

	/**
	 * @var array
	 */
	protected $pluginsConfig;

	/**
	 * @return \Change\Workspace
	 */
	protected function getWorkspace()
	{
		return $this->getApplication()->getWorkspace();
	}

	/**
	 * @return string
	 */
	protected function getCompiledPluginsPath()
	{
		return $this->getWorkspace()->compilationPath('Change', 'plugins.php');
	}

	/**
	 * @api
	 * @return \Change\Plugins\Plugin[]
	 */
	public function scanPlugins()
	{
		$plugins = [];

		// Plugin Modules.
		$pluginsModulesPattern = $this->getWorkspace()->pluginsModulesPath('*', '*', 'plugin.json');
		foreach (Glob::glob($pluginsModulesPattern, Glob::GLOB_NOESCAPE + Glob::GLOB_NOSORT) as $filePath)
		{
			$plugin = $this->getNewPlugin($filePath);
			if ($plugin)
			{
				$plugins[] = $plugin;
			}
		}

		// Project modules.
		$projectModulesPattern = $this->getWorkspace()->projectModulesPath('Project', '*', 'plugin.json');
		foreach (Glob::glob($projectModulesPattern, Glob::GLOB_NOESCAPE + Glob::GLOB_NOSORT) as $filePath)
		{
			$plugin = $this->getNewPlugin($filePath);
			if ($plugin)
			{
				$plugins[] = $plugin;
			}
		}

		// Plugin themes.
		$projectThemesPattern = $this->getWorkspace()->pluginsThemesPath('*', '*', 'plugin.json');
		foreach (Glob::glob($projectThemesPattern, Glob::GLOB_NOESCAPE + Glob::GLOB_NOSORT) as $filePath)
		{
			$plugin = $this->getNewPlugin($filePath, Plugin::TYPE_THEME);
			if ($plugin)
			{
				$plugins[] = $plugin;
			}
		}

		// Project themes.
		$projectThemesPattern = $this->getWorkspace()->projectThemesPath('Project', '*', 'plugin.json');
		foreach (Glob::glob($projectThemesPattern, Glob::GLOB_NOESCAPE + Glob::GLOB_NOSORT) as $filePath)
		{
			$plugin = $this->getNewPlugin($filePath, Plugin::TYPE_THEME);
			if ($plugin)
			{
				$plugins[] = $plugin;
			}
		}
		return $plugins;
	}

	/**
	 * @api
	 * @param boolean $checkRegistered
	 * @param boolean $resetAutoload
	 * @return \Change\Plugins\Plugin[]
	 */
	public function compile($checkRegistered = true, $resetAutoload = true)
	{
		$plugins = $this->scanPlugins();
		if ($checkRegistered)
		{
			$plugins = $this->loadRegistration($plugins);
		}
		else
		{
			$now = new \DateTime('now', new \DateTimeZone('UTC'));
			foreach ($plugins as $plugin)
			{
				$plugin->setActivated(true);
				$plugin->setConfigured(true);
				$plugin->setRegistrationDate($now);
			}
		}

		$this->plugins = $plugins;

		$code[] = '<?php';
		$code[] = 'return [';
		foreach ($plugins as $plugin)
		{
			$rd = $plugin->getRegistrationDate();
			$rd = $rd ? $rd->format(\DateTime::ATOM) : null;
			$code[] = '(new \Change\Plugins\Plugin(' . var_export($plugin->getType(), true) . ', ' . var_export($plugin->getVendor(), true) . ', '
				. var_export($plugin->getShortName(), true) . '))->setPackage(' . var_export($plugin->getPackage(), true) . ')->setRegistrationDate('
				. ($rd ? 'new \DateTime(\'' . $rd . '\')' : 'null') . ')->setConfigured(' . var_export($plugin->getConfigured(), true)
				. ')->setActivated(' . var_export($plugin->getActivated(), true) . ')->setDefaultLCID(' . var_export($plugin->getDefaultLCID(), true)
				. ')->setExtends(' . var_export($plugin->getExtends(), true)
				. ')->setConfiguration(' . var_export($plugin->getConfiguration(), true) . '),';
		}
		$code[] = '];';
		\Change\Stdlib\FileUtils::write($this->getCompiledPluginsPath(), implode(PHP_EOL, $code));

		return $plugins;
	}

	/**
	 * @api
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getUnregisteredPlugins()
	{
		$allPlugins = $this->scanPlugins();
		$registered = $this->loadRegistration($allPlugins);
		return array_values(array_filter($allPlugins, function (Plugin $p) use ($registered)
		{
			foreach ($registered as $rp)
			{
				if ($p->eq($rp))
				{
					return false;
				}
			}
			return true;
		}));
	}

	/**
	 * @param string $filePath
	 * @param string $type
	 * @return \Change\Plugins\Plugin|null
	 */
	protected function getNewPlugin($filePath, $type = Plugin::TYPE_MODULE)
	{
		$config = json_decode(file_get_contents($filePath), true);
		if (json_last_error() !== JSON_ERROR_NONE || !isset($config['vendor'], $config['name']))
		{
			return null;
		}
		$parts = explode(DIRECTORY_SEPARATOR, $filePath);
		$partsCount = count($parts);

		$vendor = $this->normalizeVendorName($config['vendor']);
		$shortName = $this->normalizePluginName($config['name']);

		$folderName = $parts[$partsCount - 3];
		if ($vendor === 'Project')
		{
			$folderName = $parts[$partsCount - 4];
			if ($folderName !== ($type === Plugin::TYPE_MODULE ? 'Modules' : 'Themes'))
			{
				return null;
			}
		}
		elseif ($vendor !== $folderName)
		{
			return null;
		}

		$folderName = $parts[$partsCount - 2];
		if ($shortName !== $folderName)
		{
			return null;
		}

		$plugin = new Plugin($type, $vendor, $shortName);
		$plugin->setWorkspace($this->getWorkspace());

		$plugin->setPackage($config['package'] ?? null);
		$plugin->setDefaultLCID($config['defaultLCID'] ?? null);
		$plugin->setExtends($config['extends'] ?? null);
		$plugin->setConfiguration($config['configuration'] ?? []);

		return $plugin;
	}

	/**
	 * @api
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getPlugins()
	{
		if ($this->plugins === null)
		{
			$this->plugins = [];
			$compiledPluginsPath = $this->getCompiledPluginsPath();
			if (is_readable($compiledPluginsPath))
			{
				$plugins = require $compiledPluginsPath;
				foreach ($plugins as $plugin)
				{
					/** @var $plugin Plugin */
					$plugin->setWorkspace($this->getWorkspace());
					$this->plugins[] = $plugin;
				}
			}
		}
		return $this->plugins;
	}

	/**
	 * @api
	 */
	public function reset()
	{
		if ($this->plugins)
		{
			foreach ($this->plugins as $i => &$plugin)
			{
				$plugin->setWorkspace(null);
				unset($this->plugins[$i]);
			}
			unset($plugin);
		}
		$this->plugins = null;
		$this->pluginsConfig = null;
	}

	/**
	 * @api
	 */
	public function unsetPluginConfig()
	{
		$pluginDataPath = $this->getWorkspace()->appPath('Config', 'plugins.json');
		if (file_exists($pluginDataPath))
		{
			unlink($pluginDataPath);
		}
		$this->reset();
	}

	/**
	 * @api
	 * @param Plugin $plugin
	 */
	public function register(Plugin $plugin)
	{
		$registrationDate = new \DateTime('now', new \DateTimeZone('UTC'));
		$plugin->setRegistrationDate($registrationDate);

		$registered = $this->readConfiguration();

		$k = $this->getPluginKey($plugin);

		$registered[$k]['type'] = $plugin->getType();
		$registered[$k]['vendor'] = $plugin->getVendor();
		$registered[$k]['name'] = $plugin->getShortName();
		$registered[$k]['package'] = $plugin->getPackage();
		$registered[$k]['activated'] = $plugin->getActivated();
		$registered[$k]['configured'] = $plugin->getConfigured();
		$registered[$k]['registrationDate'] = $registrationDate->format('Y-m-d H:i:s e');
		$registered[$k]['configuration'] = $plugin->getConfiguration() ?: null;

		$this->writeConfiguration($registered);
		if ($this->plugins !== null)
		{

			$this->plugins = array_values(array_filter($this->plugins, function (Plugin $p) use ($plugin)
			{
				return !$p->eq($plugin);
			}));
		}

		$this->plugins[] = $plugin;
	}

	/**
	 * @api
	 * @param Plugin $plugin
	 */
	public function deregister(Plugin $plugin)
	{
		$registered = $this->readConfiguration();
		if ($registered)
		{
			$k = $this->getPluginKey($plugin);
			if (isset($registered[$k]))
			{
				unset($registered[$k]);
				$this->writeConfiguration($registered);

				if ($this->plugins !== null)
				{
					$this->plugins = array_values(array_filter($this->plugins, function (Plugin $p) use ($plugin)
					{
						return !$p->eq($plugin);
					}));
				}
			}
		}
	}

	/**
	 * @return array
	 */
	protected function readConfiguration()
	{
		if ($this->pluginsConfig === null)
		{
			$this->pluginsConfig = [];
			$pluginDataPath = $this->getWorkspace()->appPath('Config', 'plugins.json');
			if (is_readable($pluginDataPath))
			{
				$pluginData = json_decode(file_get_contents($pluginDataPath), true);
				if (is_array($pluginData))
				{
					foreach ($pluginData as $infos)
					{
						$this->pluginsConfig[$infos['type'] . '.' . $infos['vendor'] . '.' . $infos['name']] = $infos;
					}
				}
			}
		}

		return $this->pluginsConfig;
	}

	/**
	 * @param array $registered
	 */
	protected function writeConfiguration(array $registered)
	{
		$this->pluginsConfig = $registered;
		$pluginDataPath = $this->getWorkspace()->appPath('Config', 'plugins.json');
		if ($registered)
		{
			file_put_contents($pluginDataPath, json_encode(array_values($registered), JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE));
		}
		elseif (file_exists($pluginDataPath))
		{
			unlink($pluginDataPath);
		}
	}

	/**
	 * @param Plugin $plugin
	 * @return string
	 */
	protected function getPluginKey(Plugin $plugin)
	{
		return $plugin->getType() . '.' . $plugin->getVendor() . '.' . $plugin->getShortName();
	}

	/**
	 * @param Plugin[] $plugins
	 * @return \Change\Plugins\Plugin[]
	 */
	public function loadRegistration(array $plugins)
	{
		$registered = $this->readConfiguration();
		if (!$registered || !$plugins)
		{
			return [];
		}

		$plugins = array_filter($plugins, function (Plugin $plugin) use ($registered)
		{
			$k = $this->getPluginKey($plugin);
			if (isset($registered[$k]))
			{
				$infos = $registered[$k];
				$plugin->setActivated($infos['activated']);
				$plugin->setConfigured($infos['configured']);
				$plugin->setRegistrationDate($infos['registrationDate'] ? new \DateTime($infos['registrationDate']) : null);
				if ($infos['configuration'])
				{
					$plugin->setConfiguration(array_merge($plugin->getConfiguration(), $infos['configuration']));
				}
				return true;
			}
			return false;
		});
		return array_values($plugins);
	}

	/**
	 * @api
	 * @param Plugin $plugin
	 * @return \Change\Plugins\Plugin|null
	 */
	public function load(Plugin $plugin = null)
	{
		$registered = $this->readConfiguration();
		if ($registered && $plugin)
		{
			$k = $this->getPluginKey($plugin);
			if (isset($registered[$k]))
			{
				$infos = $registered[$k];
				$plugin->setPackage($infos['package']);
				$plugin->setActivated($infos['activated']);
				$plugin->setConfigured($infos['configured']);
				$plugin->setRegistrationDate($infos['registrationDate'] ? new \DateTime($infos['registrationDate']) : null);
				if ($infos['configuration'])
				{
					$plugin->setConfiguration($infos['configuration']);
				}
				return $plugin;
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param Plugin $plugin
	 */
	public function update(Plugin $plugin)
	{
		$registered = $this->readConfiguration();
		$k = $this->getPluginKey($plugin);
		if (isset($registered[$k]))
		{
			$this->writeConfiguration($this->updateRegistered($registered, $plugin));
		}
	}

	/**
	 * @param array $registered
	 * @param \Change\Plugins\Plugin $plugin
	 * @return array
	 */
	protected function updateRegistered(array $registered, Plugin $plugin)
	{
		$k = $this->getPluginKey($plugin);
		$registered[$k]['package'] = $plugin->getPackage();
		$registered[$k]['activated'] = $plugin->getActivated();
		$registered[$k]['configured'] = $plugin->getConfigured();
		$registered[$k]['configuration'] = $plugin->getConfiguration() ?: null;
		return $registered;
	}

	/**
	 * @api
	 * @param string $type
	 * @param string $vendor
	 * @param string $shortName
	 * @return \Change\Plugins\Plugin|null
	 */
	public function getPlugin($type, $vendor, $shortName)
	{
		$vendor = $this->normalizeVendorName($vendor);
		$shortName = $this->normalizePluginName($shortName);
		foreach ($this->getPlugins() as $plugin)
		{
			if ($plugin->getType() === $type && $plugin->getVendor() === $vendor && $plugin->getShortName() === $shortName)
			{
				return $plugin;
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param string $vendor
	 * @param string $shortName
	 * @return \Change\Plugins\Plugin|null
	 */
	public function getModule($vendor, $shortName)
	{
		$vendor = $this->normalizeVendorName($vendor);
		$shortName = $this->normalizePluginName($shortName);
		foreach ($this->getPlugins() as $plugin)
		{
			if ($plugin->getType() === Plugin::TYPE_MODULE && $plugin->getVendor() === $vendor && $plugin->getShortName() === $shortName)
			{
				return $plugin;
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param string $vendor
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getModules($vendor = null)
	{
		$modules = [];
		$vendor = $vendor ? $this->normalizeVendorName($vendor) : null;
		foreach ($this->getPlugins() as $plugin)
		{
			if ($plugin->getType() === Plugin::TYPE_MODULE && ($vendor === null || $plugin->getVendor() === $vendor))
			{
				$modules[] = $plugin;
			}
		}
		return $modules;
	}

	/**
	 * @api
	 * @param string $vendor
	 * @param string $shortName
	 * @return \Change\Plugins\Plugin|null
	 */
	public function getTheme($vendor, $shortName)
	{
		$vendor = $this->normalizeVendorName($vendor);
		$shortName = $this->normalizePluginName($shortName);
		foreach ($this->getPlugins() as $plugin)
		{
			if ($plugin->getType() === Plugin::TYPE_THEME && $plugin->getVendor() === $vendor
				&& $plugin->getShortName() === $shortName
			)
			{
				return $plugin;
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param string $vendor
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getThemes($vendor = null)
	{
		$themes = [];
		$vendor = $vendor ? $this->normalizeVendorName($vendor) : null;
		foreach ($this->getPlugins() as $plugin)
		{
			if ($plugin->getType() === Plugin::TYPE_THEME && ($vendor === null || $plugin->getVendor() === $vendor))
			{
				$themes[] = $plugin;
			}
		}
		return $themes;
	}

	/**
	 * @api
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getRegisteredPlugins()
	{
		return array_values(array_filter($this->getPlugins(), function (Plugin $plugin)
		{
			return $plugin->getConfigured() === false;
		}));
	}

	/**
	 * @api
	 * @return \Change\Plugins\Plugin[]
	 */
	public function getInstalledPlugins()
	{
		return array_values(array_filter($this->getPlugins(), function (Plugin $plugin)
		{
			return $plugin->getConfigured() === true;
		}));
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return array
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/Plugin');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		foreach ($this->getPlugins() as $plugin)
		{
			(new Register($plugin))->attach($eventManager);
		}
	}

	/**
	 * @api
	 * @param \Change\Plugins\Plugin[] $plugins
	 * @param array $context
	 * @return \Change\Plugins\Plugin[]
	 */
	public function installPlugins(array $plugins, array $context = [])
	{
		return $this->doInstall($plugins, $context);
	}

	/**
	 * @api
	 * @param \Change\Plugins\Plugin $plugin
	 * @param array $context
	 * @return \Change\Plugins\Plugin[]
	 */
	public function installPlugin(\Change\Plugins\Plugin $plugin, array $context = [])
	{
		return $this->doInstall([$plugin], $context);
	}

	/**
	 * @api
	 * @param Plugin $plugin
	 * @throws \InvalidArgumentException
	 */
	public function deinstall(Plugin $plugin)
	{
		if (isset($plugin->getConfiguration()['locked']) && $plugin->getConfiguration()['locked'])
		{
			throw new \InvalidArgumentException('Plugin is locked, unable to deinstall');
		}
		else
		{
			//TODO do a real deinstall!
			$plugin->setActivated(false);
			$plugin->setConfigured(false);
			$plugin->setConfigurationEntry('deinstallDate', (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s e'));
			$this->update($plugin);
		}
	}

	/**
	 *
	 * @return string[]
	 */
	public function getInstallSteps()
	{
		return ['Application', 'DbSchema', 'Services'];
	}

	/**
	 * @return string|null
	 */
	public function getWebBaseDirectory()
	{
		$root = $this->getApplication()->getConfiguration()->getEntry('Change/Install/webBaseDirectory', false);
		if ($root === false)
		{
			return null;
		}
		return $this->getWorkspace()->composeAbsolutePath($root);
	}

	/**
	 * @return string|null
	 */
	public function getWebAssetsBaseDirectory()
	{
		$webBaseDirectory = $this->getWebBaseDirectory();
		if ($webBaseDirectory && ($assetsVersion = $this->getApplication()->getConfiguration()->getEntry('Change/Install/assetsVersion', null)))
		{
			return $this->getWorkspace()->composePath($webBaseDirectory, 'Assets', $assetsVersion);
		}
		return null;
	}

	/**
	 * @param \Change\Plugins\Plugin[] $pluginsToInstall
	 * @param array $context
	 * @return \Change\Plugins\Plugin[]
	 */
	protected function doInstall(array $pluginsToInstall, array $context)
	{
		$steps = $context['steps'] ?? $this->getInstallSteps();
		$installEventManager = $this->getEventManager();
		$event = new \Change\Events\Event(static::EVENT_SETUP_INITIALIZE, $this, ['steps' => $steps, 'extraPlugins' => []]);

		$this->doSetupInitialize($event, $installEventManager, $pluginsToInstall);

		$plugins = [];
		$installModules = false;
		$date = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s e');
		foreach ($pluginsToInstall as $plugin)
		{
			if ($plugin instanceof Plugin)
			{
				$plugin->setWorkspace($this->getWorkspace());
				$plugin->setActivated(true);
				if (!$plugin->getConfigurationEntry('installDate'))
				{
					$plugin->setConfigurationEntry('installDate', $date);
				}
				$plugin->setConfigured(true);
				if (!$plugin->getConfigurationEntry('configuredDate'))
				{
					$plugin->setConfigurationEntry('configuredDate', $date);
				}
				if (!$installModules && $plugin->getType() === 'module')
				{
					$installModules = true;
				}
				$plugins[] = $plugin;
			}
		}

		if (!$plugins)
		{
			return $plugins;
		}

		if (\count($plugins) > 1)
		{
			usort($plugins, [$this, 'comparePlugin']);
		}
		$event->setParam('plugins', $plugins);

		$applicationStep = in_array('Application', $steps);
		$dbSchemaStep = in_array('DbSchema', $steps);
		$servicesStep = in_array('Services', $steps);

		$editableConfiguration = null;
		$eventDbProvider = $event->getApplicationServices()->getDbProvider();
		if ($applicationStep)
		{
			$eventDbProvider->setDisabled(true);

			$event->setName(static::EVENT_SETUP_APPLICATION);

			if (!($this->getApplication()->getConfiguration() instanceof \Change\Configuration\EditableConfiguration))
			{
				$editableConfiguration = new \Change\Configuration\EditableConfiguration([]);
				$this->getApplication()->setConfiguration($editableConfiguration->import($this->getApplication()
					->getConfiguration()));
			}
			else
			{
				$editableConfiguration = $this->getApplication()->getConfiguration();
			}
			$event->setParam('configuration', true);
			$this->triggerPlugins($event, $installEventManager, $plugins);
			$editableConfiguration->save();

			//force reload configuration
			$this->getApplication()->setConfiguration();

			$event->setParam('configuration', false);
			$this->triggerPlugins($event, $installEventManager, $plugins);

			if ($installModules)
			{
				$compiler = new \Change\Documents\Generators\Compiler($this->getApplication(), $event->getApplicationServices());
				$compiler->generate();
			}


			$eventDbProvider->setDisabled(false);
		}

		if ($dbSchemaStep)
		{
			$generator = new \Change\Db\Schema\Generator($this->getWorkspace(), $event->getApplicationServices()->getDbProvider());
			$generator->generateSystemSchema();
			$generator->generatePluginsSchema();

			$event->setName(static::EVENT_SETUP_DB_SCHEMA);
			$this->triggerPlugins($event, $installEventManager, $plugins);
		}

		if ($servicesStep)
		{
			$event->setName(static::EVENT_SETUP_SERVICES);
			$this->triggerPlugins($event, $installEventManager, $plugins);
		}

		if ($applicationStep)
		{
			$eventDbProvider->setDisabled(true);

			$event->setName(static::EVENT_SETUP_FINALIZE);
			$this->triggerPlugins($event, $installEventManager, $plugins);
		}

		$event->setName(static::EVENT_SETUP_SUCCESS);
		$this->triggerPlugins($event, $installEventManager, $plugins);

		if ($applicationStep)
		{
			$registered = $this->readConfiguration();
			foreach ($plugins as $plugin)
			{
				$registered = $this->updateRegistered($registered, $plugin);
			}
			$this->writeConfiguration($registered);
			$this->compile();

			$eventDbProvider->setDisabled(false);
		}
		return $plugins;
	}

	/**
	 * @param \Change\Plugins\Plugin $a
	 * @param \Change\Plugins\Plugin $b
	 * @return int
	 */
	public function comparePlugin(Plugin $a, Plugin $b)
	{
		if ($a === $b)
		{
			return 0;
		}

		$at = $a->getType();
		if ($at !== $b->getType())
		{
			return $at === 'module' ? -1 : 1;
		}

		//Same type
		$aV = $a->getVendor();
		$bv = $b->getVendor();
		if ($aV !== $bv)
		{
			if ($aV === 'Rbs' || $bv === 'Project')
			{
				return -1;
			}
			if ($aV === 'Project' || $bv === 'Rbs')
			{
				return 1;
			}
			return $aV <=> $bv;
		}

		//Same Vendor
		$ap = $a->getPackage();
		$bp = $b->getPackage();
		if ($ap !== $bp)
		{
			if ($ap === 'Core' || !$bp)
			{
				return -1;
			}
			if ($bp === 'Core' || !$ap)
			{
				return 1;
			}
			return $ap <=> $bp;
		}

		//Same Package
		$an = $a->getShortName();
		$bn = $b->getShortName();
		if ($aV === 'Rbs')
		{
			if ($an === 'Ua')
			{
				$an = 'A';
			}
			if ($bn === 'Ua')
			{
				$bn = 'A';
			}
		}

		return $an <=> $bn;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param \Change\Events\EventManager $installEventManager
	 * @param \Change\Plugins\Plugin[] $plugins
	 */
	private function doSetupInitialize(\Change\Events\Event $event, \Change\Events\EventManager $installEventManager, array &$plugins)
	{
		$event->setName(static::EVENT_SETUP_INITIALIZE);
		$event->setParam('extraPlugins', []);
		$this->triggerPlugins($event, $installEventManager, $plugins);
		if (($extraPlugins = $event->getParam('extraPlugins')) && \is_array($extraPlugins))
		{
			$extraPlugins = array_filter($extraPlugins, function ($plugin) use (&$plugins) {
				return $plugin instanceof \Change\Plugins\Plugin && !\in_array($plugin, $plugins, true);
			});
			if (!$extraPlugins)
			{
				return;
			}
			$this->doSetupInitialize($event, $installEventManager, $extraPlugins);
			foreach ($extraPlugins as $extraPlugin)
			{
				if ($extraPlugin instanceof \Change\Plugins\Plugin && !\in_array($extraPlugin, $plugins, true))
				{
					$plugins[] = $extraPlugin;
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @param \Change\Events\EventManager $installEventManager
	 * @param Plugin[] $plugins
	 */
	private function triggerPlugins(\Change\Events\Event $event, \Change\Events\EventManager $installEventManager, array $plugins)
	{
		foreach ($plugins as $plugin)
		{
			if ($plugin instanceof \Change\Plugins\Plugin)
			{
				$event->setParam('plugin', $plugin);
				$installEventManager->triggerEvent($event);
			}
		}
	}

	/**
	 * @api
	 * @param $name
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function normalizeVendorName($name)
	{
		$lcName = strtolower($name);
		if (!preg_match('/^[a-z][a-z0-9]{1,24}$/', $lcName))
		{
			throw new \InvalidArgumentException('Vendor name "' . $lcName . '" should match ^[a-z][a-z0-9]{1,24}$', 999999);
		}
		return ucfirst($lcName);
	}

	/**
	 * @api
	 * @param $name
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function normalizePluginName($name)
	{
		$lcName = strtolower($name);
		if (!preg_match('/^[a-z][a-z0-9]{1,24}$/', $lcName))
		{
			throw new \InvalidArgumentException('Plugin name "' . $lcName . '" should match ^[a-z][a-z0-9]{1,24}$', 999999);
		}
		return ucfirst($lcName);
	}

	/**
	 * @api
	 * @param string $type
	 * @param string $vendor
	 * @param string $name
	 * @param string $defaultLCID
	 * @param string|null $package
	 * @return string
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	public function initializePlugin($type, $vendor, $name, $defaultLCID, $package = null)
	{
		if ($type !== 'module' && $type !== 'theme')
		{
			throw new \InvalidArgumentException('Type must be either "module" or "theme"');
		}

		$normalizedVendor = $this->normalizeVendorName($vendor);
		$normalizedName = $this->normalizePluginName($name);
		$path = null;

		if ($type === 'module')
		{
			if ($normalizedVendor === 'Project')
			{
				$path = $this->getWorkspace()->projectModulesPath('Project', $normalizedName, 'plugin.json');
			}
			else
			{
				$path = $this->getWorkspace()->pluginsModulesPath($normalizedVendor, $normalizedName, 'plugin.json');
			}
			$twigFileName = 'Assets/Install.module.php.twig';
		}
		else
		{
			if ($normalizedVendor === 'Project')
			{
				$path = $this->getWorkspace()->projectThemesPath('Project', $normalizedName, 'plugin.json');
			}
			else
			{
				$path = $this->getWorkspace()->pluginsThemesPath($normalizedVendor, $normalizedName, 'plugin.json');
			}
			$twigFileName = 'Assets/Install.theme.php.twig';
			$this->initializeTheme($normalizedVendor, $normalizedName, $defaultLCID);
		}
		if (file_exists($path))
		{
			throw new \RuntimeException('Plugin already exists at path ' . $path, 999999);
		}

		$attributes = ['type' => $type, 'vendor' => $normalizedVendor, 'name' => $normalizedName, 'defaultLCID' => $defaultLCID];
		if ($package)
		{
			$attributes['package'] = $package;
		}
		FileUtils::write($path, $this->encode($attributes));

		$loader = new \Twig\Loader\FilesystemLoader(__DIR__);
		$twig = new \Twig\Environment($loader);
		FileUtils::write(dirname($path) . DIRECTORY_SEPARATOR . 'Setup' . DIRECTORY_SEPARATOR . 'Install.php',
			$twig->render($twigFileName, $attributes));

		$plugin = $this->getNewPlugin($path, $type === 'module' ? Plugin::TYPE_MODULE : Plugin::TYPE_THEME);
		$this->register($plugin);

		$this->compile();
		return dirname($path);
	}

	/**
	 * @param array $array
	 * @return string
	 */
	protected function encode(array $array)
	{
		return str_replace('    ', '	', \json_encode($array, JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES + JSON_PRETTY_PRINT));
	}

	/**
	 * @param string $normalizedVendor
	 * @param string $normalizedName
	 * @param string $defaultLCID
	 */
	protected function initializeTheme($normalizedVendor, $normalizedName, $defaultLCID)
	{
		if ($normalizedVendor === 'Project')
		{
			$path = $this->getWorkspace()
				->projectThemesPath('Project', $normalizedName, 'Assets', 'I18n', $defaultLCID, 'admin.json');
		}
		else
		{
			$path = $this->getWorkspace()
				->pluginsThemesPath($normalizedVendor, $normalizedName, 'Assets', 'I18n', $defaultLCID, 'admin.json');
		}
		$content = ['label' => ['message' => $normalizedVendor . ' ' . $normalizedName]];
		FileUtils::write($path, $this->encode($content));

		$assetsDirectories = ['img', 'less', 'Templates'];
		foreach ($assetsDirectories as $directory)
		{
			if ($normalizedVendor === 'Project')
			{
				$path = $this->getWorkspace()->projectThemesPath('Project', $normalizedName, 'Assets', $directory);
			}
			else
			{
				$path = $this->getWorkspace()->pluginsThemesPath($normalizedVendor, $normalizedName, 'Assets', $directory);
			}
			\Change\Stdlib\FileUtils::mkdir($path);
		}
	}
}