<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins;

/**
 * @name \Change\Plugins\PluginTheme
 */
class PluginTheme implements \Change\Presentation\Interfaces\Theme
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $vendor;

	/**
	 * @var string
	 */
	protected $shortName;

	/**
	 * @var string $templateBasePath
	 */
	protected $templateBasePath;

	/**
	 * @var \Change\Workspace
	 */
	protected $workspace;

	/**
	 * @var \Change\Presentation\Themes\ThemeManager|null
	 */
	protected $themeManager;

	/**
	 * @var \Change\Plugins\PluginTheme
	 */
	protected $parentTheme;

	/**
	 * @var array
	 */
	protected $assetConfiguration = [];

	/**
	 * @param string $name
	 * @param \Change\Workspace $workspace
	 * @param \Change\Plugins\PluginTheme $parentTheme
	 */
	public function __construct($name, \Change\Workspace $workspace, \Change\Plugins\PluginTheme $parentTheme = null)
	{
		$this->workspace = $workspace;
		$this->name = $name;
		$this->parentTheme = $parentTheme;
		list($this->vendor, $this->shortName) = explode('_', $name);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getVendor()
	{
		return $this->vendor;
	}

	/**
	 * @return string
	 */
	public function getShortName()
	{
		return $this->shortName;
	}

	/**
	 * @param \Change\Presentation\Themes\ThemeManager|null $themeManager
	 * @return $this
	 */
	public function setThemeManager(\Change\Presentation\Themes\ThemeManager $themeManager)
	{
		$this->themeManager = $themeManager;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginTheme|null
	 */
	public function getParentTheme()
	{
		return $this->parentTheme;
	}


	/**
	 * @return string
	 */
	public function getTemplateBasePath()
	{
		if ($this->templateBasePath === null)
		{
			$this->templateBasePath = $this->workspace->compilationPath('Themes', $this->vendor, $this->shortName);
		}
		return $this->templateBasePath;
	}

	/**
	 * @return string
	 */
	public function getAssetBasePath()
	{
		if ($this->vendor === 'Project')
		{
			return $this->workspace->projectThemesPath($this->vendor, $this->shortName, 'Assets');
		}
		return $this->workspace->pluginsThemesPath($this->vendor, $this->shortName, 'Assets');
	}

	/**
	 * @param string $moduleName
	 */
	public function removeTemplatesContent($moduleName)
	{
		$basePath = $this->workspace->composePath($this->getTemplateBasePath(), $moduleName);
		\Change\Stdlib\FileUtils::rmdir($basePath);
	}

	/**
	 * @param string $moduleName
	 * @param string $pathName
	 * @param string $content
	 * @return void
	 */
	public function installTemplateContent($moduleName, $pathName, $content)
	{
		$path =  $this->workspace->composePath($this->getTemplateBasePath(), $moduleName, $pathName);
		\Change\Stdlib\FileUtils::mkdir(dirname($path));
		file_put_contents($path, $content);
	}

	/**
	 * @param string $moduleName
	 * @param string $fileName
	 * @return string
	 */
	public function getTemplateRelativePath($moduleName, $fileName)
	{
		return $this->workspace->composePath($moduleName, $fileName);
	}

	/**
	 * @param string $name
	 * @return null
	 */
	public function getPageTemplate($name)
	{
		return null;
	}

	/**
	 * @param string $resourcePath
	 * @return string
	 */
	public function getResourceFilePath($resourcePath)
	{
		return $this->workspace->composePath($this->getAssetBasePath(), $resourcePath);
	}

	/**
	 * @return array
	 */
	public function getAssetConfiguration()
	{
		return $this->assetConfiguration;
	}

	/**
	 * @param array $assetConfiguration
	 * @return $this
	 */
	public function setAssetConfiguration($assetConfiguration)
	{
		$this->assetConfiguration = $assetConfiguration;
		return $this;
	}

	/**
	 * @param array $assetConfiguration
	 * @return $this
	 */
	public function writeConfiguration($assetConfiguration)
	{
		$path =  $this->workspace->composePath($this->getTemplateBasePath(), 'assetConfiguration.ser');
		\Change\Stdlib\FileUtils::mkdir(dirname($path));
		file_put_contents($path, serialize($assetConfiguration));
		return $this;
	}
}