<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Plugins;

/**
 * @name \Change\Plugins\ThemeInstallBase
 */
class ThemeInstallBase extends InstallBase
{
	/**
	 * @var array
	 */
	protected $imageFormats;

	/**
	 * @var string
	 */
	protected $imageLessFilePath;

	/**
	 * @var string
	 */
	protected $imageScssFilePath;

	/**
	 * @var string|null
	 */
	protected $extend;

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function initialize($plugin)
	{
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupApplication(\Change\Events\Event $event)
	{
		$plugin = $this->plugin;
		if ($this->isValid($event, $plugin))
		{
			$this->imageFormats = $event->getApplication()->getConfiguration('Rbs/Media/namedImageFormats');
		}
		parent::onSetupApplication($event);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$this->imageLessFilePath = $application->getWorkspace()->compilationPath('Themes', 'compiled-image-formats.less');
		$this->imageScssFilePath = $application->getWorkspace()->compilationPath('Themes', 'compiled-image-formats.scss');
	}

	/**
	 * @param \Change\Presentation\Themes\ThemeAssets $themeAssets
	 * @param \Change\Workspace $workspace
	 * @param \Change\Plugins\Plugin[]|null $modules
	 * @return PluginTheme
	 */
	protected function getDefaultThemePlugin($themeAssets, $workspace, array $modules = null)
	{
		$defaultTheme = new \Change\Plugins\PluginTheme(\Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME, $workspace);
		$defaultTheme->setAssetConfiguration($themeAssets->buildAssetConfiguration($defaultTheme, $modules));
		return $defaultTheme;
	}

	/**
	 * @param Plugin $plugin
	 * @param \Change\Presentation\Themes\ThemeAssets $themeAssets
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Workspace $workspace
	 * @return PluginTheme
	 */
	protected function getPluginThemeTree($plugin, $themeAssets, $pluginManager, $workspace)
	{
		if (!$plugin->isTheme())
		{
			throw new \InvalidArgumentException('plugin is not a theme', 999999);
		}
		$parentPluginTheme = null;
		$parentPluginThemeName = null;
		$config = $plugin->getConfiguration();
		if (isset($config['extends']) && $config['extends'])
		{
			$parentPluginThemeName = (string)$config['extends'];
			$nameParts = \explode('_', $parentPluginThemeName);
			if (\count($nameParts) !== 2)
			{
				throw new \InvalidArgumentException('Theme "'.$plugin->getName() .'" as invalid extends: ' . $parentPluginThemeName , 999999);
			}
			[$vendor, $shortName] = $nameParts;
			$parentPlugin = $pluginManager->getTheme($vendor, $shortName);
			if (!$parentPlugin)
			{
				throw new \InvalidArgumentException('Theme "'.$plugin->getName() .'" as undefined extends: ' . $parentPluginThemeName , 999999);
			}
			$parentPluginTheme = $this->getPluginThemeTree($parentPlugin, $themeAssets, $pluginManager, $workspace);
		}
		else
		{
			$parentPluginTheme = $this->getDefaultThemePlugin($themeAssets, $workspace, $pluginManager->getModules());
		}
		$pluginTheme = new PluginTheme($plugin->getName(), $workspace, $parentPluginTheme);

		$assetConfiguration = $themeAssets->appendConfiguration(
			$parentPluginTheme->getAssetConfiguration(),
			$themeAssets->buildAssetConfiguration($pluginTheme)
		);

		$pluginTheme->setAssetConfiguration($assetConfiguration);
		return $pluginTheme;
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		if ($plugin->isTheme())
		{
			$workspace = $application->getWorkspace();

			$webThemeAssetsBaseDirectory = $workspace->composePath($pluginManager->getWebAssetsBaseDirectory(), 'Theme');
			$themeAssets = new \Change\Presentation\Themes\ThemeAssets($application);

			$pluginTheme = $this->getPluginThemeTree($plugin, $themeAssets, $pluginManager, $workspace);

			$themeAssets->installPluginTemplates($plugin, $pluginTheme);
			$templates = $themeAssets->getNgTemplatesPath($pluginTheme);
			$pt = $pluginTheme->getParentTheme();
			while($pt)
			{
				$templates = array_merge($templates, $themeAssets->getNgTemplatesPath($pt));
				$pt = $pt->getParentTheme();
			}
			$themeAssets->writeNgTemplates($pluginTheme, $templates);

			$pluginTheme->writeConfiguration($pluginTheme->getAssetConfiguration());

			$assetBaseDir =  $workspace->composePath($webThemeAssetsBaseDirectory, $pluginTheme->getVendor(), $pluginTheme->getShortName());
			$themeAssets->installPluginAssets($plugin, $assetBaseDir);

			\Change\Stdlib\FileUtils::mkdir(dirname($this->imageLessFilePath));
			file_put_contents($this->imageLessFilePath, $themeAssets->generateImageLessContent($this->imageFormats));

			\Change\Stdlib\FileUtils::mkdir(dirname($this->imageScssFilePath));
			file_put_contents($this->imageScssFilePath, $themeAssets->generateImageScssContent($this->imageFormats));

			$configuration = $themeAssets->compileAssetConfiguration($pluginTheme->getAssetConfiguration(), null, 'all');
			$am = $themeAssets->getAsseticManager($pluginTheme, $configuration);
			$themeAssets->write($am);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \RuntimeException
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$transactionManager = $applicationServices->getTransactionManager();
		$theme = $this->createOrUpdateTheme($plugin, $applicationServices, $transactionManager);

		// Fetch and create theme if necessary.
		foreach ($this->getTemplatesJsonDefinitions($plugin) as $filePath)
		{
			$this->createOrUpdateTemplate($theme, $filePath, $plugin, $applicationServices);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 */
	public function finalize($plugin)
	{
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param string $name
	 * @return \Rbs\Theme\Documents\Theme
	 */
	protected function getThemeByName($applicationServices, $name)
	{
		$themeModel = $applicationServices->getModelManager()->getModelByName('Rbs_Theme_Theme');
		$query = $applicationServices->getDocumentManager()->getNewQuery($themeModel);
		$query->andPredicates($query->eq('name', $name));
		return $query->getFirstDocument();
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param string $code
	 * @return \Rbs\Theme\Documents\Template
	 */
	protected function getTemplateByCode($applicationServices, $code)
	{
		$templateModel = $applicationServices->getModelManager()->getModelByName('Rbs_Theme_Template');
		$query = $applicationServices->getDocumentManager()->getNewQuery($templateModel);
		$query->andPredicates($query->eq('code', $code));
		return $query->getFirstDocument();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @return array
	 */
	protected function getTemplatesJsonDefinitions($plugin)
	{
		$templatesDir = $plugin->getAbsolutePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'Templates';
		// Plugin Modules.
		$templatesPattern = $templatesDir . DIRECTORY_SEPARATOR . '*.json';
		return \Zend\Stdlib\Glob::glob($templatesPattern, \Zend\Stdlib\Glob::GLOB_NOESCAPE + \Zend\Stdlib\Glob::GLOB_NOSORT);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param String $templateName
	 * @return string
	 */
	protected function buildTemplateCode($plugin, $templateName)
	{
		return $plugin->getName() . '_' . \Change\Stdlib\StringUtils::ucfirst($templateName);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @return string
	 */
	protected function buildThemeName($plugin)
	{
		return $plugin->getName();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @return string
	 */
	protected function buildThemeLabelKey($plugin)
	{
		return 't.' . strtolower($plugin->getVendor()) . '.' . strtolower($plugin->getShortName()) . '.admin.label';
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param String $templateName
	 * @return string
	 */
	protected function buildTemplateLabelKey($plugin, $templateName)
	{
		return 't.' . strtolower($plugin->getVendor()) . '.' . strtolower($plugin->getShortName()) . '.admin.' . $templateName . '_label';
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @return \Rbs\Theme\Documents\Theme
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function createOrUpdateTheme($plugin, $applicationServices, $transactionManager)
	{
		$themeName = $plugin->getName();
		/* @var $theme \Rbs\Theme\Documents\Theme */
		$theme = $this->getThemeByName($applicationServices, $themeName);
		if ($theme === null)
		{
			$themeModel = $applicationServices->getModelManager()->getModelByName('Rbs_Theme_Theme');
			$theme = $applicationServices->getDocumentManager()->getNewDocumentInstanceByModel($themeModel);
			$theme->setName($themeName);
			$theme->setActive(true);
		}
		try
		{
			$transactionManager->begin();

			$config = $plugin->getConfiguration();
			$parentThemeName = is_array($config) && isset($config['extends']) ? $config['extends'] : null;
			if ($parentThemeName)
			{
				$parentTheme = $this->getThemeByName($applicationServices, $parentThemeName);
				if (!$parentTheme)
				{
					throw new \RuntimeException('parent theme: ' . $parentThemeName . ' not found for theme: '. $themeName);
				}
				$theme->setParentTheme($parentTheme);
			}

			$theme->setLabel($applicationServices->getI18nManager()->trans($this->buildThemeLabelKey($plugin), ['ucf']));
			$theme->save();
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
		return $theme;
	}

	/**
	 * @param \Rbs\Theme\Documents\Theme $theme
	 * @param string $filePath
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws
	 */
	protected function createOrUpdateTemplate($theme, $filePath, $plugin, $applicationServices)
	{
		$basePath = $plugin->getAbsolutePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'Templates';
		$filesMeta = [];
		$filesMeta['json'] = $filePath;
		$file = new \SplFileInfo($filePath);
		$fileName = $file->getFilename();
		$templateDefinition = json_decode(file_get_contents($filePath), true);
		$templateName = substr($fileName, 0, mb_strlen($fileName) - 5);
		$templateCode = $this->buildTemplateCode($plugin, $templateName);
		$template = $this->getTemplateByCode($applicationServices, $templateCode);
		if ($template === null)
		{
			$templateModel = $applicationServices->getModelManager()->getModelByName('Rbs_Theme_Template');
			/* @var $template \Rbs\Theme\Documents\Template */
			$template = $applicationServices->getDocumentManager()->getNewDocumentInstanceByModel($templateModel);
			$template->setCode($templateCode);
			$template->setTheme($theme);
			$template->setActive(true);
		}

		$html = '';
		$htmlFilePath = $basePath . DIRECTORY_SEPARATOR . $templateName . '.twig';
		if (file_exists($htmlFilePath))
		{
			$html = file_get_contents($htmlFilePath);
		}
		$filesMeta['html'] = $htmlFilePath;

		$boHtml = '';
		$boHtmlFilePath = $basePath . DIRECTORY_SEPARATOR . $templateName . '-bo.twig';
		if (file_exists($boHtmlFilePath))
		{
			$boHtml = file_get_contents($boHtmlFilePath);
		}
		$filesMeta['htmlForBackoffice'] = $boHtmlFilePath;

		$installContent = isset($templateDefinition['editableContent'])
			&& is_array($templateDefinition['editableContent']) ? $templateDefinition['editableContent'] : [];

		$existingEditableContent = $template->getEditableContent();
		$editableContent = $installContent;
		if (is_array($existingEditableContent))
		{
			$intersection = array_intersect_key($existingEditableContent, $editableContent);
			foreach ($intersection as $name => &$data)
			{
				$data['display'] = $editableContent[$name]['display'] ?? null;
				$data['visibility'] = $editableContent[$name]['visibility'] ?? null;
			}
			unset($data);
			$editableContent = array_merge($editableContent, $intersection);
		}

		$mailSuitable = isset($templateDefinition['mailSuitable'])
			&& is_bool($templateDefinition['mailSuitable']) ? $templateDefinition['mailSuitable'] : false;
		$defaultDisplayColumnsFrom = $templateDefinition['defaultDisplayColumnsFrom'] ?? 'M';

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$template->setLabel($applicationServices->getI18nManager()->trans($this->buildTemplateLabelKey($plugin,
				$templateName), ['ucf']));
			$template->setHtmlData($html);
			$template->setEditableContent($editableContent);
			$template->setHtmlForBackoffice($boHtml);
			$template->setMailSuitable($mailSuitable);
			if (!$template->getDefaultDisplayColumnsFrom())
			{
				$template->setDefaultDisplayColumnsFrom($defaultDisplayColumnsFrom);
			}
			$template->save();
			$template->setMeta(\Rbs\Theme\Documents\Template::FILE_META_KEY, $filesMeta);
			$template->saveMetas();
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}