<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Blocks;

use Change\Http\Web\Result\BlockResult;
use Change\Presentation\Blocks\Standard\UpdateBlockInformation;

/**
 * @name \Change\Presentation\Blocks\BlockManager
 */
class BlockManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const DEFAULT_IDENTIFIER = 'Http.Web.Block';

	const EVENT_PARAMETERIZE = 'block.parameterize';

	const EVENT_EXECUTE = 'block.execute';

	const EVENT_INFORMATION = 'block.information';

	const EVENT_GET_CACHE_ADAPTER = 'getCacheAdapter';

	/**
	 * @var array
	 */
	protected $blocks;

	/**
	 * @var \Change\Cache\CacheManager
	 */
	protected $cacheManager;


	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return \Change\Cache\CacheManager
	 */
	protected function getCacheManager()
	{
		return $this->cacheManager;
	}

	/**
	 * @param \Change\Cache\CacheManager $cacheManager
	 * @return $this
	 */
	public function setCacheManager($cacheManager)
	{
		$this->cacheManager = $cacheManager;
		return $this;
	}

	/**
	 * @api
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(static::EVENT_INFORMATION, [$this, 'onDefaultInformation']);
	}

	/**
	 * @param $name
	 * @param Callable|null $informationCallback
	 */
	public function registerBlock($name, $informationCallback = null)
	{
		$this->blocks[$name] = $informationCallback;
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::DEFAULT_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/BlockManager');
	}

	/**
	 * @return string[]
	 */
	public function getBlockNames()
	{
		if ($this->blocks === null)
		{
			$this->blocks = [];
			$eventManager = $this->getEventManager();
			$event = new \Change\Presentation\Blocks\Event(static::EVENT_INFORMATION, $this);
			$eventManager->triggerEvent($event);
		}
		return array_keys($this->blocks);
	}

	/**
	 * @param \Change\Presentation\Blocks\Event $event
	 */
	public function onDefaultInformation(\Change\Presentation\Blocks\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$pluginManager = $applicationServices->getPluginManager();
		$this->registerBlocksTemplates($pluginManager);
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 */
	protected function registerBlocksTemplates($pluginManager)
	{
		foreach ($pluginManager->getThemes() as $themePlugin)
		{
			if ($themePlugin->getActivated())
			{
				$blocksTemplatesFile = $this->getApplication()->getWorkspace()
					->composePath($themePlugin->getAbsolutePath(), 'blocks-templates.json');
				if (is_readable($blocksTemplatesFile))
				{
					$configuration = json_decode(file_get_contents($blocksTemplatesFile), true);
					if (is_array($configuration) && count($configuration))
					{
						foreach ($configuration as $blockName => $blockConfig)
						{
							if (isset($blockConfig['templates']) && is_array($blockConfig['templates'])
								&& count($blockConfig['templates'])
							)
							{
								$templates = $blockConfig['templates'];
								new UpdateBlockInformation($blockName, $this->getEventManager(),
									function ($event) use ($templates, $themePlugin)
									{
										$this->onUpdateTemplateInformation($event, $templates, $themePlugin);
									});
							}
						}
					}
				}
			}
		}
	}

	/**
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param array $templatesConfiguration
	 * @param \Change\Plugins\Plugin $themePlugin
	 */
	protected function onUpdateTemplateInformation(\Change\Presentation\Blocks\Event $event, array $templatesConfiguration, $themePlugin)
	{
		$information = $event->getParam('information');
		if ($information instanceof \Change\Presentation\Blocks\Information)
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			foreach ($templatesConfiguration as $fullyQualifiedTemplateName => $templateConfig)
			{
				$templateInformation = $information->addTemplateInformation($fullyQualifiedTemplateName);
				$templateInformation->setThemeName($themePlugin->getName());
				if (isset($templateConfig['label']))
				{
					$templateInformation->setLabel($i18nManager->trans($templateConfig['label'], ['ucf']));
				}
				else
				{
					$templateInformation->setLabel($templateInformation->getTemplateName());
				}
				if (isset($templateConfig['parameters']) && is_array($templateConfig['parameters']))
				{
					foreach ($templateConfig['parameters'] as $parameterName => $parameterConfig)
					{
						$type =
							$parameterConfig['type'] ?? \Change\Documents\Property::TYPE_STRING;
						$required = isset($parameterConfig['required']) ? $parameterConfig['required'] === true : false;
						$defaultValue = $parameterConfig['defaultValue'] ?? null;
						$parameter = $templateInformation->addParameterInformation($parameterName, $type, $required,
							$defaultValue);
						if (isset($parameterConfig['label']))
						{
							$parameter->setLabel($i18nManager->trans($parameterConfig['label'], ['ucf']));
						}
						else
						{
							$parameter->setLabel($parameterName);
						}
						if (isset($parameterConfig['hidden']))
						{
							$parameter->setHidden($parameterConfig['hidden'] === true);
						}
						if (isset($parameterConfig['allowedModelsNames']))
						{
							$parameter->setAllowedModelsNames($parameterConfig['allowedModelsNames']);
						}
						if (isset($parameterConfig['collectionCode']))
						{
							$parameter->setCollectionCode($parameterConfig['collectionCode']);
						}
					}
				}
			}
		}
	}

	/**
	 * @param string $name
	 * @return Information|null
	 */
	public function getBlockInformation($name)
	{
		$this->getBlockNames();
		if (isset($this->blocks[$name]))
		{
			$information = $this->blocks[$name];
			if ($information instanceof Information)
			{
				return $information;
			}
			elseif (is_callable($information))
			{
				$information = $information();
				if ($information instanceof Information)
				{
					$this->blocks[$name] = $information;
					$eventManager = $this->getEventManager();
					$args = $eventManager->prepareArgs(['information' => $information]);
					$event = new \Change\Presentation\Blocks\Event(static::composeEventName(static::EVENT_INFORMATION, $name), $this, $args);
					$eventManager->triggerEvent($event);
					return $information;
				}
				else
				{
					unset($this->blocks[$name]);
				}
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param string $prefix
	 * @param string $blockName
	 * @return string
	 */
	public static function composeEventName($prefix, $blockName)
	{
		return $prefix . '.' . $blockName;
	}

	/**
	 * @api
	 * @param \Change\Presentation\Layout\Block $blockLayout
	 * @param \Change\Http\Web\Event $httpEvent
	 * @return Parameters
	 */
	public function getParameters(\Change\Presentation\Layout\Block $blockLayout, $httpEvent)
	{
		$eventManager = $this->getEventManager();
		$event = new Event(static::composeEventName(static::EVENT_PARAMETERIZE,
			$blockLayout->getName()), $this, $httpEvent->getParams());
		$event->setAuthenticationManager($httpEvent->getAuthenticationManager());
		$event->setPermissionsManager($httpEvent->getPermissionsManager());
		$event->setParam('httpRequest', $httpEvent->getRequest());
		$event->setBlockLayout($blockLayout);
		$event->setUrlManager($httpEvent->getUrlManager());
		try
		{
			$results = $eventManager->triggerEventUntil(function ($result) {
				return $result instanceof Parameters;
			}, $event);
			$parameters = $results->stopped() ? $results->last() : $event->getBlockParameters();
			return ($parameters instanceof Parameters) ? $parameters : $this->getNewParameters($blockLayout);
		}
		catch (\Throwable $t)
		{
			$logging = $this->getApplication()->getLogging();
			$logging->error($blockLayout->getName(), $blockLayout->getId());
			$logging->throwable($t);
			return $this->getNewParameters($blockLayout)->setNoCache();
		}
	}

	/**
	 * @param \Change\Presentation\Layout\Block $blockLayout
	 * @return Parameters
	 */
	public function getNewParameters($blockLayout)
	{
		return new Parameters($blockLayout->getName());
	}

	/**
	 * @param \Change\Presentation\Layout\Block $blockLayout
	 * @param Parameters $parameters
	 * @param \Change\Http\Web\Event $httpEvent
	 * @return BlockResult|null
	 */
	public function getResult(\Change\Presentation\Layout\Block $blockLayout, $parameters, $httpEvent)
	{
		if (!$blockLayout->getName())
		{
			return null;
		}
		$ttl = max(0, (int)$parameters->getTTL());
		if ($ttl && ($cacheManager = $this->getCacheManager()))
		{
			$options = ['ttl' => $ttl];
			$key = $blockLayout->getName()
					. '_' . (($httpEvent ? $httpEvent->getUrlManager()->getLCID() : '')
					. '_' . md5(serialize($parameters->toArray())));

			if ($cacheManager->hasEntry('BlockManager', $key, $options))
			{
				$result = $cacheManager->getEntry('BlockManager', $key);
				if ($result instanceof BlockResult)
				{
					$result->setId($blockLayout->getId());
					$parameters->setParameterValue('_cached', true);
				}
			}
			else
			{
				$result = $this->dispatchExecute($blockLayout, $parameters, $httpEvent);
				$cacheManager->setEntry('BlockManager', $key, $result);
			}
			return $result;
		}
		return $this->dispatchExecute($blockLayout, $parameters, $httpEvent);
	}

	/**
	 * @param \Change\Presentation\Layout\Block $blockLayout
	 * @param Parameters $parameters
	 * @param \Change\Http\Web\Event $httpEvent
	 * @return BlockResult|null
	 */
	protected function dispatchExecute($blockLayout, $parameters, $httpEvent)
	{
		$result = null;
		try
		{
			$eventManager = $this->getEventManager();
			$eventName = static::composeEventName(static::EVENT_EXECUTE, $blockLayout->getName());

			$event = new Event($eventName, $this, $httpEvent->getParams());
			$attributes = new \ArrayObject(['parameters' => $parameters, 'blockId' => $blockLayout->getId()]);
			$event->setParam('attributes', $attributes);
			$event->setBlockLayout($blockLayout);
			$event->setBlockParameters($parameters);
			$event->setUrlManager($httpEvent->getUrlManager());
			$eventManager->triggerEvent($event);
			$result = $event->getBlockResult();
		}
		catch (\Throwable $t)
		{
			$logging = $this->getApplication()->getLogging();
			$logging->error($blockLayout->getName(), $blockLayout->getId());
			$logging->throwable($t);
		}

		if (!($result instanceof BlockResult))
		{
			return null;
		}

		$templateName = $event->getParam('templateName');
		if (\is_string($templateName) && !$result->hasHtml())
		{
			$applicationServices = $event->getApplicationServices();
			$templateModuleName = $event->getParam('templateModuleName');
			if ($templateModuleName === null)
			{
				$sn = \explode('_', $blockLayout->getName());
				$templateModuleName = $sn[0] . '_' . $sn[1];
			}

			$relativePath = $applicationServices->getThemeManager()->getCurrent()
				->getTemplateRelativePath($templateModuleName, 'Blocks/' . $templateName);
			$attributes = $event->getParam('attributes', $attributes);
			if ($attributes instanceof \ArrayObject)
			{
				$attributes = $attributes->getArrayCopy();
			}
			elseif (!\is_array($attributes))
			{
				$attributes = [];
			}
			$templateManager = $applicationServices->getTemplateManager();
			try
			{
				$result->setHtml($templateManager->renderThemeTemplateFile($relativePath, $attributes));
			}
			catch (\Exception $e)
			{
				$error = 'Unable to render "' . $relativePath . '" template for block ' . $blockLayout->getName();
				$result->setHtml('<!-- ' . $error . ' -->');
				$this->getApplication()->getLogging()->error($error);
				$this->getApplication()->getLogging()->exception($e);
			}
		}
		if (!$result->hasHtml())
		{
			$result->setHtml('');
		}
		return $result;
	}

	/**
	 * @param \Change\Presentation\Layout\Block[] $blocks
	 */
	public function normalizeBlocksParameters(array $blocks)
	{
		foreach ($blocks as $block)
		{
			if ($block instanceof \Change\Presentation\Layout\Block)
			{
				$blockName = $block->getName();
				$blockInformation = $this->getBlockInformation($blockName);
				if ($blockInformation instanceof Information)
				{
					$label = $block->getLabel();
					$block->setLabel((!$label || $label === $blockName) ? $blockInformation->getLabel() : $label);

					$parameters = $block->getParameters();
					if (!is_array($parameters))
					{
						$parameters = [];
					}
					$parameters = $blockInformation->normalizeParameters($parameters);
					if (!is_array($parameters))
					{
						$parameters = [];
					}

					/** @var $validParameters \Change\Presentation\Blocks\ParameterInformation[] */
					$validParameters = [];
					$validParameters['TTL'] =
						new \Change\Presentation\Blocks\ParameterInformation('TTL', \Change\Documents\Property::TYPE_INTEGER, false, 60);

					foreach ($blockInformation->getParametersInformation() as $paramInfo)
					{
						$validParameters[$paramInfo->getName()] = $paramInfo;
					}

					if (!isset($parameters['TTL']) || !is_int($parameters['TTL']))
					{
						$parameters['TTL'] = (int)$validParameters['TTL']->getDefaultValue();
					}

					$fullyQualifiedTemplateName = $parameters['fullyQualifiedTemplateName'] ?? null;
					$template = $blockInformation->getTemplateInformation($fullyQualifiedTemplateName);
					if ($template)
					{
						if ($fullyQualifiedTemplateName && $fullyQualifiedTemplateName !== 'default:default')
						{
							$validParameters['fullyQualifiedTemplateName'] =
								new \Change\Presentation\Blocks\ParameterInformation('fullyQualifiedTemplateName',
									\Change\Documents\Property::TYPE_STRING, false, null);
						}

						foreach ($template->getParametersInformation() as $paramInfo)
						{
							$validParameters[$paramInfo->getName()] = $paramInfo;
						}
					}

					$normalizedParameters = [];
					foreach ($validParameters as $name => $validParameter)
					{
						$value = $validParameter->normalizeValue($parameters);
						if ($value !== null)
						{
							$normalizedParameters[$name] = $value;
						}
					}

					$block->setParameters($normalizedParameters);
				}
			}
		}
	}
}