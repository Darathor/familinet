<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Images;

/**
 * @name \Change\Presentation\Images\GDResizerEngine
 */
class GDResizerEngine
{
	/**
	 * @param $path
	 * @return array
	 */
	public function getImageSize($path)
	{
		$result = \getimagesize($path);
		if ($result === false)
		{
			$result = \getimagesizefromstring(\file_get_contents($path));
		}
		return $result ? ['width' => $result[0], 'height' => $result[1]] : ['height' => null, 'width' => null];
	}

	/**
	 * @param string $inputFileName
	 * @param string $formattedFileName
	 * @param integer $maxWidth
	 * @param integer $maxHeight
	 * @param string $compression
	 */
	public function resize($inputFileName, $formattedFileName, $maxWidth, $maxHeight, $compression)
	{
		switch ($compression)
		{
			case 'HIGH' :
				$quality = 85;
				break;
			case 'MEDIUM' :
				$quality = 90;
				break;
			case 'LOW' :
				$quality = 95;
				break;
			default :
				$quality = 100;
		}

		$sizeInfo = \getimagesize($inputFileName);
		$inputBlob = null;
		if ($sizeInfo === false)
		{
			$inputBlob = \file_get_contents($inputFileName);
			$sizeInfo = \getimagesizefromstring($inputBlob);
			if ($sizeInfo === false)
			{
				\copy($inputFileName, $formattedFileName);
				return;
			}
		}
		$imageType = $sizeInfo[2];
		if ($imageType !== \IMAGETYPE_JPEG && $imageType !== \IMAGETYPE_PNG)
		{
			\copy($inputFileName, $formattedFileName);
			return;
		}

		[$width, $height] = $this->computeImageSize($sizeInfo[0], $sizeInfo[1], $maxWidth, $maxHeight);
		if ($width == $sizeInfo[0] && $height == $sizeInfo[1])
		{
			if ($imageType === \IMAGETYPE_JPEG)
			{

				$imageSrc = $inputBlob ? \imagecreatefromstring($inputBlob) : \imagecreatefromjpeg($inputFileName);
				$inputBlob = null;
				\ob_start();
				\imagejpeg($imageSrc, null, $quality);
				if (!\file_put_contents($formattedFileName, \ob_get_clean()))
				{
					/** @noinspection PhpUsageOfSilenceOperatorInspection */
					@\unlink($formattedFileName);
				}
			}
			else
			{
				\copy($inputFileName, $formattedFileName);
			}
			return;
		}

		if ($imageType === \IMAGETYPE_PNG)
		{
			$imageSrc = $inputBlob ? \imagecreatefromstring($inputBlob) : \imagecreatefrompng($inputFileName);
			$inputBlob = null;
			$imageFormatted = \imagecreatetruecolor($width, $height);
			\imagealphablending($imageFormatted, false);
			\imagesavealpha($imageFormatted, true);
			\imagecopyresampled($imageFormatted, $imageSrc, 0, 0, 0, 0, $width, $height, $sizeInfo[0], $sizeInfo[1]);
			\ob_start();
			\imagepng($imageFormatted, null);
			if (!\file_put_contents($formattedFileName, \ob_get_clean()))
			{
				/** @noinspection PhpUsageOfSilenceOperatorInspection */
				@\unlink($formattedFileName);
			}
		}
		elseif ($imageType === \IMAGETYPE_JPEG)
		{
			$imageSrc = $inputBlob ? \imagecreatefromstring($inputBlob) : \imagecreatefromjpeg($inputFileName);
			$inputBlob = null;
			$imageFormatted = \imagecreatetruecolor($width, $height);
			\imagecopyresampled($imageFormatted, $imageSrc, 0, 0, 0, 0, $width, $height, $sizeInfo[0], $sizeInfo[1]);
			\ob_start();
			\imagejpeg($imageFormatted, null, $quality);
			if (!\file_put_contents($formattedFileName, \ob_get_clean()))
			{
				/** @noinspection PhpUsageOfSilenceOperatorInspection */
				@\unlink($formattedFileName);
			}
		}
	}

	/**
	 * @param $originalWidth
	 * @param $originalHeight
	 * @param $maxWidth
	 * @param $maxHeight
	 * @return array
	 */
	protected function computeImageSize($originalWidth, $originalHeight, $maxWidth, $maxHeight)
	{
		$resourceWidth = $originalWidth;
		$resourceHeight = $originalHeight;
		if ($maxWidth && ($originalWidth > $maxWidth))
		{
			$resourceWidth = $maxWidth;
			$resourceHeight = $resourceWidth * $originalHeight / $originalWidth;
		}

		if ($maxHeight && ($resourceHeight > $maxHeight))
		{
			$resourceHeight = $maxHeight;
			$resourceWidth = $resourceHeight * $originalWidth / $originalHeight;
		}
		$resourceWidth = \round($resourceWidth);
		$resourceHeight = \round($resourceHeight);
		return [\min($resourceWidth, $originalWidth), \min($resourceHeight, $originalHeight)];
	}
}