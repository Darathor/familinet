<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Interfaces;

/**
 * @name \Change\Presentation\Interfaces\PublicationMeta
 */
interface PublicationMeta
{
	/**
	 * @return string
	 */
	public function getType();

	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @return string
	 */
	public function getValue();
}