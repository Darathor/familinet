<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Interfaces;

/**
 * @name \Change\Presentation\Interfaces\PublicationMetas
 */
interface PublicationMetas
{
	/**
	 * @return string
	 */
	public function getMode();

	/**
	 * @return string|null
	 */
	public function getTitle();

	/**
	 * @return \Change\Presentation\Interfaces\PublicationMeta[]
	 */
	public function getMetas();
}