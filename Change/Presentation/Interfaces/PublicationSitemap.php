<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Interfaces;

/**
 * @name \Change\Presentation\Interfaces\PublicationSitemap
 */
interface PublicationSitemap
{
	/**
	 * @return string
	 */
	public function getFrequency();

	/**
	 * @return float
	 */
	public function getPriority();

	/**
	 * @param integer|\Change\Presentation\Interfaces\Website $website
	 * @return boolean
	 */
	public function isActiveForWebsite($website);
}