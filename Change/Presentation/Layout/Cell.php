<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Layout;

/**
 * @name \Change\Presentation\Layout\Cell
 */
class Cell extends Item
{
	/**
	 * @var integer
	 */
	protected $size;

	/**
	 * @var integer
	 */
	protected $offset;

	/**
	 * This property is not handled in toArray and fromArray and is manually set during the rendering using the values from the row and the template.
	 * @var string|null
	 */
	protected $displayColumnsFrom;

	/**
	 * @return int
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 * @return int
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'cell';
	}

	/**
	 * @return string
	 */
	public function getDisplayColumnsFrom()
	{
		return $this->displayColumnsFrom;
	}

	/**
	 * @param null|string $displayColumnsFrom
	 * @return $this
	 */
	public function setDisplayColumnsFrom($displayColumnsFrom)
	{
		$this->displayColumnsFrom = $displayColumnsFrom;
		return $this;
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function initialize(array $data)
	{
		parent::initialize($data);
		$this->size = $data['size'];
		$this->offset = $data['offset'] ?? 0;
	}

	public function toArray()
	{
		$result = parent::toArray();
		$result['size'] = $this->size;
		if ($this->offset)
		{
			$result['offset'] = $this->offset;
		}
		return $result;
	}
}