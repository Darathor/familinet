<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Pages;

/**
 * @name \Change\Presentation\Pages\DefaultPageResult
 */
class DefaultPageResult
{
	/**
	 * @param PageEvent $event
	 */
	public function onGetPageResult(PageEvent $event)
	{
		/* @var $page \Change\Presentation\Interfaces\Page */
		$page = $event->getPage();
		if (!($page instanceof \Change\Presentation\Interfaces\Page)
			|| !($result = $event->getPageResult()) || !($pageManager = $event->getPageManager()))
		{
			return;
		}

		$monitoring = $pageManager->getMonitoring();
		$pageTemplate = $page->getTemplate();
		$applicationServices = $event->getApplicationServices();

		if (!$result->getHttpStatusCode())
		{
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		$themeManager = $applicationServices->getThemeManager();
		$themeManager->setCurrent($pageTemplate->getTheme());
		$section = $page->getSection();
		$websiteId = ($section && $section->getWebsite()) ? $section->getWebsite()->getId() : null;
		$templateLayout = $pageTemplate->getContentLayout($websiteId);

		$navigationContext = [
			'websiteId' => $websiteId,
			'sectionId' => $section ? $section->getId() : null,
			'pageIdentifier' => $page->getIdentifier(),
			'themeName' => $pageTemplate->getTheme()->getName(),
			'LCID' => $applicationServices->getI18nManager()->getLCID()
		];
		$result->setNavigationContext($navigationContext);

		$application = $event->getApplication();
		if ($application->inDevelopmentMode())
		{
			$result->setJsonObject('debug', [
				'devMode' => true,
				'angularDevMode' => (bool)$application->getConfiguration('Change/Presentation/AngularDevMode')
			]);
		}

		$pageLayout = $page->getContentLayout();
		$containers = [];
		foreach ($templateLayout->getItems() as $item)
		{
			if ($item instanceof \Change\Presentation\Layout\Container)
			{
				$container = $pageLayout->getById($item->getId());
				if ($container)
				{
					$container->initIdPrefix($item->getIdPrefix());
					$containers[] = $container;
				}
			}
		}
		$pageLayout->setItems($containers);

		$blocks = array_merge($templateLayout->getBlocks(), $pageLayout->getBlocks());

		if (count($blocks))
		{
			$blockManager = $applicationServices->getBlockManager();

			$blockInputs = [];
			foreach ($blocks as $block)
			{
				/* @var $block \Change\Presentation\Layout\Block */
				/** @var array $bm */
				$bm = $monitoring ? ['n' => $block->getName(), 'p' => ['d' => microtime(true), 'm' => memory_get_usage()]] : null;
				$blockParameter = $blockManager->getParameters($block, $pageManager->getHttpWebEvent());
				$blockInputs[] = [$block, $blockParameter];
				if ($bm)
				{
					$bm['p']['d'] = microtime(true) - $bm['p']['d'];
					$bm['p']['m'] = memory_get_usage() - $bm['p']['m'];
					$monitoring['blocks'][$block->getId()] = $bm;
				}
			}
			$jsonParameters = [];
			$blockResults = [];
			foreach ($blockInputs as list($blockLayout, $parameters))
			{
				/** @var $blockLayout \Change\Presentation\Layout\Block */
				/** @var $parameters \Change\Presentation\Blocks\Parameters */
				$blockId = $blockLayout->getId();
				if ($bm = ($monitoring ? $monitoring['blocks'][$blockId] : null))
				{
					$bm['r'] = ['d' => microtime(true), 'm' => memory_get_usage()];
				}
				$blockResult = $blockManager->getResult($blockLayout, $parameters, $pageManager->getHttpWebEvent());
				if (isset($blockResult))
				{
					$blockResults[$blockId] = $blockResult;
				}
				if ($monitoring)
				{
					$bm['r']['d'] = microtime(true) - $bm['r']['d'];
					$bm['r']['m'] = memory_get_usage() - $bm['r']['m'];
					$bm['r']['t'] = $parameters->getTTL();
					$bm['r']['c'] = $parameters->getParameterValue('_cached') === true;
					$monitoring['blocks'][$blockId] = $bm;
				}
				$blockParamArray = $parameters->toArray();
				$jsonParameters[$blockId] = $blockParamArray;
			}
			$result->setBlockResults($blockResults);
			$result->setJsonObject('blockParameters', $jsonParameters);
		}

		$workspace = $application->getWorkspace();

		$themeManager->addPageResources($result, $pageTemplate, $blocks);
		$cacheTime = max($page->getModificationDate()->getTimestamp(), $pageTemplate->getModificationDate()->getTimestamp());
		$cachePath = $workspace->cachePath('twig', 'page', $result->getIdentifier() . ',' . $cacheTime . '.twig');
		if ($application->inDevelopmentMode() && file_exists($cachePath))
		{
			unlink($cachePath);
		}

		if (!file_exists($cachePath))
		{
			$em = $pageManager->getEventManager();
			$args = $em->prepareArgs(['page' => $page, 'pageTemplate' => $pageTemplate, 'templateLayout' => $templateLayout,
				'pageLayout' => $pageLayout]);
			$em->trigger('buildTemplateReplacements', $pageManager, $args);
			$templateReplacements =
				isset($args['templateReplacements']) && is_array($args['templateReplacements']) ? $args['templateReplacements'] : [];
			$htmlTemplate = str_replace(array_keys($templateReplacements), array_values($templateReplacements), $pageTemplate->getHtml());
			\Change\Stdlib\FileUtils::write($cachePath, $htmlTemplate);
		}

		if ($monitoring)
		{
			$monitoring = $monitoring->getArrayCopy();
			$monitoring['page']['c'] = false;
			$monitoring['page']['d'] = microtime(true) - $monitoring['page']['d'];
			$monitoring['page']['m'] = memory_get_usage() - $monitoring['page']['m'];
			$result->setMonitoring($monitoring);
		}

		$templateManager = $event->getApplicationServices()->getTemplateManager();
		$result->setHtml($templateManager->renderTemplateFile($cachePath, ['pageResult' => $result]));
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onBuildTemplateReplacements(\Change\Events\Event $event)
	{
		if ($event->getParam('templateReplacements') !== null)
		{
			return;
		}

		$templateLayout = $event->getParam('templateLayout');
		$pageLayout = $event->getParam('pageLayout');
		if ($templateLayout instanceof \Change\Presentation\Layout\Layout
			&& $pageLayout instanceof \Change\Presentation\Layout\Layout)
		{
			$twitterBootstrapHtml = new \Change\Presentation\Layout\TwitterBootstrapHtml();
			$callableTwigBlock = function (\Change\Presentation\Layout\Block $item) use ($twitterBootstrapHtml)
			{
				return '{{ pageResult.htmlBlock(\'' . $item->getId() . '\', '
				. var_export($twitterBootstrapHtml->getBlockClass($item), true) . ')|raw }}';
			};

			$templateReplacements = array_merge($twitterBootstrapHtml->getHtmlParts($templateLayout, $pageLayout, $callableTwigBlock),
				$twitterBootstrapHtml->getResourceParts());

			$event->setParam('templateReplacements', $templateReplacements);
		}
	}
}