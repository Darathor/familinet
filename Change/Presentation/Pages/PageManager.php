<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Pages;

use Change\Http\Web\Result\HtmlHeaderElement;

/**
 * @api
 * @name \Change\Presentation\Pages\PageManager
 */
class PageManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const DEFAULT_IDENTIFIER = 'PageManager';

	const EVENT_GET_FUNCTIONS = 'getFunctions';

	const EVENT_GET_PAGE_RESULT = 'getPageResult';

	/**
	 * @var \Change\Cache\CacheManager
	 */
	protected $cacheManager;

	/**
	 * @var \Change\Http\Web\Event
	 */
	protected $httpWebEvent;

	/**
	 * @var \Change\Http\Request
	 */
	protected $request;

	/**
	 * @var \Change\Http\Web\UrlManager
	 */
	protected $urlManager;

	/**
	 * @var \Change\User\AuthenticationManager
	 */
	protected $authenticationManager;

	/**
	 * @var \Change\Permissions\PermissionsManager
	 */
	protected $permissionsManager;

	/**
	 * @var \ArrayObject|null
	 */
	protected $monitoring;

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::DEFAULT_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/PageManager');
	}



	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$callback = function (PageEvent $event)
		{
			(new DefaultPageResult())->onGetPageResult($event);
		};
		$eventManager->attach(static::EVENT_GET_PAGE_RESULT, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			(new DefaultPageResult())->onBuildTemplateReplacements($event);
		};
		$eventManager->attach('buildTemplateReplacements', $callback, 5);
	}

	/**
	 * @param \Change\Http\Web\Event $httpWebEvent
	 * @return $this
	 */
	public function setHttpWebEvent(\Change\Http\Web\Event $httpWebEvent = null)
	{
		$this->httpWebEvent = $httpWebEvent;
		if ($httpWebEvent)
		{
			if (null === $this->request)
			{
				$this->setRequest($httpWebEvent->getRequest());
			}
			if (null === $this->urlManager)
			{
				$this->setUrlManager($httpWebEvent->getUrlManager());
			}
			if (null === $this->authenticationManager)
			{
				$this->setAuthenticationManager($httpWebEvent->getAuthenticationManager());
			}
			if (null === $this->permissionsManager)
			{
				$this->setPermissionsManager($httpWebEvent->getPermissionsManager());
			}
		}
		return $this;
	}

	/**
	 * @return \Change\Http\Web\Event
	 */
	public function getHttpWebEvent()
	{
		return $this->httpWebEvent;
	}

	/**
	 * @param \Change\User\AuthenticationManager $authenticationManager
	 * @return $this
	 */
	public function setAuthenticationManager($authenticationManager)
	{
		$this->authenticationManager = $authenticationManager;
		return $this;
	}

	/**
	 * @return \Change\User\AuthenticationManager
	 */
	public function getAuthenticationManager()
	{
		return $this->authenticationManager;
	}

	/**
	 * @param \Change\Permissions\PermissionsManager $permissionsManager
	 * @return $this
	 */
	public function setPermissionsManager($permissionsManager)
	{
		$this->permissionsManager = $permissionsManager;
		return $this;
	}

	/**
	 * @return \Change\Permissions\PermissionsManager
	 */
	public function getPermissionsManager()
	{
		return $this->permissionsManager;
	}

	/**
	 * @param \Change\Http\Request $request
	 * @return $this
	 */
	public function setRequest($request)
	{
		$this->request = $request;
		return $this;
	}

	/**
	 * @return \Change\Http\Request
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @param \Change\Http\Web\UrlManager $urlManager
	 * @return $this
	 */
	public function setUrlManager($urlManager)
	{
		$this->urlManager = $urlManager;
		return $this;
	}

	/**
	 * @return \Change\Http\Web\UrlManager
	 */
	public function getUrlManager()
	{
		return $this->urlManager;
	}

	/**
	 * @return \Change\Cache\CacheManager
	 */
	public function getCacheManager()
	{
		return $this->cacheManager;
	}

	/**
	 * @param \Change\Cache\CacheManager $cacheManager
	 * @return $this
	 */
	public function setCacheManager($cacheManager)
	{
		$this->cacheManager = $cacheManager;
		return $this;
	}

	/**
	 * @api
	 * @param \Change\Presentation\Interfaces\Page $page
	 * @return \Change\Http\Web\Result\Page|null
	 */
	public function getPageResult(\Change\Presentation\Interfaces\Page $page)
	{
		$webEvent = $this->getHttpWebEvent();
		if ($webEvent)
		{
			$application = $webEvent->getApplication();
			$cacheManager = $this->getCacheManager();
			$queryString = $this->getRequest()->getQuery()->toString();

			if ($this->getRequest()->isGet())
			{
				$monitoring = $application->getConfiguration()->getEntry('Change/Http/Web/Monitoring');
				$this->monitoring = $monitoring ? new \ArrayObject(['page' => ['d' => microtime(true), 'm' => memory_get_usage()]]) : null;
				$TTL = max(0, (int)$page->getTTL());
				if ($TTL && $cacheManager)
				{
					$options = ['ttl' => $TTL];
					$uid = $page->getIdentifier() . ' ' . $this->request->getPath() . '' . $queryString;
					$key = md5(serialize($uid));
					if ($cacheManager->hasEntry('PageManager', $key, $options))
					{
						$result = $cacheManager->getEntry('PageManager', $key);
						$this->addMonitoring($result, true);
					}
					else
					{
						$result = $this->dispatchGetPageResult($page, $TTL);
						$cacheManager->setEntry('PageManager', $key, $result, $options);
						$this->addMonitoring($result);
					}
					return $result;
				}
			}
			$result = $this->dispatchGetPageResult($page);
			$this->addMonitoring($result);
			return $result;
		}
		return null;
	}

	/**
	 * @api
	 * @return \ArrayObject|null
	 */
	public function getMonitoring()
	{
		return $this->monitoring;
	}

	/**
	 * @param \Change\Http\Web\Result\Page|null $result
	 * @param bool $cached
	 * @return \Change\Http\Web\Result\Page|null
	 */
	protected function addMonitoring($result, $cached = false)
	{
		if ($this->monitoring && $result instanceof \Change\Http\Web\Result\Page)
		{
			$monitoring = $this->monitoring->getArrayCopy();
			$monitoring['page']['c'] = $cached;
			$monitoring['page']['d'] = microtime(true) - $monitoring['page']['d'];
			$monitoring['page']['m'] = memory_get_usage() - $monitoring['page']['m'];
		}
		return $result;
	}

	/**
	 * @return array
	 */
	public function getFunctions()
	{
		$args = $this->getEventManager()->prepareArgs(['functions' => []]);
		$this->getEventManager()->trigger(static::EVENT_GET_FUNCTIONS, $this, $args);
		$functions = $args['functions'];
		return is_array($functions) ? $functions : [];
	}

	/**
	 * @param \Change\Presentation\Interfaces\Page $page
	 * @param integer $TTL
	 * @return \Change\Http\Web\Result\Page|null
	 */
	protected function dispatchGetPageResult($page, $TTL = 0)
	{
		$result = new \Change\Http\Web\Result\Page($page->getIdentifier());
		$result->getHeaders()->addHeaderLine('Content-Type: text/html;charset=utf-8');
		$base = $this->getUrlManager()->getByPathInfo(null)->normalize()->toString();
		$result->addNamedHeadAsString('base', new HtmlHeaderElement('base', ['href' => $base, 'target' => '_self']));
		$result->addNamedHeadAsString('generator',
			new HtmlHeaderElement('meta', ['name' => 'generator', 'content' => 'Familinet']));

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['page' => $page, 'pageResult' => $result, 'TTL' => $TTL]);
		$pageEvent = new PageEvent(static::EVENT_GET_PAGE_RESULT, $this, $args);
		$eventManager->triggerEvent($pageEvent);
		return $pageEvent->getPageResult();
	}
}