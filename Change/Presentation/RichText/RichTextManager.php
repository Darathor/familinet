<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\RichText;

/**
 * @name \Change\Presentation\RichText\RichTextManager
 */
class RichTextManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const DEFAULT_IDENTIFIER = 'Presentation.RichText';
	const EVENT_RENDER = 'render';

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::DEFAULT_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/RichTextManager');
	}

	/**
	 * @api
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$callback = function (\Change\Events\Event $event)
		{
			$html = $event->getParam('html');
			if ($html)
			{
				$event->setParam('html', $this->xssClean($html));
			}
		};
		$eventManager->attach(RichTextManager::EVENT_RENDER, $callback, 3);
	}

	/**
	 * @param \Change\Documents\RichtextProperty $richText
	 * @param string $profile 'Admin' or 'Website'
	 * @param array|null $context
	 * @return string
	 */
	public function render(\Change\Documents\RichtextProperty $richText, $profile, $context = null)
	{
		$eventManager = $this->getEventManager();
		$params = [
			'profile' => $profile,
			'richText' => $richText,
			'context' => $context
		];
		$params = $eventManager->prepareArgs($params);
		$eventManager->trigger(static::EVENT_RENDER, $this, $params);
		$html = $params['html'] ?? '';
		$richText->setHtml($html);
		return trim($html);
	}

	/**
	 * Inspired by: https://gist.github.com/mbijon/1098477
	 * @param string $html
	 * @return string
	 */
	public function xssClean($html)
	{
		// TODO: find a better way to avoid XSS than a blacklist based on regular expressions.

		// Fix &entity\n;
		$html = str_replace(['&amp;', '&lt;', '&gt;'], ['&amp;amp;', '&amp;lt;', '&amp;gt;'], $html);
		$html = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $html);
		$html = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $html);
		$html = html_entity_decode($html, ENT_COMPAT, 'UTF-8');

		// Remove any attribute starting with "on" or xmlns
		$html =	preg_replace("#<([^><]+?)([^a-z_\-]on\w*|xmlns)(\s*=\s*[^><]*)([><]*)#i", "<\\1\\4", $html); // Regex From CodeIgniter. Modified for #790

		// Remove javascript: and vbscript: protocols
		$html = preg_replace(
			'#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu',
			'$1=$2nojavascript...',
			$html
		);
		$html = preg_replace(
			'#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu',
			'$1=$2novbscript...',
			$html
		);
		$html = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $html);

		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$html = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $html);
		$html = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $html);
		$html = preg_replace(
			'#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu',
			'$1>',
			$html
		);

		// Remove namespaced elements (we do not need them)
		$html = preg_replace('#</*\w+:\w[^>]*+>#i', '', $html);

		do
		{
			// Remove really unwanted tags
			$old_data = $html;
			$html = preg_replace(
				'#</*(?:applet|b(?:ase|gsound|link|utton)|embed|form|frame(?:set)?|i(?:frame|layer)|input|l(?:ayer|ink)|meta|object|s(?:elect|cript|tyle)|t(?:extarea|itle)|xml)[^>]*+>#i',
				'',
				$html
			);
		}
		while ($old_data !== $html);

		// we are done...
		return $html;
	}
}