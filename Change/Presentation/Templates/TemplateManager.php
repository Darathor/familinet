<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Templates;

/**
 * @api
 * @name \Change\Presentation\Templates\TemplateManager
 */
class TemplateManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const DEFAULT_IDENTIFIER = 'TemplateManager';
	public const EVENT_REGISTER_EXTENSIONS = 'registerExtensions';

	/**
	 * @var string
	 */
	protected $cachePath;

	/**
	 * @var \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	protected $extensions;

	/**
	 * @var \Change\Presentation\Themes\ThemeManager
	 */
	protected $themeManager;

	/**
	 * @var \Twig\Loader\LoaderInterface
	 */
	protected $twigLoader;


	/**
	 * @var \Twig\Environment
	 */
	protected $twigEnvironment;


	/**
	 * @return \Change\Workspace
	 */
	protected function getWorkspace()
	{
		return $this->getApplication()->getWorkspace();
	}

	/**
	 * @param \Change\Presentation\Themes\ThemeManager $themeManager
	 * @return $this
	 */
	public function setThemeManager(\Change\Presentation\Themes\ThemeManager $themeManager)
	{
		$this->themeManager = $themeManager;
		return $this;
	}

	/**
	 * @return \Change\Presentation\Themes\ThemeManager
	 */
	protected function getThemeManager()
	{
		return $this->themeManager;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::DEFAULT_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/TemplateManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(static::EVENT_REGISTER_EXTENSIONS, [$this, 'onDefaultRegisterExtensions'], 5);
	}

	/**
	 * @param array $context
	 * @return \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	public function registerExtensions(array $context = [])
	{
		if ($this->extensions === null)
		{
			$this->extensions = [];
		}

		$em = $this->getEventManager();
		$arguments = $em->prepareArgs(['extensions' => new \ArrayObject(), 'context' => $context]);
		$this->getEventManager()->trigger(static::EVENT_REGISTER_EXTENSIONS, $this, $arguments);
		if ($arguments['extensions'] instanceof \ArrayObject)
		{
			foreach ($arguments['extensions'] as $extension)
			{
				if ($extension instanceof \Change\Presentation\Templates\Twig\ExtensionInterface)
				{
					$this->twigEnvironment = null;
					$this->extensions[$extension->getName()] = $extension;
				}
			}
		}

		return $this->extensions;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultRegisterExtensions(\Change\Events\Event $event)
	{
		/** @var \ArrayObject $extensions */
		$extensions = $event->getParam('extensions');
		$extension = new Twig\Extension($event->getApplicationServices()->getI18nManager(),
				$event->getApplication()->getConfiguration());
		$extensions->append($extension);
	}

	/**
	 * @return \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	public function getExtensions()
	{
		if ($this->extensions === null)
		{
			$this->registerExtensions();
		}
		return \array_values($this->extensions);
	}

	/**
	 * @param \Change\Presentation\Templates\Twig\ExtensionInterface $extension
	 * @return $this
	 */
	public function addExtension($extension)
	{
		$this->getExtensions();
		if ($extension instanceof \Change\Presentation\Templates\Twig\ExtensionInterface)
		{
			$this->extensions[$extension->getName()] = $extension;
		}
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getCachePath()
	{
		if ($this->cachePath === null)
		{
			$this->cachePath = $this->getWorkspace()->cachePath('Templates', 'Compiled');
			\Change\Stdlib\FileUtils::mkdir($this->cachePath);
		}
		return $this->cachePath;
	}

	/**
	 * @api
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 */
	public function renderTemplateFile($pathName, array $attributes)
	{
		$loader = new \Twig\Loader\FilesystemLoader(dirname($pathName));
		$twig = new \Twig\Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render(\basename($pathName), $attributes);
	}

	/**
	 * @api
	 * @param \Change\Presentation\Interfaces\Theme|null $theme
	 * @return \Change\Presentation\Templates\Twig\Loader
	 */
	public function buildThemeTwigLoader(\Change\Presentation\Interfaces\Theme $theme = null)
	{
		if (!$theme)
		{
			$theme = $this->getThemeManager()->getCurrent();
		}

		$themeName = null;
		$themeNames = [];
		while ($theme)
		{
			$themeName = $theme->getName();
			$themeNames[] = $themeName;
			$theme = $theme->getParentTheme();
		}

		if ($themeName !== \Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME)
		{
			$themeNames[] = \Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME;
		}
		return new \Change\Presentation\Templates\Twig\Loader($this->getWorkspace(), $themeNames);
	}

	/**
	 * @api
	 * @return \Twig\Loader\LoaderInterface
	 */
	public function getTwigLoader()
	{
		if ($this->twigLoader === null)
		{
			$this->setTwigLoader($this->buildThemeTwigLoader());
		}
		return $this->twigLoader;
	}

	/**
	 * @api
	 * @param \Twig\Loader\LoaderInterface $twigLoader
	 * @return $this
	 */
	public function setTwigLoader($twigLoader)
	{
		$this->twigLoader = $twigLoader;
		return $this;
	}

	/**
	 * @api
	 * @param \Twig\Loader\LoaderInterface|null $twigLoader
	 * @return \Twig\Environment
	 */
	public function buildTwigEnvironment(\Twig\Loader\LoaderInterface $twigLoader = null)
	{
		if (!$twigLoader)
		{
			$twigLoader = $this->getTwigLoader();
		}
		$twigEnvironment = new \Twig\Environment($twigLoader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twigEnvironment->addExtension($extension);
		}
		return $twigEnvironment;
	}

	/**
	 * @api
	 * @return \Twig\Environment
	 */
	public function getTwigEnvironment()
	{
		if ($this->twigEnvironment === null)
		{
			$this->setTwigEnvironment($this->buildTwigEnvironment());
		}
		return $this->twigEnvironment;
	}

	/**
	 * @api
	 * @param \Twig\Environment $twigEnvironment
	 * @return $this
	 */
	public function setTwigEnvironment($twigEnvironment)
	{
		$this->twigEnvironment = $twigEnvironment;
		return $this;
	}

	/**
	 * @param string $relativePath
	 * @param array $attributes
	 * @param \Twig\Environment $twigEnvironment
	 * @return string
	 * @throws \Twig\Error\LoaderError
	 */
	public function renderThemeTemplateFile($relativePath, array $attributes, \Twig\Environment $twigEnvironment = null)
	{
		if (!$twigEnvironment)
		{
			$twigEnvironment = $this->getTwigEnvironment();
		}
		return $twigEnvironment->render($relativePath, $attributes);
	}
}