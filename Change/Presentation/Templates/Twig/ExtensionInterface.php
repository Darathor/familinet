<?php
/**
 * Copyright (C) 2019 Darathor
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Templates\Twig;

/**
 * @name \Change\Presentation\Templates\Twig\ExtensionInterface
 */
interface ExtensionInterface extends \Twig\Extension\ExtensionInterface
{
	/**
	 * Returns the name of the extension.
	 * @return string The extension name
	 */
	public function getName();
}