<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Templates\Twig;

/**
 * @name \Change\Presentation\Templates\Twig\Loader
 */
class Loader extends \Twig\Loader\FilesystemLoader
{
	public function __construct(\Change\Workspace $workspace, array $themesName = ['Rbs_Base'])
	{
		$themeName = array_shift($themesName);
		$path = $workspace->compilationPath('Themes', str_replace('_', DIRECTORY_SEPARATOR, $themeName));
		parent::__construct([$path]);

		while ($themeName = array_shift($themesName))
		{
			$path = $workspace->compilationPath('Themes', str_replace('_', DIRECTORY_SEPARATOR, $themeName));
			$this->addPath($path);
			$this->addPath($path, $themeName);
		}
	}

	/**
	 * @param string $relativeName
	 * @return string|null
	 */
	public function getTemplateFilePath($relativeName)
	{
		$relativeName = ltrim($relativeName, '/\\');
		foreach ($this->getPaths() as $path)
		{
			$templatePath = $path . DIRECTORY_SEPARATOR . $relativeName;
			if (is_file($templatePath))
			{
				return $templatePath;
			}
		}
		return null;
	}
}