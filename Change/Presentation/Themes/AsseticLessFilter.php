<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Themes;

/**
 * @name \Change\Presentation\Themes\AsseticLessFilter
 */
class AsseticLessFilter implements \Assetic\Filter\DependencyExtractorInterface
{
	/**
	 * @var string
	 */
	private $formatter;

	/**
	 * @var string
	 */
	private $cssAssetsBaseURI;

	/**
	 * @var string[]
	 */
	protected $importPaths = [];

	/**
	 * @var string
	 */
	protected $cacheDir;

	/**
	 * @param string|null $cacheDir
	 * @param array $importPaths
	 * @param string $cssAssetsBaseURI
	 */
	public function __construct($cacheDir = null, array $importPaths, $cssAssetsBaseURI)
	{
		foreach ($importPaths as $importPath)
		{
			if (is_dir($importPath))
			{
				$this->importPaths[$importPath] = '';
			}
		}

		if ($cacheDir && is_dir($cacheDir))
		{
			$this->cacheDir = $cacheDir;
		}

		$this->cssAssetsBaseURI = $cssAssetsBaseURI;
	}

	/**
	 * @param string $formatter One of "lessjs", "compressed", or "classic".
	 */
	public function setFormatter($formatter)
	{
		$this->formatter = $formatter;
	}

	/**
	 * @param \Assetic\Asset\AssetInterface $asset
	 * @throws \Exception
	 */
	public function filterLoad(\Assetic\Asset\AssetInterface $asset)
	{
		$root = $asset->getSourceRoot();
		$path = $asset->getSourcePath();
		if (!$root || !$path)
		{
			return;
		}
		$lessFilePath = $root . '/' . $path;
		$options = [];
		switch ($this->formatter)
		{
			case 'compressed':
				$options['compress'] = true;
				break;
		}

		$paths = $this->importPaths;
		$path = dirname($lessFilePath);
		if (!array_key_exists($path, $paths))
		{
			$paths = array_merge([$path => ''], $paths);
		}

		if ($this->cacheDir)
		{
			$options['import_dirs'] = $paths;
			\Less_Cache::$cache_dir = $this->cacheDir;
			$to_cache = [$lessFilePath => ''];
			$css_file_name = \Less_Cache::Get($to_cache, $options, ['css-assets-base-uri' => '"'.$this->cssAssetsBaseURI.'"']);
			$css = file_get_contents($this->cacheDir . DIRECTORY_SEPARATOR . $css_file_name);
			$asset->setContent($css);
		}
		else
		{
			$parser = new \Less_Parser($options);
			$parser->SetImportDirs($paths);
			$parser->ModifyVars(['css-assets-base-uri' => '"'.$this->cssAssetsBaseURI.'"']);
			$parser->parse($asset->getContent());
			$asset->setContent($parser->getCss());
		}
	}

	/**
	 * @param \Assetic\Asset\AssetInterface $asset
	 */
	public function filterDump(\Assetic\Asset\AssetInterface $asset)
	{
	}

	/**
	 * @param \Assetic\Factory\AssetFactory $factory
	 * @param string $content
	 * @param null $loadPath
	 * @return array
	 */
	public function getChildren(\Assetic\Factory\AssetFactory $factory, $content, $loadPath = null)
	{
		$loadPaths = [];
		if (null !== $loadPath)
		{
			$loadPaths[] = $loadPath;
		}

		if (empty($loadPaths))
		{
			return [];
		}

		$children = [];
		foreach (\Assetic\Util\LessUtils::extractImports($content) as $reference)
		{
			if ('.css' === substr($reference, -4))
			{
				// skip normal css imports
				// todo: skip imports with media queries
				continue;
			}

			if ('.less' !== substr($reference, -5))
			{
				$reference .= '.less';
			}

			foreach ($loadPaths as $loadPath)
			{
				if (file_exists($file = $loadPath . '/' . $reference))
				{
					$coll = $factory->createAsset($file, [], ['root' => $loadPath]);
					foreach ($coll as $leaf)
					{
						$leaf->ensureFilter($this);
						$children[] = $leaf;
						goto next_reference;
					}
				}
			}

			next_reference:
		}
		return $children;
	}
}
