<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Presentation\Themes;

use Change\Presentation\Interfaces\Template;
use Change\Presentation\Layout\Layout;

class DefaultPageTemplate implements Template
{
	/**
	 * @var \Change\Presentation\Interfaces\Theme
	 */
	protected $theme;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $twigFilePath;

	/**
	 * @param DefaultTheme $theme
	 * @param string $name
	 */
	public function __construct($theme, $name)
	{
		$this->theme = $theme;
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	/**
	 * @return \Change\Presentation\Themes\DefaultTheme
	 */
	public function getTheme()
	{
		return $this->theme;
	}

	public function getCode()
	{
		return 'Change_DefaultPageTemplate';
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	protected function getTwigFilePath()
	{
		if ($this->twigFilePath === null)
		{
			$path = 'Layout/Template/' . $this->getName() . '.twig';
			$filePath = $this->getTheme()->getResourceFilePath($path);
			if (!is_readable($filePath))
			{
				throw new \RuntimeException('Layout/Template/' . $this->getName() . '.twig resource not found', 999999);
			}
			$this->twigFilePath = $filePath;
		}
		return $this->twigFilePath;
	}

	/**
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getHtml()
	{
		return file_get_contents($this->getTwigFilePath());
	}

	/**
	 * @throws \RuntimeException
	 * @param integer $websiteId
	 * @return \Change\Presentation\Layout\Layout
	 */
	public function getContentLayout($websiteId = null)
	{
		$path = 'Layout/Template/' . $this->getName() . '.json';
		$filePath = $this->getTheme()->getResourceFilePath($path);
		if (!is_readable($filePath))
		{
			throw new \RuntimeException('Layout/Template/' . $this->getName() . '.json resource not found', 999999);
		}
		$config = json_decode(file_get_contents($filePath), true);
		return new Layout($config);
	}

	/**
	 * @return \Datetime
	 */
	public function getModificationDate()
	{
		return \DateTime::createFromFormat('U', filemtime($this->getTwigFilePath()));
	}

	/**
	 * @return boolean
	 */
	public function isMailSuitable()
	{
		return false;
	}
}