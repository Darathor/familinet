<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change\Presentation\Themes;

/**
 * @name \Change\Presentation\Themes\JSMinFilter
 */
class JSMinFilter implements \Assetic\Filter\FilterInterface
{
	public function filterLoad(\Assetic\Asset\AssetInterface $asset)
	{
	}

	public function filterDump(\Assetic\Asset\AssetInterface $asset)
	{
		$asset->setContent(\JSMin\JSMin::minify($asset->getContent()));
	}
}