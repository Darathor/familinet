<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change;

/**
 * @name \Change\Saas
 */
class Saas
{
	const TARGET_PROD = 'PROD';
	const TARGET_PRE_PROD = 'PRE_PROD';
	const TARGET_DEV = 'DEV';

	const STEP_BUILD_IMG = 'BUILD_IMG';
	const STEP_RUNTIME_CONF = 'RUNTIME_CONF';
	const STEP_COMPILE_CONF = 'COMPILE_CONF';

	/**
	 * @var string
	 */
	private $script;

	/**
	 * @var
	 */
	private $projectPath;

	/**
	 * @var string 'PROD', 'PRE_PROD'
	 */
	private $target;

	/**
	 * @var string 'BUILD_IMG', 'COMPILE_CONF', 'RUNTIME_CONF'
	 */
	private $step;

	/**
	 * @var string
	 */
	private $configName = 'instance';


	protected function showHelp()
	{
		echo 'php ', $this->script, ' --target=PROD|PRE_PROD --configName=[instance] --step=BUILD_IMG|COMPILE_CONF|RUNTIME_CONF', PHP_EOL;
	}

	/**
	 * @param string $msg
	 * @param int $exitCode
	 */
	protected function error($msg, $exitCode = 1)
	{
		echo "\033[0;31m", 'ERROR: ', $msg, "\033[0m", PHP_EOL;
		if ($exitCode)
		{
			exit($exitCode);
		}
	}

	/**
	 * @return bool
	 */
	protected function inProd()
	{
		return $this->target === self::TARGET_PROD;
	}

	/**
	 * @return bool
	 */
	protected function inPreProd()
	{
		return $this->target === self::TARGET_PRE_PROD;
	}

	/**
	 * @return bool
	 */
	protected function inDev()
	{
		return $this->target === self::TARGET_DEV;
	}

	/**
	 * @param array $argv
	 * @return array
	 */
	protected function parseArgv(array $argv)
	{
		$arguments = [];
		array_shift($argv);
		foreach ($argv as $arg)
		{
			if (strpos($arg, '--') === 0 && (($eq = strpos($arg, '=')) > 2))
			{
				$n = substr($arg, 2, $eq - 2);
				$v = substr($arg, $eq + 1);
				$arguments[$n] = $v;
			}
		}
		return $arguments;
	}

	/**
	 * @param array $argv
	 */
	public function __construct($argv)
	{
		$this->script = $argv[0] ?? __FILE__;
		$arguments = $this->parseArgv($argv);

		if (!$arguments)
		{
			$this->showHelp();
			exit(0);
		}

		$this->target = $arguments['target'] ?? null;
		if (!in_array($this->target, [self::TARGET_PROD, self::TARGET_PRE_PROD, self::TARGET_DEV]))
		{
			$this->showHelp();
			exit(1);
		}

		$this->step = $arguments['step'] ?? null;
		if (!in_array($this->step, [self::STEP_BUILD_IMG, self::STEP_COMPILE_CONF, self::STEP_RUNTIME_CONF]))
		{
			$this->showHelp();
			exit(1);
		}

		$this->configName = $arguments['configName'] ?? ($this->inPreProd() ? 'preprod' : 'instance');
		$this->projectPath = dirname(__DIR__) . DIRECTORY_SEPARATOR;
	}

	/**
	 *
	 */
	public function __invoke()
	{
		echo 'Project Path: ', $this->projectPath, PHP_EOL;
		echo 'Env: ', $this->target, PHP_EOL;
		echo 'Config: ', $this->configName, PHP_EOL;
		echo 'Command: ', $this->step, PHP_EOL, PHP_EOL;

		try
		{
			switch ($this->step)
			{
				case self::STEP_BUILD_IMG:
					$plugins = $this->getPlugins();
					if (!$this->inDev())
					{
						unset($plugins['Rbs_Dev']);
					}
					$this->preInstall($plugins);
					$this->postInstall($plugins);
					break;
				case self::STEP_COMPILE_CONF:
					$this->compileConf();
					break;
				case self::STEP_RUNTIME_CONF:
					$this->runtimeConf();
					break;
				default:
					$this->error('Not implemented.');
			}
		}
		catch (\Throwable $t)
		{
			$this->error($t->getMessage(), 255);
		}
		echo PHP_EOL, 'done.', PHP_EOL;
	}

	/**
	 * @param array ...$arg
	 * @return string
	 */
	private function buildPath(...$arg)
	{
		return $this->projectPath . implode(DIRECTORY_SEPARATOR, $arg);
	}

	protected function compileConf()
	{
		$pluginsRegistration = $this->getPluginsRegistration();
		$valid = [];
		foreach ($pluginsRegistration as $r)
		{
			if ($r['type'] === 'module' && $r['vendor'] === 'Rbs' && $r['name'] === 'Dev')
			{
				continue;
			}
			$valid[] = $r;
		}
		$this->writePluginsRegistration($valid);
		unset($pluginsRegistration, $valid);

		$confPath = $this->buildPath('Change', 'Configuration', 'Assets', 'project.json');
		echo 'Read file: ', $confPath, PHP_EOL;
		$conf = json_decode(file_get_contents($confPath), true);

		$projectAutoGenConf = $this->buildPath('App', 'Config', 'project.autogen.json');
		echo 'Append file: ', $projectAutoGenConf, PHP_EOL;
		$confToMerge = json_decode(file_get_contents($projectAutoGenConf), true);
		if (!is_array($confToMerge))
		{
			$this->error('Invalid conf file: ' . $projectAutoGenConf);
		}
		$conf = $this->merge($conf, $confToMerge);

		$projectConf = $this->buildPath('App', 'Config', 'project.json');
		echo 'Append file: ', $projectConf, PHP_EOL;
		$confToMerge = json_decode(file_get_contents($projectConf), true);
		if (!is_array($confToMerge))
		{
			$this->error('Invalid conf file: ' . $projectConf);
		}
		$conf = $this->merge($conf, $confToMerge);

		$targetConf = $this->buildPath('App', 'Config', 'project.'.$this->configName.'.json');
		echo 'Append file: ', $targetConf, PHP_EOL;
		$confToMerge = json_decode(file_get_contents($targetConf), true);
		if (!is_array($confToMerge))
		{
			$this->error('Invalid conf file: ' . $targetConf);
		}
		$conf = $this->merge($conf, $confToMerge);

		$this->checkFake($conf);

		$path = $this->buildPath('App', 'Config', 'project.compiled.' . $this->configName . '.json');
		echo 'Writing file: ', $path, PHP_EOL;
		if (!file_put_contents($path, $this->encodeJson($conf)))
		{
			$this->error('Unable to write file: ' . $path);
		}
	}

	protected function runtimeConf()
	{
		$compiledConf = $this->buildPath('App', 'Config', 'project.compiled.' . $this->configName . '.json');
		if (!\is_readable($compiledConf))
		{
			$this->error('File not found: ' . $compiledConf);
		}
		$conf = json_decode(file_get_contents($compiledConf), true);
		if (!is_array($conf))
		{
			$this->error('Invalid conf file: ' . $compiledConf);
		}

		$code = '<?php' . PHP_EOL . ' return ' . var_export($conf, true) . ';';
		$path = $this->buildPath('Compilation', 'Change', 'configuration.' . $this->configName . '.php');
		echo 'Writing file: ', $path, PHP_EOL;
		if (!file_put_contents($path, $code))
		{
			$this->error('Unable to write file: ' . $path);
		}
	}

	/**
	 * @param array $a
	 * @param array $b
	 * @return array
	 */
	public function merge(array $a, array $b)
	{
		foreach ($b as $key => $value)
		{
			if (array_key_exists($key, $a) || isset($a[$key]))
			{
				if (is_array($value) && is_array($a[$key]))
				{
					$a[$key] = $this->merge($a[$key], $value);
				}
				else
				{
					$a[$key] = $value;
				}
			}
			else
			{
				$a[$key] = $value;
			}
		}
		return $a;
	}

	protected function checkFake($conf)
	{
		if ($this->inProd())
		{
			if (!($conf['Change']['Application']['uuid'] ?? false))
			{
				$this->error('Invalid conf entry: Change/Application/uuid');
			}
			if ($conf['Change']['Application']['development-mode'] ?? false)
			{
				$this->error('Invalid conf entry: Change/Application/development-mode');
			}
			if (!($conf['Change']['Cache']['Cache'] ?? false))
			{
				$this->error('Invalid conf entry: Change/Cache/Cache');
			}
			if ($conf['Change']['Mail']['fakemail'] ?? false)
			{
				$this->error('Invalid conf entry: Change/Mail/fakemail');
			}
			if ($conf['Change']['Sms']['fakeSms'] ?? false)
			{
				$this->error('Invalid conf entry: Change/Sms/fakeSms');
			}
			if ($conf['Change']['Svi']['fakeSvi'] ?? false)
			{
				$this->error('Invalid conf entry: Change/Svi/fakeSvi');
			}
		}
		else
		{
			if (!($conf['Change']['Mail']['fakemail'] ?? false))
			{
				$this->error('Invalid conf entry: Change/Mail/fakemail');
			}
			if (!($conf['Change']['Sms']['fakeSms'] ?? false))
			{
				$this->error('Invalid conf entry: Change/Sms/fakeSms');
			}
			if (!($conf['Change']['Svi']['fakeSvi'] ?? false))
			{
				$this->error('Invalid conf entry: Change/Svi/fakeSvi');
			}
		}
	}

	/**
	 * @param array $array
	 * @return string
	 */
	protected function encodeJson(array $array)
	{
		return str_replace('    ', '	', json_encode($array, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT));
	}

	/**
	 * @param array $plugins
	 */
	protected function preInstall(array $plugins)
	{
		$compiledConfPath = $this->buildPath('App', 'Config', 'project.build.json');

		$conf = [
			'Change' => [
				'Application' => [
					'development-mode' => false
				]
			]
		];

		if (!file_put_contents($compiledConfPath, $this->encodeJson($conf)))
		{
			$this->error('Unable to write file: ' . $compiledConfPath);
		}

		$script = ['#!/bin/bash'];
		$script[] = 'set -e';

		$cmd = 'export CHANGE_INSTANCE_CONFIG_FILENAME="project.build.json"';
		$script[] = 'echo "' . str_replace('"', '\\"', $cmd) . '"';
		$script[] = $cmd;
		$script[] = '';

		$cmd = 'export CHANGE_TMP="/tmp"';
		$script[] = 'echo "' . str_replace('"', '\\"', $cmd) . '"';
		$script[] = $cmd;
		$script[] = '';

		$cmd = 'php bin/change.phar change:set-document-root www';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;
		$script[] = '';
		usort($plugins, [$this, 'comparePlugin']);

		$cmd = 'php bin/change.phar change:register-plugin --all -f';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		$script[] = '';
		$corePackage = $this->getRbsPackage('Core', $plugins);
		$eComPackage = $this->getRbsPackage('ECom', $plugins);
		if ($corePackage)
		{
			$cmd = 'php bin/change.phar change:install-package -e Rbs Core --steps=Application';
			$script[] = 'echo "' . $cmd . '"';
			$script[] = $cmd;
		}
		if ($eComPackage)
		{
			$cmd = 'php bin/change.phar change:install-package -e Rbs ECom --steps=Application';
			$script[] = 'echo "' . $cmd . '"';
			$script[] = $cmd;
		}
		if ($plugins)
		{
			foreach ($plugins as $plugin)
			{
				$cmd =
					'php bin/change.phar change:install-plugin --type=' . $plugin['type'] . ' --vendor=' . $plugin['vendor'] . ' ' . $plugin['name']
					. ' --steps=Application';
				$script[] = 'echo "' . $cmd . '"';
				$script[] = $cmd;
			}
		}
		$script[] = '';
		$cmd = 'php bin/change.phar change:compile-documents';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		$cmd = 'php Change/Saas.php --target=' . $this->target . ' --step=COMPILE_CONF';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		$cmd = 'php composer.phar dump-autoload -o';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		echo 'Writing file: ', $this->projectPath, 'pre_install.sh', PHP_EOL;
		if (!file_put_contents($this->projectPath . 'pre_install.sh', implode(PHP_EOL, $script)))
		{
			$this->error('Unable to write file: ' . $this->projectPath . 'pre_install.sh');
		}
	}

	/**
	 * @param array $plugins
	 */
	protected function postInstall(array $plugins)
	{
		$corePackage = $this->getRbsPackage('Core', $plugins);
		$eComPackage = $this->getRbsPackage('ECom', $plugins);

		$script = ['#!/bin/bash'];
		$script[] = 'set -e';

		$cmd = 'export CHANGE_TMP="/tmp"';
		$script[] = 'echo "' . str_replace('"', '\\"', $cmd) . '"';
		$script[] = $cmd;
		$script[] = '';

		$cmd = 'php bin/change.phar change:generate-db-schema -m';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		$cmd = 'php bin/change.phar change:refresh-i18n';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		if ($corePackage)
		{
			$cmd = 'php bin/change.phar change:install-package -e Rbs Core --steps=Services';
			$script[] = 'echo "' . $cmd . '"';
			$script[] = $cmd;
		}
		if ($eComPackage)
		{
			$cmd = 'php bin/change.phar change:install-package -e Rbs ECom --steps=Services';
			$script[] = 'echo "' . $cmd . '"';
			$script[] = $cmd;
		}
		if ($plugins)
		{
			usort($plugins, [$this, 'comparePlugin']);
			foreach ($plugins as $plugin)
			{
				$cmd =
					'php bin/change.phar change:install-plugin --type=' . $plugin['type'] . ' --vendor=' . $plugin['vendor'] . ' ' . $plugin['name']
					. ' --steps=Services';
				$script[] = 'echo "' . $cmd . '"';
				$script[] = $cmd;
			}
		}
		$cmd = 'php bin/change.phar change:generate-db-indexes -r';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;
		$cmd = 'php bin/change.phar change:patch -r';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;
		$cmd = 'php bin/change.phar rbs_notification:install-notifications';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;
		$cmd = 'php bin/change.phar rbs_notification:configure';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;
		$cmd = 'php bin/change.phar change:refresh-i18n';
		$script[] = 'echo "' . $cmd . '"';
		$script[] = $cmd;

		echo 'Writing file: ', $this->projectPath, 'post_install.sh', PHP_EOL;
		if (!file_put_contents($this->projectPath . 'post_install.sh', implode(PHP_EOL, $script)))
		{
			$this->error('Unable to write file: ' . $this->projectPath . 'post_install.sh');
		}
	}

	/**
	 * @param array $a
	 * @param array $b
	 * @return int
	 */
	protected function comparePlugin(array $a, array $b)
	{
		if ($a['type'] !== $b['type'])
		{
			return $a['type'] === 'module' ? -1 : 1;
		}
		if ($a['vendor'] !== $b['vendor'])
		{
			if ($a['vendor'] === 'Rbs')
			{
				return -1;
			}
			if ($b['vendor'] === 'Rbs')
			{
				return 1;
			}
			if ($a['vendor'] === 'Project')
			{
				return 1;
			}
			if ($b['vendor'] === 'Project')
			{
				return -1;
			}
		}
		return $a['name'] === $b['name'] ? 0 : $a['name'] > $b['name'];
	}

	/**
	 * @return array
	 */
	protected function getPluginsRegistration()
	{
		$filePath = $this->buildPath('App', 'Config', 'plugins.json');
		if (file_exists($filePath))
		{
			$pluginsRegistration = json_decode(file_get_contents($filePath), true);
			if (!$pluginsRegistration)
			{
				$this->error('Invalid Plugins registration file: ' . $filePath);
			}
			return $pluginsRegistration;
		}
		$this->error('Plugins registration file not found: ' . $filePath);
		return [];
	}

	/**
	 * @param array $pluginsRegistration
	 */
	protected function writePluginsRegistration(array $pluginsRegistration)
	{
		$filePath = $this->buildPath('App', 'Config', 'plugins.saas.json');
		if (!file_put_contents($filePath, $this->encodeJson($pluginsRegistration)))
		{
			$this->error('Unable to write plugins registration file: ' . $filePath);
		}
	}

	/**
	 * @return array
	 */
	protected function getPlugins()
	{
		$plugins = [];
		$files = glob($this->projectPath . 'Plugins/Modules/*/*/plugin.json');
		if (is_array($files))
		{
			foreach ($files as $file)
			{
				$plugin = json_decode(file_get_contents($file), true);
				if (is_array($plugin) && $plugin['type'] === 'module')
				{
					$plugins[$plugin['vendor'] . '_' . $plugin['name']] = $plugin;
				}
				else
				{
					$this->error('Invalid Plugin in: ' . $file);
				}
			}
		}
		else
		{
			$this->error('Invalid Plugins path: ' . $this->projectPath . 'Plugins/Modules/*/*/plugin.json');
		}

		$files = glob($this->projectPath . 'Plugins/Themes/*/*/plugin.json');
		if (is_array($files))
		{
			foreach ($files as $file)
			{
				$plugin = json_decode(file_get_contents($file), true);
				if (is_array($plugin) && $plugin['type'] === 'theme')
				{
					$plugins[$plugin['vendor'] . '_' . $plugin['name']] = $plugin;
				}
				else
				{
					$this->error('Invalid Plugin in: ' . $file);
				}
			}
		}
		else
		{
			$this->error('Invalid Plugins path: ' . $this->projectPath . 'Plugins/Modules/*/*/plugin.json');
		}

		$files = glob($this->projectPath . 'App/Modules/*/*/plugin.json');
		if (is_array($files))
		{
			foreach ($files as $file)
			{
				$plugin = json_decode(file_get_contents($file), true);
				if (is_array($plugin) && $plugin['type'] === 'module' && $plugin['vendor'] === 'Project')
				{
					$plugins[$plugin['vendor'] . '_' . $plugin['name']] = $plugin;
				}
				else
				{
					$this->error('Invalid Plugin in: ' . $file);
				}
			}
		}
		else
		{
			$this->error('Invalid Plugins path: ' . $this->projectPath . 'Plugins/Modules/*/*/plugin.json');
		}

		$files = glob($this->projectPath . 'App/Themes/*/*/plugin.json');
		if (is_array($files))
		{
			foreach ($files as $file)
			{
				$plugin = json_decode(file_get_contents($file), true);
				if (is_array($plugin) && $plugin['type'] === 'theme' && $plugin['vendor'] === 'Project')
				{
					$plugins[$plugin['vendor'] . '_' . $plugin['name']] = $plugin;
				}
				else
				{
					$this->error('Invalid Plugin in: ' . $file);
				}
			}
		}
		else
		{
			$this->error('Invalid Plugins path: ' . $this->projectPath . 'Plugins/Modules/*/*/plugin.json');
		}
		if (!$plugins)
		{
			$this->error('No plugins detected');
		}
		return $plugins;
	}

	/**
	 * @param string $packageName
	 * @param array $plugins
	 * @return array
	 */
	protected function getRbsPackage($packageName, &$plugins)
	{
		$p = [];
		foreach ($plugins as $name => &$plugin)
		{
			if ($plugin['vendor'] === 'Rbs' && ($plugin['package'] ?? null) === $packageName)
			{
				$p[$name] = $plugin;
				$plugin = null;
			}
		}
		unset($plugin);

		$plugins = array_filter($plugins);
		return $p;
	}
}

$saas = new \Change\Saas($argv);
$saas();