<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Sms;

/**
 * @name \Change\Sms\ProximisProvider
 */
class ProximisProvider
{
	/**
	 * @var array
	 */
	protected $configuration = ['sender' => null, 'token' => null, 'url' => null];

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var boolean
	 */
	protected $valid = false;

	/**
	 * @param array $configuration
	 * @param \Change\Application $application
	 */
	public function __construct(array $configuration, \Change\Application $application)
	{
		$this->configuration = array_merge($this->configuration, $configuration);
		$this->application = $application;
		$this->valid = isset($this->configuration['token'], $this->configuration['url']);
	}

	/**
	 * @param string $mobilePhone
	 * @param string $message
	 * @param string $sender
	 * @return false|string
	 */
	public function sendMessage($mobilePhone, $message, $sender = null)
	{
		if (!$this->valid)
		{
			$this->application->getLogging()->error(__METHOD__, 'Invalid configuration');
			return false;
		}

		$mobilePhone = $this->application->checkDevValue($this->configuration['fakeSms']) ?: $mobilePhone;
		$data = ['to' => $mobilePhone, 'text' => $message];
		if (!$sender)
		{
			$sender = $this->getSender();
		}
		if ($sender)
		{
			$data['sender'] = $sender;
		}
		$result = $this->postSms($data);
		return $result['id'] ?? false;
	}

	/**
	 * @return string
	 */
	protected function getSender()
	{
		return $this->configuration['sender'];
	}

	/**
	 * @return string
	 */
	protected function getToken()
	{
		return $this->configuration['token'];
	}


	/**
	 * @return string
	 */
	protected function getUrl()
	{
		return $this->configuration['url'];
	}

	/**
	 * @param array $data
	 * @return array
	 */
	protected function postSms($data)
	{
		$dataString = json_encode(['sms' => $data]);
		$url = $this->getUrl() . '/notification';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json',
				'Authorization: Bearer ' . $this->getToken(),
				'Content-Length: ' . strlen($dataString)]
		);

		$result = curl_exec($ch);
		if ($result === false)
		{
			$errorData = ['no' => curl_errno($ch), 'error' => curl_error($ch), 'type' => 'curl_error'];
			$this->application->getLogging()->error(__METHOD__, 'curl_error', $errorData['no'], $errorData['error']);
			$this->application->getLogging()->debug($dataString);
			$result = false;
		}
		else
		{
			$httpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (!$result || $httpCode >= 400)
			{
				$errorData = ['no' => $httpCode, 'error' => $result, 'type' => 'http_error'];
				$this->application->getLogging()->error(__METHOD__, 'http_error', $errorData['no'], $errorData['error']);
				$this->application->getLogging()->debug($dataString);
			}
			else
			{
				$result = json_decode($result, true);
			}
		}

		curl_close($ch);
		return $result ?: [];
	}
}