<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Change;

/**
 * @name \Change\Statistics
 */
class Statistics
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'Statistics';

	/**
	 * @var array
	 */
	protected $eventsData = [];

	/**
	 * @var array
	 */
	protected $eventsDataToCommit = [];

	/**
	 * @var bool
	 */
	protected $enabled = false;

	/**
	 * @var bool
	 */
	protected $dbTransactionInProgress = false;

	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->setApplication($application);
		$this->setEnabled($application->getConfiguration('Change/Statistics/enabled'));
		register_shutdown_function([$this, 'flush']);
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return array
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/Statistics');
	}

	/**
	 * @return bool
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * @api
	 * @param bool $enabled
	 * @return $this
	 */
	public function setEnabled(bool $enabled)
	{
		$this->enabled = $enabled;
		return $this;
	}

	/**
	 * @api
	 * @param string $name
	 * @param array $data
	 * @return array|null
	 */
	public function addEvent(string $name, array $data)
	{
		if (!$this->getEnabled())
		{
			return null;
		}

		$data['__name'] = $name;
		$data['__at'] = microtime(true);

		if ($this->dbTransactionInProgress)
		{
			$this->eventsDataToCommit[] = $data;
		}
		else
		{
			$this->eventsData[] = $data;
		}

		return $data;
	}

	/**
	 * @api
	 */
	public function flush()
	{
		if (!$this->getEnabled())
		{
			return;
		}

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['eventsData' => $this->eventsData]);
		$eventManager->trigger('flush', $this, $args);
		$this->eventsData = [];
	}

	/**
	 * @ignore
	 * @param \Change\Events\Event $event
	 */
	public function onTransactionBegin(\Change\Events\Event $event)
	{
		$this->dbTransactionInProgress = true;
	}

	/**
	 * @ignore
	 * @param \Change\Events\Event $event
	 */
	public function onTransactionCommit(\Change\Events\Event $event)
	{
		if (!$event->getParam('primary'))
		{
			return;
		}
		$this->dbTransactionInProgress = false;
		$this->eventsData = array_merge($this->eventsData, $this->eventsDataToCommit);
		$this->eventsDataToCommit = [];
	}

	/**
	 * @ignore
	 * @param \Change\Events\Event $event
	 */
	public function onTransactionRollBack(\Change\Events\Event $event)
	{
		$this->dbTransactionInProgress = false;
		$this->eventsDataToCommit = [];
	}
}