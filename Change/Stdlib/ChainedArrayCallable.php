<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Stdlib;

/**
 * @name \Change\Stdlib\ChainedArrayCallable
 */
class ChainedArrayCallable
{
	/**
	 * @var callable
	 */
	protected $callback1;

	/**
	 * @var callable
	 */
	protected $callback2;

	/**
	 * @param callable $callback1
	 * @param callable $callback2
	 */
	public function __construct(callable $callback1 = null, callable $callback2 = null)
	{
		$this->callback1 = $callback1;
		$this->callback2 = $callback2;
	}

	/**
	 * Returns $callback2($callback1($data1))
	 * @param array $data
	 * @return array
	 */
	public function __invoke(array $data = [])
	{
		if (is_callable($this->callback1))
		{
			$data = call_user_func($this->callback1, $data) ?? [];
		}

		if (is_callable($this->callback2))
		{
			$data = call_user_func($this->callback2, $data) ?? [];
		}

		return $data;
	}

	/**
	 * @return callable
	 */
	public function getCallback1()
	{
		return $this->callback1;
	}

	/**
	 * @param callable $callback1
	 * @return $this
	 */
	public function setCallback1($callback1)
	{
		$this->callback1 = $callback1;
		return $this;
	}

	/**
	 * @return callable
	 */
	public function getCallback2()
	{
		return $this->callback2;
	}

	/**
	 * @param callable $callback2
	 * @return $this
	 */
	public function setCallback2($callback2)
	{
		$this->callback2 = $callback2;
		return $this;
	}
}