<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Stdlib;

/**
 * @name \Change\Stdlib\JsonUtils
 */
class JsonUtils
{
	/**
	 * @param string $json
	 * @return array
	 */
	public static function decode(string $json)
	{
		return \json_decode(self::normalize($json), true, 512, \JSON_THROW_ON_ERROR);
	}

	/**
	 * Normalize a JSON string before parsing it.
	 * @param string $json
	 * @return string
	 */
	public static function normalize(string $json)
	{
		return \preg_replace_callback('/:\s+"([^"]*)"/', static function ($m) { return ': "' . self::cleanupString($m[1]) . '"'; }, $json);
	}

	/**
	 * Cleanup a string literal.
	 * @param string $string
	 * @return string
	 */
	protected static function cleanupString(string $string)
	{
		$string = \trim($string);
		$string = \str_replace(["\r", "\n"], '\n', $string);
		$string = \preg_replace('/(\s*\\\n)+/', '\n\n', $string);
		return \preg_replace('/\s+/', ' ', $string);
	}
}