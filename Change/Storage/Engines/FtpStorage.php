<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\FtpStorage
 */
class FtpStorage extends AbstractStorage
{
	/**
	 * @var string
	 */
	private $ftpHost;

	/**
	 * @var string
	 */
	private $ftpUser;

	/**
	 * @var string
	 */
	private $ftpPass;

	/**
	 * @var string
	 */
	private $ftpPath;

	/**
	 * @var resource[]
	 */
	private static $connections = [];

	/**
	 * @var resource
	 */
	protected $resource;

	/**
	 * @var boolean
	 */
	private $write = false;

	/**
	 * @var string
	 */
	private $tmpDir;

	/**
	 * @var resource
	 */
	private $tmpName;

	/**
	 * @var string[]
	 */
	private $dirInfo;

	/**
	 * @var int
	 */
	private $dirPos;

	/**
	 * @param string $dsn
	 * @return $this
	 */
	public function setDsn($dsn)
	{
		$data = \parse_url($dsn);
		$this->ftpHost = $data['host'];
		$this->ftpUser = $data['user'];
		$this->ftpPass = $data['pass'];
		$this->ftpPath = \rtrim($data['path'] ?? '', '/');
		return $this;
	}

	/**
	 * @return resource|null
	 */
	private function getConnection()
	{
		$connection = self::$connections[$this->name] ?? null;
		if ($connection === null)
		{
			\set_error_handler(function () { return true; }, \E_WARNING);
			$connection = \ftp_connect($this->ftpHost);
			if ($connection && \ftp_login($connection, $this->ftpUser, $this->ftpPass))
			{
				\ftp_pasv($connection, true);
				self::$connections[$this->name] = $connection;
			}
			else
			{
				$this->getStorageManager()->getLogging()->error('Unable to ' . ($connection ? 'login' : 'connect') . ' on FtpStorage ' . $this->name);
				self::$connections[$this->name] = $connection = false;
			}
			\restore_error_handler();
		}
		return $connection ?: null;
	}

	/**
	 * @return string
	 */
	private function getTmpDir()
	{
		if ($this->tmpDir === null)
		{
			$this->tmpDir = $this->getStorageManager()->getWorkspace()->tmpPath('FtpStorage', $this->name);
		}
		return $this->tmpDir;
	}

	/**
	 * Generate E_WARNING
	 * @param string $path
	 * @return string
	 */
	private function makeTmpFile(string $path)
	{
		$tmpName = $this->getTmpDir() . '/' . str_replace('/', '_', trim($path, '/'));
		$dirName = \dirname($tmpName);
		if (\is_dir($dirName) || \mkdir($dirName, 0777, true))
		{
			if (\is_readable($tmpName))
			{
				\unlink($tmpName);
			}
		}
		return $tmpName;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	private function makeFtpPath(string $path)
	{
		return $this->ftpPath . $path;
	}

	/**
	 * @param string $ftpName
	 * @param string $localName
	 * @return bool
	 */
	private function download($ftpName, $localName)
	{
		return \ftp_get($this->getConnection(), $localName, $ftpName, FTP_BINARY);
	}

	/**
	 * Generate E_WARNING
	 * @param string $ftpName
	 * @param string $localName
	 * @return bool
	 */
	private function upload($ftpName, $localName)
	{
		if ($connection = $this->getConnection())
		{
			if (!\ftp_chdir($connection, $dirName = \dirname($ftpName)))
			{
				$pathParts = array_filter(explode('/', $dirName));
				\ftp_chdir($connection, '/');
				foreach ($pathParts as $pathPart)
				{
					if (!$pathPart)
					{
						continue;
					}
					if (!\ftp_chdir($connection, $pathPart))
					{
						if (ftp_mkdir($connection, $pathPart))
						{
							\ftp_chdir($connection, $pathPart);
						}
						else
						{
							return false;
						}
					}
				}
			}
			return \ftp_put($connection, $ftpName, $localName, FTP_BINARY);
		}
		return false;
	}

	/**
	 * @return string|null
	 */
	public function getMimeType()
	{
		if (!$this->getConnection())
		{
			return null;
		}

		$mimeType = null;
		\set_error_handler(function () { return true; }, \E_WARNING);
		$path = $this->parsedURL['path'] ?? null;
		if ($path && substr($path, -1) !== '/'
			&& $this->download($this->makeFtpPath($path), $fileName = $this->makeTmpFile($path)))
		{
			if (\is_readable($fileName) && \class_exists('finfo', false))
			{
				$fi = new \finfo(FILEINFO_MIME_TYPE);
				$mimeType = $fi->file($fileName);
			}
			\unlink($fileName);
		}
		\restore_error_handler();
		return $mimeType;
	}

	/**
	 * @return string|null
	 */
	public function getPublicURL()
	{
		return null;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	public function normalizePath($path)
	{
		$pathParts = [];
		$oldParts = \explode('/', \Change\Stdlib\StringUtils::stripAccents(\str_replace(DIRECTORY_SEPARATOR, '/', $path)));
		foreach ($oldParts as $part)
		{
			if ($part !== '.' && $part !== '..' && $part !== '')
			{
				$pathParts[] = \preg_replace('#[^a-zA-Z0-9.]+#', '_', $part);
			}
		}
		return \implode('/', $pathParts);
	}

	/**
	 * @param string $mode
	 * @param integer $options
	 * @param string $opened_path
	 * @param resource $context
	 * @return boolean
	 */
	public function stream_open($mode, $options, &$opened_path, &$context)
	{
		if (!$this->getConnection())
		{
			throw new \RuntimeException('Unable to open ftp connection');
		}
		\set_error_handler(function () { return true; }, \E_WARNING);

		$path = $this->parsedURL['path'];
		$opened_path = $this->tmpName = $this->makeTmpFile($path);
		$ftpName = $this->makeFtpPath($path);
		if (\strpos($mode, 'w') === false)
		{
			$this->download($ftpName, $opened_path);
		}
		$this->resource = \fopen($opened_path, $mode, false);

		\restore_error_handler();
		if (!\is_resource($this->resource))
		{
			throw new \RuntimeException('Unable to open file: ' . $opened_path);
		}
		return true;
	}

	/**
	 * @param integer $count
	 * @return string
	 */
	public function stream_read($count)
	{
		return \fread($this->resource, $count);
	}

	/**
	 * @param   string $data
	 * @return  integer
	 */
	public function stream_write($data)
	{
		$this->write = true;
		return \fwrite($this->resource, $data);
	}

	/**
	 * @return array
	 */
	public function stream_stat()
	{
		return \fstat($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_flush()
	{
		$this->write = true;
		return \fflush($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_eof()
	{
		return \feof($this->resource);
	}

	/**
	 * @param $offset
	 * @param int $whence
	 * @return bool
	 */
	public function stream_seek($offset, $whence = SEEK_SET)
	{
		return \fseek($this->resource, $offset, $whence) === 0;
	}

	/**
	 * @return void
	 */
	public function stream_close()
	{
		$r = \fclose($this->resource);
		unset($this->resource);
		\set_error_handler(function () { return true; }, \E_WARNING);

		$tmpName = $this->tmpName;
		if ($r && $this->write && $tmpName && !$this->upload($this->makeFtpPath($this->parsedURL['path']), $tmpName))
		{
			$this->getStorageManager()->getLogging()->error('Unable to upload ' . $tmpName);
		}
		if (\is_readable($tmpName))
		{
			\unlink($tmpName);
		}
		unset($this->tmpName);

		\restore_error_handler();
	}

	/**
	 * @param integer $flags
	 * @return array|false mixed
	 */
	public function url_stat($flags)
	{
		if (($flags & \STREAM_URL_STAT_LINK) === \STREAM_URL_STAT_LINK)
		{
			return false;
		}
		$path = $this->parsedURL['path'];
		if (!$path || !($connection = $this->getConnection()))
		{
			return false;
		}
		\set_error_handler(function () { return true; }, \E_WARNING);

		$ftpPath = $this->makeFtpPath($path);
		$remoteStat = [];
		$creationDate = \ftp_mdtm($connection, $ftpPath);
		$contentLength = \ftp_size($connection, $ftpPath);
		if ($creationDate !== -1 && $contentLength !== -1)
		{
			$remoteStat['directory'] = false;
			$remoteStat['creationDate'] = $creationDate;
			$remoteStat['contentLength'] = $contentLength;
		}
		else
		{

			if (\ftp_chdir($connection, $ftpPath))
			{
				$remoteStat['directory'] = true;
				$remoteStat['creationDate'] = time();
				$remoteStat['contentLength'] = 0;
			}
		}

		\restore_error_handler();
		if (!$remoteStat)
		{
			return false;
		}
		if ($remoteStat['directory'] ?? false)
		{
			$mode = self::S_IFDIR + self::S_IRUSR + self::S_IRGRP + self::S_IROTH;
		}
		else
		{
			$mode = self::S_IFREG + self::S_IRUSR + self::S_IWUSR + self::S_IRGRP + self::S_IWGRP + self::S_IROTH + self::S_IWOTH;
		}

		return [
			0 => 0, 1 => 0, 2 => $mode,
			3 => 1, 4 => 0, 5 => 0, 6 => 0,
			7 => $remoteStat['contentLength'],
			8 => $remoteStat['creationDate'],
			9 => $remoteStat['creationDate'],
			10 => $remoteStat['creationDate'],
			11 => -1, 12 => -1,
			'dev' => 0, 'ino' => 0, 'mode' => $mode,
			'nlink' => 1, 'uid' => 0, 'gid' => 0, 'rdev' => 0,
			'size' => $remoteStat['contentLength'],
			'atime' => $remoteStat['creationDate'],
			'mtime' => $remoteStat['creationDate'],
			'ctime' => $remoteStat['creationDate'],
			'blksize' => -1, 'blocks' => -1,
		];
	}

	/**
	 * @param integer $options
	 * @return boolean
	 */
	public function dir_opendir($options)
	{
		$opened = false;
		if (!($connection = $this->getConnection()))
		{
			return $opened;
		}
		\set_error_handler(function () { return true; }, \E_WARNING);
		$path = $this->parsedURL['path'];
		if (\ftp_chdir($connection, $this->makeFtpPath($path)))
		{
			$data = \ftp_nlist($connection, '.');
			if (\is_array($data))
			{
				$this->dirInfo = $data;
				$this->dirPos = 0;
				$opened = true;
			}
		}
		\restore_error_handler();
		return $opened;
	}

	/**
	 * @return  string|false
	 */
	public function dir_readdir()
	{
		return $this->dirInfo[$this->dirPos++] ?? false;
	}

	/**
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_rewinddir()
	{
		$this->dirPos = 0;
		return true;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_closedir()
	{
		$this->dirInfo = $this->dirPos = null;
		return true;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function unlink()
	{
		if ($connection = $this->getConnection())
		{
			return ftp_delete($connection, $this->makeFtpPath($this->parsedURL['path']));
		}
		return false;
	}

	/**
	 * @param   integer $mode The value passed to {@see mkdir()}.
	 * @param   integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return  boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function mkdir($mode, $options)
	{
		$path = $this->parsedURL['path'];
		if (!$path || !($connection = $this->getConnection()))
		{
			return false;
		}
		try
		{
			\set_error_handler(function () { return true; }, \E_WARNING);

			$ftpPath = $this->makeFtpPath($path);
			$recursive = (STREAM_MKDIR_RECURSIVE & $options) === STREAM_MKDIR_RECURSIVE;
			if (!$recursive)
			{
				$parentDir = \dirname($ftpPath);
				if ($parentDir !== '/' && !\ftp_chdir($connection, $parentDir))
				{
					return false;
				}
			}
			$pathParts = array_filter(explode('/', $ftpPath));
			\ftp_chdir($connection, '/');

			foreach ($pathParts as $pathPart)
			{
				if (!$pathPart)
				{
					continue;
				}
				if (!\ftp_chdir($connection, $pathPart))
				{
					if (ftp_mkdir($connection, $pathPart))
					{
						\ftp_chdir($connection, $pathPart);
					}
					else
					{
						return false;
					}
				}
			}
		}
		finally
		{
			\restore_error_handler();
		}
		return true;
	}

	/**
	 * @param string $pathTo The URL which the $path_from should be renamed to.
	 * @throws \RuntimeException
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function rename($pathTo)
	{
		$fp = $this->parsedURL['path'] ?? null;
		if (!$fp || !($connection = $this->getConnection()))
		{
			return false;
		}
		$fromFtpName = $this->makeFtpPath($fp);

		$toItem = $this->getStorageManager()->getItemInfo($pathTo);
		if ($toItem === null)
		{
			return false;
		}
		$toEngine = $toItem->getStorageEngine();
		try
		{
			\set_error_handler(function () { return true; }, \E_WARNING);
			if ($toEngine instanceof self && $toEngine->name === $this->name)
			{
				$tp = $toEngine->parsedURL['path'] ?? null;
				if (!$tp)
				{
					return false;
				}
				$toFtpName = $this->makeFtpPath($tp);
				return ftp_rename($connection, $fromFtpName, $toFtpName);
			}
			if ($this->download($fromFtpName, $localName = $this->makeTmpFile($fp)))
			{
				if ($copied = \copy($localName, $pathTo))
				{
					\ftp_delete($connection, $fromFtpName);
				}
				\unlink($localName);
				return $copied;
			}
		}
		finally
		{
			\restore_error_handler();
		}
		return false;
	}

	/**
	 * @param integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function rmdir($options)
	{
		$path = $this->parsedURL['path'];
		if (!$path || !($connection = $this->getConnection()))
		{
			return false;
		}
		$ftpPath = $this->makeFtpPath($path);
		return \ftp_rmdir($connection, $ftpPath);
	}

	/**
	 * @param   integer $option One of:
	 *                                  STREAM_META_TOUCH (The method was called in response to touch())
	 *                                  STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
	 *                                  STREAM_META_OWNER (The method was called in response to chown())
	 *                                  STREAM_META_GROUP_NAME (The method was called in response to chgrp())
	 *                                  STREAM_META_GROUP (The method was called in response to chgrp())
	 *                                  STREAM_META_ACCESS (The method was called in response to chmod())
	 * @param   array $var If option is
	 *                                  PHP_STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
	 *                                  PHP_STREAM_META_OWNER_NAME or PHP_STREAM_META_GROUP_NAME: The name of the owner
	 *                                      user/group as string.
	 *                                  PHP_STREAM_META_OWNER or PHP_STREAM_META_GROUP: The value owner user/group argument as integer.
	 *                                  PHP_STREAM_META_ACCESS: The argument of the chmod() as integer.
	 * @return  boolean             Returns TRUE on success or FALSE on failure. If option is not implemented, FALSE should be returned.
	 */
	public function stream_metadata($option, $var)
	{
		return false;
	}

	/**
	 * @return  integer     Should return the current position of the stream.
	 */
	public function stream_tell()
	{
		return \ftell($this->resource);
	}

	/**
	 * @param integer $new_size
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function stream_truncate($new_size)
	{
		$this->write = true;
		return \ftruncate($this->resource, $new_size);
	}
}