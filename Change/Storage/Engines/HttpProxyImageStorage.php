<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
* @name \Change\Storage\Engines\HttpProxyImageStorage
*/
class HttpProxyImageStorage extends HttpProxyStorage
{
	/**
	 * @var string
	 */
	protected $formats;

	/**
	 * @var boolean|integer[]
	 */
	protected $formatted = false;

	/**
	 * @param string $formats
	 * @return $this
	 */
	public function setFormats($formats)
	{
		$this->formats = $formats;
		return $this;
	}

	/**
	 * @var array
	 */
	protected static $predefinedSizes = [];

	/**
	 * @return array|false
	 */
	public function getPredefinedSizes()
	{
		if (!\array_key_exists($this->name, static::$predefinedSizes))
		{
			$predefinedSizes = [];
			if ($this->formats && \is_string($this->formats))
			{
				foreach (\explode(',', $this->formats) as $format)
				{
					$size = \explode('x', $format);
					if (\count($size) === 2)
					{
						$width = (int)$size[0];
						$height = (int)$size[1];
						if ($width > 0 && $height > 0)
						{
							$predefinedSizes[$width . 'x' . $height] = [$width, $height];
						}
					}
				}
			}
			static::$predefinedSizes[$this->name] = $predefinedSizes?: false;
		}
		return static::$predefinedSizes[$this->name];
	}

	public function setParsedURL(array $parsedURL)
	{
		$this->formatted = false;
		if (!empty($parsedURL['query']))
		{
			\parse_str($parsedURL['query'], $query);
			if (\is_array($query) && isset($query['max-width'], $query['max-height']))
			{
				$this->formatted = [(int)$query['max-width'], (int)$query['max-height']];
			}
		}
		return parent::setParsedURL($parsedURL);
	}

	/**
	 * @param integer $maxWidth
	 * @param integer $maxHeight
	 * @return array|null
	 */
	protected function findSize($maxWidth, $maxHeight)
	{
		$predefinedSizes = $this->getPredefinedSizes();
		if ($predefinedSizes === false || ($maxWidth === 0 && $maxHeight === 0))
		{
			return [$maxWidth, $maxHeight];
		}
		$size = null;
		foreach ($predefinedSizes as $fSize)
		{
			[$width, $height] = $fSize;
			if ($size === null)
			{
				$size = $fSize;
			}
			elseif ($width >= $maxWidth && $height >= $maxHeight)
			{
				$size = $fSize;
			}
		}
		return $size;
	}

	/**
	 * @return null|string
	 */
	public function getPublicURL()
	{
		if ($this->baseURL !== false && $this->formatted)
		{
			$size = $this->formatted ?: [0, 0];
			$formattedSize = $this->findSize($size[0], $size[1]);
			return $this->baseURL . '/Imagestorage/' . $this->name . '/' . $formattedSize[0] . '/' . $formattedSize[1] . $this->parsedURL['path'];
		}
		return parent::getPublicURL();
	}
}