<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\HttpProxyStorage
 */
class HttpProxyStorage extends AbstractStorage
{
	/**
	 * @var string
	 */
	protected $tmpBasePath;

	/**
	 * @var string
	 */
	protected $httpProxy;

	/**
	 * @var string|boolean
	 */
	protected $baseURL;

	/**
	 * @var resource
	 */
	protected $resource;

	/**
	 * @var bool
	 */
	protected $resourceUpdated = false;


	/**
	 * @param string $httpProxy
	 * @return $this
	 */
	public function setHttpProxy($httpProxy)
	{
		$this->httpProxy = $httpProxy;
		return $this;
	}

	/**
	 * @param string|boolean $baseURL
	 * @return $this
	 */
	public function setBaseURL($baseURL)
	{
		$this->baseURL = \is_string($baseURL) ? $baseURL : false;
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getTmpBasePath()
	{
		if ($this->tmpBasePath === null)
		{
			$this->tmpBasePath  = $this->getStorageManager()->getWorkspace()->tmpPath('HttpProxyStorage', $this->getName());
		}
		return $this->tmpBasePath;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	public function normalizePath($path)
	{
		$pathParts = [];
		$oldParts = explode('/', \Change\Stdlib\StringUtils::stripAccents(str_replace(DIRECTORY_SEPARATOR, '/', $path)));
		foreach ($oldParts as $part)
		{
			if ($part !== '.' && $part !== '..' && $part !== '')
			{
				$pathParts[] = preg_replace('#[^a-zA-Z0-9.]+#', '_', $part);
			}
		}
		return implode('/', $pathParts);
	}

	/**
	 * @return string
	 */
	public function getRemoteURL()
	{
		return $this->httpProxy . '?file='. urlencode($this->name . $this->parsedURL['path']);
	}

	/**
	 * @return string
	 */
	public function getLocalTmpFileName()
	{
		$localTmpFileName = $this->getTmpBasePath() . str_replace('/', DIRECTORY_SEPARATOR, $this->parsedURL['path']);
		\Change\Stdlib\FileUtils::mkdir(\dirname($localTmpFileName));
		return $localTmpFileName;
	}

	/**
	 * @return string
	 */
	public function download()
	{
		$localTmpPath = $this->getLocalTmpFileName();
		if (\file_exists($localTmpPath))
		{
			@\unlink($localTmpPath);
		}
		$fw = \fopen($localTmpPath, 'wb');
		$ch = \curl_init();
		\curl_setopt($ch, CURLOPT_URL, $this->getRemoteURL());
		\curl_setopt($ch, CURLOPT_FILE, $fw);
		$curlData = \curl_exec($ch);
		$downloaded = false;
		if ($curlData !== false)
		{
			$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->getStorageManager()->getLogging()->info(__METHOD__, $httpCode, $curlData);
			$downloaded = ($httpCode === 200);
			if ($httpCode === 503)
			{
				$localTmpPath = null;
			}
		}
		else
		{
			$this->getStorageManager()->getLogging()->error(__METHOD__, ':', curl_error($ch));
			$localTmpPath = null;
		}

		\curl_close($ch);
		\fflush($fw);
		\fclose($fw);
		if (!$downloaded && $localTmpPath)
		{
			@\unlink($localTmpPath);
		}
		return $localTmpPath;
	}

	/**
	 * @return string|null
	 */
	public function getMimeType()
	{
		$url = $this->getRemoteURL() . '&meta=MimeType';
		$ch = \curl_init();
		\curl_setopt($ch, CURLOPT_URL, $url);
		\curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		\curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$curlData = \curl_exec($ch);
		$mimeType = null;
		if ($curlData !== false)
		{
			$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->getStorageManager()->getLogging()->info(__METHOD__, ':', $httpCode, $curlData);
			if ($httpCode === 200)
			{
				$mimeType = $curlData;
			}
		}
		else
		{
			$this->getStorageManager()->getLogging()->error(__METHOD__, ':', curl_error($ch));
		}
		\curl_close($ch);
		return $mimeType?: null;
	}

	/**
	 * @return string|null
	 */
	public function getPublicURL()
	{
		if ($this->baseURL !== false)
		{
			$infos = $this->parsedURL;
			if ($infos && isset($infos['path']))
			{
				return $this->baseURL . '/Storage/' . $this->name . $infos['path'];
			}
		}
		return null;
	}

	/**
	 * @param string $mode
	 * @param integer $options
	 * @param string $opened_path
	 * @param resource $context
	 * @return boolean
	 */
	public function stream_open($mode, $options, &$opened_path, &$context)
	{
		$this->resourceUpdated = false;
		$fileName = $this->download();
		if (!$fileName)
		{
			return false;
		}
		\Change\StdLib\FileUtils::mkdir(\dirname($fileName));
		$this->resource = @\fopen($fileName, $mode);
		return \is_resource($this->resource);
	}

	/**
	 * @param integer $count
	 * @return string
	 */
	public function stream_read($count)
	{
		return \fread($this->resource, $count);
	}

	/**
	 * @param   string $data
	 * @return  integer
	 */
	public function stream_write($data)
	{
		$this->resourceUpdated = true;
		return \fwrite($this->resource, $data);
	}

	/**
	 * @return array
	 */
	public function stream_stat()
	{
		return \fstat($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_eof()
	{
		return \feof($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_flush()
	{
		return \fflush($this->resource);
	}

	/**
	 * @param $offset
	 * @param int $whence
	 * @return bool
	 */
	public function stream_seek($offset, $whence = SEEK_SET)
	{
		return \fseek($this->resource, $offset, $whence) === 0;
	}

	/**
	 * @return void
	 */
	public function stream_close()
	{
		\fclose($this->resource);
		unset($this->resource);
		if ($this->resourceUpdated)
		{
			$localTmpFileName = $this->getLocalTmpFileName();
			if (\file_exists($localTmpFileName))
			{
				$post = ['data'=>'@'.$localTmpFileName];
				$ch = \curl_init();
				\curl_setopt($ch, CURLOPT_URL, $this->getRemoteURL());
				\curl_setopt($ch, CURLOPT_POST, 1);
				\curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				\curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				$curlData = \curl_exec($ch);
				if ($curlData !== false)
				{
					$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$this->getStorageManager()->getLogging()->info(__METHOD__, ':', $httpCode, $curlData);
					@\unlink($localTmpFileName);
				}
				else
				{
					$this->getStorageManager()->getLogging()->error(__METHOD__, ':', \curl_error($ch));
				}
				\curl_close($ch);
			}
		}
	}

	/**
	 * @param integer $flags
	 * @return array|false
	 */
	public function url_stat($flags)
	{
		$url = $this->getRemoteURL() . '&meta=stat';
		$ch = \curl_init();
		\curl_setopt($ch, CURLOPT_URL, $url);
		\curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		\curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		\curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$curlData = curl_exec($ch);
		$stat = null;
		if ($curlData !== false)
		{
			$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->getStorageManager()->getLogging()->info(__METHOD__, ':', $httpCode, $curlData);
			if ($httpCode === 200)
			{
				/** @noinspection UnserializeExploitsInspection */
				$stat = \unserialize($curlData);
			}
		}
		else
		{
			$this->getStorageManager()->getLogging()->error(__METHOD__, ':', curl_error($ch));
		}
		\curl_close($ch);
		return $stat?: false;
	}

	/**
	 * @param integer $options
	 * @return boolean
	 */
	public function dir_opendir($options)
	{
		return false;
	}

	/**
	 * @return  string|false
	 */
	public function dir_readdir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_rewinddir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_closedir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function unlink()
	{
		$url = $this->getRemoteURL() . '&meta=unlink';
		$ch = \curl_init();
		$timeout = 5;
		\curl_setopt($ch, CURLOPT_URL, $url);
		\curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		\curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$curlData = \curl_exec($ch);
		$unlink = false;
		if ($curlData !== false)
		{
			$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->getStorageManager()->getLogging()->info(__METHOD__, ':', $httpCode, $curlData);
			if ($httpCode === 200)
			{
				$unlink = true;
				\clearstatcache();
			}
		}
		else
		{
			$this->getStorageManager()->getLogging()->error(__METHOD__, ':', \curl_error($ch));
		}
		\curl_close($ch);
		return $unlink;
	}

	/**
	 * @param   integer $mode The value passed to {@see mkdir()}.
	 * @param   integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return  boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function mkdir($mode, $options)
	{
		return false;
	}

	/**
	 * @param string $pathTo The URL which the $path_from should be renamed to.
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function rename($pathTo)
	{
		return false;
	}

	/**
	 * @param integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function rmdir($options)
	{
		return false;
	}

	/**
	 * @param   integer $option One of:
	 *                                  STREAM_META_TOUCH (The method was called in response to touch())
	 *                                  STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
	 *                                  STREAM_META_OWNER (The method was called in response to chown())
	 *                                  STREAM_META_GROUP_NAME (The method was called in response to chgrp())
	 *                                  STREAM_META_GROUP (The method was called in response to chgrp())
	 *                                  STREAM_META_ACCESS (The method was called in response to chmod())
	 * @param   integer $var If option is
	 *                                  PHP_STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
	 *                                  PHP_STREAM_META_OWNER_NAME or PHP_STREAM_META_GROUP_NAME: The name of the owner
	 *                                      user/group as string.
	 *                                  PHP_STREAM_META_OWNER or PHP_STREAM_META_GROUP: The value owner user/group argument as integer.
	 *                                  PHP_STREAM_META_ACCESS: The argument of the chmod() as integer.
	 * @return  boolean             Returns TRUE on success or FALSE on failure. If option is not implemented, FALSE should be returned.
	 */
	public function stream_metadata($option, $var)
	{
		if ($option === STREAM_META_TOUCH)
		{
			$time = $var[0] ?? null;
			$aTime = $var[1] ?? null;
			$url = $this->getRemoteURL() . '&meta=touch&time=' . $time . '&atime=' .$aTime;
			$ch = \curl_init();
			\curl_setopt($ch, CURLOPT_URL, $url);
			\curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			\curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

			$curlData = \curl_exec($ch);
			$touch = false;
			if ($curlData !== false)
			{
				$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$this->getStorageManager()->getLogging()->info(__METHOD__, ':', $httpCode, $curlData);
				$touch = ($httpCode === 200);
			}
			else
			{
				$this->getStorageManager()->getLogging()->error(__METHOD__, ':', curl_error($ch));
			}
			\curl_close($ch);
			return $touch;
		}
		return false;
	}

	/**
	 * @return  integer     Should return the current position of the stream.
	 */
	public function stream_tell()
	{
		return \ftell($this->resource);
	}

	/**
	 * @param integer $new_size
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function stream_truncate($new_size)
	{
		$this->resourceUpdated = true;
		return \ftruncate($this->resource, $new_size);
	}
}