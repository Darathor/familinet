<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\HttpRemoteStorage
 */
class HttpRemoteStorage extends AbstractStorage
{
	/**
	 * @var int
	 */
	protected static $shutdown = 0;

	/**
	 * @var string
	 */
	protected $tmpBasePath;

	/**
	 * @var string
	 */
	protected $baseRemoteURL;

	/**
	 * @var bool
	 */
	protected $allowRead = false;

	/**
	 * @return string
	 */
	public function getBaseRemoteURL()
	{
		return $this->baseRemoteURL;
	}

	/**
	 * @param string $baseRemoteURL
	 * @return $this
	 */
	public function setBaseRemoteURL($baseRemoteURL)
	{
		$this->baseRemoteURL = (string)$baseRemoteURL;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getAllowRead()
	{
		return $this->allowRead;
	}

	/**
	 * @param bool $allowRead
	 * @return $this
	 */
	public function setAllowRead($allowRead)
	{
		$this->allowRead = $allowRead;
		return $this;
	}

	/**
	 * @param string $remoteURL
	 * @return string|null
	 */
	public function makeStorageURIByRemoteURL(string $remoteURL)
	{
		if ($this->baseRemoteURL && \strpos($remoteURL, $this->baseRemoteURL) === 0)
		{
			$path = \trim(\substr($remoteURL, \strlen($this->baseRemoteURL)), '/');
			if ($path)
			{
				return $this->getStorageManager()->buildChangeURI($this->name, '/' . $path)->toString();
			}
		}
		return null;
	}

	/**
	 * @return string|null
	 */
	protected function getRemoteURL()
	{
		return $this->baseRemoteURL ? $this->baseRemoteURL . $this->parsedURL['path'] : null;
	}

	/**
	 * @return string
	 */
	protected function getTmpBasePath()
	{
		if ($this->tmpBasePath === null)
		{
			$this->tmpBasePath = $this->getStorageManager()->getWorkspace()->tmpPath('HttpRemoteStorage', $this->getName());
		}
		return $this->tmpBasePath;
	}

	/**
	 * @return string
	 */
	public function getLocalTmpFileName()
	{
		$localTmpFileName = $this->getTmpBasePath() . \str_replace('/', DIRECTORY_SEPARATOR, $this->parsedURL['path']);
		\Change\Stdlib\FileUtils::mkdir(\dirname($localTmpFileName));
		return $localTmpFileName;
	}

	/**
	 * @param string $remoteURL
	 * @return string
	 */
	protected function download(string $remoteURL)
	{
		$localTmpPath = $this->getLocalTmpFileName();
		if (\file_exists($localTmpPath))
		{
			return $localTmpPath;
		}

		if (!static::$shutdown)
		{
			static::$shutdown++;
			$dir = $this->getTmpBasePath();
			\register_shutdown_function(function () use ($dir) {
				\Change\Stdlib\FileUtils::rmdir($dir);
			});
		}
		elseif (static::$shutdown > 100)
		{
			static::$shutdown = 1;
			\Change\Stdlib\FileUtils::rmdir($this->getTmpBasePath());
		}
		else
		{
			static::$shutdown++;
		}

		$fw = \fopen($localTmpPath, 'wb');
		$ch = \curl_init();
		$this->getStorageManager()->getLogging()->info(__METHOD__, $remoteURL);
		\curl_setopt($ch, CURLOPT_URL, $remoteURL);
		\curl_setopt($ch, CURLOPT_FILE, $fw);
		$curlData = \curl_exec($ch);
		$downloaded = false;
		if ($curlData !== false)
		{
			$httpCode = \curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$this->getStorageManager()->getLogging()->info(__METHOD__, $httpCode, $curlData);
			$downloaded = ($httpCode === 200);
			if ($httpCode === 503)
			{
				$localTmpPath = null;
			}
		}
		else
		{
			$this->getStorageManager()->getLogging()->error(__METHOD__, ':', \curl_error($ch));
			$localTmpPath = null;
		}

		\curl_close($ch);
		\fflush($fw);
		\fclose($fw);
		if (!$downloaded && $localTmpPath)
		{
			@\unlink($localTmpPath);
		}
		return $localTmpPath;
	}

	/**
	 * @return string|null
	 */
	public function getMimeType()
	{
		if (!$this->allowRead || !\class_exists('finfo', false) || !($remoteURL = $this->getRemoteURL()))
		{
			return null;
		}

		$filename = $this->download($remoteURL);
		if (\file_exists($filename))
		{
			$fi = new \finfo(FILEINFO_MIME_TYPE);
			$mimeType = $fi->file($filename);
			if ($mimeType)
			{
				return $mimeType;
			}
		}
		return null;
	}

	/**
	 * @param string $path
	 * @return string
	 */
	public function normalizePath($path)
	{
		return $path;
	}

	/**
	 * @return string|null
	 */
	public function getPublicURL()
	{
		return $this->baseRemoteURL ? $this->getRemoteURL() : null;
	}

	/**
	 * @var resource
	 */
	protected $resource;

	/**
	 * @param string $mode
	 * @param integer $options
	 * @param string $opened_path
	 * @param resource $context
	 * @return boolean
	 */
	public function stream_open($mode, $options, &$opened_path, &$context)
	{
		if ($this->allowRead && \in_array($mode, ['r', 'rb', 'rt'], true) && ($remoteURL = $this->getRemoteURL()))
		{
			$fileName = $this->download($remoteURL);
			if (!$fileName)
			{
				return false;
			}
			\Change\StdLib\FileUtils::mkdir(\dirname($fileName));
			$this->resource = @\fopen($fileName, $mode);
			return \is_resource($this->resource);
		}
		return false;
	}

	/**
	 * @param integer $count
	 * @return string
	 */
	public function stream_read($count)
	{
		return \fread($this->resource, $count);
	}

	/**
	 * @param string $data
	 * @return integer
	 */
	public function stream_write($data)
	{
		return 0;
	}

	/**
	 * @return array
	 */
	public function stream_stat()
	{
		return \fstat($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_eof()
	{
		return \feof($this->resource);
	}

	/**
	 * @return bool
	 */
	public function stream_flush()
	{
		return \fflush($this->resource);
	}

	/**
	 * @param $offset
	 * @param int $whence
	 * @return bool
	 */
	public function stream_seek($offset, $whence = SEEK_SET)
	{
		return \fseek($this->resource, $offset, $whence) === 0;
	}

	/**
	 * @return  integer     Should return the current position of the stream.
	 */
	public function stream_tell()
	{
		return \ftell($this->resource);
	}

	/**
	 * @param integer $new_size
	 * @return boolean Returns TRUE on success or FALSE on failure.
	 */
	public function stream_truncate($new_size)
	{
		return false;
	}

	/**
	 * @return void
	 */
	public function stream_close()
	{
		\fclose($this->resource);
		unset($this->resource);
	}

	/**
	 * @param integer $flags
	 * @return array|mixed
	 */
	public function url_stat($flags)
	{
		if (($flags & \STREAM_URL_STAT_LINK) === \STREAM_URL_STAT_LINK)
		{
			return false;
		}
		if ($this->allowRead && ($remoteURL = $this->getRemoteURL()))
		{
			$filename = $this->download($remoteURL);
			if ((STREAM_URL_STAT_QUIET & $flags) === STREAM_URL_STAT_QUIET && !\file_exists($filename))
			{
				return false;
			}
			$stat = \stat($filename);
			$stat[2] = $stat['mode'] = 33060;
			return $stat;
		}
		return false;
	}

	/**
	 * @param   integer $option One of:
	 *                                  STREAM_META_TOUCH (The method was called in response to touch())
	 *                                  STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
	 *                                  STREAM_META_OWNER (The method was called in response to chown())
	 *                                  STREAM_META_GROUP_NAME (The method was called in response to chgrp())
	 *                                  STREAM_META_GROUP (The method was called in response to chgrp())
	 *                                  STREAM_META_ACCESS (The method was called in response to chmod())
	 * @param   integer $var If option is
	 *                                  PHP_STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
	 *                                  PHP_STREAM_META_OWNER_NAME or PHP_STREAM_META_GROUP_NAME: The name of the owner
	 *                                      user/group as string.
	 *                                  PHP_STREAM_META_OWNER or PHP_STREAM_META_GROUP: The value owner user/group argument as integer.
	 *                                  PHP_STREAM_META_ACCESS: The argument of the chmod() as integer.
	 * @return  boolean             Returns TRUE on success or FALSE on failure. If option is not implemented, FALSE should be returned.
	 */
	public function stream_metadata($option, $var)
	{
		return false;
	}

	/**
	 * @param integer $options
	 * @return boolean
	 */
	public function dir_opendir($options)
	{
		return false;
	}

	/**
	 * @return  string|false
	 */
	public function dir_readdir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_rewinddir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function dir_closedir()
	{
		return false;
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function unlink()
	{
		return false;
	}

	/**
	 * @param   integer $mode The value passed to {@see mkdir()}.
	 * @param   integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return  boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function mkdir($mode, $options)
	{
		return false;
	}

	/**
	 * @param string $pathTo The URL which the $path_from should be renamed to.
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function rename($pathTo)
	{
		return false;
	}

	/**
	 * @param integer $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
	 * @return boolean             Returns TRUE on success or FALSE on failure.
	 */
	public function rmdir($options)
	{
		return false;
	}
}