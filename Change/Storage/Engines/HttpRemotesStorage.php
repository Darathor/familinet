<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\HttpRemotesStorage
 */
class HttpRemotesStorage extends \Change\Storage\Engines\HttpRemoteStorage
{
	/**
	 * @var string[]
	 */
	protected $baseRemotesURL;

	/**
	 * @return string[]
	 */
	public function getBaseRemotesURL()
	{
		return $this->baseRemotesURL;
	}

	/**
	 * @param string[] $baseRemotesURL
	 * @return $this
	 */
	public function setBaseRemotesURL($baseRemotesURL)
	{
		$this->baseRemotesURL = (array)$baseRemotesURL;
		return $this;
	}

	/**
	 * @param string $remoteURL
	 * @return null|string
	 */
	public function makeStorageURIByRemoteURL(string $remoteURL)
	{
		foreach ($this->baseRemotesURL as $idx => $baseRemoteURL)
		{
			if (\strpos($remoteURL, $baseRemoteURL) === 0)
			{
				$path = \trim(\substr($remoteURL, \strlen($baseRemoteURL)), '/');
				if ($path)
				{
					$this->baseRemoteURL = $baseRemoteURL;
					return $this->getStorageManager()->buildChangeURI($this->name, '/' . $idx . '/' . $path)->toString();
				}
			}
		}
		return null;
	}

	/**
	 * @return string|null
	 */
	protected function getRemoteURL()
	{
		if ($path = $this->parsedURL['path'])
		{
			$p = \explode('/', \trim($path, '/'));
			$idx = \array_shift($p);
			if ($this->baseRemoteURL = $this->baseRemotesURL[$idx] ?? null)
			{
				return $this->baseRemoteURL . '/' . \implode('/', $p);
			}
		}
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getMimeType()
	{
		if ($this->getRemoteURL())
		{
			return parent::getMimeType();
		}
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getPublicURL()
	{
		return $this->baseRemotesURL ? $this->getRemoteURL() : null;
	}
}