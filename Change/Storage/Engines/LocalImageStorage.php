<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage\Engines;

/**
 * @name \Change\Storage\Engines\LocalImageStorage
 */
class LocalImageStorage extends LocalStorage
{
	/**
	 * @var string
	 */
	protected $formattedPath;

	/**
	 * @var boolean|integer[]
	 */
	protected $formatted = false;

	/**
	 * @var boolean
	 */
	protected $modifiedStream = false;

	/**
	 * List of format sorted by size eg: "500x500,200x200,100x100"
	 * @var string
	 */
	protected $formats;

	/**
	 * @var boolean
	 */
	protected $generateFormated = false;

	/**
	 * @var array
	 */
	protected static $predefinedSizes = [];

	/**
	 * @var string
	 */
	protected $compression = 'MEDIUM';

	/**
	 * @return string
	 */
	public function getCompression()
	{
		return $this->compression;
	}

	/**
	 * @param string $compression
	 * @return $this
	 */
	public function setCompression($compression)
	{
		$this->compression = $compression;
		return $this;
	}

	/**
	 * @return array|false
	 */
	public function getPredefinedSizes()
	{
		if (!\array_key_exists($this->name, static::$predefinedSizes))
		{
			$predefinedSizes = [];
			if ($this->formats && \is_string($this->formats))
			{
				foreach (explode(',', $this->formats) as $format)
				{
					$size = \explode('x', $format);
					if (\count($size) === 2)
					{
						$width = (int)$size[0];
						$height = (int)$size[1];
						if ($width > 0 && $height > 0)
						{
							$predefinedSizes[$width . 'x' . $height] = [$width, $height];
						}
					}
				}
			}
			static::$predefinedSizes[$this->name] = $predefinedSizes?: false;
		}
		return static::$predefinedSizes[$this->name];
	}

	/**
	 * @param string $formats
	 * @return $this
	 */
	public function setFormats($formats)
	{
		$this->formats = $formats;
		return $this;
	}

	public function setParsedURL(array $parsedURL)
	{
		$this->formatted = false;
		if (!empty($parsedURL['query']))
		{
			\parse_str($parsedURL['query'], $query);
			if (\is_array($query) && isset($query['max-width'], $query['max-height']))
			{
				$this->formatted = [(int)$query['max-width'], (int)$query['max-height']];
			}
		}
		return parent::setParsedURL($parsedURL);
	}

	/**
	 * @param string $formattedPath
	 */
	public function setFormattedPath($formattedPath)
	{
		$this->formattedPath = $formattedPath;
	}

	/**
	 * @return string
	 */
	public function getFormattedPath()
	{
		return $this->formattedPath;
	}

	/**
	 * @return boolean
	 */
	public function getGenerateFormated()
	{
		return $this->generateFormated;
	}

	/**
	 * @param boolean $generateFormated
	 * @return $this
	 */
	public function setGenerateFormated($generateFormated)
	{
		$this->generateFormated = (bool)$generateFormated;
		return $this;
	}

	public function setStorageManager(\Change\Storage\StorageManager $storageManager)
	{
		parent::setStorageManager($storageManager);
		$this->formattedPath = $storageManager->getWorkspace()->composeAbsolutePath($this->formattedPath);
	}

	/**
	 * @param integer $maxWidth
	 * @param integer $maxHeight
	 * @return array|null
	 */
	protected function findSize($maxWidth, $maxHeight)
	{
		$predefinedSizes = $this->getPredefinedSizes();
		if ($predefinedSizes === false || ($maxWidth === 0 && $maxHeight === 0))
		{
			return [$maxWidth, $maxHeight];
		}
		$size = null;
		foreach ($predefinedSizes as $fSize)
		{
			[$width, $height] = $fSize;
			if ($size === null)
			{
				$size = $fSize;
			}
			elseif ($width >= $maxWidth && $height >= $maxHeight)
			{
				$size = $fSize;
			}
		}
		return $size;
	}

	/**
	 * @return null|string
	 */
	public function getPublicURL()
	{
		if ($this->baseURL !== false && $this->formatted)
		{
			$size = $this->formatted ?: [0, 0];
			$formattedSize = $this->findSize($size[0], $size[1]);
			return $this->baseURL . '/Imagestorage/' . $this->name . '/' . $formattedSize[0] . '/' . $formattedSize[1] . $this->parsedURL['path'];
		}
		return parent::getPublicURL();
	}

	/**
	 * @param int $flags
	 * @return array|false
	 */
	public function url_stat($flags)
	{
		if ($this->formatted)
		{
			$formattedFilename = $this->getFormattedPath() . DIRECTORY_SEPARATOR . $this->getRelativeFormattedPath($this->parsedURL['path']);
			if ((STREAM_URL_STAT_QUIET & $flags) === STREAM_URL_STAT_QUIET && !\file_exists($formattedFilename))
			{
				return false;
			}
			return \stat($formattedFilename);
		}
		return parent::url_stat($flags);
	}

	/**
	 * @param string $path
	 * @param string $separator
	 * @return string
	 */
	protected function getRelativeFormattedPath($path, $separator = DIRECTORY_SEPARATOR)
	{
		$size = $this->formatted ?: [0, 0];
		return $size[0] . $separator  . $size[1] . $path;
	}

	public function stream_open($mode, $options, &$opened_path, &$context)
	{
		if ($this->formatted)
		{
			$fileName = $this->getFormattedPath() . DIRECTORY_SEPARATOR . $this->getRelativeFormattedPath($this->parsedURL['path']);
			$this->resource = $this->safeOpen($fileName, $mode);
			return \is_resource($this->resource);
		}
		return parent::stream_open($mode, $options, $opened_path, $context);
	}

	public function stream_write($data)
	{
		$this->modifiedStream = true;
		return parent::stream_write($data);
	}

	public function stream_truncate($new_size)
	{
		$this->modifiedStream = true;
		return parent::stream_truncate($new_size);
	}

	public function stream_close()
	{
		parent::stream_close();
		if (!$this->formatted && $this->modifiedStream)
		{
			$this->refreshFormatted();
		}
	}

	/**
	 * @return  boolean Returns TRUE on success or FALSE on failure.
	 */
	public function unlink()
	{
		if ($this->formatted)
		{
			$fileName = $this->getFormattedPath() . DIRECTORY_SEPARATOR . $this->getRelativeFormattedPath($this->parsedURL['path']);
			return @\unlink($fileName);
		}
		$unlink = parent::unlink();
		$this->refreshFormatted();
		return $unlink;
	}

	public function stream_metadata($option, $var)
	{
		if ($option === STREAM_META_TOUCH)
		{
			if ($this->formatted)
			{
				$fileName = $this->getFormattedPath() . DIRECTORY_SEPARATOR . $this->getRelativeFormattedPath($this->parsedURL['path']);
				if (@\filesize($fileName))
				{
					/** @noinspection PotentialMalwareInspection */
					return @\touch($fileName, $var[0] ?? null, $var[1] ?? null);
				}
			}
			else
			{
				$this->refreshFormatted();
				return parent::stream_metadata($option, $var);
			}
		}
		return false;
	}

	public function rename($pathTo)
	{
		if (parent::rename($pathTo))
		{
			$this->refreshFormatted();
			$toItem = $this->getStorageManager()->getItemInfo($pathTo);
			if ($toItem && $toItem->getStorageEngine() && $toItem->getStorageEngine()->getName() === $this->getName())
			{
				@\touch($pathTo);
			}
			return true;
		}
		return false;
	}

	protected function refreshFormatted()
	{
		$originalPath = $this->parsedURL['path'];
		$formattedPath = $this->getFormattedPath();
		$pattern = $formattedPath . DIRECTORY_SEPARATOR . '*' . DIRECTORY_SEPARATOR . '*' . $originalPath;

		$paths = \glob($pattern, GLOB_NOSORT + GLOB_NOESCAPE);
		if ($paths)
		{
			foreach ($paths as $path)
			{
				@\unlink($path);
			}
		}

		if (!$this->getGenerateFormated())
		{
			return;
		}
		$formattedBasePath = $this->getFormattedPath();
		$predefinedSizes = $this->getPredefinedSizes();
		if ($predefinedSizes && $formattedBasePath)
		{
			$originalFileName = $this->basePath . \str_replace('/', DIRECTORY_SEPARATOR, $originalPath);
			if (\file_exists($originalFileName))
			{
				$formattedFileName = $formattedBasePath . DIRECTORY_SEPARATOR . '0' . DIRECTORY_SEPARATOR . '0' . $originalPath;
				\Change\Stdlib\FileUtils::mkdir(\dirname($formattedFileName));
				\copy($originalFileName, $formattedFileName);

				$resizer = new \Change\Presentation\Images\Resizer();
				$originalSize = $resizer->getImageSize($originalFileName);
				if ($originalSize['width'] && $originalSize['height'])
				{
					foreach ($predefinedSizes as $formatSize)
					{
						$formattedFileName = $formattedBasePath . DIRECTORY_SEPARATOR . $formatSize[0]
							. DIRECTORY_SEPARATOR . $formatSize[1] . $originalPath;
						\Change\Stdlib\FileUtils::mkdir(\dirname($formattedFileName));
						if ($formatSize[0] >= $originalSize['width'] && $formatSize[1] >= $originalSize['height'])
						{
							\copy($originalFileName, $formattedFileName);
						}
						else
						{
							$resizer->resize($originalFileName, $formattedFileName, $formatSize[0], $formatSize[1], $this->compression);
						}
					}
				}
			}
		}
	}
}