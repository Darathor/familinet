<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Storage;

/**
 * @name \Change\Storage\StorageManager
 */
class StorageManager
{
	const DEFAULT_SCHEME = 'change';

	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\Job\JobManager
	 */
	protected $jobManager;

	/**
	 * @param \Change\Application $application
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		if ($this->isRegistered())
		{
			$this->unRegister();
		}
		$this->register();
	}

	/**
	 * @return \Change\Application
	 */
	protected function getApplication()
	{
		return $this->application;
	}

	/**
	 * @return \Change\Job\JobManager
	 */
	protected function getJobManager()
	{
		return $this->jobManager;
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @return $this
	 */
	public function setJobManager($jobManager)
	{
		$this->jobManager = $jobManager;
		return $this;
	}

	/**
	 * @return \Change\Workspace
	 */
	public function getWorkspace()
	{
		return $this->getApplication()->getWorkspace();
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	public function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @return \Change\Configuration\Configuration
	 */
	protected function getConfiguration()
	{
		return $this->getApplication()->getConfiguration();
	}

	/**
	 * @param string $storageName
	 * @param array $configuration
	 * @return $this
	 */
	public function addStorageConfiguration($storageName, $configuration)
	{
		$this->getConfiguration()->addVolatileEntry('Change/Storage/' . $storageName, $configuration);
		return $this;
	}

	/**
	 * @api
	 * @return boolean
	 */
	public function isRegistered()
	{
		return in_array(static::DEFAULT_SCHEME, stream_get_wrappers());
	}

	/**
	 * @api
	 * @return void
	 */
	public function register()
	{
		StreamWrapper::storageManager($this);
		stream_register_wrapper(static::DEFAULT_SCHEME, '\Change\Storage\StreamWrapper');
	}

	/**
	 * @api
	 * @return void
	 */
	public function unRegister()
	{
		stream_wrapper_unregister(static::DEFAULT_SCHEME);
		StreamWrapper::storageManager($this);
	}

	/**
	 * @return string[]
	 */
	public function getStorageNames()
	{
		$config = $this->getConfiguration()->getEntry('Change/Storage');
		if (!is_array($config))
		{
			return ['tmp'];
		}
		else
		{
			return array_merge(['tmp'], array_keys($config));
		}
	}

	/**
	 * @param string $storageName
	 * @param string $path
	 * @param array $query
	 * @return \Zend\Uri\Uri
	 */
	public function buildChangeURI($storageName, $path, array $query = [])
	{
		$uri = new \Zend\Uri\Uri();
		return $uri->setScheme(static::DEFAULT_SCHEME)->setHost($storageName)->setPath($path)->setQuery($query);
	}

	/**
	 * @api
	 * @param string $name
	 * @return Engines\AbstractStorage|null
	 */
	public function getStorageByName($name)
	{
		$configurations = $this->getConfiguration()->getEntry('Change/Storage');
		if (is_array($configurations))
		{
			$toLowerName = strtolower($name);
			$storageByName = null;
			$config = null;
			foreach ($configurations as $storageByName => $configuration)
			{
				if (strtolower($storageByName) == $toLowerName)
				{
					$config = $configuration;
					break;
				}
			}

			if (($className = $config['class'] ?? null) && class_exists($className))
			{
				$storageEngine = new $className($storageByName, $config);
				if ($storageEngine instanceof Engines\AbstractStorage)
				{
					$storageEngine->setStorageManager($this);
					return $storageEngine;
				}
			}
		}

		if ($name === 'tmp')
		{
			$basePath = $this->getWorkspace()->tmpPath('Storage');
			\Change\Stdlib\FileUtils::mkdir($basePath);
			$config = ['basePath' => $basePath, 'baseURL' => false];
			$storageEngine = new \Change\Storage\Engines\LocalStorage($name, $config);
			$storageEngine->setStorageManager($this);
			return $storageEngine;
		}
		return null;
	}

	/**
	 * @api
	 * @param string $storageURI
	 * @return Engines\AbstractStorage|null
	 */
	public function getStorageByStorageURI($storageURI)
	{
		if ($storageURI && (strpos($storageURI, 'change:') === 0))
		{
			$parsedURL = parse_url($storageURI);
			if (\is_array($parsedURL) && $parsedURL['scheme'] === static::DEFAULT_SCHEME && isset($parsedURL['host']))
			{
				$storageEngine = $this->getStorageByName($parsedURL['host']);
				if ($storageEngine)
				{
					if (!isset($parsedURL['path']) || !$parsedURL['path'])
					{
						$parsedURL['path'] = '/';
					}
					$storageEngine->setParsedURL($parsedURL);
					return $storageEngine;
				}
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param $storageURI
	 * @return \Change\Storage\ItemInfo|null
	 */
	public function getItemInfo($storageURI)
	{
		$storageEngine = $this->getStorageByStorageURI($storageURI);
		if ($storageEngine)
		{
			$itemInfo = new ItemInfo($storageURI);
			$itemInfo->setStorageEngine($storageEngine);
			return $itemInfo;
		}
		return null;
	}

	/**
	 * @api
	 * @param $url
	 * @return string|null
	 */
	public function getMimeType($url)
	{
		$itemInfo = $this->getItemInfo($url);
		return $itemInfo ? $itemInfo->getMimeType() : null;
	}

	/**
	 * @api
	 * @param $url
	 * @return string|null
	 */
	public function getPublicURL($url)
	{
		$itemInfo = $this->getItemInfo($url);
		return $itemInfo ? $itemInfo->getPublicURL() : null;
	}

	/**
	 * Creates a job to delete the file after the given number of minutes.
	 * @param string $storageURI
	 * @param int $TTL The file TTL in minutes.
	 * @return int
	 * @throws \Exception
	 */
	public function cleanUp($storageURI, $TTL = 60)
	{
		if ($storageURI)
		{
			$date = new \DateTime();
			if ($TTL > 0)
			{
				$date->add(new \DateInterval('PT' . $TTL . 'M'));
			}
			$job = $this->getJobManager()->createNewJob('Change_Storage_URICleanUp', ['storageURI' => $storageURI], $date);
			return $job->getId();
		}
		return 0;
	}
}