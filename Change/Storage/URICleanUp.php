<?php
namespace Change\Storage;

/**
 * @name \Change\Storage\URICleanUp
 */
class URICleanUp
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$fileChangeURIList = (array)$job->getArgument('storageURI');
		$fileChangeURIList = array_filter($fileChangeURIList);
		if (!$fileChangeURIList)
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$applicationServices->getStorageManager();
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			foreach ($fileChangeURIList as $fileChangeURI)
			{
				@unlink($fileChangeURI);
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->success();
	}
}