<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ExportEngine
 */
class ExportEngine implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const OPTION_CONTEXT_CODE = 'contextCode';

	const OPTION_NEW_CODES = 'newCodes';

	const EVENT_MANAGER_IDENTIFIER = 'ExportEngine';

	const EVENT_RESOLVE = 'resolve';

	const EVENT_POPULATE = 'populate';

	const EVENT_CHUNK = 'chunk';

	const EVENT_FINALIZE = 'finalize';

	/**
	 * @var \Zend\Stdlib\Parameters
	 */
	protected $options;

	/**
	 * @param \Change\Application $application
	 * @param array $options
	 */
	public function __construct(\Change\Application $application, array $options = null)
	{
		$this->setApplication($application);
		if ($options)
		{
			$this->getOptions()->fromArray($options);
		}
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getOptions()
	{
		if ($this->options === null)
		{
			$this->options = new \Zend\Stdlib\Parameters();
		}
		return $this->options;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return array
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/' . static::EVENT_MANAGER_IDENTIFIER);
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		(new ExportDocumentListener($this))->attach($eventManager);

		$eventManager->attach(static::EVENT_FINALIZE, function(ExportEvent $event) {
			$this->onFinalize($event);
		}, 5);
	}

	/**
	 * @return mixed
	 */
	public function getContextCode()
	{
		return $this->getOptions()->get(static::OPTION_CONTEXT_CODE, 0);
	}

	/**
	 * @api
	 * @param integer $id
	 * @param string $code
	 * @return boolean
	 */
	public function addNewCode($id, $code)
	{
		if ($id && is_int($id) && $code)
		{
			$codes = $this->getOptions()->get(self::OPTION_NEW_CODES, []);
			$codes[$id] = (string)$code;
			$this->getOptions()->set(self::OPTION_NEW_CODES, $codes);
			return true;
		}
		return false;
	}

	/**
	 * @api
	 * @param integer $id
	 * @return boolean|string
	 */
	public function getNewCode($id)
	{
		if ($id && is_int($id))
		{
			$codes = $this->getOptions()->get(self::OPTION_NEW_CODES, []);
			return isset($codes[$id]) ? $codes[$id] : false;
		}
		return false;
	}

	/**
	 * @api
	 * @param mixed $item
	 * @return array|null
	 */
	public function exportItem($item)
	{
		if ($item === null)
		{
			return null;
		}

		$exportData = new \Zend\Stdlib\Parameters();
		$event = new ExportEvent(static::EVENT_RESOLVE, $this, ['exportData' => $exportData, 'item' => $item, 'expectedModel' => null]);
		$eventManager = $this->getEventManager();
		$eventManager->triggerEvent($event);
		if (!isset($exportData['__id'], $exportData['__model']))
		{
			throw new \RuntimeException('Unable to resole item', 1);
		}
		$event->setName(static::EVENT_POPULATE);
		$eventManager->triggerEvent($event);

		$data = $exportData->toArray();
		return $data?: null;
	}

	/**
	 * @api
	 * @param mixed $item
	 * @param string $expectedModel
	 * @return array|null
	 */
	public function resolveItem($item, $expectedModel)
	{
		if ($item === null)
		{
			return null;
		}

		$exportData = new \Zend\Stdlib\Parameters();
		$event = new ExportEvent(static::EVENT_RESOLVE, $this,
			['exportData' => $exportData, 'item' => $item, 'expectedModel' => $expectedModel]);
		$eventManager = $this->getEventManager();
		$eventManager->triggerEvent($event);
		if (!isset($exportData['__id']))
		{
			return null;
		}
		$data = $exportData->toArray();
		return $data?: null;
	}

	/**
	 * @api
	 * @param string|\DateTime|null $date
	 * @return null|string
	 */
	public function normalizeDate($date)
	{
		if ($date)
		{
			if (is_string($date))
			{
				$date = new \DateTime($date);
			}
			if ($date instanceof \DateTime) {
				return $date->format(\DateTime::ATOM);
			}
		}
		return null;
	}

	/**
	 * @api
	 * @param string $model
	 * @param integer $chunk
	 * @param array $filter
	 * @return array
	 */
	public function getItems($model, $chunk, array &$filter)
	{
		$event = new ExportEvent(static::EVENT_CHUNK, $this, ['model' => $model,
			'chunk' => $chunk, 'filter' => $filter]);

		$eventManager = $this->getEventManager();
		$eventManager->triggerEvent($event);
		$items = $event->getParam('items');
		$filter = $event->getParam('filter');
		return (is_array($items)) ? $items : [];
	}

	/**
	 * @api
	 */
	public function finalize()
	{
		$event = new ExportEvent(static::EVENT_FINALIZE, $this, []);
		$eventManager = $this->getEventManager();
		$eventManager->triggerEvent($event);
	}

	/**
	 * @param ExportEvent $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onFinalize(ExportEvent $event)
	{
		$codes =  $this->getOptions()->get(self::OPTION_NEW_CODES, []);
		if ($codes)
		{
			$tm = $event->getApplicationServices()->getTransactionManager();
			$context = $this->getContextCode();
			$documentCodeManager = $event->getApplicationServices()->getDocumentCodeManager();
			try
			{
				$tm->begin();
				foreach ($codes as $id => $code)
				{
					$documentCodeManager->addDocumentCode($id, $code, $context);
				}
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}
	}
}