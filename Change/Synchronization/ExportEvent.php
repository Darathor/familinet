<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ExportEvent
 */
class ExportEvent extends \Change\Events\Event
{

	/**
	 * @return ExportEngine
	 */
	public function getExportEngine()
	{
		return $this->getTarget();
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function getExportData()
	{
		return $this->getParam('exportData');
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->getParam('item');
	}
}