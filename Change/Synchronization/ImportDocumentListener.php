<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ImportDocumentListener
 * @ignore
 */
class ImportDocumentListener extends \Zend\EventManager\AbstractListenerAggregate
{

	/**
	 * @var \Change\Synchronization\ImportEngine
	 */
	protected $importEngine;

	/**
	 * DefaultImportDocument constructor.
	 * @param \Change\Synchronization\ImportEngine $importEngine
	 */
	public function __construct(\Change\Synchronization\ImportEngine $importEngine)
	{
		$this->importEngine = $importEngine;
	}

	/**
	 * @return \Change\Synchronization\ImportEngine
	 */
	public function getImportEngine()
	{
		return $this->importEngine;
	}

	/**
	 * @param \Change\Synchronization\ImportEngine $importEngine
	 * @return $this
	 */
	public function setImportEngine(\Change\Synchronization\ImportEngine $importEngine)
	{
		$this->importEngine = $importEngine;
		return $this;
	}

	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_RESOLVE, function (ImportEvent $event)
		{
			$this->onResolveDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_CREATE, function (ImportEvent $event)
		{
			$this->onCreateDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_POPULATE, function (ImportEvent $event)
		{
			$this->onPopulateLocalizedDocument($event);
			$this->onPopulateEditableDocument($event);
		}, 15);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_POPULATE, function (ImportEvent $event)
		{
			$this->onPopulateDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_SAVE, function (ImportEvent $event)
		{
			$this->onSaveDocument($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Synchronization\ImportEngine::EVENT_DELETE, function (ImportEvent $event)
		{
			$this->onDeleteDocument($event);
		}, 5);
	}

	/**
	 * Event input params: itemData
	 * Event output params: item
	 * @param ImportEvent $event
	 */
	protected function onResolveDocument(ImportEvent $event)
	{
		if ($event->getItem() !== null)
		{
			return;
		}
		$itemData = $event->getItemData();
		$modelName = $itemData['__model'];
		$code = (string)$itemData['__id'];
		$contextCode = $this->getImportEngine()->getContextCode();
		$item = $event->getApplicationServices()->getDocumentCodeManager()
			->getFirstDocumentByCode($code, $contextCode, $modelName);

		if (!$item && strpos($code, '__REF_ID_') === 0)
		{
			$docId = substr($code, 9);
			if (is_numeric($docId))
			{
				$item = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($docId, $modelName);
			}
		}

		if (!$item)
		{
			$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
			if ($model && !($model->isAbstract() || $model->isInline()))
			{
				$property = $model->getProperty('code');
				if ($property && !($property->getLocalized() || $property->getStateless()))
				{
					$q = $event->getApplicationServices()->getDocumentManager()->getNewQuery($model);
					$item = $q->andPredicates($q->eq('code', $code))->addOrder('id')->getFirstDocument();
				}
			}
		}
		$event->setItem($item);
	}

	/**
	 * Event input params: itemData
	 * Event output params: item, addDocumentCode
	 * @param ImportEvent $event
	 */
	protected function onCreateDocument(ImportEvent $event)
	{
		if ($event->getItem() !== null || is_int($event->getItemDataId()))
		{
			return;
		}

		$itemData = $event->getItemData();
		$modelName = $itemData['__model'];
		$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
		if ($model && !($model->isAbstract() || $model->isInline()))
		{
			$item = $event->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModel($model);
			$event->setItem($item);
			$event->setParam('addDocumentCode', true);
		}
	}

	/**
	 * Event input params: itemData, item
	 * Event output params: refLCID, localizations
	 * @param ImportEvent $event
	 */
	protected function onPopulateLocalizedDocument(ImportEvent $event)
	{
		/** @var \Change\Documents\AbstractDocument|\Change\Documents\Interfaces\Localizable $document */
		$document = $event->getItem();
		if ($document instanceof \Change\Documents\Interfaces\Localizable)
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$refLCID = $document->getRefLCID();
			$newRefLCID = $event->getItemDataProperty('refLCID');
			if ($refLCID === null)
			{
				if (!$newRefLCID)
				{
					throw new ImportException('Property "refLCID" can not be null', 11);
				}
				if (!$i18nManager->isSupportedLCID($newRefLCID))
				{
					throw new ImportException('Unsupported "refLCID" value', 12);
				}
			}
			else
			{
				if (!$event->hasItemDataProperty('refLCID'))
				{
					$event->updateItemDataProperty('refLCID', $refLCID);
				}
				elseif (!$newRefLCID)
				{
					throw new ImportException('Property "refLCID" can not be null', 11);
				}
				elseif ($newRefLCID !== $refLCID)
				{
					throw new ImportException('Unsupported "refLCID" value', 12);
				}
			}
			$refLCID = $newRefLCID ?: $refLCID;
			$event->setParam('refLCID', $refLCID);

			$localizations = [];
			if ($event->hasItemDataProperty('LCID'))
			{
				$localizedItemData = $event->getItemDataProperty('LCID');
				if (is_array($localizedItemData))
				{
					foreach ($localizedItemData as $LCID => $itemLCIDData)
					{
						if (is_string($LCID) && $LCID !== $refLCID && $i18nManager->isSupportedLCID($LCID))
						{
							$localizations[] = $LCID;
						}
					}
				}
			}
			$event->setParam('localizations', $localizations);
		}
	}

	/**
	 * Event input params: itemData
	 * Event output params: item, refLCID, localizations
	 * @param ImportEvent $event
	 */
	protected function onPopulateEditableDocument(ImportEvent $event)
	{
		/** @var \Change\Documents\AbstractDocument|\Change\Documents\Interfaces\Editable $document */
		$document = $event->getItem();
		if ($document instanceof \Change\Documents\Interfaces\Editable)
		{
			if ($event->hasItemDataProperty('author'))
			{
				$author = $event->removeItemDataProperty('author');
				$event->updateItemDataProperty('authorId', $author);
			}

			if (!$event->hasItemDataProperty('label'))
			{
				$label = $document->getDocumentModel()->getPropertyValue($document, 'label');
				if ($label === null)
				{
					$document->getDocumentModel()->setPropertyValue($document, 'label', $event->getItemDataId());
				}
			}
		}
	}

	/**
	 * Event input params: itemData, refLCID, localizations
	 * Event output params: item
	 * @param ImportEvent $event
	 */
	protected function onPopulateDocument(ImportEvent $event)
	{
		$item = $event->getItem();
		if (!($item instanceof \Change\Documents\AbstractDocument))
		{
			return;
		}
		$itemData = $event->getItemData();
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$refLCID = $event->getParam('refLCID');
		if ($refLCID)
		{
			try
			{
				$documentManager->pushLCID($refLCID);
				$this->populateProperties($item, $itemData);
				$localizations = $event->getParam('localizations');
				if ($localizations && is_array($localizations))
				{
					foreach ($localizations as $LCID)
					{
						$itemLCIDData = $itemData['LCID'][$LCID];
						try
						{
							$documentManager->pushLCID($LCID);
							$this->populateLocalizedProperties($item, $itemLCIDData);
							$localizations[] = $LCID;
							$documentManager->popLCID();
						}
						catch (\Exception $e)
						{
							$documentManager->popLCID($e);
						}
					}
				}
				$documentManager->popLCID();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
			}
		}
		else
		{
			$this->populateProperties($item, $itemData);
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $item
	 * @param array $itemData
	 */
	protected function populateProperties($item, $itemData)
	{
		$model = $item->getDocumentModel();
		foreach ($itemData as $propertyName => $jsonValue)
		{
			if ($propertyName === 'LCID' || $propertyName === 'publicationStatus' || 0 === strpos($propertyName, '__'))
			{
				continue;
			}
			$property = $model->getProperty($propertyName);
			if (!$property && (substr($propertyName, -2) !== 'Id'))
			{
				$docIdProperty = $model->getProperty($propertyName . 'Id');
				if ($docIdProperty && $docIdProperty->getType() === \Change\Documents\Property::TYPE_DOCUMENTID)
				{
					$property = $docIdProperty;
				}
			}

			if (!$property)
			{
				throw new ImportException('Unsupported property "' . $propertyName . '"', 13);
			}

			$this->populateModelProperty($item, $property, $jsonValue);
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $item
	 * @param array $itemData
	 */
	protected function populateLocalizedProperties($item, $itemData)
	{
		$model = $item->getDocumentModel();
		foreach ($itemData as $propertyName => $jsonValue)
		{
			if ($propertyName === 'LCID' || $propertyName === 'publicationStatus' || 0 === strpos($propertyName, '__'))
			{
				continue;
			}

			$property = $model->getProperty($propertyName);
			if (!$property && (substr($propertyName, -2) !== 'Id'))
			{
				$docIdProperty = $model->getProperty($propertyName . 'Id');
				if ($docIdProperty && $docIdProperty->getType() === \Change\Documents\Property::TYPE_DOCUMENTID)
				{
					$property = $docIdProperty;
				}
			}

			if (!$property || !$property->getLocalized())
			{
				throw new ImportException('Unsupported localized property "' . $propertyName . '"', 14);
			}
			$this->populateModelProperty($item, $property, $jsonValue);
		}
	}

	/**
	 * @api
	 * @param \Change\Documents\AbstractDocument $document
	 * @param \Change\Documents\Property $property
	 * @param mixed $jsonValue
	 */
	public function populateModelProperty(\Change\Documents\AbstractDocument $document,
		\Change\Documents\Property $property, $jsonValue)
	{
		$propertyName = $property->getName();
		if ($jsonValue === null && $property->getRequired())
		{
			throw new ImportException('Property "' . $propertyName . '" can not be null', 11);
		}

		$importEngine = $this->getImportEngine();
		$propertyType = $property->getType();
		switch ($propertyType)
		{
			case \Change\Documents\Property::TYPE_INLINE:
			case \Change\Documents\Property::TYPE_INLINEARRAY:
				break;
			case \Change\Documents\Property::TYPE_STRING:
			case \Change\Documents\Property::TYPE_LONGSTRING:
				if ($jsonValue === null || is_string($jsonValue))
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_RICHTEXT:
				if ($jsonValue === null || is_string($jsonValue)
					|| (is_array($jsonValue) && isset($jsonValue['e'], $jsonValue['t']))
				)
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_STORAGEURI:
				if ($jsonValue === null || (is_string($jsonValue) && 0 === strpos($jsonValue, 'change://')))
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_BOOLEAN:
				if ($jsonValue === true || $jsonValue === false)
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_INTEGER:
				if ($jsonValue === null || is_int($jsonValue))
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_FLOAT:
			case \Change\Documents\Property::TYPE_DECIMAL:
				if ($jsonValue === null || is_int($jsonValue) || is_float($jsonValue))
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_JSON:
				if ($jsonValue === null || is_array($jsonValue))
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_DATE:
			case \Change\Documents\Property::TYPE_DATETIME:
				if (is_string($jsonValue))
				{
					$jsonValue = new \DateTime($jsonValue);
				}
				if ($jsonValue === null || $jsonValue instanceof \DateTime)
				{
					$property->setValue($document, $jsonValue);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_DOCUMENTID:
				if ($jsonValue === null)
				{
					$property->setValue($document, $jsonValue);
				}
				elseif (is_array($jsonValue))
				{
					if (!isset($jsonValue['__model']))
					{
						$jsonValue['__model'] = $property->getDocumentType();
					}
					$jsonValue['__property'] = $property->getName();
					$value = $importEngine->resolveItem($jsonValue);
					if ($value instanceof \Change\Documents\AbstractDocument)
					{
						$property->setValue($document, $value->getId());
					}
					else
					{
						throw new ImportException('Invalid property "' . $propertyName . '" value', 10, null, $jsonValue);
					}
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_DOCUMENT:
				if ($jsonValue === null)
				{
					$property->setValue($document, $jsonValue);
				}
				elseif (is_array($jsonValue))
				{
					if (!isset($jsonValue['__model']))
					{
						$jsonValue['__model'] = $property->getDocumentType();
					}
					$jsonValue['__property'] = $property->getName();
					$value = $importEngine->resolveItem($jsonValue);
					if ($value instanceof \Change\Documents\AbstractDocument)
					{
						$property->setValue($document, $value);
					}
					else
					{
						throw  new ImportException('Invalid property "' . $propertyName . '" value', 10, null, $jsonValue);
					}
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			case \Change\Documents\Property::TYPE_DOCUMENTARRAY:
				if ($jsonValue === null || (is_array($jsonValue) && !$jsonValue))
				{
					$property->setValue($document, []);
				}
				elseif (is_array($jsonValue))
				{
					$model = $property->getDocumentType();
					$documentArray = [];
					foreach ($jsonValue as $i => $jsonDocumentValue)
					{
						if (!isset($jsonDocumentValue['__model']))
						{
							$jsonDocumentValue['__model'] = $model;
						}
						$jsonDocumentValue['__property'] = $property->getName() . '[' . $i . ']';
						$documentValue = $importEngine->resolveItem($jsonDocumentValue);
						if ($documentValue instanceof \Change\Documents\AbstractDocument)
						{
							$documentArray[] = $documentValue;
						}
						else
						{
							throw new ImportException('Invalid property "' . $propertyName . '" value', 10, null, $jsonDocumentValue);
						}
					}
					$property->setValue($document, $documentArray);
				}
				else
				{
					throw new ImportException('Invalid property "' . $propertyName . '" value', 10);
				}
				break;
			default:
				throw new ImportException('Invalid property "' . $propertyName . '" type', 9);
		}
	}

	/**
	 * Event input params: itemData, item, refLCID, localizations, addDocumentCode
	 * @param ImportEvent $event
	 */
	protected function onSaveDocument(ImportEvent $event)
	{
		$item = $event->getItem();
		if ($item instanceof \Change\Documents\AbstractDocument)
		{
			$item->useCorrection(false);
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			if ($event->getParam('refLCID'))
			{
				try
				{
					$documentManager->pushLCID($event->getParam('refLCID'));
					if ($item->hasModifiedProperties())
					{
						$item->save();
					}
					$documentManager->popLCID();
				}
				catch (\Change\Documents\PropertiesValidationException $e)
				{
					$ie = new ImportException($e->getMessage(), 20, $e);
					$documentManager->popLCID($ie);
				}

				$localisations = $event->getParam('localizations');
				if ($localisations && is_array($localisations))
				{
					foreach ($localisations as $LCID)
					{
						try
						{
							$documentManager->pushLCID($LCID);
							if ($item->hasModifiedProperties())
							{
								$item->save();
							}
							$documentManager->popLCID();
						}
						catch (\Change\Documents\PropertiesValidationException $e)
						{
							$ie = new ImportException($e->getMessage(), 20, $e);
							$documentManager->popLCID($ie);
						}
					}
				}
			}
			else
			{
				try
				{
					if ($item->hasModifiedProperties())
					{
						$item->save();
					}
				}
				catch (\Change\Documents\PropertiesValidationException $e)
				{
					throw new ImportException($e->getMessage(), 20, $e);
				}
			}

			if ($event->getParam('addDocumentCode'))
			{
				$itemData = $event->getItemData();
				$code = $itemData['__id'];
				$contextCode = $this->getImportEngine()->getContextCode();
				$event->getApplicationServices()->getDocumentCodeManager()->addDocumentCode($item, $code, $contextCode);
				$event->setParam('addDocumentCode', false);
			}
		}
	}

	protected function onDeleteDocument(ImportEvent $event)
	{
		$item = $event->getItem();
		if ($item instanceof \Change\Documents\AbstractDocument)
		{
			$item->useCorrection(false);
			if ($item instanceof \Change\Documents\Interfaces\Localizable)
			{
				$refLCID = $item->getRefLCID();
				$documentManager = $event->getApplicationServices()->getDocumentManager();
				foreach ($item->getLCIDArray() as $LCID)
				{
					if ($LCID === $refLCID)
					{
						continue;
					}

					try
					{
						$documentManager->pushLCID($LCID);
						$item->deleteCurrentLocalization();
						$documentManager->popLCID();
					}
					catch (\Change\Documents\PropertiesValidationException $e)
					{
						$ie = new ImportException($e->getMessage(), 20, $e);
						$documentManager->popLCID($ie);
					}
				}
			}
			$item->delete();
		}
	}
}