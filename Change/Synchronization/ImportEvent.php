<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ImportEvent
 */
class ImportEvent extends \Change\Events\Event
{

	/**
	 * @return ImportEngine
	 */
	public function getImportEngine()
	{
		return $this->getTarget();
	}

	/**
	 * @return array
	 */
	public function getItemData()
	{
		return (array)$this->getParam('itemData');
	}

	/**
	 * @param array $itemData
	 * @return $this
	 */
	public function setItemData(array $itemData)
	{
		$this->setParam('itemData', $itemData);
		return $this;
	}

	/**
	 * @param string $propertyName
	 * @return boolean
	 */
	public function hasItemDataProperty($propertyName)
	{
		$itemData = $this->getItemData();
		return (array_key_exists($propertyName, $itemData));
	}

	/**
	 * @param string $propertyName
	 * @param mixed $value
	 * @return $this
	 */
	public function updateItemDataProperty($propertyName, $value)
	{
		$itemData = $this->getItemData();
		$itemData[$propertyName] = $value;
		$this->setParam('itemData', $itemData);
		return $this;
	}

	/**
	 * @param string $propertyName
	 * @return mixed Old value
	 */
	public function removeItemDataProperty($propertyName)
	{
		$itemData = $this->getItemData();
		if (array_key_exists($propertyName, $itemData))
		{
			$oldValue = $itemData[$propertyName];
			unset($itemData[$propertyName]);
			$this->setParam('itemData', $itemData);
			return $oldValue;
		}
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getItemDataModel()
	{
		$itemData = $this->getItemData();
		return isset($itemData['__model']) ? $itemData['__model'] : null;
	}

	/**
	 * @return string|null
	 */
	public function getItemDataId()
	{
		$itemData = $this->getItemData();
		return isset($itemData['__id']) ? $itemData['__id'] : null;
	}

	/**
	 * @param string $propertyName
	 * @return mixed|null
	 */
	public function getItemDataProperty($propertyName)
	{
		$itemData = $this->getItemData();
		return isset($itemData[$propertyName]) ? $itemData[$propertyName] : null;
	}

	/**
	 * @return mixed
	 */
	public function getItem()
	{
		return $this->getParam('item');
	}

	/**
	 * @param mixed $item
	 * @return $this
	 */
	public function setItem($item)
	{
		$this->setParam('item', $item);
		return $this;
	}
}