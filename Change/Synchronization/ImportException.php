<?php
/**
 * Copyright (C) 2014 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\ImportException
 */
class ImportException extends \RuntimeException
{
	/**
	 * @var array
	 */
	protected $itemData = [];

	/**
	 * @param string $message
	 * @param integer $code
	 * @param \Exception|null $previous
	 * @param array|null $itemData
	 */
	public function __construct($message = '', $code = 0, \Exception $previous = null, array $itemData = null)
	{
		if ($itemData)
		{
			$this->setItemData($itemData);
		}
		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return array
	 */
	public function getItemData()
	{
		return $this->itemData;
	}

	/**
	 * @param array $itemData
	 * @return $this
	 */
	public function setItemData(array $itemData)
	{
		$this->itemData = $itemData + ['__model' => null, '__id' => null, '__property' => null];
		return $this;
	}

	/**
	 * @param array $itemData
	 * @return string
	 */
	public function getImportError(array $itemData = null)
	{
		$path = ''; $model = null; $id = null;
		if ($itemData)
		{
			$itemData += ['__model' => null, '__id' => null];
			$model = is_string($itemData['__model']) ? $itemData['__model'] : null;
			$id = is_string($itemData['__id']) ? $itemData['__id'] : null;
			if (($id || $model)
				&& (!$this->itemData || $this->itemData['__model'] != $model || $this->itemData['__id'] != $id) )
			{
				$path .= '(__model: ' . ($model?: 'ERROR') . ' / __id: ' . ($id?: 'ERROR'). ') ';
			}
		}

		if ($this->itemData)
		{
			$itemData = $this->itemData;
			$model = is_string($itemData['__model']) ? $itemData['__model'] : null;
			$id = is_string($itemData['__id']) ? $itemData['__id'] : null;
			$property = $itemData['__property'];
			if ($id || $model)
			{
				$path .= $property . ' (__model: ' . ($model?: 'ERROR') . ' / __id: ' . ($id?: 'ERROR'). ') ';
			}
		}

		return $path . '[ERR ' . $this->getCode() . ': ' . $this->getMessage() . ']';
	}
}