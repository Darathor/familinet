<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\Synchronization;

/**
 * @name \Change\Synchronization\WorkflowLite
 */
class WorkflowLite
{

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function createNewWorkflowInstance(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('workflowInstance') !== null)
		{
			return;
		}
		$document = $event->getDocument();
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$applicationServices = $event->getApplicationServices();
			$documentManager = $applicationServices->getDocumentManager();
			/** @noinspection PhpUndefinedFieldInspection */
			$status = $document->workflowLiteStatus ?? \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE;

			if ($document->useWorkflow())
			{
				$workflowManager = $applicationServices->getWorkflowManager();
				if ($document instanceof \Change\Documents\Interfaces\Localizable)
				{
					if ($status === \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE)
					{
						/** @var \Rbs\Workflow\Documents\Workflow $workflow */
						$workflowInstance = $this->generatePublishableWorkflow($documentManager, $workflowManager, $document);
						$workflowInstance->save();
						$this->generateTasks($documentManager, $workflowInstance);
						$document->updatePublicationStatus($status);
						$event->setTarget($document);
						$event->setParam('workflowInstance', $workflowInstance);
					}
					elseif ($status === \Change\Documents\Interfaces\Publishable::STATUS_FROZEN)
					{
						$workflowInstance = $this->generateFrozenWorkflow($documentManager, $workflowManager, $document);
						$workflowInstance->save();
						$this->generateTasks($documentManager, $workflowInstance);
						$document->updatePublicationStatus($status);
						$event->setTarget($document);
						$event->setParam('workflowInstance', $workflowInstance);
					}
				}
				else
				{
					if ($status === \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE)
					{
						/** @var \Rbs\Workflow\Documents\Workflow $workflow */
						$workflowInstance = $this->generatePublishableWorkflow($documentManager, $workflowManager, $document);
						$workflowInstance->save();
						$document->updatePublicationStatus($status);
						$event->setTarget($document);
						$this->generateTasks($documentManager, $workflowInstance);
						$event->setParam('workflowInstance', $workflowInstance);
					}
					elseif ($status === \Change\Documents\Interfaces\Publishable::STATUS_FROZEN)
					{
						$workflowInstance = $this->generateFrozenWorkflow($documentManager, $workflowManager, $document);
						$workflowInstance->save();
						$document->updatePublicationStatus($status);
						$event->setTarget($document);
						$this->generateTasks($documentManager, $workflowInstance);
						$event->setParam('workflowInstance', $workflowInstance);
					}
				}
			}
			else
			{
				$document->updatePublicationStatus($status);
			}
		}
	}

	/**
	 * @param string $name
	 * @param \Rbs\Workflow\Documents\Workflow $workflow
	 * @return \Rbs\Workflow\Std\Place|null
	 */
	protected function getPlaceByName($name, \Rbs\Workflow\Documents\Workflow $workflow)
	{
		foreach ($workflow->getItems() as $item)
		{
			if ($item instanceof \Rbs\Workflow\Std\Place && $item->getName() == $name)
			{
				return $item;
			}
		}
		return null;
	}

	/**
	 * @param string $name
	 * @param \Rbs\Workflow\Documents\Workflow $workflow
	 * @return \Rbs\Workflow\Std\Transition|null
	 */
	protected function getTransitionByName($name, \Rbs\Workflow\Documents\Workflow $workflow)
	{
		foreach ($workflow->getItems() as $item)
		{
			if ($item instanceof \Rbs\Workflow\Std\Transition && $item->getName() == $name)
			{
				return $item;
			}
		}
		return null;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Workflow\WorkflowManager $workflowManager
	 * @param \Change\Documents\AbstractDocument $document
	 * @return \Rbs\Workflow\Documents\WorkflowInstance
	 */
	protected function generatePublishableWorkflow($documentManager, $workflowManager, $document)
	{
		/** @var \Rbs\Workflow\Documents\Workflow $workflow */
		$workflow = $workflowManager->getWorkflow('publicationProcess');
		/** @var \Rbs\Workflow\Documents\WorkflowInstance $workflowInstance */
		$workflowInstance = $documentManager->getNewDocumentInstanceByModelName('Rbs_Workflow_WorkflowInstance');
		$workflowInstance->setDocument($document);
		$workflowInstance->setStatus(\Rbs\Workflow\Documents\WorkflowInstance::STATUS_OPEN);
		$workflowInstance->setContextData([\Change\Workflow\Interfaces\WorkItem::DOCUMENT_ID_CONTEXT_KEY => $document->getId()]);
		$workflowInstance->setWorkflow($workflow);

		$now = new \DateTime();

		$place = $this->getPlaceByName('Publishable', $workflow);
		$token = $workflowInstance->createToken($place);
		$token->enable($now);

		$transition = $this->getTransitionByName('Check Publication', $workflow);
		$workItem1 = $workflowInstance->createWorkItem($transition);
		$workItem1->enable($now);

		$transition = $this->getTransitionByName('Freeze', $workflow);
		$workItem2 = $workflowInstance->createWorkItem($transition);
		$workItem2->enable($now);

		$transition = $this->getTransitionByName('File', $workflow);
		$workItem3 = $workflowInstance->createWorkItem($transition);
		$workItem3->enable($now);

		return $workflowInstance;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Workflow\WorkflowManager $workflowManager
	 * @param \Change\Documents\AbstractDocument $document
	 * @return \Rbs\Workflow\Documents\WorkflowInstance
	 */
	protected function generateFrozenWorkflow($documentManager, $workflowManager, $document)
	{
		/** @var \Rbs\Workflow\Documents\Workflow $workflow */
		$workflow = $workflowManager->getWorkflow('publicationProcess');
		/** @var \Rbs\Workflow\Documents\WorkflowInstance $workflowInstance */
		$workflowInstance = $documentManager->getNewDocumentInstanceByModelName('Rbs_Workflow_WorkflowInstance');
		$workflowInstance->setDocument($document);
		$workflowInstance->setStatus(\Rbs\Workflow\Documents\WorkflowInstance::STATUS_OPEN);
		$workflowInstance->setContextData([\Change\Workflow\Interfaces\WorkItem::DOCUMENT_ID_CONTEXT_KEY => $document->getId()]);
		$workflowInstance->setWorkflow($workflow);

		$now = new \DateTime();

		$place = $this->getPlaceByName('Freeze', $workflow);
		$token = $workflowInstance->createToken($place);
		$token->enable($now);

		$transition = $this->getTransitionByName('Unfreeze', $workflow);
		$workItem1 = $workflowInstance->createWorkItem($transition);
		$workItem1->enable($now);

		return $workflowInstance;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Rbs\Workflow\Documents\WorkflowInstance $workflowInstance
	 */
	protected function generateTasks($documentManager, $workflowInstance)
	{
		/* @var $workItems \Rbs\Workflow\Std\WorkItem[] */
		$workItems = [];
		foreach ($workflowInstance->getItems() as $item)
		{
			if ($item instanceof \Rbs\Workflow\Std\WorkItem && $item->getTransitionTrigger() !== \Rbs\Workflow\Std\Transition::TRIGGER_AUTO)
			{
				$workItems['T' . $item->getTaskId()] = $item;
			}
		}

		if (!$workItems)
		{
			return;
		}

		foreach ($workItems as $workItem)
		{
			if ($workItem->getStatus() != \Rbs\Workflow\Std\WorkItem::STATUS_ENABLED)
			{
				continue;
			}

			/* @var $task  \Rbs\Workflow\Documents\Task */
			$task = $documentManager->getNewDocumentInstanceByModelName('Rbs_Workflow_Task');

			$transition = $workItem->getTransition();
			$task->setLabel($transition->getName());
			$task->setWorkflowInstance($workflowInstance);
			$task->setTaskId($workItem->getTaskId());
			$task->setTaskCode($transition->getTaskCode());
			$task->setStatus($workItem->getStatus());

			$document = $workflowInstance->getDocument();
			if ($document)
			{
				$task->setDocument($workflowInstance->getDocument());
				$task->setPrivilege($document->getDocumentModelName());
				if ($document instanceof \Change\Documents\Interfaces\Localizable)
				{
					$task->setDocumentLCID($document->getCurrentLCID());
				}
			}

			if ($transition->getTrigger() === \Rbs\Workflow\Std\Transition::TRIGGER_USER)
			{
				$task->setRole($transition->getRole());
				$task->setShowInDashboard($transition->getShowInDashboard());
			}
			elseif ($transition->getTrigger() === \Rbs\Workflow\Std\Transition::TRIGGER_TIME)
			{
				$ctx = $workflowInstance->getContext();
				if (isset($ctx[$transition->getTaskCode() . 'DeadLine']))
				{
					$deadLine = new \DateTime($ctx[$transition->getTaskCode() . 'DeadLine']);
				}
				else
				{
					$deadLine = clone($workItem->getEnabledDate());
					$deadLine->add($transition->getTimeLimit());
				}
				$task->setDeadLine($deadLine);
			}
			$task->save();
		}
	}
}