<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\User;

/**
* @name \Change\User\AuthenticationManager
*/
class AuthenticationManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'AuthenticationManager';

	/**
	 * \Change\User\AuthenticationManager::getCurrentUser() or target->getCurrentUser()
	 * is affected when priority < 5
	 */
	const EVENT_LOGIN = 'login';

	/**
	 * \Change\User\AuthenticationManager::getCurrentUser() or target->getCurrentUser()
	 * is affected when priority < 5
	 */
	const EVENT_LOGOUT = 'logout';

	/**
	 * \Change\User\AuthenticationManager::getCurrentUser() or target->getCurrentUser()
	 * is affected when priority < 5
	 */
	const EVENT_BY_USER_ID = 'byUserId';


	const EVENT_INVALIDATE_TOKENS = 'invalidateTokens';
	/**
	 * @var \Change\User\UserInterface|null
	 */
	protected $currentUser;

	/**
	 * @var boolean
	 */
	protected $confirmed = false;


	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach(self::EVENT_BY_USER_ID, function ($event) {$this->onAffectCurrentUser($event);}, 5);
		$eventManager->attach(self::EVENT_LOGIN, function ($event) { $this->onAffectCurrentUser($event); }, 5);
		$eventManager->attach(self::EVENT_LOGOUT, function ($event) { $this->onRemoveCurrentUser($event); }, 5);
		$eventManager->attach(self::EVENT_INVALIDATE_TOKENS, function ($event) { $this->onInvalidateOAuthTokensCached($event); }, 10);
		$eventManager->attach(self::EVENT_INVALIDATE_TOKENS, function ($event) { $this->onInvalidateOAuthTokens($event); }, 5);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onAffectCurrentUser(\Change\Events\Event $event)
	{
		$user = $event->getParam('user');
		$this->getApplication()->getLogging()->setUser($user);
		$this->setCurrentUser($user);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onRemoveCurrentUser(\Change\Events\Event $event)
	{
		$this->getApplication()->getLogging()->setUser(null);
		$this->setCurrentUser(null);
		$this->setConfirmed(false);
	}

	/**
	 * @param \Change\User\UserInterface $currentUser
	 */
	public function setCurrentUser($currentUser = null)
	{
		$this->currentUser = $currentUser instanceof \Change\User\UserInterface ? $currentUser : null ;
	}

	/**
	 * @api
	 * @return \Change\User\UserInterface
	 */
	public function getCurrentUser()
	{
		return $this->currentUser ?? new AnonymousUser();
	}

	/**
	 * @param boolean $confirmed
	 * @return $this
	 */
	public function setConfirmed($confirmed)
	{
		$this->confirmed = $confirmed;
		return $this;
	}

	/**
	 * @api
	 * @return boolean
	 */
	public function getConfirmed()
	{
		return $this->confirmed;
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Change/Events/AuthenticationManager');
	}

	/**
	 * @api
	 * @param integer $userId
	 * @return \Change\User\UserInterface|null
	 */
	public function getById($userId)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(array('userId' => $userId));
		$event = new \Change\Events\Event(static::EVENT_BY_USER_ID, $this, $args);
		$this->getEventManager()->triggerEvent($event);
		$user = $event->getParam('user');
		if ($user instanceof \Change\User\UserInterface)
		{
			return $user;
		}
		return null;
	}

	/**
	 * @api
	 * @param string $login
	 * @param string $password
	 * @param string $realm
	 * @param array|null $options
	 * @return \Change\User\UserInterface|null
	 */
	public function login($login, $password, $realm, $options = null)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(array('login' => $login,
			'password' => $password, 'realm' => $realm, 'options' => $options));

		$event = new \Change\Events\Event(static::EVENT_LOGIN, $this, $args);
		$this->getEventManager()->triggerEvent($event);
		$user = $event->getParam('user');
		if ($user instanceof \Change\User\UserInterface)
		{
			return $user;
		}
		return null;
	}

	/**
	 * @api
	 * @param array|null $options
	 */
	public function logout($options = null)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['user' => $this->getCurrentUser(), 'options' => $options]);
		$em->trigger(static::EVENT_LOGOUT, $this, $args);
	}

	/**
	 * Need started transaction
	 * @api
	 * @param \Change\User\UserInterface|integer $user
	 */
	public function invalidateTokens($user)
	{
		$em = $this->getEventManager();
		$args = $em->prepareArgs(['user' => $user]);
		$em->trigger(static::EVENT_INVALIDATE_TOKENS, $this, $args);
	}

	/**
	 * Input Param:
	 * - user
	 * Output Param:
	 * - accessorId
	 * - invalidatedOAuthTokens
	 * @param \Change\Events\Event $event
	 */
	protected function onInvalidateOAuthTokens($event)
	{
		$user = $event->getParam('user');
		$accessorId = 0;
		if (is_int($user)) {
			$accessorId = $user;
		} elseif ($user instanceof \Change\User\UserInterface) {
			$accessorId = $user->getId();
		}

		if ($accessorId > 0)
		{
			$event->setParam('accessorId', $accessorId);
			$applicationServices = $event->getApplicationServices();
			$dbProvider = $applicationServices->getDbProvider();
			$sb = $dbProvider->getNewStatementBuilder();
			$fb = $sb->getFragmentBuilder();
			$sb->update($fb->getSqlMapping()->getOAuthTable());
			$sb->assign($fb->column('validity_date'), $fb->dateTimeParameter('validityDate'));
			$sb->where($fb->eq($fb->column('accessor_id'), $fb->integerParameter('accessorId')));

			$update = $sb->updateQuery();
			$update->bindParameter('validityDate', new \DateTime());
			$update->bindParameter('accessorId', $accessorId);
			$event->setParam('invalidatedOAuthTokens', $update->execute());
		}
	}

	/**
	 * Input Param:
	 * - user
	 * Output Param:
	 * - accessorId
	 * - invalidatedOAuthTokens
	 * @param \Change\Events\Event $event
	 */
	protected function onInvalidateOAuthTokensCached($event)
	{
		$user = $event->getParam('user');
		$accessorId = 0;
		if (is_int($user))
		{
			$accessorId = $user;
		}
		elseif ($user instanceof \Change\User\UserInterface)
		{
			$accessorId = $user->getId();
		}

		if ($accessorId > 0)
		{
			$applicationServices = $event->getApplicationServices();
			$dbProvider = $applicationServices->getDbProvider();
			$sb = $dbProvider->getNewQueryBuilder();
			$fb = $sb->getFragmentBuilder();
			$sb->select($fb->column('token'));
			$sb->from($fb->getSqlMapping()->getOAuthTable());
			$sb->where($fb->logicAnd(
				$fb->eq($fb->column('accessor_id'), $fb->integerParameter('accessorId')),
				$fb->gte($fb->column('validity_date'), $fb->dateTimeParameter('validityDate'))
			));

			$select = $sb->query();
			$select->bindParameter('accessorId', $accessorId);
			$select->bindParameter('validityDate', new \DateTime());

			$tokens = $select->getResults($select->getRowsConverter()->addStrCol('token')->singleColumn('token'));

			$cacheManager = $applicationServices->getCacheManager();

			foreach ($tokens as $token)
			{
				$cacheManager->removeEntry('user', 'token_'.$token);
			}
		}
	}
}