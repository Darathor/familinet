<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change\User;

/**
 * @name \Change\User\BaseProfileField
 */
class BaseProfileField implements ProfileFieldInterface
{
	/**
	 * @var string
	 */
	protected $profileName;

	/**
	 * @var string
	 */
	protected $fieldName;

	/**
	 * @var boolean
	 */
	protected $readonly = false;

	/**
	 * @param string $profileName
	 * @param string $fieldName
	 */
	public function __construct($profileName, $fieldName)
	{
		$this->profileName = $profileName;
		$this->fieldName = $fieldName;
	}

	/**
	 * @return string
	 */
	public function getProfileName()
	{
		return $this->profileName;
	}

	/**
	 * @param string $profileName
	 * @return $this
	 */
	public function setProfileName($profileName)
	{
		$this->profileName = $profileName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFieldName()
	{
		return $this->fieldName;
	}

	/**
	 * @param string $fieldName
	 * @return $this
	 */
	public function setFieldName($fieldName)
	{
		$this->fieldName = $fieldName;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getReadonly()
	{
		return $this->readonly;
	}

	/**
	 * @param boolean $readonly
	 * @return $this
	 */
	public function setReadonly($readonly = true)
	{
		$this->readonly = $readonly;
		return $this;
	}
}