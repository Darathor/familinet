<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Change;

/**
 * @name \Change\Workspace
 * @api
 */
class Workspace
{
	protected $tmpBase = false;

	/**
	 * Build a path relative to the "App" folder
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function appPath(string ...$args)
	{
		\array_unshift($args, $this->appBase());
		return $this->buildPathFromComponents($args);
	}

	/**
	 * Build a path relative to the "Change" folder
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function changePath(string ...$args)
	{
		\array_unshift($args, $this->changeBase());
		return $this->buildPathFromComponents($args);
	}

	/**
	 * Build a path relative to the "Compilation" folder
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function compilationPath(string ...$args)
	{
		\array_unshift($args, $this->compilationBase());
		return $this->buildPathFromComponents($args);
	}

	/**
	 * Build a path relative to the "Project" folder
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectPath(string ...$args)
	{
		\array_unshift($args, $this->projectBase());
		return $this->buildPathFromComponents($args);
	}

	/**
	 * Build an absolute path to the project's modules folder (App/Modules/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectModulesPath(string ...$args)
	{
		\array_unshift($args, 'Modules');
		return $this->appPath(...$args);
	}

	/**
	 * Build a relative path to the project's modules folder (App/Modules/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectModulesRelativePath(string ...$args)
	{
		\array_unshift($args, 'App', 'Modules');
		return $this->composePath(...$args);
	}

	/**
	 * Build an absolute path to the plugins modules folder (Plugins/Modules/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function pluginsModulesPath(string ...$args)
	{
		\array_unshift($args, 'Plugins', 'Modules');
		return $this->projectPath(...$args);
	}

	/**
	 * Build a relative path to the plugins modules folder (Plugins/Modules/)
	 *
	 * @api
	 * @param \string[] $args
	 * @return string
	 */
	public function pluginsModulesRelativePath(string ...$args)
	{
		\array_unshift($args, 'Plugins', 'Modules');
		return $this->composePath(...$args);
	}

	/**
	 * Build an absolute path to the project's themes folder (App/Themes/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectThemesPath(string ...$args)
	{
		\array_unshift($args, 'Themes');
		return $this->appPath(...$args);
	}

	/**
	 * Build a relative path to the project's themes folder (App/Themes/)
	 *
	 * @api
	 * @param \string[] $args
	 * @return string
	 */
	public function projectThemesRelativePath(string ...$args)
	{
		\array_unshift($args, 'App', 'Themes');
		return $this->composePath(...$args);
	}

	/**
	 * Build an absolute path to the plugins themes folder (Plugins/Themes/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function pluginsThemesPath(string ...$args)
	{
		\array_unshift($args, 'Plugins', 'Themes');
		return $this->projectPath(...$args);
	}

	/**
	 * Build a relative path to the plugins themes folder (Plugins/Themes/)
	 *
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function pluginsThemesRelativePath(string ...$args)
	{
		\array_unshift($args, 'Plugins', 'Themes');
		return $this->composePath(...$args);
	}

	/**
	 * @api
	 * @param \string[] $args
	 * @return string
	 */
	public function tmpPath(string ...$args)
	{
		if ($this->tmpBase === false)
		{
			$changeTmp = \getenv('CHANGE_TMP');
			if ($changeTmp !== false)
			{
				$this->tmpBase = $changeTmp;
			}
			else
			{
				$this->tmpBase = $this->projectPath('tmp');
			}
		}
		\array_unshift($args, $this->tmpBase);
		return $this->buildPathFromComponents($args);
	}

	/**
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function cachePath(string ...$args)
	{
		\array_unshift($args, 'cache');
		return \call_user_func_array([$this, 'tmpPath'], $args);
	}

	/**
	 * @api
	 * @param string $path
	 * @return bool
	 */
	public function isAbsolutePath($path)
	{
		if (\is_string($path) && $path !== '')
		{
			if ($path[0] === DIRECTORY_SEPARATOR)
			{
				return true;
			}
			if (\preg_match('/^[a-zA-Z]:/', $path))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * @api
	 * @param \string[] $args
	 * @return string
	 */
	public function composeAbsolutePath(string ...$args)
	{
		$path = $this->composePath(...$args);
		if (!$path)
		{
			return $this->projectBase();
		}

		if ($path[0] !== DIRECTORY_SEPARATOR)
		{
			if (\preg_match('/^[a-zA-Z]:/', $path))
			{
				return $path;
			}
			return $this->projectPath($path);
		}
		return $path;
	}

	/**
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function composePath(...$args)
	{
		$path = '';
		foreach ($args as $pathPartArg)
		{
			$pathPart = (string)$pathPartArg;
			if ($pathPart !== '')
			{
				if (DIRECTORY_SEPARATOR !== '/')
				{
					$pathPart = \str_replace('/', DIRECTORY_SEPARATOR, $pathPart);
				}

				if ($path !== '')
				{
					$pathPart = \trim($pathPart, DIRECTORY_SEPARATOR);
					if ($pathPart !== '')
					{
						$path .= DIRECTORY_SEPARATOR . $pathPart;
					}
				}
				else
				{
					$path .= $pathPart;
				}
			}
		}
		return $path;
	}

	/**
	 * @param string[] $pathComponents
	 * @return string
	 */
	protected function buildPathFromComponents(array $pathComponents)
	{
		if (DIRECTORY_SEPARATOR !== '/' || \strpos($pathComponents[0], DIRECTORY_SEPARATOR) === 0)
		{
			return \implode(DIRECTORY_SEPARATOR, $pathComponents);
		}
		return DIRECTORY_SEPARATOR . \implode(DIRECTORY_SEPARATOR, $pathComponents);
	}

	/**
	 * @return string
	 */
	protected function projectBase()
	{
		return PROJECT_HOME;
	}

	/**
	 * @return string
	 */
	protected function compilationBase()
	{
		return PROJECT_HOME . DIRECTORY_SEPARATOR . 'Compilation';
	}

	/**
	 * @return string
	 */
	protected function changeBase()
	{
		return PROJECT_HOME . DIRECTORY_SEPARATOR . 'Change';
	}

	/**
	 * @return string
	 */
	protected function appBase()
	{
		return PROJECT_HOME . DIRECTORY_SEPARATOR . 'App';
	}
}