<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\AllocineCastingParserTest
 */
class AllocineCastingParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\AllocineCastingParser
	 */
	protected function getNewParser(string $url)
	{
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $this->buildParser(\Project\Library\WebSiteParsers\AllocineCastingParser::class, $url);
	}

	public function testParser_Movie()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/film/fichefilm-53751/casting/');

		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Robert Downey Jr.')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturnCallback(function ($label) {
			if ($label === 'Réalisateurs')
			{
				return $this->getContributorInfos('Réalisateur', 300);
			}
			if ($label === 'Scénaristes')
			{
				return $this->getContributorInfos('Scénariste', 301);
			}
			if ($label === 'Acteur')
			{
				return $this->getContributorInfos('Acteur', 302);
			}
			if ($label === 'D\'après l\'oeuvre de')
			{
				return $this->getContributorInfos('D\'après l\'oeuvre de', 303);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();
		self::assertArrayHasKey('contributions', $parsedData);
		self::assertGreaterThan(30, \count($parsedData['contributions']));

		$contribution0 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=24377.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Jon Favreau',
			// No contributor found.
			'roles' => [
				['model' => 'Project_Library_Contributor', 'id' => 300, 'label' => 'Réalisateur'],
				['model' => 'Project_Library_Contributor', 'id' => 302, 'label' => 'Acteur']
			],
			'roleDetail' => 'le chauffeur de Stark',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution0, $this->findItemByLabel('Jon Favreau', $parsedData['contributions']));

		$contribution6 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=59857.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Stan Lee',
			// Actor and writer.
			'roles' => [
				['model' => 'Project_Library_Contributor', 'id' => 303, 'label' => 'D\'après l\'oeuvre de'],
				['model' => 'Project_Library_Contributor', 'id' => 302, 'label' => 'Acteur']
			],
			'roleDetail' => 'Hugh Heffner',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution6, $this->findItemByLabel('Stan Lee', $parsedData['contributions']));

		$contribution8 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=10976.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Robert Downey Jr.',
			'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 200, 'label' => 'Robert Downey Jr.'],
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 302, 'label' => 'Acteur']],
			'roleDetail' => 'Tony Stark / Iron Man',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution8, $this->findItemByLabel('Robert Downey Jr.', $parsedData['contributions']));
	}

	public function testParser_TVSeries()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/series/ficheserie-11/casting/saison-32/');

		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Sarah Michelle Gellar')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturnCallback(function ($label) {
			if ($label === 'Réalisateurs')
			{
				return $this->getContributorInfos('Réalisateur', 300);
			}
			if ($label === 'Acteur')
			{
				return $this->getContributorInfos('Acteur', 301);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();
		self::assertArrayHasKey('contributions', $parsedData);
		self::assertGreaterThan(30, \count($parsedData['contributions']));

		$contribution0 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=38242.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Sarah Michelle Gellar',
			'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 200, 'label' => 'Sarah Michelle Gellar'],
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Buffy Summers (S7)',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution0, $this->findItemByLabel('Sarah Michelle Gellar', $parsedData['contributions']));

		$contribution1 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=64362.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Alyson Hannigan',
			// No contributor found.
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Willow Rosenberg (S7)',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution1, $this->findItemByLabel('Alyson Hannigan', $parsedData['contributions']));
	}
}