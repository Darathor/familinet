<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\AllocineMovieParserTest
 */
class AllocineMovieParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\AllocineMovieParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\AllocineMovieParser::class, $url);
	}

	public function testParser_MultipleNationalities()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/film/fichefilm_gen_cfilm=17509.html');

		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::exactly(2), 'resolveGenreInfos')->withConsecutive(['Thriller'], ['Action'])
			->will(self::onConsecutiveCalls($this->getGenreInfos('Thriller', 110), null));
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Christian Slater')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturnCallback(function ($label) {
			if ($label === 'Réalisateurs')
			{
				return $this->getContributorInfos('Réalisateur', 300);
			}
			if ($label === 'Acteur')
			{
				return $this->getContributorInfos('Acteur', 301);
			}
			return null;
		});
		$this->expectResolverMethod(self::exactly(5), 'resolveCountryInfos')->withConsecutive(
			['U.S.A.'],
			['Nouvelle-Zélande'],
			['Danemark'],
			['Japon'],
			['France']
		)->will(self::onConsecutiveCalls(
			$this->getCountryInfos('USA', 400),
			null,
			null,
			$this->getCountryInfos('Japon', 401),
			$this->getCountryInfos('France', 402)
		));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'Anglais')->willReturn('en');
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('film')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
			'genres' => [['model' => 'Project_Library_Genre', 'id' => 110, 'label' => 'Thriller']],
			'image' => 'https://fr.web.img6.acsta.net/medias/nmedia/images/affiches/041141.jpg',
			'year' => 1997,
			'description' => '**Synopsis :** Il ne cesse de pleuvoir depuis plusieurs jours sur la petite ville d\'Huntingburg. Le niveau de l\'eau monte et le barrage situé en amont de la ville risque de céder. Les autorités décident d\'évacuer la population. Seuls le shérif, ses adjoints et quelques habitants irréductibles restent sur place. Deux convoyeurs de fonds, Tom et Charlie, viennent chercher l\'argent de la banque pour le mettre au sec. C\'est le moment attendu par Jim et ses complices, bien decidés a s\'emparer des trois millions de dollars qui dorment dans le coffre.

Autres nationalités : Nouvelle-Zélande, Danemark

Autres genres : Action', // Missing genre and nationalities.
			'typology' => 900,
			'attributes' =>
				[
					'type_film' => 'Long-métrage',
					'budget' => '70 000 000 $',
					'mode_couleur' => 'Couleur',
					'visa' => '91265',
					'date_sortie_cinema' => '1998-05-06T00:00:00+00:00',
					'date_sortie_VOD' => '2017-03-28T00:00:00+00:00',
					'date_sortie_BR' => '2009-05-05T00:00:00+00:00',
					'duree_cinema' => '1h 33min',
				],
			'label' => 'Pluie d\'enfer',
			'nationalities' => [
				['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA'],
				['model' => 'Rbs_Geo_Country', 'id' => 401, 'label' => 'Japon'],
				['model' => 'Rbs_Geo_Country', 'id' => 402, 'label' => 'France']
			],
			'aliases' => [['original' => true, 'label' => 'Hard Rain', 'model' => 'Project_Library_WorkAlias']], // No nationality (multiple on work).
			'links' => [[
				'url' => 'https://www.allocine.fr/film/fichefilm_gen_cfilm=17509.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_WorkLink'
			]],
			'originalLanguage' => 'en',
			'model' => 'Project_Library_Work'
		];

		self::assertEquals($expectedWork, $parsedData['work']);

		self::assertGreaterThan(15, \count($parsedData['contributions']));

		$contribution0 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=12630.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Morgan Freeman',
			// No contributor found.
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Jim',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution0, $parsedData['contributions'][0]);

		$contribution1 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=13604.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Christian Slater',
			'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 200, 'label' => 'Christian Slater'],
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Tom',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution1, $parsedData['contributions'][1]);
	}

	public function testParser_MultipleLanguages()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/film/fichefilm_gen_cfilm=123255.html');

		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::exactly(2), 'resolveGenreInfos')->withConsecutive(['Historique'], ['Drame'])
			->will(self::onConsecutiveCalls($this->getGenreInfos('Historique', 110), self::onConsecutiveCalls($this->getGenreInfos('Drame', 111))));
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturn(null);
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturn(null);
		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')->with('U.S.A.')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::exactly(2), 'resolveValueFromCollection')
			->withConsecutive(['Project_Library_Languages', 'Anglais'], ['Project_Library_Languages', 'Cantonais'])
			->will(self::onConsecutiveCalls('en', 'cn'));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('film')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
			'image' => 'https://fr.web.img5.acsta.net/medias/nmedia/18/66/51/21/18943662.jpg',
			'year' => 2008,
			'description' => '**Synopsis :** Les Orphelins de Huang Shi nous entraîne dans la Chine des années 30 alors ravagée par la guerre, et raconte l\'incroyable aventure d\'un jeune journaliste anglais, d\'une infirmière américaine et du chef d\'un groupe de partisans chinois qui vont unir leurs efforts pour sauver 60 orphelins.Ensemble, ils vont parcourir plus d\'un millier de kilomètres à travers les contrées les plus hostiles, des sommets enneigés aux déserts impitoyables, pour atteindre un village où les enfants pourront enfin vivre en sécurité.Surmontant tous les obstacles, leur épopée va les conduire à découvrir le sens absolu du sacrifice, de la responsabilité et du courage.

**Langues origniales :** Cantonais', // Multiple languages: the first is set in the originalLanguage filed and others are added to the description.
			'typology' => 900,
			'attributes' => [
				'type_film' => 'Long-métrage',
				'budget' => '$40 000 000',
				'mode_couleur' => 'Couleur',
				'date_sortie_VOD' => '2021-10-01T00:00:00+00:00',
				'date_sortie_DVD' => '2009-01-06T00:00:00+00:00',
				'date_sortie_cinema' => '2008-06-11T00:00:00+00:00',
				'duree_cinema' => '1h 54min'
			],
			'label' => 'Les Orphelins de Huang Shi',
			'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']],
			'aliases' => [[
				'original' => true,
				'label' => 'The Children of Huang Shi',
				'model' => 'Project_Library_WorkAlias',
				// Only one nationality on work so copied on the original alias.
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']]
			]],
			'links' => [[
				'url' => 'https://www.allocine.fr/film/fichefilm_gen_cfilm=123255.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_WorkLink'
			]],
			'originalLanguage' => 'en',
			'genres' => [
				['model' => 'Project_Library_Genre', 'id' => 110, 'label' => 'Historique'],
				['model' => 'Project_Library_Genre', 'id' => 111, 'label' => 'Drame']
			],
			'model' => 'Project_Library_Work'
		];

		self::assertEquals($expectedWork, $parsedData['work']);
	}

	public function testParser_JSONLDBadEncoding()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/film/fichefilm_gen_cfilm=123255.html');

		$this->expectResolverMethod(self::any(), 'resolveWorkStatusData');
		$this->expectResolverMethod(self::any(), 'resolveGenreInfos');
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos');
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos');
		$this->expectResolverMethod(self::any(), 'resolvePublisherInfos');
		$this->expectResolverMethod(self::any(), 'resolvePublisherCollectionInfos');
		$this->expectResolverMethod(self::any(), 'resolveCountryInfos');
		$this->expectResolverMethod(self::any(), 'resolveValueFromCollection');
		$this->expectResolverMethod(self::any(), 'resolveTypology');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteId');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteInfos');

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));
	}
}