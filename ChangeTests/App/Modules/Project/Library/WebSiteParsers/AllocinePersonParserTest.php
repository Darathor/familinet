<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\AllocinePersonParserTest
 */
class AllocinePersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\AllocinePersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\AllocinePersonParser::class, $url);
	}

	public function testParser_MultipleNationalitiesAndBirthName()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=24989.html');

		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')
			->withConsecutive(['Espagnol'], ['Chilien'])
			->will(self::onConsecutiveCalls($this->getCountryInfos('Espagne', 400), $this->getCountryInfos('Chili', 401)));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1972-03-31T00:00:00+00:00',
				'birthYear' => 1972,
				// Ignore truncated descriptions.
				'typology' => 900,
				'label' => 'Alejandro Amenábar',
				'nationalities' => [
					['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'Espagne'],
					['model' => 'Rbs_Geo_Country', 'id' => 401, 'label' => 'Chili']
				],
				'aliases' => [[
					'original' => true, // Birth name.
					'label' => 'Alejandro Fernando Amenábar Cantos',
					'model' => 'Project_Library_ContributorAlias'
				]],
				'links' => [[
					'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=24989.html',
					'label' => 'Allociné',
					'model' => 'Project_Library_ContributorLink'
				]],
				 'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_MultipleNationalities()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=57115.html');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Américain')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1969-10-17T00:00:00+00:00',
				'birthYear' => 1969,
				// Ignore truncated descriptions.
				'typology' => 900,
				'label' => 'Wood Harris',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']],
				'aliases' => [[
					// Pseudonym.
					'label' => 'Sherwin David Harris',
					'model' => 'Project_Library_ContributorAlias'
				]],
				'links' => [[
					'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=57115.html',
					'label' => 'Allociné',
					'model' => 'Project_Library_ContributorLink'
				]],
				 'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DeathDateAndDescription()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=51054.html');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Américain')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1931-09-12T00:00:00+00:00',
				'birthYear' => 1931,
				'deathDate' => '2011-12-01T00:00:00+00:00',
				'deathYear' => 2011,
				// Not truncated description.
				'description' => 'Il a toujours été associé à son rôle de "Mountain Man" dans Délivrance, malgré une riche carrière comme second rôle sous la direction des plus grands réalisateurs durant les années 70 et 80. Bill McKinney, considéré comme l\'un des plus grands méchants de l\'histoire du cinéma grâce à sa performance dans le film de John Boorman, est décédé à l\'âge de 80 ans des suites d\'un cancer. Outre Délivrance, on retiendra ses apparitions chez John Huston (Juge et Hors-la-loi), Sam Peckinpah (Le Dernier Bagarreur), Michael Cimino (Le Canardeur), Don Siegel (Le Dernier des géants), Ted Kotcheff (Rambo) et surtout aux côtés de Clint Eastwood (Josey Wales hors la loi, L\' Epreuve de force, Doux, Dur et Dingue, Ca va cogner, Bronco Billy, Pink Cadillac). Il était récemment apparu dans la comédie Comment savoir.',
				'typology' => 900,
				'label' => 'Bill McKinney',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']],
				'links' => [[
					'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=51054.html',
					'label' => 'Allociné',
					'model' => 'Project_Library_ContributorLink'
				]],
				 'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DeathYearAndDisambiguationSuffix()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=511572.html');

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteId')->with('www.allocine.fr')->willReturn(100);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthYear' => 1952,
				'typology' => 900,
				'label' => 'John Smith',
				// Ignore "Indéfini" in nationality.
				'links' => [[
					'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=511572.html',
					'label' => 'Allociné',
					'model' => 'Project_Library_ContributorLink'
				]],
				'aliases' => [[
					'label' => 'John Smith (VI)',
					'model' => 'Project_Library_ContributorAlias',
					'forWebsite' => 100
				]],
				 'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}