<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\AllocineSerieParserTest
 */
class AllocineSerieParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\AllocineSerieParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\AllocineSerieParser::class, $url);
	}

	public function testParser_EndedAndMultipleNationalities()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/series/ficheserie_gen_cserie=144.html');

		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::once(), 'resolveGenreInfos')->withConsecutive(['Fantastique'])
			->will(self::onConsecutiveCalls($this->getGenreInfos('Fantastique', 110)));
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Jason Priestley')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturnCallback(function ($label) {
			if ($label === 'Réalisateurs')
			{
				return $this->getContributorInfos('Réalisateur', 300);
			}
			if ($label === 'Acteur')
			{
				return $this->getContributorInfos('Acteur', 301);
			}
			return null;
		});
		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')
			->withConsecutive(['U.S.A.'], ['Canada'])
			->will(self::onConsecutiveCalls($this->getCountryInfos('USA', 400), $this->getCountryInfos('Canada', 401)));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('serietv')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
			'genres' => [['model' => 'Project_Library_Genre', 'id' => 110, 'label' => 'Fantastique']],
			'image' => 'https://fr.web.img6.acsta.net/medias/nmedia/18/35/64/88/18415384.jpg',
			'year' => 2003,
			'endYear' => 2005,
			'description' => '**Synopsis :** Etudiante en médecine, Tru travaille à mi-temps dans une morgue. Parfois, des cadavres lui demandent de les aider. Tru revit alors sa dernière journée, et va tout tenter pour sauver la personne. Mais c\'est sans compter sur Jack qui va lui compliquer la tâche...',
			'typology' => 900,
			'attributes' => [
				'nb_episodes' => 26,
				'nb_saisons' => 2,
				'duree_episode' => '42 min',
			],
			'label' => 'Tru Calling : compte à rebours',
			'nationalities' => [
				['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA'],
				['model' => 'Rbs_Geo_Country', 'id' => 401, 'label' => 'Canada']
			],
			'aliases' => [['original' => true, 'label' => 'Tru Calling', 'model' => 'Project_Library_WorkAlias']],
			'links' => [[
				'url' => 'https://www.allocine.fr/series/ficheserie_gen_cserie=144.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_WorkLink'
			]],
			'model' => 'Project_Library_Work'
		];

		self::assertEquals($expectedWork, $parsedData['work']);

		self::assertGreaterThan(15, \count($parsedData['contributions']));

		$contribution0 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=70654.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Eliza Dushku',
			// No contributor found.
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Tru Davies (S2, S1)',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution0, $parsedData['contributions'][0]);

		$contribution1 = [
			'link' => [
				'url' => 'https://www.allocine.fr/personne/fichepersonne_gen_cpersonne=21081.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Jason Priestley',
			'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 200, 'label' => 'Jason Priestley'],
			'roles' => [['model' => 'Project_Library_Contributor', 'id' => 301, 'label' => 'Acteur']],
			'roleDetail' => 'Jack Harper (S2, S1)',
			'model' => 'Project_Library_Contribution'
		];
		self::assertEquals($contribution1, $parsedData['contributions'][1]);
	}

	public function testParser_Canceled()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/series/ficheserie_gen_cserie=18468.html');

		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('canceled')->willReturn($this->getWorkStatusInfos('Stoppé', 100));
		$this->expectResolverMethod(self::exactly(3), 'resolveGenreInfos')->withConsecutive(
			['Drame'],
			['Science fiction'],
			['Thriller']
		)->will(self::onConsecutiveCalls(
			$this->getGenreInfos('Drame', 110),
			null,
			null
		));
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturn(null);
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->willReturn(null);
		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('U.S.A.')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('serietv')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.allocine.fr')->willReturn('Allociné');
		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Stoppé'],
			'genres' => [['model' => 'Project_Library_Genre', 'id' => 110, 'label' => 'Drame']],
			'image' => 'https://fr.web.img6.acsta.net/pictures/16/08/30/15/41/018526.jpg',
			'year' => 2016,
			'endYear' => 2017,
			'description' => '**Synopsis :** Une jeune femme, inspecteur de police, découvre qu\'elle peut communiquer avec son père, mort 20 ans plus tôt, grâce à un transmetteur de radio. Ce duo de flics père-fille va alors tenter, sur deux temporalités distinctes, de résoudre une affaire vieille de plusieurs décennies. Sans se douter des conséquences que cela pourrait avoir sur le présent, et le cours de leurs vies...Série adaptée du film Fréquence interdite (2000).

Autres genres : Science fiction, Thriller', // Unknown genres.
			'typology' => 900,
			'attributes' => [
				'nb_episodes' => 13,
				'nb_saisons' => 1,
				'duree_episode' => '42 min'
			],
			'label' => 'Frequency',
			'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']],
			'links' => [[
				'url' => 'https://www.allocine.fr/series/ficheserie_gen_cserie=18468.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_WorkLink'
			]],
			'model' => 'Project_Library_Work'
		];

		self::assertEquals($expectedWork, $parsedData['work']);
	}

	public function testParser_JSONLDBadEncoding()
	{
		$parser = $this->getNewParser('https://www.allocine.fr/series/ficheserie_gen_cserie=23840.html');

		$this->expectResolverMethod(self::any(), 'resolveWorkStatusData');
		$this->expectResolverMethod(self::any(), 'resolveGenreInfos');
		$this->expectResolverMethod(self::any(), 'resolveContributorInfos');
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos');
		$this->expectResolverMethod(self::any(), 'resolvePublisherInfos');
		$this->expectResolverMethod(self::any(), 'resolvePublisherCollectionInfos');
		$this->expectResolverMethod(self::any(), 'resolveCountryInfos');
		$this->expectResolverMethod(self::any(), 'resolveValueFromCollection');
		$this->expectResolverMethod(self::any(), 'resolveTypology');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteId');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteInfos');

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));
	}
}