<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
 */
class NormalizerTestBase extends \ChangeTests\Change\TestAssets\TestCase
{
	use \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ResolverTrait;

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->setUpResolver();
	}

	protected function tearDown(): void
	{
		$this->tearDownResolver();
		parent::tearDown();
	}

	/**
	 * @param \Project\Library\WebSiteParsers\Models\AbstractModel $model
	 * @return \Project\Library\WebSiteParsers\ModelNormalizers\AbstractNormalizer
	 */
	protected function buildNormalizer(\Project\Library\WebSiteParsers\Models\AbstractModel $model)
	{
		return $model->getNormalizer($this->resolver);
	}
}