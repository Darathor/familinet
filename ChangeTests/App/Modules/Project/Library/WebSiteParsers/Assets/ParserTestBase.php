<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
 */
class ParserTestBase extends \ChangeTests\Change\TestAssets\TestCase
{
	use \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ResolverTrait;

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$application = $this->getApplication();
		$application->getConfiguration()->addVolatileEntry('Change/Services', [
			'DbProvider' => \ChangeTests\Change\TestAssets\FakeDbProvider::class
		]);

		$this->initServices($this->getApplication());

		$this->setUpResolver();
	}

	protected function tearDown(): void
	{
		$this->applicationServices->getDbProvider()->assertClean();
		$this->tearDownResolver();
		parent::tearDown();
	}

	/**
	 * @param string $className
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\Misc\AbstractParser
	 */
	protected function buildParser(string $className, string $url)
	{
		return new $className($this->resolver, $url);
	}

	/**
	 * @param string $label
	 * @param array $array
	 * @return array|null
	 */
	protected function findItemByLabel(string $label, array $array): ?array
	{
		foreach ($array as $item)
		{
			if (($item['label'] ?? null) === $label)
			{
				return $item;
			}
		}
		return null;
	}
}