<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ResolverTrait
 */
trait ResolverTrait
{
	/**
	 * @var \Project\Library\WebSiteParsers\Misc\Resolver|\PHPUnit\Framework\MockObject\MockObject
	 */
	protected $resolver;

	/**
	 * @var array
	 */
	protected $resolverNeverMethods;

	protected function setUpResolver(): void
	{
		$this->resolver = $this->createMock(\Project\Library\WebSiteParsers\Misc\Resolver::class);

		$this->resolverNeverMethods = [
			'resolveCountryInfos' => true,
			'resolveWorkStatusData' => true,
			'resolveGenreInfos' => true,
			'resolveContributionTypeInfos' => true,
			'resolveContributorInfos' => true,
			'resolvePublisherInfos' => true,
			'resolvePublisherCollectionInfos' => true,
			'resolveTypology' => true,
			'resolveValueFromCollection' => true,
			'resolveWebsiteId' => true,
			'resolveWebsiteLabel' => true,
			'resolveWebsiteInfos' => true
		];
	}

	protected function tearDownResolver(): void
	{
		self::assertEquals([], $this->resolverNeverMethods);
	}

	/**
	 * @param \PHPUnit\Framework\MockObject\Rule\InvocationOrder $invocationRule
	 * @param string $methodName
	 * @return \PHPUnit\Framework\MockObject\Builder\InvocationMocker
	 */
	protected function expectResolverMethod(\PHPUnit\Framework\MockObject\Rule\InvocationOrder $invocationRule, string $methodName)
	{
		self::assertArrayHasKey($methodName, $this->resolverNeverMethods);
		unset($this->resolverNeverMethods[$methodName]);
		return $this->resolver->expects($invocationRule)->method($methodName);
	}

	protected function applyResolverDefault()
	{
		foreach (\array_keys($this->resolverNeverMethods) as $methodName)
		{
			$this->resolver->expects(self::never())->method($methodName);
		}
		$this->resolverNeverMethods = [];
	}

	//region Document infos shortcuts.
	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getCountryInfos(string $label, int $id)
	{
		return ['model' => 'Rbs_Geo_Country', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getContributorInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_Contributor', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getPublisherInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_Publisher', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getPublisherCollectionInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_Collection', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getContributionTypeInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_ContributionType', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getGenreInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_Genre', 'id' => $id, 'label' => $label];
	}

	/**
	 * @param string $label
	 * @param int $id
	 * @return array
	 */
	protected function getWorkStatusInfos(string $label, int $id)
	{
		return ['model' => 'Project_Library_WorkStatus', 'id' => $id, 'label' => $label];
	}
	//endregion
}