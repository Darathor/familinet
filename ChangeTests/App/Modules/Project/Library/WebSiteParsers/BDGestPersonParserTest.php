<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\BDGestPersonParserTest
 */
class BDGestPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\BDGestPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\BDGestPersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.bedetheque.com/auteur-2132-BD-Lee-Stan.html');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('États-Unis')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.bedetheque.com')->willReturn('BD Gest\'');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1922-12-28T00:00:00+00:00',
				'birthYear' => 1922,
				'deathDate' => '2018-11-12T00:00:00+00:00',
				'deathYear' => 2018,
				'label' => 'Stan Lee',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'USA'
				]],
				'aliases' => [[
					'label' => 'Stanley Martin Lieber',
					'model' => 'Project_Library_ContributorAlias',
					'original' => true
				]],
				'model' => 'Project_Library_Contributor',
				'typology' => 100,
				'links' => [[
					'url' => 'https://www.bedetheque.com/auteur-2132-BD-Lee-Stan.html',
					'label' => 'BD Gest\'',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithWebsite()
	{
		$parser = $this->getNewParser('https://www.bedetheque.com/auteur-16339-BD-Bit.html?searchwhere=0&searchsite=');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Espagne')->willReturn($this->getCountryInfos('Espagne', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.bedetheque.com')->willReturn('BD Gest\'');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1975-12-20T00:00:00+00:00',
				'birthYear' => 1975,
				'label' => 'Bit',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'Espagne'
				]],
				'aliases' => [[
					'label' => 'Javier Bergantiño',
					'model' => 'Project_Library_ContributorAlias',
					'original' => true
				]],
				'model' => 'Project_Library_Contributor',
				'typology' => 100,
				'links' => [
					[
						'url' => 'https://www.bedetheque.com/auteur-16339-BD-Bit.html',
						'label' => 'BD Gest\'',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://bit-tintachina.blogspot.com.es/',
						'label' => 'Site officiel',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}