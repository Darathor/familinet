<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\BNFDataPersonParserTest
 */
class BNFDataPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\BNFDataPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\BNFDataPersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://data.bnf.fr/fr/11922661/suzanne_rosset/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('data.bnf.fr')->willReturn('BNF Data');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'birthDate' => '1926-12-29T00:00:00+00:00',
				'birthYear' => 1926,
				'deathDate' => '2007-12-03T00:00:00+00:00',
				'deathYear' => 2007,
				'description' => 'Ancienne élève de l\'École nationale des langues et civilisations orientales et de l\'Institut d\'études politique. - Traductrice',
				'typology' => 100,
				'attributes' => [
					'sex' => 'f'
				],
				'label' => 'Suzanne Rosset',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'France'
				]],
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://data.bnf.fr/fr/11922661/suzanne_rosset/',
					'label' => 'BNF Data',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithOneAlias()
	{
		$parser = $this->getNewParser('https://data.bnf.fr/fr/16612931/marie_favereau_doumenjou/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('data.bnf.fr')->willReturn('BNF Data');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'description' => 'Archéologue. - Maître de conférences en histoire médiévale à l\'Université Paris Nanterre (en 2020)',
				'typology' => 100,
				'attributes' => [
					'sex' => 'f'
				],
				'label' => 'Marie Favereau Doumenjou',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'France',
				]],
				'model' => 'Project_Library_Contributor',
				'aliases' => [[
					'label' => 'Marie Favereau',
					'model' => 'Project_Library_ContributorAlias',
				]],
				'links' => [[
					'url' => 'https://data.bnf.fr/fr/16612931/marie_favereau_doumenjou/',
					'label' => 'BNF Data',
					'model' => 'Project_Library_ContributorLink',
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithOneAliases()
	{
		$parser = $this->getNewParser('https://data.bnf.fr/fr/15093103/hye-ri_baek/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Corée (République)')
			->willReturn($this->getCountryInfos('Corée du sud', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('data.bnf.fr')->willReturn('BNF Data');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'description' => 'Études à l\'ESIT, École supérieure d\'interprètes et de traducteurs, Paris. - Traducteur',
				'typology' => 100,
				'label' => 'Hye-Ri Baek',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'Corée du sud'
				]],
				'model' => 'Project_Library_Contributor',
				'aliases' => [
					[
						'label' => 'Hye-Ri Paik',
						'model' => 'Project_Library_ContributorAlias'
					],
					[
						'label' => 'Baek Hye-Ri',
						'model' => 'Project_Library_ContributorAlias'
					],
					[
						'label' => 'Baek Hye- Ri',
						'model' => 'Project_Library_ContributorAlias'
					],
					[
						'label' => 'Hye-Li Päk',
						'model' => 'Project_Library_ContributorAlias'
					],
					[
						'label' => 'Hye-Ree Bäg',
						'model' => 'Project_Library_ContributorAlias'
					]
				],
				'links' => [[
					'url' => 'https://data.bnf.fr/fr/15093103/hye-ri_baek/',
					'label' => 'BNF Data',
					'model' => 'Project_Library_ContributorLink',
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}