<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\BabelioPersonParserTest
 */
class BabelioPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\BabelioPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\BabelioPersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/Charles-Baudelaire/2184');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 200));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		$expected = [
			'contributor' =>
				[
					'label' => 'Charles Baudelaire',
					'birthDate' => '1821-04-09T00:00:00+00:00',
					'birthYear' => 1821,
					'deathDate' => '1867-08-31T00:00:00+00:00',
					'deathYear' => 1867,
					'description' => 'Charles Pierre Baudelaire est un poète français.

Après la mort de son père, sa mère se remarie avec le chef de bataillon Jacques Aupick ; il ne pourra jamais supporter cet événement qui l\'éloigne de sa mère, vu ses divergences avec son beau-père. Ce dernier l\'envoie dans un voyage vers les Indes qui ne s\'achèvera pas, mais trouvant un exil marquant aux îles Mascareignes.

De retour en France, il aura une liaison avec Jeanne Duval, la mulâtresse, puis connaîtra les paradis artificiels (opium et haschisch...). Dans l\'année 1848, il commence à traduire Poe qu\'il admire beaucoup.

Son recueil des "Fleurs du mal" est poursuivi pour offense à la morale religieuse et outrage à la morale publique et aux bonnes mœurs. Baudelaire se voit reprocher son écriture et le choix de ses sujets. Il n\'est compris que par quelques-uns de ses pairs. Barbey d\'Aurevilly voyait en lui «un Dante d\'une époque déchue». A travers ce recueil, Baudelaire a tenté de tisser et de démontrer les liens entre le mal et la beauté, le bonheur et l\'idéal inaccessible, la violence et la volupté, mais aussi entre le poète et son lecteur.

Il laissera, outre "Les Fleurs du Mal", "Le Spleen de Paris", recueil de poèmes en prose inspiré du "Gaspard de la nuit" d\'Aloysius Bertrand. En dehors de son œuvre poétique, Baudelaire a écrit des essais de critiques littéraires et d\'art ("L\'Art romantique", 1852) et des œuvres comme "Les Paradis artificiels", "Mon cœur mis à nu" ou encore "Fusées".

On compte parmi ses relations proches Édouard Manet, Théophile Gautier ou encore Gérard de Nerval. Il était particulièrement séduit par les peintures d\'Eugène Delacroix, la musique de Richard Wagner et bien que romantique, il fut un grand admirateur de la précision d\'observation de l\'œuvre d\'Honoré de Balzac.',
					'typology' => 100,
					'links' => [[
						'url' => 'https://www.babelio.com/auteur/Charles-Baudelaire/2184',
						'label' => 'Babelio',
						'model' => 'Project_Library_ContributorLink'
					]],
					'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 200, 'label' => 'France']],
					'model' => 'Project_Library_Contributor'
				]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_USAAndYears()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/Joshua-Hale-Fialkov/210544');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('États-Unis')
			->willReturn($this->getCountryInfos('États-Unis', 200)); // Composed name.
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthYear' => 1979,
				'description' => 'Joshua Hale Fialkov (né en 1979 à Sacramento) est un auteur américain de comics. Il officie notoirement dans le genre horrifique et est surtout connu pour son travail surs les séries "I, Vampire", "Echoes" et "Elk\'s Run". Il est également l\'auteur du scénario du film "Infected", sorti en 2008 aux États-Unis.',
				'typology' => 100,
				'label' => 'Joshua Hale Fialkov',
				'birthDate' => '1979-08-19T00:00:00+00:00',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 200, 'label' => 'États-Unis']],
				'links' => [[
					'url' => 'https://www.babelio.com/auteur/Joshua-Hale-Fialkov/210544',
					'label' => 'Babelio',
					'model' => 'Project_Library_ContributorLink'
				]],
				'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_AlternativeDateFormat()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/John-Cassaday/2519');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('États-Unis')
			->willReturn($this->getCountryInfos('États-Unis', 200)); // Composed name.
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'birthYear' => 1971,
				'birthDate' => '1971-12-14T00:00:00+00:00',
				'description' => 'John Cassaday est un dessinateur américain de comics.

Il a d\'abord suivi des cours dans une école de cinéma puis a commencé sa carrière de dessinateur. Au milieu des années 1990, il entame sa carrière à New-York dans l\'industrie des comics, en publiant Dark Angel chez Boneyard Press et Negative Burn chez Caliber Comics. Il illustre également plusieurs couvertures, ainsi qu\'une histoire, de Ghost, chez Dark Horse.

Mais son premier "grand" travail est Ka-Zar: Sibling Rivalry, publié chez Marvel. Il réalise par la suite Desperados, chez Wildstorm (série qui sera, en 2005, adaptée en français chez Delcourt).

En 1998, avec Warren Ellis au scénario, il crée Planetary, publiée chez DC Wildstorm, série actuellement publiée en version française chez Delcourt. Incontestablement sa série la plus célèbre, elle est aujourd\'hui publiée dans le monde entier.

En 2000, il publie Batman : Gotham Knights, dont le scénario est signé Devin Grayson et John Arcadi. Il relance Captain America en 2002.

2003-2004 : Il travaille sur de nombreuses séries, telles que Avengers , Ultimate Six, Black Panter, Serenity , Phantom ou encore Hellboy: Weird Tales , pour ne citer que les plus connues. Avec Joss Whedon, le scénariste de Buffy contre les vampires, Toy Story ou encore Alien 4, il relance Astonishing X-Men.

En 2004, il reçoit le Eisner Award du meilleur dessinateur/encreur pour son travail sur Planetary, Planetary/Batman: Night on Earth (Wildstorm/DC), et Hellboy Weird Tales (Dark Horse). La même année, il entame la série "Je suis légion", aux Humanoïdes associés, avec le scénariste Fabien Nury. C\'est sa première réalisation en Europe.

En 2005, il reçoit de nouveau l\'Eisner Award, toujours dans la catégorie "meilleur dessinateur/encreur", cette fois pour Astonishing X-Men, Planetary, WE3 et Je suis légion.

En 2006, il obtient le Prix Eisner du meilleur dessinateur/encreur pour Astonishing X-Men, Planetary et Je suis légion : Le Faune dansant et la Prix Eisner de la meilleure série pour Astonishing X-Men (avec Joss Whedon).',
				'typology' => 100,
				'label' => 'John Cassaday',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 200,
					'label' => 'États-Unis'
				]],
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.babelio.com/auteur/John-Cassaday/2519',
					'label' => 'Babelio',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_SpacesInNationality()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/Young-Oh-Kim/71319');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Corée du Sud')
			->willReturn($this->getCountryInfos('Corée du Sud', 200)); // Composed name.
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'birthDate' => '1976-04-19T00:00:00+00:00',
				'birthYear' => 1976,
				'description' => 'Kim Young-oh est un manhwaga.

En 1999 : prix du meilleur auteur de l\'année décerné par le magazine coréen Booking

2004-2005 "Banya" avec Jeon Sang-young, 5 volumes

2005-2006 "High school", 12 volumes

2006-2008 "Gui" ("Fantôme") avec Oh Rae Balg Eum, 5 volumes',
				'typology' => 100,
				'label' => 'Young-Oh Kim',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 200,
					'label' => 'Corée du Sud'
				]],
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.babelio.com/auteur/Young-Oh-Kim/71319',
					'label' => 'Babelio',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DatesWithFullMonth()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/Dwayne-McDuffie/208785');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('États-Unis')
			->willReturn($this->getCountryInfos('États-Unis', 200)); // Composed name.
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' =>
				[
					'birthDate' => '1962-02-21T00:00:00+00:00',
					'birthYear' => 1962,
					'deathDate' => '2011-02-21T00:00:00+00:00',
					'deathYear' => 2011,
					'description' => 'Dwayne McDuffie est un scénariste américain qui a travaillé pour les comics et la télévision. Il a écrit Fantastic Four, Captain Marvel et Deathlock pour Marvel, JLA et Firestorm pour DC. Il a participé à la fondation du label Milestone avec des titres comme Blood Syndicate, Shadow Cabinet et Static. Dans le domaiine de l\'animation, il a écrit pour Justice League: Crisis on Two Earths, Justice League: Doom et All Star Superman.

Il a gagné trois Eisner Award pour son travail sur les comics.

Il est mort des complications d\'une intervention chirurgicale d\'urgence sur son cœur.',
					'typology' => 100,
					'label' => 'Dwayne McDuffie',
					'nationalities' => [[
						'model' => 'Rbs_Geo_Country',
						'id' => 200,
						'label' => 'États-Unis'
					]],
					'model' => 'Project_Library_Contributor',
					'links' => [[
						'url' => 'https://www.babelio.com/auteur/Dwayne-McDuffie/208785',
						'label' => 'Babelio',
						'model' => 'Project_Library_ContributorLink'
					]]
				]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DatesWithShortLabel()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/Phil-Noto/119393');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('États-Unis')
			->willReturn($this->getCountryInfos('États-Unis', 200)); // Composed name.
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' =>
				[
					'birthYear' => 1971,
					'description' => 'Phil Noto est artiste peintre et dessinateur de comics.

Diplômé en arts graphiques (illustration) à Ringling School of Art and Design à Sarasota, Floride, il a travaillé pendant dix ans (1991-2001) pour Walt Disney Animation Studios (Lion King, Pocahontas, Mulan...).

site de l\'auteur:

http://www.notoart.com/',
					'typology' => 100,
					'label' => 'Phil Noto',
					'nationalities' => [[
						'model' => 'Rbs_Geo_Country',
						'id' => 200,
						'label' => 'États-Unis'
					]],
					'model' => 'Project_Library_Contributor',
					'links' => [[
						'url' => 'https://www.babelio.com/auteur/Phil-Noto/119393',
						'label' => 'Babelio',
						'model' => 'Project_Library_ContributorLink'
					]
					]
				]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DatesWithoutPlace()
	{
		$parser = $this->getNewParser('https://www.babelio.com/auteur/-Takamichi/400150');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Japon')
			->willReturn($this->getCountryInfos('Japon', 200));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'description' => 'Takamichi est un mangaka.

Il se spécialise très vite dans l\'illustration digitale sur les logiciels Photoshop, Clip Studio Paint et Painter. Il réalise de nombreux travaux en tant qu\'illustrateur ou character designer pour différents Eroge (un jeu vidéo japonais).

En octobre 2002, la maison d\'édition Akane Shinsha confie à Takamichi la couverture du magazine pour adultes COMIC LO à tendance lolicon pour le lancement de son premier numéro.

En 2008, il publie sa première série manga intitulée "Yuru Yuru".

Son site : http://www.amy.hi-ho.ne.jp/takamichi/',
				'typology' => 100,
				'label' => 'Takamichi',
				'birthDate' => '1972-04-22T00:00:00+00:00',
				'birthYear' => 1972,
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 200,
					'label' => 'Japon'
				]],
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.babelio.com/auteur/-Takamichi/400150',
					'label' => 'Babelio',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}