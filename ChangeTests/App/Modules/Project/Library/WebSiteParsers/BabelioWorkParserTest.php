<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\BabelioWorkParserTest
 */
class BabelioWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\BabelioWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\BabelioWorkParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.babelio.com/livres/Daudet-Ma-ville-interdite/1306332');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur'],
			['Auteur']
		)->will(self::onConsecutiveCalls(
			null, // Role not found for first contributor.
			$this->getContributionTypeInfos('Auteur', 400)
		));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributorInfos')->withConsecutive(
			['Yvonne Daudet', 'https://www.babelio.com/auteur/Yvonne-Daudet/307267'],
			['Charles Daney', 'https://www.babelio.com/auteur/Charles-Daney/17033']
		)->will(self::onConsecutiveCalls(
			$this->getContributorInfos('Yvonne Daudet', 500),
			null // Second contributor not found.
		));
		$this->expectResolverMethod(self::once(), 'resolvePublisherInfos')->with('Nouvelles Editions Loubatières')
			->willReturn($this->getPublisherInfos('Nouvelles Editions Loubatières', 200));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('livre')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'label' => 'Ma ville interdite',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://www.babelio.com/livres/Daudet-Ma-ville-interdite/1306332',
					'label' => 'Babelio',
					'model' => 'Project_Library_WorkLink'
				]]
			],
			'publication' => [
				'year' => 2010,
				'typology' => 100,
				'attributes' => [
					'ean' => '9782862666143',
					'nb_pages' => '128',
					'date_publication' => '2010-04-06T00:00:00+00:00'
				],
				'model' => 'Project_Library_Publication',
				'publishers' => [[
					'model' => 'Project_Library_Publisher',
					'id' => 200,
					'label' => 'Nouvelles Editions Loubatières'
				]]
			],
			'contributions' =>
				[
					[
						'link' => [
							'url' => 'https://www.babelio.com/auteur/Yvonne-Daudet/307267',
							'label' => 'Babelio',
							'model' => 'Project_Library_ContributorLink'
						],
						'label' => 'Yvonne Daudet',
						'model' => 'Project_Library_Contribution',
						'contributor' => [
							'model' => 'Project_Library_Contributor',
							'id' => 500,
							'label' => 'Yvonne Daudet'
						],
					],
					[
						'link' => [
							'url' => 'https://www.babelio.com/auteur/Charles-Daney/17033',
							'label' => 'Babelio',
							'model' => 'Project_Library_ContributorLink'
						],
						'label' => 'Charles Daney',
						'model' => 'Project_Library_Contribution',
						'roles' => [[
							'model' => 'Project_Library_ContributionType',
							'id' => 400,
							'label' => 'Auteur'
						]]
					]
				]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithTranslator()
	{
		$parser = $this->getNewParser('https://www.babelio.com/livres/Martin-Le-Trone-de-fer-tome-11--Les-sables-de-Dorne/4016');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur'],
			['Traducteur']
		)->will(self::onConsecutiveCalls(
			$this->getContributionTypeInfos('Auteur', 400),
			$this->getContributionTypeInfos('Traducteur', 401)
		));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributorInfos')->withConsecutive(
			['George R.R. Martin', 'https://www.babelio.com/auteur/George-RR-Martin/55305'],
			['Jean Sola', 'https://www.babelio.com/auteur/Jean-Sola/61194']
		)->will(self::onConsecutiveCalls(
			$this->getContributorInfos('George R.R. Martin', 500),
			null // Second contributor not found.
		));
		$this->expectResolverMethod(self::once(), 'resolvePublisherInfos')->with('J\'ai Lu')
			->willReturn($this->getPublisherInfos('J\'ai Lu', 200));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('livre')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.babelio.com')->willReturn('Babelio');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'work' => [
				'label' => 'Le Trône de fer, tome 11 : Les sables de Dorne',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://www.babelio.com/livres/Martin-Le-Trone-de-fer-tome-11--Les-sables-de-Dorne/4016',
					'label' => 'Babelio',
					'model' => 'Project_Library_WorkLink'
				]]
			],
			'publication' => [
				'year' => 2007,
				'typology' => 100,
				'attributes' => [
					'ean' => '9782290002971',
					'nb_pages' => '411',
					'date_publication' => '2007-11-09T00:00:00+00:00'
				],
				'model' => 'Project_Library_Publication',
				'publishers' => [[
					'model' => 'Project_Library_Publisher',
					'id' => 200,
					'label' => 'J\'ai Lu'
				]]
			],
			'contributions' => [
				[
					'link' => [
						'url' => 'https://www.babelio.com/auteur/George-RR-Martin/55305',
						'label' => 'Babelio',
						'model' => 'Project_Library_ContributorLink',
					],
					'label' => 'George R.R. Martin',
					'model' => 'Project_Library_Contribution',
					'roles' => [[
						'model' => 'Project_Library_ContributionType',
						'id' => 400,
						'label' => 'Auteur'
					]],
					'contributor' => [
						'model' => 'Project_Library_Contributor',
						'id' => 500,
						'label' => 'George R.R. Martin'
					]
				],
				[
					'link' => [
						'url' => 'https://www.babelio.com/auteur/Jean-Sola/61194',
						'label' => 'Babelio',
						'model' => 'Project_Library_ContributorLink',
					],
					'label' => 'Jean Sola',
					'model' => 'Project_Library_Contribution',
					'roles' => [[
						'model' => 'Project_Library_ContributionType',
						'id' => 401,
						'label' => 'Traducteur'
					]]
				],
			],
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}