<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ComicVinePersonParserTest
 */
class ComicVinePersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\ComicVinePersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\ComicVinePersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://comicvine.gamespot.com/stan-lee/4040-40467/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('United States')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('comicvine.gamespot.com')->willReturn('ComicVine');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1922-12-28T00:00:00+00:00',
				'birthYear' => 1922,
				'deathDate' => '2018-11-12T00:00:00+00:00',
				'deathYear' => 2018,
				'typology' => 100,
				'attributes' => [
					'sex' => 'h',
				],
				'label' => 'Stan Lee',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'USA',
				]],
				'model' => 'Project_Library_Contributor',
				'aliases' => [[
					'label' => 'Stanley Martin Lieber',
					'model' => 'Project_Library_ContributorAlias',
				]],
				'links' => [
					[
						'url' => 'https://comicvine.gamespot.com/stan-lee/4040-40467/',
						'label' => 'ComicVine',
						'model' => 'Project_Library_ContributorLink',
					],
					[
						'url' => 'http://www.powentertainment.com/enter.html',
						'label' => 'Site web',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithMultipleAliases()
	{
		$parser = $this->getNewParser('https://comicvine.gamespot.com/bong-dazo/4040-50944/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('Philippines')->willReturn($this->getCountryInfos('Philippines', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('comicvine.gamespot.com')->willReturn('ComicVine');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1962-06-16T00:00:00+00:00',
				'birthYear' => 1962,
				'deathDate' => '2018-06-29T00:00:00+00:00',
				'deathYear' => 2018,
				'typology' => 100,
				'attributes' => [
					'sex' => 'h'
				],
				'label' => 'Bong Dazo',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'Philippines'
				]
				],
				'model' => 'Project_Library_Contributor',
				'aliases' => [
					[
						'label' => 'Bong Ty Dazo',
						'model' => 'Project_Library_ContributorAlias',
					],
					[
						'label' => 'Angelo Ty Dazo',
						'model' => 'Project_Library_ContributorAlias',
					]
				],
				'links' => [
					[
						'url' => 'https://comicvine.gamespot.com/bong-dazo/4040-50944/',
						'label' => 'ComicVine',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://www.glasshousegraphics.com/ghgartistwebsite/bongdazo/',
						'label' => 'Site web',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_WithMultipleNationality()
	{
		$parser = $this->getNewParser('https://comicvine.gamespot.com/peter-milligan/4040-41484/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')
			->withConsecutive(['England'], ['United Kingdom'])
			->willReturn($this->getCountryInfos('Royaume-Uni', 400), $this->getCountryInfos('Royaume-Uni', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('comicvine.gamespot.com')->willReturn('ComicVine');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'typology' => 100,
				'attributes' => [
					'sex' => 'h'
				],
				'label' => 'Peter Milligan',
				'nationalities' => [
					[
						'model' => 'Rbs_Geo_Country',
						'id' => 400,
						'label' => 'Royaume-Uni'
					],
					[
						'model' => 'Rbs_Geo_Country',
						'id' => 400,
						'label' => 'Royaume-Uni'
					]
				],
				'model' => 'Project_Library_Contributor',
				'links' => [
					[
						'url' => 'https://comicvine.gamespot.com/peter-milligan/4040-41484/',
						'label' => 'ComicVine',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://www.petermilligan.co.uk/',
						'label' => 'Site web',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}