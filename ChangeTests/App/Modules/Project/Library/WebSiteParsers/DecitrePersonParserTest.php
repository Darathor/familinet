<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\DecitrePersonParserTest
 */
class DecitrePersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\DecitrePersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\DecitrePersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.decitre.fr/auteur/308433/Guillaume+Musso');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.decitre.fr')->willReturn('Decitre');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'links' => [
					[
						'label' => 'Decitre',
						'url' => 'https://www.decitre.fr/auteur/308433/Guillaume+Musso',
						'model' => 'Project_Library_ContributorLink'
					]
				],
				'label' => 'Guillaume Musso',
				'description' => 'Guillaume Musso fait partie de ces auteurs prolifiques dont les romans laissent une trace indélébile dans notre esprit. Ancien professeur, il publie son premier récit en 2001. Puis, après un grave accident de
voiture, il écrit la superbe histoire d\'un enfant revenu d\'entre les morts. Avec ce succès phénoménal qui le propulse sur le devant de la scène, il est proclamé roi du suspense par les critiques et réussit l\'étonnant exploit de voir adapter au cinéma la plupart de ses romans. Par ce biais, il accomplit la prouesse de donner goût à la lecture aux amoureux exclusifs du septième art. A travers ses best-sellers L\'appel de l\'ange et Demain, plongez dans un univers intrigant mêlant habilement mystère, amour, humour et surnaturel. Aimant jouer avec l\'espace-temps, l\'auteur nous offre des récits à suspense composés d\'une galerie de personnages à l\'humanité touchante. Si vous cherchez des livres émouvants et originaux, nous vous recommandons chaudement Je vais bien, ne t\'en fais pas d\'Olivier Adam, Si c\'était à refaire de Marc Levy ou Journal d\'un corps de Daniel Pennac. Laissez-vous entraîner par une lecture fluide et agréable et apprêtez-vous à découvrir des histoires insolites et inoubliables.',
				'typology' => 100,
				'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}