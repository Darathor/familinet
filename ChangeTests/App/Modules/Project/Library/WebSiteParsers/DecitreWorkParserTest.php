<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\DecitreWorkParserTest
 */
class DecitreWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\DecitreWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\DecitreWorkParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.decitre.fr/livres/infamous-iron-man-tome-1-redemption-9782809474923.html');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::exactly(4), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur'],
			['Auteur'],
			['Coloriste'],
			['Traducteur']
		)->will(self::onConsecutiveCalls(
			null, // Role not found for first contributor.
			$this->getContributionTypeInfos('Auteur', 400),
			$this->getContributionTypeInfos('Coloriste', 401),
			$this->getContributionTypeInfos('Traducteur', 402)
		));
		$this->expectResolverMethod(self::exactly(4), 'resolveContributorInfos')->withConsecutive(
			['Brian Michael Bendis', 'https://www.decitre.fr/auteur/978561/Brian+Michael+Bendis'],
			['Alexandre Maleev', 'https://www.decitre.fr/auteur/1264558/Alexandre+Maleev'],
			['Matt Hollingsworth', 'https://www.decitre.fr/auteur/1590115/Matt+Hollingsworth'],
			['Jérémy Manesse', 'https://www.decitre.fr/auteur/1090753/Jeremy+Manesse']
		)->will(self::onConsecutiveCalls(
			$this->getContributorInfos('Brian Michael Bendis', 500),
			null, // Second contributor not found.
			$this->getContributorInfos('Matt Hollingsworth', 502),
			$this->getContributorInfos('Jérémy Manesse', 503)
		));
		$this->expectResolverMethod(self::once(), 'resolvePublisherInfos')->with('Panini Comics')
			->willReturn($this->getPublisherInfos('Panini Comics', 200));
		$this->expectResolverMethod(self::exactly(1), 'resolvePublisherCollectionInfos')->with('Marvel now!', 200)
			->willReturn($this->getPublisherCollectionInfos('Marvel now!', 300));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('serie_livres')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.decitre.fr')->willReturn('Decitre');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'label' => 'Infamous Iron Man',
				'description' => '**Résumé :** Avant Secret Wars, Victor Von Fatalis était le super-criminel le plus redouté de l\'univers Marvel. Aujourd\'hui, c\'est un homme très différent, déterminé à racheter tous les crimes qu\'il a commis par le passé. Pour s\'amender, il a l\'intention de devenir un super-héros et de combattre ses anciens alliés. Mais est-ce un revirement sincère ou l\'un de ses nouveaux plans ? Brian M. Bendis (Guardians of the Galaxy, X-Men) et Alex Maleev (Daredevil, Spider-Woman) signent un chapitre inattendu de l\'histoire du célèbre Avenger en armure.',
				'links' => [
					[
						'label' => 'Decitre',
						'url' => 'https://www.decitre.fr/livres/infamous-iron-man-tome-1-redemption-9782809474923.html',
						'model' => 'Project_Library_WorkLink',
					]
				],
				'model' => 'Project_Library_Work'
			],
			'publication' => [
				'typology' => 100,
				'attributes' => [
					'date_publication' => '2018-11-07T00:00:00+00:00',
					'isbn' => '978-2-8094-7492-3',
					'ean' => '9782809474923',
					'nb_pages' => 136,
					'poids' => '0.6 Kg',
					'dimensions' => '17,8 cm × 26,7 cm × 1,5 cm',
					'format' => 'Album',
				],
				'publishers' => [['model' => 'Project_Library_Publisher', 'id' => 200, 'label' => 'Panini Comics']],
				'collection' => ['model' => 'Project_Library_Collection', 'id' => 300, 'label' => 'Marvel now!'],
				'model' => 'Project_Library_Publication'
			],
			'contributions' => [
				[
					'label' => 'Brian Michael Bendis',
					'link' => [
						'label' => 'Decitre',
						'url' => 'https://www.decitre.fr/auteur/978561/Brian+Michael+Bendis',
						'model' => 'Project_Library_ContributorLink'
					],
					// Not role found so no role data.
					'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 500, 'label' => 'Brian Michael Bendis'],
					'model' => 'Project_Library_Contribution'
				],
				[
					'label' => 'Alexandre Maleev',
					'link' => [
						'label' => 'Decitre',
						'url' => 'https://www.decitre.fr/auteur/1264558/Alexandre+Maleev',
						'model' => 'Project_Library_ContributorLink'
					],
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 400, 'label' => 'Auteur']],
					// Not found so no contributor data.
					'model' => 'Project_Library_Contribution'
				],
				[
					'label' => 'Matt Hollingsworth',
					'link' => [
						'label' => 'Decitre',
						'url' => 'https://www.decitre.fr/auteur/1590115/Matt+Hollingsworth',
						'model' => 'Project_Library_ContributorLink'
					],
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 401, 'label' => 'Coloriste']],
					'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 502, 'label' => 'Matt Hollingsworth'],
					'model' => 'Project_Library_Contribution'
				],
				[
					'label' => 'Jérémy Manesse',
					'link' => [
						'url' => 'https://www.decitre.fr/auteur/1090753/Jeremy+Manesse',
						'label' => 'Decitre',
						'model' => 'Project_Library_ContributorLink'
					],
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 402, 'label' => 'Traducteur']],
					'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 503, 'label' => 'Jérémy Manesse'],
					'model' => 'Project_Library_Contribution'
				],
			],
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}