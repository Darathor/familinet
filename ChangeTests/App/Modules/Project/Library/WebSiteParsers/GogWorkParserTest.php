<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\DecitreWorkParserTest
 */
class GogWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\GogWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\GogWorkParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.gog.com/fr/game/baldurs_gate_enhanced_edition');

		$typology1 = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 111);
		$typology2 = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 112);

		$this->expectResolverMethod(self::exactly(2), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['jeu_video'], ['support_logiciel'])
			->will(self::onConsecutiveCalls($typology1, $typology2));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.gog.com')->willReturn('GOG.com');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'image' => 'https://images.gog-statics.com/855573122ad636464037105f92145e943e43704875076c7f3648116fda5f18f3.jpg',
				'year' => 2014,
				'endYear' => 2014,
				'typology' => 111,
				'label' => 'Baldur\'s Gate: Enhanced Edition',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://www.gog.com/fr/game/baldurs_gate_enhanced_edition',
					'label' => 'GOG.com',
					'model' => 'Project_Library_WorkLink'
				]]
			],
			'publication' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'oneShot' => true,
				'year' => 2014,
				'endYear' => 2014,
				'typology' => 112,
				'attributes' => ['type_support_logiciel' => 'Numérique'],
				'model' => 'Project_Library_Publication'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}