<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ImdbPersonParserTest
 */
class ImdbPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\ImdbPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\ImdbPersonParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.imdb.com/name/nm0571853/?ref_=nv_sr_srsg_0');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1931-09-12T00:00:00+00:00',
				'birthYear' => 1931,
				'deathDate' => '2011-12-01T00:00:00+00:00',
				'deathYear' => 2011,
				'image' => 'https://m.media-amazon.com/images/M/MV5BOTMwMjkxODc1OV5BMl5BanBnXkFtZTgwMjQwMDIzNjM@._V1_.jpg',
				'typology' => 100,
				'label' => 'Bill McKinney',
				'links' => [
					[
						'url' => 'https://www.imdb.com/name/nm0571853/', // The query string is removed.
						'label' => 'IMDB',
						'model' => 'Project_Library_ContributorLink'
					]
				],
				'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DisambiguationSuffix()
	{
		$parser = $this->getNewParser('https://www.imdb.com/name/nm4449702/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteId')->with('www.imdb.com')->willReturn(110);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'image' => 'https://m.media-amazon.com/images/M/MV5BZjMyMDE2NWEtZTNlNS00MDllLWJhN2UtNmRlOGI4MzhkNjI5L2ltYWdlXkEyXkFqcGdeQXVyNjMyNjA0NA@@._V1_.jpg',
				'typology' => 100,
				'label' => 'Michael Oaks',
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.imdb.com/name/nm4449702/',
					'label' => 'IMDB',
					'model' => 'Project_Library_ContributorLink'
				]],
				'aliases' => [[
					'forWebsite' => 110,
					'label' => 'Michael Oaks (II)',
					'model' => 'Project_Library_ContributorAlias'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_IgnoredDisambiguationSuffix()
	{
		$parser = $this->getNewParser('https://www.imdb.com/name/nm0557219/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthDate' => '1952-09-01T00:00:00+00:00',
				'birthYear' => 1952,
				'deathDate' => '2016-10-20T00:00:00+00:00',
				'deathYear' => 2016,
				'image' => 'https://m.media-amazon.com/images/M/MV5BNDIyNjMwNTAxNl5BMl5BanBnXkFtZTgwNzI1Nzc2NTE@._V1_.jpg',
				'typology' => 100,
				'label' => 'Michael Massee',
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.imdb.com/name/nm0557219/',
					'label' => 'IMDB',
					'model' => 'Project_Library_ContributorLink'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_DeathYear()
	{
		$parser = $this->getNewParser('https://www.imdb.com/name/nm0110778/?ref_=nv_sr_srsg_0');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('person')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'birthYear' => 1964,
				'typology' => 100,
				'label' => 'Kevin Brodbin',
				'model' => 'Project_Library_Contributor',
				'links' => [[
					'url' => 'https://www.imdb.com/name/nm0110778/',
					'label' => 'IMDB',
					'model' => 'Project_Library_ContributorLink',
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}