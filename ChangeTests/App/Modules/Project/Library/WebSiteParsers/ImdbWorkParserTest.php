<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ImdbWorkParserTest
 */
class ImdbWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\ImdbWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\ImdbWorkParser::class, $url);
	}

	public function testParser_Series()
	{
		$parser = $this->getNewParser('https://www.imdb.com/title/tt0118276/?ref_=nv_sr_srsg_0');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::any(), 'resolveTypology')->with('serietv')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveCountryInfos')->with('United States')->willReturn($this->getCountryInfos('United States', 400));
		$this->expectResolverMethod(self::exactly(4), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->expectResolverMethod(self::exactly(1), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 500));
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->with('Acteur')->willReturn($this->getContributionTypeInfos('Acteur',
			600));

		$this->expectResolverMethod(self::any(), 'resolveGenreInfos')->willReturnCallback(function ($label) {
			if ($label === 'Action')
			{
				return $this->getGenreInfos('Action', 300);
			}
			return null;
		});

		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Sarah Michelle Gellar')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});

		$this->expectResolverMethod(self::exactly(1), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'English')
			->willReturn('en');

		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => [
				'model' => 'Project_Library_WorkStatus',
				'id' => 500,
				'label' => 'Terminé'
			],
			'genres' => [[
				'model' => 'Project_Library_Genre',
				'id' => 300,
				'label' => 'Action'
			]],
			'image' => 'https://m.media-amazon.com/images/M/MV5BY2MwOGIyZGYtNzgxZC00N2Q5LTllYjItM2U4MTkwMDBjYzUyXkEyXkFqcGdeQXVyNzA5NjUyNjM@._V1_.jpg',
			'year' => 1997,
			'endYear' => 2003,
			'description' => 'Autres genres : Drama, Fantasy',
			'typology' => 100,
			'attributes' => [
				'duree_episode' => '44min'
			],
			'label' => 'Buffy contre les vampires',
			'model' => 'Project_Library_Work',
			'aliases' => [[
				'original' => true,
				'label' => 'Buffy the Vampire Slayer',
				'model' => 'Project_Library_WorkAlias',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 400,
					'label' => 'United States'
				]]
			]],
			'links' => [[
				'url' => 'https://www.imdb.com/title/tt0118276/',
				'label' => 'IMDB',
				'model' => 'Project_Library_WorkLink'
			]],
			'nationalities' => [[
				'model' => 'Rbs_Geo_Country',
				'id' => 400,
				'label' => 'United States'
			]],
			'originalLanguage' => 'en'
		];

		self::assertEquals($expectedWork, $parsedData['work']);

		// Contributors

		$foundContributor = [
			'link' =>
				[
					'url' => 'https://www.imdb.com/name/nm0001264/',
					'label' => 'IMDB',
					'model' => 'Project_Library_ContributorLink',
				],
			'label' => 'Sarah Michelle Gellar',
			'model' => 'Project_Library_Contribution',
			'roles' => [[
				'model' => 'Project_Library_ContributionType',
				'id' => 600,
				'label' => 'Acteur',
			]],
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'id' => 200,
				'label' => 'Sarah Michelle Gellar',
			]
		];

		$notFoundContribution = [
			'link' => [
				'url' => 'https://www.imdb.com/name/nm0004989/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink',
			],
			'label' => 'Alyson Hannigan',
			'model' => 'Project_Library_Contribution',
			'roles' => [[
				'model' => 'Project_Library_ContributionType',
				'id' => 600,
				'label' => 'Acteur',
			]]
		];

		self::assertContains($foundContributor, $parsedData['contributions']);
		self::assertContains($notFoundContribution, $parsedData['contributions']);
	}

	public function testParser_Series_WithCountryAndLanguage()
	{
		$parser = $this->getNewParser('https://www.imdb.com/title/tt0384766/?ref_=nv_sr_srsg_0');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::any(), 'resolveTypology')->with('serietv')->willReturn($typology);
		$this->expectResolverMethod(self::any(), 'resolveCountryInfos')->willReturnCallback(function ($label) {
			if ($label === 'United States')
			{
				return $this->getCountryInfos('United States', 400);
			}
			if ($label === 'United Kingdom')
			{
				return $this->getCountryInfos('United Kingdom', 401);
			}
			return null;
		});
		$this->expectResolverMethod(self::exactly(4), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->expectResolverMethod(self::exactly(1), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 500));
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->with('Acteur')->willReturn($this->getContributionTypeInfos('Acteur',
			600));

		$this->expectResolverMethod(self::any(), 'resolveGenreInfos')->willReturnCallback(function ($label) {
			if ($label === 'Action')
			{
				return $this->getGenreInfos('Action', 300);
			}
			return null;
		});

		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Ray Stevenson')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});

		$this->expectResolverMethod(self::exactly(1), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'English')
			->willReturn('en');

		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => [
				'model' => 'Project_Library_WorkStatus',
				'id' => 500,
				'label' => 'Terminé',
			],
			'genres' => [[
				'model' => 'Project_Library_Genre',
				'id' => 300,
				'label' => 'Action',
			]],
			'image' => 'https://m.media-amazon.com/images/M/MV5BYTM4MmU1NWYtNzJjYy00YWFhLThjOGEtZmMxOGI1NzE0NGNiXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_.jpg',
			'year' => 2005,
			'endYear' => 2007,
			'description' => 'Autres genres : Drama, Romance',
			'typology' => 100,
			'attributes' => [
				'duree_episode' => '52min',
			],
			'label' => 'Rome',
			'nationalities' => [
				['model' => 'Rbs_Geo_Country', 'id' => 401, 'label' => 'United Kingdom',],
				['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'United States',]
			],
			'model' => 'Project_Library_Work',
			'links' => [[
				'url' => 'https://www.imdb.com/title/tt0384766/',
				'label' => 'IMDB',
				'model' => 'Project_Library_WorkLink',
			]],
			'originalLanguage' => 'en',
		];

		self::assertEquals($expectedWork, $parsedData['work']);

		// Contributors

		$foundContributor = [
			'link' => [
				'url' => 'https://www.imdb.com/name/nm0829032/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink',
			],
			'label' => 'Ray Stevenson',
			'model' => 'Project_Library_Contribution',
			'roles' => [
				['model' => 'Project_Library_ContributionType', 'id' => 600, 'label' => 'Acteur']
			],
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'id' => 200,
				'label' => 'Ray Stevenson'
			]
		];

		$notFoundContribution = [
			'link' => [
				'url' => 'https://www.imdb.com/name/nm0571727/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink'
			],
			'label' => 'Kevin McKidd',
			'model' => 'Project_Library_Contribution',
			'roles' => [
				['model' => 'Project_Library_ContributionType', 'id' => 600, 'label' => 'Acteur']
			]
		];

		self::assertContains($foundContributor, $parsedData['contributions']);
		self::assertContains($notFoundContribution, $parsedData['contributions']);
	}

	public function testParser_Movie()
	{
		$parser = $this->getNewParser('https://www.imdb.com/title/tt0889588/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::any(), 'resolveTypology')->with('film')->willReturn($typology);
		$this->expectResolverMethod(self::exactly(4), 'resolveWebsiteLabel')->with('www.imdb.com')->willReturn('IMDB');
		$this->expectResolverMethod(self::exactly(1), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 500));
		$this->expectResolverMethod(self::any(), 'resolveContributionTypeInfos')->with('Acteur')->willReturn($this->getContributionTypeInfos('Acteur',
			600));

		$this->expectResolverMethod(self::any(), 'resolveCountryInfos')->willReturnCallback(function ($label) {
			if ($label === 'Australia')
			{
				return $this->getCountryInfos($label, 300);
			}
			return null;
		});

		$this->expectResolverMethod(self::any(), 'resolveGenreInfos')->willReturnCallback(function ($label) {
			if ($label === 'War')
			{
				return $this->getGenreInfos('Guerre', 300);
			}
			return null;
		});

		$this->expectResolverMethod(self::any(), 'resolveContributorInfos')->willReturnCallback(function ($label) {
			if ($label === 'Jonathan Rhys Meyers')
			{
				return $this->getContributorInfos($label, 200);
			}
			return null;
		});

		$this->expectResolverMethod(self::exactly(3), 'resolveValueFromCollection')
			->withConsecutive(['Project_Library_Languages', 'English'], ['Project_Library_Languages', 'Japanese'], ['Project_Library_Languages', 'Mandarin'])
			->will(self::onConsecutiveCalls('en', 'jp', 'cn'));

		$this->applyResolverDefault();

		// -- Tests.

		$parsedData = $parser->parseDataAndNormalize();

		self::assertEquals(['work', 'contributions'], \array_keys($parsedData));

		$expectedWork = [
			'status' => [
				'model' => 'Project_Library_WorkStatus',
				'id' => 500,
				'label' => 'Terminé'
			],
			'genres' => [[
				'model' => 'Project_Library_Genre',
				'id' => 300,
				'label' => 'Guerre'
			]],
			'image' => 'https://m.media-amazon.com/images/M/MV5BMTM2MTM3MjM4NV5BMl5BanBnXkFtZTcwNDI1MzA3MQ@@._V1_.jpg',
			'year' => 2008,
			'description' => "Autres nationalités : China, Germany, United States\n\n**Langues origniales :** Japanese, Mandarin\n\nAutres genres : Drama",
			'typology' => 100,
			'attributes' => [
				'duree_cinema' => '2h 5min'
			],
			'label' => 'Les orphelins de Huang Shi',
			'model' => 'Project_Library_Work',
			'aliases' => [[
				'original' => true,
				'label' => 'The Children of Huang Shi',
				'model' => 'Project_Library_WorkAlias'
			]],
			'links' => [[
				'url' => 'https://www.imdb.com/title/tt0889588/',
				'label' => 'IMDB',
				'model' => 'Project_Library_WorkLink'
			]],
			'originalLanguage' => 'en',
			'nationalities' => [[
				'model' => 'Rbs_Geo_Country',
				'id' => 300,
				'label' => 'Australia'
			]],
		];

		self::assertEquals($expectedWork, $parsedData['work']);

		// Contributors

		$foundContributor = [
			'link' => [
				'url' => 'https://www.imdb.com/name/nm0001667/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink',
			],
			'label' => 'Jonathan Rhys Meyers',
			'model' => 'Project_Library_Contribution',
			'roles' => [
				['model' => 'Project_Library_ContributionType', 'id' => 600, 'label' => 'Acteur']
			],
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'id' => 200,
				'label' => 'Jonathan Rhys Meyers',
			]
		];

		$notFoundContribution = [
			'link' => [
				'url' => 'https://www.imdb.com/name/nm0593664/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink',
			],
			'label' => 'Radha Mitchell',
			'model' => 'Project_Library_Contribution',
			'roles' => [
				['model' => 'Project_Library_ContributionType', 'id' => 600, 'label' => 'Acteur']
			]
		];

		self::assertContains($foundContributor, $parsedData['contributions']);
		self::assertContains($notFoundContribution, $parsedData['contributions']);
	}
}