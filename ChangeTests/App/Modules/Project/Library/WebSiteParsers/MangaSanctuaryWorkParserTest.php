<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\MangaSanctuaryWorkParserTest
 */
class MangaSanctuaryWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\MangaSanctuaryWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\MangaSanctuaryWorkParser::class, $url);
	}

	public function testParser_Manga()
	{
		$parser = $this->getNewParser('https://www.manga-sanctuary.com/bdd/manga/6679-bakuman/');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')->withConsecutive(['Japon'], ['JP'])
			->will(self::onConsecutiveCalls($this->getCountryInfos('Japon', 200), $this->getCountryInfos('Japon', 200)));
		$this->expectResolverMethod(self::exactly(4), 'resolveGenreInfos')->withConsecutive(
			['Shonen'],
			['Comédie'],
			['Tranche de vie'],
			['Social']
		)->will(self::onConsecutiveCalls(
			$this->getGenreInfos('Shonen', 300),
			$this->getGenreInfos('Comédie', 301),
			$this->getGenreInfos('Tranche de vie', 302),
			null,
			null,
			null,
			$this->getGenreInfos('Bande-dessinée', 303),
			null,
			null
		));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')->withConsecutive(['Dessinateur'], ['Scénariste'])
			->will(self::onConsecutiveCalls(null/* First role not found. */, $this->getContributionTypeInfos('Scénariste', 401)));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributorInfos')->withConsecutive(
			['Takeshi Obata', 'https://www.manga-sanctuary.com/bdd/personnalites/139-obata-takeshi.html'],
			['Tsugumi Ohba', 'https://www.manga-sanctuary.com/bdd/personnalites/1389-ohba-tsugumi.html']
		)->will(self::onConsecutiveCalls(
			$this->getContributorInfos('Takeshi Obata', 500),
			null // Second contributor not found.
		));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('manga')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'ja')->willReturn('ja');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.manga-sanctuary.com')->willReturn('Manga Sanctuary');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'genres' => [
					['model' => 'Project_Library_Genre', 'id' => 300, 'label' => 'Shonen'],
					['model' => 'Project_Library_Genre', 'id' => 301, 'label' => 'Comédie'],
					['model' => 'Project_Library_Genre', 'id' => 302, 'label' => 'Tranche de vie']
				],
				'year' => 2008,
				'description' => 'Autres genres : Social', // Not found genres.
				'label' => 'Bakuman',
				'aliases' => [
					[
						'label' => 'バクマン。',
						'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 200, 'label' => 'Japon',]],
						'model' => 'Project_Library_WorkAlias',
					],
				],
				'links' => [
					[
						'url' => 'https://www.manga-sanctuary.com/bdd/manga/6679-bakuman/',
						'label' => 'Manga Sanctuary',
						'model' => 'Project_Library_WorkLink',
					]
				],
				'typology' => 100,
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 200, 'label' => 'Japon']],
				'originalLanguage' => 'ja',
				'model' => 'Project_Library_Work'
			],
			'contributions' => [
				[
					'link' => [
						'url' => 'https://www.manga-sanctuary.com/bdd/personnalites/139-obata-takeshi.html',
						'label' => 'Manga Sanctuary',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Takeshi Obata',
					// First role not found.
					'contributor' => ['model' => 'Project_Library_Contributor', 'id' => 500, 'label' => 'Takeshi Obata'],
					'model' => 'Project_Library_Contribution'
				],
				[
					'link' => [
						'url' => 'https://www.manga-sanctuary.com/bdd/personnalites/1389-ohba-tsugumi.html',
						'label' => 'Manga Sanctuary',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Tsugumi Ohba',
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 401, 'label' => 'Scénariste']],
					// Second contributor not found.
					'model' => 'Project_Library_Contribution'
				]
			],
			'publication' => [
				'model' => 'Project_Library_Publication'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_TVSeries()
	{
		$parser = $this->getNewParser('https://www.manga-sanctuary.com/bdd/serietvanimee/468-dragon-ball/');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('JP')->willReturn(null);
		$this->expectResolverMethod(self::exactly(3), 'resolveGenreInfos')->withConsecutive(
			['Animation'], // Added by the type "Série TV animée".
			['Aventure'], // "Inconnue" is ignored.
			['Arts martiaux']
		)->will(self::onConsecutiveCalls(
			null,
			null,
			null
		));
		$this->expectResolverMethod(self::once(), 'resolveContributorInfos')->with(
			'Akira Toriyama', 'https://www.manga-sanctuary.com/bdd/personnalites/111-toriyama-akira.html'
		)->willReturn(null); // Contributor not found.
		$this->expectResolverMethod(self::once(), 'resolveContributionTypeInfos')->with('Dessinateur')->willReturn(null);
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['serietv'], ['support_video'])
			->will(self::onConsecutiveCalls(
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 100),
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 101)
			));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.manga-sanctuary.com')->willReturn('Manga Sanctuary');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'year' => 1986,
				'description' => 'Autres genres : Animation, Aventure, Arts martiaux',
				'typology' => 100,
				'label' => 'Dragon Ball',
				'aliases' => [
					[
						'label' => 'ドラゴンボール',
						'model' => 'Project_Library_WorkAlias',
						'description' => 'Autres nationalités : JP' // Country not found.
					]
				],
				'links' => [
					[
						'url' => 'https://www.manga-sanctuary.com/bdd/serietvanimee/468-dragon-ball/',
						'label' => 'Manga Sanctuary',
						'model' => 'Project_Library_WorkLink',
					]
				],
				'model' => 'Project_Library_Work'
			],
			'contributions' => [
				[
					'link' => [
						'url' => 'https://www.manga-sanctuary.com/bdd/personnalites/111-toriyama-akira.html',
						'label' => 'Manga Sanctuary',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Akira Toriyama',
					// Contributor not found.
					'model' => 'Project_Library_Contribution'
				]
			],
			'publication' => [
				'typology' => 101,
				'model' => 'Project_Library_Publication'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_MultipleTitles()
	{
		$parser = $this->getNewParser('https://www.manga-sanctuary.com/bdd/manga/15347-kenshin-le-vagabond-restauration/');

		$this->expectResolverMethod(self::exactly(4), 'resolveCountryInfos')->withConsecutive(['Japon'], ['US'], ['JP'], ['JP'])->willReturn(null);
		$this->expectResolverMethod(self::exactly(3), 'resolveGenreInfos')->withConsecutive(
			['Shonen'],
			['Historique'],
			['Action']
		)->will(self::onConsecutiveCalls(
			null,
			null,
			null
		));
		$this->expectResolverMethod(self::once(), 'resolveContributorInfos')->with(
			'Nobuhiro Watsuki', 'https://www.manga-sanctuary.com/bdd/personnalites/86-watsuki-nobuhiro.html'
		)->willReturn(null); // Contributor not found.
		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')->withConsecutive(['Dessinateur'], ['Scénariste'])
			->willReturn(null);
		$this->expectResolverMethod(self::once(), 'resolveTypology')->withConsecutive(['manga'])
			->will(self::onConsecutiveCalls(
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 100)
			));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'ja')->willReturn('ja');
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('www.manga-sanctuary.com')->willReturn('Manga Sanctuary');
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'work' => [
				'year' => 2012,
				'description' => 'Autres nationalités : Japon

Autres genres : Shonen, Historique, Action',
				'typology' => 100,
				'label' => 'Kenshin le Vagabond - Restauration',
				'model' => 'Project_Library_Work',
				'aliases' => [
					[
						'label' => 'Rurouni Kenshin - Restoration',
						'model' => 'Project_Library_WorkAlias',
						'description' => 'Autres nationalités : US'
					],
					[
						'label' => 'るろうに剣心─特筆版─',
						'model' => 'Project_Library_WorkAlias',
						'description' => 'Autres nationalités : JP'
					],
					[
						'label' => 'Rurôni Kenshin - Tokuhitsuban',
						'model' => 'Project_Library_WorkAlias',
						'description' => 'Autres nationalités : JP'
					]
				],
				'links' => [[
					'url' => 'https://www.manga-sanctuary.com/bdd/manga/15347-kenshin-le-vagabond-restauration/',
					'label' => 'Manga Sanctuary',
					'model' => 'Project_Library_WorkLink'
				]],
				'originalLanguage' => 'ja'
			],
			'publication' => [
				'model' => 'Project_Library_Publication',
			],
			'contributions' => [[
				'link' => [
					'url' => 'https://www.manga-sanctuary.com/bdd/personnalites/86-watsuki-nobuhiro.html',
					'label' => 'Manga Sanctuary',
					'model' => 'Project_Library_ContributorLink'
				],
				'label' => 'Nobuhiro Watsuki',
				'model' => 'Project_Library_Contribution'
			]]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}