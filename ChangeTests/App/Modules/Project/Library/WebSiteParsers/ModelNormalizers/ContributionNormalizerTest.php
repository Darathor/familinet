<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\ContributionNormalizerTest
 */
class ContributionNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize_Found()
	{
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('website.com')->willReturn('Website');
		$this->expectResolverMethod(self::once(), 'resolveContributorInfos')->with('Label')->willReturn($this->getContributorInfos('Label', 20));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')
			->withConsecutive(['Acteur'], ['Réalisateur'])
			->will(self::onConsecutiveCalls($this->getContributionTypeInfos('Acteur', 10), $this->getContributionTypeInfos('Réalisateur', 20)));
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributionModel())
			->setLabel('Label')
			->setLink('http://website.com/page.php')
			->addRoles(['Acteur' => ['Personnage' => ['Saison 7', 'Saison 8']], 'Réalisateur' => []]);

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'link' =>
				[
					'url' => 'http://website.com/page.php',
					'label' => 'Website',
					'model' => 'Project_Library_ContributorLink',
				],
			'label' => 'Label',
			'roles' => [
				[
					'model' => 'Project_Library_ContributionType',
					'id' => 10,
					'label' => 'Acteur',
				],
				[
					'model' => 'Project_Library_ContributionType',
					'id' => 20,
					'label' => 'Réalisateur',
				],
			],
			'roleDetail' => 'Personnage (Saison 7, Saison 8)',
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'id' => 20,
				'label' => 'Label',
			],
			'model' => 'Project_Library_Contribution'
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://website.com/reference.php')));
	}
}