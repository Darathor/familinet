<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\ContributorAliasNormalizerTest
 */
class ContributorAliasNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize()
	{
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorAliasModel())
			->setLabel('Label')
			->setOriginal(true);

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'original' => true,
			'label' => 'Label',
			'model' => 'Project_Library_ContributorAlias',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}
}