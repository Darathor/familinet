<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\ContributorLinkNormalizerTest
 */
class ContributorLinkNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize_LabeledLink()
	{
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorLinkModel())
			->setUrl('http://domain.com/page.php?param=value#anchor')
			->setLabel('Label');

		$normalizer = $this->buildNormalizer($model, ['Project_Library_WorkLink', \Project\Library\Documents\WorkLink::class]);

		$expected = [
			'url' => 'http://domain.com/page.php?param=value#anchor',
			'label' => 'Label',
			'model' => 'Project_Library_ContributorLink',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}

	public function testNormalize_AbsoluteWebsiteLink()
	{
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('website.com')->willReturn('Website');
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorLinkModel())
			->setUrl('http://website.com/page.php?param=value#anchor');

		$normalizer = $this->buildNormalizer($model, ['Project_Library_WorkLink', \Project\Library\Documents\WorkLink::class]);

		$expected = [
			'url' => 'http://website.com/page.php?param=value#anchor',
			'label' => 'Website',
			'model' => 'Project_Library_ContributorLink',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}

	public function testNormalize_RelativeLink()
	{
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('website.com')->willReturn('Website');
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorLinkModel())
			->setUrl('page.php?param=value#anchor');

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'url' => 'http://website.com/page.php?param=value#anchor',
			'label' => 'Website',
			'model' => 'Project_Library_ContributorLink',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://website.com/reference.php')));
	}

	public function testNormalize_WebsiteLinkNotFound()
	{
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('website.com')->willReturn(null);
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorLinkModel())
			->setUrl('http://website.com/page.php?param=value#anchor');

		$normalizer = $this->buildNormalizer($model);

		$expected = [];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}
}