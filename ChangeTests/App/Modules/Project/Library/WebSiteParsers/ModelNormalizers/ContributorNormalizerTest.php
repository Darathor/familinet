<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\ContributorNormalizerTest
 */
class ContributorNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize_CaseOne()
	{
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('website.com')->willReturn('Website');
		$this->expectResolverMethod(self::exactly(3), 'resolveCountryInfos')
			->withConsecutive(['fr'], ['en'], ['bidon'])
			->will(self::onConsecutiveCalls($this->getCountryInfos('France', 10), $this->getCountryInfos('Grande-Bretagne', 20), null));
		$this->applyResolverDefault();

		$birthDate = (new \DateTime())->setDate(2000, 10, 11)->setTime(00, 00, 00);
		$deathDate = (new \DateTime())->setDate(2009, 06, 10)->setTime(00, 00, 00);

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorModel())
			->setLabel('Label')
			->setImage('http://image.com/image.jpg')
			->setBirthDate($birthDate->format(\DateTime::ATOM))
			->setDeathDate($deathDate)
			->addNationalities('fr')
			->addNationalities(['fr', 'en'])
			->addNationalities('bidon')
			->addLinkByUrl('page.php')
			->addLink('Lien', 'http://domaine.com/page.php')
			->addAlias('Alias', false)
			->appendToDescription('Description')
			->appendToDescription('Nouvelle entrée', 'Deuxième');

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'birthDate' => '2000-10-11T00:00:00+00:00',
			'birthYear' => 2000,
			'deathDate' => '2009-06-10T00:00:00+00:00',
			'deathYear' => 2009,
			'image' => 'http://image.com/image.jpg',
			'description' => "Description\n\n**Deuxième :** Nouvelle entrée\n\nAutres nationalités : bidon",
			'label' => 'Label',
			'nationalities' => [
				[
					'model' => 'Rbs_Geo_Country',
					'id' => 10,
					'label' => 'France',
				],
				[
					'model' => 'Rbs_Geo_Country',
					'id' => 20,
					'label' => 'Grande-Bretagne',
				],
			],
			'aliases' => [
				[
					'label' => 'Alias',
					'model' => 'Project_Library_ContributorAlias',
				],
			],
			'links' => [
				[
					'url' => 'http://website.com/page.php',
					'label' => 'Website',
					'model' => 'Project_Library_ContributorLink',
				],
				[
					'url' => 'http://domaine.com/page.php',
					'label' => 'Lien',
					'model' => 'Project_Library_ContributorLink',
				],
			],
			'model' => 'Project_Library_Contributor'
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://website.com/reference.php')));
	}

	public function testNormalize_CaseTwo()
	{
		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('fr')->willReturn($this->getCountryInfos('France', 10));
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\ContributorModel())
			->setLabel('Label')
			->setBirthYear(2000)
			->setDeathYear(2009)
			->addNationalities('fr')
			->addAlias('Alias', true)
			->appendToDescription('Description')
			->unsetDescription();

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'birthYear' => 2000,
			'deathYear' => 2009,
			'label' => 'Label',
			'nationalities' => [
				[
					'model' => 'Rbs_Geo_Country',
					'id' => 10,
					'label' => 'France',
				]
			],
			'aliases' => [
				[
					'original' => true,
					'label' => 'Alias',
					'model' => 'Project_Library_ContributorAlias'
				],
			],
			'model' => 'Project_Library_Contributor'
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://website.com/reference.php')));
	}
}