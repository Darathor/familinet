<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\PublicationNormalizerTest
 */
class PublicationNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize_Found()
	{
		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('livre')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolvePublisherInfos')->with('Publisher')->willReturn($this->getPublisherInfos('Publisher', 10));
		$this->expectResolverMethod(self::once(), 'resolvePublisherCollectionInfos')->with('Collection')
			->willReturn($this->getPublisherCollectionInfos('Collection', 20));
		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 30));
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\PublicationModel())
			->setCollection('Collection')
			->setPublisher('Publisher')
			->setOneShot(true)
			->setStatus('ended')
			->setYear(2000)
			->setTypology('livre')
			->setEndYear(2010)
			->setVolumesDetail('détail volume')
			->appendToDescription('Description');

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 30, 'label' => 'Terminé'],
			'oneShot' => true,
			'volumesDetail' => 'détail volume',
			'year' => 2000,
			'endYear' => 2010,
			'typology' => 100,
			'model' => 'Project_Library_Publication',
			'publishers' => [['model' => 'Project_Library_Publisher', 'id' => 10, 'label' => 'Publisher']],
			'collection' => ['model' => 'Project_Library_Collection', 'id' => 20, 'label' => 'Collection'],
			'description' => 'Description'
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}

	public function testNormalize_NotFound()
	{
		$this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('livre')->willReturn(null);
		$this->expectResolverMethod(self::once(), 'resolvePublisherInfos')->with('Publisher')->willReturn(null);
		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn(null);
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\PublicationModel())
			->setCollection('Collection')
			->setPublisher('Publisher')
			->setOneShot(false)
			->setStatus('ended')
			->setYear(2000)
			->setTypology('livre')
			->appendToDescription('Description')
			->unsetDescription();

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'year' => 2000,
			'description' => "Statut : ended\n\nÉditeur : Publisher\n\nCollection : Collection",
			'model' => 'Project_Library_Publication'
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}
}