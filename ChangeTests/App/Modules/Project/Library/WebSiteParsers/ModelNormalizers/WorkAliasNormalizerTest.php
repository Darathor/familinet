<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\WorkAliasNormalizerTest
 */
class WorkAliasNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize()
	{
		$this->expectResolverMethod(self::exactly(3), 'resolveCountryInfos')
			->withConsecutive(['fr'], ['en'], ['bidon'])
			->will(self::onConsecutiveCalls($this->getCountryInfos('France', 10), $this->getCountryInfos('Grande-Bretagne', 20), null));
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\WorkAliasModel())
			->setLabel('Label')
			->setOriginal(true)
			->addNationalities('fr')
			->addNationalities(['fr', 'en'])
			->addNationalities('bidon');

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'original' => true,
			'label' => 'Label',
			'nationalities' => [
				[
					'model' => 'Rbs_Geo_Country',
					'id' => 10,
					'label' => 'France',
				],
				[
					'model' => 'Rbs_Geo_Country',
					'id' => 20,
					'label' => 'Grande-Bretagne',
				],
			],
			'model' => 'Project_Library_WorkAlias',
			'description' => 'Autres nationalités : bidon',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}
}