<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\ModelNormalizers\WorkNormalizerTest
 */
class WorkNormalizerTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\NormalizerTestBase
{
	public function testNormalize()
	{
		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('serietv')->willReturn($typology);
		$this->expectResolverMethod(self::exactly(2), 'resolveCountryInfos')->with('fr')->willReturn($this->getCountryInfos('France', 10));
		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 20));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')->with('Project_Library_Languages', 'fr')->willReturn('fr');
		$this->expectResolverMethod(self::exactly(3), 'resolveGenreInfos')
			->withConsecutive(['Comédie'], ['Humour'], ['Bidon'])
			->will(self::onConsecutiveCalls($this->getGenreInfos('Comédie', 30), $this->getGenreInfos('Humour', 31), null));
		$this->applyResolverDefault();

		$model = (new \Project\Library\WebSiteParsers\Models\WorkModel())
			->setTypology('serietv')
			->setLabel('Label')
			->setEndYear(2000)
			->setYear(1999)
			->setStatus('ended')
			->setImage('http://image.ocm/image.jpg')
			->setAudience('Audience')
			->setSubtitle('Subtitle')
			->addNationalities('fr')
			->addAlias('Alias', true)
			->addLink('Lien', 'http://domaine.com/page.php')
			->addGenres('Comédie')
			->addGenres(['Comédie', 'Humour', 'bidon'])
			->addOriginalLanguages('fr')
			->appendToDescription('Contenu', 'Titre');

		$normalizer = $this->buildNormalizer($model);

		$expected = [
			'subtitle' => 'Subtitle',
			'status' => [
				'model' => 'Project_Library_WorkStatus',
				'id' => 20,
				'label' => 'Terminé',
			],
			'genres' => [
				[
					'model' => 'Project_Library_Genre',
					'id' => 30,
					'label' => 'Comédie',
				],
				[
					'model' => 'Project_Library_Genre',
					'id' => 31,
					'label' => 'Humour',
				],
			],
			'image' => 'http://image.ocm/image.jpg',
			'audience' => 'Audience',
			'year' => 1999,
			'endYear' => 2000,
			'description' => "**Titre :** Contenu\n\nAutres genres : Bidon",
			'typology' => 100,
			'label' => 'Label',
			'nationalities' => [[
				'model' => 'Rbs_Geo_Country',
				'id' => 10,
				'label' => 'France',
			]],
			'model' => 'Project_Library_Work',
			'aliases' => [[
				'original' => true,
				'label' => 'Alias',
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 10,
					'label' => 'France',
				]],
				'model' => 'Project_Library_WorkAlias',
			]],
			'links' => [[
				'url' => 'http://domaine.com/page.php',
				'label' => 'Lien',
				'model' => 'Project_Library_WorkLink',
			]],
			'originalLanguage' => 'fr',
		];

		self::assertEquals($expected, $normalizer->normalize(new \Zend\Uri\Http('http://domain.com/reference.php')));
	}
}