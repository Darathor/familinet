<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ContributionModelTest
 */
class ContributionModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DocumentModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
	}

	public function testInit()
	{
		$model = \Project\Library\WebSiteParsers\Models\ContributionModel::init('label', 'url');
		self::assertEquals('label', $model->getLabel());
		self::assertEquals('url', $model->getLink()->getUrl());
	}

	public function testGetSetLink()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		self::assertEquals(new \Project\Library\WebSiteParsers\Models\ContributorLinkModel(), $model->getLink());
		self::assertEquals($model, $model->setLink('url'));
		self::assertEquals('url', $model->getLink()->getUrl());
	}

	public function testGetAddRoles()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		self::assertEquals([], $model->getRoles());

		self::assertEquals($model, $model->addRole('code1', 'detail1', 'context1'));
		self::assertEquals(['code1' => ['detail1' => ['context1']]], $model->getRoles());

		self::assertEquals($model, $model->addRole('code1', 'detail1', 'context1'));
		self::assertEquals(['code1' => ['detail1' => ['context1']]], $model->getRoles());

		self::assertEquals($model, $model->addRole('code1', 'detail1', 'context2'));
		self::assertEquals(['code1' => ['detail1' => ['context1', 'context2']]], $model->getRoles());

		self::assertEquals($model, $model->addRole('code1', 'detail2'));
		self::assertEquals(['code1' => ['detail1' => ['context1', 'context2'], 'detail2' => []]], $model->getRoles());

		self::assertEquals($model, $model->addRole('code2', 'detail1'));
		self::assertEquals(['code1' => ['detail1' => ['context1', 'context2'], 'detail2' => []], 'code2' => ['detail1' => []]], $model->getRoles());

		$model = new \Project\Library\WebSiteParsers\Models\ContributionModel();
		self::assertEquals([], $model->getRoles());

		self::assertEquals($model, $model->addRoles(['code1' => ['detail1' => ['context1', 'context2'], 'detail2' => []], 'code2' => ['detail1' => []]]));
		self::assertEquals(['code1' => ['detail1' => ['context1', 'context2'], 'detail2' => []], 'code2' => ['detail1' => []]], $model->getRoles());
	}
}