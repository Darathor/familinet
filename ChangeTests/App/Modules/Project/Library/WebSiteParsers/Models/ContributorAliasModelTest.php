<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ContributorAliasModelTest
 */
class ContributorAliasModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorAliasModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
	}

	public function testGetSetOriginal()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorAliasModel();
		self::assertEquals(false, $model->getOriginal());
		self::assertEquals($model, $model->setOriginal(true));
		self::assertEquals(true, $model->getOriginal());
	}

	public function testGetSetForWebsite()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorAliasModel();
		self::assertEquals(null, $model->getForWebsite());
		self::assertEquals($model, $model->setForWebsite('www.allocine.fr'));
		self::assertEquals('www.allocine.fr', $model->getForWebsite());
		self::assertEquals($model, $model->setForWebsite(null));
		self::assertEquals(null, $model->getForWebsite());
	}
}