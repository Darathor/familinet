<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ContributorLinkModelTest
 */
class ContributorLinkModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorLinkModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
	}

	public function testGetSetUrl()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorLinkModel();
		self::assertEquals(null, $model->getUrl());
		self::assertEquals($model, $model->setUrl('url'));
		self::assertEquals('url', $model->getUrl());
	}
}