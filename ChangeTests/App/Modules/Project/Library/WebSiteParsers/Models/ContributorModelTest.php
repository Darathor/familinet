<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\ContributorModelTest
 */
class ContributorModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DescribedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DocumentModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LinkedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\NationalizedModel', $uses);
	}

	public function testGetSetBirth()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();
		self::assertEquals(null, $model->getBirthDate());
		self::assertEquals(null, $model->getBirthYear());

		self::assertEquals($model, $model->setBirthYear(2000));
		self::assertEquals(null, $model->getBirthDate());
		self::assertEquals(2000, $model->getBirthYear());

		$date = new \DateTime('2021-05-10T00:37:32+01:00');
		$txt = $date->format(\DateTime::ATOM);
		$year = (int)$date->format('Y');
		self::assertEquals($model, $model->setBirthDate($date));
		self::assertEquals('2021-05-10T00:00:00+00:00', $model->getBirthDate());
		self::assertEquals($year, $model->getBirthYear());
	}

	public function testGetSetDeath()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();
		self::assertEquals(null, $model->getDeathDate());
		self::assertEquals(null, $model->getDeathYear());

		self::assertEquals($model, $model->setDeathYear(2000));
		self::assertEquals(null, $model->getDeathDate());
		self::assertEquals(2000, $model->getDeathYear());

		$date = new \DateTime('2021-05-10T00:37:32+01:00');
		$txt = $date->format(\DateTime::ATOM);
		$year = (int)$date->format('Y');
		self::assertEquals($model, $model->setDeathDate($date));
		self::assertEquals('2021-05-10T00:00:00+00:00', $model->getDeathDate());
		self::assertEquals($year, $model->getDeathYear());
	}

	public function testGetSetImage()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();
		self::assertEquals(null, $model->getImage());
		self::assertEquals($model, $model->setImage('img'));
		self::assertEquals('img', $model->getImage());
	}

	public function testGetAddAliases()
	{
		$model = new \Project\Library\WebSiteParsers\Models\ContributorModel();

		$aliases = [];
		self::assertEquals($aliases, $model->getAliases());

		self::assertEquals($model, $model->addAlias('label1', true));
		$aliases[] = $this->initAlias('label1', true);
		self::assertEquals(print_r($aliases, true), print_r($model->getAliases(), true));

		self::assertEquals($model, $model->addAlias('label2', false));
		$aliases[] = $this->initAlias('label2', false);
		self::assertEquals($aliases, $model->getAliases());

		self::assertEquals($model, $model->addAlias('label3'));
		$aliases[] = $this->initAlias('label3');
		self::assertEquals($aliases, $model->getAliases());
	}

	/**
	 * @param string $label
	 * @param bool $original
	 * @return \Project\Library\WebSiteParsers\Models\ContributorAliasModel
	 */
	private function initAlias(string $label, bool $original = true)
	{
		$alias = new \Project\Library\WebSiteParsers\Models\ContributorAliasModel();
		$alias->setLabel($label);
		$alias->setOriginal($original);
		return $alias;
	}
}