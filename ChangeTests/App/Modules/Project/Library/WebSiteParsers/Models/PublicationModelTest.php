<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\PublicationModelTest
 */
class PublicationModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DatedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DescribedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DocumentModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\NationalizedModel', $uses);
	}

	public function testGetSetStatus()
	{
		$model = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		self::assertEquals(null, $model->getStatus());
		self::assertEquals($model, $model->setStatus('status'));
		self::assertEquals('status', $model->getStatus());
	}

	public function testGetSetOneShot()
	{
		$model = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		self::assertEquals(false, $model->getOneShot());
		self::assertEquals($model, $model->setOneShot(true));
		self::assertEquals(true, $model->getOneShot());
	}

	public function testGetSetPublisher()
	{
		$model = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		self::assertEquals(null, $model->getPublisher());
		self::assertEquals($model, $model->setPublisher('publisher'));
		self::assertEquals('publisher', $model->getPublisher());
	}

	public function testGetSetCollection()
	{
		$model = new \Project\Library\WebSiteParsers\Models\PublicationModel();
		self::assertEquals(null, $model->getCollection());
		self::assertEquals($model, $model->setCollection('collection'));
		self::assertEquals('collection', $model->getCollection());
	}
}