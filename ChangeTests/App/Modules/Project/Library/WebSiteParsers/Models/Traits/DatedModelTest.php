<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\DatedModelTest
 */
class DatedModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetSetYear()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\DatedModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\DatedModel::class);
		self::assertEquals(null, $mock->getYear());
		self::assertEquals($mock, $mock->setYear(2000));
		self::assertEquals(2000, $mock->getYear());
	}

	public function testGetSetEndYear()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\DatedModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\DatedModel::class);
		self::assertEquals(null, $mock->getEndYear());
		self::assertEquals($mock, $mock->setEndYear(2000));
		self::assertEquals(2000, $mock->getEndYear());
	}
}