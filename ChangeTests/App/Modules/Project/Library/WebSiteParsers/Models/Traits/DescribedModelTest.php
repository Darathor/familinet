<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\DescribedModelTest
 */
class DescribedModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetSetYear()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\DescribedModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\DescribedModel::class);
		$description = null;
		self::assertEquals($description, $mock->getDescription());

		self::assertEquals($mock, $mock->appendToDescription(''));
		self::assertEquals($description, $mock->getDescription());

		self::assertEquals($mock, $mock->appendToDescription('test'));
		$description = 'test';
		self::assertEquals($description, $mock->getDescription());

		self::assertEquals($mock, $mock->appendToDescription('test&amp;'));
		$description .= "\n\n" . 'test&';
		self::assertEquals($description, $mock->getDescription());

		self::assertEquals($mock, $mock->appendToDescription('test', 'title'));
		$description .= "\n\n" . '**title :** test';
		self::assertEquals($description, $mock->getDescription());

		self::assertEquals($mock, $mock->unsetDescription());
		$description = null;
		self::assertEquals($description, $mock->getDescription());
	}
}