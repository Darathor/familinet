<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\DocumentModelTest
 */
class DocumentModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetSetTypology()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\DocumentModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\DocumentModel::class);
		self::assertEquals(null, $mock->getTypology());
		self::assertEquals($mock, $mock->setTypology('typology'));
		self::assertEquals('typology', $mock->getTypology());
	}

	public function testGetSetAtributes()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\DocumentModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\DocumentModel::class);
		$attributes = [];
		self::assertEquals($attributes, $mock->getAttributes());

		self::assertEquals($mock, $mock->setAttribute('attr1', 'value1'));
		$attributes['attr1'] = 'value1';
		self::assertEquals($attributes, $mock->getAttributes());

		self::assertEquals($mock, $mock->setAttribute('attr1', 'value2'));
		$attributes['attr1'] = 'value2';
		self::assertEquals($attributes, $mock->getAttributes());

		self::assertEquals($mock, $mock->setAttribute('attr2', 'value3'));
		$attributes['attr2'] = 'value3';
		self::assertEquals($attributes, $mock->getAttributes());
	}
}