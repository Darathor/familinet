<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\LabeledModelTest
 */
class LabeledModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetSetLabel()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\LabeledModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\LabeledModel::class);
		self::assertEquals(null, $mock->getLabel());
		self::assertEquals($mock, $mock->setLabel('test'));
		self::assertEquals('test', $mock->getLabel());
	}
}