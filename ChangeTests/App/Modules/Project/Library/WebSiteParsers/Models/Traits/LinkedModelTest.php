<?php
/** @noinspection UnnecessaryAssertionInspection */

namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\LinkedModelTest
 */
class LinkedModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetAddLinks()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\LinkedModel|\PHPUnit\Framework\MockObject\MockObject $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\LinkedModel::class);
		$mock->expects(self::any())->method('getLinkClass')->willReturn(\Project\Library\WebSiteParsers\Models\WorkLinkModel::class);
		$links = [];
		self::assertEquals($links, $mock->getLinks());

		self::assertEquals($mock, $mock->addLink('label1', 'url1'));
		$links[] = $this->initLink('label1', 'url1');
		self::assertEquals($links, $mock->getLinks());

		self::assertEquals($mock, $mock->addLink('label2', ''));
		self::assertEquals($links, $mock->getLinks());

		self::assertEquals($mock, $mock->addLink('', 'url2'));
		self::assertEquals($links, $mock->getLinks());

		self::assertEquals($mock, $mock->addLinkByUrl('url3'));
		$links[] = $this->initLink('', 'url3');
		self::assertEquals($links, $mock->getLinks());

		self::assertEquals($mock, $mock->addLinkByUrl(''));
		self::assertEquals($links, $mock->getLinks());
	}

	/**
	 * @param string $label
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\Models\AbstractLinkModel
	 */
	private function initLink(string $label, string $url)
	{
		$link = new \Project\Library\WebSiteParsers\Models\WorkLinkModel();
		$link->setLabel($label);
		$link->setUrl($url);
		return $link;
	}
}