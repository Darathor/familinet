<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\Traits\NationalizedModelTest
 */
class NationalizedModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetAddNationalities()
	{
		/** @var \Project\Library\WebSiteParsers\Models\Traits\NationalizedModel $mock */
		$mock = $this->getMockForTrait(\Project\Library\WebSiteParsers\Models\Traits\NationalizedModel::class);
		$nationalities = [];
		self::assertEquals($nationalities, $mock->getNationalities());

		self::assertEquals($mock, $mock->addNationalities(100));
		self::assertEquals($nationalities, $mock->getNationalities());

		self::assertEquals($mock, $mock->addNationalities(''));
		self::assertEquals($nationalities, $mock->getNationalities());

		self::assertEquals($mock, $mock->addNationalities('France'));
		$nationalities[] = 'France';
		self::assertEquals($nationalities, $mock->getNationalities());

		self::assertEquals($mock, $mock->addNationalities(['Belgique', 'Italie', 'Belgique']));
		$nationalities[] = 'Belgique';
		$nationalities[] = 'Italie';
		self::assertEquals($nationalities, $mock->getNationalities());
	}
}