<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\Change\Collection\BaseItemTest
 */
class WorkAliasModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkAliasModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\NationalizedModel', $uses);
	}

	public function testGetSetOriginal()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkAliasModel();
		self::assertEquals(false, $model->getOriginal());
		self::assertEquals($model, $model->setOriginal(true));
		self::assertEquals(true, $model->getOriginal());
	}
}