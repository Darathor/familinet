<?php
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\WorkModelTest
 */
class WorkModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testTraits()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		$uses = \class_uses($model);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DatedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DescribedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\DocumentModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LabeledModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\LinkedModel', $uses);
		self::assertArrayHasKey('Project\Library\WebSiteParsers\Models\Traits\NationalizedModel', $uses);
	}

	public function testGetSetSubtitle()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals(null, $model->getSubtitle());
		self::assertEquals($model, $model->setSubtitle('subtitle'));
		self::assertEquals('subtitle', $model->getSubtitle());
	}

	public function testGetSetStatus()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals(null, $model->getStatus());
		self::assertEquals($model, $model->setStatus('status'));
		self::assertEquals('status', $model->getStatus());
	}

	public function testGetSetImage()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals(null, $model->getImage());
		self::assertEquals($model, $model->setImage('img'));
		self::assertEquals('img', $model->getImage());
	}

	public function testGetSetAudience()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals(null, $model->getAudience());
		self::assertEquals($model, $model->setAudience('audience'));
		self::assertEquals('audience', $model->getAudience());
	}

	public function testGetAddOriginalLanguages()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals([], $model->getOriginalLanguages());

		self::assertEquals($model, $model->addOriginalLanguages('fr'));
		self::assertEquals(['fr'], $model->getOriginalLanguages());

		self::assertEquals($model, $model->addOriginalLanguages(['jp', 'en', 'jp ', 200, '']));
		self::assertEquals(['fr', 'jp', 'en'], $model->getOriginalLanguages());

		self::assertEquals($model, $model->addOriginalLanguages('de'));
		self::assertEquals(['fr', 'jp', 'en', 'de'], $model->getOriginalLanguages());

		self::assertEquals($model, $model->addOriginalLanguages('jp'));
		self::assertEquals(['fr', 'jp', 'en', 'de'], $model->getOriginalLanguages());
	}

	public function testGetAddGenres()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();
		self::assertEquals([], $model->getGenres());

		self::assertEquals($model, $model->addGenres('genre1'));
		self::assertEquals(['Genre1'], $model->getGenres());

		self::assertEquals($model, $model->addGenres(['genre2', 'Genre3', 'Genre2', 200, ' ']));
		self::assertEquals(['Genre1', 'Genre2', 'Genre3'], $model->getGenres());

		self::assertEquals($model, $model->addGenres('genre1'));
		self::assertEquals(['Genre1', 'Genre2', 'Genre3'], $model->getGenres());

		self::assertEquals($model, $model->addGenres('genre4'));
		self::assertEquals(['Genre1', 'Genre2', 'Genre3', 'Genre4'], $model->getGenres());
	}

	public function testGetAddAliases()
	{
		$model = new \Project\Library\WebSiteParsers\Models\WorkModel();

		$aliases = [];
		self::assertEquals($aliases, $model->getAliases());

		self::assertEquals($model, $model->addAlias('label1', true, 'France'));
		$aliases[] = $this->initAlias('label1', true, ['France']);
		self::assertEquals(print_r($aliases, true), print_r($model->getAliases(), true));

		self::assertEquals($model, $model->addAlias('label2', false, ['France', 'USA']));
		$aliases[] = $this->initAlias('label2', false, ['France', 'USA']);
		self::assertEquals($aliases, $model->getAliases());

		self::assertEquals($model, $model->addAlias('label3'));
		$aliases[] = $this->initAlias('label3');
		self::assertEquals($aliases, $model->getAliases());
	}

	/**
	 * @param string $label
	 * @param bool $original
	 * @param string[] $nationalities
	 * @return \Project\Library\WebSiteParsers\Models\WorkAliasModel
	 */
	private function initAlias(string $label, bool $original = true, array $nationalities = [])
	{
		$alias = new \Project\Library\WebSiteParsers\Models\WorkAliasModel();
		$alias->setLabel($label);
		$alias->setOriginal($original);
		$alias->addNationalities($nationalities);
		return $alias;
	}
}