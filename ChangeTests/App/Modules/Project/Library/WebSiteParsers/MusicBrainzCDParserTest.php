<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\MusicBrainzCDParserTest
 */
class MusicBrainzCDParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\MusicBrainzCDParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\MusicBrainzCDParser::class, $url);
	}

	public function testParser_TwoAuthors()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/release/6a661fb2-a950-491d-bcbd-308aa6e24ca1');

		$this->expectResolverMethod(self::exactly(2), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur'],
			['Auteur']
		)->will(self::onConsecutiveCalls(
			null, // Role not found for first contributor.
			$this->getContributionTypeInfos('Auteur', 400)
		));
		$this->expectResolverMethod(self::exactly(2), 'resolveContributorInfos')->withConsecutive(
			['Céline Dion', 'https://musicbrainz.org/artist/847e8a0c-cc20-4213-9e16-975515c2a926'],
			['Anne Geddes', 'https://musicbrainz.org/artist/9be49fcb-3422-48ac-8163-298269f3eb62']
		)->will(self::onConsecutiveCalls(
			$this->getContributorInfos('Céline Dion', 500),
			null // Second contributor not found.
		));
		$this->expectResolverMethod(self::exactly(1), 'resolveCountryInfos')->with('Europe')->willReturn($this->getCountryInfos('Europe', 300));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'English')->willReturn('en');
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['musique'], ['support_audio'])
			->will(self::onConsecutiveCalls(
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 100), $this->getNewReadonlyDocument('Rbs_Generic_Typology', 101)
			));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
				'typology' => 100,
				'label' => 'Miracle',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://musicbrainz.org/release/6a661fb2-a950-491d-bcbd-308aa6e24ca1',
					'label' => 'MusicBrainz',
					'model' => 'Project_Library_WorkLink'
				]],
				'originalLanguage' => 'en'
			],
			'publication' => [
				'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
				'oneShot' => true,
				'year' => 2004,
				'description' => 'CD 1

1. Miracle
2. Brahms\' Lullaby
3. If I Could
4. Sleep Tight
5. What a Wonderful World
6. My Precious One
7. A Mother\'s Prayer
8. The First Time Ever I Saw Your Face
9. Baby Close Your Eyes
10. Come to Me
11. Le Loup, la biche et le chevalier (Une chanson douce)
12. Beautiful Boy
13. In Some Small Way',
				'typology' => 101,
				'attributes' => [
					'detail_format' => 'CD',
					'duree_totale' => '52:32',
					'code_barre' => '5099751874829',
					'type_album' => 'Album',
					'id_amazon' => 'DE: B0002XK4DI',
					'date_publication' => '2004-10-11T00:00:00+00:00',
					'type_support_audio' => 'CD'
				],
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 300, 'label' => 'Europe']],
				'model' => 'Project_Library_Publication'
			],
			'contributions' => [
				[
					'link' => [
						'url' => 'https://musicbrainz.org/artist/847e8a0c-cc20-4213-9e16-975515c2a926',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Céline Dion',
					'model' => 'Project_Library_Contribution',
					'contributor' => [
						'model' => 'Project_Library_Contributor',
						'id' => 500,
						'label' => 'Céline Dion'
					]
				],
				[
					'link' => [
						'url' => 'https://musicbrainz.org/artist/9be49fcb-3422-48ac-8163-298269f3eb62',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Anne Geddes',
					'model' => 'Project_Library_Contribution',
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 400, 'label' => 'Auteur']]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_TwoCDAndPublisher()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/release/5439029e-b40c-439a-a523-6c7fdcec1fc9');

		$this->expectResolverMethod(self::exactly(2), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::once(), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur']
		)->will(self::onConsecutiveCalls(
			$this->getContributionTypeInfos('Auteur', 200)
		));
		$this->expectResolverMethod(self::once(), 'resolveContributorInfos')
			->with('Les Rois de la Suède', 'https://musicbrainz.org/artist/8ba1136a-f231-474c-9084-7f9c36172ad2')
			->willReturn($this->getContributorInfos('Les Rois de la Suède', 300));
		$this->expectResolverMethod(self::exactly(1), 'resolvePublisherInfos')->with('Adone')->willReturn($this->getPublisherInfos('Adone', 400));
		$this->expectResolverMethod(self::exactly(1), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 500));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'French')->willReturn('fr');
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['musique'], ['support_audio'])
			->will(self::onConsecutiveCalls(
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 600), $this->getNewReadonlyDocument('Rbs_Generic_Typology', 601)
			));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
				'typology' => 600,
				'label' => 'Best Of, Volume 1',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://musicbrainz.org/release/5439029e-b40c-439a-a523-6c7fdcec1fc9',
					'label' => 'MusicBrainz',
					'model' => 'Project_Library_WorkLink'
				]],
				'originalLanguage' => 'fr'
			],
			'publication' => [
				'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 100, 'label' => 'Terminé'],
				'oneShot' => true,
				'year' => 2010,
				'description' => 'CD 1: Best Of Vol. I

1. Il y a la ville
2. Socialisme
3. Myspace tu vas mourir
4. Pårniåk le cougar
5. Le p\'tit gros de Tokio Hotel
6. La fête chez les pauvres
7. T\'es belle
8. La crasse américaine
9. La faute aux enfants
10. Je te regarde mon amour
11. Les deux doigts dans la prise
12. Les chanteurs qui dérangent

CD 2: Top Gün contre les Rois de la Suède

1. Prologue
2. Les Rois de la Suède (Impressionants)
3. Socialisme (vintage mix)
4. Pårniåk le cougar (vintage mix)
5. C\'est parti pour la guerre
6. Post combustion
7. Porté par les vents
8. Pardon à toi l\'enfant du futur
9. Tu ne passeras pas par moi
10. Aimer à mort
11. Pårniåk II le cougar II
12. Un plus un
13. Cheval de Suède
14. L\'important
15. Il y a la ville (vintage mix)',
				'typology' => 601,
				'attributes' => [
					'detail_format' => '2×CD',
					'duree_totale' => '1:43:13',
					'code_barre' => '3700426915649',
					'type_album' => 'Album',
					'id_amazon' => 'FR: B0043BNYVO',
					'date_publication' => '2010-11-02T00:00:00+00:00',
					'type_support_audio' => 'CD',
					'numero_catalogue' => 'ADON011'
				],
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 500, 'label' => 'France']],
				'model' => 'Project_Library_Publication',
				'publishers' => [['model' => 'Project_Library_Publisher', 'id' => 400, 'label' => 'Adone']]
			],
			'contributions' => [
				[
					'link' => [
						'url' => 'https://musicbrainz.org/artist/8ba1136a-f231-474c-9084-7f9c36172ad2',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					'label' => 'Les Rois de la Suède',
					'model' => 'Project_Library_Contribution',
					'contributor' => [
						'model' => 'Project_Library_Contributor',
						'id' => 300,
						'label' => 'Les Rois de la Suède'
					],
					'roles' => [['model' => 'Project_Library_ContributionType', 'id' => 200, 'label' => 'Auteur']]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_PublicationDateWithMonth()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/release/d8fa9ded-3fab-4c7f-aca8-3212c409dad4');

		$this->expectResolverMethod(self::exactly(2), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::once(), 'resolveContributionTypeInfos')->withConsecutive(
			['Auteur']
		)->will(self::onConsecutiveCalls(
			$this->getContributionTypeInfos('Auteur', 200)
		));
		$this->expectResolverMethod(self::once(), 'resolveContributorInfos')
			->with('Patrick Bruel', 'https://musicbrainz.org/artist/bbd476fe-2987-4e5a-a278-ce5d5f3a6b9c')
			->willReturn($this->getContributorInfos('Patrick Bruel', 300));
		$this->expectResolverMethod(self::exactly(1), 'resolvePublisherInfos')->with('RCA')->willReturn($this->getPublisherInfos('RCA', 400));
		$this->expectResolverMethod(self::exactly(1), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 500));
		$this->expectResolverMethod(self::once(), 'resolveValueFromCollection')
			->with('Project_Library_Languages', 'French')->willReturn('fr');
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['musique'], ['support_audio'])
			->will(self::onConsecutiveCalls(
				$this->getNewReadonlyDocument('Rbs_Generic_Typology', 600), $this->getNewReadonlyDocument('Rbs_Generic_Typology', 601)
			));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'work' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'typology' => 600,
				'label' => 'De face',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://musicbrainz.org/release/d8fa9ded-3fab-4c7f-aca8-3212c409dad4',
					'label' => 'MusicBrainz',
					'model' => 'Project_Library_WorkLink'
				]],
				'originalLanguage' => 'fr'
			],
			'publication' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'oneShot' => true,
				'year' => 2000,
				'description' => 'CD 1

1. Regarde devant toi
2. Tout l\'monde peut s\'tromper
3. J\'roule vers toi
4. L\'Appart\'
5. Rêve à part
6. De face
7. Ça fait des ordres
8. Musique vieille
9. J\'ai l\'beguin pour elle (duo)
10. Non, j\'veux pas (remix)
11. Comment ça va pour vous ? (remix)
12. Marre de cette nana-là',
				'typology' => 601,
				'attributes' => [
					'detail_format' => 'CD',
					'duree_totale' => '47:20',
					'code_barre' => '743217729929',
					'type_album' => 'Album',
					'id_amazon' => 'FR: B00004W4P0',
					'numero_catalogue' => '74321772992',
					'type_support_audio' => 'CD'
				],
				'nationalities' => [[
					'model' => 'Rbs_Geo_Country',
					'id' => 500,
					'label' => 'France'
				]],
				'model' => 'Project_Library_Publication',
				'publishers' => [[
					'model' => 'Project_Library_Publisher',
					'id' => 400,
					'label' => 'RCA'
				]]
			],
			'contributions' => [[
				'link' => [
					'url' => 'https://musicbrainz.org/artist/bbd476fe-2987-4e5a-a278-ce5d5f3a6b9c',
					'label' => 'MusicBrainz',
					'model' => 'Project_Library_ContributorLink',
				],
				'label' => 'Patrick Bruel',
				'model' => 'Project_Library_Contribution',
				'roles' => [[
					'model' => 'Project_Library_ContributionType',
					'id' => 200,
					'label' => 'Auteur'
				]],
				'contributor' => [
					'model' => 'Project_Library_Contributor',
					'id' => 300,
					'label' => 'Patrick Bruel'
				]
			]]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}