<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\MusicBrainzPersonParserTest
 */
class MusicBrainzPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\MusicBrainzPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\MusicBrainzPersonParser::class, $url);
	}

	public function testParser_Person()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/artist/5a6e934b-a203-456c-812e-6c5c3efd673e');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('Person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'birthDate' => '1983-05-13T00:00:00+00:00',
				'birthYear' => 1983,
				'deathDate' => '2007-04-30T00:00:00+00:00',
				'deathYear' => 2007,
				'typology' => 900,
				'attributes' => ['sex' => 'h'],
				'label' => 'Grégory Lemarchal',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'France']],
				'links' => [
					[
						'url' => 'https://musicbrainz.org/artist/5a6e934b-a203-456c-812e-6c5c3efd673e',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://gregorylemarchal.artistes.universalmusic.fr/',
						'label' => 'Site officiel',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://www.just-gregory.net/',
						'label' => 'Site officiel',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'https://www.imdb.com/name/nm1747757/',
						'label' => 'IMDB',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_Group()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/artist/3c18062e-27bd-4532-8a91-9889477a3533');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('France')->willReturn($this->getCountryInfos('France', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('Group')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'birthYear' => 1990,
				'typology' => 900,
				'label' => 'Blankass',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'France']],
				'links' => [
					[
						'url' => 'https://musicbrainz.org/artist/3c18062e-27bd-4532-8a91-9889477a3533',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'https://www.blankass.com/',
						'label' => 'Site officiel',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_MultipleAreaAndDisambiguationSuffix()
	{
		$parser = $this->getNewParser('https://musicbrainz.org/artist/505604ee-556c-402a-b5eb-0441fd153037');

		$this->expectResolverMethod(self::once(), 'resolveCountryInfos')->with('United States')->willReturn($this->getCountryInfos('USA', 400));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('Person')
			->willReturn($this->getNewReadonlyDocument('Rbs_Generic_Typology', 900));
		$this->expectResolverMethod(self::once(), 'resolveWebsiteId')->with('musicbrainz.org')->willReturn(100);
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(static function ($label) {
			if ($label === 'musicbrainz.org')
			{
				return 'MusicBrainz';
			}
			if ($label === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'model' => 'Project_Library_Contributor',
				'birthYear' => 1974,
				'typology' => 900,
				'attributes' => ['sex' => 'h'],
				'label' => 'James Scott Levine',
				'nationalities' => [['model' => 'Rbs_Geo_Country', 'id' => 400, 'label' => 'USA']],
				'links' => [
					[
						'url' => 'https://musicbrainz.org/artist/505604ee-556c-402a-b5eb-0441fd153037',
						'label' => 'MusicBrainz',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://www.jameslevinemusic.com/',
						'label' => 'Site officiel',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'https://www.imdb.com/name/nm0505828/',
						'label' => 'IMDB',
						'model' => 'Project_Library_ContributorLink'
					]
				],
				'aliases' => [[
					'forWebsite' => 100,
					'label' => 'James Scott Levine (film and television composer)',
					'model' => 'Project_Library_ContributorAlias'
				]]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}