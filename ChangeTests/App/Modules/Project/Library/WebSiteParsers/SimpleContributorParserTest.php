<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\SimpleContributorParserTest
 */
class SimpleContributorParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\SimpleContributorParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\SimpleContributorParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.discogs.com/artist/104712-Renaud');

		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.discogs.com')->willReturn('Discogs');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'links' => [[
					'label' => 'Discogs',
					'url' => 'https://www.discogs.com/artist/104712-Renaud',
					'model' => 'Project_Library_ContributorLink'
				]],
				'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_SpecialCharInUrl()
	{
		$parser = $this->getNewParser('https://www.bedetheque.com/auteur-19642-BD-Talajić-Dalibor.html');

		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.bedetheque.com')->willReturn('BD Gest\'');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'contributor' => [
				'links' => [[
					'label' => 'BD Gest\'',
					'url' => 'https://www.bedetheque.com/auteur-19642-BD-Talaji%C4%87-Dalibor.html',
					'model' => 'Project_Library_ContributorLink'
				]],
				'model' => 'Project_Library_Contributor'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}