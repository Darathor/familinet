<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\SimpleWorkParserTest
 */
class SimpleWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\SimpleWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\SimpleWorkParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.discogs.com/Renaud-Amoureux-De-Paname-/release/755074');

		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.discogs.com')->willReturn('Discogs');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'links' => [[
					'label' => 'Discogs',
					'url' => 'https://www.discogs.com/Renaud-Amoureux-De-Paname-/release/755074',
					'model' => 'Project_Library_WorkLink'
				]],
				'model' => 'Project_Library_Work'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}