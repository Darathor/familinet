<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\SteamWorkParserTest
 */
class SteamWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\SteamWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\SteamWorkParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://store.steampowered.com/app/228280/Baldurs_Gate_Enhanced_Edition/');

		$typology1 = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 111);
		$typology2 = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 112);

		$this->expectResolverMethod(self::exactly(2), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('Terminé', 100));
		$this->expectResolverMethod(self::exactly(2), 'resolveTypology')->withConsecutive(['jeu_video'], ['support_logiciel'])
			->will(self::onConsecutiveCalls($typology1, $typology2));
		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->with('store.steampowered.com')->willReturn('Steam');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'year' => 2013,
				'endYear' => 2013,
				'typology' => 111,
				'label' => 'Baldur\'s Gate: Enhanced Edition',
				'model' => 'Project_Library_Work',
				'links' => [[
					'url' => 'https://store.steampowered.com/app/228280/Baldurs_Gate_Enhanced_Edition/',
					'label' => 'Steam',
					'model' => 'Project_Library_WorkLink'
				]]
			],
			'publication' => [
				'status' => [
					'model' => 'Project_Library_WorkStatus',
					'id' => 100,
					'label' => 'Terminé'
				],
				'oneShot' => true,
				'year' => 2013,
				'endYear' => 2013,
				'typology' => 112,
				'attributes' => ['type_support_logiciel' => 'Numérique'],
				'model' => 'Project_Library_Publication'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}