<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\TricTracGameParserTest
 */
class TricTracGameParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\TricTracGameParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\TricTracGameParser::class, $url);
	}

	public function testParser()
	{
		$parser = $this->getNewParser('https://www.trictrac.net/jeu-de-societe/carcassonne-safari');

		$typology = $this->getNewReadonlyDocument('Rbs_Generic_Typology', 100);

		$this->expectResolverMethod(self::once(), 'resolveWorkStatusData')->with('ended')->willReturn($this->getWorkStatusInfos('ended', 10));
		$this->expectResolverMethod(self::once(), 'resolveTypology')->with('jeu_societe')->willReturn($typology);
		$this->expectResolverMethod(self::once(), 'resolveWebsiteLabel')->with('www.trictrac.net')->willReturn('TricTrac');
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'links' => [[
					'label' => 'TricTrac',
					'url' => 'https://www.trictrac.net/jeu-de-societe/carcassonne-safari',
					'model' => 'Project_Library_WorkLink'
				]],
				'label' => 'Carcassonne Safari',
				'typology' => 100,
				'status' => ['model' => 'Project_Library_WorkStatus', 'id' => 10, 'label' => 'ended'],
				'model' => 'Project_Library_Work'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}
}