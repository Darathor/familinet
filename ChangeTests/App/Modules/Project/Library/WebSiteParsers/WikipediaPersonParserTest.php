<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\WikipediaPersonParserTest
 */
class WikipediaPersonParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\WikipediaPersonParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\WikipediaPersonParser::class, $url);
	}

	public function testParser_FR()
	{
		$parser = $this->getNewParser('https://fr.wikipedia.org/wiki/Katherine_Kurtz');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			if ($host === 'catalogue.bnf.fr')
			{
				return 'BnF';
			}
			if ($host === 'data.bnf.fr')
			{
				return 'BnF - data';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'label' => 'Katherine Kurtz',
				'model' => 'Project_Library_Contributor',
				'links' => [
					[
						'url' => 'https://fr.wikipedia.org/wiki/Katherine_Kurtz',
						'label' => 'Wikipédia',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'https://en.wikipedia.org/wiki/Katherine_Kurtz',
						'label' => 'Wikipédia EN',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://catalogue.bnf.fr/ark:/12148/cb122185965',
						'label' => 'BnF',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'http://data.bnf.fr/ark:/12148/cb122185965',
						'label' => 'BnF - data',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_EN()
	{
		$parser = $this->getNewParser('https://en.wikipedia.org/wiki/Katherine_Kurtz');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.
		$expected = [
			'contributor' => [
				'label' => 'Katherine Kurtz',
				'model' => 'Project_Library_Contributor',
				'links' => [
					[
						'url' => 'https://en.wikipedia.org/wiki/Katherine_Kurtz',
						'label' => 'Wikipédia EN',
						'model' => 'Project_Library_ContributorLink'
					],
					[
						'url' => 'https://fr.wikipedia.org/wiki/Katherine_Kurtz',
						'label' => 'Wikipédia',
						'model' => 'Project_Library_ContributorLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_ExternalLinks()
	{
		$parser = $this->getNewParser('https://fr.wikipedia.org/wiki/Robert_Downey_Jr.');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			if ($host === 'www.allocine.fr')
			{
				return 'Allociné';
			}
			if ($host === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			[
				'url' => 'https://fr.wikipedia.org/wiki/Robert_Downey_Jr.',
				'label' => 'Wikipédia',
				'model' => 'Project_Library_ContributorLink'
			],
			[
				'url' => 'https://en.wikipedia.org/wiki/Robert_Downey_Jr.',
				'label' => 'Wikipédia EN',
				'model' => 'Project_Library_ContributorLink'
			],
			[
				'url' => 'http://www.allocine.fr/personne/fichepersonne_gen_cpersonne=10976.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_ContributorLink'
			],
			[
				'url' => 'https://www.imdb.com/name/nm0000375/',
				'label' => 'IMDB',
				'model' => 'Project_Library_ContributorLink'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize()['contributor']['links']);
	}
}