<?php
/** @noinspection DuplicatedCode */
/** @noinspection UnnecessaryAssertionInspection */
namespace ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models;

/**
 * @name \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Models\WikipediaWorkParserTest
 */
class WikipediaWorkParserTest extends \ChangeTests\App\Modules\Project\Library\WebSiteParsers\Assets\ParserTestBase
{
	/**
	 * @param string $url
	 * @return \Project\Library\WebSiteParsers\WikipediaWorkParser
	 */
	protected function getNewParser(string $url)
	{
		return $this->buildParser(\Project\Library\WebSiteParsers\WikipediaWorkParser::class, $url);
	}

	public function testParser_FR()
	{
		$parser = $this->getNewParser('https://fr.wikipedia.org/wiki/Fullmetal_Alchemist');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'model' => 'Project_Library_Work',
				'label' => 'Fullmetal Alchemist',
				'links' => [
					[
						'url' => 'https://fr.wikipedia.org/wiki/Fullmetal_Alchemist',
						'label' => 'Wikipédia',
						'model' => 'Project_Library_WorkLink'
					],
					[
						'url' => 'https://en.wikipedia.org/wiki/Fullmetal_Alchemist',
						'label' => 'Wikipédia EN',
						'model' => 'Project_Library_WorkLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_EN()
	{
		$parser = $this->getNewParser('https://en.wikipedia.org/wiki/Fullmetal_Alchemist');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			'work' => [
				'model' => 'Project_Library_Work',
				'label' => 'Fullmetal Alchemist',
				'links' => [
					[
						'url' => 'https://en.wikipedia.org/wiki/Fullmetal_Alchemist',
						'label' => 'Wikipédia EN',
						'model' => 'Project_Library_WorkLink'
					],
					[
						'url' => 'https://fr.wikipedia.org/wiki/Fullmetal_Alchemist',
						'label' => 'Wikipédia',
						'model' => 'Project_Library_WorkLink'
					]
				]
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize());
	}

	public function testParser_ExternalLinks()
	{
		$parser = $this->getNewParser('https://fr.wikipedia.org/wiki/Logan_(film)');

		$this->expectResolverMethod(self::any(), 'resolveWebsiteLabel')->willReturnCallback(function ($host) {
			if ($host === 'fr.wikipedia.org')
			{
				return 'Wikipédia';
			}
			if ($host === 'en.wikipedia.org')
			{
				return 'Wikipédia EN';
			}
			if ($host === 'www.allocine.fr')
			{
				return 'Allociné';
			}
			if ($host === 'www.imdb.com')
			{
				return 'IMDB';
			}
			return null;
		});
		$this->applyResolverDefault();

		// -- Tests.

		$expected = [
			[
				'url' => 'https://fr.wikipedia.org/wiki/Logan_(film)',
				'label' => 'Wikipédia',
				'model' => 'Project_Library_WorkLink'
			],
			[
				'url' => 'https://en.wikipedia.org/wiki/Logan_(film)',
				'label' => 'Wikipédia EN',
				'model' => 'Project_Library_WorkLink'
			],
			[
				'url' => 'http://www.allocine.fr/film/fichefilm_gen_cfilm=225116.html',
				'label' => 'Allociné',
				'model' => 'Project_Library_WorkLink'
			],
			[
				'url' => 'https://www.imdb.com/title/tt3315342/',
				'label' => 'IMDB',
				'model' => 'Project_Library_WorkLink'
			]
		];

		self::assertEquals($expected, $parser->parseDataAndNormalize()['work']['links']);
	}
}