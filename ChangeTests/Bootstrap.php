<?php
/**
 * Set error reporting to the level to which Zend Framework code must comply.
 */
\error_reporting( \E_ALL | \E_STRICT );
\ini_set('memory_limit', '-1');
\define('PROJECT_HOME', \dirname(\realpath(__DIR__)));
require_once \PROJECT_HOME . '/Change/Application.php';
require_once \PROJECT_HOME . '/ChangeTests/Change/TestAssets/Application.php';
$projectAutogenFileName = PROJECT_HOME . '/ChangeTests/UnitTestWorkspace/App/Config/project.autogen.json';
if(\file_exists($projectAutogenFileName))
{
	\unlink($projectAutogenFileName);
}
\date_default_timezone_set('UTC');
$application = new \ChangeTests\Change\TestAssets\Application();
$application->useSession(false);
$application->registerAutoload();
$application->clearCache();
$pluginManager = new \Change\Plugins\PluginManager();
$pluginManager->setApplication($application);
$pluginManager->unsetPluginConfig();
$pluginManager->compile(false);