<?php
namespace ChangeTests\Change;

/**
 */
class ApplicationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @runInSeparateProcess
	 */
	public function testRegisterAutoload()
	{
		if (!\defined('PROJECT_HOME'))
		{
			\define('PROJECT_HOME', \dirname(\realpath(__DIR__), 2));
		}
		require_once PROJECT_HOME . '/Change/Application.php';
		$application = new \Change\Application();
		$application->registerAutoload();
		self::assertTrue(\class_exists(\Zend\Stdlib\ErrorHandler::class));
		self::assertTrue(\class_exists(\Change\Stdlib\FileUtils::class));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testStart()
	{
		if (!\defined('PROJECT_HOME'))
		{
			\define('PROJECT_HOME', \dirname(\realpath(__DIR__), 2));
		}
		require_once \PROJECT_HOME . '/Change/Application.php';
		require_once 'TestAssets/Application.php';

		$application = new \ChangeTests\Change\TestAssets\Application();
		$application->registerAutoload();
		$application->start();

		self::assertTrue(\class_exists(\Zend\Stdlib\ErrorHandler::class));
		self::assertTrue(\class_exists(\ZendOAuth\OAuth::class));
		self::assertTrue(\class_exists(\Change\Stdlib\FileUtils::class));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testGetProfile()
	{
		if (!\defined('PROJECT_HOME'))
		{
			\define('PROJECT_HOME', \dirname(\realpath(__DIR__), 2));
		}
		require_once \PROJECT_HOME . '/Change/Application.php';
		require_once 'TestAssets/Application.php';

		$application = new \ChangeTests\Change\TestAssets\Application();
		$application->registerAutoload();
		self::assertInstanceOf(\Zend\Stdlib\Parameters::class, $application->getContext());
	}

	public function testGetConfiguration()
	{
		$app = new \ChangeTests\Change\TestAssets\Application();
		self::assertInstanceOf(\Change\Configuration\Configuration::class, $app->getConfiguration());
	}

	public function testGetWorkspace()
	{
		$app = new \ChangeTests\Change\TestAssets\Application();
		self::assertInstanceOf(\Change\Workspace::class, $app->getWorkspace());
	}
}