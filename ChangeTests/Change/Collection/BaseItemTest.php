<?php
namespace ChangeTests\Change\Collection;

use Change\Collection\BaseItem;

/**
 * @name \ChangeTests\Change\Collection\BaseItemTest
 */
class BaseItemTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testAll()
	{
		// Basic cases...

		$item = new BaseItem('aa');
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('aa', $item->getLabel());
		self::assertEquals('aa', $item->getTitle());

		$item = new BaseItem('aa', 'test');
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('test', $item->getLabel());
		self::assertEquals('test', $item->getTitle());

		$item = new BaseItem('aa', 'test', 'toto');
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('test', $item->getLabel());
		self::assertEquals('toto', $item->getTitle());

		// To string conversions...

		$item = new BaseItem('aa', new StringableObject1());
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('Change', $item->getLabel());
		self::assertEquals('Change', $item->getTitle());

		$item = new BaseItem('aa', new StringableObject1(), new StringableObject2());
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('Change', $item->getLabel());
		self::assertEquals('Chuck Norris', $item->getTitle());

		// Giving an array...

		$item = new BaseItem('aa', array('Youpi', new StringableObject1()));
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('Youpi', $item->getLabel());
		self::assertEquals('Change', $item->getTitle());

		$item = new BaseItem('aa', array('label' => new StringableObject1(), 'title' => 'Hello'));
		self::assertEquals('aa', $item->getValue());
		self::assertEquals('Change', $item->getLabel());
		self::assertEquals('Hello', $item->getTitle());
	}
}

class StringableObject1
{
	public function __toString()
	{
		return 'Change';
	}
}

class StringableObject2
{
	public function __toString()
	{
		return 'Chuck Norris';
	}
}