<?php
namespace ChangeTests\Change\Collection;

use Change\Collection\CollectionArray;

/**
 * @name \ChangeTests\Change\Collection\CollectionArrayTest
 */
class CollectionArrayTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testAll()
	{
		// Basic cases...
		$collection = new CollectionArray('Test_Collection', ['toto' => 'totoLabel', 'titi' => 'titiLabel']);
		self::assertEquals('Test_Collection', $collection->getCode());
		self::assertCount(2, $collection->getItems());
		$item1 = $collection->getItemByValue('toto');
		self::assertEquals('toto', $item1->getValue());
		self::assertEquals('totoLabel', $item1->getLabel());
		self::assertEquals('totoLabel', $item1->getTitle());
		$item2 = $collection->getItemByValue('titi');
		self::assertEquals('titi', $item2->getValue());
		self::assertEquals('titiLabel', $item2->getLabel());
		self::assertEquals('titiLabel', $item2->getTitle());

		// addItem()

		// If there is no item for this value, the item is added.
		$collection->addItem('tete', 'teteLabel');
		self::assertCount(3, $collection->getItems());
		$item3 = $collection->getItemByValue('tete');
		self::assertEquals('tete', $item3->getValue());
		self::assertEquals('teteLabel', $item3->getLabel());
		self::assertEquals('teteLabel', $item3->getTitle());

		// If there is an item for this value, the item is replaced.
		$collection->addItem('titi', 'youpiÇaMarche');
		self::assertCount(3, $collection->getItems());
		$item3 = $collection->getItemByValue('titi');
		self::assertEquals('titi', $item3->getValue());
		self::assertEquals('youpiÇaMarche', $item3->getLabel());
		self::assertEquals('youpiÇaMarche', $item3->getTitle());

		// removeItemByValue()

		// If there is an item for this value, the item is removed.
		$collection->removeItemByValue('toto');
		self::assertCount(2, $collection->getItems());
		$item4 = $collection->getItemByValue('toto');
		self::assertNull($item4);

		// If there is no item for this value, nothing appends.
		$collection->removeItemByValue('notExistingItem');
		self::assertCount(2, $collection->getItems());
	}
}