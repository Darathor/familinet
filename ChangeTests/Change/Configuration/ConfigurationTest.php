<?php
namespace ChangeTests\Change\Configuration;

class ConfigurationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public $listenerCalled = false;

	/**
	 * @param \PHPUnit\Framework\TestResult|null $result
	 * @return \PHPUnit\Framework\TestResult
	 */
	public function run(?\PHPUnit\Framework\TestResult $result = null): \PHPUnit\Framework\TestResult
	{
		$this->setPreserveGlobalState(false);
		return parent::run($result);
	}

	public function testConfigGetters()
	{
		$config = new \Change\Configuration\Configuration($this->getApplication()->getProjectConfigurationPaths());
		$entries = ['key1' => 'value1', 'key2' => 'value2', 'key3' => '6',
			'2levels' => ['sub-key1' => 'toto', 'sub-key2' => 'titi'],
			'booleans' => ['v1' => 'true', 'v2' => 'false', 'v3' => 'toto', 'v4' => 'TRUE', 'v5' => '1']];
		$config->setConfigArray($entries);
		self::assertEquals($entries, $config->getConfigArray());
		return $config;
	}

	/**
	 * @depends testConfigGetters
	 * @param \Change\Configuration\Configuration $config
	 */
	public function testHasEntry(\Change\Configuration\Configuration $config)
	{
		// Existing keys.
		self::assertTrue($config->hasEntry('key1'));
		self::assertTrue($config->hasEntry('2levels/sub-key1'));

		// Nonexistent keys.
		self::assertFalse($config->hasEntry('nonexistentKey'));
		self::assertFalse($config->hasEntry('2levels/nonexistentKey'));
	}

	/**
	 * @depends testConfigGetters
	 * @param \Change\Configuration\Configuration $config
	 */
	public function testGetEntry(\Change\Configuration\Configuration $config)
	{
		// Existing keys.
		self::assertEquals('value1', $config->getEntry('key1'));
		self::assertEquals(6, $config->getEntry('key3'));
		self::assertEquals('toto', $config->getEntry('2levels/sub-key1'));

		// Nonexistent keys.
		self::assertEquals(null, $config->getEntry('nonexistentKey'));
		self::assertEquals('toto', $config->getEntry('nonexistentKey', 'toto'));
		self::assertEquals(null, $config->getEntry('2levels/nonexistentKey'));
		self::assertEquals('toto', $config->getEntry('2levels/nonexistentKey', 'toto'));
	}

	/**
	 * @depends testConfigGetters
	 */
	public function testAddVolatileEntry()
	{
		$config = new \Change\Configuration\Configuration($this->getApplication()->getProjectConfigurationPaths());
		$entries = ['key1' => 'value1', 'key2' => 'value2',
			'complexEntry1' => ['entry11' => 'Test11', 'entry12' => 'Test12'],
			'complexEntry2' => [
				'contents' => ['entry21' => 'Test21', 'entry22' => 'Test22',
					'entry23' => ['entry231' => 'Test231', 'entry232' => 'Test232']]]];
		$config->setConfigArray($entries);

		// Entries with path shorter than 2 are ignored.
		self::assertEquals('value1', $config->getEntry('key1'));
		self::assertEquals('value2', $config->getEntry('key2'));
		self::assertEquals(null, $config->getEntry('key3'));

		self::assertFalse($config->addVolatileEntry('key1', 'newValue1'));
		self::assertFalse($config->addVolatileEntry('key3', 'newValue3'));
		self::assertEquals('value1', $config->getEntry('key1'));
		self::assertEquals('value2', $config->getEntry('key2'));
		self::assertEquals(null, $config->getEntry('key3'));

		// Simple entries updated.
		self::assertEquals('Test11', $config->getEntry('complexEntry1/entry11'));
		self::assertEquals('Test12', $config->getEntry('complexEntry1/entry12'));
		self::assertEquals(null, $config->getEntry('complexEntry1/entry13'));

		self::assertTrue($config->addVolatileEntry('complexEntry1/entry11', 'newValue1'));
		self::assertTrue($config->addVolatileEntry('complexEntry1/entry13', 'newValue3'));
		self::assertEquals('newValue1', $config->getEntry('complexEntry1/entry11'));
		self::assertEquals('Test12', $config->getEntry('complexEntry1/entry12'));
		self::assertEquals('newValue3', $config->getEntry('complexEntry1/entry13'));

		// New complex entries are correctly added.
		self::assertEquals(null, $config->getEntry('complexEntry3'));
		self::assertEquals(null, $config->getEntry('complexEntry3/contents'));
		self::assertEquals(null, $config->getEntry('complexEntry3/contents/entry31'));
		self::assertEquals(null, $config->getEntry('complexEntry3/contents/entry33'));

		$entry3 = ['entry31' => 'newValue31', 'entry32' => 'newValue32'];
		self::assertTrue($config->addVolatileEntry('complexEntry3/contents', $entry3));
		self::assertEquals(['contents' => $entry3], $config->getEntry('complexEntry3'));
		self::assertEquals($entry3, $config->getEntry('complexEntry3/contents'));
		self::assertEquals('newValue31', $config->getEntry('complexEntry3/contents/entry31'));
		self::assertEquals(null, $config->getEntry('complexEntry3/contents/entry33'));

		// Existing complex entries are merged recursively.
		self::assertEquals('Test21', $config->getEntry('complexEntry2/contents/entry21'));
		self::assertEquals('Test22', $config->getEntry('complexEntry2/contents/entry22'));
		self::assertEquals('Test231', $config->getEntry('complexEntry2/contents/entry23/entry231'));
		self::assertEquals('Test232', $config->getEntry('complexEntry2/contents/entry23/entry232'));
		self::assertEquals(null, $config->getEntry('complexEntry2/contents/entry23/entry233'));
		self::assertEquals(null, $config->getEntry('complexEntry2/contents/entry24'));
		self::assertEquals(null, $config->getEntry('complexEntry2/contents/entry25'));

		self::assertTrue($config->addVolatileEntry('complexEntry2/contents', ['entry21' => 'newValue21',
			'entry23' => ['entry231' => 'newValue231', 'entry233' => 'newValue233'], 'entry24' => 'newValue24']));
		self::assertEquals('newValue21', $config->getEntry('complexEntry2/contents/entry21'));
		self::assertEquals('Test22', $config->getEntry('complexEntry2/contents/entry22'));
		self::assertEquals('newValue231', $config->getEntry('complexEntry2/contents/entry23/entry231'));
		self::assertEquals('Test232', $config->getEntry('complexEntry2/contents/entry23/entry232'));
		self::assertEquals('newValue233', $config->getEntry('complexEntry2/contents/entry23/entry233'));
		self::assertEquals('newValue24', $config->getEntry('complexEntry2/contents/entry24'));
		self::assertEquals(null, $config->getEntry('complexEntry2/contents/entry25'));
	}
}