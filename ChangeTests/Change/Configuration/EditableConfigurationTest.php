<?php
namespace ChangeTests\Change\Configuration;

use Change\Configuration\EditableConfiguration;

class EditableConfigurationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testAddPersistentEntry()
	{
		$config = new EditableConfiguration(array());
		try
		{
			self::assertTrue($config->addPersistentEntry('mypath/myentry', 'value'));
			self::fail('Configuration not found: ' . EditableConfiguration::AUTOGEN);
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals(30000, $e->getCode());
		}

		$sourceConfigFile = sys_get_temp_dir() . "/testAddPersistentEntry_project1.json";
		if (file_exists($sourceConfigFile))
		{
			unlink($sourceConfigFile);
		}

		copy(__DIR__ . '/TestAssets/project1.json', $sourceConfigFile);
		$config = new EditableConfiguration(array(EditableConfiguration::AUTOGEN => $sourceConfigFile));
		self::assertNull($config->getEntry('mypath/myentry'));
		self::assertTrue($config->addPersistentEntry('mypath/myentry', 'value'));
		self::assertEquals('value', $config->getEntry('mypath/myentry'));

		self::assertCount(1, $config->getUpdateEntries());

		$config->save();

		$newConfig = new  \Change\Configuration\EditableConfiguration(array(EditableConfiguration::AUTOGEN =>  $sourceConfigFile));
		self::assertEquals('value', $newConfig->getEntry('mypath/myentry'));

		// Giving an invalid path just returns false.
		self::assertNull($newConfig->getEntry('invalidpath'));
		self::assertFalse($config->addPersistentEntry('invalidpath', 'value'));
		self::assertCount(0, $config->getUpdateEntries());


		$newConfig = new  \Change\Configuration\EditableConfiguration(array(EditableConfiguration::AUTOGEN =>  $sourceConfigFile));
		self::assertNull($newConfig->getEntry('invalidpath'));

		// Boolean and integer types values are correctly preserved.
		self::assertNull($newConfig->getEntry('mypath/integer'));
		self::assertNull($newConfig->getEntry('mypath/boolean'));
		self::assertTrue($config->addPersistentEntry('mypath/integer', 155));
		self::assertTrue($config->addPersistentEntry('mypath/boolean', true));
		self::assertCount(2, $config->getUpdateEntries());
		$config->save();

		$newConfig = new  \Change\Configuration\EditableConfiguration(array(EditableConfiguration::AUTOGEN => $sourceConfigFile));
		self::assertTrue(155 === $newConfig->getEntry('mypath/integer'));
		self::assertTrue(true === $newConfig->getEntry('mypath/boolean'));


		if (file_exists($sourceConfigFile))
		{
			unlink($sourceConfigFile);
		}
	}
}