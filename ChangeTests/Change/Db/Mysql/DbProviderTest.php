<?php
namespace ChangeTests\Change\Db\Mysql;

use Change\Db\Mysql\DbProvider;
use Change\Db\ScalarType;

class DbProviderTest extends \ChangeTests\Change\TestAssets\TestCase
{
	protected function setUp(): void
	{
		if (!\in_array('mysql', \PDO::getAvailableDrivers()))
		{
			$this->markTestSkipped('PDO Mysql is not installed.');
		}

		$provider = $this->getApplicationServices()->getDbProvider();
		if (!($provider instanceof DbProvider))
		{
			$this->markTestSkipped('The Mysql DbProvider is not configured.');
		}
		$connectionInfos = $provider->getConnectionInfos();
		if (!isset($connectionInfos['database']))
		{
			$this->markTestSkipped('The Mysql database not defined!');
		}
	}

	public function testGetConnectionWithURL()
	{
		$provider = $this->getApplicationServices()->getDbProvider();
		$infos = $provider->getConnectionInfos();

		$provider->setConnectionInfos(['url' => 'mysql://']);
		try
		{
			$pdo = $provider->getDriver();
			self::fail('Invalid RuntimeException');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Database URL is not valid', $e->getMessage());
		}

		$provider->setConnectionInfos(['url' => 'ENV:MYSQL_TEST_URL']);
		try
		{
			$pdo = $provider->getDriver();
			self::fail('Invalid RuntimeException');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringEndsWith('is not set.', $e->getMessage());
		}

		putenv('MYSQL_TEST_URL=mysql://' .
			($infos['user'] ?? '') . ':' .
			($infos['password'] ?? '') . '@' .
			($infos['host'] ?? 'localhost') . ':' .
			($infos['port'] ?? '3306') . '/' .
			$infos['database']);
		$pdo = $provider->getDriver();
		self::assertNotNull($pdo);
	}

	public function testGetInstance()
	{
		$provider = $this->getApplicationServices()->getDbProvider();

		/* @var $provider \Change\Db\Mysql\DbProvider */
		self::assertInstanceOf(DbProvider::class, $provider);

		self::assertEquals('mysql', $provider->getType());

		$pdo = $provider->getDriver();
		try
		{
			$pdo->exec('INVALID SQL');
			self::fail('Invalid PDO Exception');
		}
		catch (\PDOException $e)
		{
			self::assertStringStartsWith('SQLSTATE', $e->getMessage());
			self::assertEquals('42000', $e->getCode());
		}
		$provider->closeConnection();
	}

	public function testTransaction()
	{
		/** @var $provider DbProvider */
		$provider = $this->getApplicationServices()->getDbProvider();

		$event = new \Change\Events\Event('tm', $this, ['primary' => true]);

		self::assertFalse($provider->inTransaction());

		$provider->beginTransaction();

		self::assertTrue($provider->inTransaction());

		$provider->commit($event);

		self::assertFalse($provider->inTransaction());

		$provider->beginTransaction($event);

		self::assertTrue($provider->inTransaction());

		$provider->rollBack($event);

		self::assertFalse($provider->inTransaction());

		$event->setParam('primary', false);

		$provider->beginTransaction($event);
		self::assertFalse($provider->inTransaction());

		$provider->closeConnection();
	}

	public function testValues()
	{
		/** @var $provider DbProvider */
		$provider = $this->getApplicationServices()->getDbProvider();

		self::assertSame(1, $provider->phpToDB(true, ScalarType::BOOLEAN));
		self::assertTrue($provider->dbToPhp('1', ScalarType::BOOLEAN));
		self::assertSame(0, $provider->phpToDB(false, ScalarType::BOOLEAN));
		self::assertFalse($provider->dbToPhp('0', ScalarType::BOOLEAN));

		$dt = new \DateTime('2019-12-28T15:04:41+0000', new \DateTimeZone('UTC'));
		$dbVal = $dt->format('Y-m-d H:i:s');

		self::assertSame($dbVal, $provider->phpToDB($dt, ScalarType::DATETIME));
		self::assertEquals($dt, $provider->dbToPhp($dbVal, ScalarType::DATETIME));

		$provider->closeConnection();
	}

	public function testGetLastInsertId()
	{
		/** @var $provider DbProvider */
		$provider = $this->getApplicationServices()->getDbProvider();

		$pdo = $provider->getDriver();
		$pdo->exec('DROP TABLE IF EXISTS `test_auto_number`');
		$pdo->exec('CREATE TABLE `test_auto_number` (`auto` int(11) NOT NULL AUTO_INCREMENT, `test` int(11) NOT NULL, PRIMARY KEY (`auto`)) ENGINE=InnoDB AUTO_INCREMENT=5000');

		$pdo->beginTransaction();
		$pdo->exec('INSERT INTO `test_auto_number` (`auto`, `test`) VALUES (NULL, \'2\')');
		$auto = $provider->getLastInsertId('test_auto_number');
		self::assertEquals(5000, $auto);
		$pdo->commit();

		$provider->closeConnection();
	}
}
