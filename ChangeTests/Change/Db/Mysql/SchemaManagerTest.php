<?php
namespace ChangeTests\Change\Db\Mysql;

use Change\Db\Mysql\DbProvider;
use Change\Db\Schema\FieldDefinition;

class SchemaManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	protected function setUp(): void
	{
		if (!\in_array('mysql', \PDO::getAvailableDrivers(), true))
		{
			$this->markTestSkipped('PDO Mysql is not installed.');
		}

		$provider = $this->getApplicationServices()->getDbProvider();
		if (!($provider instanceof DbProvider))
		{
			$this->markTestSkipped('The Mysql DbProvider is not configured.');
		}

		$connectionInfos = $provider->getConnectionInfos();
		if (!isset($connectionInfos['database']))
		{
			$this->markTestSkipped('The Mysql database not defined!');
		}
	}

	/**
	 * @return \Change\Db\InterfaceSchemaManager
	 */
	public function testGetInstance()
	{
		$provider = $this->getApplicationServices()->getDbProvider();
		$schemaManager = $provider->getSchemaManager();
		self::assertTrue($schemaManager->check());
		self::assertNotNull($schemaManager->getName());

		$schemaManager->clearDB();
		self::assertCount(0, $schemaManager->getTableNames());
		self::assertNull($schemaManager->getTableDefinition('test'));
		return $schemaManager;
	}

	/**
	 * @depends testGetInstance
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function testSystemSchema($schemaManager)
	{
		$systemSchema = $schemaManager->getSystemSchema();
		self::assertInstanceOf(\Change\Db\Schema\SchemaDefinition::class, $systemSchema);
		$systemSchema->generate();
		$tables = $schemaManager->getTableNames();
		self::assertContains('change_document', $tables);
		self::assertContains('change_document_correction', $tables);
		self::assertContains('change_document_deleted', $tables);
		self::assertContains('change_document_metas', $tables);
		self::assertContains('change_path_rule', $tables);
		return $schemaManager;
	}

	/**
	 * @depends testSystemSchema
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function testGetFieldDbOptions($schemaManager)
	{
		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::STRING);
		self::assertArrayHasKey('length', $dbOptions);
		self::assertEquals(255, $dbOptions['length']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::TEXT);
		self::assertArrayHasKey('length', $dbOptions);
		self::assertEquals(16777215, $dbOptions['length']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::DECIMAL);
		self::assertArrayHasKey('precision', $dbOptions);
		self::assertEquals(13, $dbOptions['precision']);
		self::assertArrayHasKey('scale', $dbOptions);
		self::assertEquals(4, $dbOptions['scale']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::BOOLEAN);
		self::assertArrayHasKey('precision', $dbOptions);
		self::assertEquals(3, $dbOptions['precision']);
		self::assertArrayHasKey('scale', $dbOptions);
		self::assertEquals(0, $dbOptions['scale']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::INTEGER);
		self::assertArrayHasKey('precision', $dbOptions);
		self::assertEquals(10, $dbOptions['precision']);
		self::assertArrayHasKey('scale', $dbOptions);
		self::assertEquals(0, $dbOptions['scale']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::DATETIME);
		self::assertCount(0, $dbOptions);

		try
		{
			$schemaManager->getFieldDbOptions(-1);
			self::fail('Invalid Field type: -1');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Invalid Field type:', $e->getMessage());
		}

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::STRING, array('test' => 8), array('length' => 50));
		self::assertArrayHasKey('length', $dbOptions);
		self::assertArrayHasKey('test', $dbOptions);
		self::assertEquals(50, $dbOptions['length']);

		$dbOptions = $schemaManager->getFieldDbOptions(\Change\Db\ScalarType::STRING, array('length' => 8), array('length' => 50));
		self::assertArrayHasKey('length', $dbOptions);
		self::assertEquals(8, $dbOptions['length']);
		return $schemaManager;
	}

	/**
	 * @depends testGetFieldDbOptions
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function testNewTableDefinition($schemaManager)
	{
		$td = $schemaManager->newTableDefinition('test');
		self::assertEquals('test', $td->getName());
		self::assertEquals('InnoDB', $td->getOption('ENGINE'));
		self::assertEquals('utf8', $td->getOption('CHARSET'));
		return $schemaManager;
	}

	/**
	 * @depends testNewTableDefinition
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function testNewField($schemaManager)
	{
		$td = $schemaManager->newTableDefinition('test_types');

		$fd = $schemaManager->newEnumFieldDefinition('enum', array('VALUES' => array('V1', 'V2', 'V3')));
		self::assertEquals('enum', $fd->getName());
		self::assertEquals(FieldDefinition::ENUM, $fd->getType());
		self::assertCount(3, $fd->getOption('VALUES'));
		$td->addField($fd);

		try
		{
			$schemaManager->newEnumFieldDefinition('enum', array());
			self::fail('Invalid Enum values');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Invalid Enum values', $e->getMessage());
		}

		$fd = $schemaManager->newCharFieldDefinition('char');
		self::assertEquals(FieldDefinition::CHAR, $fd->getType());
		self::assertEquals('char', $fd->getName());
		self::assertEquals(255, $fd->getLength());

		$fd = $schemaManager->newCharFieldDefinition('char', array('length' => 10));
		self::assertEquals('char', $fd->getName());
		self::assertEquals(10, $fd->getLength());
		$td->addField($fd);


		$fd = $schemaManager->newVarCharFieldDefinition('varchar');
		self::assertEquals(FieldDefinition::VARCHAR, $fd->getType());
		self::assertEquals('varchar', $fd->getName());
		self::assertEquals(255, $fd->getLength());

		$fd = $schemaManager->newVarCharFieldDefinition('varchar', array('length' => 10));
		self::assertEquals('varchar', $fd->getName());
		self::assertEquals(10, $fd->getLength());
		$td->addField($fd);

		$fd = $schemaManager->newNumericFieldDefinition('numeric');
		self::assertEquals(FieldDefinition::DECIMAL, $fd->getType());
		self::assertEquals('numeric', $fd->getName());
		self::assertEquals(13, $fd->getPrecision());
		self::assertEquals(4, $fd->getScale());
		$td->addField($fd);

		$fd = $schemaManager->newBooleanFieldDefinition('boolean');
		self::assertEquals(FieldDefinition::SMALLINT, $fd->getType());
		self::assertEquals('boolean', $fd->getName());
		self::assertEquals(3, $fd->getPrecision());
		self::assertEquals(0, $fd->getScale());
		self::assertFalse($fd->getNullable());
		self::assertEquals(0, $fd->getDefaultValue());
		$td->addField($fd);

		$fd = $schemaManager->newIntegerFieldDefinition('integer');
		self::assertEquals(FieldDefinition::INTEGER, $fd->getType());
		self::assertEquals('integer', $fd->getName());
		self::assertEquals(10, $fd->getPrecision());
		self::assertEquals(0, $fd->getScale());
		$td->addField($fd);

		$fd = $schemaManager->newFloatFieldDefinition('float');
		self::assertEquals(FieldDefinition::FLOAT, $fd->getType());
		self::assertEquals('float', $fd->getName());
		$td->addField($fd);

		$fd = $schemaManager->newTextFieldDefinition('text');
		self::assertEquals(FieldDefinition::TEXT, $fd->getType());
		self::assertEquals('text', $fd->getName());
		self::assertEquals(16777215, $fd->getLength());
		$td->addField($fd);

		$fd = $schemaManager->newDateFieldDefinition('date');
		self::assertEquals(FieldDefinition::DATE, $fd->getType());
		self::assertEquals('date', $fd->getName());
		$td->addField($fd);

		$fd = $schemaManager->newTimeStampFieldDefinition('timestamp');
		self::assertEquals(FieldDefinition::TIMESTAMP, $fd->getType());
		self::assertEquals('timestamp', $fd->getName());
		self::assertEquals('CURRENT_TIMESTAMP', $fd->getDefaultValue());
		self::assertFalse($fd->getNullable());
		$td->addField($fd);

		$schemaManager->createTable($td);

		return $schemaManager;
	}


	/**
	 * @depends testNewField
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 * @return \Change\Db\Mysql\SchemaManager
	 */
	public function testGetTableDefinition($schemaManager)
	{
		$td = $schemaManager->getTableDefinition('test_types');
		self::assertNotNull($td);
		self::assertTrue($td->isValid());

		$fd = $td->getField('enum');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::ENUM, $fd->getType());
		self::assertCount(3, $fd->getOption('VALUES'));
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('char');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::CHAR, $fd->getType());
		self::assertEquals('char', $fd->getName());
		self::assertEquals(10, $fd->getLength());
		self::assertTrue($fd->getNullable());


		$fd = $td->getField('varchar');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::VARCHAR, $fd->getType());
		self::assertEquals('varchar', $fd->getName());
		self::assertEquals(10, $fd->getLength());
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('numeric');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::DECIMAL, $fd->getType());
		self::assertEquals('numeric', $fd->getName());
		self::assertEquals(13, $fd->getPrecision());
		self::assertEquals(4, $fd->getScale());
		self::assertTrue($fd->getNullable());
		self::assertEquals(0, $fd->getDefaultValue());

		$fd = $td->getField('boolean');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::SMALLINT, $fd->getType());
		self::assertEquals('boolean', $fd->getName());
		self::assertEquals(3, $fd->getPrecision());
		self::assertEquals(0, $fd->getScale());
		self::assertFalse($fd->getNullable());

		$fd = $td->getField('integer');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::INTEGER, $fd->getType());
		self::assertEquals('integer', $fd->getName());
		self::assertEquals(10, $fd->getPrecision());
		self::assertEquals(0, $fd->getScale());
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('float');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::FLOAT, $fd->getType());
		self::assertEquals('float', $fd->getName());
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('text');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::TEXT, $fd->getType());
		self::assertEquals('text', $fd->getName());
		self::assertEquals(16777215, $fd->getLength());
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('date');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::DATE, $fd->getType());
		self::assertEquals('date', $fd->getName());
		self::assertTrue($fd->getNullable());

		$fd = $td->getField('timestamp');
		self::assertNotNull($fd);
		self::assertEquals(FieldDefinition::TIMESTAMP, $fd->getType());
		self::assertEquals('timestamp', $fd->getName());
		self::assertEquals('CURRENT_TIMESTAMP', $fd->getDefaultValue());
		self::assertFalse($fd->getNullable());

		return $schemaManager;
	}

	/**
	 * @depends testGetTableDefinition
	 * @param \Change\Db\Mysql\SchemaManager $schemaManager
	 */
	public function testDropTable($schemaManager)
	{
		$td = $schemaManager->getTableDefinition('test_types');
		$sql = $schemaManager->dropTable($td);
		self::assertEquals('DROP TABLE IF EXISTS `test_types`', $sql);
	}
}
