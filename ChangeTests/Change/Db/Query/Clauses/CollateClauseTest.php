<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\CollateClause;

class CollateClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new CollateClause();
		self::assertEquals('COLLATE', $i->getName());
		self::assertNull($i->getExpression());

		$e = new \Change\Db\Query\Expressions\Raw('raw');
		$i = new CollateClause($e);

		self::assertEquals($e, $i->getExpression());
	}

	public function testExpression()
	{
		$i = new CollateClause();
		$e = new \Change\Db\Query\Expressions\Raw('raw');
		$i->setExpression($e);
		self::assertEquals($e, $i->getExpression());
	}

	public function testToSQL92String()
	{
		$i = new CollateClause();
		try
		{
			$i->toSQL92String();
			self::fail('Expression can not be null');
		}
		catch (\RuntimeException $e)
		{
			self::assertTrue(true);
		}

		$e = new \Change\Db\Query\Expressions\Raw('raw');
		$i->setExpression($e);
		self::assertEquals('COLLATE raw', $i->toSQL92String());
	}
}
