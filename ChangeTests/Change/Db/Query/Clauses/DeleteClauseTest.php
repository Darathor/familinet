<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\DeleteClause;

class DeleteClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new DeleteClause();
		self::assertEquals('DELETE', $i->getName());
	}

	public function testToSQL92String()
	{
		$i = new DeleteClause();
		self::assertEquals("DELETE", $i->toSQL92String());
	}
}
