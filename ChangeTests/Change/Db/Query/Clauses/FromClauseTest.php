<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\FromClause;

class FromClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new FromClause();
		self::assertEquals('FROM', $i->getName());
		self::assertNull($i->getTableExpression());
		self::assertCount(0, $i->getJoins());

		$el = new \Change\Db\Query\Expressions\Raw('table');
		$i = new FromClause($el);

		self::assertEquals($el, $i->getTableExpression());
	}

	public function testJoins()
	{
		$i = new FromClause();
		$jt = new \Change\Db\Query\Expressions\Raw('jointable');
		$j = new \Change\Db\Query\Expressions\Join($jt);

		$i->setJoins([$j]);
		self::assertCount(1, $i->getJoins());

		$ret = $i->addJoin($j);
		self::assertEquals($ret, $i);
		self::assertCount(2, $i->getJoins());
	}

	public function testToSQL92String()
	{
		$i = new FromClause();
		try
		{
			$i->toSQL92String();
			self::fail('throw Exception expected');
		}
		catch (\Exception $e)
		{
			self::assertEquals('TableExpression can not be null', $e->getMessage());
		}

		$i = new FromClause(new \Change\Db\Query\Expressions\Raw('table'));
		self::assertEquals('FROM table', $i->toSQL92String());

		$jt = new \Change\Db\Query\Expressions\Raw('jointable');
		$j = new \Change\Db\Query\Expressions\Join($jt);
		$i->addJoin($j);

		self::assertEquals('FROM table NATURAL CROSS JOIN jointable', $i->toSQL92String());
	}
}
