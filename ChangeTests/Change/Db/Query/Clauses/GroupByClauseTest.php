<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\GroupByClause;

class GroupByClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new GroupByClause();
		self::assertEquals('GROUP BY', $i->getName());
		self::assertNull($i->getExpressionList());

		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i = new GroupByClause($el);

		self::assertEquals($el, $i->getExpressionList());
	}

	public function testExpressionList()
	{
		$i = new GroupByClause();
		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i->setExpressionList($el);
		self::assertEquals($el, $i->getExpressionList());
		$ret = $i->addExpression(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals($ret, $i);
		self::assertEquals(1, $i->getExpressionList()->count());
	}

	public function testToSQL92String()
	{
		$i = new GroupByClause();
		try
		{
			$i->toSQL92String();
			self::fail('throw Exception expected');
		}
		catch (\Exception $e)
		{
			self::assertEquals('ExpressionList can not be null', $e->getMessage());
		}

		$i->addExpression(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals('GROUP BY raw', $i->toSQL92String());
	}
}
