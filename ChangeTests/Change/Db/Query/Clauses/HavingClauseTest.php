<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\HavingClause;

class HavingClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new HavingClause();
		self::assertEquals('HAVING', $i->getName());
		self::assertNull($i->getPredicate());

		$p = new \Change\Db\Query\Predicates\Conjunction();
		$i = new HavingClause($p);

		self::assertEquals($p, $i->getPredicate());
	}

	public function testPredicate()
	{
		$i = new HavingClause();
		$p = new \Change\Db\Query\Predicates\Conjunction();
		$i->setPredicate($p);
		self::assertEquals($p, $i->getPredicate());
	}

	public function testToSQL92String()
	{
		$i = new HavingClause();
		self::assertEquals('', $i->toSQL92String());

		$p = new \Change\Db\Query\Predicates\Conjunction(new \Change\Db\Query\Expressions\Raw('raw'));
		$i->setPredicate($p);
		self::assertEquals('HAVING (raw)', $i->toSQL92String());
	}
}
