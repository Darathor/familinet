<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\InsertClause;

class InsertClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @param string $name
	 * @return \Change\Db\Query\Expressions\Column
	 */
	protected function getNewColumn($name)
	{
		return new \Change\Db\Query\Expressions\Column(new \Change\Db\Query\Expressions\Identifier([$name]));
	}

	public function testConstruct()
	{
		$i = new InsertClause();
		self::assertEquals('INSERT', $i->getName());
		self::assertNull($i->getTable());

		$table = new \Change\Db\Query\Expressions\Table('test');
		$i = new InsertClause($table);

		self::assertEquals($table, $i->getTable());
	}

	public function testTable()
	{
		$i = new InsertClause();

		$table = new \Change\Db\Query\Expressions\Table('test');
		$i->setTable($table);

		self::assertEquals($table, $i->getTable());
	}

	public function testColumns()
	{
		$i = new InsertClause();
		$c = $this->getNewColumn('c1');
		$ret = $i->addColumn($c);
		self::assertEquals($i, $ret);

		self::assertCount(1, $i->getColumns());
		$cls = [$this->getNewColumn('c2'), $this->getNewColumn('c3')];
		$i->setColumns($cls);

		self::assertCount(2, $i->getColumns());
	}

	public function testCheckCompile()
	{
		$i = new InsertClause();
		try
		{
			$i->checkCompile();
			self::fail('Table can not be null');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Table can not be null', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$i = new InsertClause(new \Change\Db\Query\Expressions\Table('test'));
		$c = $this->getNewColumn('c1');
		$i->addColumn($c);
		self::assertEquals('INSERT "test" ("c1")', $i->toSQL92String());
	}
}
