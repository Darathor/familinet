<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\OrderByClause;

class OrderByClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new OrderByClause();
		self::assertEquals('ORDER BY', $i->getName());
		self::assertNull($i->getExpressionList());

		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i = new OrderByClause($el);

		self::assertEquals($el, $i->getExpressionList());
	}

	public function testExpressionList()
	{
		$i = new OrderByClause();
		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i->setExpressionList($el);
		self::assertEquals($el, $i->getExpressionList());
		$ret = $i->addExpression(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals($ret, $i);
		self::assertEquals(1, $i->getExpressionList()->count());
	}

	public function testToSQL92String()
	{
		$i = new OrderByClause();
		try
		{
			$i->toSQL92String();
			self::fail('ExpressionList can not be null');
		}
		catch (\RuntimeException $e)
		{
			self::assertTrue(true);
		}

		$i->addExpression(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals('ORDER BY raw', $i->toSQL92String());
	}
}
