<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\SelectClause;

class SelectClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new SelectClause();
		self::assertEquals('SELECT', $i->getName());
		self::assertEquals(SelectClause::QUANTIFIER_ALL, $i->getQuantifier());
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $i->getSelectList());

		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$el->add(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertNotEquals($el, $i->getSelectList());

		$i = new SelectClause($el);

		self::assertEquals($el, $i->getSelectList());
	}

	public function testQuantifierString()
	{
		$i = new SelectClause();
		$i->setQuantifier(SelectClause::QUANTIFIER_DISTINCT);
		self::assertEquals(SelectClause::QUANTIFIER_DISTINCT, $i->getQuantifier());
	}

	public function testSelectList()
	{
		$i = new SelectClause();
		$ret = $i->addSelect(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals($ret, $i);

		$el = $i->getSelectList();
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $el);
		self::assertEquals(1, $el->count());
	}

	public function testToSQL92String()
	{
		$i = new SelectClause();
		self::assertEquals('SELECT *', $i->toSQL92String());

		$i->setQuantifier(SelectClause::QUANTIFIER_DISTINCT);
		self::assertEquals('SELECT DISTINCT *', $i->toSQL92String());

		$i->addSelect(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals('SELECT DISTINCT *, raw', $i->toSQL92String());

		$i->addSelect(new \Change\Db\Query\Expressions\Raw('raw2'));
		$i->setQuantifier(SelectClause::QUANTIFIER_ALL);
		self::assertEquals('SELECT *, raw, raw2', $i->toSQL92String());
	}
}
