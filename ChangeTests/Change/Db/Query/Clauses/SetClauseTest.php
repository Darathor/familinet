<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\SetClause;

class SetClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new SetClause();
		self::assertEquals('SET', $i->getName());

		self::assertInstanceOf('\Change\Db\Query\Expressions\ExpressionList', $i->getSetList());
		self::assertCount(0, $i->getSetList());

		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i = new SetClause($el);

		self::assertEquals($el, $i->getSetList());
	}

	public function testSetList()
	{
		$i = new SetClause();
		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i->setSetList($el);
		self::assertEquals($el, $i->getSetList());
		$ret = $i->addSet(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals($ret, $i);
		self::assertCount(1, $i->getSetList());
	}

	public function testCheckCompile()
	{
		$i = new SetClause();
		try
		{
			$i->checkCompile();
			self::fail('Values can not be empty');
		}
		catch (\RuntimeException $e)
		{
			self::assertTrue(true);
		}
	}

	public function testToSQL92String()
	{
		$i = new SetClause();
		$i->addSet(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals('SET raw', $i->toSQL92String());
	}
}
