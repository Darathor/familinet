<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\ValuesClause;

class ValuesClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new ValuesClause();
		self::assertEquals('VALUES', $i->getName());

		self::assertInstanceOf('\Change\Db\Query\Expressions\ExpressionList', $i->getValuesList());
		self::assertCount(0, $i->getValuesList());

		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i = new ValuesClause($el);

		self::assertEquals($el, $i->getValuesList());
	}

	public function testValuesList()
	{
		$i = new ValuesClause();
		$el = new \Change\Db\Query\Expressions\ExpressionList();
		$i->setValuesList($el);
		self::assertEquals($el, $i->getValuesList());
		$ret = $i->addValue(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals($ret, $i);
		self::assertCount(1, $i->getValuesList());
	}

	public function testToSQL92String()
	{
		$i = new ValuesClause();
		try
		{
			$i->toSQL92String();
			self::fail('ValuesList can not be empty');
		}
		catch (\RuntimeException $e)
		{
			self::assertTrue(true);
		}

		$i->addValue(new \Change\Db\Query\Expressions\Raw('raw'));
		self::assertEquals('VALUES (raw)', $i->toSQL92String());
	}
}
