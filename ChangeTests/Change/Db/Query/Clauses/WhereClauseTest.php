<?php
namespace ChangeTests\Change\Db\Query\Clauses;

use Change\Db\Query\Clauses\WhereClause;

class WhereClauseTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new WhereClause();
		self::assertEquals('WHERE', $i->getName());
		self::assertNull($i->getPredicate());

		$p = new \Change\Db\Query\Predicates\Conjunction();
		$i = new WhereClause($p);

		self::assertEquals($p, $i->getPredicate());
	}

	public function testPredicate()
	{
		$i = new WhereClause();
		$p = new \Change\Db\Query\Predicates\Conjunction();
		$i->setPredicate($p);
		self::assertEquals($p, $i->getPredicate());
	}

	public function testToSQL92String()
	{
		$i = new WhereClause();
		self::assertEquals('', $i->toSQL92String());

		$p = new \Change\Db\Query\Predicates\Conjunction(new \Change\Db\Query\Expressions\Raw('raw'));
		$i->setPredicate($p);
		self::assertEquals('WHERE (raw)', $i->toSQL92String());
	}
}
