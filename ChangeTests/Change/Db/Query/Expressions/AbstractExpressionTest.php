<?php
namespace ChangeTests\Change\Db\Query\Expressions;

class FakeExpression extends \Change\Db\Query\Expressions\AbstractExpression
{
	
	public function toSQL92String()
	{
		return 'FakeExpression';
	}
}

class AbstractExpressionTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new FakeExpression();
		self::assertNull($i->getOptions());
	}

	public function testOptions()
	{
		$i = new FakeExpression();
		$i->setOptions(array('12'));
		self::assertEquals(array('12'), $i->getOptions());
	}
}
