<?php
namespace ChangeTests\Change\Db\Query\Expressions;

class FakeOperation extends \Change\Db\Query\Expressions\AbstractOperation
{
	public function toSQL92String()
	{
		return 'FakeOperation';
	}
}

class AbstractOperationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new FakeOperation();
		self::assertNull($i->getOptions());
		self::assertNull($i->getOperator());
	}

	public function testOperator()
	{
		$i = new FakeOperation();
		$i->setOperator('=');
		self::assertEquals('=', $i->getOperator());
	}
}
