<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class AliasTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Alias();
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertEquals('AS', $i->getOperator());
		$rightHandExpression = new \Change\Db\Query\Expressions\Raw('RightHandExpression');
		$leftHandExpression = new \Change\Db\Query\Expressions\Raw('LeftHandExpression');
		$i = new \Change\Db\Query\Expressions\Alias($leftHandExpression, $rightHandExpression);
		self::assertSame($leftHandExpression, $i->getLeftHandExpression());
		self::assertSame($rightHandExpression, $i->getRightHandExpression());
	}

	public function testLeftHandExpression()
	{
		$leftHandExpression = new \Change\Db\Query\Expressions\Raw('LeftHandExpression');
		$i = new \Change\Db\Query\Expressions\Alias($leftHandExpression);
		self::assertEquals($leftHandExpression, $i->getLeftHandExpression());

		$lhe = new \Change\Db\Query\Expressions\Raw('lhe');
		$i->setLeftHandExpression($lhe);
		self::assertEquals($lhe, $i->getLeftHandExpression());
	}

	public function testRightHandExpression()
	{
		$rightHandExpression = new \Change\Db\Query\Expressions\Raw('RightHandExpression');
		$i = new \Change\Db\Query\Expressions\Alias(null, $rightHandExpression);
		self::assertEquals($rightHandExpression, $i->getRightHandExpression());

		$rhe = new \Change\Db\Query\Expressions\Raw('rhe');
		$i->setRightHandExpression($rhe);
		self::assertEquals($rhe, $i->getRightHandExpression());
	}

	public function testToSQL92String()
	{
		$rightHandExpression = new \Change\Db\Query\Expressions\Raw('RightHandExpression');
		$leftHandExpression = new \Change\Db\Query\Expressions\Raw('LeftHandExpression');
		$i = new \Change\Db\Query\Expressions\Alias();
		try
		{
			$i->toSQL92String();
			self::fail('throw Exception expected');
		}
		catch (\Exception $e)
		{
			self::assertEquals('Invalid Left Hand Expression', $e->getMessage());
		}

		$i->setLeftHandExpression($leftHandExpression);
		try
		{
			$i->toSQL92String();
			self::fail('throw Exception expected');
		}
		catch (\Exception $e)
		{
			self::assertEquals('Invalid Right Hand Expression', $e->getMessage());
		}

		$i->setRightHandExpression($rightHandExpression);
	}
}
