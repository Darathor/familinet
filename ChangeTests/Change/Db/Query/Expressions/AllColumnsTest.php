<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class AllColumnsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\AllColumns();
		self::assertTrue(true);
	}
	
	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\AllColumns();
		self::assertEquals('*', $i->toSQL92String());
	}
}
