<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class BinaryOperationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\BinaryOperation();
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertNull($i->getOperator());
	}

	public function testLeftHandExpression()
	{
		$leftHandExpression = new \Change\Db\Query\Expressions\Raw('LeftHandExpression');
		$i = new \Change\Db\Query\Expressions\BinaryOperation($leftHandExpression);
		self::assertEquals($leftHandExpression, $i->getLeftHandExpression());

		$lhe = new \Change\Db\Query\Expressions\Raw('lhe');
		$i->setLeftHandExpression($lhe);
		self::assertEquals($lhe, $i->getLeftHandExpression());
	}

	public function testRightHandExpression()
	{
		$rightHandExpression = new \Change\Db\Query\Expressions\Raw('RightHandExpression');
		$i = new \Change\Db\Query\Expressions\BinaryOperation(null, $rightHandExpression);
		self::assertEquals($rightHandExpression, $i->getRightHandExpression());

		$rhe = new \Change\Db\Query\Expressions\Raw('rhe');
		$i->setRightHandExpression($rhe);
		self::assertEquals($rhe, $i->getRightHandExpression());
	}

	public function testOperator()
	{
		$i = new \Change\Db\Query\Expressions\BinaryOperation(null, null, 'operator');
		self::assertEquals('operator', $i->getOperator());
	}

	public function testCheckCompile()
	{
		$exp = new \Change\Db\Query\Expressions\Raw('Exp');
		$i = new \Change\Db\Query\Expressions\BinaryOperation();
		try
		{
			$i->checkCompile();
			self::fail('Invalid Left Hand Expression');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid Left Hand Expression', $e->getMessage());
		}

		$i->setLeftHandExpression($exp);
		try
		{
			$i->checkCompile();
			self::fail('Invalid Right Hand Expression');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid Right Hand Expression', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$rightHandExpression = new \Change\Db\Query\Expressions\Raw('RightHandExpression');
		$leftHandExpression = new \Change\Db\Query\Expressions\Raw('LeftHandExpression');

		$i = new \Change\Db\Query\Expressions\BinaryOperation($leftHandExpression, $rightHandExpression, 'operator');
		self::assertEquals('LeftHandExpression operator RightHandExpression', $i->toSQL92String());
	}
}
