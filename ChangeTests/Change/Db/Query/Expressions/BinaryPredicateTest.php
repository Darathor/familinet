<?php
namespace ChangeTests\Change\Db\Query\Predicates;

class AssignmentTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Assignment();
		self::assertInstanceOf('\Change\Db\Query\Expressions\BinaryOperation', $i);
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertEquals($i->getOperator(), '=');
		return $i;
	}
	
	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Expressions\Assignment $i
	 */
	public function testToSQL92String(\Change\Db\Query\Expressions\Assignment $i)
	{		
		$i->setLeftHandExpression(new \Change\Db\Query\Expressions\Raw('LeftHandExpression'));
		$i->setRightHandExpression(new \Change\Db\Query\Expressions\Raw('RightHandExpression'));
		self::assertEquals('LeftHandExpression = RightHandExpression', $i->toSQL92String());
	}
}
