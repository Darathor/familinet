<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class ColumnTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$ident = new \Change\Db\Query\Expressions\Identifier(['a']);
		$i = new \Change\Db\Query\Expressions\Column($ident);
		self::assertEquals($ident, $i->getColumnName());
		self::assertNull($i->getTableOrIdentifier());

		$ident2 = new \Change\Db\Query\Expressions\Identifier(['b']);
		$i = new \Change\Db\Query\Expressions\Column($ident, $ident2);
		self::assertEquals($ident2, $i->getTableOrIdentifier());
	}

	public function testColumnName()
	{
		$ident = new \Change\Db\Query\Expressions\Identifier(['a']);
		$i = new \Change\Db\Query\Expressions\Column($ident);
		$ident2 = new \Change\Db\Query\Expressions\Identifier(['b']);

		$i->setColumnName($ident2);
		self::assertEquals($ident2, $i->getColumnName());
	}

	public function testTableOrIdentifier()
	{
		$ident = new \Change\Db\Query\Expressions\Identifier(['a']);

		$i = new \Change\Db\Query\Expressions\Column($ident);
		$ident2 = new \Change\Db\Query\Expressions\Identifier(['b']);

		$i->setTableOrIdentifier($ident2);
		self::assertEquals($ident2, $i->getTableOrIdentifier());

		try
		{
			$i->setTableOrIdentifier('table');
			self::fail('Argument 1 must be an instance of Expressions\Table | Expressions\Identifier');
		}
		catch (\Exception $e)
		{
			self::assertTrue(true);
		}
	}

	public function testToSQL92String()
	{
		$ident = new \Change\Db\Query\Expressions\Identifier(['a']);
		$i = new \Change\Db\Query\Expressions\Column($ident);
		self::assertEquals('"a"', $i->toSQL92String());

		$ident2 = new \Change\Db\Query\Expressions\Identifier(['b']);
		$i->setTableOrIdentifier($ident2);
		self::assertEquals('"b"."a"', $i->toSQL92String());
	}
}
