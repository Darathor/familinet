<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class ConcatTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Concat();
		self::assertCount(0, $i->getList());

		$s1 = new \Change\Db\Query\Expressions\StringValue('test');
		$i = new \Change\Db\Query\Expressions\Concat([$s1]);

		self::assertCount(1, $i->getList());
	}

	public function testToSQL92String()
	{
		$s1 = new \Change\Db\Query\Expressions\Raw('a');
		$s2 = new \Change\Db\Query\Expressions\Raw('b');
		$i = new \Change\Db\Query\Expressions\Concat([$s1, $s2]);
		self::assertEquals('a || b', $i->toSQL92String());
	}
}
