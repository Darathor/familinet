<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class ExpressionListTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\ExpressionList();
		self::assertCount(0, $i->getList());

		$s1 = new \Change\Db\Query\Expressions\StringValue('test');
		$i = new \Change\Db\Query\Expressions\ExpressionList([$s1]);

		self::assertCount(1, $i->getList());
	}

	public function testList()
	{
		$s1 = new \Change\Db\Query\Expressions\StringValue('test');
		$s2 = new \Change\Db\Query\Expressions\Raw('b');
		$i = new \Change\Db\Query\Expressions\ExpressionList();
		$i->setList([$s1, $s2]);
		self::assertCount(2, $i->getList());

		try
		{
			$i->setList($s1);
			self::fail('throw Exception expected');
		}
		catch (\Throwable $e)
		{
			self::assertStringStartsWith('Argument 1 passed to Change\Db\Query\Expressions\ExpressionList::setList() must be of the type array', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$s1 = new \Change\Db\Query\Expressions\Raw('a');
		$s2 = new \Change\Db\Query\Expressions\Raw('b');
		$i = new \Change\Db\Query\Expressions\ExpressionList([$s1, $s2]);
		self::assertEquals('a, b', $i->toSQL92String());
	}
}
