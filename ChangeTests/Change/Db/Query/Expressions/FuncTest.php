<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class FuncTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Func();
		self::assertNull($i->getFunctionName());
		self::assertCount(0, $i->getArguments());

		$r = new \Change\Db\Query\Expressions\Raw('r');
		$i = new \Change\Db\Query\Expressions\Func('test', [$r]);
		self::assertEquals('test', $i->getFunctionName());
		self::assertCount(1, $i->getArguments());
	}

	public function testFunctionName()
	{
		$i = new \Change\Db\Query\Expressions\Func();
		$i->setFunctionName('Value');
		self::assertEquals('Value', $i->getFunctionName());
	}

	public function testArguments()
	{
		$i = new \Change\Db\Query\Expressions\Func();

		$i->setArguments([new \Change\Db\Query\Expressions\Raw('r')]);
		self::assertCount(1, $i->getArguments());
		$i->addArgument(new \Change\Db\Query\Expressions\Raw('s'));
		self::assertCount(2, $i->getArguments());

		try
		{
			$i->setArguments('s');
			self::fail('throw Exception expected');
		}
		catch (\Throwable $e)
		{
			self::assertStringStartsWith('Argument 1 passed to Change\Db\Query\Expressions\Func::setArguments() must be of the type array', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Func();
		self::assertEquals('()', $i->toSQL92String());

		$i->setFunctionName('test');
		self::assertEquals('test()', $i->toSQL92String());

		$i->addArgument(new \Change\Db\Query\Expressions\Raw('r'));
		self::assertEquals('test(r)', $i->toSQL92String());

		$i->addArgument(new \Change\Db\Query\Expressions\Raw('s'));
		self::assertEquals('test(r, s)', $i->toSQL92String());
	}
}
