<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class IdentifierTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Identifier();
		self::assertCount(0, $i->getParts());

		$i = new \Change\Db\Query\Expressions\Identifier(['test', ' tutu']);
		self::assertEquals(['test', 'tutu'], $i->getParts());
	}

	public function testParts()
	{
		$i = new \Change\Db\Query\Expressions\Identifier();
		$i->setParts(['test', ' tutu', '', ' 0']);
		self::assertEquals(['test', 'tutu', '0'], $i->getParts());

		try
		{
			new \Change\Db\Query\Expressions\Identifier('test');
			self::fail('Argument 1 must be an instance of Array');
		}
		catch (\Throwable $e)
		{
			self::assertStringStartsWith('Argument 1 passed to Change\Db\Query\Expressions\Identifier::__construct() must be of the type array, string given',
				$e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Identifier(['a', 'b']);
		self::assertEquals('"a"."b"', $i->toSQL92String());
	}
}
