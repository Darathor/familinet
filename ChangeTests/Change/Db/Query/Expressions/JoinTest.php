<?php

namespace ChangeTests\Change\Db\Query\Expressions;

use Change\Db\Query\Expressions\Join;

class JoinTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$t = new \Change\Db\Query\Expressions\Table('t');
		$i = new Join($t);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Table::class, $i->getTableExpression());
		self::assertEquals(Join::CROSS_JOIN, $i->getType());
		self::assertNull($i->getSpecification());
	}

	public function testType()
	{
		$i = new Join(new \Change\Db\Query\Expressions\Table('t'));
		foreach ([Join::INNER_JOIN, Join::FULL_OUTER_JOIN, Join::LEFT_OUTER_JOIN, Join::RIGHT_OUTER_JOIN] as $value)
		{
			$i->setType($value);
			self::assertEquals($value, $i->getType());
		}
	}

	public function testToSQL92String()
	{
		$i = new Join(new \Change\Db\Query\Expressions\Table('t'));
		self::assertEquals('NATURAL CROSS JOIN "t"', $i->toSQL92String());

		$i->setType(Join::INNER_JOIN);
		self::assertEquals('NATURAL INNER JOIN "t"', $i->toSQL92String());

		$i->setSpecification(new \Change\Db\Query\Expressions\Raw('r'));
		$i->setType(Join::INNER_JOIN);
		self::assertEquals('INNER JOIN "t" r', $i->toSQL92String());
	}
}
