<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class NumericTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Numeric();
		self::assertNull($i->getValue());

		$i = new \Change\Db\Query\Expressions\Numeric('test');
		self::assertEquals('test', $i->getValue());
	}
	
	public function testValue()
	{
		$i = new \Change\Db\Query\Expressions\Numeric();
		$i->setValue('Value');
		self::assertEquals('Value', $i->getValue());
	}
		
	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Numeric();
		$i->setValue('Value');
		self::assertEquals('0', $i->toSQL92String());
		
		$i->setValue('3.3');
		self::assertEquals('3.3', $i->toSQL92String());
	}
}