<?php

namespace ChangeTests\Change\Db\Query\Expressions;

use Change\Db\Query\Expressions\OrderingSpecification;

class OrderingSpecificationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new OrderingSpecification(new \Change\Db\Query\Expressions\Raw('r'));
		self::assertEquals(OrderingSpecification::ASC, $i->getOperator());
		return $i;
	}

	/**
	 * @depends testConstruct
	 */
	public function testOperator(OrderingSpecification $i)
	{
		$i->setOperator(OrderingSpecification::DESC);
		self::assertEquals(OrderingSpecification::DESC, $i->getOperator());

		try
		{
			$i->setOperator('a');
			self::fail('throw Exception expected');
		}
		catch (\Exception $e)
		{
			self::assertEquals('Argument 1 must be a valid const', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$i = new OrderingSpecification(new \Change\Db\Query\Expressions\Raw('r'));
		self::assertEquals('r ASC', $i->toSQL92String());

		$i->setOperator(OrderingSpecification::DESC);
		self::assertEquals('r DESC', $i->toSQL92String());
	}
}