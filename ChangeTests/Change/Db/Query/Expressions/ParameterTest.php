<?php

namespace ChangeTests\Change\Db\Query\Expressions;

use Change\Db\Query\Expressions\Parameter;
use Change\Db\ScalarType;

class ParameterTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new Parameter('test');
		self::assertEquals('test', $i->getName());
		self::assertEquals(ScalarType::STRING, $i->getType());
	}

	public function testType()
	{
		$i = new Parameter('test');
		foreach ([ScalarType::STRING, ScalarType::BOOLEAN, ScalarType::DATETIME,
					 ScalarType::LOB, ScalarType::TEXT, ScalarType::DECIMAL, ScalarType::INTEGER] as $value)
		{
			$i->setType($value);
			self::assertEquals($value, $i->getType());
		}

		try
		{
			$i->setType(null);
			self::fail('throw Exception expected');
		}
		catch (\Throwable $e)
		{
			self::assertStringStartsWith('Argument 1 passed to Change\Db\Query\Expressions\Parameter::setType() must be of the type int',
				$e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Parameter('test');
		self::assertEquals(':test', $i->toSQL92String());
	}
}