<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class ParenthesesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Parentheses(new \Change\Db\Query\Expressions\Raw('test'));
		self::assertInstanceOf('\Change\Db\Query\Expressions\Raw', $i->getExpression());
		return $i;
	}
	
	/**
	 * @depends testConstruct
	 */
	public function testExpression(\Change\Db\Query\Expressions\Parentheses $i)
	{
		$e = new \Change\Db\Query\Expressions\Raw('set');
		$i->setExpression($e);
		self::assertSame($e, $i->getExpression());
		self::assertEquals('set', $e->getValue());
		return $i;

	}
		
	/**
	 * @depends testExpression
	 */
	public function testToSQL92String(\Change\Db\Query\Expressions\Parentheses $i)
	{
		self::assertEquals('(set)', $i->toSQL92String());
	}
}