<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class RawTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Raw();
		self::assertNull($i->getValue());

		$i = new \Change\Db\Query\Expressions\Raw('test');
		self::assertEquals('test', $i->getValue());
	}
	
	public function testValue()
	{
		$i = new \Change\Db\Query\Expressions\Raw();
		$i->setValue('Value');
		self::assertEquals('Value', $i->getValue());
	}
		
	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Raw();
		$i->setValue('Value');
		self::assertEquals('Value', $i->toSQL92String());
	}
}