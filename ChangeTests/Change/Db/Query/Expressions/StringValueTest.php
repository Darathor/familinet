<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class StringValueTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\StringValue();
		self::assertInstanceOf(\Change\Db\Query\Expressions\Value::class, $i);
		self::assertEquals(\Change\Db\ScalarType::STRING, $i->getScalarType());

		$i = new \Change\Db\Query\Expressions\StringValue('test');
		self::assertEquals('test', $i->getValue());
		return $i;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Expressions\StringValue $i
	 */
	public function testToSQL92String(\Change\Db\Query\Expressions\StringValue $i)
	{
		self::assertEquals("'test'", $i->toSQL92String());
		
		$i->setValue(null);
		self::assertEquals('NULL', $i->toSQL92String());
		
		$i->setValue("t'est");
		self::assertEquals("'t\\'est'", $i->toSQL92String());
	}
}