<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class SubQueryTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\DbProvider
	 */
	protected function getDbProvider()
	{
		return $this->getApplicationServices()->getDbProvider();
	}

	/**
	 * @return \Change\Db\Query\SelectQuery
	 */
	protected function getSelectQuery()
	{
		$sq = new \Change\Db\Query\SelectQuery($this->getDbProvider());
		$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
		$sq->getSelectClause()->addSelect(new \Change\Db\Query\Expressions\Numeric(1));
		return $sq;
	}

	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\SubQuery($this->getSelectQuery());
		self::assertInstanceOf(\Change\Db\Query\SelectQuery::class, $i->getSubQuery());
		return $i;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Expressions\SubQuery $i
	 */
	public function testToSQL92SubQuery(\Change\Db\Query\Expressions\SubQuery $i)
	{
		self::assertEquals('(SELECT 1)', $i->toSQL92String());
	}
}