<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class TableTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Table('table');
		self::assertEquals('table', $i->getName());
		self::assertNull($i->getDatabase());

		$i = new \Change\Db\Query\Expressions\Table('table', 'db');
		self::assertEquals('table', $i->getName());
		self::assertEquals('db', $i->getDatabase());
		
		
	}
	
	public function testName()
	{

		$i = new \Change\Db\Query\Expressions\Table(null);
		$i->setName('table');
		self::assertEquals('table', $i->getName());
	}
	
	public function testDatabase()
	{
	
		$i = new \Change\Db\Query\Expressions\Table(null);
		$i->setDatabase('db');
		self::assertEquals('db', $i->getDatabase());
	}
	
	public function testToSQL92String()
	{
		$i = new \Change\Db\Query\Expressions\Table('table');
		self::assertEquals('"table"', $i->toSQL92String());
		
		$i->setDatabase('db');
		self::assertEquals('"db"."table"', $i->toSQL92String());
	}
}
