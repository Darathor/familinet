<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class UnaryOperationTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\Query\Expressions\UnaryOperation
	 */
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\UnaryOperation();
		self::assertNull($i->getOperator());
		self::assertNull($i->getExpression());

		$r = new \Change\Db\Query\Expressions\Raw('raw');
		$i = new \Change\Db\Query\Expressions\UnaryOperation($r, 'op');
		self::assertEquals('op', $i->getOperator());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $i->getExpression());

		return $i;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Expressions\UnaryOperation $i
	 * @return \Change\Db\Query\Expressions\UnaryOperation
	 */
	public function testExpression(\Change\Db\Query\Expressions\UnaryOperation $i)
	{
		$e = new \Change\Db\Query\Expressions\Raw('test');
		$i->setExpression($e);
		self::assertSame($e, $i->getExpression());
		self::assertEquals('test', $e->getValue());
		return $i;
	}

	/**
	 * @depends testExpression
	 * @param \Change\Db\Query\Expressions\UnaryOperation $i
	 */
	public function testToSQL92String(\Change\Db\Query\Expressions\UnaryOperation $i)
	{
		self::assertEquals('op test', $i->toSQL92String());

		$i = new \Change\Db\Query\Expressions\UnaryOperation();
		try
		{
			$i->toSQL92String();
			self::fail('throw Exception expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('Expression can not be null', $e->getMessage());
		}
	}
}
