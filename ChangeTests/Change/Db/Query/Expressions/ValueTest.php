<?php

namespace ChangeTests\Change\Db\Query\Expressions;

class ValueTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Expressions\Value();
		self::assertNull($i->getValue());
		self::assertTrue($i->isNull());
		self::assertEquals(\Change\Db\ScalarType::STRING, $i->getScalarType());

		$i = new \Change\Db\Query\Expressions\Value(45.6, \Change\Db\ScalarType::DECIMAL);
		self::assertEquals(45.6, $i->getValue());
		self::assertEquals(\Change\Db\ScalarType::DECIMAL, $i->getScalarType());
		self::assertFalse($i->isNull());
		return $i;
	}
	
	/**
	 * @depends testConstruct
	 */
	public function testValue(\Change\Db\Query\Expressions\Value $i)
	{
		$i->setValue('Value');
		self::assertEquals('Value', $i->getValue());
		
		$i->setScalarType(\Change\Db\ScalarType::STRING);
		self::assertEquals(\Change\Db\ScalarType::STRING, $i->getScalarType());
		
		return $i;
	}

	/**
	 * @depends testValue
	 */
	public function testToSQL92String(\Change\Db\Query\Expressions\Value $i)
	{
		self::assertEquals("'Value'", $i->toSQL92String());
		$i->setValue(null);
		self::assertEquals("NULL", $i->toSQL92String());
		
		$i->setValue("test'");
		self::assertEquals("'test\\''", $i->toSQL92String());
	}
}