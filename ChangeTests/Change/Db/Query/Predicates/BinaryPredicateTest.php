<?php
namespace ChangeTests\Change\Db\Query\Predicates;

class BinaryPredicateTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Predicates\BinaryPredicate();
		self::assertInstanceOf('\Change\Db\Query\Expressions\BinaryOperation', $i);
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertNull($i->getOperator());
		
		self::assertInstanceOf('\Change\Db\Query\Predicates\InterfacePredicate', $i);
		
		return $i;
	}
	
	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Predicates\BinaryPredicate $i
	 */
	public function testToSQL92String(\Change\Db\Query\Predicates\BinaryPredicate $i)
	{		
		$i->setLeftHandExpression(new \Change\Db\Query\Expressions\Raw('LeftHandExpression'));
		$i->setRightHandExpression(new \Change\Db\Query\Expressions\Raw('RightHandExpression'));
		$i->setOperator('operator');
		self::assertEquals('LeftHandExpression operator RightHandExpression', $i->toSQL92String());
	}
}
