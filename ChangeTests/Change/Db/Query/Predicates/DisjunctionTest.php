<?php
namespace ChangeTests\Change\Db\Query\Predicates;

class DisjunctionTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new \Change\Db\Query\Predicates\Disjunction();
		self::assertInstanceOf('\Change\Db\Query\Predicates\InterfacePredicate', $i);
		
		$i = new \Change\Db\Query\Predicates\Disjunction(new \Change\Db\Query\Expressions\Raw('f1'), new \Change\Db\Query\Expressions\Raw('f2'));
		self::assertCount(2, $i->getArguments());
		return $i;
	}
	
	/**
	 * @depends testConstruct
	 * @param \Change\Db\Query\Predicates\Disjunction $i
	 */
	public function testArguments(\Change\Db\Query\Predicates\Disjunction $i)
	{	
		$i->setArguments(array());
		self::assertCount(0, $i->getArguments());
		
		$i->setArguments(array(new \Change\Db\Query\Expressions\Raw('f1')));
		self::assertCount(1, $i->getArguments());
		
		$ret = $i->addArgument(new \Change\Db\Query\Expressions\Raw('f2'));
		self::assertEquals($ret, $i);
		self::assertCount(2, $i->getArguments());
		
		return $i;
	}
	
	/**
	 * @depends testArguments
	 * @param \Change\Db\Query\Predicates\Disjunction $i
	 */
	public function testToSQL92String(\Change\Db\Query\Predicates\Disjunction $i)
	{		
		self::assertEquals('(f1 OR f2)', $i->toSQL92String());
	}
}
