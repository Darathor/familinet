<?php
namespace ChangeTests\Change\Db\Query\Predicates;

use Change\Db\Query\Predicates\Exists;

class ExistsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$obj = new Exists();
		self::assertInstanceOf('\Change\Db\Query\Predicates\UnaryPredicate', $obj);
		self::assertNull($obj->getExpression());
		self::assertFalse($obj->getNot());
		self::assertEquals('EXISTS', $obj->getOperator());
	}

	public function testNot()
	{
		$obj = new Exists();
		$obj->setNot(true);
		self::assertTrue($obj->getNot());
		self::assertEquals('NOT EXISTS', $obj->getOperator());

		$obj->setNot(false);
		self::assertFalse($obj->getNot());
		self::assertEquals('EXISTS', $obj->getOperator());
	}
	
	public function testCheckCompile()
	{
		$obj = new Exists();
		try
		{			
			$obj->checkCompile();
			self::fail('Exception Expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('Expression must be a SubQuery', $e->getMessage());
		}
	}

	public function testToSQL92String()
	{
		$sq = new \Change\Db\Query\SelectQuery();
		$sq->setSelectClause(
			new \Change\Db\Query\Clauses\SelectClause(new \Change\Db\Query\Expressions\ExpressionList(
				array(new \Change\Db\Query\Expressions\Column(
					new \Change\Db\Query\Expressions\Identifier(array('c1'))))
			)
			));
		$subQuery = new \Change\Db\Query\Expressions\SubQuery($sq);
		$obj = new Exists($subQuery);
		self::assertEquals("EXISTS(SELECT \"c1\")", $obj->toSQL92String());
	}
}
