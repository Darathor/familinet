<?php
namespace ChangeTests\Change\Db\Query\Predicates;

use Change\Db\Query\Expressions;
use Change\Db\Query\Predicates\HasPermission;

class HasPermissionTest extends \ChangeTests\Change\TestAssets\TestCase
{

	public function testConstruct()
	{
		$obj = new HasPermission();
		self::assertInstanceOf(HasPermission::class, $obj);
		self::assertNull($obj->getAccessor());
		self::assertNull($obj->getRole());
		self::assertNull($obj->getPrivilege());
		self::assertNull($obj->getResource());
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());
	}

	public function testAccessor()
	{
		$obj = new HasPermission(new Expressions\Numeric(45));
		self::assertInstanceOf(\Change\Db\Query\Expressions\Numeric::class, $obj->getAccessor());
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE (("accessor_id" = 45 OR "accessor_id" = 0) AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list = new Expressions\ExpressionList();
		$list->add(new Expressions\Numeric(45));
		$obj->setAccessor($list);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $obj->getAccessor());

		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" IN (45, 0) AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list->add(new Expressions\Numeric(100));
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" IN (45, 100, 0) AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());
	}

	public function testRole()
	{
		$obj = new HasPermission(null, new Expressions\StringValue('test'));
		self::assertInstanceOf(\Change\Db\Query\Expressions\StringValue::class, $obj->getRole());
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND ("role" = \'test\' OR "role" = \'*\') AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list = new Expressions\ExpressionList();
		$list->add(new Expressions\StringValue('test'));
		$obj->setRole($list);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $obj->getRole());

		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" IN (\'test\', \'*\') AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list->add(new Expressions\StringValue('t'));
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" IN (\'test\', \'t\', \'*\') AND "resource_id" = 0 AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());
	}

	public function testResource()
	{
		$obj = new HasPermission(null, null, new Expressions\Numeric(45));
		self::assertInstanceOf(\Change\Db\Query\Expressions\Numeric::class, $obj->getResource());
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND ("resource_id" = 45 OR "resource_id" = 0) AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list = new Expressions\ExpressionList();
		$list->add(new Expressions\Numeric(45));
		$obj->setResource($list);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $obj->getResource());

		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" IN (45, 0) AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list->add(new Expressions\Numeric(100));
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" IN (45, 100, 0) AND "privilege" = \'*\'))';
		self::assertEquals($sql, $obj->toSQL92String());
	}

	public function testPrivilege()
	{
		$obj = new HasPermission(null, null, null, new Expressions\StringValue('test'));
		self::assertInstanceOf(\Change\Db\Query\Expressions\StringValue::class, $obj->getPrivilege());
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" = 0 AND ("privilege" = \'test\' OR "privilege" = \'*\')))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list = new Expressions\ExpressionList();
		$list->add(new Expressions\StringValue('test'));
		$obj->setPrivilege($list);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $obj->getPrivilege());

		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" IN (\'test\', \'*\')))';
		self::assertEquals($sql, $obj->toSQL92String());

		$list->add(new Expressions\StringValue('t'));
		$sql = 'EXISTS(SELECT * FROM "change_permission_rule" WHERE ("accessor_id" = 0 AND "role" = \'*\' AND "resource_id" = 0 AND "privilege" IN (\'test\', \'t\', \'*\')))';
		self::assertEquals($sql, $obj->toSQL92String());
	}
}
