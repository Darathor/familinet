<?php
namespace ChangeTests\Change\Db\Query\Predicates;

use Change\Db\Query\Predicates\In;

class InTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new In();
		self::assertInstanceOf('\Change\Db\Query\Predicates\BinaryPredicate', $i);
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertFalse($i->getNot());
		self::assertEquals('IN', $i->getOperator());
	}
	
	
	public function testNot()
	{
		$i = new In();
		$i->setNot(true);
		self::assertTrue($i->getNot());
		self::assertEquals('NOT IN', $i->getOperator());

		$i->setNot(false);
		self::assertFalse($i->getNot());
		self::assertEquals('IN', $i->getOperator());
	}
	
	public function testCheckCompile()
	{
		$i = new In(new \Change\Db\Query\Expressions\Raw('lhe'), new \Change\Db\Query\Expressions\Raw('rhe'));
		
		try
		{			
			$i->checkCompile();
			self::fail('Right Hand Expression must be a Subquery or ExpressionList');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Right Hand Expression must', $e->getMessage());
		}
		
		
		try
		{
			$expList = new \Change\Db\Query\Expressions\ExpressionList();
			$i->setRightHandExpression($expList);
			$i->checkCompile();
			self::fail('Right Hand Expression must be a ExpressionList with one element or more');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Right Hand Expression must be', $e->getMessage());
		}
		$expList->add(new \Change\Db\Query\Expressions\Raw('rhe1'));
		$i->checkCompile();
	}
	
	/**
	 * @return \Change\Db\DbProvider
	 */
	protected function getDbProvider()
	{
		return $this->getApplicationServices()->getDbProvider();
	}
	
	public function testToSQL92String()
	{		

		$expList = new \Change\Db\Query\Expressions\ExpressionList();
		$expList->add(new \Change\Db\Query\Expressions\Raw('rhe1'));
		$i = new In(new \Change\Db\Query\Expressions\Raw('lhe'), $expList);	
		$i->setRightHandExpression($expList);
		self::assertEquals("lhe IN (rhe1)", $i->toSQL92String());
		
		$s = new \Change\Db\Query\SelectQuery($this->getDbProvider());
		$s->setSelectClause(new \Change\Db\Query\Clauses\SelectClause($expList));
		$i->setRightHandExpression(new \Change\Db\Query\Expressions\SubQuery($s));
		
		self::assertEquals("lhe IN (SELECT rhe1)", $i->toSQL92String());
	}
}
