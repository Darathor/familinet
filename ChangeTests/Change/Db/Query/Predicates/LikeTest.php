<?php
namespace ChangeTests\Change\Db\Query\Predicates;

use Change\Db\Query\Predicates\Like;

class LikeTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$i = new Like();
		self::assertInstanceOf('\Change\Db\Query\Predicates\BinaryPredicate', $i);
		self::assertNull($i->getLeftHandExpression());
		self::assertNull($i->getRightHandExpression());
		self::assertFalse($i->getCaseSensitive());
		self::assertEquals('LIKE', $i->getOperator());	
		self::assertEquals(Like::ANYWHERE, $i->getMatchMode());
	}
	
	public function testMatchMode()
	{
		$i = new Like();
		foreach (array(Like::ANYWHERE, Like::BEGIN, Like::END, Like::EXACT) as $matchMode)
		{
			$i->setMatchMode($matchMode);
			self::assertEquals($matchMode, $i->getMatchMode());
		}
		
		try
		{
			$i->setMatchMode(null);
			self::fail('Argument 1 must be a valid const');
		}
		catch (\Exception $e)
		{
			self::assertTrue(true);
		}
	}
	
	public function testCaseSensitive()
	{
		$i = new Like();
		$i->setCaseSensitive(true);
		self::assertTrue($i->getCaseSensitive());
		self::assertEquals('LIKE BINARY', $i->getOperator());

		$i->setCaseSensitive(false);
		self::assertFalse($i->getCaseSensitive());
		self::assertEquals('LIKE', $i->getOperator());	
	}
	
	public function testToSQL92String()
	{		
		$i = new Like();
		$i->setLeftHandExpression(new \Change\Db\Query\Expressions\Raw('lhe'));
		$i->setRightHandExpression(new \Change\Db\Query\Expressions\Raw('rhe'));
		self::assertEquals("lhe LIKE '%' || rhe || '%'", $i->toSQL92String());
		$i->setCaseSensitive(true);
		self::assertEquals("lhe LIKE BINARY '%' || rhe || '%'", $i->toSQL92String());
		
		$i->setMatchMode(Like::BEGIN);
		self::assertEquals("lhe LIKE BINARY rhe || '%'", $i->toSQL92String());
		
		$i->setMatchMode(Like::END);
		self::assertEquals("lhe LIKE BINARY '%' || rhe", $i->toSQL92String());
		
		$i->setMatchMode(Like::EXACT);
		self::assertEquals("lhe LIKE BINARY rhe", $i->toSQL92String());
	}
}
