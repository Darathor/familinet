<?php
namespace ChangeTests\Change\Db\Query;

use Change\Db\Query\SQLFragmentBuilder;

class SQLFragmentBuilderTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\Query\SQLFragmentBuilder
	 */
	protected function getNewSQLFragmentBuilder()
	{
		return new SQLFragmentBuilder(new \Change\Db\SqlMapping());
	}

	public function testConstruct()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		self::assertTrue(true);
	}

	public function testFunc()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$func = $fb->func('test', new \Change\Db\Query\Expressions\Raw('raw'));

		self::assertInstanceOf(\Change\Db\Query\Expressions\Func::class, $func);
		self::assertEquals('test', $func->getFunctionName());

		$args = $func->getArguments();
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $args[0]);
	}

	public function testSum()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$func = $fb->sum('test', 'test2');

		self::assertInstanceOf(\Change\Db\Query\Expressions\Func::class, $func);
		self::assertEquals('SUM', $func->getFunctionName());

		[$arg1, $arg2] = $func->getArguments();
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $arg1);
		self::assertEquals('test', $arg1->getValue());

		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $arg2);
		self::assertEquals('test2', $arg2->getValue());
	}

	public function testTable()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->table('test', 'db');

		self::assertInstanceOf(\Change\Db\Query\Expressions\Table::class, $frag);
		self::assertEquals('test', $frag->getName());
		self::assertEquals('db', $frag->getDatabase());
	}

	public function testColumn()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->column('test', 'table');

		self::assertInstanceOf(\Change\Db\Query\Expressions\Column::class, $frag);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Identifier::class, $frag->getColumnName());
		self::assertEquals(['test'], $frag->getColumnName()->getParts());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Identifier::class, $frag->getTableOrIdentifier());
		self::assertEquals(['table'], $frag->getTableOrIdentifier()->getParts());
	}

	public function testIdentifier()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->identifier('test', 'table');
		self::assertInstanceOf(\Change\Db\Query\Expressions\Identifier::class, $frag);
		self::assertEquals(['test', 'table'], $frag->getParts());
	}

	public function testAlias()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$ident = $fb->identifier('test', 'table');
		$frag = $fb->alias($ident, 'alias');

		self::assertInstanceOf(\Change\Db\Query\Expressions\Alias::class, $frag);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Identifier::class, $frag->getLeftHandExpression());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Identifier::class, $frag->getRightHandExpression());

		try
		{
			$frag = $fb->alias($ident, null);
			self::fail('A InvalidArgumentException should be thrown.');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertTrue(true);
		}
	}

	public function testParameter()
	{
		$fb = $this->getNewSQLFragmentBuilder();

		$frag = $fb->parameter('test');
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $frag);
		self::assertEquals('test', $frag->getName());
		self::assertEquals(\Change\Db\ScalarType::STRING, $frag->getType());

		$frag = $fb->integerParameter('test');
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $frag);
		self::assertEquals('test', $frag->getName());
		self::assertEquals(\Change\Db\ScalarType::INTEGER, $frag->getType());

		$frag = $fb->dateTimeParameter('test');
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $frag);
		self::assertEquals('test', $frag->getName());
		self::assertEquals(\Change\Db\ScalarType::DATETIME, $frag->getType());

		$frag = $fb->typedParameter('test', \Change\Db\ScalarType::LOB);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $frag);
		self::assertEquals('test', $frag->getName());
		self::assertEquals(\Change\Db\ScalarType::LOB, $frag->getType());
	}

	public function testNumber()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->number(5);

		self::assertInstanceOf(\Change\Db\Query\Expressions\Numeric::class, $frag);
		self::assertEquals(5, $frag->getValue());
	}

	public function testString()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->string('test');

		self::assertInstanceOf(\Change\Db\Query\Expressions\StringValue::class, $frag);
		self::assertEquals('test', $frag->getValue());
	}

	public function testExpressionList()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->expressionList($fb->identifier('test'));
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $frag);
		self::assertCount(1, $frag->getList());
	}

	public function testSubQuery()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$q = new \Change\Db\Query\SelectQuery($this->getApplicationServices()->getDbProvider());
		$frag = $fb->subQuery($q);
		self::assertInstanceOf(\Change\Db\Query\Expressions\SubQuery::class, $frag);
	}

	public function testEq()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->eq('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('=', $frag->getOperator());
	}

	public function testNeq()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->neq('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('<>', $frag->getOperator());
	}

	public function testGt()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->gt('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('>', $frag->getOperator());
	}

	public function testGte()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->gte('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('>=', $frag->getOperator());
	}

	public function testLt()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->lt('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('<', $frag->getOperator());
	}

	public function testLte()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->lte('a', 'b');
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $frag);
		self::assertEquals('<=', $frag->getOperator());
	}

	public function testLike()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->like('a', 'b', \Change\Db\Query\Predicates\Like::BEGIN, true);

		self::assertInstanceOf(\Change\Db\Query\Predicates\Like::class, $frag);
		self::assertEquals('LIKE BINARY', $frag->getOperator());
		self::assertEquals(\Change\Db\Query\Predicates\Like::BEGIN, $frag->getMatchMode());
	}

	public function testIsNull()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->isNull('a');

		self::assertInstanceOf(\Change\Db\Query\Predicates\UnaryPredicate::class, $frag);
		self::assertEquals('IS NULL', $frag->getOperator());
	}

	public function testIsNotNull()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->isNotNull('a');

		self::assertInstanceOf(\Change\Db\Query\Predicates\UnaryPredicate::class, $frag);
		self::assertEquals('IS NOT NULL', $frag->getOperator());
	}

	public function testLogicAnd()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->logicAnd('a', 'b');

		self::assertInstanceOf(\Change\Db\Query\Predicates\Conjunction::class, $frag);
		self::assertCount(2, $frag->getArguments());
	}

	public function testLogicOr()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->logicOr('a', 'b', 'c');

		self::assertInstanceOf(\Change\Db\Query\Predicates\Disjunction::class, $frag);
		self::assertCount(3, $frag->getArguments());
	}

	public function testIn()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->in('a', 'b', 'c');
		self::assertFalse($frag->getNot());
		self::assertInstanceOf(\Change\Db\Query\Predicates\In::class, $frag);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $frag->getRightHandExpression());
		self::assertCount(2, $frag->getRightHandExpression()->getList());

		$q = new \Change\Db\Query\SelectQuery($this->getApplicationServices()->getDbProvider());
		$frag = $fb->in('a', $fb->subQuery($q));
		self::assertInstanceOf(\Change\Db\Query\Predicates\In::class, $frag);
		self::assertInstanceOf(\Change\Db\Query\Expressions\SubQuery::class, $frag->getRightHandExpression());
	}

	public function testNotIn()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->notIn('a', 'b');
		self::assertTrue($frag->getNot());
		self::assertInstanceOf(\Change\Db\Query\Predicates\In::class, $frag);
		self::assertInstanceOf(\Change\Db\Query\Expressions\ExpressionList::class, $frag->getRightHandExpression());
		self::assertCount(1, $frag->getRightHandExpression()->getList());
	}

	public function testAddition()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->addition('a', 'b');
		self::assertEquals('+', $frag->getOperator());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $frag->getLeftHandExpression());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $frag->getRightHandExpression());
	}

	public function testSubtraction()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->subtraction('a', 'b');
		self::assertEquals('-', $frag->getOperator());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $frag->getLeftHandExpression());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Raw::class, $frag->getRightHandExpression());
	}

	public function testConcat()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->concat('a', $fb->column('b'));
		self::assertEquals('a || "b"', $frag->toSQL92String());
	}

	public function testAllColumns()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->allColumns();
		self::assertEquals('*', $frag->toSQL92String());
	}

	public function testHasPermission()
	{
		$fb = $this->getNewSQLFragmentBuilder();
		$frag = $fb->hasPermission(10, 'R', 5, 'P');
		self::assertEquals('EXISTS(SELECT * FROM "change_permission_rule" WHERE (("accessor_id" = 10 OR "accessor_id" = 0) AND ("role" = \'R\' OR "role" = \'*\') AND ("resource_id" = 5 OR "resource_id" = 0) AND ("privilege" = \'P\' OR "privilege" = \'*\')))',
			$frag->toSQL92String());
	}
}