<?php

namespace ChangeTests\Change\Db\Query;

use Change\Db\Query\SelectQuery;

class SelectQueryTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\DbProvider
	 */
	protected function getDbProvider()
	{
		return $this->getApplicationServices()->getDbProvider();
	}

	/**
	 * @return \Change\Db\Query\SQLFragmentBuilder
	 */
	protected function getSQLFragmentBuilder()
	{
		return new \Change\Db\Query\SQLFragmentBuilder($this->getDbProvider()->getSqlMapping());
	}

	/**
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testSelectClause()
	{
		$selectQuery = new SelectQuery($this->getDbProvider());;
		self::assertNull($selectQuery->getSelectClause());
		$cl = new \Change\Db\Query\Clauses\SelectClause($this->getSQLFragmentBuilder()->expressionList('c1', 'c2'));
		$selectQuery->setSelectClause($cl);

		self::assertEquals($cl, $selectQuery->getSelectClause());
		return $selectQuery;
	}

	/**
	 * @depends testSelectClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testFromClause($selectQuery)
	{
		self::assertNull($selectQuery->getFromClause());
		$cl = new \Change\Db\Query\Clauses\FromClause($this->getSQLFragmentBuilder()->table('table'));
		$selectQuery->setFromClause($cl);

		self::assertEquals($cl, $selectQuery->getFromClause());
		return $selectQuery;
	}

	/**
	 * @depends testFromClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testWhereClause($selectQuery)
	{
		self::assertNull($selectQuery->getWhereClause());
		$cl = new \Change\Db\Query\Clauses\WhereClause($this->getSQLFragmentBuilder()->logicAnd('w'));
		$selectQuery->setWhereClause($cl);

		self::assertEquals($cl, $selectQuery->getWhereClause());
		return $selectQuery;
	}

	/**
	 * @depends testWhereClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testGroupByClause($selectQuery)
	{
		self::assertNull($selectQuery->getGroupByClause());
		$cl = new \Change\Db\Query\Clauses\GroupByClause($this->getSQLFragmentBuilder()->expressionList('g1', 'g2'));
		$selectQuery->setGroupByClause($cl);

		self::assertEquals($cl, $selectQuery->getGroupByClause());
		return $selectQuery;
	}

	/**
	 * @depends testGroupByClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testHavingClause($selectQuery)
	{
		self::assertNull($selectQuery->getHavingClause());
		$cl = new \Change\Db\Query\Clauses\HavingClause($this->getSQLFragmentBuilder()->logicOr('h'));
		$selectQuery->setHavingClause($cl);

		self::assertEquals($cl, $selectQuery->getHavingClause());
		return $selectQuery;
	}

	/**
	 * @depends testHavingClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testOrderByClause($selectQuery)
	{
		self::assertNull($selectQuery->getOrderByClause());
		$cl = new \Change\Db\Query\Clauses\OrderByClause($this->getSQLFragmentBuilder()->expressionList('o1'));
		$selectQuery->setOrderByClause($cl);

		self::assertEquals($cl, $selectQuery->getOrderByClause());
		return $selectQuery;
	}

	/**
	 * @depends testOrderByClause
	 * @param SelectQuery $selectQuery
	 * @return \Change\Db\Query\SelectQuery
	 */
	public function testCollateClause($selectQuery)
	{
		self::assertNull($selectQuery->getCollateClause());
		$c = new \Change\Db\Query\Clauses\CollateClause(new \Change\Db\Query\Expressions\Raw('collation'));
		$selectQuery->setCollateClause($c);
		self::assertEquals($c, $selectQuery->getCollateClause());
		return $selectQuery;
	}

	/**
	 * @depends testCollateClause
	 * @param SelectQuery $selectQuery
	 */
	public function testToSQL92String($selectQuery)
	{
		self::assertEquals('SELECT c1, c2 FROM "table" WHERE (w) GROUP BY g1, g2 HAVING (h) ORDER BY o1 COLLATE collation',
			$selectQuery->toSQL92String());
	}
}