<?php

namespace ChangeTests\Change\Db\Query;

class StatementBuilderTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\Query\StatementBuilder
	 */
	protected function getNewStatementBuilder()
	{
		return new \Change\Db\Query\StatementBuilder($this->getApplicationServices()->getDbProvider());
	}

	public function testConstruct()
	{
		$instance = $this->getNewStatementBuilder();
		self::assertTrue(true);

		try
		{
			$instance->insertQuery();
			self::fail('Call insert() before');
		}
		catch (\LogicException $e)
		{
			self::assertEquals('Call insert() before', $e->getMessage());
		}

		try
		{
			$instance->updateQuery();
			self::fail('Call update() before');
		}
		catch (\LogicException $e)
		{
			self::assertEquals('Call update() before', $e->getMessage());
		}

		try
		{
			$instance->deleteQuery();
			self::fail('Call delete() before');
		}
		catch (\LogicException $e)
		{
			self::assertEquals('Call delete() before', $e->getMessage());
		}
	}

	public function testGetFragmentBuilder()
	{
		$instance = $this->getNewStatementBuilder();
		self::assertInstanceOf(\Change\Db\Query\SQLFragmentBuilder::class, $instance->getFragmentBuilder());
	}

	public function testAddParameter()
	{
		$instance = $this->getNewStatementBuilder();
		try
		{
			$instance->addParameter('test');
			self::fail('Query not initialized');
		}
		catch (\LogicException $e)
		{
			self::assertEquals(42016, $e->getCode());
		}

		$instance->insert('test');
		$instance->addParameter('test');

		try
		{
			$instance->addParameter(['test']);
			self::fail('Argument 1 must be a Expressions\Parameter');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertEquals(42004, $e->getCode());
		}
	}

	public function testInsert()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert();
		self::assertNull($instance->insertQuery()->getInsertClause());

		$instance->insert('test');
		$ic = $instance->insertQuery()->getInsertClause();
		self::assertInstanceOf(\Change\Db\Query\Clauses\InsertClause::class, $ic);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Table::class, $ic->getTable());
		self::assertEquals('test', $ic->getTable()->getName());
		self::assertCount(0, $ic->getColumns());

		$instance->insert('test', 'f1');
		$q = $instance->insertQuery();
		self::assertCount(1, $q->getInsertClause()->getColumns());
	}

	public function testAddColumns()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert('test');
		$instance->addColumns('c1', 'c2');
		$q = $instance->insertQuery();
		self::assertCount(2, $q->getInsertClause()->getColumns());
	}

	public function testAddColumn()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert('test');
		$instance->addColumn('c1');
		$q = $instance->insertQuery();
		self::assertCount(1, $q->getInsertClause()->getColumns());

		$instance->addColumn('c2');
		self::assertCount(2, $q->getInsertClause()->getColumns());
	}

	public function testAddValues()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert('test');
		$instance->addValues('t1', 't2');
		$q = $instance->insertQuery();
		self::assertEquals(2, $q->getValuesClause()->getValuesList()->count());
	}

	public function testAddValue()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert('test');
		$instance->addValue('t1');
		$q = $instance->insertQuery();
		self::assertEquals(1, $q->getValuesClause()->getValuesList()->count());

		$instance->addValue('t1');
		self::assertEquals(2, $q->getValuesClause()->getValuesList()->count());
	}

	public function testUpdate()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->update();
		self::assertNull($instance->updateQuery()->getUpdateClause());

		$instance->update('test');
		$c = $instance->updateQuery()->getUpdateClause();
		self::assertInstanceOf(\Change\Db\Query\Clauses\UpdateClause::class, $c);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Table::class, $c->getTable());
		self::assertEquals('test', $c->getTable()->getName());
	}

	public function testAssign()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->update('test');
		$instance->assign('c1', 'text1');
		$c = $instance->updateQuery()->getSetClause();
		$l = $c->getSetList();
		self::assertEquals(1, $l->count());
		$array = $l->getList();
		$a1 = $array[0];
		/* @var $a1 \Change\Db\Query\Expressions\Assignment */
		self::assertInstanceOf(\Change\Db\Query\Expressions\Assignment::class, $a1);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Column::class, $a1->getLeftHandExpression());
		self::assertInstanceOf(\Change\Db\Query\Expressions\StringValue::class, $a1->getRightHandExpression());
	}

	public function testDelete()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->delete();
		self::assertNull($instance->deleteQuery()->getFromClause());

		$instance->delete('test');
		$c = $instance->deleteQuery()->getFromClause();
		self::assertInstanceOf(\Change\Db\Query\Clauses\FromClause::class, $c);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Table::class, $c->getTableExpression());
	}

	public function testWhere()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->delete();
		$instance->where($instance->getFragmentBuilder()->eq('i1', 'i2'));
		$c = $instance->deleteQuery()->getWhereClause();
		self::assertInstanceOf(\Change\Db\Query\Clauses\WhereClause::class, $c);
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $c->getPredicate());

		$instance = $this->getNewStatementBuilder();
		$instance->update();
		$instance->where($instance->getFragmentBuilder()->eq('i1', 'i2'));
		$c = $instance->updateQuery()->getWhereClause();
		self::assertInstanceOf(\Change\Db\Query\Clauses\WhereClause::class, $c);
		self::assertInstanceOf(\Change\Db\Query\Predicates\BinaryPredicate::class, $c->getPredicate());
	}

	public function testReset()
	{
		$instance = $this->getNewStatementBuilder();
		$instance->insert('test');

		$instance->reset();

		try
		{
			$instance->insertQuery();
			self::fail('Call insert() before');
		}
		catch (\LogicException $e)
		{
			self::assertEquals('Call insert() before', $e->getMessage());
		}
	}
}
