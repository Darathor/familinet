<?php
namespace ChangeTests\Db\Schema;

use Change\Db\Schema\FieldDefinition;

class FieldDefinitionTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$fd = new FieldDefinition('test');
		self::assertEquals('test', $fd->getName());
		self::assertEquals(FieldDefinition::VARCHAR, $fd->getType());
		self::assertCount(0, $fd->getOptions());
		self::assertNull($fd->getDefaultValue());
		self::assertTrue($fd->getNullable());
	}

	public function testName()
	{
		$fd = new FieldDefinition('test');
		$fd->setName('test2');
		self::assertEquals('test2', $fd->getName());
	}

	public function testType()
	{
		$fd = new FieldDefinition('test');
		$fd->setType(FieldDefinition::DATE);
		self::assertEquals(FieldDefinition::DATE, $fd->getType());
	}

	public function testDefaultValue()
	{
		$fd = new FieldDefinition('test');
		$fd->setDefaultValue('def');
		self::assertEquals('def', $fd->getDefaultValue());
	}

	public function testNullable()
	{
		$fd = new FieldDefinition('test');
		$fd->setNullable(false);
		self::assertFalse($fd->getNullable());
	}

	public function testOptions()
	{
		$fd = new FieldDefinition('test');
		$fd->setOptions(array('V1' => 'test1'));
		self::assertCount(1, $fd->getOptions());

		self::assertArrayHasKey('V1', $fd->getOptions());
		self::assertEquals('test1', $fd->getOption('V1'));
		self::assertNull($fd->getOption('V2'));

		$fd->setOption('V2', 'test2');
		self::assertEquals('test2', $fd->getOption('V2'));
		self::assertCount(2, $fd->getOptions());
	}

	public function testLength()
	{
		$fd = new FieldDefinition('test');
		$fd->setLength(255);
		self::assertCount(1, $fd->getOptions());
		self::assertEquals(255, $fd->getLength());
	}

	public function testCharset()
	{
		$fd = new FieldDefinition('test');
		$fd->setCharset('UTF-8');
		self::assertCount(1, $fd->getOptions());
		self::assertEquals('UTF-8', $fd->getCharset());
	}

	public function testCollation()
	{
		$fd = new FieldDefinition('test');
		$fd->setCollation('general');
		self::assertCount(1, $fd->getOptions());
		self::assertEquals('general', $fd->getCollation());
	}

	public function testAutoNumber()
	{
		$fd = new FieldDefinition('test');
		$fd->setAutoNumber(true);
		self::assertCount(1, $fd->getOptions());
		self::assertTrue($fd->getAutoNumber());
	}

	public function testPrecision()
	{
		$fd = new FieldDefinition('test');
		$fd->setPrecision(15);
		self::assertCount(1, $fd->getOptions());
		self::assertEquals(15, $fd->getPrecision());
	}

	public function testScale()
	{
		$fd = new FieldDefinition('test');
		$fd->setScale(4);
		self::assertCount(1, $fd->getOptions());
		self::assertEquals(4, $fd->getScale());
	}
}
