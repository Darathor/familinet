<?php
namespace ChangeTests\Db\Schema;

use Change\Db\Schema\KeyDefinition;
use Change\Db\Schema\FieldDefinition;

class KeyDefinitionTest extends \ChangeTests\Change\TestAssets\TestCase
{
	
	public function testConstruct()
	{
		$kd = new KeyDefinition();
		self::assertEquals(KeyDefinition::INDEX, $kd->getType());
		self::assertCount(0, $kd->getOptions());
		self::assertNull($kd->getName());
		self::assertNull($kd->getFields());
	}

	public function testName()
	{
		$kd = new KeyDefinition();
		$kd->setName('test2');
		self::assertEquals('test2', $kd->getName());
	}

	public function testType()
	{
		$kd = new KeyDefinition(null);
		self::assertTrue($kd->isIndex());
		self::assertFalse($kd->isPrimary());
		self::assertFalse($kd->isUnique());
		$kd->setType(KeyDefinition::PRIMARY);
		self::assertEquals(KeyDefinition::PRIMARY, $kd->getType());
		self::assertTrue($kd->isPrimary());
		self::assertFalse($kd->isIndex());

		$kd->setType(KeyDefinition::UNIQUE);
		self::assertTrue($kd->isUnique());
		self::assertFalse($kd->isPrimary());
	}

	public function testOptions()
	{
		$kd = new KeyDefinition();
		$kd->setOptions(array('V1' => 'test1'));
		self::assertCount(1, $kd->getOptions());

		self::assertArrayHasKey('V1', $kd->getOptions());
		self::assertEquals('test1', $kd->getOption('V1'));
		self::assertNull($kd->getOption('V2'));

		$kd->setOption('V2', 'test2');
		self::assertEquals('test2', $kd->getOption('V2'));
		self::assertCount(2, $kd->getOptions());
	}

	public function testFields()
	{
		$kd = new KeyDefinition(null);
		$kd->setFields(array(new FieldDefinition('f1')));
		self::assertCount(1, $kd->getFields());
		$r =  $kd->addField(new FieldDefinition('f2'));
		self::assertSame($r, $kd);
		self::assertCount(2, $kd->getFields());
	}
}
