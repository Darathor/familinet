<?php
namespace ChangeTests\Db\Schema;

use Change\Db\Schema\TableDefinition;
use Change\Db\Schema\FieldDefinition;
use Change\Db\Schema\KeyDefinition;

class TableDefinitionTest extends \ChangeTests\Change\TestAssets\TestCase
{
	
	public function testConstruct()
	{
		$td = new TableDefinition('test');
		self::assertEquals('test', $td->getName());
		self::assertCount(0, $td->getOptions());
		self::assertCount(0, $td->getFields());
		self::assertCount(0, $td->getKeys());
		self::assertFalse($td->isValid());
	}

	public function testName()
	{
		$td = new TableDefinition('test');
		$td->setName('test2');
		self::assertEquals('test2', $td->getName());
	}
	public function testOptions()
	{
		$td = new TableDefinition('test');
		$td->setOptions(array('V1' => 'test1'));
		self::assertCount(1, $td->getOptions());

		self::assertArrayHasKey('V1', $td->getOptions());
		self::assertEquals('test1', $td->getOption('V1'));
		self::assertNull($td->getOption('V2'));

		$td->setOption('V2', 'test2');
		self::assertEquals('test2', $td->getOption('V2'));
		self::assertCount(2, $td->getOptions());
	}

	public function testFields()
	{
		$td = new TableDefinition('test');
		$td->setFields(array(new FieldDefinition('f1')));
		self::assertCount(1, $td->getFields());
		self::assertTrue($td->isValid());

		$r =  $td->addField(new FieldDefinition('f2'));
		self::assertSame($r, $td);
		self::assertCount(2, $td->getFields());
	}

	public function testKeys()
	{
		$td = new TableDefinition('test');
		$td->setKeys(array(new KeyDefinition()));
		self::assertCount(1, $td->getKeys());

		$r =  $td->addKey(new KeyDefinition());
		self::assertSame($r, $td);
		self::assertCount(2, $td->getKeys());
	}

	public function testCharset()
	{
		$td = new TableDefinition('test');
		$td->setCharset('UTF-8');
		self::assertCount(1, $td->getOptions());
		self::assertEquals('UTF-8', $td->getCharset());
	}

	public function testCollation()
	{
		$td = new TableDefinition('test');
		$td->setCollation('general');
		self::assertCount(1, $td->getOptions());
		self::assertEquals('general', $td->getCollation());
	}
}
