<?php
namespace ChangeTests\Db;

use Change\Db\ScalarType;
use Change\Db\SqlMapping;
use Change\Documents\Property;

class SqlMappingTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Db\SqlMapping
	 */
	public function testConstruct()
	{
		$sqlMapping = new SqlMapping();
		self::assertTrue(true);
		return $sqlMapping;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDocumentTableName(SqlMapping $sqlMapping)
	{
		self::assertEquals('rbs_website_doc_page', $sqlMapping->getDocumentTableName('Rbs_Website_Page'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDocumentRelationTableName(SqlMapping $sqlMapping)
	{
		self::assertEquals('rbs_website_rel_page', $sqlMapping->getDocumentRelationTableName('Rbs_Website_Page'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDocumentI18nTableName(SqlMapping $sqlMapping)
	{
		self::assertEquals('rbs_website_doc_page_i18n', $sqlMapping->getDocumentI18nTableName('Rbs_Website_Page'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetTreeTableName(SqlMapping $sqlMapping)
	{
		self::assertEquals('rbs_website_tree', $sqlMapping->getTreeTableName('Rbs_Website'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDocumentFieldName(SqlMapping $sqlMapping)
	{
		self::assertEquals('document_id', $sqlMapping->getDocumentFieldName('id'));
		self::assertEquals('document_model', $sqlMapping->getDocumentFieldName('model'));
		self::assertEquals('treename', $sqlMapping->getDocumentFieldName('treeName'));

		self::assertEquals('test_name', $sqlMapping->getDocumentFieldName('test_Name'));
		self::assertEquals('test_name', $sqlMapping->getDocumentFieldName('TEST_NAME'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDbScalarType(SqlMapping $sqlMapping)
	{
		self::assertEquals(ScalarType::BOOLEAN, $sqlMapping->getDbScalarType(Property::TYPE_BOOLEAN));

		self::assertEquals(ScalarType::INTEGER, $sqlMapping->getDbScalarType(Property::TYPE_DOCUMENT));
		self::assertEquals(ScalarType::INTEGER, $sqlMapping->getDbScalarType(Property::TYPE_DOCUMENTARRAY));
		self::assertEquals(ScalarType::INTEGER, $sqlMapping->getDbScalarType(Property::TYPE_DOCUMENTID));
		self::assertEquals(ScalarType::INTEGER, $sqlMapping->getDbScalarType(Property::TYPE_INTEGER));

		self::assertEquals(ScalarType::DATETIME, $sqlMapping->getDbScalarType(Property::TYPE_DATE));
		self::assertEquals(ScalarType::DATETIME, $sqlMapping->getDbScalarType(Property::TYPE_DATETIME));

		self::assertEquals(ScalarType::DECIMAL, $sqlMapping->getDbScalarType(Property::TYPE_FLOAT));
		self::assertEquals(ScalarType::DECIMAL, $sqlMapping->getDbScalarType(Property::TYPE_DECIMAL));

		self::assertEquals(ScalarType::TEXT, $sqlMapping->getDbScalarType(Property::TYPE_JSON));
		self::assertEquals(ScalarType::TEXT, $sqlMapping->getDbScalarType(Property::TYPE_LONGSTRING));
		self::assertEquals(ScalarType::TEXT, $sqlMapping->getDbScalarType(Property::TYPE_STORAGEURI));
		self::assertEquals(ScalarType::TEXT, $sqlMapping->getDbScalarType(Property::TYPE_RICHTEXT));

		self::assertEquals(ScalarType::LOB, $sqlMapping->getDbScalarType(Property::TYPE_INLINE));
		self::assertEquals(ScalarType::LOB, $sqlMapping->getDbScalarType(Property::TYPE_INLINEARRAY));

		self::assertEquals(ScalarType::STRING, $sqlMapping->getDbScalarType('Unknown'));
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Db\SqlMapping $sqlMapping
	 */
	public function testGetDocumentTableNames(SqlMapping $sqlMapping)
	{
		self::assertEquals('change_document', $sqlMapping->getDocumentIndexTableName());
		self::assertEquals('change_document_metas', $sqlMapping->getDocumentMetasTableName());
		self::assertEquals('change_document_deleted', $sqlMapping->getDocumentDeletedTable());
		self::assertEquals('change_document_correction', $sqlMapping->getDocumentCorrectionTable());
		self::assertEquals('change_oauth', $sqlMapping->getOAuthTable());
		self::assertEquals('change_oauth_application', $sqlMapping->getOAuthApplicationTable());
		self::assertEquals('change_path_rule', $sqlMapping->getPathRuleTable());
	}
}
