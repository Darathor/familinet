<?php
namespace ChangeTests\Change\Documents;

class AbstractDocumentPropertiesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function testStringPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPStr());
		self::assertFalse($basicDoc->isPropertyModified('pStr'));
		self::assertSame($basicDoc, $basicDoc->setPStr('toto'));
		self::assertEquals('toto', $basicDoc->getPStr());
		self::assertTrue($basicDoc->isPropertyModified('pStr'));
		$basicDoc->setPStr(null);
		self::assertNull($basicDoc->getPStr());

		self::assertNull($basicDoc->getPStrOldValue());
		$basicDoc->setPStr('default');
		self::assertTrue($basicDoc->isPropertyModified('pStr'));
		$basicDoc->removeOldPropertyValue('pStr');
		self::assertFalse($basicDoc->isPropertyModified('pStr'));

		$basicDoc->setPStr(null);
		self::assertNull($basicDoc->getPStr());
		self::assertTrue($basicDoc->isPropertyModified('pStr'));
		self::assertEquals('default', $basicDoc->getPStrOldValue());
	}

	public function testLocalizedStringPropertyAccessors()
	{
		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$localizedDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		self::assertFalse(method_exists($localizedDoc, 'getPLStr'));
		self::assertFalse(method_exists($localizedDoc, 'setPLStr'));
		$cl = $localizedDoc->getCurrentLocalization();
		self::assertNull($cl->getPLStr());
		self::assertFalse($localizedDoc->isPropertyModified('pLStr'));
		self::assertFalse($cl->isPropertyModified('pLStr'));

		self::assertSame($cl, $cl->setPLStr('toto'));
		self::assertEquals('toto', $cl->getPLStr());
		self::assertTrue($localizedDoc->isPropertyModified('pLStr'));
		self::assertTrue($cl->isPropertyModified('pLStr'));
		$cl->setPLStr(null);
		self::assertNull($cl->getPLStr());

		self::assertNull($cl->getPLStrOldValue());
		$cl->setPLStr('default');
		self::assertTrue($cl->isPropertyModified('pLStr'));
		$localizedDoc->removeOldPropertyValue('pLStr');
		self::assertFalse($localizedDoc->isPropertyModified('pLStr'));

		$cl->setPLStr(null);
		self::assertNull($cl->getPLStr());
		self::assertTrue($localizedDoc->isPropertyModified('pLStr'));
		self::assertEquals('default', $cl->getPLStrOldValue());
	}

	public function testBooleanPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertFalse($basicDoc->getPBool());
		self::assertFalse($basicDoc->isPropertyModified('pBool'));
		self::assertSame($basicDoc, $basicDoc->setPBool(true));
		self::assertTrue($basicDoc->getPBool());
		self::assertTrue($basicDoc->isPropertyModified('pBool'));
		$basicDoc->setPBool(false);
		self::assertFalse($basicDoc->getPBool());
		self::assertFalse($basicDoc->isPropertyModified('pBool'));

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$localizedDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		self::assertFalse(method_exists($localizedDoc, 'getPLBool'));
		self::assertFalse(method_exists($localizedDoc, 'setPLBool'));
		$cl = $localizedDoc->getCurrentLocalization();

		self::assertFalse($cl->getPLBool());
		self::assertFalse($cl->isPropertyModified('pLBool'));
		self::assertFalse($localizedDoc->isPropertyModified('pLBool'));
		self::assertSame($cl, $cl->setPLBool(true));
		self::assertTrue($cl->getPLBool());
		self::assertTrue($cl->isPropertyModified('pLBool'));
		self::assertTrue($localizedDoc->isPropertyModified('pLBool'));
		$cl->setPLBool(false);
		self::assertFalse($cl->getPLBool());
		self::assertFalse($cl->isPropertyModified('pLBool'));
	}

	public function testIntegerPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPInt());

		self::assertSame($basicDoc, $basicDoc->setPInt(10));
		self::assertEquals(10, $basicDoc->getPInt());

		$basicDoc->setPInt(null);
		self::assertNull($basicDoc->getPInt());
	}

	public function testFloatPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPFloat());
		self::assertFalse($basicDoc->isPropertyModified('pFloat'));
		self::assertSame($basicDoc, $basicDoc->setPFloat(10.1));
		self::assertEquals(10.1, $basicDoc->getPFloat());
		self::assertTrue($basicDoc->isPropertyModified('pFloat'));
		$basicDoc->setPFloat(null);
		self::assertNull($basicDoc->getPFloat());
		self::assertFalse($basicDoc->isPropertyModified('pFloat'));
		$basicDoc->setPFloat(0);
		self::assertSame(0.0, $basicDoc->getPFloat());
		self::assertTrue($basicDoc->isPropertyModified('pFloat'));
	}

	public function testDecimalPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPDec());

		self::assertSame($basicDoc, $basicDoc->setPDec(10.1));
		self::assertEquals(10.1, $basicDoc->getPDec());

		$basicDoc->setPDec(null);
		self::assertNull($basicDoc->getPDec());

		$basicDoc->setPDec(0);
		self::assertSame(0.0, $basicDoc->getPDec());
	}

	public function testDateTimePropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPDaTi());

		$date = new \DateTime();
		self::assertSame($basicDoc, $basicDoc->setPDaTi($date));
		self::assertSame($date->format('c'), $basicDoc->getPDaTi()->format('c'));

		$basicDoc->setPDaTi(null);
		self::assertNull($basicDoc->getPDaTi());

		self::assertSame($basicDoc, $basicDoc->setPDaTi(new \DateTime('2013-06-20T17:45:04+02:00')));
		self::assertEquals('2013-06-20T17:45:04+02:00', $basicDoc->getPDaTi()->format('c'));
	}

	public function testDatePropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPDa());

		$date = new \DateTime();
		self::assertSame($basicDoc, $basicDoc->setPDa($date));
		self::assertSame($date->format('Y-m-d'), $basicDoc->getPDa()->format('Y-m-d'));

		$basicDoc->setPDa(null);
		self::assertNull($basicDoc->getPDa());

		self::assertSame($basicDoc, $basicDoc->setPDa(new \DateTime('2013-06-20')));
		self::assertEquals('2013-06-20', $basicDoc->getPDa()->format('Y-m-d'));
	}

	public function testLongStringPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPText());

		$text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor.

		Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

		Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.';
		self::assertSame($basicDoc, $basicDoc->setPText($text));
		self::assertSame($text, $basicDoc->getPText());

		$basicDoc->setPText(null);
		self::assertNull($basicDoc->getPText());
	}

	public function testStorageUriPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPStorUri());

		$text = 'change://tmp/test.txt';
		self::assertSame($basicDoc, $basicDoc->setPStorUri($text));
		self::assertSame($text, $basicDoc->getPStorUri());
		$obj = $basicDoc->getPStorUriItemInfo($this->getApplicationServices()->getStorageManager());
		self::assertInstanceOf(\Change\Storage\ItemInfo::class, $obj);
		self::assertEquals($text, $obj->getPathname());
		$basicDoc->setPStorUri(null);
		self::assertNull($basicDoc->getPText());

		$basicDoc->setPStorUri('http://tmp/test.txt');
		$l = new \Change\Documents\Events\ValidateListener();
		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_CREATE, $basicDoc, $this->getDefaultEventArguments());
		$l->onValidate($event);
		$pe = $event->getParam('propertiesErrors');
		self::assertArrayHasKey('pStorUri', $pe);
		self::assertEquals('\'http://tmp/test.txt\' doit être une URI de stockage valide.', $pe['pStorUri'][0]);

		$basicDoc->setPStorUri($text);
		$event = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_CREATE, $basicDoc, $this->getDefaultEventArguments());
		$l->onValidate($event);
		$pe = $event->getParam('propertiesErrors');
		self::assertArrayNotHasKey('pStorUri', $pe);
	}

	public function testJSONPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		self::assertNull($basicDoc->getPJson());
		self::assertFalse($basicDoc->isPropertyModified('pJson'));

		$data = array('toto' => 'youpi', 'plop' => 12.2, 1 => 'test');
		$json = '{"toto":"youpi","plop":12.2,"1":"test"}';
		self::assertSame($basicDoc, $basicDoc->setPJson($data));
		self::assertEquals($data, $basicDoc->getPJson());
		self::assertEquals($json, $basicDoc->getPJsonString());
		self::assertTrue($basicDoc->isPropertyModified('pJson'));
		$basicDoc->setPJson(null);
		self::assertFalse($basicDoc->isPropertyModified('pJson'));
		self::assertNull($basicDoc->getPJson());

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$localizedDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		self::assertFalse(method_exists($localizedDoc, 'getPLJson'));
		self::assertFalse(method_exists($localizedDoc, 'setPLJson'));
		$cl = $localizedDoc->getCurrentLocalization();

		self::assertNull($cl->getPLJson());
		self::assertFalse($cl->isPropertyModified('pLJson'));
		self::assertFalse($localizedDoc->isPropertyModified('pLJson'));

		$data = array('toto' => 'youpi', 'plop' => 12.2, 1 => 'test');
		self::assertSame($cl, $cl->setPLJson($data));
		self::assertEquals($data, $cl->getPLJson());

		self::assertTrue($cl->isPropertyModified('pLJson'));
		self::assertTrue($localizedDoc->isPropertyModified('pLJson'));
		$cl->setPLJson(null);
		self::assertFalse($localizedDoc->isPropertyModified('pLJson'));
		self::assertNull($cl->getPLJson());
	}

	public function testRichtextPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		$string = str_repeat('Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 1000);

		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $basicDoc->getPRt());
		self::assertTrue($basicDoc->getPRt()->isEmpty());
		self::assertFalse($basicDoc->getPRt()->isModified());
		self::assertFalse($basicDoc->isPropertyModified('pRt'));


		$basicDoc->getPRt()->setRawText($string);
		self::assertSame($string, $basicDoc->getPRt()->getRawText());
		self::assertTrue($basicDoc->isPropertyModified('pRt'));
		self::assertTrue($basicDoc->getPRt()->isModified());
		self::assertSame($basicDoc, $basicDoc->setPRt(null));
		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $basicDoc->getPRt());
		self::assertTrue($basicDoc->getPRt()->isEmpty());
		self::assertFalse($basicDoc->getPRt()->isModified());
		self::assertFalse($basicDoc->isPropertyModified('pRt'));


		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$localizedDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		self::assertFalse(method_exists($localizedDoc, 'getPLRt'));
		self::assertFalse(method_exists($localizedDoc, 'setPLRt'));
		$cl = $localizedDoc->getCurrentLocalization();

		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $cl->getPLRt());
		self::assertTrue($cl->getPLRt()->isEmpty());
		self::assertFalse($cl->getPLRt()->isModified());
		self::assertFalse($cl->isPropertyModified('pLRt'));
		self::assertFalse($localizedDoc->isPropertyModified('pLRt'));

		$cl->getPLRt()->setRawText($string);
		self::assertSame($string, $cl->getPLRt()->getRawText());
		self::assertTrue($localizedDoc->isPropertyModified('pLRt'));
		self::assertTrue($cl->isPropertyModified('pLRt'));

		self::assertTrue($cl->getPLRt()->isModified());
		self::assertSame($cl, $cl->setPLRt(null));

		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $cl->getPLRt());
		self::assertTrue($cl->getPLRt()->isEmpty());
		self::assertFalse($cl->getPLRt()->isModified());
		self::assertFalse($localizedDoc->isPropertyModified('pLRt'));
	}

	public function testDocumentIdPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		/* @var $doc1 \Project\Tests\Documents\Localized */
		$doc1 = $this->getNewReadonlyDocument('Project_Tests_Localized', 200);
		$doc1->setPStr('Doc1');
		$doc1Id = $doc1->getId();

		self::assertSame(0, $basicDoc->getPDocId());
		self::assertFalse($basicDoc->isPropertyModified('pDocId'));
		self::assertSame($basicDoc, $basicDoc->setPDocId($doc1Id));
		self::assertEquals($doc1Id, $basicDoc->getPDocId());
		self::assertSame($doc1, $basicDoc->getPDocIdInstance());
		self::assertTrue($basicDoc->isPropertyModified('pDocId'));

		$basicDoc->setPDocId(null);
		self::assertSame(0, $basicDoc->getPDocId());
		self::assertFalse($basicDoc->isPropertyModified('pDocId'));
		$basicDoc->setPDocId(0);
		self::assertSame(0, $basicDoc->getPDocId());
		self::assertFalse($basicDoc->isPropertyModified('pDocId'));
	}

	public function testDocumentPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		/* @var $doc1 \Project\Tests\Documents\Localized */
		$doc1 = $this->getNewReadonlyDocument('Project_Tests_Localized', 201);
		$doc1->setPStr('Doc1');
		$doc1Id = $doc1->getId();

		self::assertNull($basicDoc->getPDocInst());
		self::assertSame(0, $basicDoc->getPDocInstId());
		self::assertFalse($basicDoc->isPropertyModified('pDocInst'));

		self::assertSame($basicDoc, $basicDoc->setPDocInst($doc1));
		self::assertSame($doc1, $basicDoc->getPDocInst());
		self::assertEquals($doc1Id, $basicDoc->getPDocInstId());
		self::assertTrue($basicDoc->isPropertyModified('pDocInst'));

		$basicDoc->setPDocInst(null);
		self::assertNull($basicDoc->getPDocInst());
		self::assertSame(0, $basicDoc->getPDocInstId());
		self::assertFalse($basicDoc->isPropertyModified('pDocInst'));
	}

	public function testDocumentArrayPropertyAccessors()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		/* @var $doc1 \Project\Tests\Documents\Localized */
		$doc1 = $this->getNewReadonlyDocument('Project_Tests_Localized', 202);
		$doc1->setPStr('Doc1');
		$doc1Id = $doc1->getId();

		/* @var $doc2 \Project\Tests\Documents\Localized */
		$doc2 = $this->getNewReadonlyDocument('Project_Tests_Localized', 203);
		$doc2->setPStr('Doc2');
		$doc2Id = $doc2->getId();

		/* @var $doc3 \Project\Tests\Documents\Localized */
		$doc3 = $this->getNewReadonlyDocument('Project_Tests_Localized', 204);
		$doc3->setPStr('Doc3');
		$doc3Id = $doc3->getId();

		self::assertInstanceOf(\Change\Documents\DocumentArrayProperty::class, $basicDoc->getPDocArr());
		self::assertEquals('Project_Tests_Localized', $basicDoc->getPDocArr()->getModelName());

		self::assertSame($basicDoc, $basicDoc->setPDocArr(array($doc1, $doc2)));
		self::assertEquals(2, $basicDoc->getPDocArr()->count());
		self::assertEquals(array($doc1, $doc2), $basicDoc->getPDocArr()->toArray());

		self::assertSame($basicDoc->getPDocArr(),  $basicDoc->getPDocArr()->add($doc2));
		self::assertEquals(array($doc1, $doc2), $basicDoc->getPDocArr()->toArray());
		self::assertEquals(2, $basicDoc->getPDocArr()->count());
		$basicDoc->getPDocArr()->add($doc3);
		self::assertEquals(array($doc1, $doc2, $doc3), $basicDoc->getPDocArr()->toArray());
		self::assertEquals(3, $basicDoc->getPDocArr()->count());
		self::assertEquals(array($doc1Id, $doc2Id, $doc3Id), $basicDoc->getPDocArrIds());

		self::assertSame($basicDoc->getPDocArr(),  $basicDoc->getPDocArr()->remove($doc1));
		self::assertEquals(2, $basicDoc->getPDocArr()->count());
		self::assertEquals(array($doc2, $doc3), $basicDoc->getPDocArr()->toArray());
		$basicDoc->getPDocArr()[1] = $doc1;
		self::assertEquals(2, $basicDoc->getPDocArr()->count());
		self::assertEquals(array($doc2, $doc1), $basicDoc->getPDocArr()->toArray());
		self::assertEquals(1, $basicDoc->getPDocArr()->indexOf($doc1));
		self::assertEquals(0, $basicDoc->getPDocArr()->indexOf($doc2));

		self::assertFalse($basicDoc->getPDocArr()->indexOf($doc3));

		$basicDoc->setPDocArr(array());
		self::assertEquals(array(), $basicDoc->getPDocArr()->toArray());
		self::assertEquals(array(), $basicDoc->getPDocArr()->getIds());
		self::assertEquals(0, $basicDoc->getPDocArr()->count());
	}
}