<?php
namespace ChangeTests\Change\Documents;

use Change\Documents\AbstractDocument;
use Change\Documents\Events\Event;
use Change\Documents\Interfaces\Publishable;
use Change\Documents\Correction;

class AbstractDocumentTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	public function testSerialize()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$str = serialize($basicDoc);
		self::assertEquals(serialize(null), $str);
	}

	public function testBasic()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$basicDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertInstanceOf(\Project\Tests\Documents\Basic::class, $basicDoc);
		self::assertEquals('Project_Tests_Basic', $basicDoc->getDocumentModelName());

		$eventCollection = new \ArrayObject();
		$callBack = static function (Event $event) use ($eventCollection) {
			$eventCollection[] = $event->getName();
		};
		$basicDoc->getEventManager()->attach('*', $callBack);

		self::assertEquals(AbstractDocument::STATE_NEW, $basicDoc->getPersistentState());
		self::assertLessThan(0, $basicDoc->getId());
		self::assertTrue($basicDoc->isNew());
		self::assertFalse($basicDoc->isDeleted());
		self::assertFalse($basicDoc->hasModifiedProperties());
		self::assertEquals([], $basicDoc->getModifiedPropertyNames());
		self::assertFalse($basicDoc->hasModifiedMetas());

		self::assertNull($basicDoc->getPStr());
		self::assertNull($basicDoc->getPStrOldValue());

		self::assertInstanceOf(\DateTime::class, $basicDoc->getCreationDate());
		self::assertInstanceOf(\DateTime::class, $basicDoc->getModificationDate());

		$event = new Event(Event::EVENT_CREATE, $basicDoc, $this->getDefaultEventArguments());
		$validation = new \Change\Documents\Events\ValidateListener();
		$validation->onValidate($event);
		$errors = $event->getParam('propertiesErrors');

		self::assertCount(1, $errors);
		self::assertArrayHasKey('pStr', $errors);

		$basicDoc->setPStr('string');
		self::assertEquals('string', $basicDoc->getPStr());
		self::assertNull($basicDoc->getPStrOldValue());

		$basicDoc->setPInt(50);
		$basicDoc->setPFloat(0.03);

		$event->setParam('propertiesErrors', null);
		$validation->onValidate($event);
		$errors = $event->getParam('propertiesErrors');
		self::assertNull($errors);
		self::assertTrue($basicDoc->hasModifiedProperties());
		self::assertEquals(['pStr', 'pInt', 'pFloat'], $basicDoc->getModifiedPropertyNames());

		$basicDoc->save();
		self::assertEquals([Event::EVENT_CREATE, Event::EVENT_CREATED], $eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertGreaterThan(0, $basicDoc->getId());
		self::assertEquals(AbstractDocument::STATE_LOADED, $basicDoc->getPersistentState());
		self::assertFalse($basicDoc->isNew());
		self::assertFalse($basicDoc->isDeleted());
		self::assertFalse($basicDoc->hasModifiedProperties());

		$basicDoc->setPStr('string 2');
		self::assertTrue($basicDoc->hasModifiedProperties());
		self::assertTrue($basicDoc->isPropertyModified('pStr'));
		self::assertEquals('string', $basicDoc->getPStrOldValue());

		$basicDoc->setPStr('string');
		self::assertFalse($basicDoc->hasModifiedProperties());
		self::assertFalse($basicDoc->isPropertyModified('pStr'));
		self::assertNull($basicDoc->getPStrOldValue());

		$basicDoc->setPStr('string 2');
		$basicDoc->setPDec(8.7);
		self::assertTrue($basicDoc->hasModifiedProperties());
		self::assertCount(2, $basicDoc->getModifiedPropertyNames());

		self::assertNull($basicDoc->getPDecOldValue());
		self::assertEquals('string', $basicDoc->getPStrOldValue());

		$basicDoc->save();
		self::assertEquals([Event::EVENT_UPDATE, Event::EVENT_UPDATED], $eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertEquals(AbstractDocument::STATE_LOADED, $basicDoc->getPersistentState());
		self::assertFalse($basicDoc->hasModifiedProperties());
		self::assertEquals('string 2', $basicDoc->getPStr());
		self::assertEquals('8.7', $basicDoc->getPDec());

		$documentId = $basicDoc->getId();
		$this->getApplicationServices()->getDocumentManager()->reset();

		/* @var $basicDoc2 \Project\Tests\Documents\Basic */
		$basicDoc2 = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($documentId);
		$basicDoc2->getEventManager()->attach('*', $callBack);

		self::assertInstanceOf(\Project\Tests\Documents\Basic::class, $basicDoc2);
		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $basicDoc2->getPersistentState());
		self::assertNotSame($basicDoc, $basicDoc2);

		self::assertEquals('string 2', $basicDoc2->getPStr());
		self::assertEquals(AbstractDocument::STATE_LOADED, $basicDoc2->getPersistentState());

		$basicDoc2->delete();
		self::assertEquals([Event::EVENT_LOADED, Event::EVENT_DELETE, Event::EVENT_DELETED], $eventCollection->getArrayCopy());
		self::assertEquals(AbstractDocument::STATE_DELETED, $basicDoc2->getPersistentState());
		self::assertTrue($basicDoc2->isDeleted());

		$datas = $this->getApplicationServices()->getDocumentManager()->getBackupData($documentId);
		self::assertArrayHasKey('pStr', $datas);
		self::assertEquals('string 2', $datas['pStr']);
		self::assertArrayHasKey('deletiondate', $datas);
		self::assertInstanceOf(\DateTime::class, $datas['deletiondate']);

		$this->getApplicationServices()->getDocumentManager()->reset();
	}

	public function testLocalized()
	{
		$dm = $this->getApplicationServices()->getDocumentManager();

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$localizedDoc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$eventCollection = new \ArrayObject();
		$callBack = static function (Event $event) use ($eventCollection) {
			$eventCollection[] = $event->getName();
		};
		$localizedDoc->getEventManager()->attach('*', $callBack);

		$dm->pushLCID('fr_FR');

		self::assertInstanceOf(\Project\Tests\Documents\Localized::class, $localizedDoc);
		self::assertEquals('Project_Tests_Localized', $localizedDoc->getDocumentModelName());

		self::assertEquals(AbstractDocument::STATE_NEW, $localizedDoc->getPersistentState());
		self::assertLessThan(0, $localizedDoc->getId());
		self::assertTrue($localizedDoc->isNew());
		self::assertFalse($localizedDoc->isDeleted());
		self::assertEquals([], $localizedDoc->getModifiedPropertyNames());
		self::assertFalse($localizedDoc->hasModifiedProperties());
		self::assertFalse($localizedDoc->hasModifiedMetas());

		self::assertEquals('fr_FR', $localizedDoc->getCurrentLCID());
		$cl = $localizedDoc->getCurrentLocalization();
		self::assertEquals('fr_FR', $cl->getLCID());

		self::assertNull($localizedDoc->getRefLCID());

		self::assertNull($localizedDoc->getPStr());
		self::assertNull($localizedDoc->getPStrOldValue());

		self::assertNull($cl->getPLStr());
		self::assertNull($cl->getPLStrOldValue());

		self::assertInstanceOf(\DateTime::class, $cl->getCreationDate());
		self::assertInstanceOf(\DateTime::class, $cl->getModificationDate());

		$event = new Event(Event::EVENT_CREATE, $localizedDoc, $this->getDefaultEventArguments());
		$validation = new \Change\Documents\Events\ValidateListener();
		$validation->onValidate($event);
		$errors = $event->getParam('propertiesErrors');

		self::assertCount(2, $errors);
		self::assertArrayHasKey('pStr', $errors);
		self::assertArrayHasKey('pLStr', $errors);

		$localizedDoc->setPStr('string');
		self::assertEquals('string', $localizedDoc->getPStr());
		self::assertNull($localizedDoc->getPStrOldValue());

		$cl->setPLStr('string FR');
		self::assertEquals('string FR', $cl->getPLStr());
		self::assertNull($cl->getPLStrOldValue());

		$localizedDoc->setRefLCID('fr_FR');
		$localizedDoc->setPInt(50);
		$localizedDoc->setPFloat(0.03);

		$event->setParam('propertiesErrors', null);
		$validation->onValidate($event);
		$errors = $event->getParam('propertiesErrors');

		self::assertNull($errors);
		self::assertTrue($localizedDoc->hasModifiedProperties());
		self::assertEquals(['refLCID', 'pStr', 'pInt', 'pFloat', 'pLStr'],
			$localizedDoc->getModifiedPropertyNames());

		$localizedDoc->save();
		self::assertEquals([Event::EVENT_CREATE, Event::EVENT_CREATED], $eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertFalse($localizedDoc->hasModifiedProperties());
		self::assertEquals([], $localizedDoc->getModifiedPropertyNames());

		self::assertGreaterThan(0, $localizedDoc->getId());
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc->getPersistentState());
		self::assertFalse($localizedDoc->isNew());
		self::assertFalse($localizedDoc->isDeleted());
		self::assertFalse($localizedDoc->hasModifiedProperties());

		$localizedDoc->getCurrentLocalization()->setPLStr('string FR 2');

		self::assertTrue($localizedDoc->hasModifiedProperties());
		self::assertTrue($localizedDoc->isPropertyModified('pLStr'));
		self::assertEquals('string FR', $localizedDoc->getCurrentLocalization()->getPLStrOldValue());

		$localizedDoc->getCurrentLocalization()->setPLStr('string FR');
		self::assertFalse($localizedDoc->hasModifiedProperties());
		self::assertFalse($localizedDoc->isPropertyModified('pLStr'));
		self::assertNull($localizedDoc->getCurrentLocalization()->getPLStrOldValue());

		$localizedDoc->getCurrentLocalization()->setPLStr('string FR 2');
		$localizedDoc->getCurrentLocalization()->setPLDec(8.7);
		self::assertTrue($localizedDoc->hasModifiedProperties());
		self::assertCount(2, $localizedDoc->getModifiedPropertyNames());

		self::assertNull($localizedDoc->getCurrentLocalization()->getPLDecOldValue());
		self::assertEquals('string FR', $localizedDoc->getCurrentLocalization()->getPLStrOldValue());

		$localizedDoc->save();
		self::assertEquals([Event::EVENT_UPDATE, Event::EVENT_UPDATED], $eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc->getPersistentState());
		self::assertFalse($localizedDoc->hasModifiedProperties());
		self::assertEquals('string FR 2', $localizedDoc->getCurrentLocalization()->getPLStr());
		self::assertEquals('8.7', $localizedDoc->getCurrentLocalization()->getPLDec());

		$documentId = $localizedDoc->getId();

		$dm->popLCID();

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$dm->pushLCID('en_US');

		self::assertEquals('en_US', $localizedDoc->getCurrentLCID());
		self::assertEquals('en_US', $localizedDoc->getCurrentLocalization()->getLCID());

		self::assertEquals(AbstractDocument::STATE_NEW, $localizedDoc->getCurrentLocalization()->getPersistentState());
		self::assertNull($localizedDoc->getCurrentLocalization()->getPLStr());
		$localizedDoc->getCurrentLocalization()->setPLStr('string EN');
		self::assertTrue($localizedDoc->isPropertyModified('pLStr'));
		$localizedDoc->save();
		self::assertEquals([Event::EVENT_LOCALIZED_LOADED, Event::EVENT_UPDATE, Event::EVENT_LOCALIZED_CREATED, Event::EVENT_UPDATED],
			$eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertFalse($localizedDoc->hasModifiedProperties());

		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc->getCurrentLocalization()->getPersistentState());
		$dm->popLCID();

		$dm->pushLCID('fr_FR');
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc->getCurrentLocalization()->getPersistentState());
		self::assertEquals('fr_FR', $localizedDoc->getCurrentLocalization()->getLCID());
		self::assertEquals('string FR 2', $localizedDoc->getCurrentLocalization()->getPLStr());
		$dm->popLCID();

		$dm->pushLCID('en_US');
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc->getCurrentLocalization()->getPersistentState());
		self::assertEquals('en_US', $localizedDoc->getCurrentLocalization()->getLCID());
		self::assertEquals('string EN', $localizedDoc->getCurrentLocalization()->getPLStr());
		$dm->popLCID();

		$dm->reset();

		$dm->pushLCID('en_US');

		/* @var $localizedDoc2 \Project\Tests\Documents\Localized */
		$localizedDoc2 = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($documentId);
		$localizedDoc2->getEventManager()->attach('*', $callBack);
		self::assertInstanceOf(\Project\Tests\Documents\Localized::class, $localizedDoc2);
		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $localizedDoc2->getPersistentState());
		self::assertNotSame($localizedDoc, $localizedDoc2);

		self::assertEquals('string', $localizedDoc2->getPStr());
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc2->getPersistentState());

		$localizedDoc2->deleteCurrentLocalization();
		self::assertEquals([Event::EVENT_LOADED, Event::EVENT_LOCALIZED_LOADED, Event::EVENT_LOCALIZED_DELETED], $eventCollection->getArrayCopy());
		$eventCollection->exchangeArray([]);

		self::assertEquals(AbstractDocument::STATE_DELETED, $localizedDoc2->getCurrentLocalization()->getPersistentState());
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedDoc2->getPersistentState());

		$localizedDoc2->delete();
		self::assertEquals([Event::EVENT_LOCALIZED_LOADED, Event::EVENT_DELETE, Event::EVENT_DELETED], $eventCollection->getArrayCopy());
		self::assertTrue($localizedDoc2->isDeleted());

		$data = $this->getApplicationServices()->getDocumentManager()->getBackupData($documentId);

		self::assertArrayHasKey('pStr', $data);
		self::assertEquals('string', $data['pStr']);
		self::assertArrayHasKey('deletiondate', $data);
		self::assertInstanceOf(\DateTime::class, $data['deletiondate']);

		self::assertArrayHasKey('LCID', $data);
		self::assertEquals('string FR 2', $data['LCID']['fr_FR']['pLStr']);
		$dm->popLCID();

		$this->getApplicationServices()->getDocumentManager()->reset();
	}

	public function testCorrection()
	{
		/** @var $d1 \Project\Tests\Documents\Basic */
		$d1 = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$d1->setPStr('pStr d1');
		$d1->save();
		self::assertFalse($d1->useCorrection());
		/** @var $d2 \Project\Tests\Documents\Basic */
		$d2 = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$d2->setPStr('pStr d2');
		self::assertFalse($d2->useCorrection());
		$d2->save();

		/* @var $c1 \Project\Tests\Documents\Correction */
		$c1 = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Correction');

		$c1->setLabel('c1');
		$c1->getCurrentLocalization()->setPublicationStatus(Publishable::STATUS_DRAFT);
		$c1->setStr1('Str1');
		$c1->setStr2('Str2');
		$c1->getCurrentLocalization()->setStr3('Str3');
		$c1->getCurrentLocalization()->setStr4('Str4');
		$c1->getDocs1()->add($d1);
		$c1->getDocs2()->add($d1);
		$c1->create();

		self::assertTrue($c1->useCorrection());

		self::assertTrue($c1->useCorrection());

		self::assertFalse($c1->hasCorrection());
		self::assertFalse($c1->hasModifiedProperties());

		$c1Id = $c1->getId();
		self::assertGreaterThan(0, $c1Id);
		self::assertEquals(AbstractDocument::STATE_LOADED, $c1->getPersistentState());
		self::assertEquals(AbstractDocument::STATE_LOADED, $c1->getCurrentLocalization()->getPersistentState());

		$c1->getCurrentLocalization()->setPublicationStatus(Publishable::STATUS_PUBLISHABLE);
		self::assertTrue($c1->isPropertyModified('publicationStatus'));
		$c1->update();
		self::assertFalse($c1->hasCorrection());
		self::assertFalse($c1->hasModifiedProperties());

		$c1->setStr1('Str1 v2');
		$c1->setStr2('Str2 v2');
		$c1->getCurrentLocalization()->setStr3('Str3 v2');
		$c1->getCurrentLocalization()->setStr4('Str4 v2');
		$c1->getDocs1()->add($d2);
		$c1->getDocs2()->add($d2);

		self::assertTrue($c1->hasModifiedProperties());
		$c1->update();
		self::assertFalse($c1->hasModifiedProperties());

		self::assertFalse($c1->hasModifiedProperties());
		self::assertTrue($c1->hasCorrection());

		/* @var $correction \Change\Documents\Correction */
		/* @var $c1 \Project\Tests\Documents\Correction */
		$correction = $c1->getCurrentCorrection();
		self::assertInstanceOf(Correction::class, $correction);
		self::assertGreaterThan(0, $correction->getId());
		self::assertEquals(\Change\Documents\Correction::STATUS_DRAFT, $correction->getStatus());
		self::assertTrue($correction->isDraft());
		self::assertEquals(\Change\Documents\Correction::NULL_LCID_KEY, $correction->getLCID());
		self::assertArrayHasKey('str2', $correction->getDatas());
		self::assertArrayHasKey('str4', $correction->getDatas());
		self::assertArrayHasKey('docs2', $correction->getDatas());
		self::assertEquals(['str2', 'str4', 'docs2'], $correction->getPropertiesNames());

		self::assertEquals('Str1 v2', $c1->getStr1());
		self::assertEquals('Str2', $c1->getStr2());
		self::assertEquals('Str3 v2', $c1->getCurrentLocalization()->getStr3());
		self::assertEquals('Str4', $c1->getCurrentLocalization()->getStr4());
		self::assertCount(2, $c1->getDocs1());
		self::assertCount(1, $c1->getDocs2());

		$c1->reset();
		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $c1->getPersistentState());
		self::assertEquals('Str1 v2', $c1->getStr1());
		self::assertEquals('Str2', $c1->getStr2());
		self::assertEquals('Str3 v2', $c1->getCurrentLocalization()->getStr3());
		self::assertEquals('Str4', $c1->getCurrentLocalization()->getStr4());
		self::assertCount(2, $c1->getDocs1());
		self::assertCount(1, $c1->getDocs2());
		self::assertTrue($c1->hasCorrection());

		$corr = $c1->getCurrentCorrection();
		self::assertEquals('Str2 v2', $corr->getPropertyValue('str2'));
		self::assertEquals('Str4 v2', $corr->getPropertyValue('str4'));
		self::assertCount(2, $corr->getPropertyValue('docs2'));
		self::assertEquals(Correction::STATUS_DRAFT, $corr->getStatus());

		$corr->setStatus(Correction::STATUS_PUBLISHABLE);
		$corr->save();

		$c1->mergeCurrentCorrection();

		self::assertEquals(Correction::STATUS_FILED, $corr->getStatus());
		self::assertEquals('Str2', $corr->getPropertyValue('str2'));
		self::assertEquals('Str4', $corr->getPropertyValue('str4'));
		self::assertCount(1, $corr->getPropertyValue('docs2'));

		self::assertEquals('Str2 v2', $c1->getStr2());
		self::assertEquals('Str4 v2', $c1->getCurrentLocalization()->getStr4());
		self::assertCount(2, $c1->getDocs2());
		self::assertFalse($c1->hasCorrection());
		self::assertFalse($c1->hasModifiedProperties());
	}

	public function testIgnoreCorrection()
	{
		/* @var $c1 \Project\Tests\Documents\Correction */
		$c1 = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Correction');

		$c1->setLabel('c1');
		$c1->getCurrentLocalization()->setPublicationStatus(Publishable::STATUS_DRAFT);
		$c1->setStr2('Str2');
		$c1->getCurrentLocalization()->setStr4('Str4');
		$c1->create();
		self::assertTrue($c1->useCorrection());
		self::assertFalse($c1->hasCorrection());
		self::assertFalse($c1->hasModifiedProperties());

		$c1Id = $c1->getId();

		self::assertTrue($c1->useCorrection(false));
		self::assertFalse($c1->useCorrection());

		$c1->setStr2('Str2 v2');
		$c1->getCurrentLocalization()->setStr4('Str4 v2');

		self::assertTrue($c1->hasModifiedProperties());
		$c1->update();
		self::assertFalse($c1->hasModifiedProperties());
		self::assertFalse($c1->hasCorrection());

		$this->getApplicationServices()->getDocumentManager()->reset();

		/* @var $c2 \Project\Tests\Documents\Correction */
		$c2 = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($c1Id);
		self::assertSame($c2->getId(), $c1->getId());
		self::assertNotSame($c1, $c2);

		self::assertTrue($c2->useCorrection());
		self::assertFalse($c2->hasCorrection());

		self::assertEquals('Str2 v2', $c2->getStr2());
		self::assertEquals('Str4 v2', $c2->getCurrentLocalization()->getStr4());

		self::assertTrue($c2->useCorrection());
		self::assertFalse($c2->hasCorrection());
	}
}