<?php
namespace ChangeTests\Change\Documents;

class AbstractInlineTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	public function testInline()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();

		/* @var $document \Project\Tests\Documents\Inline */
		$document = $documentManager->getNewDocumentInstanceByModelName('Project_Tests_Inline');
		self::assertInstanceOf(\Project\Tests\Documents\Inline::class, $document);
		self::assertEquals('Project_Tests_Inline', $document->getDocumentModelName());

		self::assertNull($document->getPInline());

		/** @var $id \Project\Tests\Documents\InlinePInline */
		$id = $document->newInlinePInline();
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInline::class, $id);
		$id->setTest('Test string');

		$document->setPInline($id);
		self::assertTrue($document->isPropertyModified('pInline'));

		$document->setPInline(null);
		self::assertFalse($document->isPropertyModified('pInline'));

		$document->setPInline($id);
		self::assertTrue($document->isPropertyModified('pInline'));

		self::assertSame($id, $document->getPInline());

		$document->save();

		$documentManager->reset();

		/* @var $loadedDocument \Project\Tests\Documents\Inline */
		$loadedDocument = $documentManager->getDocumentInstance($document->getId());

		self::assertNotSame($document, $loadedDocument);

		$id2 = $loadedDocument->getPInline();
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInline::class, $id2);
		self::assertEquals('Test string', $id2->getTest());

		self::assertFalse($loadedDocument->isPropertyModified('pInline'));

		$id2->setTest('Test 2');
		self::assertTrue($loadedDocument->isPropertyModified('pInline'));

		$oldValue = $loadedDocument->getPInlineOldValue();
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInline::class, $oldValue);
		self::assertEquals('Test string', $oldValue->getTest());

		$loadedDocument->save();

		$documentManager->reset();

		/* @var $reLoadedDocument \Project\Tests\Documents\Inline */
		$reLoadedDocument = $documentManager->getDocumentInstance($document->getId());
		self::assertNotSame($loadedDocument, $reLoadedDocument);
		self::assertEquals('Test 2', $reLoadedDocument->getPInline()->getTest());
	}

	public function testInlineArray()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();

		/* @var $document \Project\Tests\Documents\Inline */
		$document = $documentManager->getNewDocumentInstanceByModelName('Project_Tests_Inline');
		self::assertInstanceOf(\Project\Tests\Documents\Inline::class, $document);
		self::assertEquals('Project_Tests_Inline', $document->getDocumentModelName());

		self::assertFalse($document->isPropertyModified('pInlineArray'));

		$arrayObject = $document->getPInlineArray();
		self::assertInstanceOf(\Change\Documents\InlineArrayProperty::class, $arrayObject);
		self::assertEquals(0, $arrayObject->count());

		/** @var $id \Project\Tests\Documents\InlinePInlineArray */
		$id = $document->newInlinePInlineArray();
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInlineArray::class, $id);
		$id->setTest('Test string');
		$id->setNumber(40);

		$arrayObject->add($id);

		self::assertTrue($document->isPropertyModified('pInlineArray'));

		$document->save();

		$documentManager->reset();

		/* @var $loadedDocument \Project\Tests\Documents\Inline */
		$loadedDocument = $documentManager->getDocumentInstance($document->getId());

		self::assertNotSame($document, $loadedDocument);

		$arrayObject = $loadedDocument->getPInlineArray();
		self::assertInstanceOf(\Change\Documents\InlineArrayProperty::class, $arrayObject);
		self::assertEquals(1, $arrayObject->count());

		self::assertFalse($loadedDocument->isPropertyModified('pInline'));
		$id2 = $arrayObject[0];
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInlineArray::class, $id2);
		self::assertEquals('Test string', $id2->getTest());
		self::assertEquals(40, $id2->getNumber());

		self::assertFalse($loadedDocument->isPropertyModified('pInlineArray'));

		$id2->setNumber(70);
		self::assertTrue($loadedDocument->isPropertyModified('pInlineArray'));

		$array = $loadedDocument->getPInlineArrayOldValue();
		self::assertCount(1, $array);
		self::assertInstanceOf(\Project\Tests\Documents\InlinePInlineArray::class, $array[0]);
		self::assertEquals(40, $array[0]->getNumber());

		$arrayObject->add($id);

		$loadedDocument->save();

		$documentManager->reset();

		/* @var $reLoadedDocument \Project\Tests\Documents\Inline */
		$reLoadedDocument = $documentManager->getDocumentInstance($document->getId());
		self::assertNotSame($loadedDocument, $reLoadedDocument);
		$arrayObject = $reLoadedDocument->getPInlineArray();
		self::assertEquals(2, $arrayObject->count());
		self::assertEquals(70, $arrayObject[0]->getNumber());
		self::assertEquals(40, $arrayObject[1]->getNumber());
	}
}