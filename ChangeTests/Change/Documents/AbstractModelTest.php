<?php
namespace ChangeTests\Change\Documents;

class AbstractModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	public function testType()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		self::assertInstanceOf(\Change\Documents\ModelManager::class, $modelManager);
	}

	public function testGetInstance()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		$modelBasic = $modelManager->getModelByName('Project_Tests_Basic');
		self::assertEquals('Project', $modelBasic->getVendorName());
		self::assertEquals('Tests', $modelBasic->getShortModuleName());
		self::assertEquals('Basic', $modelBasic->getShortName());
		self::assertEquals('Project_Tests', $modelBasic->getModuleName());
		self::assertEquals('Project_Tests_Basic', $modelBasic->getName());
		self::assertFalse($modelBasic->isLocalized());
		self::assertFalse($modelBasic->isEditable());
		self::assertFalse($modelBasic->isPublishable());
		self::assertFalse($modelBasic->isInline());
		self::assertFalse($modelBasic->useCorrection());
		self::assertFalse($modelBasic->hasDescendants());
		self::assertTrue($modelBasic->useTree());
		self::assertCount(0, $modelBasic->getDescendantsNames());
		self::assertFalse($modelBasic->hasParent());
		self::assertNull($modelBasic->getParentName());
		self::assertCount(0, $modelBasic->getAncestorsNames());
		self::assertEquals('Project_Tests_Basic', $modelBasic->getRootName());
		self::assertEquals('Project_Tests', $modelBasic->getTreeName());

		self::assertCount(19, $modelBasic->getProperties());
		self::assertArrayHasKey('pStr', $modelBasic->getProperties());

		self::assertCount(0, $modelBasic->getLocalizedProperties());
		self::assertCount(19, $modelBasic->getNonLocalizedProperties());
		self::assertCount(0, $modelBasic->getPropertiesWithCorrection());
		self::assertCount(0, $modelBasic->getLocalizedPropertiesWithCorrection());
		self::assertCount(0, $modelBasic->getNonLocalizedPropertiesWithCorrection());

		self::assertTrue($modelBasic->hasProperty('pStr'));
		self::assertTrue($modelBasic->hasProperty('id'));
		self::assertTrue($modelBasic->hasProperty('model'));

		$property = $modelBasic->getProperty('pStr');
		self::assertInstanceOf(\Change\Documents\Property::class, $property);
		self::assertEquals('pStr', $property->getName());

		$names = $modelBasic->getPropertyNames();
		self::assertCount(19, $names);
		self::assertContains('id', $names);
		self::assertContains('model', $names);
		self::assertContains('creationDate', $names);
		self::assertContains('modificationDate', $names);

		$inverseProperties = $modelBasic->getInverseProperties();
		self::assertArrayHasKey('ProjectTestsLocalizedPDocInst', $inverseProperties);
		self::assertArrayHasKey('ProjectTestsLocalizedPDocArr', $inverseProperties);

		self::assertTrue($modelBasic->hasInverseProperty('ProjectTestsLocalizedPDocArr'));

		$inverseProperty = $modelBasic->getInverseProperty('ProjectTestsLocalizedPDocArr');
		self::assertInstanceOf(\Change\Documents\InverseProperty::class, $inverseProperty);
		self::assertEquals('ProjectTestsLocalizedPDocArr', $inverseProperty->getName());
		self::assertEquals('Project_Tests_Localized', $inverseProperty->getRelatedDocumentType());
		self::assertEquals('pDocArr', $inverseProperty->getRelatedPropertyName());

		self::assertEquals('m.project.tests.documents.basic', $modelBasic->getLabelKey());
		self::assertEquals('undefined', $modelBasic->getPropertyLabelKey('undefined'));
		self::assertEquals('m.project.tests.documents.basic_pstr', $modelBasic->getPropertyLabelKey('pStr'));
		self::assertEquals('Project_Tests_Basic', (string)$modelBasic);
	}

	public function testLocalized()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		$modelLocalized = $modelManager->getModelByName('Project_Tests_Localized');
		self::assertTrue($modelLocalized->isLocalized());
		self::assertCount(32, $modelLocalized->getProperties());
		self::assertCount(15, $modelLocalized->getLocalizedProperties());
		self::assertCount(17, $modelLocalized->getNonLocalizedProperties());

		self::assertArrayHasKey('refLCID', $modelLocalized->getNonLocalizedProperties());
		self::assertArrayHasKey('LCID', $modelLocalized->getLocalizedProperties());
		self::assertArrayHasKey('creationDate', $modelLocalized->getLocalizedProperties());
	}

	public function testCorrection()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		$modelCorrection = $modelManager->getModelByName('Project_Tests_Correction');
		self::assertTrue($modelCorrection->useCorrection());
		self::assertCount(3, $modelCorrection->getPropertiesWithCorrection());
		self::assertCount(2, $modelCorrection->getNonLocalizedPropertiesWithCorrection());
		self::assertCount(1, $modelCorrection->getLocalizedPropertiesWithCorrection());
	}

	public function testExtend()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		$modelBase = $modelManager->getModelByName('Project_Tests_Correction');
		$modelExtend = $modelManager->getModelByName('Project_Tests_CorrectionExt');
		self::assertEquals('Project_Tests_Correction', $modelBase->getRootName());
		self::assertEquals('Project_Tests_Correction', $modelExtend->getRootName());
		self::assertCount(1, $modelBase->getDescendantsNames());
		self::assertContains('Project_Tests_CorrectionExt', $modelBase->getDescendantsNames());

		self::assertEquals('Project_Tests_Correction', $modelExtend->getParentName());
		self::assertCount(1, $modelExtend->getAncestorsNames());
		self::assertContains('Project_Tests_Correction', $modelExtend->getAncestorsNames());

		self::assertCount(5, $modelExtend->getPropertiesWithCorrection());
		self::assertCount(3, $modelExtend->getNonLocalizedPropertiesWithCorrection());
		self::assertCount(2, $modelExtend->getLocalizedPropertiesWithCorrection());

		self::assertArrayHasKey('str2', $modelExtend->getPropertiesWithCorrection());
		self::assertArrayHasKey('strext2', $modelExtend->getNonLocalizedPropertiesWithCorrection());
		self::assertArrayHasKey('strext4', $modelExtend->getLocalizedPropertiesWithCorrection());
	}
}
