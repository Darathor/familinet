<?php
namespace ChangeTests\Documents\Constraints;

/**
 * @name \ChangeTests\Documents\Constraints\ConstraintsManagerTest
 */
class ConstraintsManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();
		self::assertInstanceOf('\Change\Documents\Constraints\ConstraintsManager', $constraintsManager);
		
		self::assertEquals('c.constraints', \Zend\Validator\AbstractValidator::getDefaultTranslatorTextDomain());
		
		$t = \Zend\Validator\AbstractValidator::getDefaultTranslator();
		self::assertInstanceOf('\Change\Documents\Constraints\Translator', $t);
		self::assertInstanceOf('\Change\I18n\I18nManager', $t->getI18nManager());
	}
	
	/**
	 * @depends testConstruct
	 */
	public function testDefaultConstraint()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();
		self::assertTrue($constraintsManager->hasDefaultConstraint('domain'));
		self::assertTrue($constraintsManager->hasDefaultConstraint('url'));
		self::assertTrue($constraintsManager->hasDefaultConstraint('unique'));
		self::assertTrue($constraintsManager->hasDefaultConstraint('enum'));
		
		self::assertFalse($constraintsManager->hasDefaultConstraint('test'));
		$constraintsManager->registerConstraint('test', '\ChangeTests\Documents\Constraints\ConstraintNotFound');
		self::assertTrue($constraintsManager->hasDefaultConstraint('test'));
	}
	
	/**
	 * @depends testDefaultConstraint
	 */
	public function testGetByName()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();
		self::assertInstanceOf('\Change\Documents\Constraints\Domain', $constraintsManager->getByName('domain'));
		self::assertInstanceOf('\Change\Documents\Constraints\Min', $constraintsManager->getByName('min', array('min' => 1)));
	
		$c1 = $constraintsManager->getByName('email');
		$c2 = $constraintsManager->getByName('email');
		
		self::assertNotNull($c1);
		self::assertNotNull($c2);
		self::assertNotSame($c1, $c2);
		try 
		{
			$constraintsManager->getByName('test');
			self::fail('Constraint test not found');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Constraint test not found', $e->getMessage());
		}
		
		try
		{
			$constraintsManager->getByName('invalidconstraint');
			self::fail('Constraint invalidconstraint not found');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Constraint invalidconstraint not found', $e->getMessage());
		}
	}	
	
	/**
	 * @depends testGetByName
	 */
	public function testEmail()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();
		include __DIR__ . '/TestAssets/Translator.php';
		$t = \ChangeTests\Documents\Constraints\TestAssets\Translator::factory(array());
		\Zend\Validator\AbstractValidator::setDefaultTranslator($t);
		

		$constraint = $constraintsManager->getByName('email');
		self::assertTrue($constraint->isValid('noreplay@rbschange.fr'));
		
		$constraint = $constraintsManager->getByName('email');
		self::assertFalse($constraint->isValid('rbschange.fr'));
		
		$messages = $constraint->getMessages();
		self::assertArrayHasKey('emailAddressInvalidFormat', $messages);
		self::assertStringStartsWith('c.constraints.', $messages['emailAddressInvalidFormat']);
	}
	

	/**
	 * @depends testEmail
	 */
	public function testEmails()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();

		$t = \ChangeTests\Documents\Constraints\TestAssets\Translator::factory(array());
		\Zend\Validator\AbstractValidator::setDefaultTranslator($t);

		$constraint = $constraintsManager->getByName('emails');
		self::assertTrue($constraint->isValid('noreplay@rbschange.fr'));
		self::assertTrue($constraint->isValid('noreplay@rbschange.fr, admin@rbschange.fr'));
	
		$constraint = $constraintsManager->getByName('emails');
	
		self::assertFalse($constraint->isValid('noreplay@rbschange.fr,,rbschange.fr'));
		$messages = $constraint->getMessages();
		self::assertArrayHasKey('emailsAddressInvalid', $messages);
		self::assertStringStartsWith('c.constraints.', $messages['emailsAddressInvalid']);
	}
	
	/**
	 * @depends testEmails
	 */
	public function testInteger()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();

		$t = \ChangeTests\Documents\Constraints\TestAssets\Translator::factory(array());
		\Zend\Validator\AbstractValidator::setDefaultTranslator($t);

		$constraint = $constraintsManager->getByName('integer');
		self::assertTrue($constraint->isValid('5'));

		$constraint = $constraintsManager->getByName('integer');
	
		self::assertFalse($constraint->isValid('5.3'));
		$messages = $constraint->getMessages();
		self::assertArrayHasKey('notDigits', $messages);
		self::assertStringStartsWith('c.constraints.', $messages['notDigits']);
	}
	
	/**
	 * @depends testInteger
	 */
	public function testMatches()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();

		$t = \ChangeTests\Documents\Constraints\TestAssets\Translator::factory(array());
		\Zend\Validator\AbstractValidator::setDefaultTranslator($t);

		$constraint = $constraintsManager->getByName('matches', array('pattern' => '/^[0-5]+$/'));
		self::assertTrue($constraint->isValid('4'));
		self::assertTrue($constraint->isValid('45550'));
	
		$constraint = $constraintsManager->getByName('matches', array('pattern' => '/^[0-5]+$/', 'message' => 'test'));
	
		self::assertFalse($constraint->isValid('46'));
		$messages = $constraint->getMessages();
		self::assertArrayHasKey('regexNotMatch', $messages);
		self::assertStringStartsWith('c.constraints.', $messages['regexNotMatch']);
	}
	
	/**
	 * @depends testMatches
	 */
	public function testRange()
	{
		$constraintsManager = $this->getApplicationServices()->getConstraintsManager();

		$t = \ChangeTests\Documents\Constraints\TestAssets\Translator::factory(array());
		\Zend\Validator\AbstractValidator::setDefaultTranslator($t);

		$constraint = $constraintsManager->getByName('range', array('min' => 5, 'max' => 10));
		self::assertTrue($constraint->isValid('5'));
		self::assertTrue($constraint->isValid('6'));
		self::assertTrue($constraint->isValid('10'));
	
		$constraint = $constraintsManager->getByName('range', array('min' => 5, 'max' => 10));
	
		self::assertFalse($constraint->isValid('46'));
		$messages = $constraint->getMessages();

		self::assertArrayHasKey('notBetween', $messages);
		self::assertStringStartsWith('c.constraints.', $messages['notBetween']);
		
		$constraint = $constraintsManager->getByName('range', array('min' => 5, 'max' => 10, 'inclusive' => false));
		self::assertFalse($constraint->isValid('5'));
		$messages = $constraint->getMessages();
		self::assertArrayHasKey('notBetweenStrict', $messages);
	}
}
