<?php
namespace ChangeTests\Change\Documents;

use Change\Documents\DocumentArrayProperty;

/**
* @name \ChangeTests\Change\Documents\DocumentArrayPropertyTest
*/
class DocumentArrayPropertyTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return DocumentArrayProperty
	 */
	protected function getObject()
	{
		return new DocumentArrayProperty($this->getApplicationServices()->getDocumentManager(), null);
	}

	/**
	 * @param integer $id
	 * @return \Project\Tests\Documents\Basic
	 */
	protected function getTestDoc($id)
	{
		return $this->getNewReadonlyDocument('Project_Tests_Basic', $id);
	}

	public function testConstruct()
	{
		$o = $this->getObject();
		self::assertNull($o->getModelName());
		self::assertCount(0, $o);
		self::assertSame(0, $o->count());
		self::assertFalse(isset($o[5]));
		self::assertSame(array(), $o->getIds());
		self::assertNull($o->getDefaultIds());
		self::assertNull($o->getDefaultDocuments());
		self::assertSame(array(), $o->toArray());
	}

	public function testSetModelName()
	{
		$o = $this->getObject();
		$o->setModelName('Project_Tests_Basic');
		self::assertEquals('Project_Tests_Basic', $o->getModelName());
	}

	public function testSetDocument()
	{
		$o = $this->getObject();
		$o->setModelName('Project_Tests_Basic');
		$doc1 = $this->getTestDoc(200);
		$o->add($doc1);
		self::assertCount(1, $o);
		self::assertSame(1, $o->count());
		self::assertSame($doc1, $o[0]);

		$o->add($doc1);
		self::assertCount(1, $o);

		$o = $this->getObject();
		$o[] = $doc1;
		self::assertCount(1, $o);
		$o[] = $doc1;
		self::assertCount(1, $o);

		$o = $this->getObject();
		$o->fromArray(array($doc1, $doc1));
		self::assertCount(1, $o);
		self::assertSame(1, $o->count());

		$o = $this->getObject();
		$o->fromArray(array($doc1, $doc1, 8));
		self::assertCount(1, $o);

		$o = $this->getObject();
		$o->fromIds(array(200, '200', 'a'));
		self::assertCount(1, $o);
		self::assertSame($doc1, $o[0]);
	}

	public function testDefault()
	{
		$o = $this->getObject();
		$o->setModelName('Project_Tests_Basic');
		$doc1 = $this->getTestDoc(200);
		$doc2 = $this->getTestDoc(201);
		self::assertNull($o->getDefaultIds());
		$o->add($doc1);
		self::assertSame(array(), $o->getDefaultIds());

		$o = $this->getObject();
		$o->setModelName('Project_Tests_Basic');

		$o->setDefaultIds(array(201));
		self::assertNull($o->getDefaultIds());

		self::assertSame(array(201), $o->getIds());

		$o->add($doc2);
		self::assertSame(array(201), $o->getIds());

		self::assertNull($o->getDefaultIds());

		$o->add($doc1);
		self::assertSame(array(201, 200), $o->getIds());
		self::assertSame(array(201), $o->getDefaultIds());

		unset($o[1]);
		self::assertSame(array(201), $o->getIds());
		self::assertNull($o->getDefaultIds());

		$o = $this->getObject();
		$o->setDefaultIds(array(200, 201));
		$o->fromIds(array());
		self::assertSame(array(200, 201), $o->getDefaultIds());
		self::assertCount(0, $o);

		$o->fromIds(array(201, 200));
		self::assertSame(array(200, 201), $o->getDefaultIds());
		self::assertCount(2, $o);

		$o->fromIds(array(200, 201));
		self::assertNull($o->getDefaultIds());
		self::assertCount(2, $o);

	}

	public function testUnset()
	{
		$o = $this->getObject();
		$o->setModelName('Project_Tests_Basic');
		$doc1 = $this->getTestDoc(200);
		$doc2 = $this->getTestDoc(201);
		$o->add($doc1)->add($doc2);
		self::assertCount(2, $o);
		unset($o[0]);
		self::assertCount(1, $o);
		self::assertSame(array(201), $o->getIds());
		self::assertTrue(isset($o[0]));
		$o->add($doc1);
		self::assertSame(array(201, 200), $o->getIds());
		$o->fromIds(array());
		self::assertCount(0, $o);
	}
}