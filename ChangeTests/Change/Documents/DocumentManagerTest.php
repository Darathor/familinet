<?php
namespace ChangeTests\Change\Documents;

use Change\Documents\AbstractDocument;
use Change\Documents\DocumentManager;

class DocumentManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getObject()
	{
		$manager = $this->getApplicationServices()->getDocumentManager();
		$manager->reset();
		return $manager;
	}

	public function testGetNewDocumentInstance()
	{
		$manager = $this->getObject();

		$document = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertInstanceOf('\Project\Tests\Documents\Basic', $document);
		self::assertEquals(-1, $document->getId());
		self::assertEquals(AbstractDocument::STATE_NEW, $document->getPersistentState());

		$model = $document->getDocumentModel();

		$document2 = $manager->getNewDocumentInstanceByModel($model);
		self::assertInstanceOf('\Project\Tests\Documents\Basic', $document2);
		self::assertEquals(-2, $document2->getId());
		self::assertEquals(AbstractDocument::STATE_NEW, $document2->getPersistentState());
	}

	public function testTransaction()
	{
		$manager = $this->getObject();
		self::assertFalse($manager->inTransaction());

		$this->getApplicationServices()->getTransactionManager()->begin();
		self::assertTrue($manager->inTransaction());
		$this->getApplicationServices()->getTransactionManager()->commit();

		self::assertFalse($manager->inTransaction());
	}

	public function testCache()
	{
		$manager = $this->getObject();

		$this->getApplicationServices()->getTransactionManager()->begin();

		/* @var $document \Project\Tests\Documents\Basic */
		$document = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertLessThan(0, $document->getId());
		self::assertFalse($manager->isInCache($document->getId()));
		$document->setPStr('Required');
		$document->create();
		self::assertGreaterThan(0, $document->getId());
		self::assertTrue($manager->isInCache($document->getId()));

		$this->getApplicationServices()->getTransactionManager()->commit();
		self::assertFalse($manager->isInCache($document->getId()));
	}

	public function testNewQuery()
	{
		$manager = $this->getObject();
		$query = $manager->getNewQuery('Project_Tests_Basic');
		self::assertInstanceOf('Change\Documents\Query\Query', $query);
		self::assertNull($query->getLCID());

		$query = $manager->getNewQuery('Project_Tests_Basic', 'xx_XX');
		self::assertEquals('xx_XX', $query->getLCID());
	}

	/**
	 * Tests for:
	 *  - getLCIDStackSize
	 *  - getLCID
	 *  - pushLCID
	 *  - popLCID
	 */
	public function testLangStack()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$config->addVolatileEntry('Change/I18n/supported-lcids', null);
		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_GB', 'it_IT', 'es_ES', 'en_US']);

		$config->addVolatileEntry('Change/I18n/langs', null);
		$config->addVolatileEntry('Change/I18n/langs', ['en_US' => 'us']);

		$i18nManger = $this->getApplicationServices()->getI18nManager();
		$manager = new DocumentManager();
		$manager->setI18nManager($this->getApplicationServices()->getI18nManager());

		// There is no default value.
		self::assertEquals(0, $manager->getLCIDStackSize());
		self::assertEquals($i18nManger->getLCID(), $manager->getLCID());

		// Push/pop supported languages.
		$manager->pushLCID('it_IT');
		self::assertEquals(1, $manager->getLCIDStackSize());
		self::assertEquals('it_IT', $manager->getLCID());
		$manager->pushLCID('en_GB');
		self::assertEquals(2, $manager->getLCIDStackSize());
		self::assertEquals('en_GB', $manager->getLCID());
		$manager->popLCID();
		self::assertEquals(1, $manager->getLCIDStackSize());
		self::assertEquals('it_IT', $manager->getLCID());
		$manager->popLCID();
		self::assertEquals(0, $manager->getLCIDStackSize());
		self::assertEquals($i18nManger->getLCID(), $manager->getLCID());

		// Pop from an empty stack.
		try
		{
			$manager->popLCID();
			self::fail('A LogicException should be thrown.');
		}
		catch (\LogicException $e)
		{
			self::assertEquals(0, $manager->getLCIDStackSize());
			self::assertEquals($i18nManger->getLCID(), $manager->getLCID());
		}

		// Push not spported language.
		try
		{
			$manager->pushLCID('kl');
			self::fail('A InvalidArgumentException should be thrown.');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertEquals(0, $manager->getLCIDStackSize());
			self::assertEquals($i18nManger->getLCID(), $manager->getLCID());
		}
	}

	public function testTransactionLangStack()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$config->addVolatileEntry('Change/I18n/supported-lcids', null);
		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_GB', 'it_IT', 'es_ES', 'en_US']);

		$manager = $this->getApplicationServices()->getDocumentManager();

		$manager->pushLCID('fr_FR');
		$manager->pushLCID('en_GB');
		$manager->pushLCID('it_IT');
		$this->getApplicationServices()->getTransactionManager()->begin();
		$manager->pushLCID('es_ES');
		$manager->pushLCID('en_US');
		$this->getApplicationServices()->getTransactionManager()->begin();
		$manager->pushLCID('en_GB');
		self::assertEquals('en_GB', $manager->getLCID());

		try
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
			self::fail('RollbackException expected');
		}
		catch (\Change\Transaction\RollbackException $e)
		{
			// Nothing to do.
		}
		self::assertEquals('en_US', $manager->getLCID());

		$this->getApplicationServices()->getTransactionManager()->rollBack();
		self::assertEquals('it_IT', $manager->getLCID());
	}

}