<?php
namespace ChangeTests\Change\Documents;

class DocumentWeakReferenceTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	public function testSerialize()
	{
		$mi = new \ChangeTests\Change\Documents\TestAssets\MemoryInstance();
		$manager = $this->getApplicationServices()->getDocumentManager();
		$document = $mi->getInstanceRo5001($manager);

		$id = $document->getId();
		$wr = new \Change\Documents\DocumentWeakReference($document);
		self::assertEquals($id, $wr->getId());
		self::assertEquals($document->getDocumentModelName(), $wr->getModelName());

		self::assertSame($document, $wr->getDocument($manager));

		$serialized = serialize($wr);
		$wr2 = unserialize($serialized);
		self::assertNotSame($wr, $wr2);

		self::assertEquals($wr2->getId(), $wr->getId());
		self::assertSame($document, $wr2->getDocument($manager));
	}
}