<?php
namespace ChangeTests\Documents;

/**
 * @name \ChangeTests\Documents\Generators\CompilerTest
 */
class CompilerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$compiler = new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());
		self::assertCount(0, $compiler->getModels());
	}

	public function testLoadDocument()
	{
		$compiler = new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());
		$definitionPath = __DIR__ . '/TestAssets/TestType.xml';
		$model = $compiler->loadDocument('Change', 'Test', 'TestType', $definitionPath);
		self::assertEquals('Change_Test_TestType', $model->getName());
		self::assertCount(3, $compiler->getModels());

		$this->expectException(\RuntimeException::class);
		$compiler->loadDocument('Change', 'test', 'test1', __DIR__ . '/TestAssets/notfound');
	}

	public function testCheckExtends()
	{
		$compiler = new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());

		$definitionPath = __DIR__ . '/TestAssets/TestType.xml';
		$m1 = $compiler->loadDocument('Change', 'Test', 'TestType', $definitionPath);

		$definitionPath = __DIR__ . '/TestAssets/TestTypeExt.xml';
		$m2 = $compiler->loadDocument('Change', 'Test', 'TestTypeExt', $definitionPath);


		self::assertCount(4, $compiler->getModels());
		self::assertCount(0, $compiler->getRootModelNames());

		$userModel = new \Change\Documents\Generators\Model('Rbs', 'User', 'User');
		$compiler->addModel($userModel);

		$compiler->buildTree();
		$compiler->validateInheritance();

		self::assertCount(2, $compiler->getRootModelNames());

		$p1 = $m1->getPropertyByName('int1');
		self::assertCount(0, $p1->getAncestors());

		$p2 = $m2->getPropertyByName('int1');
		self::assertCount(1, $p2->getAncestors());

		$compiler->saveModelsPHPCode();

		$compilationPath = $this->getApplication()->getWorkspace()->compilationPath();
		$commonPath = $compilationPath . DIRECTORY_SEPARATOR . \str_replace('\\', DIRECTORY_SEPARATOR, $m1->getNameSpace()) . DIRECTORY_SEPARATOR;
		self::assertFileExists($commonPath . $m2->getShortModelClassName() . '.php');
		self::assertFileExists($commonPath . $m2->getShortBaseDocumentClassName() . '.php');
		self::assertFileExists($commonPath . $m2->getShortDocumentLocalizedClassName() . '.php');
	}
}
