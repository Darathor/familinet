<?php /** @noinspection DuplicatedCode */
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace ChangeTests\Documents\Generators;

/**
 * @name \ChangeTests\Documents\Generators\InlineClassGeneratorTest
 */
class InlineClassGeneratorTest extends \ChangeTests\Change\TestAssets\TestCase // NOSONAR
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	public static function tearDownAfterClass(): void
	{
		static::invalidateDocumentClasses();
	}

	public function testCompile()
	{
		$definitionPath = __DIR__ . '/TestAssets/InlineCompile.xml';
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->load($definitionPath);

		$compiler = new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());
		$model = new \Change\Documents\Generators\Model('ChangeTests', 'Generators', 'inline');
		$model->setXmlInlineElement($doc->documentElement, $compiler);
		$model->validateInheritance();
		$codeDir = $this->getApplication()->getWorkspace()->tmpPath('InlineCompile');

		$generator = new \Change\Documents\Generators\ModelClass();
		$generator->savePHPCode($compiler, $model, $codeDir);

		$phpClassPath = $codeDir . '/ChangeTests/Generators/Documents/InlineCompileModel.php';
		self::assertFileExists($phpClassPath);
		/** @noinspection PhpIncludeInspection */
		include_once $phpClassPath;
		self::assertTrue(class_exists(\Compilation\ChangeTests\Generators\Documents\InlineCompileModel::class));

		$generator = new \Change\Documents\Generators\BaseInlineClass();
		$generator->savePHPCode($compiler, $model, $codeDir);

		$phpClassPath = $codeDir . '/ChangeTests/Generators/Documents/InlineCompile.php';
		self::assertFileExists($phpClassPath);
		/** @noinspection PhpIncludeInspection */
		include_once $phpClassPath;
		self::assertTrue(class_exists(\Compilation\ChangeTests\Generators\Documents\InlineCompile::class));

		$generator = new \Change\Documents\Generators\InlineLocalizedClass();
		$generator->savePHPCode($compiler, $model, $codeDir);

		$phpClassPath = $codeDir . '/ChangeTests/Generators/Documents/LocalizedInlineCompile.php';
		self::assertFileExists($phpClassPath);
		/** @noinspection PhpIncludeInspection */
		include_once $phpClassPath;
		self::assertTrue(class_exists(\Compilation\ChangeTests\Generators\Documents\LocalizedInlineCompile::class));

		$model = $this->getApplicationServices()->getModelManager()->getModelByName('ChangeTests_Generators_InlineCompile');
		self::assertInstanceOf(\Compilation\ChangeTests\Generators\Documents\InlineCompileModel::class, $model);
		self::assertTrue($model->isInline());

		include_once __DIR__ . '/TestAssets/InlineCompile.inc';

		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModel($model, false);
		self::assertInstanceOf(\ChangeTests\Generators\Documents\InlineCompile::class, $instance);
		$instance->cleanUp();
	}

	public function testStateMethods()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance =
			$this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile', false);
		self::assertTrue($instance->isNew());
		self::assertFalse($instance->isModified());

		self::assertFalse($instance->isNew(false));
		self::assertFalse($instance->isNew());

		self::assertTrue($instance->isModified(true));
		self::assertTrue($instance->isModified());
		$instance->cleanUp();

		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');
		self::assertTrue($instance->isNew());
		self::assertFalse($instance->isModified());
		$instance->cleanUp();
	}

	public function testDbData()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance =
			$this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile', false);
		$instance->isModified(true);
		$instance->isNew(true);

		$dbData = $instance->dbData();
		self::assertArrayHasKey('model', $dbData);
		self::assertEquals('ChangeTests_Generators_InlineCompile', $dbData['model']);
		self::assertFalse($instance->isNew());
		self::assertFalse($instance->isModified());

		$instance->isModified(true);
		$instance->isNew(true);
		$instance->dbData($dbData);
		self::assertFalse($instance->isNew());
		self::assertFalse($instance->isModified());

		$instance->cleanUp();
	}

	public function testStringProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPString());

		self::assertSame($instance, $instance->setPString('test'));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals('test', $instance->getPString());
		$instance->setPString('test');
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPString());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pString')->setDefaultValue('default');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals('default', $instance->getPString());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pString', $dbData);
		self::assertEquals('default', $dbData['pString']);
		self::assertEquals(1, $callBackCount);

		$dbData['pString'] = 'dbData';
		$instance->dbData($dbData);
		self::assertEquals('dbData', $instance->getPString());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testDecimalProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPDecimal());

		self::assertSame($instance, $instance->setPDecimal(0.333333));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals(0.333333, $instance->getPDecimal());
		$instance->setPDecimal(0.333333);
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPDecimal());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pDecimal')->setDefaultValue('0.343434');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals(0.343434, $instance->getPDecimal());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDecimal', $dbData);
		self::assertEquals(0.343434, $dbData['pDecimal']);
		self::assertEquals(1, $callBackCount);

		$dbData['pDecimal'] = 0.333333;
		$instance->dbData($dbData);
		self::assertEquals(0.333333, $instance->getPDecimal());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testFloatProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPFloat());

		self::assertSame($instance, $instance->setPFloat(0.333333));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals(0.333333, $instance->getPFloat());
		$instance->setPFloat(0.333333);
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPFloat());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pFloat')->setDefaultValue('0.343434');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals(0.343434, $instance->getPFloat());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pFloat', $dbData);
		self::assertEquals(0.343434, $dbData['pFloat']);
		self::assertEquals(1, $callBackCount);

		$dbData['pFloat'] = 0.333333;
		$instance->dbData($dbData);
		self::assertEquals(0.333333, $instance->getPFloat());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testIntegerProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPInteger());

		self::assertSame($instance, $instance->setPInteger(10));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals(10, $instance->getPInteger());
		$instance->setPInteger(10);
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPInteger());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pInteger')->setDefaultValue('20');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals(20, $instance->getPInteger());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pInteger', $dbData);
		self::assertEquals(20, $dbData['pInteger']);
		self::assertEquals(1, $callBackCount);

		$dbData['pInteger'] = 10;
		$instance->dbData($dbData);
		self::assertEquals(10, $instance->getPInteger());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testLongStringProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPLongString());

		self::assertSame($instance, $instance->setPLongString('test'));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals('test', $instance->getPLongString());
		$instance->setPLongString('test');
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPLongString());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pLongString')->setDefaultValue('default');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals('default', $instance->getPLongString());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pLongString', $dbData);
		self::assertEquals('default', $dbData['pLongString']);
		self::assertEquals(1, $callBackCount);

		$dbData['pLongString'] = 'dbData';
		$instance->dbData($dbData);
		self::assertEquals('dbData', $instance->getPLongString());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testBooleanProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertFalse($instance->getPBoolean());

		self::assertSame($instance, $instance->setPBoolean(true));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertTrue($instance->getPBoolean());
		$instance->setPBoolean(true);
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertFalse($instance->getPBoolean());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pBoolean')->setDefaultValue('true');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->getPBoolean());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pBoolean', $dbData);
		self::assertTrue($dbData['pBoolean']);
		self::assertEquals(1, $callBackCount);

		$dbData['pBoolean'] = false;
		$instance->dbData($dbData);
		self::assertFalse($instance->getPBoolean());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testDateProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPDate());

		$date = '2014-07-17T16:31:33+02:00';
		$date2 = '2014-07-18';

		self::assertSame($instance, $instance->setPDate(new \DateTime($date)));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals('2014-07-17 00:00:00', $instance->getPDate()->format('Y-m-d H:i:s'));
		$instance->setPDate(new \DateTime('2014-07-17T00:00:00+00:00'));
		self::assertEquals(1, $callBackCount);
		$instance->setPDate(new \DateTime('2014-07-17'));
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPDate());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pDate')->setDefaultValue($date2);
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals('2014-07-18 00:00:00', $instance->getPDate()->format('Y-m-d H:i:s'));
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDate', $dbData);
		self::assertEquals($date2, $dbData['pDate']);
		self::assertEquals(1, $callBackCount);

		$dbData['pDate'] = '2014-07-17';
		$instance->dbData($dbData);
		self::assertEquals('2014-07-17 00:00:00', $instance->getPDate()->format('Y-m-d H:i:s'));
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testDateTimeProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPDateTime());

		$date = '2014-07-17T16:31:33+02:00';
		$date2 = '2014-07-18T14:55:00+00:00';

		self::assertSame($instance, $instance->setPDateTime(new \DateTime($date)));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals('2014-07-17T16:31:33+02:00', $instance->getPDateTime()->format(\DateTime::ATOM));
		$instance->setPDateTime(new \DateTime('2014-07-17T16:31:33+02:00'));
		self::assertEquals(1, $callBackCount);
		$instance->setPDateTime(new \DateTime('2014-07-17T14:31:33+00:00'));
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPDateTime());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pDateTime')->setDefaultValue($date2);
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals($date2, $instance->getPDateTime()->format(\DateTime::ATOM));
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDateTime', $dbData);
		self::assertEquals($date2, $dbData['pDateTime']);
		self::assertEquals(1, $callBackCount);

		$dbData['pDateTime'] = $date;
		$instance->dbData($dbData);
		self::assertEquals($date, $instance->getPDateTime()->format(\DateTime::ATOM));
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testDocumentProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPDocument());
		self::assertSame(0, $instance->getPDocumentId());

		$doc101 = $this->getNewReadonlyDocument('Project_Tests_Basic', 101);

		$doc102 = $this->getNewReadonlyDocument('Project_Tests_Basic', 102);

		self::assertSame($instance, $instance->setPDocument($doc101));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());
		self::assertSame($doc101, $instance->getPDocument());
		self::assertSame(101, $instance->getPDocumentId());

		$instance->setPDocument($doc101);
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPDocument());
		self::assertSame(0, $instance->getPDocumentId());
		self::assertEquals(1, $callBackCount);

		self::assertSame($instance, $instance->setPDocument($doc102));
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDocument', $dbData);
		self::assertEquals(102, $dbData['pDocument']);

		$dbData['pDocument'] = 101;
		$instance->dbData($dbData);
		self::assertEquals($doc101, $instance->getPDocument());
		self::assertEquals(2, $callBackCount);

		$instance->cleanUp();
	}

	public function testDocumentArrayProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		$array = $instance->getPDocumentArray();
		self::assertInstanceOf(\Change\Documents\DocumentArrayProperty::class, $array);
		self::assertEquals(0, $array->count());
		self::assertSame(0, $instance->getPDocumentArrayCount());
		self::assertSame([], $instance->getPDocumentArrayIds());

		$doc101 = $this->getNewReadonlyDocument('Project_Tests_Basic', 101);

		$doc102 = $this->getNewReadonlyDocument('Project_Tests_Basic', 102);

		$doc103 = $this->getNewReadonlyDocument('Project_Tests_Basic', 103);

		self::assertSame($instance, $instance->setPDocumentArray([$doc101, $doc103]));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());
		self::assertNotSame($array, $instance->getPDocumentArray());
		$array = $instance->getPDocumentArray();
		self::assertEquals([101, 103], $array->getIds());
		self::assertEquals(2, $instance->getPDocumentArrayCount());
		self::assertEquals([101, 103], $instance->getPDocumentArrayIds());

		$instance->getPDocumentArray()->add($doc102);
		self::assertEquals(1, $callBackCount);
		self::assertEquals(3, $array->count());

		$instance->unsetProperties();
		self::assertNotSame($array, $instance->getPDocumentArray());
		self::assertEquals(1, $callBackCount);
		$array = $instance->getPDocumentArray();
		self::assertInstanceOf(\Change\Documents\DocumentArrayProperty::class, $array);

		self::assertSame($instance, $instance->setPDocumentArray([$doc102, $doc103]));
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDocumentArray', $dbData);
		self::assertEquals([102, 103], $dbData['pDocumentArray']);

		$dbData['pDocumentArray'] = [101, 102];
		$instance->dbData($dbData);
		self::assertNotSame($array, $instance->getPDocumentArray());

		self::assertEquals([101, 102], $instance->getPDocumentArray()->getIds());
		self::assertEquals(2, $callBackCount);

		$instance->cleanUp();
	}

	public function testDocIdProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertSame(0, $instance->getPDocId());
		self::assertNull($instance->getPDocIdInstance());

		$doc101 = $this->getNewReadonlyDocument('Project_Tests_Basic', 101);

		self::assertSame($instance, $instance->setPDocId(101));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());
		self::assertSame(101, $instance->getPDocId());
		self::assertSame($doc101, $instance->getPDocIdInstance());

		$instance->unsetProperties();
		self::assertSame(0, $instance->getPDocId());
		self::assertEquals(1, $callBackCount);

		self::assertSame($instance, $instance->setPDocId(102));
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pDocId', $dbData);
		self::assertEquals(102, $dbData['pDocId']);

		$dbData['pDocId'] = 101;
		$instance->dbData($dbData);
		self::assertSame(101, $instance->getPDocId());
		self::assertEquals(2, $callBackCount);
		$instance->cleanUp();
	}

	public function testJSONProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPJSON());

		$json1 = ['test' => 0, 'test2' => true, 'Test3' => ['v1', 'v2']];
		self::assertSame($instance, $instance->setPJSON($json1));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());
		self::assertSame($json1, $instance->getPJSON());

		$instance->setPJSON($json1);
		self::assertEquals(1, $callBackCount);
		self::assertSame($json1, $instance->getPJSON());

		$instance->unsetProperties();
		self::assertNull($instance->getPJSON());
		self::assertEquals(1, $callBackCount);
		$instance->setPJSON(null);
		self::assertNull($instance->getPJSON());
		self::assertEquals(1, $callBackCount);

		$json2 = ['test' => 4, 'test2' => false, 'Test3' => ['v1', 'v2']];
		self::assertSame($instance, $instance->setPJSON($json2));
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pJSON', $dbData);
		self::assertEquals($json2, $dbData['pJSON']);

		$dbData['pJSON'] = $json1;
		$instance->dbData($dbData);
		self::assertSame($json1, $instance->getPJSON());
		self::assertEquals(2, $callBackCount);
		$instance->cleanUp();
	}

	public function testRichTextProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		$richtextProperty = $instance->getPRichText();
		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $richtextProperty);
		self::assertTrue($richtextProperty->isEmpty());

		$richtext1 = new \Change\Documents\RichtextProperty();
		$richtext1->setEditor('Html')->setRawText('Dummy text');

		$richtext2 = new \Change\Documents\RichtextProperty();
		$richtext2->setEditor('Html')->setRawText('Dummy text 2');

		self::assertSame($instance, $instance->setPRichText($richtext1));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());
		self::assertSame($richtext1, $instance->getPRichText());

		$instance->setPRichText($richtext1->toArray());
		self::assertSame($richtext1, $instance->getPRichText());
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNotSame($richtext1, $instance->getPRichText());
		self::assertEquals(1, $callBackCount);
		$richtextProperty = $instance->getPRichText();
		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $richtextProperty);

		$instance->setPRichText($richtext2->toArray());
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pRichText', $dbData);
		self::assertEquals($richtext2->toArray(), $dbData['pRichText']);

		$dbData['pRichText'] = $richtext1->toArray();
		$instance->dbData($dbData);

		self::assertEquals($richtext1->toArray(), $instance->getPRichText()->toArray());
		self::assertEquals(2, $callBackCount);

		$instance->setPRichText(null);
		$dbData = $instance->dbData();
		self::assertArrayHasKey('pRichText', $dbData);
		self::assertNull($dbData['pRichText']);

		$instance->cleanUp();
	}

	public function testStorageUriProperty()
	{
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $this->getApplicationServices()->getDocumentManager()->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });

		self::assertNull($instance->getPStorageUri());

		self::assertSame($instance, $instance->setPStorageUri('test'));
		self::assertEquals(1, $callBackCount);
		self::assertTrue($instance->isModified());

		self::assertEquals('test', $instance->getPStorageUri());
		$instance->setPStorageUri('test');
		self::assertEquals(1, $callBackCount);

		$instance->unsetProperties();
		self::assertNull($instance->getPStorageUri());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('pStorageUri')->setDefaultValue('default');
		$instance->setDefaultValues();
		self::assertEquals(1, $callBackCount);
		self::assertEquals('default', $instance->getPStorageUri());
		self::assertFalse($instance->isModified());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('pStorageUri', $dbData);
		self::assertEquals('default', $dbData['pStorageUri']);
		self::assertEquals(1, $callBackCount);

		$dbData['pStorageUri'] = 'dbData';
		$instance->dbData($dbData);
		self::assertEquals('dbData', $instance->getPStorageUri());
		self::assertEquals(1, $callBackCount);

		$instance->cleanUp();
	}

	public function testLocalizedProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();

		self::assertCount(0, $instance->getLCIDArray());
		self::assertNull($instance->getRefLCID());
		$localizedPart = $instance->getRefLocalization();
		self::assertInstanceOf(\Compilation\ChangeTests\Generators\Documents\LocalizedInlineCompile::class, $localizedPart);
		self::assertEquals($refLCID, $instance->getRefLCID());
		self::assertEquals($refLCID, $localizedPart->getLCID());
		self::assertTrue($localizedPart->isNew());
		self::assertFalse($localizedPart->isModified());
		self::assertTrue($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertSame($localizedPart, $instance->getCurrentLocalization());
		self::assertCount(1, $instance->getLCIDArray());

		$localizedPart->setLString('test');
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(2, $callBackCount);

		$documentManager->pushLCID('en_US');
		$enLocalizedPart = $instance->getCurrentLocalization();

		self::assertEquals('en_US', $enLocalizedPart->getLCID());
		self::assertTrue($enLocalizedPart->isNew());
		self::assertFalse($enLocalizedPart->isModified());
		self::assertTrue($enLocalizedPart->isEmpty());
		self::assertEquals(2, $callBackCount);
		self::assertCount(2, $instance->getLCIDArray());

		$dbData = $instance->dbData();
		self::assertArrayHasKey('_LCID', $dbData);
		self::assertCount(1, $dbData['_LCID']);
		self::assertArrayHasKey(0, $dbData['_LCID']);
		self::assertEquals('fr_FR', $dbData['_LCID'][0]['LCID']);

		self::assertEquals(2, $callBackCount);
		self::assertFalse($localizedPart->isNew());
		self::assertFalse($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());

		self::assertTrue($enLocalizedPart->isNew());
		self::assertFalse($enLocalizedPart->isModified());
		self::assertTrue($enLocalizedPart->isEmpty());

		$instance->deleteCurrentLocalization();

		self::assertNotSame($enLocalizedPart, $instance->getCurrentLocalization());
		$enLocalizedPart->setLString('test');
		self::assertEquals(2, $callBackCount);

		$instance->getCurrentLocalization()->setLString('test2');
		self::assertEquals(3, $callBackCount);

		$documentManager->popLCID();
		$instance->cleanUp();
	}

	public function testLocalizedStringProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLString());
		self::assertSame($localizedPart, $localizedPart->setLString('test'));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals('test', $localizedPart->getLString());
		$localizedPart->setLString('test');
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLString('test 2');
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals('test 2', $dbData['_LCID'][0]['lString']);

		$localizedPart->setLString('test');
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals('test 2', $localizedPart->getLString());
		$instance->cleanUp();
	}

	public function testLocalizedLongStringProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLLongString());

		self::assertSame($localizedPart, $localizedPart->setLLongString('test'));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals('test', $localizedPart->getLLongString());
		$localizedPart->setLLongString('test');
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLLongString('test 2');
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals('test 2', $dbData['_LCID'][0]['lLongString']);

		$localizedPart->setLLongString('test');
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals('test 2', $localizedPart->getLLongString());
		$instance->cleanUp();
	}

	public function testLocalizedStorageUriProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLStorageUri());

		self::assertSame($localizedPart, $localizedPart->setLStorageUri('test'));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals('test', $localizedPart->getLStorageUri());
		$localizedPart->setLStorageUri('test');
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLStorageUri('test 2');
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals('test 2', $dbData['_LCID'][0]['lStorageUri']);

		$localizedPart->setLStorageUri('test');
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals('test 2', $localizedPart->getLStorageUri());
		$instance->cleanUp();
	}

	public function testLocalizedIntegerProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLInteger());

		self::assertSame($localizedPart, $localizedPart->setLInteger(10));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals(10, $localizedPart->getLInteger());
		$localizedPart->setLInteger(10);
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLInteger(20);
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals(20, $dbData['_LCID'][0]['lInteger']);

		$localizedPart->setLInteger(10);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals(20, $localizedPart->getLInteger());
		$instance->cleanUp();
	}

	public function testLocalizedDecimalProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLDecimal());

		self::assertSame($localizedPart, $localizedPart->setLDecimal(10.3356));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals(10.3356, $localizedPart->getLDecimal());
		$localizedPart->setLDecimal(10.3356);
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLDecimal(20.37854);
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals(20.37854, $dbData['_LCID'][0]['lDecimal']);

		$localizedPart->setLDecimal(10.3356);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals(20.37854, $localizedPart->getLDecimal());
		$instance->cleanUp();
	}

	public function testLocalizedFloatProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLFloat());

		self::assertSame($localizedPart, $localizedPart->setLFloat(10.3356));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);
		self::assertEquals(10.3356, $localizedPart->getLFloat());
		$localizedPart->setLFloat(10.3356);
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLFloat(20.37854);
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals(20.37854, $dbData['_LCID'][0]['lFloat']);

		$localizedPart->setLFloat(10.3356);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals(20.37854, $localizedPart->getLFloat());
		$instance->cleanUp();
	}

	public function testLocalizedBooleanProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertFalse($localizedPart->getLBoolean());

		self::assertSame($localizedPart, $localizedPart->setLBoolean(false));
		self::assertTrue($localizedPart->isNew());
		self::assertFalse($localizedPart->isModified());
		self::assertTrue($localizedPart->isEmpty());
		self::assertEquals(0, $callBackCount);
		self::assertFalse($localizedPart->getLBoolean());
		$localizedPart->setLBoolean(false);
		self::assertEquals(0, $callBackCount);
		$localizedPart->setLBoolean(true);
		self::assertEquals(1, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(1, $callBackCount);
		self::assertTrue($dbData['_LCID'][0]['lBoolean']);

		$localizedPart->setLBoolean(false);
		self::assertEquals(2, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(2, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertTrue($localizedPart->getLBoolean());
		$instance->cleanUp();
	}

	public function testLocalizedDateProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLDate());

		$date = '2014-07-17T16:31:33+02:00';
		$date2 = '2014-07-18';

		self::assertSame($localizedPart, $localizedPart->setLDate(new \DateTime($date)));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);

		self::assertEquals('2014-07-17 00:00:00', $localizedPart->getLDate()->format('Y-m-d H:i:s'));

		$localizedPart->setLDate(new \DateTime('2014-07-17T16:31:33+00:00'));
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLDate(new \DateTime('2014-07-17'));
		self::assertEquals(1, $callBackCount);

		$localizedPart->unsetProperties();
		self::assertNull($localizedPart->getLDate());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('lDate')->setDefaultValue($date2);
		$instance->resetCurrentLocalized();
		$localizedPart = $instance->getRefLocalization();

		self::assertEquals('2014-07-18 00:00:00', $localizedPart->getLDate()->format('Y-m-d H:i:s'));
		self::assertEquals(1, $callBackCount);
		self::assertFalse($localizedPart->isModified());

		$localizedPart->isModified(true);
		$dbData = $instance->dbData();
		self::assertEquals(1, $callBackCount);
		self::assertEquals($date2, $dbData['_LCID'][0]['lDate']);

		$localizedPart->setLDate(new \DateTime($date));
		self::assertEquals(2, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->getDocumentModel()->getProperty('lDate')->setDefaultValue(null);
		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(2, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals('2014-07-18 00:00:00', $localizedPart->getLDate()->format('Y-m-d H:i:s'));

		$instance->cleanUp();
	}

	public function testLocalizedDateTimeProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLDateTime());

		$date = '2014-07-17T16:31:33+02:00';
		$date2 = '2014-07-18T04:04:33+02:00';

		self::assertSame($localizedPart, $localizedPart->setLDateTime(new \DateTime($date)));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);

		self::assertEquals(new \DateTime($date), $localizedPart->getLDateTime());

		$localizedPart->setLDateTime(new \DateTime($date));
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLDateTime(new \DateTime($date));
		self::assertEquals(1, $callBackCount);

		$localizedPart->unsetProperties();
		self::assertNull($localizedPart->getLDateTime());
		self::assertEquals(1, $callBackCount);

		$instance->getDocumentModel()->getProperty('lDateTime')->setDefaultValue($date2);
		$instance->resetCurrentLocalized();
		$localizedPart = $instance->getRefLocalization();

		self::assertEquals(new \DateTime($date2), $localizedPart->getLDateTime());
		self::assertEquals(1, $callBackCount);
		self::assertFalse($localizedPart->isModified());

		$localizedPart->isModified(true);
		$dbData = $instance->dbData();
		self::assertEquals(1, $callBackCount);
		self::assertEquals($date2, $dbData['_LCID'][0]['lDateTime']);

		$localizedPart->setLDateTime(new \DateTime($date));
		self::assertEquals(2, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->getDocumentModel()->getProperty('lDateTime');
		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(2, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals(new \DateTime($date2), $localizedPart->getLDateTime());

		$instance->cleanUp();
	}

	public function testLocalizedDocIdProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertSame(0, $localizedPart->getLDocId());

		$doc101 = $this->getNewReadonlyDocument('Project_Tests_Basic', 101);

		self::assertSame($localizedPart, $localizedPart->setLDocId($doc101->getId()));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);

		self::assertEquals(101, $localizedPart->getLDocId());

		$localizedPart->setLDocId($doc101->getId());
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLDocId(101);
		self::assertEquals(1, $callBackCount);

		$localizedPart->unsetProperties();
		self::assertSame(0, $localizedPart->getLDocId());
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLDocId(null);
		self::assertEquals(1, $callBackCount);

		$localizedPart->setLDocId(102);
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals(102, $dbData['_LCID'][0]['lDocId']);

		$localizedPart->setLDocId(101);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals(102, $localizedPart->getLDocId());

		$instance->cleanUp();
	}

	public function testLocalizedJSONProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		self::assertNull($localizedPart->getLJSON());

		$json1 = ['test' => 0, 'test2' => true, 'Test3' => ['v1', 'v2']];

		self::assertSame($localizedPart, $localizedPart->setLJSON($json1));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);

		self::assertEquals($json1, $localizedPart->getLJSON());

		$localizedPart->setLJSON($json1);
		self::assertEquals(1, $callBackCount);

		$localizedPart->unsetProperties();
		self::assertNull($localizedPart->getLJSON());
		self::assertEquals(1, $callBackCount);
		$localizedPart->setLJSON(null);
		self::assertEquals(1, $callBackCount);

		$json2 = ['test' => 4, 'test2' => false, 'Test3' => ['v1', 'v2']];
		$localizedPart->setLJSON($json2);
		self::assertEquals(2, $callBackCount);
		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals($json2, $dbData['_LCID'][0]['lJSON']);

		$localizedPart->setLJSON($json1);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals($json2, $localizedPart->getLJSON());

		$localizedPart->setLJSON(null);
		self::assertNull($localizedPart->getLJSON());

		$localizedPart->setLJSON([]);
		self::assertNull($localizedPart->getLJSON());

		$instance->cleanUp();
	}

	public function testLocalizedRichTextProperty()
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		/** @var $instance \ChangeTests\Generators\Documents\InlineCompile */
		$instance = $documentManager->getNewInlineInstanceByModelName('ChangeTests_Generators_InlineCompile');

		$refLCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$instance->setRefLCID($refLCID);

		$callBackCount = 0;
		$instance->link(static function () use (&$callBackCount) { $callBackCount++; });
		$localizedPart = $instance->getRefLocalization();
		$richtextProperty = $localizedPart->getLRichText();
		self::assertInstanceOf(\Change\Documents\RichtextProperty::class, $richtextProperty);
		self::assertTrue($richtextProperty->isEmpty());

		$richtext1 = new \Change\Documents\RichtextProperty();
		$richtext1->setEditor('Html')->setRawText('Dummy text');

		$richtext2 = new \Change\Documents\RichtextProperty();
		$richtext2->setEditor('Html')->setRawText('Dummy text 2');

		self::assertSame($localizedPart, $localizedPart->setLRichText($richtext1));
		self::assertTrue($localizedPart->isNew());
		self::assertTrue($localizedPart->isModified());
		self::assertFalse($localizedPart->isEmpty());
		self::assertEquals(1, $callBackCount);

		self::assertEquals($richtext1->toArray(), $localizedPart->getLRichText()->toArray());

		$localizedPart->setLRichText($richtext1->toArray());
		self::assertEquals(1, $callBackCount);

		$localizedPart->unsetProperties();
		$richtextProperty = $localizedPart->getLRichText();
		self::assertTrue($richtextProperty->isEmpty());

		self::assertEquals(1, $callBackCount);
		$localizedPart->setLRichText(null);
		self::assertEquals(1, $callBackCount);

		$localizedPart->setLRichText($richtext2);
		self::assertEquals(2, $callBackCount);

		$dbData = $instance->dbData();
		self::assertEquals(2, $callBackCount);
		self::assertEquals($richtext2->toArray(), $dbData['_LCID'][0]['lRichText']);

		$localizedPart->setLRichText($richtext1);
		self::assertEquals(3, $callBackCount);
		self::assertTrue($localizedPart->isModified());

		$instance->dbData($dbData);
		self::assertNotSame($localizedPart, $instance->getRefLocalization());
		$localizedPart = $instance->getRefLocalization();
		self::assertEquals(3, $callBackCount);
		self::assertFalse($localizedPart->isModified());
		self::assertEquals($richtext2->toArray(), $localizedPart->getLRichText()->toArray());

		$instance->cleanUp();
	}
}