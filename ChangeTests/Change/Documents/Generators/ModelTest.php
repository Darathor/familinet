<?php
namespace ChangeTests\Documents\Generators;

/**
 * @name \ChangeTests\Documents\Generators\ModelTest
 */
class ModelTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Documents\Generators\Compiler
	 */
	protected function getCompiler()
	{
		return new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());
	}

	public function testConstruct()
	{
		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		self::assertEquals('change', $model->getVendor());
		self::assertEquals('generic', $model->getShortModuleName());
		self::assertEquals('document', $model->getShortName());
		self::assertEquals('change_generic_document', $model->getName());

		self::assertNull($model->getParent());
		self::assertCount(0, $model->getProperties());
		self::assertCount(0, $model->getInverseProperties());
		self::assertNull($model->getExtends());
		self::assertNull($model->getLocalized());
		self::assertNull($model->getPublishable());
		self::assertNull($model->getEditable());
		self::assertNull($model->getInline());
		self::assertNull($model->getAbstract());

		self::assertEquals('Compilation\change\generic\Documents', $model->getCompilationNameSpace());
		self::assertEquals('change\generic\Documents', $model->getNameSpace());

		self::assertEquals('documentModel', $model->getShortModelClassName());
		self::assertEquals('\Compilation\change\generic\Documents\documentModel', $model->getModelClassName());

		self::assertEquals('document', $model->getShortBaseDocumentClassName());
		self::assertEquals('\Compilation\change\generic\Documents\document', $model->getBaseDocumentClassName());

		self::assertEquals('document', $model->getShortDocumentClassName());
		self::assertEquals('\change\generic\Documents\document', $model->getDocumentClassName());

		self::assertEquals('Localizeddocument', $model->getShortDocumentLocalizedClassName());
		self::assertEquals('\Compilation\change\generic\Documents\Localizeddocument', $model->getDocumentLocalizedClassName());

		return $model;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Documents\Generators\Model $model
	 */
	public function testSetXmlDocument(\Change\Documents\Generators\Model $model)
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document editable="true" publishable="true" abstract="true" localized="true">
	<properties>
		<property name="test" />
	</properties>
</document>');
		$model->setXmlDocument($doc, $compiler);
		self::assertTrue($model->getAbstract());

		$model->addStandardProperties();
		self::assertCount(14, $model->getProperties());

		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('test'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('creationDate'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('modificationDate'));

		self::assertTrue($model->getLocalized());
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('refLCID'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('LCID'));

		self::assertTrue($model->getEditable());
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('label'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('authorId'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('documentVersion'));

		self::assertTrue($model->getPublishable());
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('title'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('publicationSections'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('publicationStatus'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('startPublication'));
		self::assertInstanceOf(\Change\Documents\Generators\Property::class, $model->getPropertyByName('endPublication'));
	}

	public function testInvalidDocumentNode()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<documents>
	<properties>
		<property name="test" />
	</properties>
</documents>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidEmptyAttribute()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document test="" > </document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidAttributeName()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document test="test" > </document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidTrueAttribute()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document localized="false" > </document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidPropertiesNode()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document><property name="test" /></document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidPropertyNode()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document><properties><prop name="test" /></properties></document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testInvalidDuplicatePropertyNode()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<document><properties><property name="test" /><property name="test" /></properties></document>');

		$model = new \Change\Documents\Generators\Model('change', 'generic', 'document');
		$this->expectException(\RuntimeException::class);
		$model->setXmlDocument($doc, $compiler);
	}

	public function testValidate()
	{
		$compiler = $this->getCompiler();
		$model = new \Change\Documents\Generators\Model('change', 'testing', 'test');
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<?xml version="1.0" encoding="UTF-8"?>
<document>
	<properties>
		<property name="string1" type="String" localized="true" />
		<property name="string2" type="String" />
	</properties>
</document>');

		$model->setXmlDocument($doc, $compiler);
		$model->addStandardProperties();
		self::assertCount(4, $model->getProperties());
		self::assertNull($model->getLocalized());
	}
}