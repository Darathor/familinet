<?php
namespace ChangeTests\Documents\Generators;

use Change\Documents\Generators\Property;
use Change\Documents\Generators\Model;

/**
 * @name \ChangeTests\Documents\Generators\PropertyTest
 */
class PropertyTest extends \ChangeTests\Change\TestAssets\TestCase // NOSONAR
{
	/**
	 * @return \Change\Documents\Generators\Compiler
	 */
	protected function getCompiler()
	{
		return new \Change\Documents\Generators\Compiler($this->getApplication(), $this->getApplicationServices());
	}

	public function testConstruct()
	{
		$model = new Model('vendor', 'module', 'name');
		$p = new Property($model, 'name', 'String');
		self::assertEquals('name', $p->getName());
		self::assertEquals('String', $p->getType());
		self::assertEquals('String', $p->getComputedType());
		self::assertEquals(0, $p->getComputedMinOccurs());
		self::assertEquals(100, $p->getComputedMaxOccurs());
		self::assertNull($p->getDocumentType());
		self::assertNull($p->getDefaultValue());
		self::assertNull($p->getDefaultPhpValue());
		self::assertNull($p->getRequired());
		self::assertNull($p->getMinOccurs());
		self::assertNull($p->getMaxOccurs());
		self::assertNull($p->getLocalized());
		self::assertNull($p->getInternal());
		self::assertCount(0, $p->getConstraintArray());
		self::assertNull($p->getParent());
		self::assertNull($p->getStateless());
		self::assertEquals($model, $p->getModel());
		self::assertEquals($p, $p->getRoot());
		self::assertCount(0, $p->getAncestors());
		self::assertFalse($p->hasRelation());
	}

	public function testInvalidAttribute()
	{
		$model = new Model('vendor', 'module', 'name');

		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property invalid="id" />');
		$compiler = $this->getCompiler();
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid property attribute');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid property attribute', $e->getMessage());
		}
	}

	public function testInvalidAttributeValue()
	{
		$model = new Model('vendor', 'module', 'name');

		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="" />');
		$compiler = $this->getCompiler();
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid empty or spaced');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid empty or spaced', $e->getMessage());
		}

		$doc->loadXML('<property name=" test" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid empty or spaced');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid empty or spaced', $e->getMessage());
		}
	}

	public function testInvalidChildrenNode()
	{
		$model = new Model('vendor', 'module', 'name');
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test"><test /></property>');
		$compiler = $this->getCompiler();
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid property children node');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid property children node', $e->getMessage());
		}
	}

	public function testNameAttribute()
	{
		$compiler = $this->getCompiler();
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" />');
		$model = new Model('vendor', 'module', 'name');
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals('test', $p->getName());

		$reservedNames = Property::getReservedPropertyNames();
		self::assertGreaterThan(1, count($reservedNames));

		$doc->loadXML('<property name="' . $reservedNames[0] . '" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid property Name');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid name attribute value', $e->getMessage());
		}

		$doc->loadXML('<property />');
		try
		{

			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Property name can not be null');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Property name can not be null', $e->getMessage());
		}
	}

	public function testTypeAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" type="String" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals('String', $p->getType());

		$doc->loadXML('<property name="test" type="string"/>');

		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid property Type');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid type attribute value', $e->getMessage());
		}
	}

	public function testDocumentTypeAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" document-type="Change_Tests_Basic" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals('Change_Tests_Basic', $p->getDocumentType());
	}

	public function testDefaultValueAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" default-value="test" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals('test', $p->getDefaultValue());
	}

	public function testRequiredAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" required="true" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->getRequired());

		$doc->loadXML('<property name="test" required="false" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid required attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid required attribute value', $e->getMessage());
		}
	}

	public function testInternalAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" internal="true" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->getInternal());

		$doc->loadXML('<property name="test" internal="false" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid internal attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid internal attribute value', $e->getMessage());
		}
	}

	public function testMinOccursAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" min-occurs="2" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals(2, $p->getMinOccurs());

		$doc->loadXML('<property name="test" min-occurs="0" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid min-occurs attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid min-occurs attribute value', $e->getMessage());
		}
	}

	public function testMaxOccursAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" max-occurs="2" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertEquals(2, $p->getMaxOccurs());

		$doc->loadXML('<property name="test" max-occurs="0" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid max-occurs attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid max-occurs attribute value', $e->getMessage());
		}
	}

	public function testLocalizedAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" localized="true" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->getLocalized());

		$doc->loadXML('<property name="test" localized="false" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid localized attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid localized attribute value', $e->getMessage());
		}
	}

	public function testStatelessAttribute()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" stateless="true" />');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->getStateless());

		$doc->loadXML('<property name="test" stateless="false" />');
		try
		{
			$p = new Property($model);
			$p->initialize($doc->documentElement, $compiler);
			self::fail('Invalid stateless attribute value');
		}
		catch (\RuntimeException $e)
		{
			self::assertStringStartsWith('Invalid stateless attribute value', $e->getMessage());
		}
	}

	public function testConstraintNode()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test"><constraint name="minSize" min="5" /></property>');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		$ca = $p->getConstraintArray();
		self::assertCount(1, $ca);
		self::assertArrayHasKey('minSize', $ca);
		self::assertCount(1, $ca['minSize']);
		self::assertArrayHasKey('min', $ca['minSize']);
		self::assertEquals(5, $ca['minSize']['min']);
	}

	public function testDbOptionsNode()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test"><dboptions length="80" /></property>');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		$opts = $p->getDbOptions();
		self::assertCount(1, $opts);
		self::assertArrayHasKey('length', $opts);
		self::assertEquals(80, $opts['length']);
	}

	public function testHasRelation()
	{
		$model = new Model('vendor', 'module', 'name');
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" type="Document" />');
		$compiler = $this->getCompiler();

		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->hasRelation());

		$doc->loadXML('<property name="test" type="DocumentArray" />');
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->hasRelation());

		$doc->loadXML('<property name="test" type="DocumentId" />');
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertTrue($p->hasRelation());

		$doc->loadXML('<property name="test" type="String" />');
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		self::assertFalse($p->hasRelation());
	}

	public function testSetDefaultConstraints()
	{
		$doc = new \DOMDocument('1.0', 'utf-8');
		$doc->loadXML('<property name="test" type="String"><constraint name="min" min="5" /><constraint name="maxSize" max="8" /></property>');
		$model = new Model('vendor', 'module', 'name');
		$compiler = $this->getCompiler();
		$p = new Property($model);
		$p->initialize($doc->documentElement, $compiler);
		$p->applyDefaultProperties();
		self::assertEquals(['min' => ['min' => '5'], 'maxSize' => ['max' => '8']], $p->getConstraintArray());
	}

	public function testValidate()
	{
		$model = new Model('vendor', 'module', 'name');

		$p = new Property($model, 'label');
		$p->applyDefaultProperties();
		self::assertNull($p->getType());

		$p = new Property($model, 'label', 'Integer');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());
		self::assertTrue($p->getRequired());
		self::assertEquals(['maxSize' => ['max' => 255]], $p->getConstraintArray());

		$p = new Property($model, 'refLCID');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());
		self::assertTrue($p->getRequired());
		self::assertEquals(['maxSize' => ['max' => 5]], $p->getConstraintArray());

		$p = new Property($model, 'LCID');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());
		self::assertTrue($p->getRequired());
		self::assertEquals(['maxSize' => ['max' => 5]], $p->getConstraintArray());

		$p = new Property($model, 'authorId');
		$p->applyDefaultProperties();
		self::assertEquals('DocumentId', $p->getType());
		self::assertNull($p->getDocumentType());

		$p = new Property($model, 'documentVersion');
		$p->applyDefaultProperties();
		self::assertEquals('Integer', $p->getType());
		self::assertNull($p->getDefaultValue());
		self::assertNull($p->getRequired());

		$p = new Property($model, 'title');
		$p->applyDefaultProperties();
		self::assertNull($p->getType());

		$p = new Property($model, 'title', 'Integer');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());

		$p = new Property($model, 'publicationSections');
		$p->applyDefaultProperties();
		self::assertNull($p->getType());
		self::assertNull($p->getDocumentType());

		$p = new Property($model, 'publicationStatus');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());
		self::assertEquals('DRAFT', $p->getDefaultValue());
		self::assertTrue($p->getRequired());

		$p = new Property($model, 'startPublication');
		$p->applyDefaultProperties();
		self::assertEquals('DateTime', $p->getType());

		$p = new Property($model, 'endPublication');
		$p->applyDefaultProperties();
		self::assertEquals('DateTime', $p->getType());

		$p = new Property($model, 'treeName');
		$p->applyDefaultProperties();
		self::assertEquals('String', $p->getType());
		self::assertEquals(['maxSize' => ['max' => 50]], $p->getConstraintArray());
	}

	public function testValidateInheritance()
	{
		$modelParent = new Model('vendor', 'module', 'modelParent');

		$model = new Model('vendor', 'module', 'model');
		$model->setParent($modelParent);

		$p = new Property($modelParent, 'test1');
		$modelParent->addProperty($p);

		$p->validateInheritance();
		self::assertEquals('String', $p->getType());

		$p2 = new Property($model, 'test1');
		$model->addProperty($p2);
		$p2->validateInheritance();
		self::assertNull($p2->getType());
		self::assertSame($p, $p2->getParent());
		self::assertEquals('String', $p2->getComputedType());
	}
}