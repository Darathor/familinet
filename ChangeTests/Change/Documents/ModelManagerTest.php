<?php
namespace ChangeTests\Change\Documents;

class ModelManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	public function testInitialize()
	{
		$modelManager = $this->getApplicationServices()->getModelManager();
		self::assertInstanceOf(\Change\Documents\ModelManager::class, $modelManager);
		return $modelManager;
	}

	/**
	 * @depends testInitialize
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return \Change\Documents\ModelManager
	 */
	public function testGetModelByName($modelManager)
	{
		$m = $modelManager->getModelByName('Project_Tests_Basic');
		self::assertInstanceOf(\Compilation\Project\Tests\Documents\BasicModel::class, $m);
		self::assertEquals('Project_Tests_Basic', $m->getName());
		return $modelManager;
	}
}
