<?php
/** @noinspection DuplicatedCode */
namespace ChangeTests\Change\Documents\Query;

use Change\Documents\Query\Query;

class BuilderTest extends \ChangeTests\Change\TestAssets\TestCase // NOSONAR
{

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	protected function initializeDB()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$dm = $this->getApplicationServices()->getDocumentManager();
		$basicDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc->initialize(1000);
		$basicDoc->setPStr('Test 1000');
		$basicDoc->setPInt(1001);
		$basicDoc->setPDocId(1002);
		$basicDoc->save();

		$basicDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc->initialize(1001);
		$basicDoc->setPStr('1001 Test');
		$basicDoc->setPInt(1001);
		$basicDoc->setPDocId(1000);
		$basicDoc->save();

		$basicDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc->initialize(1002);
		$basicDoc->setPStr('1002');
		$basicDoc->setPInt(7);
		$basicDoc->setPDocId(1000);
		$basicDoc->save();

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$dm->pushLCID('fr_FR');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1005);
		$localizedDoc->setPStr('test 1005');
		$localizedDoc->getCurrentLocalization()->setPLStr('text un');
		$localizedDoc->setPInt(1001);
		$localizedDoc->getCurrentLocalization()->setPLInt(1001);
		$localizedDoc->setPDocInst($dm->getDocumentInstance(1000));
		$localizedDoc->setPDocArr([$dm->getDocumentInstance(1000), $dm->getDocumentInstance(1002)]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->getCurrentLocalization()->setPLInt(1002);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1006);
		$localizedDoc->setPStr('test 1006');
		$localizedDoc->getCurrentLocalization()->setPLStr('text two');
		$localizedDoc->getCurrentLocalization()->setPLInt(7);
		$localizedDoc->setPDocInst($dm->getDocumentInstance(1001));
		$localizedDoc->setPDocArr([$dm->getDocumentInstance(1002)]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->save();
		$dm->popLCID();
	}

	/**
	 * @param $model
	 * @return Query
	 */
	protected function getNewQuery($model)
	{
		return $this->getApplicationServices()->getDocumentManager()->getNewQuery($model);
	}

	public function testConstruct()
	{
		$this->initializeDB();

		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		self::assertSame($model, $builder->getModel());
		self::assertSame($builder, $builder->getQuery());

		$fb = $builder->getFragmentBuilder();
		self::assertInstanceOf(\Change\Db\Query\SQLFragmentBuilder::class, $fb);

		$pb = $builder->getPredicateBuilder();
		self::assertInstanceOf(\Change\Documents\Query\PredicateBuilder::class, $pb);

		$builder2 = $this->getNewQuery('Project_Tests_Basic');
		self::assertSame($model, $builder2->getModel());

		try
		{
			$this->getNewQuery('Project_Tests_Invalid');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 2 must by a valid', $e->getMessage());
		}
	}

	/**
	 * @depends testConstruct
	 */
	public function testGetFirstDocument()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		$doc = $builder->getFirstDocument();
		self::assertInstanceOf(\Project\Tests\Documents\Basic::class, $doc);
	}

	/**
	 * @depends testGetFirstDocument
	 */
	public function testGetDocuments()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		$documents = $builder->getDocuments();
		self::assertInstanceOf(\Change\Documents\DocumentCollection::class, $documents);
		self::assertEquals(3, $documents->count());
	}

	/**
	 * @depends testGetDocuments
	 */
	public function testGetCountDocuments()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		self::assertEquals(3, $builder->getCountDocuments());
	}

	/**
	 * @depends testGetCountDocuments
	 */
	public function testGetQueryBuilder()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		$qb = $builder->dbQueryBuilder();
		$rows = $qb->query()->getResults();
		self::assertCount(3, $rows);
		self::assertArrayHasKey('pstr', $rows[0]);
		self::assertArrayHasKey('pbool', $rows[0]);
	}

	/**
	 * @depends testGetQueryBuilder
	 */
	public function testJoin()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		self::assertNull($builder->getJoin('_test'));
		$te = new \Change\Db\Query\Expressions\Table('test');
		$j = new \Change\Db\Query\Expressions\Join($te);
		self::assertSame($builder, $builder->addJoin('_test', $j));
		self::assertSame($j, $builder->getJoin('_test'));
	}

	/**
	 * @depends testJoin
	 */
	public function testGetNextAliasCounter()
	{
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		self::assertEquals(1, $builder->getNextAliasCounter());
		self::assertEquals(2, $builder->getNextAliasCounter());
	}

	/**
	 * @depends testGetNextAliasCounter
	 */
	public function testLCID()
	{
		$LCID = $this->getApplicationServices()->getDocumentManager()->getLCID();
		$model = $this->getApplicationServices()->getModelManager()->getModelByName('Project_Tests_Basic');
		$builder = $this->getNewQuery($model);
		self::assertNull($builder->getLCID());
		$builder->setLCID($LCID);
		self::assertEquals($LCID, $builder->getLCID());
	}

	/**
	 * @depends testLCID
	 */
	public function testTableAliasName()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		self::assertEquals('_t0', $builder->getTableAliasName());
	}

	/**
	 * @depends testTableAliasName
	 */
	public function testLocalized()
	{
		$builder = $this->getNewQuery('Project_Tests_Localized');
		self::assertFalse($builder->hasLocalizedTable());
		self::assertEquals('_t0L', $builder->getLocalizedTableAliasName());
		self::assertTrue($builder->hasLocalizedTable());
	}

	/**
	 * @depends testLocalized
	 */
	public function testGetValidProperty()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		$p = $builder->getValidProperty('pStr');
		self::assertInstanceOf(\Change\Documents\Property::class, $p);
		self::assertNull($builder->getValidProperty('invalid'));
	}

	/**
	 * @depends testGetValidProperty
	 */
	public function testPropertyBuilder()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		$cb = $builder->getPropertyBuilder('pDocArr');
		self::assertInstanceOf(\Change\Documents\Query\ChildBuilder::class, $cb);
		self::assertEquals('Project_Tests_Localized', $cb->getModel()->getName());

		try
		{
			$builder->getPropertyBuilder('invalid');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 1 must be a valid', $e->getMessage());
		}
	}

	/**
	 * @depends testPropertyBuilder
	 */
	public function testModelBuilder()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		$cb = $builder->getModelBuilder('Project_Tests_Localized', 'pDocId');
		self::assertInstanceOf(\Change\Documents\Query\ChildBuilder::class, $cb);
		self::assertEquals('Project_Tests_Localized', $cb->getModel()->getName());
		try
		{
			$builder->getModelBuilder('Project_Tests_Invalid', 'pDocId');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 1 must be a valid', $e->getMessage());
		}

		try
		{
			$builder->getModelBuilder('Project_Tests_Localized', 'invalid');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 2 must be a valid', $e->getMessage());
		}
	}

	/**
	 * @depends testModelBuilder
	 */
	public function testGetPropertyModelBuilder()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		$cb = $builder->getPropertyModelBuilder('pDocId', 'Project_Tests_Localized', 'pInt');
		self::assertInstanceOf(\Change\Documents\Query\ChildBuilder::class, $cb);
		self::assertEquals('Project_Tests_Localized', $cb->getModel()->getName());
		try
		{
			$builder->getPropertyModelBuilder('invalid', 'Project_Tests_Localized', 'pInt');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 1 must be a valid', $e->getMessage());
		}

		try
		{
			$builder->getPropertyModelBuilder('pDocId', 'Project_Tests_Invalid', 'pInt');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 2 must be a valid', $e->getMessage());
		}

		try
		{
			$builder->getPropertyModelBuilder('pDocId', 'Project_Tests_Localized', 'invalid');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 3 must be a valid', $e->getMessage());
		}

		try
		{
			$builder->getPropertyModelBuilder('pDocArr', 'Project_Tests_Localized', 'pDocArr');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Invalid Properties type', $e->getMessage());
		}
	}

	/**
	 * @depends testGetPropertyModelBuilder
	 */
	public function testGetColumn()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		self::assertInstanceOf(\Change\Db\Query\Expressions\Column::class, $builder->getColumn('pStr'));
		self::assertInstanceOf(\Change\Db\Query\Expressions\Column::class, $builder->getColumn($builder->getModel()->getProperty('pInt')));

		try
		{
			$builder->getColumn('invalid');
			self::fail('Exception expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 1 must be a valid', $e->getMessage());
		}
	}

	/**
	 * @depends testGetColumn
	 */
	public function testGetValueAsParameter()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');

		$p = $builder->getValueAsParameter(12);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $p);
		self::assertEquals(\Change\Db\ScalarType::INTEGER, $p->getType());

		$p = $builder->getValueAsParameter(new \DateTime());
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $p);
		self::assertEquals(\Change\Db\ScalarType::DATETIME, $p->getType());

		$p = $builder->getValueAsParameter(false, \Change\Documents\Property::TYPE_BOOLEAN);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $p);
		self::assertEquals(\Change\Db\ScalarType::BOOLEAN, $p->getType());

		$p = $builder->getValueAsParameter(null, $builder->getModel()->getProperty('pInt'));
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $p);
		self::assertEquals(\Change\Db\ScalarType::INTEGER, $p->getType());

		$p = $builder->getValueAsParameter('invalid', 12);
		self::assertInstanceOf(\Change\Db\Query\Expressions\Parameter::class, $p);
		self::assertEquals(\Change\Db\ScalarType::STRING, $p->getType());
	}

	/**
	 * @depends testGetValueAsParameter
	 */
	public function testOrder()
	{
		$builder = $this->getNewQuery('Project_Tests_Basic');
		$builder->addOrder('id');
		$ids = $builder->getDocuments()->ids();
		self::assertCount(3, $ids);
		self::assertEquals([1000, 1001, 1002], $ids);

		$builder = $this->getNewQuery('Project_Tests_Basic');
		$builder->addOrder('id', false);
		$ids = $builder->getDocuments()->ids();
		self::assertCount(3, $ids);
		self::assertEquals([1002, 1001, 1000], $ids);

		$builder = $this->getNewQuery('Project_Tests_Localized');
		$builder->getPropertyBuilder('pDocInst')->addOrder('pDocId');
		$ids = $builder->getDocuments()->ids();
		self::assertCount(2, $ids);
		self::assertEquals([1006, 1005], $ids);

		$builder = $this->getNewQuery('Project_Tests_Localized');
		$builder->getPropertyBuilder('pDocInst')->addOrder('pDocId', false);
		$ids = $builder->getDocuments()->ids();
		self::assertCount(2, $ids);
		self::assertEquals([1005, 1006], $ids);
	}

}