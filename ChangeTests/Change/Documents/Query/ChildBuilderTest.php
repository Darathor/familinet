<?php
namespace ChangeTests\Change\Documents\Query;

use Change\Documents\Query\ChildBuilder;
use ChangeTests\Change\TestAssets\TestCase;

class ChildBuilderTest extends TestCase
{

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function testConstruct()
	{
		$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');

		$childBuilder = new ChildBuilder($builder, 'Project_Tests_Localized', 'id', 'id');
		self::assertSame($builder, $childBuilder->getParent());
		self::assertSame($builder, $childBuilder->getQuery());
		self::assertSame($this->getApplicationServices()->getDbProvider(), $builder->getDbProvider());
		self::assertEquals('Project_Tests_Localized', $childBuilder->getModel()->getName());

		try
		{
			new ChildBuilder($builder, null, 'id', 'id');
			self::fail('Argument 2 must be a valid model');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 2 must be a valid', $e->getMessage());
		}

		try
		{
			new ChildBuilder($builder, 'Project_Tests_Localized', null, 'id');
			self::fail('Argument 3 must be a valid property');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 3 must be a valid', $e->getMessage());
		}

		try
		{
			new ChildBuilder($builder, 'Project_Tests_Localized', 'id', null);
			self::fail('Argument 4 must be a valid property');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertStringStartsWith('Argument 4 must be a valid', $e->getMessage());
		}
	}
}