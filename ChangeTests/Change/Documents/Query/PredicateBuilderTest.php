<?php
/** @noinspection DuplicatedCode */
namespace ChangeTests\Change\Documents\Query;

use Change\Documents\Query\PredicateBuilder;
use ChangeTests\Change\TestAssets\TestCase;

class PredicateBuilderTest extends TestCase // NOSONAR
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function initializeDB()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$dm = $this->getApplicationServices()->getDocumentManager();
		self::assertInstanceOf(\Change\Documents\DocumentManager::class, $dm);

		/** @var \Project\Tests\Documents\Basic $basicDoc0 */
		$basicDoc0 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc0->initialize(1000);
		$basicDoc0->setPStr('Test 1000');
		$basicDoc0->setPInt(1001);
		$basicDoc0->setPFloat(5.0);
		$basicDoc0->setPDocId(1002);
		$basicDoc0->save();

		/** @var \Project\Tests\Documents\Basic $basicDoc1 */
		$basicDoc1 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc1->initialize(1001);
		$basicDoc1->setPStr('1001 Test');
		$basicDoc1->setPInt(1001);
		$basicDoc1->setPDocId(1000);
		$basicDoc1->save();

		/** @var \Project\Tests\Documents\Basic $basicDoc2 */
		$basicDoc2 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc2->initialize(1002);
		$basicDoc2->setPStr('1002');
		$basicDoc2->setPInt(7);
		$basicDoc2->setPDocId(1000);
		$basicDoc2->save();

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$dm->pushLCID('fr_FR');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1005);
		$localizedDoc->setPStr('test 1005');
		$localizedDoc->getCurrentLocalization()->setPLStr('text un');
		$localizedDoc->setPInt(1001);
		$localizedDoc->getCurrentLocalization()->setPLInt(1001);
		$localizedDoc->setPDocInst($basicDoc0);
		$localizedDoc->setPDocArr([$basicDoc0, $basicDoc2]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->getCurrentLocalization()->setPLInt(1002);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1006);
		$localizedDoc->setPStr('test 1006');
		$localizedDoc->getCurrentLocalization()->setPLStr('text two');
		$localizedDoc->getCurrentLocalization()->setPLInt(7);
		$localizedDoc->setPDocInst($basicDoc1);
		$localizedDoc->setPDocArr([$basicDoc2]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->save();
		$dm->popLCID();

		$treeManager = $this->getApplicationServices()->getTreeManager();
		/* @var $folderDoc \Rbs\Generic\Documents\Folder */
		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('Root');
		$folderDoc->initialize(2000);
		$folderDoc->save();
		$rn = $treeManager->insertRootNode($folderDoc, 'Project_Tests');

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 1');
		$folderDoc->initialize(2001);
		$folderDoc->save();
		$treeManager->insertNode($rn, $folderDoc);
		$treeManager->insertNode($rn, $basicDoc0);

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 10');
		$folderDoc->initialize(2010);
		$folderDoc->save();
		$n = $treeManager->insertNode($treeManager->getNodeById(2001), $folderDoc);
		$treeManager->insertNode($n, $basicDoc1);
		$treeManager->insertNode($n, $basicDoc2);

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 11');
		$folderDoc->initialize(2011);
		$folderDoc->save();
		$treeManager->insertNode($n, $folderDoc, $treeManager->getNodeById(1002));

		/* @var $corDoc \Project\Tests\Documents\Correction */
		$corDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Correction');
		$corDoc->initialize(3000);
		$corDoc->setLabel('C0');
		$corDoc->save();

		$corDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Correction');
		$corDoc->initialize(3001);
		$corDoc->setLabel('C1');
		$corDoc->save();
		$corDoc->getCurrentLocalization()->setPublicationStatus(\Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE);
		$corDoc->save();

		/* @var $actDoc \Project\Tests\Documents\Activable */
		$actDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Activable');
		$actDoc->initialize(4000);
		$actDoc->setLabel('A0');
		$actDoc->getCurrentLocalization()->setActive(false);
		$actDoc->save();

		$actDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Activable');
		$actDoc->initialize(4001);
		$actDoc->setLabel('A1');
		$actDoc->getCurrentLocalization()->setActive(true);
		$actDoc->save();
	}

	public function testConstruct()
	{
		$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
		$pb = new PredicateBuilder($builder);
		self::assertNotNull($pb);
	}

	public function testLogicAnd()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();

			$predicate = $pb->logicAnd($pb->eq('id', 1001), $pb->eq('pInt', 1001));
			self::assertInstanceOf(\Change\Db\Query\Predicates\Conjunction::class, $predicate);

			self::assertCount(2, $predicate->getArguments());
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1001, $ids);

			$predicate = $pb->logicAnd([$pb->eq('id', 1001), $pb->eq('pInt', 1001)]);
			self::assertInstanceOf(\Change\Db\Query\Predicates\Conjunction::class, $predicate);

			self::assertCount(2, $predicate->getArguments());

			$predicate = $pb->logicAnd([$pb->eq('id', 1001), $pb->eq('pInt', 1001), $pb->eq('id', 1001)]);
			self::assertCount(3, $predicate->getArguments());

			try
			{
				/** @noinspection PhpParamsInspection */
				$pb->logicAnd(11);
				self::fail('InvalidArgumentException expected');
			}
			catch (\InvalidArgumentException $e)
			{
				$str = 'Argument 1 must be a valid InterfaceSQLFragment';
				self::assertEquals($str, $e->getMessage());
			}
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testLogicOr()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();

			$predicate = $pb->logicOr($pb->eq('id', 1001), $pb->eq('pInt', 1001));
			self::assertInstanceOf(\Change\Db\Query\Predicates\Disjunction::class, $predicate);

			self::assertCount(2, $predicate->getArguments());
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1000, $ids);
			self::assertContains(1001, $ids);

			$predicate = $pb->logicOr([$pb->eq('id', 1001), $pb->eq('pInt', 1001)]);
			self::assertInstanceOf(\Change\Db\Query\Predicates\Disjunction::class, $predicate);

			self::assertCount(2, $predicate->getArguments());

			$predicate = $pb->logicOr([$pb->eq('id', 1001), $pb->eq('pInt', 1001), $pb->eq('id', 1001)]);
			self::assertCount(3, $predicate->getArguments());

			try
			{
				/** @noinspection PhpParamsInspection */
				$pb->logicOr(11);
				self::fail('InvalidArgumentException expected');
			}
			catch (\InvalidArgumentException $e)
			{
				$str = 'Argument 1 must be a valid InterfaceSQLFragment';
				self::assertEquals($str, $e->getMessage());
			}
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testEq()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->eq('id', 1000);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1000, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Localized');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->eq('pLStr', 'text un');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1005, $ids);

			try
			{
				$pb->eq('invalid', 'text un');
				self::fail('InvalidArgumentException expected');
			}
			catch (\InvalidArgumentException $e)
			{
				$str = 'Argument 1 must be a valid property';
				self::assertEquals($str, $e->getMessage());
			}
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testNeq()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->neq('id', 1000);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1002, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testGt()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->gt('id', 1001);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1002, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testLt()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->lt('id', 1001);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1000, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testGte()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->gte('id', 1001);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1002, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testLte()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->lte('id', 1001);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1000, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testLike()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->like('pStr', 'test');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1000, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->like('pStr', 'test', \Change\Db\Query\Predicates\Like::ANYWHERE, true);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->like('pStr', 'test', \Change\Db\Query\Predicates\Like::BEGIN);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1000, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->like('pStr', 'test', \Change\Db\Query\Predicates\Like::END);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1001, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testIn()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->in('id', 1000, 2000, 1002);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1000, $ids);
			self::assertContains(1002, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->in('id', ['A', 1000, '1001']);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1000, $ids);
			self::assertContains(1001, $ids);

			// Special case for document array properties.
			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Localized');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->in('pDocArr', 1000, 2000, 1002);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1005, $ids);
			self::assertContains(1006, $ids);

			try
			{
				$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
				$pb = $builder->getPredicateBuilder();
				$predicate = $pb->in('id', []);
				$builder->andPredicates($predicate);
				$builder->getDocuments()->ids();
				self::fail('RuntimeException expected');
			}
			catch (\RuntimeException $e)
			{
				$str = 'Right Hand Expression must be a ExpressionList with one element or more';
				self::assertEquals($str, $e->getMessage());
			}
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testNotIn()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->notIn('id', 1000, 2000, 1002);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1001, $ids);

			// Special case for document array properties.
			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Localized');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->notIn('pDocArr', 1000, 2000, 1003);
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1006, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testIsNull()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNull('pFloat');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1002, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNull('pDocArr');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(3, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Localized');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNull('pDocArr');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testIsNotNull()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNotNull('pFloat');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1000, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNotNull('pDocArr');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Localized');
			$pb = $builder->getPredicateBuilder();
			$predicate = $pb->isNotNull('pDocArr');
			$builder->andPredicates($predicate);
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testPublished()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$str = 'Model is not publishable: Project_Tests_Basic';
			try
			{

				$builder->andPredicates($pb->published());
				self::fail('Exception');
			}
			catch (\RuntimeException $e)
			{
				self::assertEquals($str, $e->getMessage());
			}

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Correction');
			$pb = $builder->getPredicateBuilder();
			$builder->andPredicates($pb->published());
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(3001, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testPublishedAt()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Correction');
			$pb = $builder->getPredicateBuilder();
			$at = \DateTime::createFromFormat(\DateTime::ATOM, '2013-07-04T15:04:08+00:00');
			$to = \DateTime::createFromFormat(\DateTime::ATOM, '2013-08-04T15:04:00+00:00');
			$predicate = $pb->published($at, $to);
			self::assertEquals('2013-07-04T15:05:00+00:00', $at->format(\DateTime::ATOM));
			self::assertEquals('2013-08-04T15:04:00+00:00', $to->format(\DateTime::ATOM));

			$builder->andPredicates($predicate);
			self::assertEquals('("_t0L"."publicationstatus" = :_p1 AND "_t0L"."startpublication" <= :_p2 AND "_t0L"."endpublication" > :_p3)',
				$predicate->toSQL92String());
			$q = $builder->dbQueryBuilder()->query();
			self::assertEquals($at, $q->getParameterValue('_p2'));
			self::assertEquals($to, $q->getParameterValue('_p3'));
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testActivated()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = $builder->getPredicateBuilder();
			$str = 'Model is not activable: Project_Tests_Basic';
			try
			{

				$builder->andPredicates($pb->activated());
				self::fail('Exception');
			}
			catch (\RuntimeException $e)
			{
				self::assertEquals($str, $e->getMessage());
			}

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Activable');
			$pb = $builder->getPredicateBuilder();
			$builder->andPredicates($pb->activated());
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(4001, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testActivatedAt()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();

			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Activable');
			$pb = $builder->getPredicateBuilder();
			$at = \DateTime::createFromFormat(\DateTime::ATOM, '2013-07-04T15:04:00+00:00');
			$to = \DateTime::createFromFormat(\DateTime::ATOM, '2013-08-04T15:04:08+00:00');
			$predicate = $pb->activated($at, $to);
			self::assertEquals('2013-07-04T15:04:00+00:00', $at->format(\DateTime::ATOM));
			self::assertEquals('2013-08-04T15:05:00+00:00', $to->format(\DateTime::ATOM));

			$builder->andPredicates($predicate);
			self::assertEquals('("_t0L"."active" = :_p1 AND "_t0L"."startactivation" <= :_p2 AND "_t0L"."endactivation" > :_p3)',
				$predicate->toSQL92String());
			$q = $builder->dbQueryBuilder()->query();
			self::assertTrue($q->getParameterValue('_p1'));
			self::assertEquals($at, $q->getParameterValue('_p2'));
			self::assertEquals($to, $q->getParameterValue('_p3'));
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}
}