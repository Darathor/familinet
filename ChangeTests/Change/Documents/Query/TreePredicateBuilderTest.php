<?php
/** @noinspection DuplicatedCode */
namespace ChangeTests\Change\Documents\Query;

use Change\Documents\Query\TreePredicateBuilder;
use ChangeTests\Change\TestAssets\TestCase;

class TreePredicateBuilderTest extends TestCase
{

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function initializeDB()
	{
		/* @var $basicDoc \Project\Tests\Documents\Basic */
		$dm = $this->getApplicationServices()->getDocumentManager();

		/** @var \Project\Tests\Documents\Basic $basicDoc0 */
		$basicDoc0 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc0->initialize(1000);
		$basicDoc0->setPStr('Test 1000');
		$basicDoc0->setPInt(1001);
		$basicDoc0->setPFloat(5.0);
		$basicDoc0->setPDocId(1002);
		$basicDoc0->save();

		/** @var \Project\Tests\Documents\Basic $basicDoc1 */
		$basicDoc1 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc1->initialize(1001);
		$basicDoc1->setPStr('1001 Test');
		$basicDoc1->setPInt(1001);
		$basicDoc1->setPDocId(1000);
		$basicDoc1->save();

		/** @var \Project\Tests\Documents\Basic $basicDoc2 */
		$basicDoc2 = $dm->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$basicDoc2->initialize(1002);
		$basicDoc2->setPStr('1002');
		$basicDoc2->setPInt(7);
		$basicDoc2->setPDocId(1000);
		$basicDoc2->save();

		/* @var $localizedDoc \Project\Tests\Documents\Localized */
		$dm->pushLCID('fr_FR');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1005);
		$localizedDoc->setPStr('test 1005');
		$localizedDoc->getCurrentLocalization()->setPLStr('text un');
		$localizedDoc->setPInt(1001);
		$localizedDoc->getCurrentLocalization()->setPLInt(1001);
		$localizedDoc->setPDocInst($basicDoc0);
		$localizedDoc->setPDocArr([$basicDoc0, $basicDoc2]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->getCurrentLocalization()->setPLInt(1002);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedDoc->initialize(1006);
		$localizedDoc->setPStr('test 1006');
		$localizedDoc->getCurrentLocalization()->setPLStr('text two');
		$localizedDoc->getCurrentLocalization()->setPLInt(7);
		$localizedDoc->setPDocInst($basicDoc1);
		$localizedDoc->setPDocArr([$basicDoc2]);
		$localizedDoc->save();
		$dm->popLCID();

		$dm->pushLCID('en_US');
		$localizedDoc->getCurrentLocalization()->setPLStr('text one');
		$localizedDoc->save();
		$dm->popLCID();

		$treeManager = $this->getApplicationServices()->getTreeManager();
		/* @var $folderDoc \Rbs\Generic\Documents\Folder */
		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('Root');
		$folderDoc->initialize(2000);
		$folderDoc->save();
		$rn = $treeManager->insertRootNode($folderDoc, 'Project_Tests');

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 1');
		$folderDoc->initialize(2001);
		$folderDoc->save();
		$treeManager->insertNode($rn, $folderDoc);
		$treeManager->insertNode($rn, $basicDoc0);

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 10');
		$folderDoc->initialize(2010);
		$folderDoc->save();
		$n = $treeManager->insertNode($treeManager->getNodeById(2001), $folderDoc);
		$treeManager->insertNode($n, $basicDoc1);
		$treeManager->insertNode($n, $basicDoc2);

		$folderDoc = $dm->getNewDocumentInstanceByModelName('Rbs_Generic_Folder');
		$folderDoc->setLabel('F 11');
		$folderDoc->initialize(2011);
		$folderDoc->save();
		$treeManager->insertNode($n, $folderDoc, $treeManager->getNodeById(1002));

		/* @var $corDoc \Project\Tests\Documents\Correction */
		$corDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Correction');
		$corDoc->initialize(3000);
		$corDoc->setLabel('C0');
		$corDoc->save();

		$corDoc = $dm->getNewDocumentInstanceByModelName('Project_Tests_Correction');
		$corDoc->initialize(3001);
		$corDoc->setLabel('C1');
		$corDoc->save();
		$corDoc->getCurrentLocalization()->setPublicationStatus(\Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE);
		$corDoc->save();
	}

	public function testConstruct()
	{
		$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
		$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
		self::assertNotNull($pb);
	}

	public function testChildOf()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();
			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->childOf(2000));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1000, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->childOf(2001));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->childOf(2010));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(2, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1002, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testDescendantOf()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();
			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->descendantOf(2000));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(3, $ids);
			self::assertContains(1000, $ids);
			self::assertContains(1001, $ids);
			self::assertContains(1002, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->descendantOf(1000));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testAncestorOf()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();
			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->ancestorOf(2000));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(0, $ids);

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Generic_Folder');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->ancestorOf(2011));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(3, $ids);
			self::assertContains(2000, $ids);
			self::assertContains(2001, $ids);
			self::assertContains(2010, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testNextSiblingOf()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();
			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->nextSiblingOf(2011));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1002, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}

	public function testPreviousSiblingOf()
	{
		try
		{
			$this->getApplicationServices()->getTransactionManager()->begin();
			$this->initializeDB();

			$builder = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Project_Tests_Basic');
			$pb = new TreePredicateBuilder($builder, $this->getApplicationServices()->getTreeManager());
			$builder->andPredicates($pb->previousSiblingOf(1002));
			$ids = $builder->getDocuments()->ids();
			self::assertCount(1, $ids);
			self::assertContains(1001, $ids);
		}
		finally
		{
			$this->getApplicationServices()->getTransactionManager()->rollBack();
		}
	}
}
