<?php
namespace ChangeTests\Change\Documents;

class RestfulDocumentTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	public function testPopulateDocumentFromRestEvent()
	{
		$data = ['model' => 'toto', 'id' => 1, 'treeName' => 'foo', 'pStr' => 'a string', 'pBool' => true, 'pInt' => 10];

		/* @var $document \Project\Tests\Documents\Basic */
		$document = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');

		$event = new \Change\Http\Event();
		$event->setRequest(new \Change\Http\Request());
		$event->getRequest()->setPost(new \Zend\Stdlib\Parameters($data));
		$event->setParams($this->getDefaultEventArguments());

		$result = $document->populateDocumentFromRestEvent($event);
		self::assertInstanceOf(\Project\Tests\Documents\Basic::class, $result);
		self::assertEquals($data['pStr'], $document->getPStr());
		self::assertEquals('Project_Tests_Basic', $document->getDocumentModel()->getName());
		self::assertEquals('foo', $document->getTreeName());
		self::assertEquals(true, $document->getPBool());
		self::assertEquals(10, $document->getPInt());

		$document->save();

		/* @var $document \Project\Tests\Documents\Basic */
		$newDocument = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$result = $newDocument->populateDocumentFromRestEvent($event);
		self::assertEquals(false, $result);
		self::assertInstanceOf(\Change\Http\Rest\V1\ErrorResult::class, $event->getResult());
		self::assertEquals(\Zend\Http\Response::STATUS_CODE_409, $event->getResult()->getHttpStatusCode());
		self::assertEquals('DOCUMENT-ALREADY-EXIST', $event->getResult()->getErrorCode());

		unset($data['id']);
		$data['pStr'] = new \DateTime();
		$event->getRequest()->setPost(new \Zend\Stdlib\Parameters($data));
		$result = $newDocument->populateDocumentFromRestEvent($event);
		self::assertEquals(false, $result);
		self::assertInstanceOf(\Change\Http\Rest\V1\ErrorResult::class, $event->getResult());
		self::assertEquals(\Zend\Http\Response::STATUS_CODE_409, $event->getResult()->getHttpStatusCode());
		self::assertEquals('INVALID-VALUE-TYPE', $event->getResult()->getErrorCode());
		self::assertEquals('pStr', $event->getResult()->getData()['name']);

		unset($data['pStr']);
		$event = new \Change\Http\Event();
		$event->setRequest(new \Change\Http\Request());
		$event->getRequest()->setPost(new \Zend\Stdlib\Parameters($data));
		$event->setParams($this->getDefaultEventArguments());

		/** @var $newDocument \Project\Tests\Documents\Basic */
		$newDocument = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$eventManager = $this->getApplication()->getSharedEventManager();
		$eventManager->attach('Project_Tests_Basic', 'populateDocumentFromRestEvent', static function (\Change\Documents\Events\Event $event) {
			/* @var $document \Project\Tests\Documents\Basic */
			$document = $event->getDocument();
			self::assertNull($document->getPStr());
			$document->setPStr('tutu');
		}, 5);

		$result = $newDocument->populateDocumentFromRestEvent($event);
		self::assertInstanceOf(\Project\Tests\Documents\Basic::class, $result);
		self::assertEquals('tutu', $newDocument->getPStr());
	}
}