<?php
namespace ChangeTests\Change\Documents;

use Change\Documents\RichtextProperty;

/**
 * @name \ChangeTests\Change\Documents\RichtextPropertyTest
 */
class RichtextPropertyTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @param $default
	 * @return RichtextProperty
	 */
	protected function getObject($default = null)
	{
		return new RichtextProperty($default);
	}

	public function testConstruct()
	{
		$o = $this->getObject();
		self::assertNull($o->getHtml());
		self::assertNull($o->getRawText());
		self::assertTrue($o->isEmpty());
		self::assertFalse($o->isModified());
		self::assertEquals(RichtextProperty::DEFAULT_EDITOR, $o->getEditor());
		self::assertNull($o->toJSONString());
		self::assertNull($o->getDefaultJSONString());
		self::assertEquals(['e' => 'Markdown', 't' => null, 'h' => null], $o->toArray());
	}

	public function testEditor()
	{
		$o = $this->getObject();
		self::assertSame($o, $o->setEditor('Test'));
		self::assertEquals('Test', $o->getEditor());
		self::assertTrue($o->isModified());
		self::assertNull($o->getDefaultJSONString());
		$o->setEditor(null);
		self::assertFalse($o->isModified());
		self::assertEquals(RichtextProperty::DEFAULT_EDITOR, $o->getEditor());
	}

	public function testRawText()
	{
		$o = $this->getObject();
		self::assertSame($o, $o->setRawText('RawText'));
		self::assertFalse($o->isEmpty());
		self::assertTrue($o->isModified());
		self::assertNull($o->getDefaultJSONString());
		self::assertEquals('RawText', $o->getRawText());
		$o->setRawText(null);
		self::assertFalse($o->isModified());
		self::assertTrue($o->isEmpty());
	}

	public function testHtml()
	{
		$o = $this->getObject();
		self::assertSame($o, $o->setHtml('Html'));
		self::assertEquals('Html', $o->getHtml());
		self::assertTrue($o->isModified());
		self::assertNull($o->getDefaultJSONString());
		$o->setHtml(null);
		self::assertFalse($o->isModified());
		self::assertNull($o->getHtml());
	}

	public function testDefault()
	{
		$o = $this->getObject(['e' => 'TEXT', 't' => 'tt', 'h' => 'hh']);
		self::assertFalse($o->isModified());
		self::assertEquals('{"e":"TEXT","t":"tt","h":"hh"}', $o->getDefaultJSONString());
		self::assertEquals('tt', $o->getRawText());
		$o->setRawText('modified');
		self::assertTrue($o->isModified());
		self::assertEquals('{"e":"TEXT","t":"tt","h":"hh"}', $o->getDefaultJSONString());
		self::assertEquals('{"e":"TEXT","t":"modified","h":"hh"}', $o->toJSONString());
		$o->setRawText('tt');
		self::assertFalse($o->isModified());

		$o = $this->getObject('{"e":"TEXT","t":"tt","h":"hh"}');
		self::assertFalse($o->isModified());
		self::assertEquals('{"e":"TEXT","t":"tt","h":"hh"}', $o->getDefaultJSONString());
		self::assertEquals('tt', $o->getRawText());
		$o->setRawText('modified');

		$o2 = $this->getObject($o);
		self::assertNotSame($o, $o2);
		self::assertFalse($o2->isModified());
		self::assertEquals('{"e":"TEXT","t":"modified","h":"hh"}', $o2->getDefaultJSONString());
		self::assertEquals('modified', $o2->getRawText());
		$o2->setRawText('original');
		self::assertTrue($o2->isModified());
		self::assertEquals('{"e":"TEXT","t":"modified","h":"hh"}', $o2->getDefaultJSONString());
		$o2->setAsDefault();
		self::assertFalse($o2->isModified());
		self::assertEquals('{"e":"TEXT","t":"original","h":"hh"}', $o2->getDefaultJSONString());
	}

	public function testArray()
	{
		$o = $this->getObject();
		self::assertSame($o, $o->fromArray([]));
		self::assertEquals(['e' => 'Markdown', 't' => null, 'h' => null], $o->toArray());

		$o->fromArray(['e' => 'TEXT', 't' => 'tt', 'h' => 'hh']);
		self::assertEquals('TEXT', $o->getEditor());
		self::assertEquals('tt', $o->getRawText());
		self::assertEquals('hh', $o->getHtml());
		self::assertEquals(['e' => 'TEXT', 't' => 'tt', 'h' => 'hh'], $o->toArray());

		$o->fromArray([]);
		self::assertEquals(['e' => 'Markdown', 't' => null, 'h' => null], $o->toArray());
	}

	public function testJSONString()
	{
		$o = $this->getObject();
		self::assertSame($o, $o->fromJSONString('text'));
		self::assertEquals('{"e":"Markdown","t":"text","h":null}', $o->toJSONString());
		self::assertEquals('text', $o->getRawText());

		$o->fromJSONString('{"e":"TEXT","t":"tt","h":"hh"}');
		self::assertEquals('TEXT', $o->getEditor());
		self::assertEquals('tt', $o->getRawText());
		self::assertEquals('hh', $o->getHtml());
		self::assertEquals('{"e":"TEXT","t":"tt","h":"hh"}', $o->toJSONString());

		$o->fromJSONString(null);
		self::assertNull($o->toJSONString());
	}
}