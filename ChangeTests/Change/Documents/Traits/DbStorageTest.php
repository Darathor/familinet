<?php
namespace ChangeTests\Change\Documents\Traits;

use Change\Documents\AbstractDocument;

/**
 * @name \ChangeTests\Change\Documents\Traits\DbStorageTest
 */
class DbStorageTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		$manager = $this->getApplicationServices()->getDocumentManager();
		$manager->reset();
		return $manager;
	}

	public function testTransaction()
	{
		/* @var $document \Project\Tests\Documents\Basic */
		$manager = $this->getDocumentManager();
		$document = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$document->setPStr('pStr');
		try
		{
			$document->create();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('No transaction started!', $e->getMessage());
		}

		try
		{
			$document->initialize(1, AbstractDocument::STATE_LOADED);
			$document->setPStr('pStr2');
			$document->update();
			self::fail('RuntimeException: expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('No transaction started!', $e->getMessage());
		}
	}

	public function testDocument()
	{
		$manager = $this->getDocumentManager();

		/* @var $document \Project\Tests\Documents\Basic */
		$this->getApplicationServices()->getTransactionManager()->begin();

		$document = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertEquals(AbstractDocument::STATE_NEW, $document->getPersistentState());
		self::assertTrue($document->isNew());
		self::assertFalse($document->isLoaded());
		self::assertFalse($document->isDeleted());

		self::assertLessThan(0, $document->getId());
		$document->setPStr('Required');
		$document->create();
		self::assertEquals(AbstractDocument::STATE_LOADED, $document->getPersistentState());
		self::assertTrue($document->isLoaded());
		self::assertGreaterThan(0, $document->getId());
		try
		{
			$document->create();
			self::fail('RuntimeException Expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('Document is not new', $e->getMessage());
		}

		$definedId = $document->getId() + 10;
		/* @var $document2 \Project\Tests\Documents\Basic */
		$document2 = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertLessThan(0, $document2->getId());
		$document2->initialize($definedId);
		self::assertEquals($definedId, $document2->getId());
		self::assertEquals(AbstractDocument::STATE_NEW, $document2->getPersistentState());
		$document2->setPStr('Required');
		$document2->create();
		self::assertEquals(AbstractDocument::STATE_LOADED, $document2->getPersistentState());

		/* @var $document3 \Project\Tests\Documents\Basic */
		$document3 = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		self::assertLessThan(0, $document3->getId());
		$document3->setPStr('Required');
		$document3->create();
		self::assertEquals($definedId + 1, $document3->getId());
		self::assertEquals(AbstractDocument::STATE_LOADED, $document3->getPersistentState());

		$document2->delete();
		self::assertEquals(AbstractDocument::STATE_DELETED, $document2->getPersistentState());
		self::assertTrue($document2->isDeleted());

		$document->setPStr('Document Label');
		self::assertTrue($document->isPropertyModified('pStr'));

		$document->update();
		self::assertFalse($document->isPropertyModified('pStr'));

		$cachedDoc = $manager->getDocumentInstance($document->getId());
		self::assertTrue($cachedDoc === $document);
		self::assertEquals(AbstractDocument::STATE_LOADED, $cachedDoc->getPersistentState());

		$manager->reset();

		/* @var $newDoc1 \Project\Tests\Documents\Basic */
		$newDoc1 = $manager->getDocumentInstance($document->getId());
		self::assertNotSame($document, $newDoc1);
		self::assertInstanceOf('\Project\Tests\Documents\Basic', $newDoc1);
		self::assertEquals($document->getId(), $newDoc1->getId());
		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $newDoc1->getPersistentState());

		$newDoc1->load();
		self::assertEquals(AbstractDocument::STATE_LOADED, $newDoc1->getPersistentState());
		self::assertEquals('Document Label', $newDoc1->getPStr());

		$metas = array('k1' => 'v1', 'k2' => array(45, 46, 50));
		$newDoc1->setMetas($metas);
		$newDoc1->saveMetas();

		$manager->reset();

		/* @var $newDocMeta \Project\Tests\Documents\Basic */
		$newDocMeta = $manager->getDocumentInstance($newDoc1->getId());
		self::assertNotSame($newDoc1, $newDocMeta);
		self::assertEquals($metas, $newDocMeta->getMetas());

		/* @var $newDoc \Project\Tests\Documents\Basic */
		$newDoc = $manager->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$tmpId = $newDoc->getId();
		self::assertLessThan(0, $tmpId);
		self::assertNull($manager->getDocumentInstance($tmpId));
		self::assertNull($manager->getDocumentInstance(-5000));

		$newDoc->setPStr('required');
		$newDoc->create();

		$finalId = $newDoc->getId();
		self::assertGreaterThan(0, $finalId);

		self::assertSame($newDoc, $manager->getDocumentInstance($finalId));
		$this->getApplicationServices()->getTransactionManager()->commit();
	}

	public function testPropertyDocumentIds()
	{
		$manager = $this->getDocumentManager();

		$this->getApplicationServices()->getTransactionManager()->begin();

		/* @var $sd1 \Project\Tests\Documents\DocProps */
		$sd1 = $manager->getNewDocumentInstanceByModelName('Project_Tests_DocProps');
		$sd1->create();

		/* @var $sd2 \Project\Tests\Documents\DocProps */
		$sd2 = $manager->getNewDocumentInstanceByModelName('Project_Tests_DocProps');
		$sd2->create();

		/* @var $basic \Project\Tests\Documents\DocProps */
		$basic = $manager->getNewDocumentInstanceByModelName('Project_Tests_DocProps');

		$basic->getPDocArr()->add($sd1);
		$basic->getPDocArr()->add($sd2);
		$basic->create();

		self::assertEquals(array($sd1->getId(), $sd2->getId()), $basic->getPDocArrIds());
		$this->getApplicationServices()->getTransactionManager()->commit();

		/* @var $b2 \Project\Tests\Documents\DocProps */
		$b2 = $manager->getDocumentInstance($basic->getId());
		self::assertNotSame($basic, $b2);
		self::assertEquals(array($sd1->getId(), $sd2->getId()), $b2->getPDocArrIds());
	}
}