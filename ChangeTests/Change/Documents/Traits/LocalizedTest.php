<?php
namespace ChangeTests\Change\Documents\Traits;

use Change\Documents\AbstractDocument;

/**
 * @name \ChangeTests\Change\Documents\Traits\LocalizedTest
 */
class LocalizedTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		$manager = $this->getApplicationServices()->getDocumentManager();
		$manager->reset();
		return $manager;
	}

	public function testI18nDocument()
	{

		$this->getApplicationServices()->getTransactionManager()->begin();

		$manager = $this->getDocumentManager();

		/* @var $localized \Project\Tests\Documents\Localized */
		$localized = $manager->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localizedI18nPartFr = $localized->getCurrentLocalization();

		$tmpId = $localized->getId();
		self::assertNotNull($localizedI18nPartFr);
		self::assertEquals($tmpId, $localizedI18nPartFr->getId());
		self::assertEquals('fr_FR', $localizedI18nPartFr->getLCID());
		self::assertEquals(AbstractDocument::STATE_NEW, $localizedI18nPartFr->getPersistentState());

		$localized->setPStr('Required');
		$localizedI18nPartFr->setPLStr('Required');

		$localized->create();
		self::assertEquals(AbstractDocument::STATE_LOADED, $localized->getPersistentState());

		self::assertNotEquals($tmpId, $localized->getId());

		self::assertEquals($localized->getId(), $localizedI18nPartFr->getId());
		self::assertEquals(AbstractDocument::STATE_LOADED, $localizedI18nPartFr->getPersistentState());

		$localizedI18nPartFr->setPLStr('Localized Label');
		self::assertTrue($localizedI18nPartFr->isPropertyModified('pLStr'));
		self::assertTrue($localized->isPropertyModified('pLStr'));

		$localized->save();
		self::assertFalse($localizedI18nPartFr->isPropertyModified('pLStr'));
		self::assertFalse($localized->isPropertyModified('pLStr'));

		$localized->reset();

		$loaded = $localized->getCurrentLocalization();
		self::assertNotSame($loaded, $localizedI18nPartFr);

		self::assertEquals($localized->getId(), $loaded->getId());
		self::assertEquals('fr_FR', $loaded->getLCID());
		self::assertEquals('Localized Label', $loaded->getPLStr());
		self::assertEquals(AbstractDocument::STATE_LOADED, $loaded->getPersistentState());

		$localized->delete();
		self::assertEquals(AbstractDocument::STATE_DELETED, $loaded->getPersistentState());

		$deleted = $localized->getCurrentLocalization();
		self::assertSame($loaded, $deleted);
		$this->getApplicationServices()->getTransactionManager()->commit();
	}

	public function testUpdate()
	{
		$this->getApplicationServices()->getTransactionManager()->begin();

		$manager = $this->getDocumentManager();
		/* @var $localized \Project\Tests\Documents\Localized */
		$localized = $manager->getNewDocumentInstanceByModelName('Project_Tests_Localized');
		$localized->setPStr('Required');
		$localized->getCurrentLocalization()->setPLStr('Required');
		$localized->create();

		$manager->reset();

		/* @var $localized2 \Project\Tests\Documents\Localized */
		$localized2 = $manager->getDocumentInstance($localized->getId());
		self::assertNotSame($localized, $localized2);
		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $localized2->getPersistentState());
		self::assertEquals($localized->getId(), $localized2->getId());
		$localized2->getCurrentLocalization()->setPLStr('Required 2');

		self::assertEquals(AbstractDocument::STATE_INITIALIZED, $localized2->getPersistentState());
		self::assertTrue($localized2->hasModifiedProperties());
		self::assertEquals(array('pLStr'), $localized2->getModifiedPropertyNames());
		$localized2->update();

		self::assertFalse($localized2->hasModifiedProperties());
		self::assertCount(0, $localized2->getModifiedPropertyNames());

		$this->getApplicationServices()->getTransactionManager()->commit();
	}
}