<?php
namespace ChangeTests\Change\Documents;

use Change\Documents\TreeNode;

/**
 * @name \ChangeTests\Change\Documents\TreeManagerTest
 */
class TreeManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	/**
	 * @param string $label
	 * @return \Project\Tests\Documents\Basic
	 */
	protected function getNewBasicDoc($label = 'node')
	{
		/* @var $doc \Project\Tests\Documents\Basic */
		$doc = $this->getApplicationServices()->getDocumentManager()->getNewDocumentInstanceByModelName('Project_Tests_Basic');
		$doc->setPStr($label);
		$doc->save();
		return $doc;
	}

	/**
	 * @param integer $id
	 * @param string $label
	 * @return bool
	 */
	protected function checkBasicDocLabel($id, $label)
	{
		/* @var $doc \Project\Tests\Documents\Basic */
		$doc = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($id);
		if ($doc instanceof \Project\Tests\Documents\Basic)
		{
			return $doc->getPStr() === $label;
		}
		return false;
	}

	/**
	 * @return \Change\Documents\TreeManager
	 */
	protected function getTreeManager()
	{
		return $this->getApplicationServices()->getTreeManager();
	}

	public function testType()
	{
		$treeManager = $this->getTreeManager();
		self::assertInstanceOf(\Change\Documents\TreeManager::class, $treeManager);
	}

	public function testTreeNames()
	{
		$treeManager = $this->getTreeManager();
		$treeNames = $treeManager->getTreeNames();
		self::assertContains('Project_Tests', $treeNames);
		self::assertTrue($treeManager->hasTreeName('Project_Tests'));
		self::assertFalse($treeManager->hasTreeName('Project_NotFound'));
	}

	public function testCreate()
	{
		$treeManager = $this->getTreeManager();

		/* @var $doc \Project\Tests\Documents\Basic */
		$doc = $this->getNewBasicDoc('Root Node');
		self::assertNull($doc->getTreeName());
		$rootId = $doc->getId();

		$rootNode = $treeManager->insertRootNode($doc, 'Project_Tests');
		self::assertEquals('Project_Tests', $doc->getTreeName());

		self::assertInstanceOf(TreeNode::class, $rootNode);
		self::assertEquals('Project_Tests', $rootNode->getTreeName());
		self::assertEquals('/', $rootNode->getPath());
		self::assertTrue($rootNode->isRoot());
		self::assertEquals(0, $rootNode->getParentId());
		self::assertEquals(0, $rootNode->getPosition());
		self::assertEquals(0, $rootNode->getChildrenCount());
		self::assertEquals($rootId, $rootNode->getDocumentId());

		$tmpNode = $treeManager->insertRootNode($doc, 'Project_Tests');
		self::assertTrue($rootNode->eq($tmpNode));

		$doc1 = $this->getNewBasicDoc('Node 1');
		try
		{
			$treeManager->insertRootNode($doc1, 'Project_Tests');
			self::fail('RuntimeException expected');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertEquals('Root node (' . $rootNode . ' ) already exist.', $e->getMessage());
		}

		$tn1 = $treeManager->insertNode($rootNode, $doc1);
		self::assertEquals('Project_Tests', $doc1->getTreeName());

		self::assertInstanceOf(TreeNode::class, $tn1);
		self::assertEquals('Project_Tests', $tn1->getTreeName());
		self::assertEquals($rootNode->getFullPath(), $tn1->getPath());

		self::assertFalse($tn1->isRoot());
		self::assertEquals($rootId, $tn1->getParentId());
		self::assertEquals(0, $tn1->getPosition());
		self::assertEquals(0, $tn1->getChildrenCount());
		self::assertEquals($doc1->getId(), $tn1->getDocumentId());

		$doc2 = $this->getNewBasicDoc('Node 2');

		$tn2 = $treeManager->insertNode($rootNode, $doc2, $tn1);
		self::assertEquals('Project_Tests', $doc2->getTreeName());

		self::assertInstanceOf(TreeNode::class, $tn2);
		self::assertEquals('Project_Tests', $tn2->getTreeName());
		self::assertEquals($rootNode->getFullPath(), $tn2->getPath());
		self::assertEquals($rootId, $tn2->getParentId());
		self::assertEquals(0, $tn2->getPosition());
		self::assertEquals(0, $tn2->getChildrenCount());
		self::assertEquals($doc2->getId(), $tn2->getDocumentId());
	}

	/**
	 * @depends testCreate
	 */
	public function testGetNode()
	{
		$treeManager = $this->getTreeManager();

		$rootNode = $treeManager->getRootNode('Project_Tests');

		self::assertInstanceOf(TreeNode::class, $rootNode);

		self::assertTrue($rootNode->hasChildren());
		self::assertEquals(2, $rootNode->getChildrenCount());

		$children = $treeManager->getChildrenNode($rootNode);

		self::assertCount(2, $children);

		$tn1 = $children[0];
		self::assertInstanceOf(TreeNode::class, $tn1);
		self::assertEquals(0, $tn1->getPosition());

		self::assertTrue($this->checkBasicDocLabel($tn1->getDocumentId(), 'Node 2'));

		$tn2 = $children[1];
		self::assertInstanceOf(TreeNode::class, $tn2);
		self::assertEquals(1, $tn2->getPosition());

		self::assertTrue($this->checkBasicDocLabel($tn2->getDocumentId(), 'Node 1'));
	}

	/**
	 * @depends testGetNode
	 */
	public function testDeleteNode()
	{
		$treeManager = $this->getTreeManager();
		$rootNode = $treeManager->getRootNode('Project_Tests');
		self::assertEquals(2, $rootNode->getChildrenCount());

		$children = $treeManager->getChildrenNode($rootNode);

		$doc2 = $children[0]->setTreeManager($treeManager)->getDocument();
		$doc2->load();

		$doc1 = $children[1]->setTreeManager($treeManager)->getDocument();

		$treeManager->deleteDocumentNode($doc2);
		self::assertNull($doc2->getTreeName());

		self::assertEquals(1, $treeManager->getRootNode('Project_Tests')->getChildrenCount());

		$treeManager->deleteChildrenNodes($rootNode);
		self::assertNull($doc1->getTreeName());

		$rootDoc = $rootNode->setTreeManager($treeManager)->getDocument();
		$treeManager->deleteNode($rootNode);

		self::assertNull($rootDoc->getTreeName());

		self::assertNull($treeManager->getRootNode('Project_Tests'));
	}

	public function testDescendant()
	{
		$treeManager = $this->getTreeManager();
		$rd = $this->getNewBasicDoc('root');
		$rootNode = $treeManager->insertRootNode($rd, 'Project_Tests');

		$n1 = $treeManager->insertNode($rootNode, $this->getNewBasicDoc('lvl 1,0'));
		$n2 = $treeManager->insertNode($rootNode, $this->getNewBasicDoc('lvl 1,1'));

		$n11 = $treeManager->insertNode($n1, $this->getNewBasicDoc('lvl 2,0'));
		$n12 = $treeManager->insertNode($n1, $this->getNewBasicDoc('lvl 2,1'));

		$n21 = $treeManager->insertNode($n2, $this->getNewBasicDoc('lvl 2,3'));
		$n22 = $treeManager->insertNode($n2, $this->getNewBasicDoc('lvl 2,4'));
		$n23 = $treeManager->insertNode($n2, $this->getNewBasicDoc('lvl 2,5'));

		$n31 = $treeManager->insertNode($n22, $this->getNewBasicDoc('lvl 3,0'));

		$nodes = $treeManager->getDescendantNodes($rootNode, 1);
		self::assertCount(2, $nodes);
		self::assertFalse($nodes[0]->loadedChildren());
		self::assertEquals(2, $nodes[0]->getChildrenCount());

		self::assertFalse($nodes[1]->loadedChildren());
		self::assertEquals(3, $nodes[1]->getChildrenCount());

		$treeManager->moveNode($n1, $n31);
		self::assertTrue($treeManager->refreshNode($n1));
		self::assertEquals(2, $n1->getChildrenCount());
		self::assertEquals(4, $n1->getLevel());
		self::assertEquals($n31->getDocumentId(), $n1->getParentId());
		self::assertEquals($n31->getFullPath(), $n1->getPath());

		$treeManager->moveNode($n1, $n2, $n22);
		self::assertTrue($treeManager->refreshNode($n1));
		self::assertEquals(2, $n1->getLevel());
		self::assertEquals($n2->getFullPath(), $n1->getPath());
		self::assertEquals(1, $n1->getPosition());
		self::assertTrue($treeManager->refreshNode($n22));
		self::assertEquals(2, $n22->getPosition());

		$treeManager->moveNode($n1, $n2);
		$treeManager->refreshNode($n1);
		self::assertEquals(3, $n1->getPosition());

		$treeManager->moveNode($n1, $n2, $n21);
		$treeManager->refreshNode($n1);
		self::assertEquals(0, $n1->getPosition());

		$treeManager->moveNode($n1, $n2, $n23);
		$treeManager->refreshNode($n1);
		self::assertEquals(2, $n1->getPosition());
	}

	protected function dumpNode(TreeNode $node, $indent = 0)
	{
		if ($indent === 0)
		{
			echo PHP_EOL, PHP_EOL;
		}
		echo str_repeat("\t", $indent), $node, PHP_EOL;
		foreach ($node->getChildren() as $subNode)
		{
			$this->dumpNode($subNode, $indent + 1);
		}
	}
}
