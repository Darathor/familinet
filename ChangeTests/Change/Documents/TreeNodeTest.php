<?php
namespace ChangeTests\Change\Documents;

/**
 * @name \ChangeTests\Change\Documents\TreeNodeTest
 */
class TreeNodeTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	public function testConstructor()
	{
		$node = new \Change\Documents\TreeNode('test');
		self::assertEquals('test', $node->getTreeName());
		self::assertNull($node->getParentId());
		self::assertNull($node->getDocumentId());
		self::assertNull($node->getLevel());
		self::assertNull($node->getPath());
		self::assertNull($node->getPosition());
		self::assertNull($node->getChildrenCount());

		$node = new \Change\Documents\TreeNode('test', 1);
		self::assertEquals(1, $node->getDocumentId());
	}

	public function testProperties()
	{
		$node = new \Change\Documents\TreeNode('test');
		self::assertSame($node, $node->setTreeName('Project_Tests'));
		self::assertEquals('Project_Tests', $node->getTreeName());

		self::assertSame($node, $node->setDocumentId(1));
		self::assertEquals(1, $node->getDocumentId());

		self::assertSame($node, $node->setLevel(2));
		self::assertEquals(2, $node->getLevel());

		self::assertSame($node, $node->setParentId(3));
		self::assertEquals(3, $node->getParentId());

		self::assertSame($node, $node->setPosition(4));
		self::assertEquals(4, $node->getPosition());

		self::assertSame($node, $node->setPath('/'));
		self::assertEquals('/', $node->getPath());

		self::assertSame($node, $node->setChildrenCount(5));
		self::assertEquals(5, $node->getChildrenCount());
	}

	public function testTreeManager()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests');
		self::assertSame($node, $node->setTreeManager(null));
		try
		{
			$node->getTreeManager();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('TreeManager not set.', $e->getMessage());
		}
		$tm = $this->getApplicationServices()->getTreeManager();
		$node->setTreeManager($tm);
		self::assertSame($tm, $node->getTreeManager());
	}

	public function testIsRoot()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests', 10);
		self::assertTrue($node->isRoot());
		self::assertTrue($node->setParentId(0)->isRoot());
		self::assertFalse($node->setParentId(5)->isRoot());
	}

	public function testHasChildren()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests', 10);
		self::assertFalse($node->hasChildren());
		self::assertTrue($node->setChildrenCount(5)->hasChildren());
		self::assertFalse($node->setChildrenCount(0)->hasChildren());
	}

	public function testEq()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests', 10);
		$node2 = new \Change\Documents\TreeNode('Project_Tests', 10);
		self::assertTrue($node->eq($node));
		self::assertTrue($node->eq($node2));
		self::assertTrue($node->eq(10));

		$node3 = new \Change\Documents\TreeNode('Project_Tests', 11);
		self::assertFalse($node->eq($node3));
		self::assertFalse($node->eq(11));
		self::assertFalse($node->eq('a'));
	}

	public function testPath()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests', 10);
		$node->setPath('/');
		self::assertEquals('/10/', $node->getFullPath());
		self::assertEquals([], $node->getAncestorIds());

		$node->setPath('/1/2/3/');
		self::assertCount(3, $node->getAncestorIds());
		self::assertEquals([1, 2, 3], $node->getAncestorIds());

		$node2 = new \Change\Documents\TreeNode('Project_Tests', 1);
		self::assertTrue($node2->ancestorOf($node));
		self::assertFalse($node2->setDocumentId(10)->ancestorOf($node));
	}

	public function testChildren()
	{
		$n2 = new \Change\Documents\TreeNode('Project_Tests', 2);
		$node = new \Change\Documents\TreeNode('Project_Tests', 10);
		self::assertSame($node, $node->setChildren([$n2]));
		self::assertEquals([$n2], $node->getChildren());
		$node->setChildren(null);
		try
		{
			$node->getChildren();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('TreeManager not set.', $e->getMessage());
		}
	}

	public function testDocumentProperty()
	{
		$node = new \Change\Documents\TreeNode('Project_Tests');
		$node->setTreeManager($this->getApplicationServices()->getTreeManager());

		$mi = new \ChangeTests\Change\Documents\TestAssets\MemoryInstance();
		$doc = $mi->getInstanceRo5001($this->getApplicationServices()->getDocumentManager());
		$node->setDocumentId(5001);
		$doc2 = $node->getDocument();
		self::assertSame($doc, $doc2);

		$node->setTreeManager(null);
		try
		{
			$node->getDocument();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('TreeManager not set.', $e->getMessage());
		}
	}
}
