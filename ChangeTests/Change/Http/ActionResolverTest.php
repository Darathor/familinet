<?php
namespace ChangeTests\Change\Http;

use Change\Http\BaseResolver;

class ActionResolverTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testResolve()
	{
		$event = new \Change\Http\Event();
		$event->setAction('test');

		$resolver = new BaseResolver();

		$resolver->resolve($event);

		self::assertNull($event->getAction());
	}
}
