<?php
namespace ChangeTests\Change\Http;

use Change\Http\Controller;
use ChangeTests\Change\Http\TestAssets\FakeEventHandler;

class ControllerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$application = $this->getApplication();
		$controller = new Controller($application);
		self::assertInstanceOf(\Change\Events\EventManager::class, $controller->getEventManager());
		self::assertSame(['Http'], $controller->getEventManager()->getIdentifiers());
	}

	public function testActionResolver()
	{
		$application = $this->getApplication();
		$controller = new Controller($application);
		$ac = new \Change\Http\BaseResolver();
		$controller->setActionResolver($ac);
		self::assertSame($ac, $controller->getActionResolver());
	}

	public function testCreateResponse()
	{
		$application = $this->getApplication();
		$controller = new Controller($application);
		$response = $controller->createResponse();
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
	}

	public function testHandle()
	{
		$application = $this->getApplication();
		$controller = new Controller($application);

		$eventManager = $controller->getEventManager();
		$fakeEventHandler = new FakeEventHandler();

		$eventManager->attach(\Change\Http\Event::EVENT_REQUEST,
			static function ($event) use ($fakeEventHandler) { $fakeEventHandler->onRequest($event); }, 5);
		$eventManager->attach(\Change\Http\Event::EVENT_ACTION, [$fakeEventHandler, 'onAction'], 5);
		$eventManager->attach(\Change\Http\Event::EVENT_RESULT, [$fakeEventHandler, 'onResult'], 5);
		$eventManager->attach(\Change\Http\Event::EVENT_RESPONSE, [$fakeEventHandler, 'onResponse'], 5);
		$eventManager->attach(\Change\Http\Event::EVENT_EXCEPTION, [$fakeEventHandler, 'onException'], 5);

		$request = new \Change\Http\Request();
		$response = $controller->handle($request);

		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(5, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onAction', 'execute', 'onResult', 'onResponse'], $fakeEventHandler->callNames);

		$fakeEventHandler->setThrowOn('onRequest');
		$response = $controller->handle($request);
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(3, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onException(10000)', 'onResponse'], $fakeEventHandler->callNames);

		$fakeEventHandler->setThrowOn('onAction');
		$response = $controller->handle($request);
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(4, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onAction', 'onException(10001)', 'onResponse'], $fakeEventHandler->callNames);

		$fakeEventHandler->setThrowOn('execute');
		$response = $controller->handle($request);
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(5, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onAction', 'execute', 'onException(10005)', 'onResponse'], $fakeEventHandler->callNames);

		$fakeEventHandler->setThrowOn('onResult');
		$response = $controller->handle($request);
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(6, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onAction', 'execute', 'onResult', 'onException(10002)', 'onResponse'], $fakeEventHandler->callNames);

		$fakeEventHandler->setThrowOn('onResponse');
		$response = $controller->handle($request);
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class, $response);
		self::assertEquals(500, $response->getStatusCode());
		self::assertCount(6, $fakeEventHandler->callNames);
		self::assertEquals(['onRequest', 'onAction', 'execute', 'onResult', 'onResponse', 'onException(10003)'],
			$fakeEventHandler->callNames);
	}
}