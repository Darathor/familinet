<?php
namespace ChangeTests\Change\Http;

class EventTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Http\Event
	 */
	public function testConstruct()
	{
		$event = new \Change\Http\Event();
		self::assertInstanceOf(\Change\Http\Event::class, $event);
		return $event;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testController($event)
	{
		$application = $this->getApplication();
		$controller = new \Change\Http\Controller($application);
		$event->setTarget($controller);
		self::assertSame($controller, $event->getController());
		return $event;
	}

	/**
	 * @depends testController
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testApplication($event)
	{
		try
		{
			$event->getApplication();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			// OK.
		}

		$event->setParam('application', $this->getApplication());
		self::assertSame($this->getApplication(), $event->getApplication());
		return $event;
	}

	/**
	 * @depends testApplication
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testApplicationServices($event)
	{
try
		{
			$event->getApplicationServices();
			self::fail('RuntimeException expected');
		}
		catch (\RuntimeException $e)
		{
			// OK.
		}

		$event->setParams($this->getDefaultEventArguments());
		self::assertSame($this->getApplicationServices(), $event->getApplicationServices());
		return $event;

	}

	/**
	 * @depends testApplicationServices
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testRequest($event)
	{
		self::assertNull($event->getRequest());
		$request = new \Change\Http\Request();
		$event->setRequest($request);
		self::assertSame($request, $event->getRequest());
		return $event;
	}

	/**
	 * @depends testRequest
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testUrlManager($event)
	{
		self::assertNull($event->getUrlManager());

		$uri = new \Zend\Uri\Http();
		$uri->parse('http://domain.net');
		$urlManager = new \Change\Http\UrlManager($uri);

		$event->setUrlManager($urlManager);
		self::assertSame($urlManager, $event->getUrlManager());
		return $event;
	}

	/**
	 * @depends testUrlManager
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testAction($event)
	{
		self::assertNull($event->getAction());

		$action = [$this, 'testAction'];

		$event->setAction($action);
		self::assertSame($action, $event->getAction());

		return $event;
	}

	/**
	 * @depends testAction
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testResult($event)
	{
		self::assertNull($event->getResult());

		$result = new \Change\Http\Result();
		$event->setResult($result);
		self::assertSame($result, $event->getResult());

		return $event;
	}

	/**
	 * @depends testResult
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testResponse($event)
	{
		self::assertNull($event->getResponse());

		$response = new \Zend\Http\PhpEnvironment\Response();
		$event->setResponse($response);
		self::assertSame($response, $event->getResponse());

		return $event;
	}
}