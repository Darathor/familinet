<?php
namespace ChangeTests\Change\Http;

use Change\Http\Request;

class RequestTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Http\Request
	 */
	public function testConstruct()
	{
		//$_SERVER['REQUEST_URI'] = '/';
		$request = new Request();
		self::assertInstanceOf('\Change\Http\Request', $request);
		self::assertNull($request->getPath());
		self::assertNull($request->getIfModifiedSince());
		self::assertNull($request->getIfNoneMatch());

		$_SERVER['REQUEST_URI'] = '/';
		$_SERVER['HTTP_IF_MODIFIED_SINCE'] = 'Sat, 29 Oct 1994 19:43:31 GMT';
		$_SERVER['HTTP_IF_NONE_MATCH'] = 'd00806e9f37d1764a1948ea1edf';

		$request = new Request();
		self::assertEquals('/', $request->getPath());
		self::assertInstanceOf('\DateTime', $request->getIfModifiedSince());
		$date = new \DateTime('Sat, 29 Oct 1994 19:43:31 GMT');

		self::assertEquals($date->format(\DateTime::ATOM), $request->getIfModifiedSince()->format(\DateTime::ATOM));
		self::assertEquals('d00806e9f37d1764a1948ea1edf', $request->getIfNoneMatch());

		return $request;
	}

	/**
	 * @depends testConstruct
	 * @param Request $request
	 * @return Request
	 */
	public function testPath($request)
	{
		$request->setPath(null);
		self::assertNull($request->getPath());
		return $request;
	}

	/**
	 * @depends testPath
	 * @param Request $request
	 * @return Request
	 */
	public function testIfModifiedSince($request)
	{
		$request->setIfModifiedSince(null);
		self::assertNull($request->getIfModifiedSince());
		return $request;
	}

	/**
	 * @depends testIfModifiedSince
	 * @param Request $request
	 * @return Request
	 */
	public function testIfNoneMatch($request)
	{
		$request->setIfNoneMatch(null);
		self::assertNull($request->getIfNoneMatch());
		return $request;
	}

	/**
	 * @depends testIfNoneMatch
	 * @param Request $request
	 * @return Request
	 */
	public function testLCID($request)
	{
		self::assertNull($request->getLCID());
		$request->setLCID('fr_FR');
		self::assertEquals('fr_FR', $request->getLCID());
		return $request;
	}
}