<?php
namespace ChangeTests\Change\Http\Rest\V1;

class ControllerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Http\Rest\V1\Controller
	 */
	public function getController()
	{
		return new \Change\Http\Rest\V1\Controller($this->getApplication());
	}

	public function testCreateResponse()
	{
	$controller = $this->getController();
		$response = $controller->createResponse();
		self::assertInstanceOf(\Zend\Http\PhpEnvironment\Response::class,$response);
		$ct = $response->getHeaders()->get('Content-Type');
		self::assertInstanceOf(\Zend\Http\Header\HeaderInterface::class, $ct);
		self::assertEquals('application/json', $ct->getFieldValue());
	}

	/**
	 * @return \Change\Http\Event
	 */
	public function testOnException()
	{
	$controller = $this->getController();
		$event = new \Change\Http\Event(\Change\Http\Event::EVENT_EXCEPTION, $controller);
		$event->setRequest(new \Change\Http\Request());
		$exception = new \RuntimeException('test message', 10);
		$exception->httpStatusCode = 501;
		$event->setParam('Exception', $exception);

		$controller->onException($event);

		$result = $event->getResult();
		self::assertInstanceOf(\Change\Http\Rest\V1\ErrorResult::class, $result);

		/* @var $result \Change\Http\Rest\V1\ErrorResult */
		self::assertEquals(501, $result->getHttpStatusCode());
		self::assertEquals('EXCEPTION-10', $result->getErrorCode());
		self::assertEquals('test message', $result->getErrorMessage());

		self::assertNull($event->getResponse());

		return $event;
	}

	/**
	 * @depends testOnException
	 * @param \Change\Http\Event $event
	 */
	public function testOnDefaultJsonResponse($event)
	{
		$controller = $event->getController();
		$event->getResult()->setHeaderEtag('testEtag');
		/* @var $controller \Change\Http\Rest\V1\Controller */
		$controller->onDefaultJsonResponse($event);
		$response = $event->getResponse();

		self::assertEquals(501, $response->getStatusCode());
		$ct = $response->getHeaders()->get('Etag');
		self::assertInstanceOf(\Zend\Http\Header\HeaderInterface::class, $ct);
		self::assertEquals('testEtag', $ct->getFieldValue());

		$content = $response->getContent();
		self::assertEquals('{"code":"EXCEPTION-10","message":"test message"}', $content);

		$event->setResult(new \Change\Http\Result());
		$event->getResult()->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		$event->getRequest()->setPath('/testPath');

		$controller->onDefaultJsonResponse($event);
		$response = $event->getResponse();
		self::assertEquals(\Zend\Http\Response::STATUS_CODE_404, $response->getStatusCode());
		$content = $response->getContent();
		self::assertEquals('{"code":"PATH-NOT-FOUND","message":"Unable to resolve path","data":{"path":"\/testPath"}}', $content);
	}
}