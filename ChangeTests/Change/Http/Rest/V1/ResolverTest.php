<?php
namespace ChangeTests\Change\Http\Rest\V1;

use Change\Http\Rest\V1\Resolver;
use Zend\Http\Request;

class ResolverTest extends \ChangeTests\Change\TestAssets\TestCase
{

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsClasses();
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string|null $path
	 * @param string $method
	 * @return void
	 */
	protected function resetEvent(\Change\Http\Event $event, $path = null, $method = Request::METHOD_GET)
	{
		$event->setAction(null);
		$event->setResult(null);
		$event->setParams($this->getDefaultEventArguments());
		$event->getRequest()->setMethod($method);
		$event->getRequest()->setPath($path);
	}

	public function testConstruct()
	{
		$resolver = new Resolver();
		$actionsResolver = $resolver->getResolverByName('actions');
		self::assertInstanceOf('\Change\Http\Rest\V1\Actions\ActionsResolver', $actionsResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $actionsResolver);
		self::assertSame($actionsResolver, $resolver->getActionsResolver());

		$namedResolver = $resolver->getResolverByName('resources');
		self::assertInstanceOf('\Change\Http\Rest\V1\Resources\ResourcesResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('blocks');
		self::assertInstanceOf('\Change\Http\Rest\V1\Blocks\BlocksResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('models');
		self::assertInstanceOf('\Change\Http\Rest\V1\Models\ModelsResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('query');
		self::assertInstanceOf('\Change\Http\Rest\V1\Query\QueryResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('storage');
		self::assertInstanceOf('\Change\Http\Rest\V1\Storage\StorageResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('jobs');
		self::assertInstanceOf('\Change\Http\Rest\V1\Jobs\JobsResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$namedResolver = $resolver->getResolverByName('commands');
		self::assertInstanceOf('\Change\Http\Rest\V1\Commands\CommandsResolver', $namedResolver);
		self::assertInstanceOf('\Change\Http\Rest\V1\NameSpaceDiscoverInterface', $namedResolver);

		$application = $this->getApplication();

		$event = new \Change\Http\Event();
		$event->setRequest(new \ChangeTests\Change\Http\Rest\TestAssets\Request());
		$event->getRequest()->setMethod(Request::METHOD_GET);

		$event->setUrlManager(new \ChangeTests\Change\Http\Rest\TestAssets\UrlManager());
		$event->setTarget(new \Change\Http\Rest\V1\Controller($application));
		$event->setParams($this->getDefaultEventArguments());
		return $event;
	}

	/**
	 * @depends testConstruct
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testRootResolver($event)
	{
		$resolver = new Resolver();

		$this->resetEvent($event, '');
		$resolver->resolve($event);
		self::assertTrue(is_callable($event->getAction()));
		self::assertEquals('', $event->getParam('namespace', ''));
		self::assertTrue($event->getParam('isDirectory'));

		$this->resetEvent($event, '/');
		$resolver->resolve($event);
		self::assertEquals('', $event->getParam('namespace', 'fail'));
		self::assertTrue($event->getParam('isDirectory'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/', 'POST');
		$resolver->resolve($event);
		self::assertFalse(is_callable($event->getAction()));
		self::assertInstanceOf('\Change\Http\Result', $event->getResult());
		self::assertEquals(405, $event->getResult()->getHttpStatusCode());
		return $event;
	}

	/**
	 * @depends testRootResolver
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testDiscover($event)
	{
		$resolver = new Resolver();
		$this->resetEvent($event, '/resources/');
		$resolver->resolve($event);
		self::assertTrue(is_callable($event->getAction()));
		self::assertEquals('resources', $event->getParam('namespace', 'fail'));
		self::assertTrue($event->getParam('isDirectory'));


		$this->resetEvent($event, '/test/');
		$resolver->resolve($event);
		self::assertNull($event->getAction());
		self::assertNull($event->getParam('namespace'));
		self::assertTrue($event->getParam('isDirectory'));

		$this->resetEvent($event, '/test/', 'POST');
		$resolver->resolve($event);

		self::assertNull($event->getAction());
		self::assertNull($event->getResult());
		return $event;
	}

	/**
	 * @depends testDiscover
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testCollection($event)
	{
		$resolver = new Resolver();
		$this->resetEvent($event, '/resources/Project/Tests/NotFound/');
		$resolver->resolve($event);
		self::assertEquals('fail', $event->getParam('modelName', 'fail'));

		$this->resetEvent($event, '/resources/Project/Tests/Basic/');
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));


		$this->resetEvent($event, '/resources/Project/Tests/Basic/', Request::METHOD_PUT);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));

		self::assertInstanceOf('\Change\Http\Result', $event->getResult());
		self::assertEquals(405, $event->getResult()->getHttpStatusCode());

		$this->resetEvent($event, '/resources/Project/Tests/Basic/', Request::METHOD_DELETE);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));
		self::assertInstanceOf('\Change\Http\Result', $event->getResult());
		self::assertEquals(405, $event->getResult()->getHttpStatusCode());


		return $event;
	}

	/**
	 * @depends testCollection
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testDocument($event)
	{
		$resolver = new Resolver();

		$this->resetEvent($event, '/resources/Project/Tests/Basic/', Request::METHOD_POST);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Basic/-3', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertNull($event->getAction());

		$this->resetEvent($event, '/resources/Project/Tests/Basic/a10', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('fail', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Basic/3', Request::METHOD_POST);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertEquals($event->getParam('documentId'), 3);
		self::assertTrue(is_callable($event->getAction()));


		$mi = new \ChangeTests\Change\Documents\TestAssets\MemoryInstance();
		$document = $mi->getInstanceRo5001($this->getApplicationServices()->getDocumentManager());

		$this->resetEvent($event, '/resources/Project/Tests/Basic/' . $document->getId(), Request::METHOD_GET);

		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId(), Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Basic/' . $document->getId() . '/fr_FR', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('fail', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));


		$this->resetEvent($event, '/resources/Project/Tests/Basic/' . $document->getId(), Request::METHOD_PUT);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Basic/' . $document->getId(), Request::METHOD_DELETE);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Basic', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		return $event;
	}

	/**
	 * @depends testDocument
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testLocalizedDocument($event)
	{
		$resolver = new \Change\Http\Rest\V1\Resolver();

		$this->resetEvent($event, '/resources/Project/Tests/Localized/', Request::METHOD_POST);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));


		$this->resetEvent($event, '/resources/Project/Tests/Localized/-3', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertNull($event->getAction());

		$this->resetEvent($event, '/resources/Project/Tests/Localized/a10', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('fail', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));

		$mi = new \ChangeTests\Change\Documents\TestAssets\MemoryInstance();
		$document = $mi->getInstanceRo5002($this->getApplicationServices()->getDocumentManager());

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId(), Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId() . '/fr_FR', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId() . '/zz_ZZ', Request::METHOD_GET);
		$resolver->resolve($event);
		self::assertEquals('fail', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId(), Request::METHOD_POST);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId() . '/fr_FR', Request::METHOD_POST);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));


		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId(), Request::METHOD_PUT);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId() . '/fr_FR', Request::METHOD_PUT);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));


		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId(), Request::METHOD_DELETE);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Localized/' . $document->getId() . '/fr_FR', Request::METHOD_DELETE);
		$resolver->resolve($event);
		self::assertEquals('Project_Tests_Localized', $event->getParam('modelName', 'fail'));
		self::assertEquals($document->getId(), $event->getParam('documentId'));
		self::assertTrue(is_callable($event->getAction()));
		return $event;
	}

	/**
	 * @depends testLocalizedDocument
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Event
	 */
	public function testResActions($event)
	{
		$resolver = new Resolver();

		$this->resetEvent($event, '/resources/Project/Tests/Basic/1000/notfound');
		$resolver->resolve($event);
		self::assertNull($event->getAction());

		$document = $this->getNewReadonlyDocument('Project_Tests_Basic', 5001);

		$this->resetEvent($event, '/resources/Project/Tests/Basic/' . $document->getId() . '/correction');
		$resolver->resolve($event);

		self::assertEquals('fail', $event->getParam('modelName', 'fail'));
		self::assertNull($event->getParam('documentId'));
		self::assertFalse(is_callable($event->getAction()));


		$document = $this->getNewReadonlyDocument('Project_Tests_Correction', 5002);
		$this->resetEvent($event, '/resources/Project/Tests/Correction/' . $document->getId() . '/correction');
		$resolver->resolve($event);
		self::assertFalse(is_callable($event->getAction()));

		$this->resetEvent($event, '/resources/Project/Tests/Correction/' . $document->getId(). '/fr_FR/correction');
		$resolver->resolve($event);
		self::assertTrue(is_callable($event->getAction()));

		return $event;
	}

}