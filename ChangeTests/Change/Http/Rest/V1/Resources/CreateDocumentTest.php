<?php
namespace ChangeTests\Change\Http\Rest\V1\Resources;

use Change\Http\Rest\V1\Resources\CreateDocument;

/**
* @name \ChangeTests\Change\Http\Rest\V1\Resources\CreateDocumentTest
*/
class CreateDocumentTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}


	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function getHttpEvent()
	{
		$event = new \Change\Http\Event();
		$event->setParams($this->getDefaultEventArguments());
		$uri = new \Zend\Uri\Http('http://localhost/rest.php');
		$event->setRequest(new \Change\Http\Request());
		$event->setUrlManager(new \Change\Http\UrlManager($uri, '/rest.php'));

		return $event;
	}

	public function testExecute()
	{
		//documentId, modelName
		$action = new CreateDocument();

		$event = $this->getHttpEvent();
		$event->setParam('modelName', 'Project_Tests_Basic');
		$event->getRequest()->getPost()->set('pStr', 'test');
		$action->execute($event);
		$id = $event->getParam('documentId', -1);
		self::assertGreaterThan(0,	$id);

		/* @var $result \Change\Http\Rest\V1\Resources\DocumentResult */
		$result = $event->getResult();
		self::assertInstanceOf('Change\Http\Rest\V1\Resources\DocumentResult', $result);

		self::assertEquals(201,	$result->getHttpStatusCode());
		$array = $result->toArray();


		self::assertArrayHasKey('properties', $array);
		self::assertArrayHasKey('id', $array['properties']);
		self::assertEquals($id, $array['properties']['id']);

		self::assertArrayHasKey('links', $array);
		$self = $array['links'][0];
		$model = $array['links'][1];
		self::assertEquals('http://localhost/rest.php/models/Project/Tests/Basic', $model['href']);
		self::assertEquals('http://localhost/rest.php/resources/Project/Tests/Basic/' . $id, $self['href']);
	}
}