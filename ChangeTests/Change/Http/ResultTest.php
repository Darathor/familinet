<?php
namespace ChangeTests\Change\Http;

use Change\Http\Result;

class ResultTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\Http\Result
	 */
	public function testConstruct()
	{
		$result = new Result();
		self::assertInstanceOf('\Change\Http\Result', $result);
		self::assertEquals(200, $result->getHttpStatusCode());

		return $result;
	}

	/**
	 * @depends testConstruct
	 * @param Result $result
	 * @return Result
	 */
	public function testHttpStatusCode($result)
	{
		$result->setHttpStatusCode(201);
		self::assertEquals(201, $result->getHttpStatusCode());
		return $result;
	}

	/**
	 * @depends testHttpStatusCode
	 * @param Result $result
	 * @return Result
	 */
	public function testHeaders($result)
	{
		$h = $result->getHeaders();
		self::assertInstanceOf('\Zend\Http\Headers', $h);

		$headers = new \Zend\Http\Headers();
		$result->setHeaders($headers);
		self::assertNotSame($h, $result->getHeaders());

		self::assertSame($headers, $result->getHeaders());

		$result->setHeaderLocation('http://headerlocation.net');

		self::assertEquals('http://headerlocation.net', $headers->get('Location')->getFieldValue());

		$result->setHeaderContentLocation('http://headercontentlocation.net');
		self::assertEquals('http://headercontentlocation.net', $headers->get('Content-location')->getFieldValue());

		self::assertNull($result->getHeaderLastModified());
		$date = new \DateTime();
		$result->setHeaderLastModified($date);

		self::assertEquals($date, $result->getHeaderLastModified());

		$result->setHeaderLastModified(null);
		self::assertNull($result->getHeaderLastModified());

		self::assertNull($result->getHeaderEtag());
		$result->setHeaderEtag('test');
		self::assertEquals('test', $result->getHeaderEtag());
		$result->setHeaderEtag(null);
		self::assertNull($result->getHeaderEtag());

		return $result;
	}
}