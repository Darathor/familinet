<?php
namespace ChangeTests\Change\I18n;

use Change\I18n\I18nManager;

class I18nManagerTest extends \ChangeTests\Change\TestAssets\TestCase // NOSONAR
{
	protected function setUp(): void
	{
		parent::setUp();

		// Stop logging non existent keys.
		$callback = static function (\Change\Events\Event $event) {
			// Add '--' before key to validate that this callback is really used.
			return '--' . $event->getParam('preparedKey')->getKey();
		};
		$this->getApplicationServices()->getI18nManager()->getEventManager()->attach(I18nManager::EVENT_KEY_NOT_FOUND, $callback, 10);

		$callback = static function (\Zend\EventManager\Event $event) {
			$formatters = $event->getParam('formatters');
			if (\in_array('required', $formatters, true))
			{
				if (\in_array('html', $formatters, true))
				{
					$event->setParam('text', '<span class="required">*</span> ' . $event->getParam('text'));
				}
				else
				{
					$event->setParam('text', '* ' . $event->getParam('text'));
				}
			}
		};
		$this->getApplicationServices()->getI18nManager()->getEventManager()->attach(I18nManager::EVENT_FORMATTING, $callback, 2);
	}

	public function testIsValidLCID()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertTrue($manager->isValidLCID('fr_FR'));
		self::assertTrue($manager->isValidLCID('en_GB'));
		self::assertTrue($manager->isValidLCID('to_TO'));
		self::assertTrue($manager->isValidLCID('ab_CD'));
		self::assertFalse($manager->isValidLCID('toto'));
		self::assertFalse($manager->isValidLCID('FR_FR'));
		self::assertFalse($manager->isValidLCID('fr_fr'));
		self::assertFalse($manager->isValidLCID('FR_fr'));
		self::assertFalse($manager->isValidLCID('a8_A9'));
		self::assertFalse($manager->isValidLCID('fr-FR'));
		self::assertFalse($manager->isValidLCID('fr_FR '));
		self::assertFalse($manager->isValidLCID(' fr_FR'));
		self::assertFalse($manager->isValidLCID('fr__FR'));
	}

	public function testGetSupportedLCIDs()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$config->addVolatileEntry('Change/I18n/supported-lcids', null);
		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_US', 'it_IT', 'es_ES', 'en_GB']);

		$config->addVolatileEntry('Change/I18n/langs', null);
		$config->addVolatileEntry('Change/I18n/langs', ['en_US' => 'us']);

		$manager = new I18nManager();
		$manager->setApplication($application);

		self::assertEquals(['fr_FR', 'en_US', 'it_IT', 'es_ES', 'en_GB'], $manager->getSupportedLCIDs());
		return $manager;
	}

	public function testSupportsMultipleLCIDs()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();

		$config->addVolatileEntry('Change/I18n/supported-lcids', null);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertFalse($manager->supportsMultipleLCIDs());

		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertFalse($manager->supportsMultipleLCIDs());

		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_US']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertTrue($manager->supportsMultipleLCIDs());

		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_US', 'it_IT', 'es_ES', 'en_GB']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertTrue($manager->supportsMultipleLCIDs());
	}

	public function testHasGetI18nSynchro()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$config->addVolatileEntry('Change/I18n/supported-lcids', ['fr_FR', 'en_US', 'it_IT', 'es_ES', 'en_GB']);
		$configArray = $config->getConfigArray();

		$config->addVolatileEntry('Change/I18n/synchro/keys', []);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertFalse($manager->hasI18nSynchro());
		self::assertEquals([], $manager->getI18nSynchro());

		$config->setConfigArray($configArray);
		$config->addVolatileEntry('Change/I18n/synchro/keys', ['en_US' => 'fr_FR']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertTrue($manager->hasI18nSynchro());
		self::assertEquals(['en_US' => 'fr_FR'], $manager->getI18nSynchro());

		$config->setConfigArray($configArray);
		$config->addVolatileEntry('Change/I18n/synchro/keys',
			['en_US' => 'fr_FR', 'en_GB' => 'en_US']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertTrue($manager->hasI18nSynchro());
		self::assertEquals(['en_US' => 'fr_FR', 'en_GB' => 'en_US'], $manager->getI18nSynchro());

		// Unsupported LCIDs are ignored
		$config->setConfigArray($configArray);
		$config->addVolatileEntry('Change/I18n/synchro/keys', ['en_US' => 'fr_FR', 'fr_FR' => 'kl_KL', 'to_TO' => 'fr_FR']);
		$manager = new I18nManager();
		$manager->setApplication($application);
		self::assertTrue($manager->hasI18nSynchro());
		self::assertEquals(['en_US' => 'fr_FR'], $manager->getI18nSynchro());
	}

	/**
	 * @depends testGetSupportedLCIDs
	 * @param \Change\I18n\I18nManager $manager
	 */
	public function testGetDefaultLang(\Change\I18n\I18nManager $manager)
	{
		self::assertEquals('fr_FR', $manager->getDefaultLCID());
	}

	/**
	 * @depends testGetSupportedLCIDs
	 * @param \Change\I18n\I18nManager $manager
	 */
	public function testGetLangByLCID(\Change\I18n\I18nManager $manager)
	{
		self::assertEquals('fr', $manager->getLangByLCID('fr_FR'));
		self::assertEquals('us', $manager->getLangByLCID('en_US'));
		self::assertEquals('xx', $manager->getLangByLCID('xx_XX'));
		try
		{
			$manager->getLangByLCID('fr');
			self::fail('A InvalidArgumentException should be thrown.');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertEquals('Not supported LCID: fr', $e->getMessage());
		}
	}

	/**
	 * @depends testGetSupportedLCIDs
	 * @param \Change\I18n\I18nManager $manager
	 */
	public function testGetSetLCID(\Change\I18n\I18nManager $manager)
	{
		// If no UI lang is set, use the default one.
		self::assertEquals($manager->getDefaultLCID(), $manager->getLCID());

		// Set/get supported languages.
		$manager->setLCID('it_IT');
		self::assertEquals('it_IT', $manager->getLCID());
		$manager->setLCID('en_US');
		self::assertEquals('en_US', $manager->getLCID());

		// Setting an unsupported language.
		try
		{
			$manager->setLCID('kl_KL');
			self::fail('A InvalidArgumentException should be thrown.');
		}
		catch (\InvalidArgumentException $e)
		{
			self::assertEquals('Not supported LCID: kl_KL', $e->getMessage());
		}
	}

	public function testPrepareKeyFromTransString()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$preparedKey = $manager->prepareKeyFromTransString('m.website.fo.test,ucf,toto=titi,attr');
		self::assertEquals('m.website.fo.test', $preparedKey->getKey());
		self::assertEquals(['ucf', 'attr'], $preparedKey->getFormatters());
		self::assertEquals(['toto' => 'titi'], $preparedKey->getReplacements());

		// Keys and formatters are lower cased and spaces are cleaned.
		$preparedKey = $manager->prepareKeyFromTransString(' m.Website.Fo.test.TEst ,uCf , toTo= titI , aTTr');
		self::assertEquals('m.website.fo.test.test', $preparedKey->getKey());
		self::assertEquals(['ucf', 'attr'], $preparedKey->getFormatters());
		self::assertEquals(['toTo' => 'titI'], $preparedKey->getReplacements());
	}

	public function testTranslateNoKey()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$a = 'çé Té tutu';
		self::assertEquals($a, $manager->trans($a));
	}

	public function testTransForLCID()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		// Key translation.
		self::assertEquals('plop fr a.aa', $manager->transForLCID('fr_FR', 'm.project.tests.a.plop'));
		self::assertEquals('plip fr b', $manager->transForLCID('fr_FR', 'm.project.tests.a.plip'));
		self::assertEquals('', $manager->transForLCID('fr_FR', 'm.project.tests.a.empty'));

		self::assertEquals('--m.project.tests.a.plep', $manager->transForLCID('fr_FR', 'm.project.tests.a.plep'));

		self::assertEquals('plop us a.aa', $manager->transForLCID('en_US', 'm.project.tests.a.plop'));
		// Default fallback test
		self::assertEquals('plip fr b', $manager->transForLCID('en_US', 'm.project.tests.a.plip'));
		self::assertEquals('--m.project.tests.a.undefinedkey', $manager->transForLCID('en_US', 'm.project.tests.a.undefinedkey'));

		self::assertEquals('un texte quelconque', $manager->transForLCID('fr_FR', 'un texte quelconque'));

		// Converters.
		self::assertEquals('Plop fr a.aa', $manager->transForLCID('fr_FR', 'm.project.tests.a.plop', ['ucf']));
		self::assertEquals('un texte quelconque', $manager->transForLCID('fr_FR', 'un texte quelconque', ['ucf']));
		self::assertEquals('PLOP FR A.AA :', $manager->transForLCID('fr_FR', 'm.project.tests.a.plop', ['uc', 'lab']));
		self::assertEquals('un texte quelconque', $manager->transForLCID('fr_FR', 'un texte quelconque', ['uc', 'lab']));

		// Substitutions.
		self::assertEquals('Withparams test $PARAM2$ fr a',
			$manager->transForLCID('fr_FR', 'm.project.tests.a.withparams', ['ucf'], ['PARAM1' => 'test']));
		self::assertEquals('withparams test youpi fr a', $manager->transForLCID('fr_FR', 'm.project.tests.a.withparams', [],
			['PARAM1' => 'test', 'PARAM2' => 'youpi']));

		self::assertEquals('withparams test youpi fr a', $manager->transForLCID('fr_FR', 'm.project.tests.a.withparams', [],
			['PaRaM1' => 'test', 'param2' => 'youpi']));

		// Key synchro.
		$config = $this->getApplication()->getConfiguration();
		self::assertFalse($manager->hasI18nSynchro());
		self::assertEquals('plop us a.aa', $manager->transForLCID('en_US', 'm.project.tests.a.plop'));
		self::assertEquals('--m.project.tests.a.plop', $manager->transForLCID('en_GB', 'm.project.tests.a.plop'));

		$config->addVolatileEntry('Change/I18n/supported-lcids', ['en_GB']);
		$config->addVolatileEntry('Change/I18n/synchro/keys', ['en_GB' => 'en_US']);

		$syncManager = new I18nManager();
		$syncManager->setApplication($this->getApplication());
		$syncManager->setPluginManager($this->getApplicationServices()->getPluginManager());
		self::assertTrue($syncManager->hasI18nSynchro());
		self::assertEquals('plop us a.aa', $syncManager->transForLCID('en_GB', 'm.project.tests.a.plop'));
		self::assertEquals('plop us a.aa', $syncManager->transForLCID('en_US', 'm.project.tests.a.plop'));
	}

	public function testFormatText()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		// Converters.
		self::assertEquals('Un texte quelconque', $manager->formatText('fr_FR', 'un texte quelconque', ['ucf']));
		self::assertEquals('UN TEXTE QUELCONQUE :',
			$manager->formatText('fr_FR', 'un texte quelconque', ['uc', 'lab']));
		self::assertEquals('UN TEXTE QUELCONQUE:',
			$manager->formatText('en_US', 'un texte quelconque', ['uc', 'lab']));

		// Substitutions.
		self::assertEquals('Un texte pramétré test $param2$',
			$manager->formatText('fr_FR', 'un texte pramétré $param1$ $param2$', ['ucf'], ['param1' => 'test']));
		self::assertEquals('un texte pramétré test youpi',
			$manager->formatText('fr_FR', 'un texte pramétré $param1$ $param2$', [],
				['param1' => 'test', 'param2' => 'youpi']));

		// Specific formatter.
		self::assertEquals('* Un texte quelconque',
			$manager->formatText('fr_FR', 'un texte quelconque', ['required', 'ucf']));
		self::assertEquals('<span class="required">*</span> Un texte quelconque',
			$manager->formatText('fr_FR', 'un texte quelconque', ['ucf', 'required', 'html']));
	}

	/**
	 * @depends testTransForLCID
	 */
	public function testTrans()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		self::assertEquals('fr_FR', $manager->getLCID());

		// Key translation.
		self::assertEquals('plop fr a.aa', $manager->trans('m.project.tests.a.plop'));
		self::assertEquals('plip fr b', $manager->trans('m.project.tests.a.plip'));
		self::assertEquals('--m.project.tests.a.plep', $manager->trans('m.project.tests.a.plep'));
		self::assertEquals('un texte quelconque', $manager->trans('un texte quelconque'));

		// Converters.
		self::assertEquals('Plop fr a.aa', $manager->trans('m.project.tests.a.plop', ['ucf']));
		self::assertEquals('un texte quelconque', $manager->trans('un texte quelconque', ['ucf']));
		self::assertEquals('PLOP FR A.AA :', $manager->trans('m.project.tests.a.plop', ['uc', 'lab']));
		self::assertEquals('un texte quelconque', $manager->trans('un texte quelconque', ['uc', 'lab']));

		// Substitutions.
		self::assertEquals('Withparams test $PARAM2$ fr a',
			$manager->trans('m.project.tests.a.withparams', ['ucf'], ['param1' => 'test']));
		self::assertEquals('withparams test youpi fr a',
			$manager->trans('m.project.tests.a.withparams', [], ['param1' => 'test', 'param2' => 'youpi']));
	}

	// Dates.

	/**
	 * Tests for:
	 *  - get/setDateFormat
	 *  - get/setDateTimeFormat
	 *  - get/setTimeZone
	 */
	public function testGetSetDateFormatsAndTimeZone()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		// If no values set, use the default ones.
		$config = $this->getApplication()->getConfiguration();
		self::assertEquals($config->getEntry('Change/I18n/default-timezone'), $manager->getTimeZone()->getName());
		foreach (['fr_FR', 'en_US'] as $lang)
		{
			self::assertEquals($manager->transForLCID($lang, 'c.date.default_date_format'), $manager->getDateFormat($lang));
			self::assertEquals($manager->transForLCID($lang, 'c.date.default_datetime_format'),
				$manager->getDateTimeFormat($lang));
		}

		// If values are set, these values are returned.
		$manager->setDateFormat('test');
		self::assertEquals('test', $manager->getDateFormat('fr_FR'));
		self::assertEquals('test', $manager->getDateFormat('en_US'));

		$manager->setDateTimeFormat('toto');
		self::assertEquals('toto', $manager->getDateTimeFormat('fr_FR'));
		self::assertEquals('toto', $manager->getDateTimeFormat('en_US'));

		$manager->setTimeZone('Asia/Tokyo'); // Time zone may be set by code...
		self::assertEquals('Asia/Tokyo', $manager->getTimeZone()->getName());
		$manager->setTimeZone(new \DateTimeZone('America/New_York')); // ... or by DateTimeZone instance.
		self::assertEquals('America/New_York', $manager->getTimeZone()->getName());

		// Admitted values for following tests.
		$manager->setTimeZone('Europe/Paris');
		self::assertEquals('Europe/Paris', $manager->getTimeZone()->getName());
		$manager->setDateFormat('dd/MM/yyyy');
		self::assertEquals('dd/MM/yyyy', $manager->getDateFormat($manager->getLCID()));
		$manager->setDateTimeFormat('dd/MM/yyyy HH:mm');
		self::assertEquals('dd/MM/yyyy HH:mm', $manager->getDateTimeFormat($manager->getLCID()));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testGetLocalDateTime()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getLocalDateTime('2012-10-16 09:00:00');
		self::assertEquals('Europe/Paris', $date->getTimezone()->getName());
		self::assertEquals('2012-10-16 09:00:00', $date->format('Y-m-d H:i:s'));
		return $date;
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testGetGMTDateTime()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getGMTDateTime('2012-10-16 09:00:00');
		self::assertEquals('UTC', $date->getTimezone()->getName());
		self::assertEquals('2012-10-16 09:00:00', $date->format('Y-m-d H:i:s'));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testToGMTDateTime()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getLocalDateTime('2012-10-16 09:00:00');
		$manager->toGMTDateTime($date);
		self::assertEquals('UTC', $date->getTimezone()->getName());
		self::assertEquals('2012-10-16 07:00:00', $date->format('Y-m-d H:i:s'));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testToLocalDateTime()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getGMTDateTime('2012-10-16 09:00:00');
		$manager->toLocalDateTime($date);
		self::assertEquals('Europe/Paris', $date->getTimezone()->getName());
		self::assertEquals('2012-10-16 11:00:00', $date->format('Y-m-d H:i:s'));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testFormatDate()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		// Basic formatting.
		$date = $manager->getLocalDateTime('2012-10-16 09:00:00');
		self::assertEquals('16/10/2012 09:00', $manager->formatDate('fr_FR', $date, 'dd/MM/yyyy HH:mm'));

		// The language is correctly used for days and months.
		self::assertEquals('mardi, 16 octobre 2012', $manager->formatDate('fr_FR', $date, 'EEEE, d MMMM yyyy'));
		self::assertEquals('Tuesday, October 16 2012', $manager->formatDate('en_US', $date, 'EEEE, MMMM d yyyy'));

		// Without any specified time zone, the date is converted to the local timezone.
		self::assertEquals('09:00 UTC+02:00', $manager->formatDate('fr_FR', $date, 'HH:mm ZZZZ'));
		$gmtDate = $manager->getGMTDateTime('2012-10-16 09:00:00');
		self::assertEquals('11:00 UTC+02:00', $manager->formatDate('fr_FR', $gmtDate, 'HH:mm ZZZZ'));

		// If there is a specified time zone, the date is converted to it.
		$dt = $manager->formatDate('fr_FR', $date, 'HH:mm ZZZZ', new \DateTimeZone('America/New_York'));
		if (strpos($dt, '-'))
		{
			self::assertEquals('03:00 UTC-04:00', $dt);
		}
		else
		{
			//TODO fix this php 5.4.15 bug
			self::assertEquals('03:00 UTC−04:00', $dt);
		}

		self::assertEquals('18:00 UTC+09:00', $manager->formatDate('fr_FR', $gmtDate, 'HH:mm ZZZZ', new \DateTimeZone('Asia/Tokyo')));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testTransDate()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getLocalDateTime('2012-10-16 09:00:00');
		self::assertEquals('dd/MM/yyyy', $manager->getDateFormat($manager->getLCID()));
		self::assertEquals('16/10/2012', $manager->transDate($date));
	}

	/**
	 * @depends testGetSetDateFormatsAndTimeZone
	 */
	public function testTransDateTime()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$date = $manager->getLocalDateTime('2012-10-16 09:00:00');
		self::assertEquals('dd/MM/yyyy à HH:mm', $manager->getDateTimeFormat($manager->getLCID()));
		self::assertEquals('16/10/2012 à 09:00', $manager->transDateTime($date));
	}

	// File sizes.

	public function testFormatFileSize()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		self::assertEquals('10 octets', $manager->formatFileSize('fr_FR', 10));
		self::assertEquals('9 <abbr title="Kilo-octets">Ko</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 - 1));
		self::assertEquals('10 <abbr title="Kilo-octets">Ko</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024));
		self::assertEquals('9 <abbr title="Méga-octets">Mo</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Méga-octets">Mo</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024));
		self::assertEquals('9 <abbr title="Giga-octets">Go</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Giga-octets">Go</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024 * 1024));
		self::assertEquals('9 <abbr title="Tera-octets">To</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Tera-octets">To</abbr>', $manager->formatFileSize('fr_FR', 10 * 1024 * 1024 * 1024 * 1024));
	}

	public function testTransFileSize()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$manager->setLCID('fr_FR');

		self::assertEquals('10 octets', $manager->transFileSize(10));
		self::assertEquals('9 <abbr title="Kilo-octets">Ko</abbr>', $manager->transFileSize(10 * 1024 - 1));
		self::assertEquals('10 <abbr title="Kilo-octets">Ko</abbr>', $manager->transFileSize(10 * 1024));
		self::assertEquals('9 <abbr title="Méga-octets">Mo</abbr>', $manager->transFileSize(10 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Méga-octets">Mo</abbr>', $manager->transFileSize(10 * 1024 * 1024));
		self::assertEquals('9 <abbr title="Giga-octets">Go</abbr>', $manager->transFileSize(10 * 1024 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Giga-octets">Go</abbr>', $manager->transFileSize(10 * 1024 * 1024 * 1024));
		self::assertEquals('9 <abbr title="Tera-octets">To</abbr>', $manager->transFileSize(10 * 1024 * 1024 * 1024 * 1024 - 1));
		self::assertEquals('10 <abbr title="Tera-octets">To</abbr>', $manager->transFileSize(10 * 1024 * 1024 * 1024 * 1024));
	}

	// Transformers.

	public function testTransformLab()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('test :', $manager->transformLab('test', 'fr_FR'));
		self::assertEquals('test:', $manager->transformLab('test', 'en_US'));
	}

	public function testTransformUc()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('TEST', $manager->transformUc('test', 'fr_FR'));
		self::assertEquals('TEST', $manager->transformUc('tEsT', 'fr_FR'));
		self::assertEquals('ÉTÉ ÇA', $manager->transformUc('été ça', 'fr_FR'));
	}

	public function testTransformUcf()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('Test', $manager->transformUcf('test', 'fr_FR'));
		self::assertEquals('TEsT', $manager->transformUcf('tEsT', 'fr_FR'));
		self::assertEquals('Été ça', $manager->transformUcf('été ça', 'fr_FR'));
	}

	public function testTransformUcw()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('Test Test', $manager->transformUcw('test test', 'fr_FR'));
		self::assertEquals('Test', $manager->transformUcw('tEsT', 'fr_FR'));
		self::assertEquals('Été Ça', $manager->transformUcw('été ça', 'fr_FR'));
	}

	public function testTransformLc()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('test test', $manager->transformLc('test test', 'fr_FR'));
		self::assertEquals('test', $manager->transformLc('tEsT', 'fr_FR'));
		self::assertEquals('été ça été', $manager->transformLc('été ça Été', 'fr_FR'));
	}

	public function testTransformJs()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('test \"test\"', $manager->transformJs('test "test"', 'fr_FR'));
		self::assertEquals('tEsT \t \n \\\'test\\\' \\\\', $manager->transformJs("tEsT \t \n 'test' \\", 'fr_FR'));
	}

	public function testTransformHtml()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals("test <br />\n &lt;em&gt;toto&lt;/em&gt; &quot;test&quot;",
			$manager->transformHtml("test \n <em>toto</em> \"test\"", 'fr_FR'));
	}

	public function testTransformText()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		$source = '<h1>Titre</h1><p>Un<br/>paragraphe</p><ul><li>item 1</li><li>item 2</li></ul><hr><div class="test">Contenu du div</div>';
		$expected =
			'Titre' . PHP_EOL . 'Un' . PHP_EOL . 'paragraphe' . PHP_EOL . ' * item 1' . PHP_EOL . ' * item 2' . PHP_EOL . '------'
			. PHP_EOL . 'Contenu du div';
		self::assertEquals($expected, $manager->transformText($source, 'fr_FR'));

		$source = '<table><tr><th>Titre 1</th><th>Titre 2</th></tr><tr><td>Cellule 1</td><td>Cellule 2</td></tr><table>';
		$expected = "Titre 1\tTitre 2\t" . PHP_EOL . "Cellule 1\tCellule 2";
		self::assertEquals($expected, $manager->transformText($source, 'fr_FR'));
	}

	public function testTransformAttr()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('test&quot;sqdqs&quot; qsdqsd&lt;sdsdf&gt;',
			$manager->transformAttr('test"sqdqs" qsdqsd<sdsdf>', 'fr_FR'));
	}

	public function testTransformSpace()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals(' test 3 ', $manager->transformSpace('test 3', 'fr_FR'));
		self::assertEquals(' ... ', $manager->transformSpace('...', 'fr_FR'));
	}

	public function testTransformEtc()
	{
		$manager = $this->getApplicationServices()->getI18nManager();
		self::assertEquals('test 3...', $manager->transformEtc('test 3', 'fr_FR'));
		self::assertEquals('......', $manager->transformEtc('...', 'fr_FR'));
	}
}