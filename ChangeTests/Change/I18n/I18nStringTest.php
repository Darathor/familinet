<?php
namespace ChangeTests\Change\I18n;

use Change\I18n\I18nString;

class I18nStringTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstructor()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		// Giving a string as key.

		$string0 = new I18nString($manager, '');
		self::assertEquals('', $string0->getKey());
		self::assertEquals(array(), $string0->getFormatters());
		self::assertEquals(array(), $string0->getReplacements());

		$string1 = new I18nString($manager, 'm.website.fo.test');
		self::assertEquals('m.website.fo.test', $string1->getKey());
		self::assertEquals(array(), $string1->getFormatters());
		self::assertEquals(array(), $string1->getReplacements());

		$string2 = new I18nString($manager, 'm.website.fo.test-params', array('ucf'), array('param1' => 'Value 1',
			'param2' => 'Value 2'));
		self::assertEquals('m.website.fo.test-params', $string2->getKey());
		self::assertEquals(array('ucf'), $string2->getFormatters());
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2'), $string2->getReplacements());

		// Giving a PreparedKey.

		$key = new \Change\I18n\PreparedKey('m.website.fo.test-params', array('ucf'), array('param1' => 'Value 1',
			'param10' => 'Value 10'));

		$string3 = new I18nString($manager, $key);
		self::assertEquals('m.website.fo.test-params', $string3->getKey());
		self::assertEquals(array('ucf'), $string3->getFormatters());
		self::assertEquals(array('param1' => 'Value 1', 'param10' => 'Value 10'),
			$string3->getReplacements());

		$string4 = new I18nString($manager, $key, array('ucf', 'lab'), array('param1' => 'Value 1 bis',
			'param2' => 'Value 2'));
		self::assertEquals('m.website.fo.test-params', $string3->getKey());
		self::assertEquals(array('ucf', 'lab'), $string4->getFormatters());
		self::assertEquals(array('param1' => 'Value 1 bis', 'param2' => 'Value 2', 'param10' => 'Value 10'),
			$string4->getReplacements());
	}

	public function testToString()
	{
		$manager = $this->getApplicationServices()->getI18nManager();

		self::assertEquals('fr_FR', $manager->getLCID());

		// Key translation.
		self::assertEquals('plip fr b', strval(new I18nString($manager, 'm.project.tests.a.plip')));

		// Converters.
		self::assertEquals('Plop fr a.aa', strval(new I18nString($manager, 'm.project.tests.a.plop', array('ucf'))));
		self::assertEquals('PLOP FR A.AA :', strval(new I18nString($manager, 'm.project.tests.a.plop', array('uc', 'lab'))));

		// Substitutions.
		self::assertEquals('Withparams test $PARAM2$ fr a',
			strval(new I18nString($manager, 'm.project.tests.a.withparams', array('ucf'), array('param1' => 'test'))));
		self::assertEquals('withparams test youpi fr a',
			strval(new I18nString($manager, 'm.project.tests.a.withparams', array(), array('param1' => 'test',
				'param2' => 'youpi'))));
	}
}