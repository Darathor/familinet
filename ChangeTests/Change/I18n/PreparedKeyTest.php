<?php
namespace ChangeTests\Change\I18n;

class PreparedKeyTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstructor()
	{
		$key0 = new \Change\I18n\PreparedKey('');
		self::assertEquals('', $key0->getKey());
		self::assertEquals(array(), $key0->getFormatters());
		self::assertEquals(array(), $key0->getReplacements());

		$key1 = new \Change\I18n\PreparedKey('m.website.fo.test');
		self::assertEquals('m.website.fo.test', $key1->getKey());
		self::assertEquals(array(), $key1->getFormatters());
		self::assertEquals(array(), $key1->getReplacements());

		$key2 = new \Change\I18n\PreparedKey('m.website.fo.test-params', array('ucf'), array('param1' => 'Value 1',
			'param2' => 'Value 2'));
		self::assertEquals('m.website.fo.test-params', $key2->getKey());
		self::assertEquals(array('ucf'), $key2->getFormatters());
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2'), $key2->getReplacements());
	}

	// Methods on key.

	/**
	 * Tests the following methods:
	 * - getKey
	 * - setKey
	 * - __toString
	 * @depends testConstructor
	 */
	public function testGetSetKey()
	{
		$key = new \Change\I18n\PreparedKey('');
		$key->setKey('c.date.default-format');
		self::assertEquals('c.date.default-format', $key->getKey());
		self::assertEquals('c.date.default-format', strval($key));

		$key->setKey('m.website.fo.test');
		self::assertEquals('m.website.fo.test', $key->getKey());
		self::assertEquals('m.website.fo.test', strval($key));

		$key->setKey('t.default.templates.tpl1');
		self::assertEquals('t.default.templates.tpl1', $key->getKey());
		self::assertEquals('t.default.templates.tpl1', strval($key));

		// Invalid keys are not modified.
		$key->setKey(' c.date.Default-Format');
		self::assertEquals(' c.date.Default-Format', $key->getKey());
		self::assertEquals(' c.date.Default-Format', strval($key));

		$key->setKey('C\'est l\'ÉTÉ. Ça va chauffer !');
		self::assertEquals('C\'est l\'ÉTÉ. Ça va chauffer !', $key->getKey());
		self::assertEquals('C\'est l\'ÉTÉ. Ça va chauffer !', strval($key));
	}

	/**
	 * @depends testGetSetKey
	 */
	public function testIsValid()
	{
		$key = new \Change\I18n\PreparedKey('');

		// Minimum 4 parts.
		$key->setKey('m');
		self::assertFalse($key->isValid());
		$key->setKey('m.website');
		self::assertFalse($key->isValid());
		$key->setKey('m.website.test');
		self::assertFalse($key->isValid());
		$key->setKey('m.website.fo.test');
		self::assertFalse($key->isValid());
		$key->setKey('m.website.fo.test.titi');
		self::assertTrue($key->isValid());


		// For the first path, only 'c', 'm' or 't' are valid.
		$key->setKey('toto.website.test');
		self::assertFalse($key->isValid());
		$key->setKey('t.website.test.tutu.titi');
		self::assertTrue($key->isValid());
		$key->setKey('c.website.test');
		self::assertTrue($key->isValid());
		$key->setKey('v.website.test');
		self::assertFalse($key->isValid());
	}

	/**
	 * @depends testIsValid
	 */
	public function testGetPathGetId()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test.tutu');
		self::assertEquals('m.website.fo.test.tutu', $key->getKey());
		self::assertTrue($key->isValid());
		self::assertEquals('m.website.fo.test', $key->getPath());
		self::assertEquals('tutu', $key->getId());

		$key->setKey('t.default.templates.a.tpl1');
		self::assertEquals('t.default.templates.a.tpl1', $key->getKey());
		self::assertTrue($key->isValid());
		self::assertEquals('t.default.templates.a', $key->getPath());
		self::assertEquals('tpl1', $key->getId());

		$key->setKey('t.default');
		self::assertEquals('t.default', $key->getKey());
		self::assertFalse($key->isValid());
		self::assertNull($key->getPath());
		self::assertNull($key->getId());
	}

	// Methods on transformers.

	/**
	 * Tests the following methods:
	 * - getFormatters
	 * - setFormatters
	 * - hasFormatters
	 * @depends testConstructor
	 */
	public function testGetSetFormatters()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test');
		self::assertFalse($key->hasFormatters());
		self::assertEquals(array(), $key->getFormatters());

		$key->setFormatters(array('ucf', 'js'));
		self::assertTrue($key->hasFormatters());
		self::assertEquals(array('ucf', 'js'), $key->getFormatters());

		$key->setFormatters(array('html'));
		self::assertTrue($key->hasFormatters());
		self::assertEquals(array('html'), $key->getFormatters());
	}

	/**
	 * @depends testGetSetFormatters
	 */
	public function testAddFormatter()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test', array('html'));
		self::assertEquals(array('html'), $key->getFormatters());
		$key->addFormatter('ucf');
		self::assertEquals(array('html', 'ucf'), $key->getFormatters());
		$key->addFormatter('lab');
		self::assertEquals(array('html', 'ucf', 'lab'), $key->getFormatters());
		$key->addFormatter('html');
		$key->addFormatter('ucf');
		self::assertEquals(array('html', 'ucf', 'lab'), $key->getFormatters());
	}

	/**
	 * @depends testGetSetFormatters
	 */
	public function testMergeFormatters()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test', array('html'));
		self::assertEquals(array('html'), $key->getFormatters());

		$key->mergeFormatters(array('ucf', 'lab'));
		$formatters = $key->getFormatters();
		sort($formatters); // Merge do not preserve order.
		self::assertEquals(array('html', 'lab', 'ucf'), $formatters);

		$key->mergeFormatters(array('html', 'js'));
		$formatters = $key->getFormatters();
		sort($formatters);
		self::assertEquals(array('html', 'js', 'lab', 'ucf'), $formatters);
	}

	// Methods on replacements.

	/**
	 * Tests the following methods:
	 * - getReplacements
	 * - setReplacements
	 * - hasReplacements
	 * @depends testConstructor
	 */
	public function testGetSetReplacements()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test');
		self::assertFalse($key->hasReplacements());
		self::assertEquals(array(), $key->getReplacements());

		$key->setReplacements(array('param1' => 'Value 1', 'param2' => 'Value 2'));
		self::assertTrue($key->hasReplacements());
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2'), $key->getReplacements());

		$key->setReplacements(array('toto' => 'titi'));
		self::assertTrue($key->hasReplacements());
		self::assertEquals(array('toto' => 'titi'), $key->getReplacements());
	}

	/**
	 * @depends testGetSetReplacements
	 */
	public function testSetReplacements()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test', array('html'), array('param1' => 'Value 1',
			'param2' => 'Value 2'));
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2'), $key->getReplacements());
		$key->setReplacement('test1', 'test1value');
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2', 'test1' => 'test1value'),
			$key->getReplacements());
		$key->setReplacement('cms', 'RBS Change');
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2', 'test1' => 'test1value', 'cms' => 'RBS Change'),
			$key->getReplacements());
		$key->setReplacement('cms', 'RBS Change');
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2', 'test1' => 'test1value', 'cms' => 'RBS Change'),
			$key->getReplacements());
		$key->setReplacement('test1', 'une autre valeur');
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2', 'test1' => 'une autre valeur',
			'cms' => 'RBS Change'), $key->getReplacements());
	}

	/**
	 * @depends testGetSetReplacements
	 */
	public function testMergeReplacements()
	{
		$key = new \Change\I18n\PreparedKey('m.website.fo.test', array('html'), array('param1' => 'Value 1',
			'param2' => 'Value 2'));
		self::assertEquals(array('param1' => 'Value 1', 'param2' => 'Value 2'), $key->getReplacements());

		$key->mergeReplacements(array('cms' => 'RBS Change 3.6', 'browser' => 'Firefox'));
		$replacements = $key->getReplacements();
		ksort($replacements); // Merge preserves keys but not order.
		self::assertEquals(array('browser' => 'Firefox', 'cms' => 'RBS Change 3.6', 'param1' => 'Value 1',
			'param2' => 'Value 2'), $replacements);

		$key->mergeReplacements(array('cms' => 'RBS Change 4.0', 'browser' => 'Firefox', 'os' => 'windows'));
		$replacements = $key->getReplacements();
		ksort($replacements); // Merge preserves keys but not order.
		self::assertEquals(array('browser' => 'Firefox', 'cms' => 'RBS Change 4.0', 'os' => 'windows', 'param1' => 'Value 1',
			'param2' => 'Value 2'), $replacements);
	}
}