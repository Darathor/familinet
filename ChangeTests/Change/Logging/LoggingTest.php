<?php
namespace Tests\Change\Logging;

/**
 * @name \Tests\Change\Logging\LoggingTest
 */
class LoggingTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		$configuration = $this->getApplication()->getConfiguration();
		$testLogConfiguration = $configuration->getEntry('Change/Logging');
		$testLogConfiguration['level'] = 'WARN';
		$configuration->addVolatileEntry('Change/Logging', $testLogConfiguration);
	}

	public function testGetLevel()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$logging = $this->getApplicationServices()->getLogging();

		$config->addVolatileEntry('Change/Logging/level', 'ALERT');
		self::assertEquals('ALERT', $logging->getLevel());
		$config->addVolatileEntry('Change/Logging/level', 'ERR');
		self::assertEquals('ERR', $logging->getLevel());
	}

	public function testGetAndSetPriority()
	{
		$application = $this->getApplication();
		$config = $application->getConfiguration();
		$logging = $this->getApplicationServices()->getLogging();
		$config->addVolatileEntry('Change/Logging/level', 'DEBUG');

		// Setting valid value is OK.
		$logging->setPriority(5);
		self::assertEquals(5, $logging->getPriority());
		$logging->setPriority(2);
		self::assertEquals(2, $logging->getPriority());

		// Setting null calculate the value.
		$config->addVolatileEntry('Change/Logging/level', 'DEBUG');
		$logging->setPriority(null);
		self::assertEquals(7, $logging->getPriority());
		$config->addVolatileEntry('Change/Logging/level', 'ERR');
		$logging->setPriority(null);
		self::assertEquals(3, $logging->getPriority());

		// Setting invalid value reset it to calculated one.
		$config->addVolatileEntry('Change/Logging/level', 'NOTICE');
		$logging->setPriority(15);
		self::assertEquals(5, $logging->getPriority());
		$config->addVolatileEntry('Change/Logging/level', 'INFO');
		$logging->setPriority('toto');
		self::assertEquals(6, $logging->getPriority());
	}

	public function testGetSetLoggerByName()
	{
		$logging = $this->getApplicationServices()->getLogging();

		include __DIR__ . '/TestAssets/TestWriter.php';
		$writer = new \Tests\Change\Logging\TestAssets\TestWriter();
		$writer->addFilter(new \Zend\Log\Filter\Priority(7));
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logging->setLoggerByName('application', $logger);
		self::assertEquals($logger, $logging->getLoggerByName('application'));

		$writer = new \Tests\Change\Logging\TestAssets\TestWriter();
		$writer->setFormatter(new \Zend\Log\Formatter\ErrorHandler());
		$writer->setConvertWriteErrorsToExceptions(false);
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logging->setLoggerByName('phperror', $logger);
		self::assertEquals($logger, $logging->getLoggerByName('phperror'));

		$writer = new \Tests\Change\Logging\TestAssets\TestWriter();
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logging->setLoggerByName('toto', $logger);
		self::assertEquals($logger, $logging->getLoggerByName('toto'));

		// Logger automatic creation.
		$config = $logging->getConfiguration();

		$logging->setLoggerByName('test', null);
		self::assertNull($config->getEntry('Change/Logging/writers/test'));
		self::assertEquals('stream', $config->getEntry('Change/Logging/writers/default'));
		$logger = $logging->getLoggerByName('test');
		$writers = $logger->getWriters()->toArray();
		self::assertInstanceOf(\Zend\Log\Writer\Stream::class, $writers[0]);

		$logging->setLoggerByName('test', null);
		$config->addVolatileEntry('Change/Logging/writers/test', 'syslog');
		self::assertEquals('syslog', $config->getEntry('Change/Logging/writers/test'));
		$logger = $logging->getLoggerByName('test');
		$writers = $logger->getWriters()->toArray();
		self::assertInstanceOf(\Zend\Log\Writer\Syslog::class, $writers[0]);

		return $logging;
	}

	/**
	 * @depends testGetSetLoggerByName
	 * @param \Change\Logging\Logging $logging
	 */
	public function testLogs($logging)
	{
		$logger = $logging->getLoggerByName('application');
		$writers = $logger->getWriters()->toArray();
		/* @var $writer \Tests\Change\Logging\TestAssets\TestWriter */
		$writer = $writers[0];
		self::assertInstanceOf(\Tests\Change\Logging\TestAssets\TestWriter::class, $writer);
		$writer->clearMessages();

		// Log levels.
		$logging->debug('TOTO');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('DEBUG (7): TOTO', $writer->shiftMessage());
		self::assertEquals(0, $writer->getMessageCount());
		$logging->debug('TOTO', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('DEBUG (7): TOTO 1 Other', $writer->shiftMessage());

		$logging->info('TITI');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('INFO (6): TITI', $writer->shiftMessage());
		$logging->info('TITI', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('INFO (6): TITI 1 Other', $writer->shiftMessage());

		$logging->warn('TWTW');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('WARN (4): TWTW', $writer->shiftMessage());
		$logging->warn('TWTW', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('WARN (4): TWTW 1 Other', $writer->shiftMessage());

		$logging->error('TETE');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('ERR (3): TETE', $writer->shiftMessage());
		$logging->error('TETE', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('ERR (3): TETE 1 Other', $writer->shiftMessage());

		$logging->fatal('TFTF');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('EMERG (0): TFTF', $writer->shiftMessage());
		$logging->fatal('TFTF', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('EMERG (0): TFTF 1 Other', $writer->shiftMessage());

		$logging->namedLog('Youpi', 'application');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('WARN (4): Youpi', $writer->shiftMessage());

		$logger = $logging->getLoggerByName('toto');
		$writers = $logger->getWriters()->toArray();
		$writer = $writers[0];
		$logging->namedLog('Youpi', 'toto');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertEquals('WARN (4): Youpi', $writer->shiftMessage());

		// Deprecated logs raised as error and only in development mode.
		$logger = $logging->getLoggerByName('phperror');
		\Zend\Log\Logger::registerErrorHandler($logger);
		$writers = $logger->getWriters()->toArray();
		$writer = $writers[0];
		self::assertInstanceOf('\Tests\Change\Logging\TestAssets\Testwriter', $writer);
		$writer->clearMessages();

		$logging->exception(new \Exception('Une exception !'));
		self::assertEquals(1, $writer->getMessageCount());
		$message = $writer->shiftMessage();
		$lines = explode(PHP_EOL, $message); // Message contains a stack trace.
		self::assertGreaterThan(1, count($lines));
		self::assertStringEndsWith('ALERT (1) Exception: Une exception !', $lines[0]);

		$logging->getConfiguration()->addVolatileEntry('Change/Application/development-mode', true);
		$logging->deprecated('deprecated code usage!');
		self::assertEquals(1, $writer->getMessageCount());
		self::assertGreaterThan(0, strpos($writer->shiftMessage(), 'DEBUG (7) deprecated code usage!'));
		$logging->deprecated('deprecated code usage!', 1, 'Other');
		self::assertEquals(1, $writer->getMessageCount());

		self::assertGreaterThan(0, strpos($writer->shiftMessage(), 'DEBUG (7) deprecated code usage! 1 Other'));

		$logging->getConfiguration()->addVolatileEntry('Change/Application/development-mode', false);
		$logging->deprecated('another deprecated code usage!');
		self::assertEquals(0, $writer->getMessageCount());
		\Zend\Log\Logger::unregisterErrorHandler();
	}
}