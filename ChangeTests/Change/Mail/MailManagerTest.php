<?php
namespace ChangeTests\Change\Mail;

use ChangeTests\Change\Mail\TestAssets\FakeMailManager;
use ChangeTests\Change\TestAssets\TestCase;

/**
 * @name \ChangeTests\Change\Mail\MailManagerTest
 */
class MailManagerTest extends TestCase
{
	public function testPrepareMessage()
	{
		$application = $this->getApplicationServices();
		$mailManager = $application->getMailManager();

		$from = ['fromtest@rbschange.fr'];
		$to = ['totest@rbschange.fr'];
		$cc = ['cctest@rbschange.fr'];
		$bcc = ['bcctest@rbschange.fr'];
		$replyTo = ['replytotest@rbschange.fr'];
		$attachFiles = [];
		$encoding = null;
		$subject = 'Le retour de Chuck Norris';
		$body = 'Chuck Norris est de retour !';
		$txtBody = null;

		$message = $mailManager->prepareMessage($from, $to, $subject, $body, $txtBody, $cc, $bcc, $replyTo, $encoding, $attachFiles);

		self::assertEquals('ASCII', $message->getEncoding());
		self::assertEquals($subject, $message->getSubject());
		/** @var \Zend\Mime\Message $messageBody */
		$messageBody = $message->getBody();
		self::assertInstanceOf(\Zend\Mime\Message::class, $messageBody);
		/** @var \Zend\Mime\Part $htmlPart */
		$htmlPart = $messageBody->getParts()[0];
		self::assertInstanceOf(\Zend\Mime\Part::class, $htmlPart);
		self::assertEquals($body, $htmlPart->getRawContent());
		self::assertEquals($htmlPart->getType(), \Zend\Mime\Mime::TYPE_HTML);

		self::assertCount($message->getFrom()->count(), $from);
		self::assertCount($message->getTo()->count(), $to);
		self::assertCount($message->getCc()->count(), $cc);
		self::assertCount($message->getBcc()->count(), $bcc);
		self::assertCount($message->getReplyTo()->count(), $replyTo);

		self::assertTrue($message->getFrom()->has($from[0]));
		self::assertTrue($message->getTo()->has($to[0]));
		self::assertTrue($message->getCc()->has($cc[0]));
		self::assertTrue($message->getBcc()->has($bcc[0]));
		self::assertTrue($message->getReplyTo()->has($replyTo[0]));

		$from = [
			['email' => 'fromtest@rbschange.fr', 'name' => 'fromtest'],
			['email' => 'fromtest2@rbschange.fr', 'name' => 'fromtest2']
		];
		$to = ['totest@rbschange.fr', 'totest2@rbschange.fr'];
		$cc = ['cctest@rbschange.fr', 'cctest2@rbschange.fr'];
		$bcc = ['bcctest@rbschange.fr', 'bcctest2@rbschange.fr'];
		$replyTo = ['replytotest@rbschange.fr', 'replytotest2@rbschange.fr'];

		$attachFiles = [
			__DIR__ . '/TestAssets/test.zip' => 'application/zip',
			__DIR__ . '/TestAssets/logo.png' => 'image/png'
		];
		$encoding = 'UTF-8';
		$subject = 'Le retour de Chuck Norris 2';
		$body = '<html><body>Chuck Norris est de retour !</body><html>';
		$txtBody = 'Chuck Norris est de retour !';

		$message = $mailManager->prepareMessage($from, $to, $subject, $body, $txtBody, $cc, $bcc, $replyTo, $encoding, $attachFiles);
		self::assertEquals('UTF-8', $message->getEncoding());
		self::assertEquals($subject, $message->getSubject());
		self::assertTrue($message->getBody()->isMultiPart());
		$content = $message->getBody()->getPartContent(0);
		self::assertTrue(strpos($content, $body) > 0);
		self::assertTrue(strpos($content, $txtBody) > 0);

		self::assertCount($message->getFrom()->count(), $from);
		self::assertCount($message->getTo()->count(), $to);
		self::assertCount($message->getCc()->count(), $cc);
		self::assertCount($message->getBcc()->count(), $bcc);
		self::assertCount($message->getReplyTo()->count(), $replyTo);

		self::assertTrue($message->getFrom()->has('fromtest@rbschange.fr'));
		self::assertEquals('fromtest', $message->getFrom()->get('fromtest@rbschange.fr')->getName());
		self::assertTrue($message->getFrom()->has('fromtest2@rbschange.fr'));
		self::assertTrue($message->getTo()->has($to[0]));
		self::assertTrue($message->getTo()->has($to[1]));
		self::assertTrue($message->getCc()->has($cc[0]));
		self::assertTrue($message->getCc()->has($cc[1]));
		self::assertTrue($message->getBcc()->has($bcc[0]));
		self::assertTrue($message->getBcc()->has($bcc[1]));
		self::assertTrue($message->getReplyTo()->has($replyTo[0]));
		self::assertTrue($message->getReplyTo()->has($replyTo[1]));

		$parts = $message->getBody()->getParts();
		self::assertEquals(\Zend\Mime\Mime::MULTIPART_ALTERNATIVE, $parts[0]->type);
		self::assertEquals('application/zip', $parts[1]->type);
		self::assertEquals('image/png', $parts[2]->type);
	}

	public function testPrepareFakeMail()
	{
		$application = $this->getApplication();
		$configuration = $application->getConfiguration();

		$mailManager = new FakeMailManager();
		$mailManager->setApplication($application);
		\putenv('CHANGE_PROD_INSTANCE=true');

		$from = ['fromtest@rbschange.fr'];
		$to = ['totest@rbschange.fr'];
		$cc = ['cctest@rbschange.fr'];
		$bcc = ['bcctest@rbschange.fr'];
		$subject = 'Le retour de Chuck Norris';
		$body = 'Chuck Norris est de retour !';

		$message = $mailManager->prepareMessage($from, $to, $subject, $body, null, $cc, $bcc);
		$message = $mailManager->prepareFakeMail($message);

		self::assertTrue($message->getTo()->has('totest@rbschange.fr'));
		self::assertEquals($subject, $message->getSubject());
		self::assertEquals(1, $message->getCc()->count());
		self::assertEquals(1, $message->getBcc()->count());

		\putenv('CHANGE_PROD_INSTANCE=');
		$configuration->addVolatileEntry('Change/Mail/fakemail', 'loic.couturier@rbs.fr');
		$message = $mailManager->prepareFakeMail($message);

		self::assertFalse($message->getTo()->has('totest@rbschange.fr'));
		self::assertTrue($message->getTo()->has('loic.couturier@rbs.fr'));
		self::assertEquals('[FAKE] ' . $subject . ' [To : totest@rbschange.fr][Cc : cctest@rbschange.fr][Bcc : bcctest@rbschange.fr]',
			$message->getSubject());
		self::assertEquals(0, $message->getCc()->count());
		self::assertEquals(0, $message->getBcc()->count());

		$configuration->addVolatileEntry('Change/Mail/fakemail', ['loic.couturier@rbs.fr', 'franck.stauffer@rbs.fr']);
		$to = ['totest@rbschange.fr', 'totest2@rbschange.fr'];

		$message = $mailManager->prepareMessage($from, $to, $subject, $body, null, $cc, $bcc);
		$message = $mailManager->prepareFakeMail($message);

		self::assertTrue($message->getTo()->has('loic.couturier@rbs.fr'));
		self::assertTrue($message->getTo()->has('franck.stauffer@rbs.fr'));
		self::assertEquals('[FAKE] ' . $subject
			. ' [To : totest@rbschange.fr, totest2@rbschange.fr][Cc : cctest@rbschange.fr][Bcc : bcctest@rbschange.fr]', $message->getSubject());
	}
}