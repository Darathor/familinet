<?php
namespace ChangeTests\Change\Mail\TestAssets;

use Change\Mail\MailManager;

/**
 * @name \ChangeTests\Change\Mail\TestAssets\FakeMailManager
 */
class FakeMailManager extends MailManager
{
	/**
	 * @param \Zend\Mail\Message $message
	 * @return \Zend\Mail\Message
	 */
	public function prepareFakeMail($message)
	{
		return parent::prepareFakeMail($message);
	}
}