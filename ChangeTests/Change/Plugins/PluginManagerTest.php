<?php
namespace ChangeTests\Change\Plugins;

use Change\Plugins\Plugin;
use ChangeTests\Change\TestAssets\TestCase;

/**
 * @name \ChangeTests\Change\Plugins\PluginManagerTest
 */
class PluginManagerTest extends TestCase
{
	public function testService()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		self::assertInstanceOf(\Change\Plugins\PluginManager::class, $pluginManager);
	}

	/**
	 * @param Plugin[] $plugins
	 * @param string $type
	 * @param string $vendor
	 * @param string $shortName
	 * @return Plugin|null
	 */
	protected function findPlugin($plugins, $type, $vendor, $shortName)
	{
		$tmpPlugin = new Plugin($type, $vendor, $shortName);
		foreach ($plugins as $plugin)
		{
			if ($tmpPlugin->eq($plugin))
			{
				return $plugin;
			}
		}
		return null;
	}

	public function testScanPlugins()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$plugins = $pluginManager->scanPlugins();
		self::assertNotEmpty($plugins);
		$plugin = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $plugin);

		$plugin2 = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $plugin2);

		return $plugin;
	}

	/**
	 * @depends testScanPlugins
	 * @param Plugin $plugin
	 */
	public function testCompile($plugin)
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$plugins = $pluginManager->compile();
		self::assertEmpty($plugins);
		$pluginManager->register($plugin);

		$plugins = $pluginManager->compile();
		self::assertCount(1, $plugins);
		$p = $plugins[0];
		self::assertInstanceOf(Plugin::class, $p);
		self::assertNotSame($plugin, $p);
		self::assertTrue($plugin->eq($p));
		self::assertInstanceOf(\DateTime::class, $p->getRegistrationDate());

		$p2 = $pluginManager->getModule('Project', 'Tests');
		self::assertSame($p, $p2);
	}

	/**
	 * @depends testCompile
	 */
	public function testModule()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$p = $pluginManager->getModule('Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $p);
		$pluginManager->deregister($p);
		$p2 = $pluginManager->getModule('Project', 'Tests');
		self::assertNull($p2);

		$pluginManager->compile();
		$p3 = $pluginManager->getModule('Project', 'Tests');
		self::assertNull($p3);
	}

	/**
	 * @depends testModule
	 */
	public function testGetUnregisteredPlugins()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		self::assertNull($pluginManager->getModule('Project', 'Tests'));
		self::assertNull($pluginManager->getTheme('Project', 'Tests'));

		$plugins = $pluginManager->getUnregisteredPlugins();
		$plugin = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $plugin);
		$pluginManager->register($plugin);
		$p2 = $pluginManager->getModule('Project', 'Tests');
		self::assertSame($plugin, $p2);
		self::assertNull($pluginManager->getTheme('Project', 'Tests'));

		$plugin = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $plugin);
		$pluginManager->register($plugin);
		$p2 = $pluginManager->getTheme('Project', 'Tests');
		self::assertSame($plugin, $p2);
	}

	/**
	 * @depends testGetUnregisteredPlugins
	 */
	public function testDeregister()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		self::assertNull($pluginManager->getModule('Project', 'Tests'));
		self::assertNull($pluginManager->getTheme('Project', 'Tests'));
		$pluginManager->compile();
		$module = $pluginManager->getModule('Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $module);

		$theme = $pluginManager->getTheme('Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $theme);

		$pluginManager->deregister($module);
		self::assertNull($pluginManager->getModule('Project', 'Tests'));

		$pluginManager->deregister($theme);
		self::assertNull($pluginManager->getTheme('Project', 'Tests'));

		$pluginManager->reset();
		self::assertInstanceOf(Plugin::class, $pluginManager->getModule('Project', 'Tests'));
		self::assertInstanceOf(Plugin::class, $pluginManager->getTheme('Project', 'Tests'));

		$pluginManager->compile();
		self::assertNull($pluginManager->getModule('Project', 'Tests'));
		self::assertNull($pluginManager->getTheme('Project', 'Tests'));
	}

	/**
	 * @depends testDeregister
	 */
	public function testLoad()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$plugins = $pluginManager->getUnregisteredPlugins();
		$module = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $module);
		$pluginManager->register($module);

		$module->setActivated(true);
		$module->setPackage('test');
		$module->setConfiguration(['locked' => true]);

		$pluginManager->update($module);

		$module2 = new Plugin(Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertTrue($module2->eq($module));
		self::assertFalse($module2->getActivated());
		self::assertNull($module2->getPackage());
		self::assertEmpty($module2->getConfiguration());

		$module3 = $pluginManager->load($module2);
		self::assertSame($module2, $module3);
		self::assertTrue($module2->getActivated());
		self::assertEquals('test', $module2->getPackage());
		self::assertEquals(['locked' => true], $module2->getConfiguration());

		$theme = new Plugin(Plugin::TYPE_THEME, 'Project', 'Tests');
		$theme2 = $pluginManager->load($theme);
		self::assertNull($theme2);
	}

	public function testGetPlugin()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$plugins = $pluginManager->compile();
		$module = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $module);
		$p = $pluginManager->getPlugin(Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $p);
		$pluginManager->deregister($p);
		$p2 = $pluginManager->getPlugin(Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertNull($p2);
		$pluginManager->compile();
		$p3 = $pluginManager->getPlugin(Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertNull($p3);

		$plugins = $pluginManager->getUnregisteredPlugins();
		$theme = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $theme);
		$pluginManager->register($theme);
		$p = $pluginManager->getPlugin(Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertInstanceOf(Plugin::class, $p);
		$pluginManager->deregister($p);
		$p2 = $pluginManager->getPlugin(Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertNull($p2);
		$pluginManager->compile();
		$p3 = $pluginManager->getPlugin(Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertNull($p3);
	}

	public function testGetRegisteredPlugins()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();
		$registeredPlugins = $pluginManager->getRegisteredPlugins();
		self::assertEmpty($registeredPlugins);
		$plugins = $pluginManager->getUnregisteredPlugins();
		$module = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertNotNull($module);
		$pluginManager->register($module);
		$plugins = $pluginManager->compile();
		$module = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertNotNull($module);
		$registeredPlugins = $pluginManager->getRegisteredPlugins();
		self::assertCount(1, $registeredPlugins);

		$plugins = $pluginManager->getUnregisteredPlugins();
		$theme = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertNotNull($theme);
		$pluginManager->register($theme);
		$plugins = $pluginManager->compile();
		$theme = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		self::assertNotNull($theme);
		$registeredPlugins = $pluginManager->getRegisteredPlugins();
		self::assertCount(2, $registeredPlugins);
	}

	public function testGetInstalledPlugins()
	{
		$pluginManager = $this->getApplicationServices()->getPluginManager();

		$installedPlugins = $pluginManager->getInstalledPlugins();
		self::assertEmpty($installedPlugins);
		$plugins = $pluginManager->compile();
		$module = $this->findPlugin($plugins, Plugin::TYPE_MODULE, 'Project', 'Tests');
		self::assertNotNull($module);

		$pluginManager->installPlugin($module);
		$installedPlugins = $pluginManager->getInstalledPlugins();
		self::assertCount(1, $installedPlugins);

		// TODO LESS compilation problem (missing vars for image formats).
		// $theme = $this->findPlugin($plugins, Plugin::TYPE_THEME, 'Project', 'Tests');
		// $pluginManager->installPlugin($theme);
		// $installedPlugins = $pluginManager->getInstalledPlugins();
		// self::assertCount(2, $installedPlugins);
	}
}