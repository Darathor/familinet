<?php
namespace ChangeTests\Change\Plugins;

use Change\Plugins\Plugin;

/**
 * @name \ChangeTests\Change\Plugins\PluginTest
 */
class PluginTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		self::assertEquals(Plugin::TYPE_MODULE, $plugin->getType());
		self::assertEquals('Change', $plugin->getVendor());
		self::assertEquals('Tests', $plugin->getShortName());
		self::assertFalse($plugin->getActivated());
		self::assertFalse($plugin->getConfigured());
		self::assertNull($plugin->getRegistrationDate());
		self::assertNull($plugin->getPackage());
		self::assertFalse($plugin->isAvailable());
		self::assertEquals('Change_Tests', $plugin->getName());
	}

	public function testRegistrationDate()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$date = new \DateTime();
		$p2 = $plugin->setRegistrationDate($date);
		self::assertSame($plugin, $p2);
		self::assertSame($date, $plugin->getRegistrationDate());
	}

	public function testType()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		self::assertTrue($plugin->getType() === Plugin::TYPE_MODULE);
		self::assertTrue($plugin->isModule());
		self::assertFalse($plugin->isTheme());

		$plugin = new Plugin(Plugin::TYPE_THEME, 'Change', 'Tests');
		self::assertTrue($plugin->getType() === Plugin::TYPE_THEME);
		self::assertFalse($plugin->isModule());
		self::assertTrue($plugin->isTheme());

		self::assertSame($plugin, $plugin->setType(null));
		self::assertNull($plugin->getType());
		self::assertFalse($plugin->isModule());
		self::assertFalse($plugin->isTheme());
	}

	public function testActivated()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$p2 = $plugin->setActivated(true);
		self::assertSame($plugin, $p2);
		self::assertTrue($plugin->getActivated());
	}

	public function testConfigured()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$p2 = $plugin->setConfigured(true);
		self::assertSame($plugin, $p2);
		self::assertTrue($plugin->getConfigured());
	}

	public function testPackage()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$p2 = $plugin->setPackage('core');
		self::assertSame($plugin, $p2);
		self::assertEquals('core', $plugin->getPackage());
	}

	public function testAvailable()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$plugin->setActivated(true);
		self::assertFalse($plugin->isAvailable());
		$plugin->setConfigured(true);
		self::assertTrue($plugin->isAvailable());
	}

	public function testEq()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		self::assertTrue($plugin->eq($plugin));

		$plugin2 = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		self::assertTrue($plugin->eq($plugin2));

		$plugin2 = new Plugin(Plugin::TYPE_MODULE, 'Change', 'tests2');
		self::assertFalse($plugin->eq($plugin2));
	}

	public function testNamespace()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		self::assertEquals('Change\\Tests', $plugin->getNamespace());

		$plugin = new Plugin(Plugin::TYPE_THEME, 'Change', 'Tests');
		self::assertEquals('Themes\\Change\\Tests', $plugin->getNamespace());
	}

	public function testToArray()
	{
		$plugin = new Plugin(Plugin::TYPE_MODULE, 'Change', 'Tests');
		$expected = [
			'type' => 'module',
			'vendor' => 'Change',
			'shortName' => 'Tests',
			'package' => null,
			'registrationDate' => null,
			'configured' => false,
			'activated' => false,
			'configuration' => [],
			'defaultLCID' => null,
			'extends' => null
		];
		self::assertEquals($expected, $plugin->toArray());
	}
}
