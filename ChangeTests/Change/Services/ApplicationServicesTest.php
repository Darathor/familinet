<?php
namespace ChangeTests\Change\Services;

/**
* @name \ChangeTests\Change\Services\ApplicationServicesTest
*/
class ApplicationServicesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testConstruct()
	{
		$applicationServices = $this->getApplicationServices();
		self::assertInstanceOf('Change\Services\ApplicationServices', $applicationServices);

		self::assertInstanceOf('Change\Logging\Logging', $applicationServices->getLogging());

		self::assertInstanceOf('Change\Db\DbProvider', $applicationServices->getDbProvider());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getDbProvider()->getEventManager());

		self::assertInstanceOf('Change\Transaction\TransactionManager', $applicationServices->getTransactionManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getTransactionManager()->getEventManager());

		self::assertInstanceOf('Change\I18n\I18nManager', $applicationServices->getI18nManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getI18nManager()->getEventManager());

		self::assertInstanceOf('Change\Plugins\PluginManager', $applicationServices->getPluginManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getPluginManager()->getEventManager());

		self::assertInstanceOf('Change\Storage\StorageManager', $applicationServices->getStorageManager());

		self::assertInstanceOf('Change\Mail\MailManager', $applicationServices->getMailManager());

		self::assertInstanceOf('Change\Documents\ModelManager', $applicationServices->getModelManager());

		self::assertInstanceOf('Change\Documents\DocumentManager', $applicationServices->getDocumentManager());

		self::assertInstanceOf('Change\Documents\DocumentCodeManager', $applicationServices->getDocumentCodeManager());

		self::assertInstanceOf('Change\Documents\TreeManager', $applicationServices->getTreeManager());

		self::assertInstanceOf('Change\Documents\Constraints\ConstraintsManager', $applicationServices->getConstraintsManager());

		self::assertInstanceOf('Change\Collection\CollectionManager', $applicationServices->getCollectionManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getCollectionManager()->getEventManager());

		self::assertInstanceOf('Change\Job\JobManager', $applicationServices->getJobManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getJobManager()->getEventManager());

		self::assertInstanceOf('Change\Presentation\Blocks\BlockManager', $applicationServices->getBlockManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getBlockManager()->getEventManager());

		self::assertInstanceOf('Change\Presentation\RichText\RichTextManager', $applicationServices->getRichTextManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getRichTextManager()->getEventManager());

		self::assertInstanceOf('Change\Presentation\Themes\ThemeManager', $applicationServices->getThemeManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getThemeManager()->getEventManager());

		self::assertInstanceOf('Change\Presentation\Templates\TemplateManager', $applicationServices->getTemplateManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getTemplateManager()->getEventManager());

		self::assertInstanceOf('Change\Presentation\Pages\PageManager', $applicationServices->getPageManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getPageManager()->getEventManager());

		self::assertInstanceOf('Change\Workflow\WorkflowManager', $applicationServices->getWorkflowManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getWorkflowManager()->getEventManager());

		self::assertInstanceOf('Change\User\AuthenticationManager', $applicationServices->getAuthenticationManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getAuthenticationManager()->getEventManager());

		self::assertInstanceOf('Change\User\ProfileManager', $applicationServices->getProfileManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getProfileManager()->getEventManager());

		self::assertInstanceOf('Change\Permissions\PermissionsManager', $applicationServices->getPermissionsManager());

		self::assertInstanceOf('Change\Http\OAuth\OAuthManager', $applicationServices->getOAuthManager());

		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getOAuthManager()->getEventManager());

		self::assertInstanceOf('Change\Http\Web\PathRuleManager', $applicationServices->getPathRuleManager());
		self::assertInstanceOf('Change\Events\EventManager', $applicationServices->getPathRuleManager()->getEventManager());
	}
} 