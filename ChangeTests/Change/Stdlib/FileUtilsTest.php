<?php
namespace ChangeTests\Change\Stdlib;

class FileUtilsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testMkdir()
	{
		$path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'FileTest' . DIRECTORY_SEPARATOR . 'testMkdir' . DIRECTORY_SEPARATOR
			. 'Test';
		// Cleanup dir
		$components = explode(DIRECTORY_SEPARATOR, $path);
		@rmdir(implode(DIRECTORY_SEPARATOR, $components));
		array_pop($components);
		@rmdir(implode(DIRECTORY_SEPARATOR, $components));
		array_pop($components);
		@rmdir(implode(DIRECTORY_SEPARATOR, $components));
		\Change\Stdlib\FileUtils::mkdir($path);
		self::assertFileExists($path);
		$this->expectException(\RuntimeException::class);
		\Change\Stdlib\FileUtils::mkdir(__FILE__);
	}

	/**
	 * @depends testMkdir
	 */
	public function testRmdir()
	{
		$path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'Toto' . DIRECTORY_SEPARATOR . 'Tutu';
		\Change\Stdlib\FileUtils::mkdir($path);
		self::assertDirectoryExists($path);
		\Change\Stdlib\FileUtils::rmdir(dirname($path));
		self::assertDirectoryNotExists($path);
		self::assertDirectoryNotExists(dirname($path));

		\Change\Stdlib\FileUtils::mkdir($path);
		self::assertDirectoryExists($path);
		\Change\Stdlib\FileUtils::rmdir(dirname($path), true);
		self::assertDirectoryNotExists($path);
		self::assertDirectoryExists(dirname($path));
	}

	public function testWrite()
	{
		$path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'FileTest' . DIRECTORY_SEPARATOR . 'testWrite' . DIRECTORY_SEPARATOR
			. 'test.txt';
		@unlink($path);
		\Change\Stdlib\FileUtils::write($path, 'test');
		self::assertFileExists($path);
		$content = file_get_contents($path);
		self::assertEquals('test', $content);
		$this->expectException(\RuntimeException::class);
		\Change\Stdlib\FileUtils::write(__DIR__, 'test');
	}

	/**
	 * @depends testWrite
	 */
	public function testRead()
	{
		$path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'FileTest' . DIRECTORY_SEPARATOR . 'testWrite' . DIRECTORY_SEPARATOR
			. 'test.txt';
		self::assertEquals('test', \Change\Stdlib\FileUtils::read($path));
		$this->expectException(\RuntimeException::class);
		\Change\Stdlib\FileUtils::read(__DIR__ . DIRECTORY_SEPARATOR . 'aazeazeazeazeazeaze');
	}
}