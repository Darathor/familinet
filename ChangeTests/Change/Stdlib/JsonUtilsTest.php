<?php
namespace ChangeTests\Change\Stdlib;

class JsonUtilsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testDecode()
	{
		self::assertEquals(['a' => "a b\n\nc"], \Change\Stdlib\JsonUtils::decode("{\"a\": \"	a		b\r\n\r\n\nc\"}"));
	}

	public function testNormalize()
	{
		self::assertEquals('{"a": "a b\n\nc"}', \Change\Stdlib\JsonUtils::normalize("{\"a\": \"	a		b\r\n\r\n\nc\"}"));
	}
}