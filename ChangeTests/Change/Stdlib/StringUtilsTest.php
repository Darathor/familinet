<?php
namespace ChangeTests\Change\Stdlib;

class StringUtilsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testToLower()
	{
		self::assertEquals('abcdefgh0', \Change\Stdlib\StringUtils::toLower('AbCdEFgH0'));
		self::assertEquals('été', \Change\Stdlib\StringUtils::toLower('Été'));
		self::assertEquals('ça', \Change\Stdlib\StringUtils::toLower('Ça'));
	}

	public function testToUpper()
	{
		self::assertEquals('ABCDEFGH0', \Change\Stdlib\StringUtils::toUpper('AbCdEFgh0'));
		self::assertEquals('ÉTÉ', \Change\Stdlib\StringUtils::toUpper('été'));
		self::assertEquals('ÇA', \Change\Stdlib\StringUtils::toUpper('ça'));
	}

	public function testUcfirst()
	{
		self::assertEquals('Été', \Change\Stdlib\StringUtils::ucfirst('été'));
		self::assertEquals('Ça', \Change\Stdlib\StringUtils::ucfirst('ça'));
		self::assertEquals('Čuit', \Change\Stdlib\StringUtils::ucfirst('čuit'));
		self::assertEquals('Alphabet', \Change\Stdlib\StringUtils::ucfirst('alphabet'));
	}

	public function testSubstring()
	{
		self::assertEquals('étç', \Change\Stdlib\StringUtils::subString('abcdétçefgh', 4, 3));
	}

	public function testLength()
	{
		self::assertEquals(3, \Change\Stdlib\StringUtils::length('étç'));
	}

	public function testShorten()
	{
		self::assertEquals('étçétç!', \Change\Stdlib\StringUtils::shorten('étçétçétçétçétçétç', 7, '!'));
	}

	public function testRandom()
	{
		$trial1 = \Change\Stdlib\StringUtils::random(255);
		$trial2 = \Change\Stdlib\StringUtils::random(255);
		// If this test fails, you might be really lucky or facing a bug ;)
		self::assertNotEquals($trial2, $trial1);

		$string = \Change\Stdlib\StringUtils::random(42);
		self::assertEquals(42, \Change\Stdlib\StringUtils::length($string));
	}

	public function testIsEmpty()
	{
		// Empty strings.
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty(null));
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty(''));
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty(' '));
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty('		'));
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty("	\n\n	"));

		// Not empty strings.
		self::assertFalse(\Change\Stdlib\StringUtils::isEmpty('test'));
		self::assertFalse(\Change\Stdlib\StringUtils::isEmpty('1'));

		// Strings-compatible values.
		self::assertFalse(\Change\Stdlib\StringUtils::isEmpty(0));
		self::assertFalse(\Change\Stdlib\StringUtils::isEmpty(125));
		self::assertFalse(\Change\Stdlib\StringUtils::isEmpty(1.25));

		// String-incompatible values.
		/** @noinspection PhpParamsInspection */
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty([]));
		/** @noinspection PhpParamsInspection */
		self::assertTrue(\Change\Stdlib\StringUtils::isEmpty(['test']));
	}

	public function testBeginsWith()
	{
		self::assertTrue(\Change\Stdlib\StringUtils::beginsWith('testing string', 'test'));
		self::assertFalse(\Change\Stdlib\StringUtils::beginsWith('testing string', 'rest'));
		self::assertTrue(\Change\Stdlib\StringUtils::beginsWith('testing string', 'testing string'));
		self::assertFalse(\Change\Stdlib\StringUtils::beginsWith('testing string', 'testing string 123'));

		// Should work with UTF-8 characters.
		self::assertTrue(\Change\Stdlib\StringUtils::beginsWith('chaîne de test', 'chaîn'));
		self::assertFalse(\Change\Stdlib\StringUtils::beginsWith('chaîne de test', 'chain'));
		self::assertFalse(\Change\Stdlib\StringUtils::beginsWith('chaîne de test', 'thaîn'));
		self::assertTrue(\Change\Stdlib\StringUtils::beginsWith('chaîne de test', 'chaîne de test'));
		self::assertFalse(\Change\Stdlib\StringUtils::beginsWith('chaîne de test', 'chaîne de test 123'));
	}

	public function testEndsWith()
	{
		self::assertTrue(\Change\Stdlib\StringUtils::endsWith('testing string', 'ring'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('testing string', 'rind'));
		self::assertTrue(\Change\Stdlib\StringUtils::endsWith('testing string', 'testing string'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('testing string', 'testing string 123'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('testing string', '123 testing string'));

		// Should work with UTF-8 characters.
		self::assertTrue(\Change\Stdlib\StringUtils::endsWith('on a testé', 'esté'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('on a testé', 'este'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('on a testé', 'est'));
		self::assertTrue(\Change\Stdlib\StringUtils::endsWith('on a testé', 'on a testé'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('on a testé', 'on a testé 123'));
		self::assertFalse(\Change\Stdlib\StringUtils::endsWith('on a testé', '123 on a testé'));
	}

	public function testGetSubstitutedString()
	{
		// The order of substitutions does not matter.
		$stringToSubstitute = 'When {inventor} invented the telephone he had 3 missed calls from {missedCallsFrom}.';
		$substitutions = ['missedCallsFrom' => 'Chuck Norris', 'inventor' => 'Alexander Bell'];
		$substitutedString = \Change\Stdlib\StringUtils::getSubstitutedString($stringToSubstitute, $substitutions);
		self::assertEquals('When Alexander Bell invented the telephone he had 3 missed calls from Chuck Norris.', $substitutedString);

		// Substitutions may contain dots and upper cased letters but must start with a lower case letter.
		$stringToSubstitute =
			'Fear of {first.fear.object} is arachnophobia, fear of {secondFearObject} is claustrophobia, fear of {Chuck.Norris} is just called Logic.';
		$substitutions = ['first.fear.object' => 'spiders', 'secondFearObject' => 'tight spaces', 'Chuck.Norris' => 'Chuck Norris'];
		$substitutedString = \Change\Stdlib\StringUtils::getSubstitutedString($stringToSubstitute, $substitutions);
		self::assertEquals('Fear of spiders is arachnophobia, fear of tight spaces is claustrophobia, fear of {Chuck.Norris} is just called Logic.',
			$substitutedString);

		// Substitutions are case sensitive and unknown substitutions are replaced by empty string.
		$stringToSubstitute = '{chuck.Norris} has already been to {where}; that\'s why there are no signs of {life}.';
		$substitutions = ['wHeRe' => 'Mars', 'chuck.Norris' => 'Chuck Norris'];
		$substitutedString = \Change\Stdlib\StringUtils::getSubstitutedString($stringToSubstitute, $substitutions);
		self::assertEquals('Chuck Norris has already been to ; that\'s why there are no signs of .', $substitutedString);

		// Replacement of multiple occurrences.
		$stringToSubstitute = '{who} destroyed {what}, because {who} only recognizes the element of surprise.';
		$substitutions = ['who' => 'Chuck Norris', 'what' => 'the periodic table'];
		$substitutedString = \Change\Stdlib\StringUtils::getSubstitutedString($stringToSubstitute, $substitutions);
		self::assertEquals('Chuck Norris destroyed the periodic table, because Chuck Norris only recognizes the element of surprise.',
			$substitutedString);

		// Specific regexp...
		$stringToSubstitute = '%who% once had a near-%Chuck% experience.';
		$substitutions = ['Chuck' => 'Chuck Norris', 'who' => 'Death'];
		$substitutedString = \Change\Stdlib\StringUtils::getSubstitutedString($stringToSubstitute, $substitutions, '/%([A-Za-z][A-Za-z0-9.]*)%/');
		self::assertEquals('Death once had a near-Chuck Norris experience.', $substitutedString);
	}

	public function testSnakeCase()
	{
		self::assertEquals('snake_case', \Change\Stdlib\StringUtils::snakeCase('SnakeCase'));
		self::assertEquals('snake-case', \Change\Stdlib\StringUtils::snakeCase('SnakeCase', '-'));
		self::assertEquals('snake-cased-string', \Change\Stdlib\StringUtils::snakeCase('---Snake.Cased - string!!!', '-'));
	}

	public function testCamelCase()
	{
		self::assertEquals('CamelCase', \Change\Stdlib\StringUtils::camelCase('camel-case'));
		self::assertEquals('CamelCasedString', \Change\Stdlib\StringUtils::camelCase('---camelCased - string!!!'));
	}
}