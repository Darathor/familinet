<?php
namespace ChangeTests\Change\Storage;

use  Change\Storage\StorageManager;
use ChangeTests\Change\TestAssets\TestCase;

/**
* @name \ChangeTests\Change\Storage\StorageManagerTest
*/
class StorageManagerTest extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		parent::setUpBeforeClass();
		static::clearDB();
		static::initDb();
	}

	public static function tearDownAfterClass(): void
	{
		parent::tearDownAfterClass();
		static::clearDB();
	}

	/**
	 * @return StorageManager
	 */
	protected function getObject()
	{
		return $this->getApplicationServices()->getStorageManager();
	}

	public function testInstance()
	{
		$o = $this->getObject();
		self::assertInstanceOf(StorageManager::class, $o);

		$se = $o->getStorageByName('tmp');
		self::assertInstanceOf(\Change\Storage\Engines\LocalStorage::class, $se);
		self::assertCount(0, $se->getParsedURL());

		$se2 = $o->getStorageByName('tmp');
		self::assertNotSame($se, $se2);

		$se3 = $o->getStorageByStorageURI('change://tmp');
		self::assertInstanceOf(\Change\Storage\Engines\LocalStorage::class, $se3);
		self::assertEquals(array ('scheme' => 'change', 'host' => 'tmp', 'path' => '/'), $se3->getParsedURL());

		$storageURI = 'change://tmp/e/aa.txt';
		$se4 = $o->getStorageByStorageURI($storageURI);
		self::assertInstanceOf(\Change\Storage\Engines\LocalStorage::class, $se4);
		self::assertEquals(array ('scheme' => 'change', 'host' => 'tmp', 'path' => '/e/aa.txt'), $se4->getParsedURL());

		$localFileName = $this->getApplication()->getWorkspace()->tmpPath('Storage', 'e', 'aa.txt');
		$fc = \str_repeat('abcdef ', 5000);
		if (\file_exists($localFileName))
		{
			\unlink($localFileName);
		}

		self::assertFileNotExists($storageURI);
		\file_put_contents($storageURI, $fc);

		self::assertFileExists($storageURI);
		self::assertFileExists($localFileName);

		self::assertEquals('text/plain', $o->getMimeType($storageURI));

		$dt = new \DateTime('2013-08-19 06:44:39');

		\touch($storageURI, $dt->getTimestamp());

		self::assertEquals($dt->getTimestamp(), \filemtime($storageURI));

		self::assertTrue(\unlink($storageURI));

		self::assertFileNotExists($storageURI);
		self::assertFileNotExists($localFileName);
	}

	public function testMimeType()
	{
		$o = $this->getObject();
		$changeURI = 'change://tmp/cc.gif';
		$localPath = __DIR__ . '/Assets/cc.gif';
		\copy($localPath, $changeURI);
		$itemInfo = $o->getItemInfo($changeURI);
		self::assertInstanceOf(\Change\Storage\ItemInfo::class, $itemInfo);
		self::assertTrue($itemInfo->isFile());
		self::assertTrue($itemInfo->isReadable());
		self::assertEquals('image/gif', $itemInfo->getMimeType());

		$changeURI = 'change://tmp/cc.png';
		$localPath = __DIR__ . '/Assets/cc.png';
		\copy($localPath, $changeURI);
		$itemInfo = $o->getItemInfo($changeURI);
		self::assertInstanceOf(\Change\Storage\ItemInfo::class, $itemInfo);
		self::assertTrue($itemInfo->isFile());
		self::assertTrue($itemInfo->isReadable());
		self::assertEquals('image/png', $itemInfo->getMimeType());

		$changeURI = 'change://tmp/cc.jpg';
		$localPath = __DIR__ . '/Assets/cc.jpg';
		\copy($localPath, $changeURI);
		$itemInfo = $o->getItemInfo($changeURI);
		self::assertInstanceOf(\Change\Storage\ItemInfo::class, $itemInfo);
		self::assertTrue($itemInfo->isFile());
		self::assertTrue($itemInfo->isReadable());
		self::assertEquals('image/jpeg', $itemInfo->getMimeType());
	}
}