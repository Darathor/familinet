<?php

namespace ChangeTests\Change\TestAssets;

/**
 * @name \ChangeTests\Change\TestAssets\Application
 * @api
 */
class Application extends \Change\Application
{
	public function registerCoreAutoload()
	{
		$classLoader = parent::registerCoreAutoload();
		if (!($classLoader instanceof \Composer\Autoload\ClassLoader))
		{
			$classLoader = new \Composer\Autoload\ClassLoader();
			$classLoader->register(true);
		}
		$changeTestsPath = \dirname(__DIR__, 2);
		$classLoader->setPsr4('ChangeTests\\', [$changeTestsPath]);
		$classLoader->setPsr4('Compilation\\', [$this->getWorkspace()->compilationPath()]);
		$classLoader->setPsr4('Project\\Tests\\', [$changeTestsPath . '/UnitTestWorkspace/App/Modules/Project/Tests']);
		$classLoader->setPsr4('Themes\\Project\\Tests\\', [$changeTestsPath . '/UnitTestWorkspace/App/Themes/Project/Tests']);
	}

	/**
	 * @return \Change\Workspace
	 */
	public function getWorkspace()
	{
		if (!$this->workspace)
		{
			$this->workspace = new \ChangeTests\Change\TestAssets\Workspace();
		}
		return $this->workspace;
	}

	/**
	 * Get all the project-level config files paths, in the correct order
	 *
	 * @return array string
	 * @api
	 */
	public function getProjectConfigurationPaths()
	{
		$result = parent::getProjectConfigurationPaths();
		if (isset($_ENV['TestConfigFile']) && $_ENV['TestConfigFile'] !== '')
		{
			$testConfigFile = $this->getWorkspace()->appPath('Config', $_ENV['TestConfigFile']);
			$result['TEST'] = $testConfigFile;
		}
		return $result;
	}

	public function shutdown()
	{
		if ($this->sharedEventManager)
		{
			$this->sharedEventManager->clearListeners('*');
			$this->sharedEventManager->clearListeners('Documents');
		}
	}

	/**
	 * @return \Zend\Stdlib\Parameters
	 */
	public function initServices()
	{
		return $this->getServices();
	}
}
