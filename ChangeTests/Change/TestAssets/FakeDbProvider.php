<?php
namespace ChangeTests\Change\TestAssets;

/**
 * @name \ChangeTests\Change\TestAssets\FakeDbProvider
 */
class FakeDbProvider extends \Change\Db\DbProvider
{
	/**
	 * @var bool
	 */
	protected $transaction = false;

	/**
	 * @var int[][]
	 */
	public $lastInsertedIds = [];

	/**
	 * @var array[]
	 */
	public $queryResults = [];

	/**
	 * @var int[][]
	 */
	public $rowCounts = [];

	/**
	 * Check that each expected query was really called and that there is no more open transaction.
	 */
	public function assertClean()
	{
		\ChangeTests\Change\TestAssets\TestCase::assertFalse($this->transaction, 'There should not be any open transaction.');
		\ChangeTests\Change\TestAssets\TestCase::assertEquals([], $this->lastInsertedIds, 'One or more insertion didn\'t occur.');
		\ChangeTests\Change\TestAssets\TestCase::assertEquals([], $this->queryResults, 'One or more select didn\'t occur.');
		\ChangeTests\Change\TestAssets\TestCase::assertEquals([], $this->rowCounts, 'One or more insertion didn\'t occur.');
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'fake';
	}

	/**
	 * @return \Change\Db\InterfaceSchemaManager|void
	 */
	public function getSchemaManager()
	{
		throw new \LogicException('Not implemented');
	}

	/**
	 * @return void
	 */
	public function openTransaction()
	{
		$this->transaction = true;
	}

	/**
	 * @return void
	 */
	public function closeTransaction()
	{
		$this->transaction = false;
	}

	/**
	 * @return bool
	 */
	public function inTransaction()
	{
		return $this->transaction;
	}

	/**
	 * @return void
	 */
	public function revertTransaction()
	{
		$this->transaction = false;
	}

	/**
	 * @param string $tableName
	 * @return integer
	 */
	public function getLastInsertId($tableName)
	{
		if (!isset($this->lastInsertedIds[$tableName][0]))
		{
			throw new \RuntimeException('Unexpected lastInsertedIds for table ' . $tableName);
		}
		$result = \array_shift($this->lastInsertedIds[$tableName]);
		if (!$this->lastInsertedIds[$tableName])
		{
			unset($this->lastInsertedIds[$tableName]);
		}
		return $result;
	}

	/**
	 * @param \Change\Db\Query\InterfaceSQLFragment $fragment
	 * @return string
	 * @api
	 */
	public function buildSQLFragment(\Change\Db\Query\InterfaceSQLFragment $fragment)
	{
		return $fragment->toSQL92String();
	}

	/**
	 * @param \Change\Db\Query\SelectQuery $selectQuery
	 * @param callable|null $rowsConverter
	 * @return array
	 */
	public function getQueryResultsArray(\Change\Db\Query\SelectQuery $selectQuery, $rowsConverter = null)
	{
		$buildSQLFragment = $this->buildSQLFragment($selectQuery);
		if (!isset($this->queryResults[$buildSQLFragment][0]))
		{
			throw new \RuntimeException('Unexpected queryResults ' . $buildSQLFragment);
		}
		$rows = \array_shift($this->queryResults[$buildSQLFragment]);
		if (!$this->queryResults[$buildSQLFragment])
		{
			unset($this->queryResults[$buildSQLFragment]);
		}

		if ($rows && \is_callable($rowsConverter))
		{
			$rows = $rowsConverter($rows);
		}
		return $rows;
	}

	/**
	 * @param \Change\Db\Query\AbstractQuery $query
	 * @return integer
	 */
	public function executeQuery(\Change\Db\Query\AbstractQuery $query)
	{
		$buildSQLFragment = $this->buildSQLFragment($query);
		if (!isset($this->rowCounts[$buildSQLFragment][0]))
		{
			throw new \RuntimeException('Unexpected rowCounts ' . $buildSQLFragment);
		}
		$result = \array_shift($this->rowCounts[$buildSQLFragment]);
		if (!$this->rowCounts[$buildSQLFragment])
		{
			unset($this->rowCounts[$buildSQLFragment]);
		}
		return $result;
	}

	/**
	 * @param mixed $value
	 * @param integer $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public function phpToDB($value, $scalarType)
	{
		switch ($scalarType)
		{
			case \Change\Db\ScalarType::BOOLEAN :
				return $value ? 1 : 0;
			case \Change\Db\ScalarType::INTEGER :
				if ($value !== null)
				{
					if (\is_object($value) && \is_callable([$value, 'getId']))
					{
						$value = $value->getId();
					}
					return (int)$value;
				}
				break;
			case \Change\Db\ScalarType::DECIMAL :
				if ($value !== null)
				{
					return (float)$value;
				}
				break;
			case \Change\Db\ScalarType::DATETIME :
				if ($value instanceof \DateTime)
				{
					$value->setTimezone(new \DateTimeZone('UTC'));
					return $value->format('Y-m-d H:i:s');
				}
				break;
		}
		return $value;
	}

	/**
	 * @param mixed $value
	 * @param integer $scalarType \Change\Db\ScalarType::*
	 * @return mixed
	 */
	public function dbToPhp($value, $scalarType)
	{
		switch ($scalarType)
		{
			case \Change\Db\ScalarType::BOOLEAN :
				return ((int)$value) > 0;
			case \Change\Db\ScalarType::INTEGER :
				if ($value !== null)
				{
					return (int)$value;
				}
				break;
			case \Change\Db\ScalarType::DECIMAL :
				if ($value !== null)
				{
					return (float)$value;
				}
				break;
			case \Change\Db\ScalarType::DATETIME :
				if ($value !== null)
				{
					return \DateTime::createFromFormat('Y-m-d H:i:s', $value, new \DateTimeZone('UTC'));
				}
				break;
		}
		return $value;
	}

	/**
	 * @return void
	 */
	public function closeConnection()
	{
		// TODO: Implement closeConnection() method.
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function beginTransaction($event = null)
	{
		// TODO: Implement beginTransaction() method.
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function commit($event)
	{
		// TODO: Implement commit() method.
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return void
	 */
	public function rollBack($event)
	{
		// TODO: Implement rollBack() method.
	}
}