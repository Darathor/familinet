<?php
namespace ChangeTests\Change\TestAssets;

/**
 * @name \ChangeTests\Change\TestAssets\Workspace
 * @api
 */
class Workspace extends \Change\Workspace
{
	public function clear()
	{
		\Change\Stdlib\FileUtils::rmdir(PROJECT_HOME . '/ChangeTests/UnitTestWorkspace/Compilation', true);
		\Change\Stdlib\FileUtils::rmdir(PROJECT_HOME . '/ChangeTests/UnitTestWorkspace/tmp', true);
	}

	/**
	 *
	 * @return string
	 */
	protected function appBase()
	{
		return PROJECT_HOME . '/ChangeTests/UnitTestWorkspace/App';
	}

	/**
	 *
	 * @return string
	 */
	protected function compilationBase()
	{
		return PROJECT_HOME . '/ChangeTests/UnitTestWorkspace/Compilation';
	}

/**
	 * Build a relative path to the project's modules folder (App/Modules/)
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectModulesRelativePath(string ...$args)
	{
		\array_unshift($args, 'ChangeTests', 'UnitTestWorkspace', 'App', 'Modules');
		return $this->composePath(...$args);
	}

	/**
	 * Build a relative path to the project's modules folder (App/Modules/)
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function projectThemesRelativePath(string ...$args)
	{
		\array_unshift($args, 'ChangeTests', 'UnitTestWorkspace', 'App', 'Themes');
		return $this->composePath(...$args);
	}


	/**
	 * @api
	 * @param string[] $args
	 * @return string
	 */
	public function tmpPath(string ...$args)
	{
		\array_unshift($args, 'ChangeTests', 'UnitTestWorkspace', 'tmp');
		return $this->projectPath(...$args);
	}

}