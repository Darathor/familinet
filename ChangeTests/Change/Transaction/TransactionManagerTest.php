<?php
namespace ChangeTests\Change\Transaction;

use Change\Transaction\TransactionManager;

/**
 * @name \ChangeTests\Change\Transaction\TransactionManagerTest
 */
class TransactionManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{

	public function testTransaction()
	{
		$tm = $this->getApplicationServices()->getTransactionManager();
		self::assertFalse($tm->started());
		self::assertEquals(0, $tm->count());
		$l = new Listener_5421181245();
		$tm->getEventManager()->attach(TransactionManager::EVENT_BEGIN, array($l, 'begin'));
		$tm->getEventManager()->attach(TransactionManager::EVENT_COMMIT, array($l, 'commit'));
		$tm->getEventManager()->attach(TransactionManager::EVENT_ROLLBACK, array($l, 'rollback'));

		$tm->begin();

		self::assertEquals('begin', $l->type);
		self::assertTrue($l->primary);
		self::assertEquals(1, $l->count);
		self::assertSame($tm, $l->event->getTarget());

		$tm->begin();

		self::assertEquals('begin', $l->type);
		self::assertFalse($l->primary);
		self::assertEquals(2, $l->count);

		$tm->commit();

		self::assertEquals('commit', $l->type);
		self::assertFalse($l->primary);
		self::assertEquals(2, $l->count);


		$tm->commit();

		self::assertEquals('commit', $l->type);
		self::assertTrue($l->primary);
		self::assertEquals(1, $l->count);
	}

	public function testRollBack()
	{
		$tm = $this->getApplicationServices()->getTransactionManager();
		self::assertFalse($tm->started());
		self::assertEquals(0, $tm->count());
		$l = new Listener_5421181245();
		$tm->getEventManager()->attach(TransactionManager::EVENT_BEGIN, array($l, 'begin'));
		$tm->getEventManager()->attach(TransactionManager::EVENT_COMMIT, array($l, 'commit'));
		$tm->getEventManager()->attach(TransactionManager::EVENT_ROLLBACK, array($l, 'rollback'));

		$tm->begin();

		self::assertEquals('begin', $l->type);
		self::assertTrue($l->primary);
		self::assertEquals(1, $l->count);
		self::assertSame($tm, $l->event->getTarget());

		$tm->begin();

		self::assertEquals('begin', $l->type);
		self::assertFalse($l->primary);
		self::assertEquals(2, $l->count);

		try
		{
			$tm->rollBack();
			self::fail('RollbackException: Transaction cancelled');
		}
		catch (\Change\Transaction\RollbackException $e)
		{
			self::assertEquals(120000, $e->getCode());
		}

		self::assertEquals('rollback', $l->type);
		self::assertFalse($l->primary);
		self::assertEquals(2, $l->count);

		try
		{
			$tm->commit();
			self::fail('LogicException: Transaction is dirty');
		}
		catch (\LogicException $e)
		{
			self::assertEquals(121002, $e->getCode());
		}

		$tm->rollBack();
		self::assertEquals('rollback', $l->type);
		self::assertTrue($l->primary);
		self::assertEquals(1, $l->count);
	}
}

class Listener_5421181245 {

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var \Zend\EventManager\Event
	 */
	public $event;

	/**
	 * @var integer
	 */
	public $count;

	/**
	 * @var boolean
	 */
	public $primary;


	public function begin(\Zend\EventManager\Event $event)
	{
		$this->type = 'begin';
		$this->event = $event;
		$this->count = $event->getParam('count');
		$this->primary = $event->getParam('primary');
	}

	public function commit(\Zend\EventManager\Event $event)
	{
		$this->type = 'commit';
		$this->event = $event;
		$this->count = $event->getParam('count');
		$this->primary = $event->getParam('primary');
	}

	public function rollback(\Zend\EventManager\Event $event)
	{
		$this->type = 'rollback';
		$this->event = $event;
		$this->count = $event->getParam('count');
		$this->primary = $event->getParam('primary');
	}
}
