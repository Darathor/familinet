<?php
namespace ChangeTests\Change\User;

/**
 * @name \ChangeTests\Change\User\AuthenticationManagerTest
 */
class AuthenticationManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Change\User\AuthenticationManager
	 */
	protected function getAuthenticationManager()
	{
		return $this->getApplicationServices()->getAuthenticationManager();
	}

	public function testConstruct()
	{
		self::assertInstanceOf(\Change\User\AuthenticationManager::class, $this->getAuthenticationManager());
	}

	public function testLogin()
	{
		$am = $this->getAuthenticationManager();

		self::assertNull($am->login('testLogin', 'testPWD', 'testRealm'));

		$callback = static function (\Zend\EventManager\Event $event) {
			if ($event->getParam('login') === 'testLogin' && $event->getParam('password') === 'testPWD' && $event->getParam('realm') === 'testRealm')
			{
				$event->setParam('user', new User_2124512348());
			}
		};

		$toDetach = $am->getEventManager()->attach(\Change\User\AuthenticationManager::EVENT_LOGIN, $callback);
		$u = $am->login('testLogin', 'testPWD', 'testRealm');
		self::assertInstanceOf(\Change\User\UserInterface::class, $u);
		self::assertEquals('User_2124512348', $u->getName());
		$am->getEventManager()->detach($toDetach);

		self::assertNull($am->login('testLogin', 'testPWD', 'testRealm'));
	}

	public function testCurrentUser()
	{
		$am = $this->getAuthenticationManager();
		self::assertInstanceOf(\Change\User\AnonymousUser::class, $am->getCurrentUser());
		self::assertFalse($am->getCurrentUser()->authenticated());
		$u = new User_2124512348();
		$am->setCurrentUser($u);

		self::assertSame($u, $am->getCurrentUser());

		$am->setCurrentUser(null);
		self::assertInstanceOf(\Change\User\AnonymousUser::class, $am->getCurrentUser());
	}
}

class User_2124512348 implements \Change\User\UserInterface // NOSONAR
{
	/**
	 * @return integer
	 */
	public function getId()
	{
		return 1;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'User_2124512348';
	}

	/**
	 * @return integer[]
	 */
	public function getGroupIds()
	{
		return [];
	}

	/**
	 * @return \Change\User\GroupInterface[]
	 */
	public function getGroups()
	{
		return [];
	}

	/**
	 * @return boolean
	 */
	public function authenticated()
	{
		return true;
	}
}