<?php
namespace ChangeTests\Change\Workflow;

use Change\Workflow\Engine;
use ChangeTests\Change\Workflow\TestAssets\Workflow;
use ChangeTests\Change\Workflow\TestAssets\WorkflowInstance;

/**
* @name \ChangeTests\Change\Workflow\EngineTest
*/
class EngineTest extends \ChangeTests\Change\TestAssets\TestCase
{

	/**
	 * @return $o
	 */
	protected function getObject($dt = null)
	{
		return new Engine(new WorkflowInstance(new Workflow()), $dt);
	}

	public function testConstruct()
	{
		$dt = new \DateTime('2013-06-07 00:00:00');
		$o = $this->getObject($dt);

		self::assertInstanceOf('\ChangeTests\Change\Workflow\TestAssets\WorkflowInstance', $o->getWorkflowInstance());
		self::assertEquals($dt, $o->getDateTime());

/*
		try
		{
			$o->getStartPlace();
			self::fail('throw \RuntimeException');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('Invalid Workflow type', $e->getMessage());
		}
*/
	}

	public function testGetStartPlace()
	{
		$o = $this->getObject();
		self::assertNull($o->getStartPlace());

		/* @var $w TestAssets\Workflow */
		$w = $o->getWorkflowInstance()->getWorkflow();
		$w->initMinimalValid();

		$p = $o->getStartPlace();
		self::assertInstanceOf('\ChangeTests\Change\Workflow\TestAssets\Place', $p);
		self::assertEquals(1, $p->getId());
	}

	public function testEnableToken()
	{
		$o = $this->getObject();
		/* @var $i TestAssets\WorkflowInstance */
		$i = $o->getWorkflowInstance();
		$w = $i->getWorkflow();
		$w->initMinimalValid();

		/* @var $p TestAssets\Place */
		$p = $o->getStartPlace();

		try
		{
			$p->workflow = null;
			$o->enableToken($p);

			self::fail('throw \RuntimeException');
		}
		catch (\RuntimeException $e)
		{
			self::assertEquals('Invalid Place Workflow', $e->getMessage());
			$p->workflow = $w;
		}

		$t = $o->enableToken($p);
		self::assertInstanceOf('\ChangeTests\Change\Workflow\TestAssets\Token', $t);
		self::assertEquals(TestAssets\WorkflowInstance::STATUS_CLOSED, $i->getStatus());
		self::assertInstanceOf('\DateTime', $i->getEndDate());
	}

	public function testGetEnabledWorkItemByTaskId()
	{
		$o = $this->getObject();
		/* @var $i TestAssets\WorkflowInstance */
		$i = $o->getWorkflowInstance();
		$w = $i->getWorkflow();
		$w->initMinimalValid();

		/* @var $p TestAssets\Place */
		$p = $o->getStartPlace();

		/* @var $transition TestAssets\Transition */
		$transition = $w->getItemById(3);
		$transition->trigger = TestAssets\Transition::TRIGGER_MSG;

		$o->enableToken($p);
		$t = $i->item[0];
		self::assertEquals(TestAssets\Token::STATUS_FREE, $t->getStatus());

		$wi = $i->item[1];
		self::assertEquals(TestAssets\WorkItem::STATUS_ENABLED, $wi->getStatus());
		$id = $wi->getTaskId();
		self::assertEquals(1, $id);

		self::assertNull($o->getEnabledWorkItemByTaskId(-1));
		self::assertSame($wi, $o->getEnabledWorkItemByTaskId(1));
		self::assertInstanceOf('\DateTime', $wi->getEnabledDate());
		self::assertNull($wi->getCanceledDate());
		self::assertNull($wi->getFinishedDate());
	}

	public function testFiredWorkItem()
	{
		$o = $this->getObject();
		/* @var $i TestAssets\WorkflowInstance */
		$i = $o->getWorkflowInstance();
		$w = $i->getWorkflow();
		$w->initMinimalValid();

		/* @var $p TestAssets\Place */
		$p = $o->getStartPlace();

		/* @var $transition TestAssets\Transition */
		$transition = $w->getItemById(3);
		$transition->trigger = TestAssets\Transition::TRIGGER_MSG;

		$o->enableToken($p);
		$wi = $o->getEnabledWorkItemByTaskId(1);
		self::assertEquals(TestAssets\WorkItem::STATUS_ENABLED, $wi->getStatus());

		$o->firedWorkItem($wi);
		self::assertCount(3, $i->item);
		$to = $i->item[0];
		self::assertEquals(TestAssets\Token::STATUS_CONSUMED, $to->getStatus());
		self::assertInstanceOf('\DateTime', $to->getConsumedDate());
		self::assertSame($wi, $i->execute[0]);

		self::assertEquals(TestAssets\WorkItem::STATUS_FINISHED, $wi->getStatus());
		self::assertInstanceOf('\DateTime', $wi->getFinishedDate());
		self::assertEquals(TestAssets\WorkflowInstance::STATUS_CLOSED, $i->getStatus());
		self::assertInstanceOf('\DateTime', $i->getEndDate());

		$to = $i->item[2];
		self::assertEquals(TestAssets\Token::STATUS_CONSUMED, $to->getStatus());
		self::assertInstanceOf('\DateTime', $to->getConsumedDate());
		self::assertSame($wi, $i->execute[0]);
	}
}