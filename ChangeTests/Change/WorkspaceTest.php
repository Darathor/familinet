<?php
namespace ChangeTests\Change;

use Change\Workspace;

class WorkspaceTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testAppPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'App', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->appPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testCompilationPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'Compilation', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->compilationPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testProjectPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->projectPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testProjectModulesPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'App', 'Modules', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->projectModulesPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testProjectModulesRelativePath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, ['App', 'Modules', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->projectModulesRelativePath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testPluginsModulesPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'Plugins', 'Modules', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->pluginsModulesPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testPluginsModulesRelativePath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, ['Plugins', 'Modules', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->pluginsModulesRelativePath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testProjectThemesPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'App', 'Themes', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->projectThemesPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testProjectThemesRelativePath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, ['App', 'Themes', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->projectThemesRelativePath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testPluginsThemesPath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, [PROJECT_HOME, 'Plugins', 'Themes', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->pluginsThemesPath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}

	public function testPluginsThemesRelativePath()
	{
		$workspace = new Workspace();
		$expected = implode(DIRECTORY_SEPARATOR, ['Plugins', 'Themes', 'Dir1', 'Dir2', 'Dir3', 'File.php']);
		self::assertEquals($expected, $workspace->pluginsThemesRelativePath('Dir1', 'Dir2', 'Dir3', 'File.php'));
	}
}