<?php
namespace ChangeTests\Perf;

class SetupTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::clearDB();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->initServices($this->getApplication());
	}

	/**
	 * @return \Change\Events\EventManager
	 */
	protected function getCommandsEventManager()
	{
		return $this->getApplication()->getNewEventManager('Commands', 'Change/Events/Commands');
	}

	/**
	 * @param \Change\Application $application
	 * @param \Zend\EventManager\EventManager $eventManager
	 * @param $cmd
	 * @param $arguments
	 * @return \Change\Commands\Events\CommandResponseInterface
	 */
	protected function executeCommand($application, $eventManager, $cmd, $arguments)
	{
		$cmdEvent = new \Change\Commands\Events\Event($cmd, $application, $arguments);

		$response = new \Change\Commands\Events\RestCommandResponse();
		$cmdEvent->setCommandResponse($response);

		$eventManager->triggerEvent($cmdEvent);
		return $cmdEvent->getCommandResponse();
	}

	public function testSetDocumentRoot()
	{
		$webBaseDirectory = 'ChangeTests/UnitTestWorkspace/www';
		$cmd = 'change:set-document-root';
		$arguments = ['webBaseDirectory' => $webBaseDirectory, 'webBaseURLPath' => ''];

		$application = $this->getApplication();
		$webBasePath = $application->getWorkspace()->composeAbsolutePath($webBaseDirectory);
		\Change\Stdlib\FileUtils::mkdir($webBasePath);

		$eventManager = $this->getCommandsEventManager();
		$commandResponse = $this->executeCommand($application, $eventManager, $cmd, $arguments);
		self::assertInstanceOf(\Change\Commands\Events\CommandResponseInterface::class, $commandResponse);
		$result = $commandResponse->toArray();
		self::assertArrayHasKey('info', $result);
		self::assertGreaterThanOrEqual(1, \count($result['info']));
		self::assertStringStartsWith('Web base Directory', $result['info'][0]);
	}

	public function testGenerateDbSchema()
	{
		$application = $this->getApplication();
		$eventManager = $this->getCommandsEventManager();
		$cmd = 'change:generate-db-schema';
		$arguments = ['with-modules' => false];
		$commandResponse = $this->executeCommand($application, $eventManager, $cmd, $arguments);
		self::assertInstanceOf(\Change\Commands\Events\CommandResponseInterface::class, $commandResponse);
		$result = $commandResponse->toArray();
		self::assertArrayHasKey('info', $result);
		self::assertCount(1, $result['info']);
		self::assertStringStartsWith('Change DB schema generated', $result['info'][0]);
	}

	public function testRegisterPlugins()
	{
		$application = $this->getApplication();
		$eventManager = $this->getCommandsEventManager();
		$cmd = 'change:register-plugin';
		$arguments = ['all' => true];
		$commandResponse = $this->executeCommand($application, $eventManager, $cmd, $arguments);
		self::assertInstanceOf(\Change\Commands\Events\CommandResponseInterface::class, $commandResponse);
		$result = $commandResponse->toArray();
		self::assertArrayHasKey('info', $result);
		self::assertGreaterThan(2, \count($result['info']));
		self::assertArrayNotHasKey('error', $result);
	}

	public function testInstallCorePackage()
	{
		$application = $this->getApplication();
		$eventManager = $this->getCommandsEventManager();
		$cmd = 'change:install-package';
		$arguments = ['vendor' => 'Rbs', 'name' => 'Core', 'steps' => 'All'];
		$commandResponse = $this->executeCommand($application, $eventManager, $cmd, $arguments);
		self::assertInstanceOf(\Change\Commands\Events\CommandResponseInterface::class, $commandResponse);
		$result = $commandResponse->toArray();
		self::assertArrayHasKey('info', $result);
		self::assertGreaterThan(1, \count($result['info']));
		self::assertArrayNotHasKey('error', $result);
	}

	public function testInstallCommonTheme()
	{
		$application = $this->getApplication();
		$eventManager = $this->getCommandsEventManager();
		$cmd = 'change:install-plugin';
		$arguments = ['type' => 'theme', 'vendor' => 'Rbs', 'name' => 'Common', 'steps' => 'All'];
		$commandResponse = $this->executeCommand($application, $eventManager, $cmd, $arguments);
		self::assertInstanceOf(\Change\Commands\Events\CommandResponseInterface::class, $commandResponse);
		$result = $commandResponse->toArray();
		self::assertArrayHasKey('info', $result);
		self::assertGreaterThan(0, \count($result['info']));
		self::assertArrayNotHasKey('error', $result);
	}
}