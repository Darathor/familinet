<?php
namespace ChangeTests\Rbs\Collection\Events;

class CollectionResolverTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->getApplicationServices()->getTransactionManager()->begin();
	}

	protected function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->commit();
		parent::tearDown();
	}

	public function testGetCollection()
	{
		$this->createFiveFakeCollections();

		$collectionResolver = new \Rbs\Collection\Events\CollectionResolver();
		$event = new \Change\Events\Event();
		$event->setParams($this->getDefaultEventArguments());
		$event->setParam('code', 'rbsCollectionTest1');
		$collectionResolver->getCollection($event);
		$collection = $event->getParam('collection');

		/* @var $collection \Rbs\Collection\Documents\Collection */
		self::assertEquals('collection1', $collection->getLabel());
	}

	/**
	 * @depends testGetCollection
	 */
	public function testGetCodes()
	{
		$event = new \Change\Events\Event();
		$event->setParams($this->getDefaultEventArguments());
		$collectionResolver = new \Rbs\Collection\Events\CollectionResolver();
		$collectionResolver->getCodes($event);
		$codes = $event->getParam('codes');
		$countCodes = count($codes);
		self::assertGreaterThanOrEqual(5, $countCodes);

		self::assertContains('rbsCollectionTest0', $codes);
		self::assertContains('rbsCollectionTest1', $codes);
		self::assertContains('rbsCollectionTest2', $codes);
		self::assertContains('rbsCollectionTest3', $codes);
		self::assertContains('rbsCollectionTest4', $codes);

		//Test adding of five new collections (need to be merged with current collections)
		$this->createFiveFakeCollections(5);
		$collectionResolver->getCodes($event);
		$codes = $event->getParam('codes');

		self::assertCount($countCodes + 5, $codes);

		self::assertContains('rbsCollectionTest5', $codes);
		self::assertContains('rbsCollectionTest6', $codes);
		self::assertContains('rbsCollectionTest7', $codes);
		self::assertContains('rbsCollectionTest8', $codes);
		self::assertContains('rbsCollectionTest9', $codes);
	}

	/**
	 * @param int $beginIndex
	 */
	protected function createFiveFakeCollections($beginIndex = 0)
	{
		$dm = $this->getApplicationServices()->getDocumentManager();
		$end = $beginIndex + 5;
		for ($i = $beginIndex; $i < $end; $i++)
		{

			/* @var $collection \Rbs\Collection\Documents\Collection */
			$collection = $dm->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
			$collection->setCode('rbsCollectionTest' . $i);
			$collection->setLabel('collection' . $i);
			$collection->save();
		}
	}

}