<?php
namespace ChangeTests\Rbs\Generic;

class GenericServicesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @return \Rbs\Generic\GenericServices
	 */
	protected function getGenericServices()
	{
		return new \Rbs\Generic\GenericServices($this->getApplication(),  $this->getApplicationServices());
	}

	public function testGetInstance()
	{
		$genericServices = $this->getGenericServices();
		self::assertInstanceOf(\Rbs\Geo\GeoManager::class, $genericServices->getGeoManager());
		self::assertInstanceOf(\Rbs\Media\Avatar\AvatarManager::class, $genericServices->getAvatarManager());
		self::assertInstanceOf(\Rbs\Seo\SeoManager::class, $genericServices->getSeoManager());
		self::assertInstanceOf(\Rbs\Simpleform\Field\FieldManager::class, $genericServices->getFieldManager());
		self::assertInstanceOf(\Rbs\Simpleform\Security\SecurityManager::class, $genericServices->getSecurityManager());
		self::assertInstanceOf(\Rbs\Admin\AdminManager::class, $genericServices->getAdminManager());
	}
} 