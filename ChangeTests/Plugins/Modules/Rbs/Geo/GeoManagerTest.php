<?php
namespace ChangeTests\Rbs\Geo;

class GeoManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	protected function attachSharedListener(\Zend\EventManager\SharedEventManager $sharedEventManager)
	{
		parent::attachSharedListener($sharedEventManager);
		$this->attachGenericServicesSharedListener($sharedEventManager);
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->initServices($this->getApplication());
	}

	public function testGetCountriesByZoneCode()
	{
		$genericServices = $this->genericServices;
		$geoManager = $genericServices->getGeoManager();
		self::assertInstanceOf(\Rbs\Geo\GeoManager::class, $geoManager);

		// No zone code specified.
		self::assertCount(0, $geoManager->getCountriesByZoneCode(null));

		$documentManager = $this->getApplicationServices()->getDocumentManager();
		$tm = $this->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			/** @var \Rbs\Geo\Documents\Country $FR */
			$FR = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Country');
			$FR->setCode('FR');
			$FR->setActive(true);
			$FR->setLabel('France');
			$FR->save();

			/** @var \Rbs\Geo\Documents\Country $DE */
			$DE = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Country');
			$DE->setCode('DE');
			$DE->setActive(false);
			$DE->setLabel('Germany');
			$DE->save();

			/** @var \Rbs\Geo\Documents\Country $IT */
			$IT = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Country');
			$IT->setCode('IT');
			$IT->setActive(true);
			$IT->setLabel('Italy');
			$IT->save();

			$countries = $geoManager->getCountriesByZoneCode(null);
			self::assertCount(2, $countries);
			self::assertContains($FR, $countries);
			self::assertContains($IT, $countries);

			// A zone code is specified.

			// A country matches...
			self::assertCount(0, $geoManager->getCountriesByZoneCode('BE'));

			/** @var \Rbs\Geo\Documents\Country $BE */
			$BE = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Country');
			$BE->setCode('BE');
			$BE->setActive(false);
			$BE->setLabel('Belgium');
			$BE->save();

			self::assertCount(0, $geoManager->getCountriesByZoneCode('BE'));

			$BE->setActive(true);
			$BE->save();

			$countries = $geoManager->getCountriesByZoneCode('BE');
			self::assertCount(1, $countries);
			self::assertContains($BE, $countries);

			// A zone matches...
			self::assertCount(0, $geoManager->getCountriesByZoneCode('FR-CONTINENTAL'));

			/** @var \Rbs\Geo\Documents\Zone $FRC */
			$FRC = $documentManager->getNewDocumentInstanceByModelName('Rbs_Geo_Zone');
			$FRC->setCode('FR-CONTINENTAL');
			$FRC->setLabel('France Continental');
			$FRC->setCountry($FR);
			$FRC->save();

			$countries = $geoManager->getCountriesByZoneCode('FR-CONTINENTAL');
			self::assertCount(1, $countries);
			self::assertContains($FR, $countries);

			$FR->setActive(false);
			$FR->save();

			self::assertCount(0, $geoManager->getCountriesByZoneCode('FR-CONTINENTAL'));
		}
		finally
		{
			$tm->rollBack();
		}
	}
}