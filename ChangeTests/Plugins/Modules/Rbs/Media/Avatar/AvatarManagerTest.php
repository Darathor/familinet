<?php
namespace ChangeTests\Rbs\Media\Avatar;

class AvatarManagerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	protected function attachSharedListener(\Zend\EventManager\SharedEventManager $sharedEventManager)
	{
		parent::attachSharedListener($sharedEventManager);
		$this->attachGenericServicesSharedListener($sharedEventManager);
	}

	protected function setUp(): void
	{
		parent::setUp();
		$this->initServices($this->getApplication());
	}

	public function testGetAvatarUrl()
	{
		$baseURL = 'http://www.rbs.fr';
		$urlManager = new \Change\Http\UrlManager(new \Zend\Uri\Http($baseURL));

		$avatarManager = $this->genericServices->getAvatarManager();
		self::assertInstanceOf(\Rbs\Media\Avatar\AvatarManager::class, $avatarManager);
		$avatarManager->setUrlManager($urlManager);

		$url = $avatarManager->getAvatarUrl(90, 'test@test.com');
		self::assertNotNull($url);

		/** @var \Rbs\User\Documents\User $user */
		$user = $this->getNewReadonlyDocument('Rbs_User_User', 1001);
		$user->setEmail('usertest@test.com');
		$url = $avatarManager->getAvatarUrl(90, null, $user);
		self::assertNotNull($url);

		$callback = static function (\Change\Events\Event $event) {
			$event->setParam('url', $event->getParam('email'));
			$event->stopPropagation();
		};

		$attach = $avatarManager->getEventManager()->attach(\Rbs\Media\Avatar\AvatarManager::AVATAR_GET_AVATAR_URL, $callback, 10);
		$url = $avatarManager->getAvatarUrl(90, 'test@test.com');
		self::assertEquals('test@test.com', $url);
		$avatarManager->getEventManager()->detach($attach);

		$url = $avatarManager->getAvatarUrl(90, 'test@test.com');
		self::assertNotEquals('test@test.com', $url);
	}
}