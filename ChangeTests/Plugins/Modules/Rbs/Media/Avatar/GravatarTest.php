<?php
namespace ChangeTests\Rbs\Media\Avatar;

class GravatarTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function testGetUrl()
	{
		$gravatar = new \Rbs\Media\Avatar\Gravatar('email@email.com');

		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=g', $gravatar->getUrl());

		$gravatar->setSize(250);
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=250&d=mm&r=g', $gravatar->getUrl());
		$gravatar->setSize(-1);
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=g', $gravatar->getUrl());
		$gravatar->setSize(2049);
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=g', $gravatar->getUrl());

		$gravatar->setDefaultImg('404');
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=404&r=g', $gravatar->getUrl());
		$gravatar->setDefaultImg('http://rbs.fr/avatar.png');
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=http%3A%2F%2Frbs.fr%2Favatar.png&r=g',
			$gravatar->getUrl());
		$gravatar->setDefaultImg('mm');

		$gravatar->setRating('x');
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=x', $gravatar->getUrl());
		$gravatar->setRating('y');
		self::assertEquals('http://www.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=g', $gravatar->getUrl());

		$gravatar->setSecure(true);
		self::assertEquals('https://secure.gravatar.com/avatar/4f64c9f81bb0d4ee969aaf7b4a5a6f40?s=80&d=mm&r=g', $gravatar->getUrl());
	}
}
