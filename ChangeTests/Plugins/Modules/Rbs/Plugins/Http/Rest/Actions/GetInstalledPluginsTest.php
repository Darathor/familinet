<?php

use Change\Http\Event;

class GetInstalledPluginsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		$this->getApplicationServices()->getPluginManager()->unsetPluginConfig();
		parent::setUp();
	}

	public function testExecute()
	{
		$pm = $this->getApplicationServices()->getPluginManager();
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$getInstalledPlugins = new \Rbs\Plugins\Http\Rest\Actions\GetInstalledPlugins();
		$getInstalledPlugins->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();

		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		$installedPlugins = $pm->getInstalledPlugins();
		self::assertNotEmpty($installedPlugins);
		self::assertCount(\count($installedPlugins), $arrayResult);
		//Check the date (because GetInstalledPlugins do a reformatting on registrationDate)
		$testPlugin = $arrayResult[0];
		self::assertIsString($testPlugin['registrationDate']);
		//search our test plugin in installed plugins
		$filteredPlugins = \array_filter($installedPlugins, static function (\Change\Plugins\Plugin $plugin) use ($testPlugin) {
			return $plugin->getType() === $testPlugin['type'] && $plugin->getVendor() === $testPlugin['vendor']
				&& $plugin->getShortName() === $testPlugin['shortName'];
		});
		self::assertCount(1, $filteredPlugins);
		$plugin = \array_pop($filteredPlugins);
		/* @var $plugin \Change\Plugins\Plugin */
		self::assertEquals($plugin->getRegistrationDate()->format(\DateTime::ATOM), $testPlugin['registrationDate']);
	}
}