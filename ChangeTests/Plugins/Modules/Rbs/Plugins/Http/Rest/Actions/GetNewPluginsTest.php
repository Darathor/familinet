<?php
use Change\Http\Event;

class GetNewPluginsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		$this->getApplicationServices()->getPluginManager()->unsetPluginConfig();
		parent::setUp();
	}

	public function testExecute()
	{
		$pm = $this->getApplicationServices()->getPluginManager();

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$getNewPlugins = new \Rbs\Plugins\Http\Rest\Actions\GetNewPlugins();
		$getNewPlugins->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();

		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		$newPlugins = $pm->getUnregisteredPlugins();
		self::assertNotEmpty($newPlugins);
		self::assertCount(\count($newPlugins), $arrayResult);
		//Do the test with a registered module, do a compile after, but compile erase all the module in the compiled file
		//So register the module Plugins (because without it, the action GetNewPlugins couldn't be performed!
		$pluginsModule = $pm->getModule('Rbs', 'Plugins');
		$pm->register($pluginsModule);
		$pm->compile();
		$newPlugins = $pm->getUnregisteredPlugins();
		self::assertNotEmpty($newPlugins);
		//Compare with the old result array
		self::assertNotCount(\count($newPlugins), $arrayResult);
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$getNewPlugins = new \Rbs\Plugins\Http\Rest\Actions\GetNewPlugins();
		$getNewPlugins->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		//And the the count is the same
		self::assertCount(\count($newPlugins), $arrayResult);
	}
}