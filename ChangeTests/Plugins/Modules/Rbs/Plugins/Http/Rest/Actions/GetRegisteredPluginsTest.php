<?php

use Change\Http\Event;

class GetRegisteredPluginsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		$this->getApplicationServices()->getPluginManager()->unsetPluginConfig();
		parent::setUp();
	}

	public function testExecute()
	{
		$pm = $this->getApplicationServices()->getPluginManager();

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$getRegisteredPlugins = new \Rbs\Plugins\Http\Rest\Actions\GetRegisteredPlugins();
		$getRegisteredPlugins->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();

		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		$registeredPlugins = $pm->getRegisteredPlugins();
		//There is no registered plugins, because they already fake installed (in the compiled file)
		self::assertEmpty($registeredPlugins);
		self::assertCount(\count($registeredPlugins), $arrayResult);
		//first we need to register the module Plugins for real!
		//Because without it, the action GetRegisteredPlugins couldn't be performed!
		$pluginsModule = $pm->getModule('Rbs', 'Plugins');
		$pluginsModule->setConfigured(false)->setActivated(false)->setRegistrationDate(null);
		$pm->register($pluginsModule);

		//Now, the plugin module is the only one to be registered!
		$registeredPlugins = $pm->getRegisteredPlugins();
		self::assertCount(1, $registeredPlugins);
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$getRegisteredPlugins = new \Rbs\Plugins\Http\Rest\Actions\GetRegisteredPlugins();
		$getRegisteredPlugins->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		//Compare result again
		self::assertCount(\count($registeredPlugins), $arrayResult);

		//Check the date (because GetInstalledPlugins do a reformatting on registrationDate)
		$testPlugin = $arrayResult[0];
		self::assertIsString($testPlugin['registrationDate']);
		$plugin = $registeredPlugins[0];
		self::assertInstanceOf(\Change\Plugins\Plugin::class, $plugin);
		/* @var $plugin \Change\Plugins\Plugin */
		self::assertEquals($plugin->getRegistrationDate()->format(\DateTime::ATOM), $testPlugin['registrationDate']);
	}
}