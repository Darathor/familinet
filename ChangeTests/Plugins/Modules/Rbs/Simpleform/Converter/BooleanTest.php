<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\BooleanTest
 */
class BooleanTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Boolean($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('true'));
		self::assertFalse($converter->isEmptyFromUI(' false'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Boolean($i18n, []);

		self::assertTrue($converter->parseFromUI('true'));
		self::assertTrue($converter->parseFromUI('		true  '));
		self::assertFalse($converter->parseFromUI('false'));
		self::assertFalse($converter->parseFromUI('		false  '));
		self::assertError($converter->parseFromUI('FALSE'));
		self::assertError($converter->parseFromUI('1'));
		self::assertError($converter->parseFromUI('toto'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}