<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\DateTest
 */
class DateTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Date($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('Toto'));
		self::assertFalse($converter->isEmptyFromUI(' Toto'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Date($i18n, []);

		self::assertEquals('2013-12-24', $converter->parseFromUI('2013-12-24'));
		self::assertEquals('2013-12-24', $converter->parseFromUI('   2013-12-24  '));
		self::assertError($converter->parseFromUI('abc'));
		self::assertError($converter->parseFromUI('2013-13-24'));
		self::assertError($converter->parseFromUI('2013-12-32'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}