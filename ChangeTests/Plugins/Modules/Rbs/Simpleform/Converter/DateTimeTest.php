<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\DateTimeTest
 */
class DateTimeTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\DateTime($i18n, []);

		self::assertTrue($converter->isEmptyFromUI([]));
		self::assertTrue($converter->isEmptyFromUI(['date' => '', 'time' => '']));
		self::assertFalse($converter->isEmptyFromUI(['date' => 'toto', 'time' => '']));
		self::assertFalse($converter->isEmptyFromUI(['date' => 'toto', 'time' => 'test']));
		self::assertFalse($converter->isEmptyFromUI(['date' => '', 'time' => 'test']));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\DateTime($i18n, []);

		self::assertError($converter->parseFromUI('2013-12-24'));
		self::assertEquals('2013-12-24 12:12:00', $converter->parseFromUI(['date' => '2013-12-24', 'time' => '12:12']));
		self::assertEquals('2013-12-24 12:12:01', $converter->parseFromUI(['date' => '2013-12-24', 'time' => '12:12:01']));
		self::assertError($converter->parseFromUI(['date' => '2013-12-24', 'time' => '']));
		self::assertError($converter->parseFromUI(['date' => '', 'time' => '12:12']));
		self::assertError($converter->parseFromUI(['date' => '2013-12-24', 'time' => '12:12a']));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}