<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\EmailsTest
 */
class EmailsTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Emails($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('Toto'));
		self::assertFalse($converter->isEmptyFromUI(' Toto'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Emails($i18n, []);

		self::assertEquals(['no-reply@rbs.fr'], $converter->parseFromUI('no-reply@rbs.fr'));
		self::assertEquals(['no-reply@rbs.fr', 'chuck.norris@rbs.fr'], $converter->parseFromUI('		no-reply@rbs.fr , chuck.norris@rbs.fr '));
		self::assertError($converter->parseFromUI('no-reply'));
		self::assertError($converter->parseFromUI('chuck.norris@rbs.fr,no-reply'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}