<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\IntegerTest
 */
class IntegerTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Integer($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('85'));
		self::assertFalse($converter->isEmptyFromUI(' 36'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Integer($i18n, []);

		self::assertEquals(6, $converter->parseFromUI('6'));
		self::assertEquals(8, $converter->parseFromUI('		8  '));
		self::assertEquals(-7253, $converter->parseFromUI('-7253'));
		self::assertEquals(8924, $converter->parseFromUI('		8924  '));

		// Invalid formats.
		self::assertError($converter->parseFromUI('894b'));
		self::assertError($converter->parseFromUI('n56'));
		self::assertError($converter->parseFromUI('85v63'));

		// Handle locales.
		$i18n->setLCID('fr_FR');
		self::assertEquals(895546423, $converter->parseFromUI('895 546 423'));
		self::assertEquals(-999555666, $converter->parseFromUI('		-999 555 666  '));

		$i18n->setLCID('en_US');
		self::assertEquals(895546423, $converter->parseFromUI('895,546,423'));
		self::assertEquals(-999555666, $converter->parseFromUI('		-999,555,666  '));

		// Validators.
		$converter = new \Rbs\Simpleform\Converter\Integer($i18n, ['max' => 42]);
		self::assertEquals(41, $converter->parseFromUI('41'));
		self::assertEquals(42, $converter->parseFromUI('42'));
		self::assertError($converter->parseFromUI('43'));

		$converter = new \Rbs\Simpleform\Converter\Integer($i18n, ['min' => 42]);
		self::assertError($converter->parseFromUI('41'));
		self::assertEquals(42, $converter->parseFromUI('42'));
		self::assertEquals(43, $converter->parseFromUI('43'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}