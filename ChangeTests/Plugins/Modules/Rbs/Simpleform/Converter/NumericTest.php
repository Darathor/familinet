<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\NumericTest
 */
class NumericTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Numeric($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('85.54'));
		self::assertFalse($converter->isEmptyFromUI(' 36.36'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Numeric($i18n, []);

		self::assertEquals(6, $converter->parseFromUI('6'));
		self::assertEquals(8, $converter->parseFromUI('		8  '));
		self::assertEquals(-72.53, $converter->parseFromUI('-72.53'));
		self::assertEquals(89.24, $converter->parseFromUI('		89.24  '));

		// Invalid formats.
		self::assertError($converter->parseFromUI('89.4b'));
		self::assertError($converter->parseFromUI('n56'));
		self::assertError($converter->parseFromUI('85v6.3'));

		// Handle locales.
		$i18n->setLCID('fr_FR');
		self::assertEquals(895546423, $converter->parseFromUI('895 546 423'));
		self::assertEquals(-999555666, $converter->parseFromUI('		-999 555 666  '));
		self::assertEquals(5546423.56, $converter->parseFromUI('5 546 423,56'));
		self::assertEquals(-9555666.98, $converter->parseFromUI('		-9 555 666,98  '));

		$i18n->setLCID('en_US');
		self::assertEquals(895546423, $converter->parseFromUI('895,546,423'));
		self::assertEquals(-999555666, $converter->parseFromUI('		-999,555,666  '));
		self::assertEquals(5546423.35, $converter->parseFromUI('5,546,423.35'));
		self::assertEquals(-9555666.96, $converter->parseFromUI('		-9,555,666.96  '));

		// Validators.
		$converter = new \Rbs\Simpleform\Converter\Numeric($i18n, ['max' => 42]);
		self::assertEquals(41.99, $converter->parseFromUI('41.99'));
		self::assertEquals(42, $converter->parseFromUI('42'));
		self::assertError($converter->parseFromUI('42.01'));

		$converter = new \Rbs\Simpleform\Converter\Numeric($i18n, ['min' => 42]);
		self::assertError($converter->parseFromUI('41.99'));
		self::assertEquals(42, $converter->parseFromUI('42'));
		self::assertEquals(42.01, $converter->parseFromUI('42.01'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}