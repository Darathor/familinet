<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\TextTest
 */
class TextTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Text($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('Toto'));
		self::assertFalse($converter->isEmptyFromUI(' Toto'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Text($i18n, []);

		self::assertEquals('test', $converter->parseFromUI('test'));
		self::assertEquals('test', $converter->parseFromUI('   test  '));

		$converter = new \Rbs\Simpleform\Converter\Text($i18n, ['minSize' => 4]);
		self::assertEquals('test', $converter->parseFromUI('   test  '));

		$converter = new \Rbs\Simpleform\Converter\Text($i18n, ['minSize' => 5]);
		self::assertError($converter->parseFromUI('   test  '));

		$converter = new \Rbs\Simpleform\Converter\Text($i18n, ['maxSize' => 4]);
		self::assertEquals('test', $converter->parseFromUI('   test  '));

		$converter = new \Rbs\Simpleform\Converter\Text($i18n, ['maxSize' => 2]);
		self::assertError($converter->parseFromUI('   test  '));

		$converter = new \Rbs\Simpleform\Converter\Text($i18n, ['pattern' => '^[A-Z][0-9]$']);
		self::assertEquals('A7', $converter->parseFromUI('   A7  '));
		self::assertError($converter->parseFromUI('A7 zqf qfz77'));
		self::assertError($converter->parseFromUI('a7'));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}