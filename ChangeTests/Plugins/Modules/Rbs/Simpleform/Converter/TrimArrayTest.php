<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\TrimArrayTest
 */
class TrimArrayTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\TrimArray($i18n, []);

		self::assertTrue($converter->isEmptyFromUI([]));
		self::assertTrue($converter->isEmptyFromUI(['    ']));
		self::assertFalse($converter->isEmptyFromUI(['Toto']));
		// If the value is not an array, the value is considered as empty.
		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('Toto'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\TrimArray($i18n, []);

		self::assertError($converter->parseFromUI(null));
		self::assertError($converter->parseFromUI(''));
		self::assertError($converter->parseFromUI('test'));
		self::assertEquals(['test'], $converter->parseFromUI(['test']));
		// Empty values are removed.
		self::assertEquals(['test', 'toto'], $converter->parseFromUI(['   ', 'test', 'toto    ']));
	}

	protected static function assertError($value)
	{
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $value);
	}
}