<?php
namespace ChangeTests\Plugins\Modules\Simpleform\Converter;

/**
 * @name \ChangeTests\Plugins\Modules\Simpleform\Converter\TrimTest
 */
class TrimTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public function setUp(): void
	{
		parent::setUp();
		//Register default Translator and Change Constraint
		$this->getApplicationServices()->getConstraintsManager();
	}

	public function testIsEmptyFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Trim($i18n, []);

		self::assertTrue($converter->isEmptyFromUI(null));
		self::assertTrue($converter->isEmptyFromUI(''));
		self::assertTrue($converter->isEmptyFromUI('  	  	 '));
		self::assertFalse($converter->isEmptyFromUI('Toto'));
		self::assertFalse($converter->isEmptyFromUI(' Toto'));
	}

	public function testParseFromUI()
	{
		$i18n = $this->getApplicationServices()->getI18nManager();
		$converter = new \Rbs\Simpleform\Converter\Trim($i18n, []);

		self::assertEquals('test', $converter->parseFromUI('test'));
		self::assertEquals('test', $converter->parseFromUI('   test  '));
		self::assertInstanceOf(\Rbs\Simpleform\Converter\Validation\Error::class, $converter->parseFromUI([]));
	}
}