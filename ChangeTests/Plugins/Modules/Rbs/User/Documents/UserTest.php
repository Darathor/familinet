<?php
namespace ChangeTests\Rbs\User\Documents;

class UserTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function tearDown(): void
	{
		//delete the new user if exist
		$this->deleteNewUserIfExist();
		parent::tearDown();
	}

	public function testOnCreate()
	{
		$user = $this->getNewUser(false);
		self::assertNotNull($user->getHashMethod());
		self::assertEquals('bcrypt', $user->getHashMethod());
		$user = $this->getNewUser(true);
		self::assertNotNull($user->getPasswordHash());
		$bcryptHash = $user->getPasswordHash();
		self::assertNotEquals('abcd123', $bcryptHash);
		$this->deleteNewUserIfExist();
		//a salt must be defined if you want a custom hash
		$this->getApplication()->getConfiguration()->addVolatileEntry('Rbs/User/salt', 'aSalt');
		$user = $this->getNewUser(true, 'md5');
		self::assertNotNull($user->getHashMethod());
		self::assertEquals('md5', $user->getHashMethod());
		self::assertNotNull($user->getPasswordHash());
		self::assertNotEquals($bcryptHash, $user->getPasswordHash());
	}

	public function testOnUpdate()
	{
		$user = $this->getNewUser(true);
		self::assertNotNull($user->getPasswordHash());
		$passwordHash = $user->getPasswordHash();
		$user->setPassword('321dcba');
		$dm = $this->getApplicationServices()->getDocumentManager();
		$tm = $this->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();
			$user->update();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
			self::fail('new user cannot be updated with this error: ' . $e->getMessage());
		}
		$user = $dm->getDocumentInstance($user->getId());
		/* @var $user \Rbs\User\Documents\User */
		self::assertNotNull($user->getPasswordHash());
		self::assertNotEquals($passwordHash, $user->getPasswordHash());
	}

	public function testUpdateRestDocumentResult()
	{
		$documentResult = new \Change\Http\Rest\V1\Resources\DocumentResult(new \Change\Http\UrlManager(new \Zend\Uri\Http()), $this->getNewUser());
		self::assertNotNull($documentResult);
		$result = $documentResult->toArray();
		self::assertNotNull($result);
		self::assertArrayHasKey('links', $result);
		self::assertNotNull($result['links']);
		//search in links if the profile link is available
		$isProfileLinkAvailable = false;
		foreach ($result['links'] as $link)
		{
			if (\in_array('profiles', $link, true))
			{
				self::assertArrayHasKey('rel', $link);
				self::assertEquals('profiles', $link['rel']);
				self::assertArrayHasKey('href', $link);
				self::assertNotNull($link['href']);
				$isProfileLinkAvailable = true;
			}
		}
		self::assertTrue($isProfileLinkAvailable, 'link profiles has to be in Rest User document result');
	}

	/**
	 * @param boolean $save
	 * @param null $hashMethod
	 * @return \Rbs\User\Documents\User
	 * @throws \Change\Transaction\RollbackException
	 */
	protected function getNewUser($save = false, $hashMethod = null)
	{
		$dm = $this->getApplicationServices()->getDocumentManager();
		$tm = $this->getApplicationServices()->getTransactionManager();

		$newUser = $dm->getNewDocumentInstanceByModelName('Rbs_User_User');
		/* @var $newUser \Rbs\User\Documents\User */
		$newUser->setLogin(\Change\Stdlib\StringUtils::random(16));
		$newUser->setEmail('mario.bros@nintendo.com');
		$newUser->setPassword('abcd123');
		if ($hashMethod)
		{
			$newUser->setHashMethod($hashMethod);
		}

		if ($save)
		{
			try
			{
				$tm->begin();
				$newUser->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				$tm->rollBack($e);
				self::fail('new user cannot be created with this error: ' . $e->getMessage());
			}
			self::assertTrue($newUser->getId() > 0);
		}

		return $newUser;
	}

	protected function deleteNewUserIfExist()
	{
		$dqb = $this->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_User_User');
		$user = $dqb->andPredicates($dqb->eq('login', 'super_mario'))->getFirstDocument();
		if ($user)
		{
			$tm = $this->getApplicationServices()->getTransactionManager();
			try
			{
				$tm->begin();
				$user->delete();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				$tm->rollBack($e);
				self::fail('new user cannot be deleted with this error: ' . $e->getMessage());
			}
		}
	}
}