<?php
namespace ChangeTests\Rbs\User\Http\Rest\Actions;

use Change\Http\Event;
use Change\Http\Request;

class AddPermissionRulesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function testExecute()
	{
		$permissionRules = [
			'accessor_id' => 123456,
			'roles' => ['Administrator'],
			'privileges' => ['Rbs_User_User'],
			'resources' => [123456]
		];

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$pm = $event->getPermissionsManager();
		self::assertInstanceOf(\Change\Permissions\PermissionsManager::class, $pm);
		$paramArray = ['permissionRules' => $permissionRules];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\AddPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check the added permissions
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		self::assertCount(1, $arrayResult);
		//Check this new rule with permissionManager
		self::assertTrue($pm->hasRule(123456, 'Administrator', 123456, 'Rbs_User_User'));

		//insert more rules and test if the function delete useless rules works like expected
		$permissionRules = [
			'accessor_id' => 123456,
			'roles' => ['Creator', 'Editor', 'Publisher'],
			'privileges' => ['Rbs_User_Group', 'Rbs_Media_Image', 'Rbs_Website_StaticPage'],
			'resources' => [123, 456, 789]
		];
		//this array will create 3x3x3 (27) rules!

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$pm = $event->getPermissionsManager();
		$paramArray = ['permissionRules' => $permissionRules];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\AddPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check the added permissions
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());

		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		//our 27 new rules + the older: 28
		self::assertCount(28, $arrayResult);
		//check just one rule
		self::assertTrue($pm->hasRule(123456, 'Creator', 789, 'Rbs_Media_Image'));

		//now we insert a rule with a better permission
		$permissionRules = [
			'accessor_id' => 123456,
			'roles' => ['Creator'],
			'privileges' => ['Rbs_User_Group'],
			'resources' => ['*']
		];
		//this rule will make 3 older rules useless.

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$pm = $event->getPermissionsManager();
		$paramArray = ['permissionRules' => $permissionRules];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\AddPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check the added permissions
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		//28 rules + 1 new - 3 useless = 26 rules
		self::assertCount(26, $arrayResult);
		self::assertTrue($pm->hasRule(123456, 'Creator', '*', 'Rbs_User_Group'));
		self::assertFalse($pm->hasRule(123456, 'Creator', 123, 'Rbs_User_Group'));

		//with a very better permission
		$permissionRules = [
			'accessor_id' => 123456,
			'roles' => ['Creator', 'Editor'],
			'privileges' => ['*'],
			'resources' => ['*']
		];
		//this rule will make the 9 Editor's older rules and 7 Creator's older rules useless (total: 16).

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$pm = $event->getPermissionsManager();
		$paramArray = ['permissionRules' => $permissionRules];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\AddPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check the added permissions
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		//26 rules + 2 new - 16 useless = 12 rules
		self::assertCount(12, $arrayResult);
		//check one new rule
		self::assertTrue($pm->hasRule(123456, 'Editor', '*', '*'));
		self::assertFalse($pm->hasRule(123456, 'Creator', '*', 'Rbs_User_Group'));

		//give a VIP **Gold** prime ultra full access \o/
		$permissionRules = [
			'accessor_id' => 123456,
			'roles' => ['*'],
			'privileges' => ['*'],
			'resources' => ['*']
		];
		//this rule will make all old rules useless!

		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$pm = $event->getPermissionsManager();
		$paramArray = ['permissionRules' => $permissionRules];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\AddPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check the added permissions
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		//only one rule!
		self::assertCount(1, $arrayResult);
		//check the rule
		self::assertTrue($pm->hasRule(123456, '*', '*', '*'));
	}
}