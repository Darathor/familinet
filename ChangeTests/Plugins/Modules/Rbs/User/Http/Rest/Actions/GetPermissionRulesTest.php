<?php
namespace ChangeTests\Rbs\User\Http\Rest\Actions;

use Change\Http\Event;
use Change\Http\Request;

class GetPermissionRulesTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function setUp(): void
	{
		//add fake permissions
		$pm = $this->getApplicationServices()->getPermissionsManager();

		$pm->addRule(123456, 'Administrator', 123456, 'Rbs_User_User');
		$pm->addRule(123456, 'Administrator', '*', 'Rbs_User_Group');
		$pm->addRule(123456, 'Editor', 1234567, 'Rbs_Media_Image');
		$pm->addRule(123456, 'Consumer', '*', '*');
		$pm->addRule(123456, 'Publisher', '*', 'Rbs_Collection_Collection');
		//not the same accessor id
		$pm->addRule(123457, 'Creator', '*', 'Rbs_Collection_Collection');
		$pm->addRule(123457, 'Creator', '*', 'Rbs_Collection_Collection');
	}

	public function testExecute()
	{
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		self::assertCount(5, $arrayResult, 'if it is 7, that mean that all permission are taken et not only them for targeted user');
		//take the first one to check if all key exist
		$permission = $arrayResult[0];
		self::assertArrayHasKey('rule_id', $permission);
		self::assertArrayHasKey('role', $permission);
		self::assertArrayHasKey('resource_id', $permission);
		self::assertArrayHasKey('privilege', $permission);
	}
}