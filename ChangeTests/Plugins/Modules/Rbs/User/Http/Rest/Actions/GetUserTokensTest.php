<?php
namespace ChangeTests\Rbs\User\Http\Rest\Actions;

use Change\Http\Event;
use Change\Http\Request;

class GetUserTokensTest extends \ChangeTests\Change\TestAssets\TestCase
{
	/**
	 * @var \Change\Http\OAuth\OAuthDbEntry
	 */
	protected $validStoredOAuth;

	/**
	 * @var \Change\Http\OAuth\OAuthDbEntry
	 */
	protected $invalidStoredOAuth;

	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function setUp(): void
	{
		$this->getApplicationServices()->getTransactionManager()->begin();

		//add a fake application
		$qb = $this->getApplicationServices()->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->insert($qb->getSqlMapping()->getOAuthApplicationTable())
			->addColumns($fb->column('application'), $fb->column('consumer_key'), $fb->column('consumer_secret'))
			->addValues($fb->parameter('application'), $fb->parameter('consumer_key'), $fb->integerParameter('consumer_secret'));
		$iq = $qb->insertQuery();

		$iq->bindParameter('application', 'Rbs_Tests');
		$iq->bindParameter('consumer_key', 'consumerKeyForTests');
		$iq->bindParameter('consumer_secret', 'consumerSecretForTests');
		$iq->execute();

		//insert fake tokens in database (AccessorId need to be the same for both)
		//a valid
		$this->validStoredOAuth = new \Change\Http\OAuth\OAuthDbEntry();
		$this->validStoredOAuth->setAccessorId(123456);
		$this->validStoredOAuth->setAuthorized(1);
		$this->validStoredOAuth->setRealm('Change_Tests');
		$this->validStoredOAuth->setToken('abcd123456789');
		$this->validStoredOAuth->setTokenSecret('TestTokenSecret');
		$this->validStoredOAuth->setType(\Change\Http\OAuth\OAuthDbEntry::TYPE_ACCESS);
		$this->validStoredOAuth->setCallback('oob');
		$this->validStoredOAuth->setCreationDate((new \DateTime())->sub(new \DateInterval('P5D')));
		$this->validStoredOAuth->setValidityDate((new \DateTime())->add(new \DateInterval('P10Y')));
		$this->validStoredOAuth->setConsumerKey('consumerKeyForTests');
		//an invalid
		$this->invalidStoredOAuth = new \Change\Http\OAuth\OAuthDbEntry();
		$this->invalidStoredOAuth->setAccessorId(123456);
		$this->invalidStoredOAuth->setAuthorized(1);
		$this->invalidStoredOAuth->setRealm('Change_Tests');
		$this->invalidStoredOAuth->setToken('dcba987654321');
		$this->invalidStoredOAuth->setTokenSecret('TestTokenSecret');
		$this->invalidStoredOAuth->setType(\Change\Http\OAuth\OAuthDbEntry::TYPE_ACCESS);
		$this->invalidStoredOAuth->setCallback('oob');
		$this->invalidStoredOAuth->setCreationDate((new \DateTime())->sub(new \DateInterval('P5D')));
		$this->invalidStoredOAuth->setValidityDate(new \DateTime());
		$this->invalidStoredOAuth->setConsumerKey('consumerKeyForTests');
		$oauth = $this->getApplicationServices()->getOAuthManager();
		$oauth->insertToken($this->validStoredOAuth);
		$oauth->insertToken($this->invalidStoredOAuth);
	}

	public function tearDown(): void
	{
		$this->getApplicationServices()->getTransactionManager()->rollBack();
		parent::tearDown();
	}

	public function testExecute()
	{
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['userId' => $this->validStoredOAuth->getAccessorId()];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getUserTokens = new \Rbs\User\Http\Rest\Actions\GetUserTokens();
		$getUserTokens->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$arrayResult = $result->toArray();
		self::assertNotEmpty($arrayResult);
		self::assertCount(1, $arrayResult, 'if it is 2, that mean that the invalid token is taken with');
		//Check token content
		$token = $arrayResult[0];
		self::assertEquals($this->validStoredOAuth->getToken(), $token['token']);
		self::assertEquals($this->validStoredOAuth->getRealm(), $token['realm']);
		self::assertEquals('Rbs_Tests', $token['application']);
		self::assertIsString($token['creation_date']);
		self::assertEquals($this->validStoredOAuth->getCreationDate()->format(\DateTime::ATOM), $token['creation_date']);
		self::assertIsString($token['validity_date']);
		self::assertEquals($this->validStoredOAuth->getValidityDate()->format(\DateTime::ATOM), $token['validity_date']);
	}
}