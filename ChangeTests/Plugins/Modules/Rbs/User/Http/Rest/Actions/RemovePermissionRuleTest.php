<?php
namespace ChangeTests\Rbs\User\Http\Rest\Actions;

use Change\Http\Event;
use Change\Http\Request;

class RemovePermissionRuleTest extends \ChangeTests\Change\TestAssets\TestCase
{
	public static function setUpBeforeClass(): void
	{
		static::initDocumentsDb();
	}

	public static function tearDownAfterClass(): void
	{
		static::clearDB();
	}

	public function setUp(): void
	{
		//add fake permissions
		$pm = $this->getApplicationServices()->getPermissionsManager();

		$pm->addRule(123456, 'Administrator', 123456, 'Rbs_User_User');
		$pm->addRule(123456, 'Administrator', '*', 'Rbs_User_Group');
		$pm->addRule(123456, 'Editor', 1234567, 'Rbs_Media_Image');
		$pm->addRule(123456, 'Consumer', '*', '*');
		$pm->addRule(123456, 'Publisher', '*', 'Rbs_Collection_Collection');
		//not the same accessor id
		$pm->addRule(123457, 'Creator', '*', 'Rbs_Collection_Collection');
		$pm->addRule(123457, 'Creator', '*', 'Rbs_Collection_Collection');
	}

	public function testExecute()
	{
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$firstArrayResult = $result->toArray();
		self::assertNotEmpty($firstArrayResult);
		self::assertCount(5, $firstArrayResult);
		$firstRule = array_pop($firstArrayResult);

		//delete this rule
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['rule_id' => $firstRule['rule_id']];
		$event->setRequest((new Request())->setPost(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\RemovePermissionRule();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());

		//check if the rule is actually deleted
		$event = new Event();
		$event->setParams($this->getDefaultEventArguments());
		$paramArray = ['accessorId' => 123456];
		$event->setRequest((new Request())->setQuery(new \Zend\Stdlib\Parameters($paramArray)));
		$getPermissionRules = new \Rbs\User\Http\Rest\Actions\GetPermissionRules();
		$getPermissionRules->execute($event);
		self::assertEquals(200, $event->getResult()->getHttpStatusCode());
		$result = $event->getResult();
		/* @var $result \Change\Http\Rest\V1\ArrayResult */
		$secondArrayResult = $result->toArray();
		self::assertNotEmpty($secondArrayResult);
		self::assertCount(4, $secondArrayResult);

		self::assertEquals($firstArrayResult, $secondArrayResult);
	}
}