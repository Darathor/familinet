<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin;

/**
 * @name \Rbs\Admin\AdminManager
 */
class AdminManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var \Assetic\AssetManager
	 */
	protected $jsAssetManager;

	/**
	 * @var \Assetic\AssetManager
	 */
	protected $cssAssetManager;

	/**
	 * @var string
	 */
	protected $cachePath;

	/**
	 * @var \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	protected $extensions;

	public function __construct()
	{
		$this->jsAssetManager = new \Assetic\AssetManager();
		$this->cssAssetManager = new \Assetic\AssetManager();
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @api
	 * @param \Change\Presentation\Templates\Twig\ExtensionInterface $extension
	 * @return $this
	 */
	public function addExtension(\Change\Presentation\Templates\Twig\ExtensionInterface $extension)
	{
		$this->getExtensions();
		$this->extensions[] = $extension;
		return $this;
	}

	/**
	 * @return \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	public function getExtensions()
	{
		if ($this->extensions === null)
		{
			$extension = new \Change\Presentation\Templates\Twig\Extension($this->i18nManager, $this->application->getConfiguration());
			$this->extensions = [$extension];

			$extension = new \Rbs\Admin\Presentation\Twig\Extension($this, $this->application, $this->i18nManager, $this->modelManager);
			$this->extensions[] = $extension;
		}
		return $this->extensions;
	}

	/**
	 * @return \Assetic\AssetManager
	 */
	public function getCssAssetManager()
	{
		return $this->cssAssetManager;
	}

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return 'Rbs_Admin';
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Admin/Events/AdminManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('getModelTwigAttributes', function ($event) { $this->onDefaultGetModelTwigAttributes($event); }, 5);
		$eventManager->attach('getModelTwigAttributes', function ($event) { $this->onGetModelTwigSynchronizationAttributes($event); });

		$eventManager->attach('getRoutes', function ($event) { $this->onDefaultGetRoutes($event); }, 5);
		$eventManager->attach('searchDocuments', function ($event) { $this->onQuerySearchDocuments($event); }, 5);
		$eventManager->attach('searchDocuments', function ($event) { $this->onDefaultSearchDocuments($event); });
		$eventManager->attach('searchDocumentsQuery', function ($event) { $this->onQuerySearchDocuments($event); }, 5);
	}

	/**
	 * @return array
	 */
	public function getMainMenu()
	{
		$pm = $this->getPluginManager();
		$sections = [];
		$entries = [];
		$routesHelper = $this->getRoutesHelper();
		foreach ($pm->getInstalledPlugins() as $plugin)
		{
			$mainMenuPath = $plugin->getAssetsPath() . '/Admin/main-menu.json';
			if (\is_readable($mainMenuPath))
			{
				$menuJson = \json_decode(\file_get_contents($mainMenuPath), true, 512, \JSON_THROW_ON_ERROR);
				if (\is_array($menuJson))
				{
					$sections = \array_merge($sections, $this->parseSections($menuJson));
					$entries = \array_merge($entries, $this->parseEntries($menuJson));
				}
			}
		}
		\uasort($sections, static function ($a, $b) {
			$ida = $a['index'] ?? PHP_INT_MAX;
			$idb = $b['index'] ?? PHP_INT_MAX;
			return $ida >= $idb;
		});
		$defaultSection = null;
		if ($sections)
		{
			\reset($sections);
			$defaultSection = \key($sections);
		}
		foreach ($entries as $entry)
		{
			$section = isset($entry['section'], $sections[$entry['section']]) ? $entry['section'] : $defaultSection;
			if ($section)
			{
				$sections[$section]['entries'][] = $entry;
			}
		}

		$result = [];
		foreach ($sections as $section)
		{
			$entries = $section['entries'];
			\usort($entries, static function ($a, $b) {
				return \strcmp(\Change\Stdlib\StringUtils::stripAccents($a['label']), \Change\Stdlib\StringUtils::stripAccents($b['label']));
			});

			foreach ($entries as &$entry)
			{
				$m = $entry['model'] ?? $entry['module'] ?? null;
				$n = $entry['name'] ?? null;
				if ($m && $n && ($routeEntry = $routesHelper->getNamedRoute($m, $n)))
				{
					if ($models = $routesHelper->getModels($routeEntry['path']))
					{
						$entry['permissionRule'] = $entry['permissionRule'] ?? \implode(',', $models);
					}

					$redirectPath = $routeEntry['rule']['redirectTo'] ?? null;
					$entry['url'] = $entry['url'] ?? \ltrim($redirectPath ?? $routeEntry['path'] ?? '', '/');
				}
			}
			unset($entry);

			$section['entries'] = $entries;
			$result[] = $section;
		}
		return $result;
	}

	/**
	 * @return string
	 */
	protected function getCachePath()
	{
		if ($this->cachePath === null)
		{
			$this->cachePath = $this->getApplication()->getWorkspace()->cachePath('Admin', 'Templates', 'Compiled');
			\Change\Stdlib\FileUtils::mkdir($this->cachePath);
		}
		return $this->cachePath;
	}

	/**
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function renderTemplateFile($pathName, array $attributes)
	{
		$overridePath = $this->getApplication()->getWorkspace()->appPath('AdminOverrides');
		$loader = new \Rbs\Admin\Presentation\Twig\Loader($overridePath, $this->getPluginManager(), \dirname($pathName));
		$twig = new \Twig\Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render(\basename($pathName), $attributes);
	}

	/**
	 * @param string $moduleName
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function renderModuleTemplateFile($moduleName, $pathName, array $attributes)
	{
		$overridePath = $this->getApplication()->getWorkspace()->appPath('AdminOverrides');
		$loader = new \Rbs\Admin\Presentation\Twig\Loader($overridePath, $this->getPluginManager());
		$twig = new \Twig\Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render('@' . $moduleName . '/' . $pathName, $attributes);
	}

	/**
	 * @api
	 * @return array
	 */
	public function getRoutes()
	{
		$args = $this->getEventManager()->prepareArgs([]);
		$this->getEventManager()->trigger('getRoutes', $this, $args);
		$routes = isset($args['routes']) && \is_array($args['routes']) ? $args['routes'] : [];

		foreach ($routes as $path => $route)
		{
			if (\is_array($route) && isset($route['rule']))
			{
				unset($route['auto']);
				$routes[$path] = $route;
			}
			else
			{
				unset($routes[$path]);
			}
		}
		return $routes;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultGetRoutes($event)
	{
		$routes = [];
		foreach ($this->getPluginManager()->getModules() as $module)
		{
			if ($module->isAvailable())
			{
				$filePath = $this->getApplication()->getWorkspace()
					->composePath($module->getAssetsPath(), 'Admin', 'routes.json');
				if (\is_readable($filePath))
				{
					$moduleRoutes = \json_decode(\file_get_contents($filePath), true, 512, \JSON_THROW_ON_ERROR);
					if (\is_array($moduleRoutes))
					{
						$routes = \array_merge($routes, $moduleRoutes);
					}
					else
					{
						$event->getApplicationServices()->getLogging()->error('invalid json file: ' . $filePath);
					}
				}
			}
		}
		$event->setParam('routes', $routes);
	}

	/**
	 * @var \Rbs\Admin\RoutesHelper
	 */
	protected $routesHelper;

	/**
	 * @return \Rbs\Admin\RoutesHelper
	 */
	public function getRoutesHelper()
	{
		if ($this->routesHelper === null)
		{
			$this->routesHelper = new \Rbs\Admin\RoutesHelper($this->getRoutes());
		}
		return $this->routesHelper;
	}

	/**
	 * @param \Change\Documents\AbstractModel $model
	 * @param string $view edit|list|new|translate
	 * @return array
	 */
	public function getModelTwigAttributes($model, $view)
	{
		$args = $this->getEventManager()->prepareArgs([
			'model' => $model,
			'view' => $view
		]);

		$this->getEventManager()->trigger('getModelTwigAttributes', $this, $args);
		return isset($args['attributes']) && \is_array($args['attributes']) ? $args['attributes'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \RuntimeException
	 */
	protected function onDefaultGetModelTwigAttributes(\Change\Events\Event $event)
	{
		/* @var $model \Change\Documents\AbstractModel */
		$model = $event->getParam('model');
		$view = $event->getParam('view');

		$attributes = [];

		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$module = $pluginManager->getModule($model->getVendorName(), $model->getShortModuleName());
		if (!$module)
		{
			throw new \RuntimeException('module ' . $model->getVendorName() . '_' . $model->getShortModuleName()
				. ' is not found', 999999);
		}

		$collectionManager = $event->getApplicationServices()->getCollectionManager();
		/** @var \Change\Collection\CollectionInterface $typologyCollection */
		$typologyCollection = $collectionManager->getCollection('Rbs_Generic_Typologies', ['modelName' => $model->getName()]);

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		switch ($view)
		{
			case 'new':
				$attributes = [
					'model' => $model,
					'hasTypology' => \count($typologyCollection->getItems()) > 0,
					'asideDirectives' => [['name' => 'rbs-aside-editor-menu']]
				];
				break;

			case 'edit':
				$attributes = [
					'model' => $model,
					'hasTypology' => \count($typologyCollection->getItems()) > 0,
					'asideDirectives' => [['name' => 'rbs-aside-editor-menu']],
					'links' => []
				];
				if ($model->isLocalized())
				{
					$attributes['asideDirectives'][] = [
						'name' => 'rbs-aside-translation',
						'attributes' => [
							['name' => 'document', 'value' => 'document']
						]
					];
				}
				break;

			case 'list':
				$newDocumentLinks = [];
				$modelNames = [];
				if (!$model->isAbstract())
				{
					$key = \strtolower(\implode('.', ['m', $model->getVendorName(), $model->getShortModuleName(),
						'admin', $model->getShortName() . '_create']));
					$label = $i18nManager->trans($key, ['ucf']);
					$modelNames[] = $model->getName();
					$newDocumentLinks[] = ['modelName' => $model->getName(), 'label' => $label];
				}

				foreach ($model->getDescendantsNames() as $descendantName)
				{
					$m = $event->getApplicationServices()->getModelManager()->getModelByName($descendantName);
					if ($m && !$m->isAbstract())
					{
						$key = \strtolower(\implode('.', ['m', $m->getVendorName(), $m->getShortModuleName(),
							'admin', $m->getShortName() . '_create']));
						$label = $i18nManager->trans($key, ['ucf']);
						$modelNames[] = $m->getName();
						$newDocumentLinks[] = ['modelName' => $m->getName(), 'label' => $label];
					}
				}

				$attributes = [
					'asideDirectives' => [['name' => 'rbs-default-asides-for-list']],
					'newDocumentLinks' => $newDocumentLinks,
					'modelNames' => $modelNames
				];
				break;

			case 'translate':
				$attributes = [
					'model' => $model,
					'hasTypology' => \count($typologyCollection->getItems()) > 0,
					'asideDirectives' => [
						['name' => 'rbs-aside-editor-menu'],
						['name' => 'rbs-aside-translation', 'attributes' => [['name' => 'document', 'value' => 'document']]]
					],
					'links' => []
				];
				break;

			default:
				break;
		}

		$event->setParam('attributes', $attributes);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \RuntimeException
	 */
	protected function onGetModelTwigSynchronizationAttributes(\Change\Events\Event $event)
	{
		/* @var $model \Change\Documents\AbstractModel */
		$model = $event->getParam('model');
		$view = $event->getParam('view');
		$attributes = $event->getParam('attributes');
		if ($model && $view === 'edit' && isset($attributes['asideDirectives']))
		{
			$configFiles = (array)$event->getApplication()->getConfiguration('Rbs/Admin/Asides/codeConfig');
			foreach ($configFiles as &$configFile)
			{
				$configFile = $event->getApplication()->getWorkspace()->appPath('Config', $configFile);
				if (!\file_exists($configFile))
				{
					$configFile = null;
				}
			}
			unset($configFile);

			$configFiles = \array_filter($configFiles);
			if ($configFiles)
			{
				$configuration = new \Change\Configuration\Configuration($configFiles);
				$contexts = $configuration->getEntry('Contexts');
				if (\is_array($contexts))
				{
					$modelName = $model->getName();
					$i18nManager = $event->getApplicationServices()->getI18nManager();
					/** @var array $contexts */
					foreach ($contexts as $context => $contextData)
					{
						if (\is_array($contextData) && isset($contextData['models'])
							&& \is_array($contextData['models'])
							&& \in_array($modelName, $contextData['models'], true)
						)
						{
							$asideDirective = [
								'name' => 'rbs-aside-code',
								'attributes' => [
									['name' => 'context-id', 'value' => $context],
									['name' => 'document', 'value' => 'document']
								]
							];

							if (isset($contextData['icon']))
							{
								$asideDirective['attributes'][] = ['name' => 'icon', 'value' => $contextData['icon']];
							}

							if (isset($contextData['label']))
							{
								$asideDirective['attributes'][] = ['name' => 'context-label',
									'value' => $i18nManager->trans($contextData['label'])];
							}
							$attributes['asideDirectives'][] = $asideDirective;
						}
					}
					$event->setParam('attributes', $attributes);
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\AbstractModel $model
	 * @param array $views
	 * @return array
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function initializeView($model, $views)
	{
		$pluginManager = $this->getPluginManager();
		/** @var \Change\Plugins\Plugin $module */
		$module = $pluginManager->getModule($model->getVendorName(), $model->getShortModuleName());

		$filesGenerated = ['paths' => [], 'errors' => []];

		foreach ($views as $view => $generate)
		{
			if ($generate)
			{
				// Check if the file already exist.
				$docPath = \implode(DIRECTORY_SEPARATOR,
					[$module->getAbsolutePath(), 'Assets', 'Admin', 'Documents', $model->getShortName(), $view . '.twig']
				);
				if (\file_exists($docPath))
				{
					$filesGenerated['errors'][$view] = \ucfirst($view) . ' view file already exists at path ' . $docPath;
					continue;
				}

				$excludedProperties = null;
				if ($view === 'new' || $view === 'edit')
				{
					// refLCID selector will automatically added in the template for "new".
					$excludedProperties = ['id', 'model', 'creationDate', 'modificationDate', 'refLCID', 'LCID',
						'authorId', 'documentVersion', 'publicationStatus',
						'publicationSections', 'startPublication', 'endPublication', // Already present in publication panel.
						'active', 'startActivation', 'endActivation' // Already present in activation panel.
					];
				}
				elseif ($view === 'translate')
				{
					$excludedProperties = ['creationDate', 'modificationDate', 'LCID',
						'authorId', 'documentVersion', 'publicationStatus',
						'startPublication', 'endPublication', // Already present in publication panel.
						'active', 'startActivation', 'endActivation' // Already present in activation panel.
					];
				}
				$attributes = ['model' => $model];
				if ($excludedProperties)
				{
					// If the model is publishable and we are not in translation view, we exclude the title and the label from
					// properties because they will be automatically added in the template and synced with each other.
					if ($view !== 'translate' && $model->isPublishable())
					{
						$excludedProperties = array_merge($excludedProperties, ['label', 'title']);
					}
					$attributes['properties'] = $this->getFormModelProperties($model, $excludedProperties, $view === 'translate');
				}
				$loader = new \Twig\Loader\FilesystemLoader(__DIR__);
				$twig = new \Twig\Environment($loader);
				foreach ($this->getExtensions() as $extension)
				{
					$twig->addExtension($extension);
				}
				\Change\Stdlib\FileUtils::write($docPath, $twig->render('Assets/view/' . $view . '.twig', $attributes));

				$filesGenerated['paths'][$view] = $docPath;
			}
		}

		return $filesGenerated;
	}

	/**
	 * @param \Change\Documents\AbstractModel $model
	 * @param array $excludedProperties
	 * @param boolean $localizedOnly
	 * @return array
	 */
	protected function getFormModelProperties($model, $excludedProperties, $localizedOnly = false)
	{
		$properties = [];

		$modelProperties = $localizedOnly ? $model->getLocalizedProperties() : $model->getProperties();
		foreach ($modelProperties as $property)
		{
			$propertyName = $property->getName();
			if (!\in_array($propertyName, $excludedProperties, true) && !$property->getInternal() && !$property->getStateless())
			{
				$properties[] = [
					'name' => $propertyName,
					'type' => $this->getFormTypeFromModelType($property->getType()),
					'required' => $property->getRequired(),
					'modelName' => $model->getName(),
					'documentType' => $property->getDocumentType()
				];
			}
		}

		//required property first
		\usort($properties, static function ($a, $b) {
			if ($a['required'] && !$b['required'])
			{
				return -1;
			}
			if ($a['required'] && $b['required'])
			{
				return 0;
			}
			return 1;
		});

		return $properties;
	}

	protected function getFormTypeFromModelType($modelType)
	{
		switch ($modelType)
		{
			case 'String':
				return 'text';
			case 'LongString':
				return 'long-text';
			case 'RichText':
				return 'rich-text';
			case 'Document':
				return 'picker';
			case 'DocumentArray':
				return 'picker-multiple';
			case 'Boolean':
				return 'boolean';
			case 'Integer':
				return 'integer';
			case 'Float':
				return 'float';
			case 'Date':
			case 'DateTime':
				return 'date';
			default:
				return 'unknown';
		}
	}

	/**
	 * @return \Assetic\AssetManager
	 */
	public function getJsAssetManager()
	{
		return $this->jsAssetManager;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	protected function parseSections($menuJson)
	{
		$result = [];
		if (isset($menuJson['sections']) && \is_array($menuJson['sections']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['sections'] as $item)
			{
				if (isset($item['code']))
				{
					if (isset($item['label']) && \is_string($item['label']))
					{
						$item['label'] = $i18nManager->trans($item['label'], ['ucf']);
					}
					$result[$item['code']] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	private function parseEntries($menuJson)
	{
		$result = [];
		if (isset($menuJson['entries']) && \is_array($menuJson['entries']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['entries'] as $item)
			{
				if ($item['url'] ?? $item['name'])
				{
					if (isset($item['label']) && \is_string($item['label']))
					{
						$item['label'] = $i18nManager->trans($item['label'], ['ucf']);
					}
					$result[] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @param array $attributes
	 * @param array $order
	 */
	public function getSortedAttributes(&$attributes, $order)
	{
		foreach ($order as $type => $newOrder)
		{
			if (\array_key_exists($type, $attributes))
			{
				\usort($attributes[$type], static function ($a, $b) use ($newOrder) {
					if (\in_array($a['name'], $newOrder, true))
					{
						if (\in_array($b['name'], $newOrder, true))
						{
							return \array_search($a['name'], $newOrder, true) - \array_search($b['name'], $newOrder, true);
						}
						return -1;
					}
					if (\in_array($b['name'], $newOrder, true))
					{
						return 1;
					}
					return 0;
				});
			}
		}
	}

	/**
	 * @param string $type
	 * @return array
	 */
	public function getSettingsStructures($type)
	{
		$args = $this->getEventManager()->prepareArgs(['type' => $type]);
		$this->getEventManager()->trigger('getGenericSettingsStructures', $this, $args);
		return isset($args['structures']) && \is_array($args['structures']) ? $args['structures'] : [];
	}

	/**
	 * @param string $modelName
	 * @param string $searchString
	 * @param integer $limit
	 * @return \Change\Documents\AbstractDocument[]
	 */
	public function searchDocuments($modelName, $searchString, $limit = 10)
	{
		$args = $this->getEventManager()->prepareArgs([
			'modelName' => $modelName,
			'searchString' => $searchString,
			'limit' => $limit,
			'propertyNames' => ['label']
		]);
		$this->getEventManager()->trigger('searchDocuments', $this, $args);
		return isset($args['documents']) && \is_array($args['documents']) ? $args['documents'] : [];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onQuerySearchDocuments($event)
	{
		if ($event->getParam('query') !== null)
		{
			return;
		}

		$model = $event->getApplicationServices()->getModelManager()->getModelByName($event->getParam('modelName'));
		if (!$model)
		{
			return;
		}
		$propertyNames = [];
		foreach ($event->getParam('propertyNames') as $propertyName)
		{
			$property = $model->getProperty($propertyName);
			if ($property && !$property->getStateless() && $property->getType() === \Change\Documents\Property::TYPE_STRING)
			{
				$propertyNames[] = $propertyName;
			}
		}

		if (!count($propertyNames))
		{
			return;
		}

		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery($model);
		$restrictions = [];
		foreach ($propertyNames as $propertyName)
		{
			$restrictions[] = $query->like($propertyName, $event->getParam('searchString'));
			$query->addOrder($propertyName);
		}
		$query->orPredicates($restrictions);
		if (!\in_array('id', $propertyNames, true))
		{
			$query->addOrder('id');
		}
		$event->setParam('query', $query);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDefaultSearchDocuments($event)
	{
		if (is_array($event->getParam('documents')) || $event->getParam('query') === null)
		{
			return;
		}

		$query = $event->getParam('query');
		$event->setParam('documents', $query->getDocuments(0, $event->getParam('limit'))->toArray());
	}
}
