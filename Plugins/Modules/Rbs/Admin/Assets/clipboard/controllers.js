(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/clipboard', { templateUrl: 'Rbs/Admin/clipboard/list.twig', reloadOnSearch: false });
	}]);

	app.controller('Rbs_Admin_ClipboardController', ['$scope', 'RbsChange.Clipboard', ChangeAdminClipboardController]);
	function ChangeAdminClipboardController($scope, Clipboard) {
		$scope.clipboardItems = Clipboard.values;

		$scope.removeFromSelection = function(doc) {
			Clipboard.remove(doc);
		};

		$scope.clipboardList = {
			'removeFromClipboard': function($docs) {
				angular.forEach($docs, function(doc) {
					Clipboard.remove(doc);
				});
			},
			'clearClipboard': function() {
				Clipboard.clear();
			}
		};
	}
})();