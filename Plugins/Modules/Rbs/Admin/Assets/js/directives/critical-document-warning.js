/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsCriticalDocumentWarning
	 * @name rbsCriticalDocumentWarning
	 * @restrict A
	 *
	 * @description
	 * Displays a warning if the document passed in parameter is considered as critical
	 *
	 * @param {ChangeDocument} document a Document.
	 * @param {string=} additionalMessage a message to add to the standard message.
	 */
	angular.module('RbsChange').directive('rbsCriticalDocumentWarning', [function() {
		return {
			restrict: 'A',
			scope: {
				document: '='
			},
			templateUrl: 'Rbs/Admin/js/directives/critical-document-warning.twig',
			link: function(scope, elmts, attrs) {
				scope.additionalMessage = attrs.additionalMessage;
			}
		}
	}]);
})();