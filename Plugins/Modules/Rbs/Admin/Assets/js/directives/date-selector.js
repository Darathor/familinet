(function($, moment) {
	'use strict';

	var app = angular.module('RbsChange'),
		inputEl,
		isNativeDatePickerAvailable,
		tzPromises = {};

	// Detect native date picker.
	/*
	 As input[date] is only supported by few browsers in 2016
	 and when supported it's quite bugged
	 and angular form validation for this field is quite bugged too
	 we avoid using it for now
	 */
	//inputEl = document.createElement("input");
	//inputEl.setAttribute("type", "date");
	//isNativeDatePickerAvailable = inputEl.type === 'date';
	isNativeDatePickerAvailable = false;

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDateSelector
	 * @name rbsDateSelector
	 * @restrict AE
	 *
	 * @description
	 * The date selector.
	 * TODO params.
	 */
	app.directive('rbsDateSelector', ['RbsChange.Dialog', '$rootScope', 'RbsChange.i18n', 'RbsChange.Settings', '$q',
		function(Dialog, $rootScope, i18n, Settings, $q) {
			return {
				restrict: 'AE',
				templateUrl: 'Rbs/Admin/js/directives/date-selector.twig',
				require: 'ngModel',
				replace: true,
				scope: {
					hideTime: '@'
				},

				link: function(scope, elm, attrs, ngModel) {
					scope.isNativeDatePickerAvailable = isNativeDatePickerAvailable;

					var dateRole = isNativeDatePickerAvailable ? 'input-date' : 'input-date-native';
					var dInput = $(elm).find('[data-role="' + dateRole + '"]').first();
					var hInput = $(elm).find('[data-role="input-hour"]').first();
					var mInput = $(elm).find('[data-role="input-minute"]').first();

					function setTimeZone(tz) {
						ngModel.$render();
					}

					function updateDate() {
						ngModel.$setViewValue(getFullDate());
					}

					// Merge the date coming from the "datepicker" and the hour/minute information coming from the
					// two additional input fields. The result is a Date object correctly set.
					function getFullDate() {
						var date;
						if (isNativeDatePickerAvailable) {
							date = scope.dateNative;
						}
						else {
							var inputValue = scope.date;
							date = (inputValue && inputValue != '') ? new Date(inputValue) : null;
						}

						if (!date) {
							return null;
						}

						if (scope.hideTime) {
							return moment.utc(date).hours(0).minutes(0)
								.second(0).milliseconds(0).toDate();
						}
						else {
							return moment.utc(date).tz(scope.timeZone).hours(parseInt(hInput.val(), 10))
								.minutes(parseInt(mInput.val(), 10))
								.second(0).milliseconds(0).toDate();
						}
					}

					// viewValue => modelValue
					ngModel.$parsers.unshift(function(viewValue) {

						function addZero(i) {
							return ((i < 10) ? '0' : '') + i;
						}

						if (viewValue) {
							if (scope.hideTime) {
								return viewValue.getFullYear()
									+ '-' + addZero(viewValue.getMonth() + 1)
									+ '-' + addZero(viewValue.getDate())
									+ 'T00:00:00+00:00';
							}
							else {
								return viewValue.getUTCFullYear()
									+ '-' + addZero(viewValue.getUTCMonth() + 1)
									+ '-' + addZero(viewValue.getUTCDate())
									+ 'T' + addZero(viewValue.getUTCHours())
									+ ':' + addZero(viewValue.getUTCMinutes())
									+ ':00+00:00';
							}
						}
						return null;
					});

					// modelValue => viewValue
					ngModel.$formatters.unshift(function(modelValue) {
						if (modelValue) {
							return new Date(modelValue);
						}
						return null;
					});

					scope.timeZone = Settings.get('TimeZone');

					// If 'id' and 'input-id' attributes are found are equal, move this id to the real input field
					// so that the binding with the '<label/>' element works as expected
					// (see Directives in 'Rbs/Admin/Assets/js/directives/form-fields.js').
					if (attrs.id && attrs.id === attrs['inputId']) {
						dInput.attr('id', attrs.id);
						elm.removeAttr('id');
						elm.removeAttr('input-id');
					}

					scope.openTimeZoneSelector = function($event) {
						Dialog.embed(
							$(elm).find('.timeZoneSelectorContainer'),
							{
								"title": i18n.trans('m.rbs.admin.admin.time_zone_selector_title | ucf'),
								"contents": '<rbs-time-zone-selector time-zone="timeZone"></rbs-time-zone-selector>'
							},
							scope,
							{ "pointedElement": $event.target }
						);
					};

					scope.$on('Change:TimeZoneChanged', function(event, tz) {
						scope.timeZone = tz;
					});

					scope.$watch('timeZone', function(newValue, oldValue) {
						if (newValue !== oldValue && ngModel.$viewValue) {
							setTimeZone(newValue);
						}
					}, true);

					ngModel.$render = function() {
						if (ngModel.$viewValue) {
							var date = moment.utc(ngModel.$viewValue).tz(scope.timeZone);
							scope.dateNative = date.toDate();
							scope.date = date.toDate();

							if (scope.hideTime) {
								scope.hours = 0;
								scope.minutes = 0;
							}
							else {
								scope.hours = date.hours();
								scope.minutes = date.minutes();
							}
						}
						else {
							scope.dateNative = null;
							scope.date = null;
							scope.hours = 0;
							scope.minutes = 0;
						}
					};

					ngModel.$render();

					scope.updateDate = updateDate;

					scope.setDateToNow = function() {
						ngModel.$setViewValue(new Date());
						ngModel.$render();
					};

					scope.clearDate = function() {
						ngModel.$setViewValue(null);
						ngModel.$render();
					};

					if (!isNativeDatePickerAvailable) {
						scope.opened = false;
						scope.openPicker = function() {
							scope.opened = true;
						};
					}

				}
			};
		}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsTimeZoneSelector
	 * @name rbsTimeZoneSelector
	 * @restrict AE
	 *
	 * @description
	 * The time zone selector.
	 * TODO params.
	 */
	app.directive('rbsTimeZoneSelector', ['$rootScope', 'RbsChange.Dialog', 'RbsChange.Settings', function($rootScope, Dialog, Settings) {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Admin/js/directives/time-zone-selector.twig',
			replace: true,
			scope: {
				timeZone: '='
			},

			link: {
				pre: function(scope) {
					scope.selected = {
						timeZone: scope.timeZone ? scope.timeZone : null
					}
				},
				post: function(scope, elm) {
					scope.$watch('selected.timeZone', function(tz) {
						if (tz) {
							var now = moment().tz(tz);
							scope.formattedTz = now.format('LLLL');
							scope.tzOffset = now.format('Z');
						}
					}, true);

					scope.$on('$destroy', function() {
						Dialog.closeEmbedded();
					});

					scope.select = function() {
						scope.timeZone = scope.selected.timeZone;
						$rootScope.$broadcast('Change:TimeZoneChanged', scope.timeZone);
						if (scope['saveSetting']) {
							Settings.set('TimeZone', scope.timeZone, true).then(
								function() {
									Dialog.closeEmbedded();
								},
								function(result) { console.error(result); }
							);
						}
						else {
							Dialog.closeEmbedded();
						}
					};

					scope.cancel = function() {
						Dialog.closeEmbedded();
					}
				}
			}
		};
	}]);
})(window.jQuery, moment);