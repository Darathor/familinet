(function($) {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorButtonBar', ['$http', '$rootScope', 'RbsChange.Dialog', 'RbsChange.Utils',
		'RbsChange.Actions', 'RbsChange.Events', 'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.NotificationCenter',
		function($http, $rootScope, Dialog, Utils, Actions, Events, i18n, REST, NotificationCenter) {
			return {
				restrict: 'E',
				transclude: true,
				templateUrl: 'Rbs/Admin/js/directives/document-editor-button-bar.twig',
				require: '^rbsDocumentEditorBase',

				link: function(scope, element, attrs) {
					scope.allowDelete = false;
					scope.allowDeleteLocalized = false;
					scope.advancedDiffs = true;
					scope.showChanges = false;

					function updateDisableDelete() {
						var isRefLocalization = scope.document.refLCID === scope.document.LCID;
						var isNewLocalization = true;
						if (!isRefLocalization && scope.document.META$) {
							angular.forEach(scope.document.META$.locales, function (locale) {
								if (locale.id === scope.document.LCID) {
									isNewLocalization = false;
								}
							});
						}

						scope.allowDelete = isRefLocalization && Utils.isDocument(scope.document) && !scope.document.isNew() &&
							scope.document.isActionAvailable('delete') && attrs.disableDelete !== 'true';
						scope.allowDeleteLocalized = !isRefLocalization && Utils.isDocument(scope.document) && !isNewLocalization &&
							scope.document.isActionAvailable('deleteLocalized') && attrs.disableDelete !== 'true';
					}

					attrs.$observe('disableDelete', updateDisableDelete);
					scope.$on(Events.EditorReady, updateDisableDelete);
					scope.$on('Change:DocumentSaved', updateDisableDelete);

					scope.confirmReset = function($event) {
						Dialog.confirmEmbed(
							element.find('.confirmation-area'),
							i18n.trans('m.rbs.admin.admin.confirm_restore | ucf'),
							i18n.trans('m.rbs.admin.admin.confirm_restore_message | ucf'),
							scope,
							{
								'pointedElement': $($event.target),
								'primaryButtonText': i18n.trans('m.rbs.admin.admin.restore_data_button | ucf'),
								'cssClass': 'warning'
							}
						).then(
							function() {
								scope.reset();
							},
							function(result) { console.error(result); }
						);
					};

					scope.confirmDelete = function($event) {
						var text = i18n.trans('m.rbs.admin.admin.confirm_delete_message | ucf');
						if (attrs.warningOnDeleteMessage) {
							text += '<div class="text-danger"><span class="icon icon-warning-sign"></span> <strong>' + attrs.warningOnDeleteMessage +
								'</strong></div>';
						}
						Dialog.confirmEmbed(
							element.find('.confirmation-area'),
							i18n.trans('m.rbs.admin.admin.confirm_delete | ucf'),
							text,
							scope,
							{
								'pointedElement': $($event.target),
								'primaryButtonText': i18n.trans('m.rbs.admin.admin.delete_data_button | ucf'),
								'primaryButtonClass': 'btn-danger',
								'cssClass': 'danger'
							}
						).then(
							function() {
								REST.delete(scope.document).then(
									function() {
										scope.goBack();
									},
									function(data) {
										if (data && data.message) {
											NotificationCenter.error(data.message, null, data.code);
										}
										else {
											console.error(data);
										}
									}
								);
							},
							function(result) {
								// Do nothing.
							}
						);
					};

					scope.confirmDeleteLocalized = function($event) {
						var text = i18n.trans('m.rbs.admin.admin.confirm_delete_localized_message | ucf');
						if (attrs.warningOnDeleteMessage) {
							text += '<div class="text-danger"><span class="icon icon-warning-sign"></span> <strong>' + attrs.warningOnDeleteMessage +
								'</strong></div>';
						}
						Dialog.confirmEmbed(
							element.find('.confirmation-area'),
							i18n.trans('m.rbs.admin.admin.confirm_delete | ucf'),
							text,
							scope,
							{
								'pointedElement': $($event.target),
								'primaryButtonText': i18n.trans('m.rbs.admin.admin.delete_localized_data_button | ucf'),
								'primaryButtonClass': 'btn-danger',
								'cssClass': 'danger'
							}
						).then(
							function() {
								$http.delete(scope.document.getActionUrl('deleteLocalized')).then(
									function() {
										scope.goBack();
									},
									function(data) {
										if (data && data.message) {
											NotificationCenter.error(data.message, null, data.code);
										}
										else {
											console.error(data);
										}
									}
								);
							},
							function(result) {
								// Do nothing.
							}
						);
					};

					scope.$on('Change:EditorPreSubmit', function(event, doc, promises) {
						if (Utils.hasCorrection(scope.document)) {
							promises.push(Dialog.confirmEmbed(
								element.find('.confirmation-area'),
								i18n.trans('m.rbs.admin.admin.confirm_update_correction | ucf'),
								i18n.trans('m.rbs.admin.admin.confirm_update_correction_message | ucf'),
								scope,
								{
									'pointedElement': $(element).find('[data-role=save]').first(),
									'cssClass': 'warning'
								}
							));
						}
					});
				}
			};
		}]);
})(window.jQuery);