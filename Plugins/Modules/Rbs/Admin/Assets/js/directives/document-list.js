/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange');

	var __columns = {},
		__preview = {},
		__gridItems = {},
		__quickActions = {},
		__actions = {},
		DEFAULT_PAGE_SIZE = 20,
		DEFAULT_ACTIONS = 'delete(icon)',
		DEFAULT_PUBLISHABLE_ACTIONS = 'requestValidation publicationValidation freeze(icon) unfreeze(icon) delete(icon)',
		DEFAULT_ACTIVABLE_ACTIONS = 'activate deactivate delete(icon)';

	var lists = {
		count: 0,
		columns: {},
		localActions: {}
	};

	function getListId(element) {
		var listId = element.attr('dlid') || element.attr('data-list-id');
		if (!listId) {
			listId = lists.count++;
			element.attr('data-list-id', listId);
		}
		return listId;
	}

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDocumentList
	 * @name rbsDocumentList
	 * @restrict AE
	 * @scope
	 *
	 * @param {string} model The Model name of the Documents to display in the list, in the form "Vendor_Plugin_Name".
	 * @param {Object=} extend Object that extends the logic of the <code>&lt;rbs-document-list/&gt;</code>, used
	 * for communication between the <em>isolated scope</em> of the list and your surrounding controller.
	 * @param {Object=} loadQuery Query object used to load the collection of Documents.
	 * @param {string=} collectionUrl URL of a REST service that delivers a collection of Documents.
	 * @param {Array|Object=} collection Collection of Documents.
	 * @param {Function=} onPreview Function called when a preview is requested on a Document.
	 * @param {Function=} onReload Function called when a reload is requested, in case of `collection`.
	 * @param {string=} useProperties A list of additional wanted properties from the documents.
	 *
	 * @description
	 * Displays a list of Documents in a table.
	 *
	 * Children Directives:
	 *
	 * - `rbs-column`
	 * - `rbs-preview`
	 * - `rbs-action`
	 *
	 * For more information about how to build Documents lists, please read the
	 * {@link lists/index guide to Documents lists}.
	 *
	 * @example
	 * ```html
	 *     <rbs-document-list model="...">
	 *         <rbs-column name="label"></rbs-column>
	 *         <rbs-column></rbs-column>
	 *         ...
	 *         <rbs-grid-item></<rbs-grid-item>
	 *         <rbs-preview></rbs-preview>
	 *         <rbs-action></rbs-action>
	 *     </div>
	 * ```
	 */
	app.directive('rbsDocumentList', [
		'$q', '$rootScope', '$location', '$compile', '$cacheFactory', 'RbsChange.i18n', 'RbsChange.REST', 'RbsChange.Utils',
		'RbsChange.ArrayUtils', 'RbsChange.Actions', 'RbsChange.NotificationCenter', 'RbsChange.Settings', 'RbsChange.Events',
		'RbsChange.Navigation', 'RbsChange.ErrorFormatter', 'localStorageService', documentListDirectiveFn
	]);

	function documentListDirectiveFn($q, $rootScope, $location, $compile, $cacheFactory, i18n, REST, Utils, ArrayUtils, Actions,
		NotificationCenter, Settings, Events, Navigation, ErrorFormatter, localStorageService) {

		/**
		 * Prepare local actions data.
		 */
		function initLocalActions(listId) {
			var localActions = {};
			angular.forEach(__actions[listId], function(action) {
				if (!action.name) {
					throw new Error("Actions defined in <rbs-document-list/> should have a 'name' parameter.");
				}
				if (localActions[action.name]) {
					throw new Error("Parameter 'name' for actions defined in <rbs-document-list/> should be unique.");
				}

				localActions[action.name] = action;
				Actions.register({
					name: (listId + '_' + action.name),
					models: action.models || '*',
					description: action.description,
					label: action.label,
					icon: action.icon,
					selection: (action.selection && /\d+/.test(action.selection)) ? parseInt(action.selection, 10) : action.selection,
					loading: action.loading === 'true',

					execute: ['$extend', '$docs', '$embedDialog', '$target', function($extend, $docs, $embedDialog, $target) {
						if (angular.isFunction($extend[action.name])) {
							return $extend[action.name]($docs, $embedDialog, $target);
						}
						else {
							throw new Error("Method '" + this.name + "' is not defined in '$extend'.");
						}
					}]
				});
			});
			delete __actions[listId];

			lists.localActions[listId] = localActions;
			return localActions;
		}

		/**
		 * Prepare columns data.
		 */
		function initColumns(listId, attrs, undefinedColumnLabels) {
			var result = {
				columns: {},
				preview: false
			};

			// Prepare preview
			if (!__preview[listId] && attrs.preview === 'true') {
				__preview[listId] = {};
			}

			if (__preview[listId]) {
				result.preview = true;
			}

			var columns = __columns[listId];

			if (!columns) {
				throw new Error("Could not find any column for <rbs-document-list/> directive with id='" + listId +
					"'. We are sure you want something to be displayed in this list :)");
			}

			// Status column
			if (attrs.publishable === 'true') {
				columns.unshift({
					name: 'publicationStatus',
					align: 'center',
					width: '44px',
					label: '<abbr title="' + i18n.trans('m.rbs.admin.admin.status | ucf') + '">' +
					i18n.trans('m.rbs.admin.admin.status_minified | ucf') + '</abbr>',
					textLabel: i18n.trans('m.rbs.admin.admin.status | ucf'),
					content: '<a href="" data-ng-click="showWorkflow($index)"><rbs-status ng-model="doc" /></a>',
					dummy: true
				});
			}

			// Correction column
			if (attrs.correction === 'true') {
				columns.unshift({
					name: 'correction',
					align: 'center',
					width: '44px',
					label: '<abbr title="' + i18n.trans('m.rbs.admin.admin.correction | ucf') + '">' +
					i18n.trans('m.rbs.admin.admin.correction_minified | ucf') + '</abbr>',
					textLabel: i18n.trans('m.rbs.admin.admin.correction | ucf'),
					content: '<a href="" data-ng-click="showWorkflow($index)"><rbs-bullet-correction ng-model="doc" /></a>',
					dummy: true
				});
			}

			// Selectable column
			if (angular.isUndefined(attrs.selectable) || attrs.selectable === 'true') {
				columns.unshift({
					name: 'selectable',
					label: '<input type="checkbox" data-ng-click="$event.stopPropagation()" data-ng-model="selection.all" />',
					content: '<input type="checkbox" data-ng-click="$event.stopPropagation()" data-ng-model="selection[doc.id]" />',
					dummy: true
				});
			}

			// Tree navigation link
			if (attrs.tree) {
				columns.push({
					name: 'navigation'
				});
			}

			// Modification Date column
			if (angular.isUndefined(attrs.modificationDate) || attrs.modificationDate === 'true') {
				columns.push({
					name: 'modificationDate',
					label: i18n.trans('m.rbs.admin.admin.modification_date | ucf'),
					format: 'date'
				});
			}

			lists.columns[listId] = [];

			// Activable switch column
			if (attrs.activable === 'true') {
				columns.push({
					name: 'publicationStatusSwitch',
					align: 'center',
					width: '90px',
					label: i18n.trans('m.rbs.admin.admin.activated | ucf'),
					content: '(= doc.active | rbsBoolean =)',
					dummy: true
				});
			}

			// Loop through all the columns and build header et body cells.
			while (columns.length) {
				var column = columns.shift(0);
				var p = column.name.indexOf('.');
				if (p === -1) {
					column.valuePath = column.name;
				}
				else {
					column.valuePath = column.name;
					column.name = column.name.substring(0, p);
				}

				if (column.name === 'navigation') {
					column.content = '<a data-ng-if="doc.hasUrl(\'tree\')" href="" data-ng-href="(= doc|rbsURL:\'tree\'=)"><i class="icon-circle-arrow-right icon-large"></i></a>';
					column.width = '40px';
					column.label = 'Nav.'; // TODO
					column.align = 'center';
				}

				if (!column.sort) {
					column.sort = column.name;
				}

				switch (column.format) {
					case 'number' :
						column.valuePath += '|number';
						if (!column.align) {
							column.align = 'right';
						}
						break;

					case 'date' :
						if (!column.width) {
							column.width = '180px';
						}
						column.content = '<time data-column="' + column.name + '" display="(= dateDisplay.' + column.name +
							' =)" datetime="(=doc.' + column.valuePath + '=)"></time>';
						break;
				}

				result.columns[column.name] = column;
				lists.columns[listId].push(column);

				// Check if the label has been provided or not.
				// If one at least label has not been provided, the Model's information will be
				// loaded to automatically set the columns' header text.
				if (!column.label) {
					undefinedColumnLabels.push(column.name);
				}
			}

			delete __columns[listId];

			return result;
		}

		/**
		 * <rbs-document-list/> directive.
		 */
		return {
			restrict: 'AE',
			transclude: true,
			templateUrl: 'Rbs/Admin/js/directives/document-list.twig',

			scope: {
				'filterCollection': '=',
				'loadQuery': '=',
				'onPreview': '&',
				'onReload': '=',
				'collectionUrl': '@',
				'externalCollection': '=collection',
				'extend': '=',
				'model': '@'
			},

			controller: ['$scope', function(scope) {
				//
				// Pagination.
				//
				scope.pagination = {
					itemsPerPage: Settings.get('pagingSize', DEFAULT_PAGE_SIZE),
					currentPage: 1,
					totalItems: 0,
					getOffset: function() {
						return (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage;
					}
				};

				function refreshIfWatchChanged(value, oldValue) {
					if (value != oldValue) {
						$location.search('limit', scope.pagination.itemsPerPage);
						$location.search('offset', scope.pagination.getOffset());
						scope.reload();
					}
				}

				scope.$watch('pagination.itemsPerPage', refreshIfWatchChanged);
				scope.$watch('pagination.currentPage', refreshIfWatchChanged);
			}],

			link: {
				post: function(scope, elm, attrs) {
					var undefinedColumnLabels = [], localActions, columnResult;
					var queryObject, search, columnNames, currentPath, previewCache, self = this;

					var listId = scope.listId = getListId(elm.find('.document-list-main'));
					localActions = initLocalActions(listId);
					columnResult = initColumns(listId, attrs, undefinedColumnLabels);
					scope.gridModeAvailable = !!__gridItems[listId];

					scope.stripedRows = attrs.stripedRows !== 'false';
					scope.hoverRows = attrs.hoverRows !== 'false';
					scope.animationClass = attrs.animationClass;
					scope.selection = { all: false };

					if (attrs.display) {
						scope.viewMode = attrs.display;
					}
					else {
						scope.viewMode = scope.gridModeAvailable ? Settings.get('documentListViewMode', 'grid') : 'list';
					}

					var html = '<div data-rbs-document-list-table="" data-ng-if="viewMode == \'list\'" data-list-id="' + listId + '"></div>';
					if (scope.gridModeAvailable) {
						html += '<div data-rbs-document-list-grid="" data-ng-if="viewMode == \'grid\'" data-list-id="' + listId + '"></div>';
					}
					var listBody = $compile(html)(scope);
					elm.find('.document-list-body').append(listBody);

					scope.$watch('selectedDocuments', function(selectedDocuments) {
						scope.$broadcast('checkDocumentSelectionState', selectedDocuments);
					});

					scope.$on('triggerCheckDocumentSelectionState', function(event) {
						scope.$broadcast('checkDocumentSelectionState', scope.selectedDocuments);
					});

					scope.$emit('Change:DocumentList:' + listId + ':Ready', scope);
					scope.collection = [];

					scope.columns = columnResult.columns;

					scope.dateDisplay = {};
					scope.previewAvailable = columnResult.preview;
					scope.embeddedActionsOptionsContainerId = 'EAOC_' + listId;
					scope.useToolBar = attrs.toolbar !== 'false';

					scope.deleteConfirm = {};

					scope.setViewMode = function(viewMode) {
						scope.viewMode = viewMode;
					};

					scope.askDeleteConfirmation = function($index, $event) {
						scope.deleteConfirm[$index] = true;
						$event.stopPropagation();
					};

					scope.cancelDelete = function($index, $event) {
						delete scope.deleteConfirm[$index];
						$event.stopPropagation();
					};

					jQuery('body').on('click.rbs.document.list', function() {
						scope.hideQuickActions(currentQuickActionsIndex);
					});

					//
					// Selection of Document(s) from a DocumentPicker
					//
					scope.selectionContext = null;
					scope.selectionContextDocuments = [];

					scope.$on('$locationChangeSuccess', function() {
						var navCtx = Navigation.getCurrentContext();
						if (navCtx === undefined) {
							navCtx = null;
						}
						if (navCtx !== scope.selectionContext) {
							if (scope.selectionContext) {
								scope.selectionContextDocuments = [];
							}
							scope.selectionContext = navCtx ? navCtx : null;
						}
					});

					var navCtx = Navigation.getCurrentContext();
					if (navCtx && navCtx.isSelection()) {
						scope.selectionContext = navCtx;
						scope.selectionContextAppend = function(doc, commit) {
							var docs = doc ? [doc] : scope.selectedDocuments;
							if (scope.selectionContext.param('multiple')) {
								angular.forEach(docs, function(doc) {
									if (!ArrayUtils.documentInArray(doc, scope.selectionContextDocuments)) {
										scope.selectionContextDocuments.push(doc);
									}
								});
							}
							else {
								scope.selectionContextDocuments.length = 1;
								scope.selectionContextDocuments[0] = docs[0];
							}

							if (commit) {
								scope.selectionContextResolve();
							}
						};

						scope.selectionContextClear = function() {
							scope.selectionContextDocuments.length = 0;
						};

						scope.selectionContextResolve = function() {
							if (scope.selectionContext.param('multiple')) {
								Navigation.setSelectionContextValue(scope.selectionContextDocuments);
							}
							else {
								Navigation.setSelectionContextValue(
									scope.selectionContextDocuments.length ? scope.selectionContextDocuments[0] : null);
							}
						};

						scope.selectionContextReject = function() {
							Navigation.setSelectionContextValue();
						};
					}

					//
					// data-* attributes
					//

					// Watch for changes on 'data-*' attributes, and transpose them into the 'data' object of the scope.
					scope.data = {};
					angular.forEach(elm.data(), function(value, key) {
						if (key === 'columns' || key == 'listId') {
							return;
						}
						scope.$parent.$watch(value, function(v) {
							scope.data[key] = v;
						}, true);
					});

					// Load Model's information and update the columns' header with the correct property label.
					if (undefinedColumnLabels.length && attrs.model) {
						REST.modelInfo(attrs.model).then(
							function(modelInfo) {
								scope.sortable = modelInfo.collections['sortableBy'];
								angular.forEach(undefinedColumnLabels, function(columnName) {
									if (columnName in modelInfo.properties) {
										scope.columns[columnName].label = modelInfo.properties[columnName].label;
									}
								});
							},
							function(result) { console.error(result); }
						);
					}

					// The list listens to these events: 'Change:DocumentList:call' and 'Change:DocumentList:<listId>:call'
					scope.$on('Change:DocumentList:call',
						function(event, args) { onCall(event, args, 'Change:DocumentList:call') });
					scope.$on('Change:DocumentList:' + listId + ':call',
						function(event, args) { onCall(event, args, 'Change:DocumentList:' + listId + ':call') });

					function onCall(event, args, eventName) {
						if (angular.isFunction(scope[args.method])) {
							var q, result;

							// Call the method on the Scope...
							result = scope[args.method].apply(self, args.params || []);

							// Ensure that "args.promises" is an Array.
							if (!angular.isArray(args.promises)) {
								args.promises = [];
							}

							// Store the result as a Promise in the "args.promises" Array.
							if (result && angular.isFunction(result.then)) {
								args.promises.push(result);
							}
							else {
								q = $q.defer();
								q.resolve(result);
								args.promises.push(q.promise);
							}
						}
						else {
							console.warn("Received event '" + eventName + "' but no method '" + args.method +
								"' is defined in the DocumentList's scope.");
						}
					}

					// Save selected view mode is user's settings.
					scope.$watch('viewMode', function(value) {
						Settings.set('documentListViewMode', value);
					}, true);

					scope.hasColumn = function(columnName) {
						return angular.isObject(scope.columns[columnName]);
					};

					scope.$watch('selectionContext', function(selectionContext) {
						scope.selectionEnabled = scope.hasColumn('selectable') &&
							(!selectionContext || !selectionContext.isSelection() || selectionContext.param('multiple'));
						scope.selection = { all: scope.selection.all };
					});

					//
					// Document selection.
					//
					function updateSelectedDocuments() {
						var selectedDocuments = [];
						var selection = { all: scope.selection.all };
						if (scope.selectionEnabled) {
							angular.forEach(scope.collection, function(doc) {
								selection[doc.id] = false;
								if (scope.selection[doc.id]) {
									selectedDocuments.push(doc);
									selection[doc.id] = true;
								}
							});
						}
						scope.selection = selection;
						scope.selectedDocuments = selectedDocuments;
						scope.$emit('Change:DocumentList:' + listId + ':CollectionChanged', scope.collection);
					}

					scope.$watch('selection.all', function() {
						angular.forEach(scope.selection, function(selected, docId) {
							scope.selection[docId] = scope.selection.all;
						});
					}, true);

					scope.$watchCollection('collection', updateSelectedDocuments);
					scope.$watch('selection', updateSelectedDocuments, true);

					scope.deselectAll = function() {
						scope.selection.all = false;
					};

					//
					// Actions.
					//

					// Locally defined actions.
					var actionList = elm.is('[actions]') ? attrs.actions : 'default';
					angular.forEach(localActions, function(action) {
						if (actionList !== action.name && actionList.indexOf(action.name + ' ') === -1 &&
							actionList.indexOf(' ' + action.name) === -1) {
							if (actionList.length) {
								actionList += ' ';
							}
							actionList += action.name;
						}
					});

					scope.actions = [];
					if (actionList.length) {
						if (attrs.publishable === 'true') {
							actionList = actionList.replace('default', DEFAULT_PUBLISHABLE_ACTIONS);
						}
						else if (attrs.activable === 'true') {
							actionList = actionList.replace('default', DEFAULT_ACTIVABLE_ACTIONS);
						}
						else {
							actionList = actionList.replace('default', DEFAULT_ACTIONS);
						}

						angular.forEach(actionList.split(/ +/), function(action) {
							// Locally defined action?
							if (localActions[action]) {
								var actionId = listId + '_' + action;
								if (localActions[action].display) {
									actionId += '(' + localActions[action].display + ')';
								}
								else if (localActions[action].icon) {
									actionId += '(icon+label)';
								}
								scope.actions.push({
									"type": "single",
									"name": actionId
								});
							}
							else {
								scope.actions.push({
									"type": "single",
									"name": action
								});
							}
						});
					}

					scope.executeAction = function(actionName, doc, $event) {
						return Actions.execute(actionName, {
							'$docs': [doc],
							'$target': $event.target,
							'$scope': scope,
							'$extend': scope.extend
						});
					};

					// Unregisters the locally defined actions from the Actions service (called from $on('$destroy')).
					function unregisterLocalActions() {
						angular.forEach(localActions, function(action) {
							Actions.unregister(listId + '_' + action.name);
						});
					}

					scope.remove = function(doc) {
						REST['delete'](doc).then(
							function() {
								scope.deselectAll();
								reload();
							},
							function(data) {
								if (data && data.message) {
									NotificationCenter.error(data.message, null, data.code);
								}
								else {
									console.error(data);
								}
							}
						);
					};

					scope.refresh = function() {
						return reload();
					};

					scope.reload = function() {
						return reload();
					};

					scope.isLastCreated = function(doc) {
						return REST.isLastCreated(doc);
					};

					// Complete actionMethods object to share methods.
					if (angular.isObject(scope.extend)) {
						// Refresh.
						if (!scope.extend.hasOwnProperty('refresh')) {
							scope.extend.refresh = function() {
								reload();
							}
						}
					}

					//
					// Embedded preview.
					//

					function doPreview(index, currentItem, newItem) {
						var previewPromises = [];
						delete currentItem.__dlPreviewLoading;
						newItem.__dlPreview = true;
						// Store reference to the original document.
						newItem.__document = currentItem;

						$rootScope.$broadcast(Events.DocumentListPreview, {
							document: newItem,
							promises: previewPromises
						});

						if (newItem.id) {
							previewCache.put(newItem.id, newItem);
						}

						function terminatePreview() {
							scope.collection.splice(index + 1, 0, newItem);
						}

						if (previewPromises.length) {
							$q.all(previewPromises).then(
								terminatePreview,
								function(result) { console.error(result); }
							);
						}
						else {
							terminatePreview();
						}

					}

					previewCache = $cacheFactory('chgRbsDocumentListPreview_' + listId);
					scope.$on('$destroy', function() {
						previewCache.destroy();
						unregisterLocalActions();
					});

					scope.preview = function(index, $event) {
						if ($event) {
							$event.preventDefault();
						}

						if (angular.isObject(index)) {
							if (scope.isPreview(index)) {
								ArrayUtils.removeValue(scope.collection, index);
								return;
							}
							index = scope.collection.indexOf(index);
						}

						var current = scope.collection[index], cachedDoc;

						if (scope.hasPreview(index)) {
							scope.collection.splice(index + 1, 1);
							return;
						}

						cachedDoc = ($event && $event.shiftKey) ? null : previewCache.get(current.id);
						if (cachedDoc) {
							scope.collection.splice(index + 1, 0, cachedDoc);
						}
						else {
							current.__dlPreviewLoading = true;
							if (Utils.isDocument(current)) {
								REST.resource(current).then(
									function(doc) {
										REST.modelInfo(doc).then(
											function(modelInfo) {
												doc.META$.modelInfo = modelInfo;
												doPreview(index, current, doc);
											},
											function(result) { console.error(result); }
										);
									},
									function(result) { console.error(result); }
								);
							}
							else {
								doPreview(index, current, angular.copy(current));
							}
						}
					};

					scope.isPreview = function(doc) {
						return doc && doc.__dlPreview === true;
					};

					scope.isPreviewReady = function(doc) {
						return !doc || !doc.__dlPreviewLoading;
					};

					scope.isPreviewLoading = function(doc) {
						return doc && doc.__dlPreviewLoading;
					};

					scope.hasPreview = function(index) {
						if (angular.isObject(index)) {
							index = scope.collection.indexOf(index);
						}
						var current, next;
						current = scope.collection[index];
						next = (scope.collection.length > (index + 1)) ? scope.collection[index + 1] : null;
						return !!(next && next.__dlPreview && next.id === current.id);
					};

					scope.closeAllPreviews = function() {
						var i = 0;
						while (i < scope.collection.length) {
							if (scope.isPreview(scope.collection[i])) {
								scope.collection.splice(i, 1);
							}
							else {
								i++;
							}
						}
					};

					scope.isNormalCell = function(doc) {
						return !scope.isPreview(doc) && !scope.isWorkflow(doc);
					};

					scope.showWorkflow = function(index) {
						var current = scope.collection[index],
							newItem;

						// Close workflow UI
						if (scope.hasWorkflow(current)) {
							delete current.__hasWorkflow;
							scope.collection.splice(index + 1, 1);
						}
						// Show workflow UI
						else {
							current.__hasWorkflow = true;
							newItem = angular.copy(current);
							newItem.__document = current;
							newItem.__workflow = true;
							scope.collection.splice(index + 1, 0, newItem);
						}
					};

					scope.closeWorkflow = function(index) {
						var current = scope.collection[index - 1];
						delete current.__hasWorkflow;
						scope.collection.splice(index, 1);
					};

					scope.isWorkflow = function(doc) {
						return doc && doc.__workflow === true;
					};

					scope.hasWorkflow = function(doc) {
						if (angular.isNumber(doc)) {
							doc = scope.collection[doc];
						}
						return doc && doc.__hasWorkflow === true;
					};

					/**
					 * Save the given doc.
					 * @param doc
					 */
					scope.save = function(doc) {
						REST.save(doc).then(
							function(savedDoc) {
								angular.extend(doc, savedDoc);
							},
							function(result) { console.error(result); }
						);
					};

					//
					// Sort.
					//

					// Compute columns list to give to the REST server.
					columnNames = [];
					angular.forEach(scope.columns, function(column) {
						if (!column.dummy) {
							columnNames.push(column.name);
						}
						if (column.tags) {
							columnNames.push('tags');
						}

						if (column.format === 'date') {
							scope.dateDisplay[column.name] = '';
						}

					});
					if (attrs.useProperties) {
						angular.forEach(attrs.useProperties.split(/[\s,]+/), function(name) {
							if (columnNames.indexOf(name) === -1) {
								columnNames.push(name);
							}
						});
					}

					function getStoredSort() {
						if (attrs.model && scope.filterCollection) {
							var local = localStorageService.get('list_sort_' + attrs.model);
							if (local) {
								return angular.fromJson(local);
							}
						}
						return null;
					}

					function removeStoredSort() {
						if (attrs.model && scope.filterCollection) {
							localStorageService.remove('list_sort_' + attrs.model);
						}
					}

					function saveStoredSort(sort) {
						if (attrs.model && scope.filterCollection) {
							if (!sort) {
								removeStoredSort();
							}
							else {
								localStorageService.set('list_sort_' + attrs.model, sort);
							}
						}
					}

					function getEmptySort() {
						return { column: 'id', descending: true, offset: 0 };
					}

					function getDefaultSort() {
						var sort = getStoredSort();
						if (sort) {
							return sort;
						}
						return getEmptySort();
					}

					scope.sort = getDefaultSort();
					scope.localSortColumn = null;

					scope.headerUrl = function(sortProperty) {
						var search = angular.copy($location.search());
						search.sort = sortProperty;
						if (scope.sort.column === sortProperty) {
							if (!scope.sort.descending) {
								search.desc = true;
							}
							else {
								var emptySort = getEmptySort();
								search.sort = emptySort.column;
								search.desc = emptySort.descending ? 'desc' : 'asc';
							}
						}
						else {
							search.desc = false;
						}
						return Utils.makeUrl($location.absUrl(), search);
					};

					scope.isSortable = function(columnName) {
						return ArrayUtils.inArray(columnName, scope.sortable) !== -1 ||
							(scope.columns[columnName] && scope.columns[columnName].localSort === 'true');
					};

					scope.isSortedOn = function(columnName) {
						if (scope.localSortColumn !== null) {
							return scope.localSortColumn === ('+' + columnName) || scope.localSortColumn === ('-' + columnName);
						}
						return scope.sort.column === columnName;
					};

					scope.isSortDescending = function() {
						if (scope.localSortColumn !== null) {
							return scope.localSortColumn.charAt(0) === '-';
						}
						return scope.sort.descending;
					};

					scope.toggleLocalSort = function(columnName) {
						if (scope.localSortColumn === ('+' + columnName)) {
							scope.localSortColumn = '-' + columnName;
						}
						else {
							scope.localSortColumn = '+' + columnName;
						}
					};

					scope.clearLocalSort = function() {
						scope.localSortColumn = null;
					};

					//
					// Resources loading.
					//
					function setExternalCollection(collection) {
						if (angular.isObject(collection) && collection.pagination && collection.resources) {
							documentCollectionLoadedCallback(collection);
							scope.disablePagination = false;
						}
						else {
							replaceCollection(collection);
							scope.disablePagination = true;
						}
					}

					var useExternalCollection = elm.is('[collection]');
					if (useExternalCollection) {
						// External collection may be already here, if defined directly in the scope.
						if (scope.externalCollection) {
							setExternalCollection(scope.externalCollection);
						}
						scope.$watchCollection('externalCollection', function(collection, oldCollection) {
							if (collection !== oldCollection || !scope.collection || !scope.collection.length) {
								setExternalCollection(collection);
							}
						});
					}

					function replaceCollection(collection) {
						scope.collection = collection;
					}

					function documentCollectionLoadedCallback(response) {
						scope.pagination.totalItems = response.pagination.count;
						if (scope.pagination.totalItems < scope.pagination.getOffset()) {
							$location.search('offset', 0);
						}
						else {
							replaceCollection(response.resources);
							scope.$broadcast('Change:DocumentListChanged', scope.collection);
						}
					}

					function reload() {
						var promise, params;

						params = {
							'offset': scope.pagination.getOffset(),
							'limit': scope.pagination.itemsPerPage,
							'sort': scope.sort.column,
							'desc': scope.sort.descending,
							'column': columnNames
						};

						if (useExternalCollection) {
							if (angular.isFunction(scope.onReload)) {
								setBusy();
								var p = scope.onReload(params);
								if (p && angular.isFunction(p.then)) {
									p.then(
										function() { stopLoading(); },
										function(result) { console.error(result); }
									);
								}
								else {
									stopLoading();
								}
							}
							return null;
						}
						setBusy();

						// TODO Reorganize this to use a query for tree and/or tag
						if (angular.isObject(queryObject) && angular.isObject(queryObject.where)) {
							promise = REST.query(prepareQueryObject(queryObject), { 'column': columnNames });
						}
						else if (elm.is('[collection-url]')) {
							if (attrs.collectionUrl) {
								promise = REST.collection(scope.collectionUrl, params);
							}
						}
						else if (attrs.model && !attrs.loadQuery) {
							if (scope.filterCollection) {
								params.filter = angular.copy(scope.filterCollection);
								$rootScope.$broadcast('Change:DocumentList.ApplyFilter', {
									"filter": params.filter,
									"model": attrs.model,
									"scope": scope
								});
							}
							promise = REST.collection(attrs.model, params);
						}

						if (promise) {
							promise.then(
								function(response) {
									stopLoading();
									if (response !== null) {
										documentCollectionLoadedCallback(response);
									}
								},
								function(reason) {
									stopLoading(reason);
								}
							);
							return promise;
						}
						return null;
					}

					function stopLoading(reason) {
						setNotBusy();
						if (reason) {
							NotificationCenter.error(i18n.trans('m.rbs.admin.admin.loading_list_error | ucf'), ErrorFormatter.format(reason));
						}
					}

					scope.location = $location;
					currentPath = scope.location.path();

					scope.$watch('location.search()', function locationSearchFn(search) {
						// Are we leaving this place?
						if (currentPath !== scope.location.path()) {
							// If yes, there is nothing to do.
							return;
						}

						var defaultSort = getDefaultSort();
						var offset = parseInt(search.hasOwnProperty('offset') ? search.offset : defaultSort.offset, 10);
						var limit = parseInt(search.hasOwnProperty('limit') ? search.limit : Settings.get('pagingSize', DEFAULT_PAGE_SIZE), 10);
						var sort = search.hasOwnProperty('sort') ? search.sort : defaultSort.column;
						var desc = search.hasOwnProperty('desc') ? (search.desc === 'true') : defaultSort.descending;

						scope.pagination.currentPage = Math.floor(offset / limit) + 1;
						scope.pagination.itemsPerPage = limit;

						var sortChanged = scope.sort.column !== sort || scope.sort.descending !== desc;
						if (sortChanged || scope.sort.offset !== offset) {
							scope.sort.column = sort;
							scope.sort.descending = desc;
							scope.sort.offset = offset;
							saveStoredSort(scope.sort);
						}

						// Reload only for sort, pagination's reload is in the controller
						if (sortChanged) {
							reload();
						}
					}, true);

					if (elm.is('[collection-url]')) {
						attrs.$observe('collectionUrl', function() {
							if (scope.collectionUrl) {
								reload();
							}
						});
					}

					//---------------------------------------------------------
					//
					// Converters
					//
					//---------------------------------------------------------

					function initializeConverters() {
						var promises = [];
						scope.convertersValues = {};

						scope.getConvertedValue = function(value, columnName) {
							if (value) {
								var converter = scope.columns[columnName].converter;
								if (scope.convertersValues[converter] && scope.convertersValues[converter][value]) {
									return scope.convertersValues[converter][value];
								}
								return '[' + value + ']';
							}
							return '';
						};

						angular.forEach(scope.columns, function(column) {
							//noinspection JSUnresolvedVariable
							var converter = column.converter, params = column.converterParams;
							if (converter) {
								if (converter === 'object' && /^{.*}$/.test(params)) {
									scope.convertersValues[converter] = scope.$eval(params);
								}
								else {
									scope.convertersValues[converter] = {};
									$rootScope.$broadcast(Events.DocumentListConverterGetValues, {
										converter: converter,
										params: params,
										promises: promises,
										values: scope.convertersValues[converter]
									});
								}
							}
						});

						function errorFn() {
							successFn();
						}

						function successFn() {
							if (elm.is('[model]')) {
								// No model value yet?
								if (attrs.model) {
									initialLoad();
								}
								else {
									attrs.$observe('model', function(model) {
										if (model) {
											initialLoad();
										}
									});
								}
							}
							else {
								initialLoad();
							}
						}

						if (promises.length) {
							$q.all(promises).then(successFn, errorFn);
						}
						else {
							successFn();
						}
					}

					initializeConverters();

					//---------------------------------------------------------
					//
					// Initial load.
					//
					//---------------------------------------------------------

					function initialLoad() {
						if (scope.externalCollection) {
							return;
						}
						// Not in a tree.
						var search = $location.search();

						// If one of "load-query" or "collection-url" attribute is present,
						// the list should not be loaded now: it will be when these objects are $watched.
						// And it works the same way with "sort" parameter in the URL:
						// the $watch() on $location.search() will load the query.
						if (!elm.is('[load-query]') && !elm.is('[collection-url]') &&
							(!search['limit'] || parseInt(search['limit']) === scope.pagination.itemsPerPage) &&
							(!search['offset'] || parseInt(search['offset']) === scope.pagination.getOffset()) &&
							(!search['sort'] || search['sort'] === scope.sort.column) &&
							(!search['desc'] || (search['desc'] === 'true') === scope.sort.descending)) {
							reload();
						}
					}

					// Query

					function prepareQueryObject(query) {
						query = angular.copy(query);
						query.offset = scope.pagination.getOffset();
						query.limit = scope.pagination.itemsPerPage;
						if (!angular.isObject(query.order)) {
							query.order = [
								{
									'property': scope.sort.column,
									'order': scope.sort.descending ? 'desc' : 'asc'
								}
							];
						}
						if (attrs.model) {
							query.model = attrs.model;
						}

						if (scope.filterCollection &&
							scope.filterCollection.filters && scope.filterCollection.filters.length > 0) {
							query.filter = angular.copy(scope.filterCollection);
							$rootScope.$broadcast('Change:DocumentList.ApplyFilter', {
								"filter": query.filter,
								"model": attrs.model,
								"scope": scope
							});
						}
						return query;
					}

					function watchQueryFn(query) {
						if (!angular.equals(query, queryObject)) {
							queryObject = angular.copy(query);
							reload();
						}
					}

					scope.$watch('loadQuery', watchQueryFn, true);

					scope.$on('filterUpdated', function() {
						reload();
					});

					var currentQuickActionsIndex = -1;

					function getQuickActionsElByIndex(index) {
						return elm.find('.document-list tbody tr:nth-child(' + (index + 1) + ') .quick-actions');
					}

					scope.showQuickActions = function($index) {
						if (currentQuickActionsIndex > -1) {
							getQuickActionsElByIndex(currentQuickActionsIndex).hide();
						}
						currentQuickActionsIndex = $index;
						getQuickActionsElByIndex($index).show();
					};

					scope.hideQuickActions = function($index) {
						getQuickActionsElByIndex($index).hide();
						currentQuickActionsIndex = -1;
						delete scope.deleteConfirm[$index];
					};

					scope.toggleQuickActions = function($index, $event) {
						$event.stopPropagation();
						var el = getQuickActionsElByIndex($index);
						if (el.is(':visible')) {
							scope.hideQuickActions($index);
						}
						else {
							scope.showQuickActions($index);
						}
					};

					scope.toggleRelativeDates = function(column) {
						scope.dateDisplay[column] = scope.dateDisplay[column] === 'relative' ? '' : 'relative';
					};

					function setBusy() {
						scope.busy = true;
						scope.pagination.loading = true;
					}

					function setNotBusy() {
						scope.busy = false;
						scope.pagination.loading = false;
					}

					scope.setBusy = setBusy;
					scope.setNotBusy = setNotBusy;

					scope.hasErrors = function(doc) {
						return scope.extend
							&& angular.isFunction(scope.extend.getDocumentErrors)
							&& scope.extend.getDocumentErrors(doc) !== null;
					};
				}
			}
		};
	}

	app.directive('rbsDocumentListGrid', ['RbsChange.Utils', 'RbsChange.i18n', function(Utils, i18n) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/document-list-grid.twig',
			scope: false,
			compile: function(tElement, tAttrs) {
				var listId = tAttrs.listId;
				if (__gridItems[listId]) {
					var inner = tElement.find('ul.thumbnail-grid li .inner');
					if (__gridItems[listId]['class']) {
						inner.addClass(__gridItems[listId]['class']);
					}

					angular.forEach(__gridItems[listId], function(value, name) {
						if (name !== 'class' && name !== 'content' && !Utils.startsWith(name, '$')) {
							inner.attr('data-' + Utils.normalizeAttrName(name), value);
						}
					});

					inner.html(__gridItems[listId].content);
					delete __gridItems[listId];

					var selectHtml =
						'<button type="button" data-ng-show="selectionContext.param(\'multiple\')" class="btn btn-selection btn-xs" data-ng-click="selectionContextAppend(doc)">' +
						' <i class="icon-plus"></i></button>';

					selectHtml += ' <button type="button" class="btn btn-selection btn-xs" data-ng-click="selectionContextAppend(doc, true)">' +
						i18n.trans('m.rbs.admin.admin.select') +
						' <i class="icon-circle-arrow-right"></i></button>';

					inner.find('.caption').prepend('<div data-ng-show="selectionContext" class="quick-actions-buttons pull-right">' +
						selectHtml + '</div>');
				}
			}
		};
	}]);

	app.directive('rbsDocumentListTable', ['RbsChange.i18n', 'RbsChange.Actions', function(i18n, Actions) {
		/**
		 * Build the HTML used in the "Quick actions" toolbar.
		 */
		function buildQuickActionsHtml(listId, tAttrs, localActions) {
			var html, quickActionsHtml;

			html = '<div class="quick-actions popover left"><div class="arrow"></div><div class="popover-content clearfix">';

			function buildDefault() {
				var out = buildDeleteAction();
				//noinspection JSUnresolvedVariable
				if (tAttrs.publishable === 'true' || tAttrs.correction === 'true') {
					out += buildWorkflowAction();
				}
				return out;
			}

			function buildDeleteAction() {
				return '<a data-ng-hide="deleteConfirm[$index] || !doc.isActionAvailable(\'delete\')" href="" data-ng-click="askDeleteConfirmation($index, $event)" class="danger"><i class="icon-trash"></i> ' +
					i18n.trans('m.rbs.admin.admin.delete') +
					'</a>' +
					'<div class="quick-action danger" data-ng-show="deleteConfirm[$index]">' +
					'<i class="icon-trash"></i> ?' +
					'<span class="pull-right"><button type="button" class="btn btn-danger btn-xs" data-ng-click="remove(doc, $event)">' +
					i18n.trans('m.rbs.admin.admin.yes') + '</button>' +
					' <button type="button" class="btn btn-default btn-xs" data-ng-click="cancelDelete($index, $event)">' +
					i18n.trans('m.rbs.admin.admin.no') + '</button></span>' +
					'</div>';
			}

			function buildEditAction() {
				return '<a href="" data-ng-href="(= doc | rbsURL =)">' + i18n.trans('m.rbs.admin.admin.edit') + '</a>';
			}

			function buildOtherAction(action) {
				var html = '<a href="" data-ng-click="executeAction(\'' + action.name + '\', doc, $event)">';
				if (action.icon) {
					html += '<i class="' + action.icon + '"></i>';
				}
				return html + action.label + '</a>';
			}

			function buildWorkflowAction() {
				return '<a href="" data-ng-click="showWorkflow($index, $event)"><i class="icon-ok"></i> ' +
					i18n.trans('m.rbs.admin.admin.workflow') + '</a>';
			}

			if (__quickActions[listId]) {
				quickActionsHtml = __quickActions[listId].contents;

				//noinspection JSUnresolvedVariable
				if ((tAttrs.publishable === 'true' || tAttrs.correction === 'true') && (!quickActionsHtml ||
					(quickActionsHtml.indexOf('[action default]') === -1 &&
					quickActionsHtml.indexOf('[action workflow]') === -1))) {
					quickActionsHtml += '[action workflow]';
				}

				if (!quickActionsHtml.length) {
					return null;
				}
				quickActionsHtml = quickActionsHtml.replace(/\s*\|\|\s*/g, '').replace(/\[action\s+([A-Za-z0-9_\-]+)\]/g,
					function(match, actionName) {
						if (actionName === 'delete') {
							return buildDeleteAction();
						}
						if (actionName === 'edit') {
							return buildEditAction();
						}
						if (actionName === 'default') {
							return buildDefault();
						}
						if (actionName === 'workflow') {
							return buildWorkflowAction();
						}

						if (localActions.hasOwnProperty(actionName)) {
							actionName = listId + '_' + actionName;
						}

						var actionObject = Actions.get(actionName);
						if (actionObject !== null) {
							return buildOtherAction(actionObject);
						}
						return '';
					});
				html += quickActionsHtml.trim();
			}
			else {
				html += buildDefault();
			}

			html += '</div></div>';

			return html;
		}

		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/document-list-table.twig',
			scope: false,
			compile: function(tElement, tAttrs) {
				var $th, $td, html, inner;

				var listId = tAttrs.listId;
				var columns = lists.columns[listId];
				var localActions = lists.localActions[listId];

				// Update colspan value for preview and empty cells.
				tElement.find('td[data-colspan="auto"]').attr('colspan', columns.length);

				// Prepare preview.
				if (__preview[listId]) {
					inner = tElement.find('tbody tr td.preview .inner');
					if (__preview[listId]['class']) {
						inner.addClass(__preview[listId]['class']);
					}
					if (__preview[listId]['style']) {
						inner.attr('style', __preview[listId]['style']);
					}
					inner.find('[data-role="preview-contents"]').replaceWith(__preview[listId].contents ||
						'<div data-ng-include="doc | rbsAdminTemplateURL:\'preview-list\'"></div>');
				}

				var $head = tElement.find('table.document-list thead tr');
				var $body = tElement.find('table.document-list tbody tr.normal-row');

				// Loop through all the columns and build header et body cells.
				while (columns.length) {
					var column = columns.shift(0);

					// Create header cell
					if (column.name === 'selectable') {
						$th = jQuery('<th data-ng-show="selectionEnabled" data-ng-click="selection.all = !selection.all" class="column-checkbox">' +
							column.label + '</th>');
					}
					else {
						var toggleDateBtn, htmlTh;

						if (column.format === 'date') {
							toggleDateBtn = '<button type="button" data-ng-class="{\'active\':dateDisplay.' + column.name +
								'==\'relative\'}" data-ng-click="toggleRelativeDates(\'' + column.name +
								'\');" class="btn btn-xs btn-default pull-right margin-left"><i class="icon-time"></i></button>';
						}
						else {
							toggleDateBtn = '';
						}

						htmlTh = '<th data-ng-if="isSortable(\'' + column.sort + '\')" data-ng-class="{\'sorted\':isSortedOn(\'' + column.sort +
							'\')}">' + toggleDateBtn;

						//noinspection JSUnresolvedVariable
						if (column.localSort === 'true') {
							htmlTh += '<a href="" data-ng-click="toggleLocalSort(\'' + column.sort + '\')"><span data-ng-bind-html="columns.' +
								column.name + '.label">' + column.name + '</span>';
						}
						else {
							htmlTh += '<a href="" data-ng-click="clearLocalSort()" data-ng-href="(= headerUrl(\'' + column.sort +
								'\') =)"><span data-ng-bind-html="columns.' + column.name + '.label">' + column.name + '</span>';
						}

						htmlTh +=
							'  <i class="column-sort-indicator pull-right hidden-print" data-ng-class="{true:\'icon-sort-down\', false:\'icon-sort-up\'}[isSortDescending()]"' +
							'    data-ng-if="isSortedOn(\'' + column.sort + '\')"></i>' +
							'  <i class="column-sort-indicator icon-sort pull-right hidden-print" data-ng-if="!isSortedOn(\'' + column.sort + '\')"></i>' +
							'</a></th>' +
							'<th data-ng-if="!isSortable(\'' + column.sort + '\')">' +
							'  ' + toggleDateBtn + '<span data-ng-bind-html="columns.' + column.name + '.label">' + column.name + '</span>' +
							'</th>';

						$th = jQuery(htmlTh);
					}

					if (column.align) {
						$th.css('text-align', column.align);
					}
					if (column.width) {
						$th.css('width', column.width);
					}
					$head.append($th);

					// Create body cell
					if (column.content) {
						if (column.name === 'selectable') {
							$td = jQuery(
								'<td data-ng-show="selectionEnabled" data-ng-click="selection[doc.id] = !selection[doc.id]" class="column-checkbox">' +
								column.content + '</td>');
						}
						else {
							// Allow the use of "converted(<property>)" instead of "getConvertedValue(<property>, <columnName>)"
							// in column templates that have a converter defined on them.
							column.content = column.content.replace(/converted\s*\(\s*([a-zA-Z0-9\.]+)\s*\)/,
								'getConvertedValue($1, "' + column.name + '")');

							html = '<td data-ng-class="{' + (column.primary ? '\'preview\':hasPreview(doc),' : '') +
								'\'sorted\':isSortedOn(\'' + column.sort + '\')}">';
							if (column.primary) {
								html += '<div class="primary-cell" data-ng-style="extend.getPrimaryCellStyle(doc, $index)">' +
									column.content + '</div>';
							}
							else {
								html += column.content;
							}
							html += '</td>';
							$td = jQuery(html);
						}
					}
					else {
						if (column.thumbnail) {
							//noinspection JSUnresolvedVariable
							var imageRef = column.thumbnailPath ? column.thumbnailPath : ('doc.' + column.valuePath);
							column.content = '<img data-rbs-storage-image="' + imageRef + '" data-thumbnail="' + column.thumbnail + '"/>';
						}
						else {
							if (column.converter) {
								column.content = '(= getConvertedValue(doc.' + column.valuePath + ', "' + column.name + '") =)';
							}
							else {
								column.content = '(= doc.' + column.valuePath + ' =)';
							}
						}
						if (column.primary) {
							$td = jQuery('<td data-ng-class="{\'preview\':hasPreview(doc),\'sorted\':isSortedOn(\'' + column.name +
								'\')}"><div class="primary-cell" data-ng-style="extend.getPrimaryCellStyle(doc, $index)">' +
								'<a href="" data-ng-href="(= doc | rbsURL =)"><strong>' + column.content + '</strong></a></div></td>');
						}
						else {
							$td = jQuery('<td data-ng-class="{\'sorted\':isSortedOn(\'' + column.name + '\')}">' + column.content + '</td>');
						}
					}

					// The primary column has extra links for preview, edit and delete.
					if (column.primary) {
						var previewButton = '';
						if (__preview[listId]) {
							previewButton = '<button type="button" class="btn-xs btn-icon btn-default btn" data-ng-click="preview(doc, $event)" title="' +
								i18n.trans('m.rbs.admin.admin.preview') +
								'"><i data-ng-class="{\'icon-spinner icon-spin\':isPreviewLoading(doc), \'icon-eye-close\':hasPreview($index), \'icon-eye-open\':!hasPreview($index)}"></i></button>';
						}

						var selectHtml =
							'<button type="button" data-ng-show="selectionContext.param(\'multiple\')" class="btn btn-selection btn-xs"' +
							'  data-ng-click="selectionContextAppend(doc)">' + ' <i class="icon-plus"></i></button>';

						selectHtml += ' <button type="button" class="btn btn-selection btn-xs" data-ng-click="selectionContextAppend(doc, true)">' +
							i18n.trans('m.rbs.admin.admin.select | ucf') + ' <i class="icon-circle-arrow-right"></i></button>';

						$td.find('.primary-cell').prepend('<span data-ng-show="selectionContext" class="pull-right quick-actions-buttons">' +
							previewButton + selectHtml + '</span>');

						if (angular.isUndefined(__quickActions[listId]) || __quickActions[listId].contents.length > 0) {
							// if quickActions markup is not present, default quick actions are taken
							// but if it present and empty, don't add the quick actions button
							$td.find('.primary-cell').prepend(
								'<span class="pull-right quick-actions-buttons" data-ng-hide="selectionContext">' +
								previewButton +
								'<button type="button" class="btn-xs btn-icon btn-default btn" data-ng-click="toggleQuickActions($index, $event)"><i class="icon-ellipsis-horizontal icon-large"></i></button>' +
								buildQuickActionsHtml(listId, tAttrs, localActions) +
								'</span>'
							);
						}
					}

					if ($td.attr('ng-if')) {
						$td.attr('ng-if', $td.attr('ng-if') + ' && isNormalCell(doc)');
					}
					else {
						$td.attr('ng-if', 'isNormalCell(doc)');
					}

					if (column.align) {
						$td.css('text-align', column.align);
					}
					$body.append($td);
				}
			}
		};
	}]);

	app.directive('rbsColumn', ['rbsThumbnailSizes', function(sizes) {
		return {
			restrict: 'AE',
			require: '^rbsDocumentList',
			compile: function(tElement, tAttrs) {
				var content = tElement.html().trim();
				if (content.length) {
					tAttrs.content = content;
				}

				if (tAttrs.thumbnail) {
					tAttrs.thumbnail = tAttrs.thumbnail.toLowerCase();
					if (sizes[tAttrs.thumbnail]) {
						tAttrs.thumbnail = sizes[tAttrs.thumbnail];
					}
					if (/^\d+x\d+$/.test(tAttrs.thumbnail)) {
						var dim = tAttrs.thumbnail.split('x');
						tAttrs.maxWidth = dim[0];
						tAttrs.maxHeight = dim[1];
					}
					else {
						tAttrs.maxWidth = '100';
						tAttrs.maxHeight = '100';
					}
					if (!tAttrs.width) {
						tAttrs.width = tAttrs.maxWidth + 'px';
					}
					if (!tAttrs.align) {
						tAttrs.align = 'center';
					}
				}

				return {
					pre: function(scope, elm) {
						var listId = getListId(elm.parents('.document-list-main'));
						if (!__columns.hasOwnProperty(listId)) {
							__columns[listId] = [];
						}
						__columns[listId].push(tAttrs);
					}
				}
			}
		};
	}]);

	app.directive('rbsGridItem', [function() {
		return {
			restrict: 'AE',
			require: '^rbsDocumentList',
			compile: function(tElement, tAttrs) {
				var content = tElement.html().trim();
				if (content.length) {
					tAttrs.content = content;
				}

				return {
					pre: function(scope, elm) {
						var listId = getListId(elm.parents('.document-list-main'));
						__gridItems[listId] = tAttrs;
					}
				}
			}
		};
	}]);

	app.directive('rbsPreview', [function() {
		return {
			restrict: 'AE',
			require: '^rbsDocumentList',
			compile: function(tElement, tAttrs) {
				var content = tElement.html().trim();
				if (content.length) {
					tAttrs.contents = content;
				}

				return {
					pre: function(scope, elm) {
						var listId = getListId(elm.parents('.document-list-main'));
						__preview[listId] = tAttrs;
					}
				}
			}
		};
	}]);

	app.directive('rbsQuickActions', [function() {
		return {
			restrict: 'AE',
			require: '^rbsDocumentList',
			compile: function(tElement, tAttrs) {
				var content = tElement.html().trim();
				if (content.length || !tAttrs.hasOwnProperty('contents')) {
					tAttrs.contents = content;
				}

				return {
					pre: function(scope, elm) {
						var listId = getListId(elm.parents('.document-list-main'));
						__quickActions[listId] = tAttrs;
					}
				}
			}
		};
	}]);

	app.directive('rbsAction', [function() {
		return {
			restrict: 'AE',
			require: '^rbsDocumentList',
			link: {
				pre: function(scope, elm, attrs) {
					var listId = getListId(elm.parents('.document-list-main'));

					if (!__actions.hasOwnProperty(listId)) {
						__actions[listId] = [];
					}
					__actions[listId].push(attrs);
				}
			}
		};
	}]);
})(window.jQuery);
