/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsDocumentPreviewForm
	 * @name rbsDocumentPreviewForm
	 * @restrict A
	 *
	 * @description
	 * Used to configure context for document preview.
	 */
	app.directive('rbsDocumentPreviewForm', ['RbsChange.REST', '$window', '$compile', function(REST, $window, $compile) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/document-preview-form.twig',
			scope: {
				document: '=rbsDocumentPreviewForm',
				language: '='
			},
			link: function(scope, document, attributes) {
				function showInTab(applyModifications) {
					scope.previewContext.LCID = scope.language;
					REST.preview(scope.document, scope.previewContext, applyModifications).then(
						function (data) {
							var previewWindow = $window.open('Rbs/Admin/preview.html', 'change_admin_preview');
							if (previewWindow && !previewWindow.closed) {
								previewWindow.document.open();
								previewWindow.document.write(data.pageContent);
								previewWindow.document.close();
							}
							else {
								console.error('No window!')
							}
						},
						function (error) {
							console.error('Error during preview generation: ', error);
						}
					);
				}

				var currentIframe;
				var correctionIframe;

				function showComparison() {
					scope.previewContext.LCID = scope.language;
					if (!scope.previewManager.comparisonInitialized) {
						currentIframe = document.find('.preview-iframe-current');
						correctionIframe = document.find('.preview-iframe-correction');
						scope.previewManager.comparisonInitialized = true;
					}

					currentIframe.attr('src', 'Rbs/Admin/preview.html');
					correctionIframe.attr('src', 'Rbs/Admin/preview.html');

					REST.preview(scope.document, angular.copy(scope.previewContext), false).then(
						function (data) {
							var currentDocument = currentIframe.get(0).contentWindow.document;
							currentDocument.open();
							currentDocument.write(data.pageContent);
							currentDocument.close();
						},
						function (error) {
							console.error("Error during preview generation: ", error);
						}
					);
					REST.preview(scope.document, angular.copy(scope.previewContext), true).then(
						function (data) {
							var correctionDocument = correctionIframe.get(0).contentWindow.document;
							correctionDocument.open();
							correctionDocument.write(data.pageContent);
							correctionDocument.close();
						},
						function (error) {
							console.error("Error during preview generation: ", error);
						}
					);
				}

				function togglePanel() {
					scope.previewManager.deployed = !scope.previewManager.deployed
				}

				scope.previewContext = {
					sectionId: 0,
					userId: 0,
					detailId: 0,
					pageId: 0,
					templateId: 0
				};

				scope.previewManager = {
					showComparison: showComparison,
					showInTab: showInTab,
					deployed: false,
					toggle: togglePanel,
					comparisonInitialized: false,
					mode: attributes['mode'] || 'tab'
				};
			}
		}
	}]);
})();