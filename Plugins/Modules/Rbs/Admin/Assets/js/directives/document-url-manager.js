(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentUrlManager', ['RbsChange.REST', rbsDocumentUrlManager]);

	function rbsDocumentUrlManager(REST) {
		return {
			restrict: 'AE',
			scope: {
				targetDocument: '=document'
			},
			templateUrl: 'Rbs/Admin/js/directives/document-url-manager.twig',

			link: function(scope) {
				scope.locations = [];
				scope.displayConfig = {};

				var initPathRule = function(result) {
					scope.locations = result.locations;

					for (var i = 0; i < scope.locations.length; i++) {
						scope.displayConfig[i] = {
							'showDetails': false,
							'showRedirects': false
						};

						var urls = scope.locations[i].urls;
						scope.locations[i].urls = [];
						for (var j = 0; j < urls.length; j++) {
							scope.locations[i].urls.push(urls[j]);
						}

						var redirects = scope.locations[i].redirects;
						scope.locations[i].redirects = [];
						for (j = 0; j < redirects.length; j++) {
							scope.locations[i].redirects.push(redirects[j]);
						}
					}
				};

				scope.$watch('targetDocument', function(targetDoc) {
					// Wait for document to become ready.
					if (!targetDoc) {
						return;
					}

					// Load pathRules related to the target document.
					var pathRulesURL = targetDoc.getLink('pathRules');
					if (pathRulesURL) {
						scope.pathRulesURL = pathRulesURL;
						REST.call(scope.pathRulesURL, null, REST.resourceTransformer()).then(
							initPathRule,
							function(result) { console.error(result); }
						);
					}
				});

				REST.getAvailableLanguages().then(
					function(langs) {
						scope.availableLanguages = langs.items;
					},
					function(result) { console.error(result); }
				);

				scope.getHref = function(location, url) {
					return location.baseUrl + (url ? url.relativePath : '');
				};

				scope.urlDecode = function(url) {
					return decodeURIComponent((url + '').replace(/\+/g, '%20'));
				};

				scope.getLocationDefaultRelativePath = function(locationIndex, index) {
					var location = scope.locations[locationIndex];
					if (location.canonical && !location.urls[index].sectionId) {
						return location.defaultCanonicalUrl.relativePath;
					}
					else {
						return location.defaultUrl.relativePath;
					}
				};

				scope.getDefaultCanonical = function(location) {
					return location.baseUrl + ((location.defaultCanonicalUrl && location.defaultCanonicalUrl.relativePath) || '');
				};
			}
		};
	}
})();