(function() {
	'use strict';

	var app = angular.module('RbsChange');

	// TODO: Dispatch the content of this file in appropriate files?
	app.controller('RbsChangeTranslateEditorController', ['$scope', function($scope) {
		$scope.document = {};
		$scope.editMode = 'translate';
	}]);

	app.controller('RbsChangeWorkflowController',
		['RbsChange.REST', '$scope', '$routeParams',
			function(REST, $scope, $routeParams) {
				$scope.$watch('model', function(model) {
					if (model) {
						REST.resource(model, $routeParams.id, $routeParams.LCID).then(
							function(doc) {
								$scope.document = doc;
							},
							function(result) { console.error(result); }
						);
					}
				});
			}]);

	/**
	 * Default controller for Document-based views.
	 */
	app.controller('RbsChangeSimpleDocumentController',
		['RbsChange.REST', '$scope', '$routeParams', function(REST, $scope, $routeParams) {
			REST.resource($routeParams.id).then(
				function(doc) {
					$scope.document = doc;
				},
				function(result) { console.error(result); }
			);
		}]);

	/**
	 * Redirects to the editor of the document with the id specified in $routeParams.
	 */
	function RedirectToForm($routeParams, $location, REST, $filter) {
		var listId = $routeParams.id;
		REST.resource(listId).then(
			function(doc) {
				$location.path($filter('rbsURL')(doc, 'edit'));
			},
			function(result) { console.error(result); }
		);
	}

	RedirectToForm.$inject = ['$routeParams', '$location', 'RbsChange.REST', '$filter'];
	app.controller('RbsChangeRedirectToForm', RedirectToForm);

	// Validators directives.

	var INTEGER_REGEXP = /^\-?\d*$/;
	app.directive('rbsInteger', function() {
		return {
			require: 'ngModel',
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$parsers.unshift(function(viewValue) {
					if (angular.isNumber(viewValue)) {
						return viewValue;
					}
					else if (viewValue == '' || INTEGER_REGEXP.test(viewValue)) {
						// it is valid
						ctrl.$setValidity('integer', true);
						return viewValue;
					}
					else {
						// it is invalid, return undefined (no model update)
						ctrl.$setValidity('integer', false);
						return undefined;
					}
				});
			}
		};
	});

	var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
	app.directive('rbsSmartFloat', function() {
		return {
			require: 'ngModel',
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$parsers.unshift(function(viewValue) {
					if (angular.isNumber(viewValue)) {
						return viewValue;
					}
					else if (FLOAT_REGEXP.test(viewValue)) {
						ctrl.$setValidity('float', true);
						return parseFloat(viewValue.replace(',', '.'));
					}
					else if (viewValue == '') {
						ctrl.$setValidity('float', true);
						return undefined;
					}
					else {
						ctrl.$setValidity('float', false);
						return undefined;
					}
				});
			}
		};
	});
})();