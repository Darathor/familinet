(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsFiltersContainer', ['$compile', function($compile) {
		return {
			restrict: 'A',
			require: ['ngModel', 'rbsFiltersContainer'],
			controller: ['$scope', function(scope) {
				var filterDefinitions;
				var customDefinitions = {};
				var rootFilter = {};

				function delFilter(filterToDelete, filters) {
					if (filters && filters.length) {
						for (var i = 0; i < filters.length; i++) {
							if (filters[i] === filterToDelete) {
								filters.splice(i, 1);
								return true;
							}
							else if (filters[i].filters) {
								if (delFilter(filterToDelete, filters[i].filters)) {
									return true;
								}
							}
						}
					}
					return false;
				}

				function parseDefinitions(definitions) {
					var parsedDefinitions = {};
					angular.forEach(definitions, function(definition) {
						if (definition.hasOwnProperty('name') &&
							definition.hasOwnProperty('directiveName') &&
							definition.hasOwnProperty('config')) {
							if (!definition.hasOwnProperty('parameters')) {
								definition.parameters = {};
							}
							definition.label = definition.config.listLabel;
							definition.group = definition.config.hasOwnProperty('group') ? definition.config.group : '';
							if (parsedDefinitions[definition.name]) {
								throw new Error('Duplicate filter name: ' + definition.name);
							}
							parsedDefinitions[definition.name] = definition;
						}
					});
					return parsedDefinitions;
				}

				var contentScopes = {};

				function redrawFilters(parentScope, element, filters, definitionName) {
					var id = parentScope.$id;
					if (contentScopes[id]) {
						contentScopes[id].$destroy();
						contentScopes[id] = null;
					}

					element.children('ul.list-group').children('li:not(:last)').remove();

					var html = '';
					var definitions = definitionName ? customDefinitions[definitionName] : filterDefinitions;

					if (definitions && filters && filters.length) {
						angular.forEach(filters, function(filter, idx) {
							if (definitions[filter.name]) {
								var directiveName = definitions[filter.name].directiveName;
								var deprecated = definitions[filter.name].deprecated;
								html += '<li class="list-group-item' + (deprecated ? ' filter-deprecated' : '') + '" ' +
									directiveName + '="" filter="filter.filters[' + idx + ']"';
								var contextKey = element.attr('context-key');
								if (contextKey && contextKey.length) {
									html += ' context-key="' + contextKey + '_' + idx + '"';
								}
								else {
									html += ' context-key="' + idx + '"';
								}
								if (filter.name === 'group') {
									html += ' parent-operator="filter.parameters.operator"';
								}
								html += '></li>';
							}
							else {
								console.error('Invalid filter: ', filter);
							}
						});
					}

					if (html != '') {
						element.children('ul.list-group').children('li:last').before(html);
						contentScopes[id] = parentScope.$new();
						$compile(element.children('ul.list-group').children('li:not(:last)'))(contentScopes[id]);
					}
				}

				this.getFilterDefinitions = function() {
					return filterDefinitions;
				};

				this.getRootFilter = function() {
					return rootFilter;
				};

				this.setRootFilter = function(filter) {
					rootFilter = filter;
				};

				this.linkNode = function(nodeScope) {
					if (nodeScope.filter) {
						if (filterDefinitions && filterDefinitions[nodeScope.filter.name]) {
							nodeScope.delFilter = function() {
								delFilter(nodeScope.filter, rootFilter.filters);
							};
							nodeScope.filterDefinition = filterDefinitions[nodeScope.filter.name];
						}
						else {
							angular.forEach(customDefinitions, function(definitions) {
								if (definitions[nodeScope.filter.name]) {
									nodeScope.delFilter = function() {
										delFilter(nodeScope.filter, rootFilter.filters);
									};
									nodeScope.filterDefinition = definitions[nodeScope.filter.name];
								}
							});
						}
					}
				};

				this.applyConfigured = function(element, configured, old) {
					if (!old !== !configured || old === configured) {
						if (configured) {
							element.addClass('filter-configured');
							element.removeClass('filter-not-configured')
						}
						else {
							element.addClass('filter-not-configured');
							element.removeClass('filter-configured')
						}
					}
				};

				this.redrawFilters = function(scope, element, filters, customDefinitions) {
					redrawFilters(scope, element, filters, customDefinitions);
				};

				this.setDefinitions = function(definitions) {
					filterDefinitions = parseDefinitions(definitions);
				};
				this.setCustomDefinitions = function(name, definitions) {
					customDefinitions[name] = parseDefinitions(definitions);
				};
			}],

			link: function(scope, element, attrs, controllers) {
				var ngModel = controllers[0];
				var rbsFiltersContainerCtrl = controllers[1];

				var filterFromModel = false;

				// viewValue => modelValue
				ngModel.$parsers.unshift(function(viewValue) {
					return viewValue;
				});

				// modelValue => viewValue
				ngModel.$formatters.unshift(function(modelValue) {
					filterFromModel = true;
					if (angular.isObject(modelValue) && !angular.isArray(modelValue)) {
						rbsFiltersContainerCtrl.setRootFilter(modelValue);
					}
					return modelValue;
				});

				scope.getRootFilter = function() {
					return rbsFiltersContainerCtrl.getRootFilter();
				};

				scope.$watch('getRootFilter()', function(filter, oldFilter) {
					if (filter !== oldFilter) {
						if (!filterFromModel) {
							ngModel.$setViewValue(angular.copy(filter));
						}
						else {
							filterFromModel = false;
						}
					}
				}, true);
			}
		}
	}
	]);

	app.directive('rbsFilterGroupAndOr', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl: 'Rbs/Admin/js/directives/filter-group-and-or.twig',
			scope: {
				filter: '=',
				parentOperator: '='
			},
			link: function(scope, element, attrs, containerController) {

				scope.definitionToAdd = null;

				if (scope.filter) {
					scope.filter.name = 'group';
					if (!angular.isObject(scope.filter.parameters) || angular.isArray(scope.filter.parameters)) {
						scope.filter.parameters = {};
					}
					scope.filter.parameters.operator = ((scope.parentOperator == 'AND') ? 'OR' : 'AND');
					if (!angular.isArray(scope.filter.filters)) {
						scope.filter.filters = [];
					}
					containerController.linkNode(scope);
				}

				scope.getFilterDefinitions = function() {
					return containerController.getFilterDefinitions();
				};

				scope.isAnd = function() {
					return (scope.filter && scope.filter.parameters && scope.filter.parameters.operator) == 'AND';
				};

				scope.isConfigured = function() {
					if (scope.filter) {
						return !!(scope.filter.filters && (scope.filter.filters.length >= (scope.parentOperator ? 2 : 0)));
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.addFilter = function() {
					if (scope.filter && angular.isObject(scope.definitionToAdd)) {
						var childName = scope.definitionToAdd.name;
						if (!angular.isArray(scope.filter.filters)) {
							angular.copy({ name: 'group', 'parameters': { operator: 'AND' }, filters: [] }, scope.filter);
						}
						scope.filter.filters.push({ name: childName, parameters: angular.copy(scope.definitionToAdd.parameters) });
						scope.definitionToAdd = null;
					}
				};

				scope.$watchCollection('filter.filters', function(filters) {
					containerController.redrawFilters(scope, element, filters);
				});

				scope.$watch('getFilterDefinitions()', function(definitions, old) {
					if (definitions !== old) {
						containerController.redrawFilters(scope, element, scope.filter.filters);
					}
				});
			}
		};
	});
})();