(function() {
	"use strict";
	var app = angular.module('RbsChange');

	app.directive('rbsInputPhoneNumber', ['RbsChange.REST', '$q', 'RbsChange.PhoneService', '$timeout', rbsInputPhoneNumber]);
	function rbsInputPhoneNumber(REST, $q, PhoneService, $timeout) {

		var defaultCode = 'FR';

		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/input-phone-number.twig',
			require: 'ngModel',
			scope: {
				required: "=",
				mobileOnly: "@",
				fixedLineOnly: "@",
				language: "="
			},
			controller: ['$scope', function(scope) {
				if (scope.mobileOnly) {
					scope.numberType = 'MOBILE';
				}
				else if (scope.fixedLineOnly) {
					scope.numberType = 'FIXED_LINE';
				}
				else {
					scope.numberType = 'FIXED_LINE_OR_MOBILE';
				}

				scope.currentCfg = null;
				scope.regionNumberConfig = [{ id : 0}];

				PhoneService.getRegionNumberConfig().then(
					function(regionNumberConfig) {
						scope.regionNumberConfig = regionNumberConfig;
						angular.forEach(regionNumberConfig, function(cfg) {
							if (scope.currentCfg === null || cfg[0] === defaultCode) {
								scope.currentCfg = cfg;
							}
						});
					},
					function(result) { console.error(result); }
				);
			}],
			link: function(scope, element, attributes, ngModel) {
				scope.parsedNumber = null;
				scope.name = attributes.name;

				scope.getCurrentPlaceHolder = function() {
					if (scope.currentCfg) {
						if (scope.mobileOnly) {
							return scope.currentCfg[4];
						}
						else if (scope.fixedLineOnly) {
							return scope.currentCfg[3];
						}
						else {
							return scope.currentCfg[3] || scope.currentCfg[4] || '';
						}
					}
					return '';
				};

				ngModel.$render = function() {
					scope.rawNumber = ngModel.$viewValue;
					if (scope.rawNumber) {

						PhoneService.parseNumber(scope.rawNumber, defaultCode).then(
							function(parsedNumber) {
								scope.parsedNumber = parsedNumber;
								if (parsedNumber) {
									scope.rawNumber = parsedNumber.national;
									angular.forEach(scope.regionNumberConfig, function(cfg) {
										if (cfg[0] === parsedNumber.regionCode) {
											scope.currentCfg = cfg;
										}
									});
								}
							},
							function() {
								scope.parsedNumber = null;
							}
						);
					}
				};

				scope.setCurrentCfg = function(cfg) {
					scope.currentCfg = cfg;
					ngModel.$setViewValue(scope.rawNumber);
					ngModel.$validate();
				};

				var delayedPromise = null;

				scope.validateRawNumber = function(delayed) {
					if (scope.rawNumber) {
						ngModel.$setTouched();
					}

					if (delayedPromise) {
						$timeout.cancel(delayedPromise);
						delayedPromise = null;
					}

					if (delayed) {
						delayedPromise = $timeout(function() {
							ngModel.$setViewValue(scope.rawNumber);
						}, 500)
					}
					else {
						ngModel.$setViewValue(scope.rawNumber);
					}
				};

				var errorCallback = function(error) {
					scope.parsedNumber = null;
					return $q.reject(error);
				};

				var successCallback = function(parsedNumber) {
					scope.parsedNumber = parsedNumber;
					if (parsedNumber) {
						scope.rawNumber = parsedNumber.national;
						ngModel.$setViewValue(parsedNumber.E164);
					}
					return parsedNumber;
				};

				ngModel.$asyncValidators.mobileNumber = function(modelValue, viewValue) {
					var regionCode = scope.currentCfg ? scope.currentCfg[0] : defaultCode;
					if (scope.mobileOnly) {
						return PhoneService.parseMobileNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
					else if (scope.fixedLineOnly) {
						return PhoneService.parseFixedLineNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
					else {
						return PhoneService.parseNumber(viewValue, regionCode).then(successCallback, errorCallback);
					}
				};
			}
		}
	}
})();
