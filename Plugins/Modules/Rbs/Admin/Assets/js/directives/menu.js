(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsMenuSection', ['RbsChange.Settings', rbsMenuSection]);
	function rbsMenuSection(Settings) {
		return {
			restrict: 'A',
			scope: true,
			controller: ['$scope', function(scope) {
				var userId = 0;
				var rules = {};
				var entries = [];
				scope.visibleSection = false;

				this.addEntry = function(entry) {
					entry.visible = checkEntry(entry.permissionRule);
					scope.visibleSection = scope.visibleSection || entry.visible;
					entries.push(entry);
				};

				Settings.ready().then(
					function(user) {
						if (user && user.id && (user.id !== userId)) {
							var visibleSection = false;
							userId = user.id;
							rules = user.rules || {};
							angular.forEach(entries, function(entry) {
								entry.visible = checkEntry(entry.permissionRule);
								visibleSection = visibleSection || entry.visible;
							});
							scope.visibleSection = visibleSection;
						}
					}
				);

				function checkEntry(permissionRule) {
					if (!permissionRule || rules['*']) {
						return true;
					}
					for( var i = permissionRule.length - 1; i >= 0; i--)
					{
						if (permissionRule[i] && rules[permissionRule[i]]) {
							return true;
						}
					}
					return false;
				}
			}],
			link: function(scope, iElement, iAttrs) {
				var i = parseInt(iAttrs.rbsMenuSection, 10);
				scope.$watch('visibleSection', function(visible) {
					if (visible) {
						iElement.show();
					}
					else {
						iElement.hide()
					}
				});
			}
		};
	}

	app.directive('rbsMenuSectionEntry', rbsMenuSectionEntry);
	function rbsMenuSectionEntry() {
		return {
			restrict: 'A',
			require: '^rbsMenuSection',
			scope: true,
			link: function(scope, iElement, iAttrs, controller) {
				var i = parseInt(iAttrs.rbsMenuSectionEntry, 10);
				var permissionRule = iAttrs.permissionRule;
				if (permissionRule && angular.isString(permissionRule)) {
					permissionRule = permissionRule.split(',');
				}
				else {
					permissionRule = null
				}
				scope.entry = { index: i, permissionRule: permissionRule };
				controller.addEntry(scope.entry);
				scope.$watch('entry.visible', function(visible) {
					if (visible) {
						iElement.show();
					}
					else {
						iElement.hide()
					}
				});
			}
		};
	}
})();