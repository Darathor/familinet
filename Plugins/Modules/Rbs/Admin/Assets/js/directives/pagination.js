(function() {
	"use strict";

	var app = angular.module('RbsChange');

	app.constant('RbsChange.PaginationPageSizes', [10, 20, 30, 50, 75, 100]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsPagination
	 * @name rbsPagination
	 *
	 * @description
	 * A directive showing a pagination.
	 *
	 * The given pagination object will be updated on user interaction so, parent directives/controllers may use a `$watch`
	 * to handle `currentPage` and `itemsPerPage` changes.
	 *
	 * Make sure to set `loading` property of the pagination object to `true` during the data loading.
	 *
	 * @param {Object} rbsPagination The pagination object:
	 *  <ul>
	 *      <li>**itemsPerPage** - integer, optional, default `20` - will be updated by the directive</li>
	 *      <li>**currentPage** - integer, optional, default `1` - will be updated by the directive</li>
	 *      <li>**totalItems** - integer, optional, default `0`</li>
	 *      <li>**loading** - boolean, optional, default `false`</li>
	 *  </ul>
	 */
	app.directive('rbsPagination', ['RbsChange.Settings', 'RbsChange.PaginationPageSizes', rbsPagination]);

	function rbsPagination(Settings, PaginationPageSizes) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/pagination.twig',
			scope: {
				pagination: '=rbsPagination'
			},
			link: function(scope) {
				scope.predefinedPageSizes = PaginationPageSizes;
				scope.smallWidth = window.screen.width <= 991;

				scope.pagination.pageCount = null;
				if (!scope.pagination.totalItems) {
					scope.pagination.totalItems = 0;
				}
				if (!scope.pagination.itemsPerPage) {
					scope.pagination.itemsPerPage = Settings.get('pagingSize', 20);
				}
				if (!scope.pagination.currentPage) {
					scope.pagination.currentPage = 1;
				}

				scope.updatePageSize = function(newPerPage) {
					if (!newPerPage) {
						return;
					}
					var oldPerPage = scope.pagination.itemsPerPage;
					scope.pagination.currentPage = Math.floor(((scope.pagination.currentPage - 1) * oldPerPage) / newPerPage) + 1;
					scope.pagination.itemsPerPage = newPerPage;
				};
			}
		};
	}
})();