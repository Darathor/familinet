(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsPanel', function() {
		return {
			restrict: 'AE',
			transclude: true,
			templateUrl: 'Rbs/Admin/js/directives/panel.twig',
			scope: true,
			link: function(scope, iElement, iAttrs) {
				scope.panel = {
					title: iAttrs.title,
					icon: iAttrs.icon
				};
				iAttrs.$observe('title', function(value) {
					scope.panel.title = value;
				});
				iAttrs.$observe('icon', function(value) {
					scope.panel.icon = value;
				});
			}
		};
	});

	app.directive('rbsPanelLink', function() {
		return {
			restrict: 'A',
			link: function(scope, iElement, iAttrs) {
				iElement.addClass('list-group-item');
				if (iAttrs.icon) {
					iElement.prepend('<i class="' + iAttrs.icon + '"></i> ');
				}
			}
		};
	});
})();