(function() {
	"use strict";
	var app = angular.module('RbsChange');

	app.directive('rbsPhoneFormatter', ['RbsChange.REST', 'RbsChange.PhoneService', rbsGeoPhoneFormatter]);
	function rbsGeoPhoneFormatter(REST, PhoneService) {

		return {
			restrict: 'A',
			templateUrl: 'Rbs/Admin/js/directives/phone-formatter.twig',
			scope: {
				number: "=",
				regionCode: "=",
				language:"="
			},
			link: function(scope, element, attributes) {
				var defaultCode = scope.language ? scope.language.substring(3) : 'FR';

				scope.addLink = attributes.addLink === 'true';

				scope.$watch('number', function(number) {
					if (number) {
						PhoneService.parseNumber(number, scope.regionCode || defaultCode).then(
							function(parsedNumber) {
								if (parsedNumber) {
									scope.data = parsedNumber;
									parsedNumber.flagCountry = parsedNumber.regionCode.toLowerCase();
									parsedNumber.country = parsedNumber.regionCode;
									parsedNumber.class = (parsedNumber.type === 'MOBILE' || parsedNumber.type === 'FIXED_LINE_OR_MOBILE')
										? 'phone' : 'earphone';

									PhoneService.getRegionNumberConfig().then(
										function(regionNumberConfig) {
											angular.forEach(regionNumberConfig, function(cfg) {
												if (cfg[0] === scope.data.regionCode) {
													scope.data.country = cfg[2];
												}
											});
										},
										function(result) { console.error(result); }
									)
								}
								else {
									scope.data = { empty: true }
								}
							},
							function(error) {
								scope.data = { invalid: true, error: error }
							}
						)
					}
					else {
						scope.data = { empty: true }
					}
				})
			}
		}
	}
})();
