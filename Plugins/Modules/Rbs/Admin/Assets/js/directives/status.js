/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsStatus
	 * @name rbsStatus
	 * @restrict AE
	 *
	 * @description
	 * Displays a bullet that indicates the publication status of a Document.
	 *
	 * @param {ChangeDocument} ngModel The Document.
	 */
	angular.module('RbsChange').directive('rbsStatus', ['RbsChange.Utils', 'RbsChange.i18n', function(Utils, i18n) {
		return {
			restrict: 'AE',
			template: '<span title="(=tooltip=)" class="bullet-status (= publicationStatus =)"><span class="overlay correction" data-ng-if="correction">C</span></span>',
			require: '?ngModel',
			scope: {
				document: '=ngModel'
			},

			link: function(scope, iElement) {
				scope.$watchCollection('document', function(doc) {
					if (doc) {
						if (!doc.publicationStatus && Utils.hasLocalCopy(doc)) {
							scope.publicationStatus = doc.META$.localCopy.publicationStatus;
						}
						else {
							scope.publicationStatus = doc.publicationStatus;
						}

						if (scope.publicationStatus) {
							scope.tooltip = i18n.trans('m.rbs.admin.admin.status_' + scope.publicationStatus.toLowerCase());
							scope.correction = Utils.hasCorrection(doc);
							if (scope.correction) {
								scope.tooltip += ' (' + i18n.trans('m.rbs.admin.admin.with_correction') + ')';
							}
						}
						else {
							iElement.css('visibility', 'hidden');
						}
					}
					else {
						iElement.css('visibility', 'hidden');
					}
				});
			}
		};
	}]);
})();