(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange');

	var RICH_TEXT_BLOCK_NAMES = ['Rbs_Website_Richtext', 'Rbs_Mail_Richtext'];
	var MAIL_SUITABLE_MODELS = ['Rbs_Mail_Mail'];
	var DEFAULT_GRID_SIZE = 12;

	/**
	 * @ngdoc service
	 * @id RbsChange.service:structureEditorService
	 * @name structureEditorService
	 *
	 * @description
	 * This service provides basic operations that are not attached to a particular editor instance (there can be multiple editors is the same page).
	 */
	app.service('RbsChange.structureEditorService', ['$compile', structureEditorService]);
	app.service('RbsChange.structureEditorService', ['$compile', structureEditorService]);

	function structureEditorService($compile) {
		var structureEditorService = this;

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name isRichTextBlock
		 *
		 * @description
		 * Check if a block name corresponds is a rich text.
		 *
		 * @param {string} blockName The block name.
		 * @returns {boolean}
		 */
		structureEditorService.isRichTextBlock = function (blockName) {
			return RICH_TEXT_BLOCK_NAMES.indexOf(blockName) > -1;
		};

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name useMailSuitable
		 *
		 * @description
		 * Check if a model uses mail suitable blocks or standard ones.
		 *
		 * @param {string} modelName The model name.
		 * @returns {boolean}
		 */
		structureEditorService.useMailSuitable = function (modelName) {
			return MAIL_SUITABLE_MODELS.indexOf(modelName) > -1;
		};

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name getDefaultGridSize
		 *
		 * @description
		 * Get the default grid size.
		 *
		 * @returns {number}
		 */
		structureEditorService.getDefaultGridSize = function () {
			return DEFAULT_GRID_SIZE;
		};

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name initEditableZone
		 *
		 * @description
		 * Initialize an editable zone for edition.
		 *
		 * @param {Object} scope The scope.
		 * @param {Object} zoneEl The element in which the editable content should be created.
		 * @param {Object} zoneObj The zone object definition.
		 * @param {boolean} readonly Set to `true` for a read only zone.
		 */
		structureEditorService.initEditableZone = function(scope, zoneEl, zoneObj, readonly) {
			zoneEl.html('');
			zoneEl.addClass('editable-zone');
			zoneEl.attr('data-id', zoneObj.id);
			zoneEl.attr('data-grid', zoneObj.grid);

			angular.forEach(zoneObj.items, function(item) {
				structureEditorService.initItem(scope, zoneEl, item, -1, readonly);
			});
		};

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name getEditableZone
		 *
		 * @description
		 * Get the editable zone ancestor of a given element.
		 *
		 * @param {Object} elm An element.
		 */
		structureEditorService.getEditableZone = function(elm) {
			return elm.closest('.editable-zone');
		};

		/**
		 * @ngdoc method
		 * @methodOf structureEditorService
		 * @name initItem
		 *
		 * @description
		 * Init an item.
		 *
		 * @param {Object} scope The scope of the container.
		 * @param {Object} container The container for the new item.
		 * @param {Object} item The item description.
		 * @param {number} atIndex The index of the new item.
		 * @param {boolean} readonly Set to `true` for a readonly item.
		 */
		structureEditorService.initItem = function(scope, container, item, atIndex, readonly) {
			var html = null;
			switch (item.type) {
				case 'block' :
					html = structureEditorService.initBlock(item, readonly);
					break;
				case 'row' :
					html = structureEditorService.initRow(item);
					break;
				case 'cell' :
					html = structureEditorService.initCell(item);
					break;
				case 'block-chooser' :
					html = structureEditorService.initBlockChooser(item);
					break;
				default :
					throw new Error("Unsupported item type '" + item.type + "'. Must be one of these: 'block', 'row', 'cell', 'block-chooser'.");
			}

			if (container.is('.empty')) {
				container.html('');
				container.removeClass('empty');
			}

			var newEl = jQuery(html);
			if (angular.isUndefined(atIndex) || atIndex === -1 || atIndex > container.children().length - 1) {
				container.append(newEl);
			}
			else {
				jQuery(container.children()[atIndex]).before(newEl);
			}
			$compile(newEl)(scope);
			return newEl;
		};

		structureEditorService.initBlock = function(item, readonly) {
			return '<div data-rbs-block-template="" ' + (readonly ? 'readonly="true" ' : '') + 'data-id="' + item.id + '">' + item.name + '</div>';
		};

		structureEditorService.initRow = function(item) {
			return '<div data-rbs-row="" data-id="' + item.id + '" data-grid="' + item.grid + '"></div>';
		};

		structureEditorService.initCell = function(item) {
			return '<div data-rbs-cell="" data-id="' + item.id + '" data-size="' + item.size + '"></div>';
		};

		structureEditorService.initBlockChooser = function(item) {
			return '<div data-rbs-block-chooser="" data-id="' + item.id + '"></div>';
		};
	}
})(window.jQuery);