(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange');

	/**
	 * RbsStructureEditorSettingsController
	 */
	app.controller('RbsStructureEditorSettingsController', ['proximisModalStack', '$scope', '$uibModalInstance', 'modalData',
		RbsStructureEditorSettingsController]);

	function RbsStructureEditorSettingsController(proximisModalStack, scope, $uibModalInstance, modalData) {
		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};

		$uibModalInstance.result.then(closeFunction, closeFunction);

		scope.uibModalInstance = $uibModalInstance;

		scope.close = function() {
			$uibModalInstance.close();
		};

		scope.modalData = modalData;
	}

	/**
	 * data-rbs-structure-editor-block-settings
	 */
	app.directive('rbsStructureEditorBlockSettings', ['RbsChange.ArrayUtils', 'RbsChange.Blocks', '$http', '$compile', '$templateCache',
		rbsStructureEditorBlockSettings]);

	function rbsStructureEditorBlockSettings(ArrayUtils, Blocks, $http, $compile, $templateCache) {
		return {
			restrict: 'A',

			link: function(scope, element) {
				scope.formValues = {};
				scope.formDirection = 'horizontal';
				scope.blockParametersLoading = false;

				scope.profile = scope.modalData.profile;
				scope.substitutionVariables = scope.modalData.substitutionVariables;

				scope.block = scope.modalData.item;
				scope.block.parameters = scope.block.parameters || {};
				scope.blockParameters = scope.block.parameters;

				if (scope.block || scope.block.name) {
					var blockDefinition = Blocks.getBlock(scope.block.name);
					if (blockDefinition) {
						scope.blockDefinition = blockDefinition;
						$http.get(blockDefinition.template, { cache: $templateCache }).then(
							function(result) {
								var html = jQuery(result.data);
								$compile(html)(scope, function(clone) {
									element.find('[data-role="blockParametersContainer"]').append(clone);
									onRenderTemplateParams();
								});
							},
							function(result) { console.error(result); }
						);
					}
				}

				if (scope.modalData.blockMethods && scope.modalData.blockMethods.refreshPreview) {
					scope.uibModalInstance.result.then(
						function() { scope.modalData.blockMethods.refreshPreview(); },
						function() { scope.modalData.blockMethods.refreshPreview(); }
					);
				}

				scope.$watch('block.parameters.fullyQualifiedTemplateName', function() {
					onRenderTemplateParams();
				});

				function onRenderTemplateParams() {
					if (!blockDefinition) {
						return;
					}

					var templateBlockParametersContainer = element.find('[data-role="templateBlockParametersContainer"]');
					templateBlockParametersContainer.html('');

					var templateURL;
					var fullyQualifiedTemplateName = scope.block.parameters['fullyQualifiedTemplateName'];
					if (angular.isString(fullyQualifiedTemplateName) && fullyQualifiedTemplateName.length) {
						templateURL = blockDefinition.template + '?fullyQualifiedTemplateName=' + fullyQualifiedTemplateName;
					}
					else if (angular.isObject(blockDefinition['defaultTemplate']) && blockDefinition['defaultTemplate']['hasParameter']) {
						templateURL = blockDefinition.template + '?fullyQualifiedTemplateName=default:default';
					}

					if (templateURL) {
						$http.get(templateURL, { cache: $templateCache }).then(
							function(result) {
								templateBlockParametersContainer.html('');
								var html = jQuery(result.data);
								$compile(html)(scope, function(clone) {
									templateBlockParametersContainer.append(clone);
								});
							},
							function(result) { console.error(result); }
						);
					}
				}

				// Block visibility options -----------------------------------

				scope.isVisibleFor = function(device) {
					if (!scope.block) {
						return false;
					}
					if (device == 'raw') {
						return (scope.block.visibility == 'raw');
					}
					else if (scope.block.visibility == 'raw') {
						return false;
					}
					return (!scope.block.visibility || scope.block.visibility.indexOf(device) !== -1);
				};

				scope.toggleVisibility = function(device) {
					var value = !scope.isVisibleFor(device);
					if (device == 'raw') {
						if (value) {
							scope.block.visibility = device;
						}
						else {
							delete scope.block.visibility;
						}
					}
					else {
						if (scope.block.visibility == 'raw') {
							delete scope.block.visibility;
						}
						else {
							if (scope.block.visibility) {
								var splat = scope.block.visibility.split('');
								if (ArrayUtils.inArray(device, splat) !== -1 && !value) {
									ArrayUtils.removeValue(splat, device);
								}
								else {
									if (ArrayUtils.inArray(device, splat) === -1 && value) {
										splat.push(device);
									}
								}
								splat.sort();
								scope.block.visibility = splat.join('');
							}
							else {
								if (value) {
									scope.block.visibility = device;
								}
								else {
									switch (device) {
										case 'X' :
											scope.block.visibility = 'SML';
											break;
										case 'S' :
											scope.block.visibility = 'XML';
											break;
										case 'M' :
											scope.block.visibility = 'XSL';
											break;
										case 'L' :
											scope.block.visibility = 'XSM';
											break;
									}
								}
							}
						}
					}
				};
			}
		};
	}

	/**
	 * data-rbs-block-default-values
	 * /!\ Used in Rbs_Theme_Template editor too.
	 */
	app.directive('rbsBlockDefaultValues', [function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				if (attrs.hasOwnProperty('definitions')) {
					var definitions = angular.fromJson(attrs.definitions);
					angular.forEach(definitions, function(value, name) {
						if (!scope.blockParameters.hasOwnProperty(name)) {
							scope.blockParameters[name] = value;
						}
					});
				}
			}
		};
	}]);

	/**
	 * data-rbs-structure-editor-row-settings
	 */
	app.directive('rbsStructureEditorRowSettings', [function() {
		return {
			restrict: 'A',
			scope: true,
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-row-settings.twig'
		};
	}]);

	/**
	 * data-rbs-structure-editor-cell-settings
	 */
	app.directive('rbsStructureEditorCellSettings', [function() {
		return {
			restrict: 'A',
			scope: true,
			templateUrl: 'Rbs/Admin/js/directives/structure-editor-cell-settings.twig'
		};
	}]);
})(window.jQuery);