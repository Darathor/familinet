(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange');

	jQuery('body').append(
		'<div id="structure-editor-dropzone-indicator"><span class="content"></span><i class="icon-arrow-right"></i></div>'
	);
	var dropZoneIndicator = jQuery('#structure-editor-dropzone-indicator');

	/**
	 * data-rbs-structure-editor
	 */
	app.directive('rbsStructureEditor', ['$timeout', '$compile', 'RbsChange.structureEditorService', 'RbsChange.ArrayUtils',
		'RbsChange.Utils', 'RbsChange.NotificationCenter', 'RbsChange.Navigation', 'proximisModalStack', rbsStructureEditor]);

	function rbsStructureEditor($timeout, $compile, structureEditorService, ArrayUtils, Utils, NotificationCenter, Navigation, proximisModalStack) {
		return {
			restrict: 'AE',
			require: ['ngModel', 'rbsStructureEditor'],
			scope: true,
			transclude: true,
			templateUrl: 'Rbs/Admin/js/directives/structure-editor.twig',

			/**
			 * structureEditor.controller
			 *
			 * This Controller manages all the operations on the blocks inside the editor:
			 * creation, selection, settings, ...
			 *
			 * @param $scope
			 * @param $element
			 * @param $attrs
			 */
			controller: ['$scope', '$element', '$attrs', function(scope, $element, $attrs) {
				var lastSelectedBlock = null;
				var asideContainer, asideElement, asidePanelElement;
				var selectedBlock = null,
					selectedBlockId = -1,
					openSelectedBlockModal = false,
					self = this,
					draggedEl, containerOfDraggedEl,
					dropTarget, dropPosition = -1,
					lastIndicatorY = 0;
				var currentSettingsModal;

				function initAsideElements() {
					if (!asideContainer) {
						asideContainer = jQuery('#rbsWebsitePageBlockPropertiesAside');
						asideElement = asideContainer.find('#structure-editor-aside');
						asidePanelElement = asideElement.find('#structure-editor-aside-panel');
					}
				}

				this.defaultDisplayColumnsFrom = null;

				/**
				 * Set the position of the editor for the settings of the selected block.
				 *
				 * @param blockEl
				 */
				this.positionEditorAside = function(blockEl) {
					blockEl = blockEl || lastSelectedBlock;
					if (blockEl === null) {
						return;
					}
					initAsideElements();
					asideElement.css('opacity', 1);
					asideElement.css('top', blockEl.offset().top - asideContainer.offset().top);
					lastSelectedBlock = blockEl;
				};

				this.clearEditorAside = function() {
					initAsideElements();
					asidePanelElement.html('');
					asideElement.css('opacity', 0);
				};

				scope.$watch('document.model', function() {
					self.mailSuitable = structureEditorService.useMailSuitable(scope.document.model);
				});

				var blockMethods = {};

				this.registerBlockMethods = function(id, methods) {
					blockMethods['item' + id] = methods;
				};

				function getBlockMethods(id) {
					return blockMethods['item' + id];
				}

				function removeBlockMethods(id) {
					blockMethods['item' + id] = undefined;
				}

				var blockScopes = {};

				this.registerBlockScope = function(elm, scope) {
					scope.editorCtrl = this;
					blockScopes['item' + elm.attr('data-id')] = scope;
				};

				function getBlockScope(elm) {
					return blockScopes['item' + elm.attr('data-id')];
				}

				function removeBlockScope(elm) {
					blockScopes['item' + elm.attr('data-id')] = undefined;
				}

				this.isReadOnly = function() {
					return $attrs.readonly === 'true';
				};

				scope.readOnly = this.isReadOnly();

				// Init from context.
				var currentContext = Navigation.getCurrentContext();
				if (currentContext) {
					var contextData = currentContext.savedData('pageEditor');
					if (angular.isObject(contextData)) {
						scope.contextData = contextData;
						selectedBlockId = contextData.blockId;
						openSelectedBlockModal = contextData.openBlockModal;
					}
				}

				/**
				 * Shows the block's settings editor.
				 */
				function showEditorAside(blockEl, params) {
					var item = self.getItemById(blockEl.data('id'));
					if (item === null) {
						self.clearEditorAside();
						return;
					}

					var html;
					if (blockEl.is('[data-rbs-row]')) {
						html = '<div data-rbs-structure-editor-aside-row="" data-id="' + blockEl.data('id') + '"';
					}
					else {
						html = '<div data-rbs-structure-editor-aside-block="" data-id="' + blockEl.data('id') + '"';
					}
					angular.forEach(params, function(value, name) {
						html += ' data-' + name + '="' + value + '"';
					});
					html += '></div>';

					var blockScope = getBlockScope(blockEl);
					if (!blockScope) {
						console.error('[showEditorAside] Scope not found!', item.id, blockEl, params);
						return;
					}

					initAsideElements();
					asidePanelElement.html(html);
					$compile(asidePanelElement)(blockScope);

					self.positionEditorAside(blockEl);
				}

				/**
				 * Selects a block.
				 *
				 * @param {Object} blockEl
				 * @param {Object=} params
				 * @returns {*}
				 */
				this.selectBlock = function(blockEl, params) {
					if (this.isReadOnly()) {
						return;
					}

					if (selectedBlock !== blockEl) {
						if (selectedBlock !== null) {
							selectedBlock.removeClass('active');
						}
						selectedBlock = blockEl;
						selectedBlock.addClass('active');
						$timeout(function() {
							showEditorAside(blockEl.is('[data-rbs-cell]') ? blockEl.parent().parent() : blockEl, params);
						});
					}

					selectedBlockId = blockEl.data('id');
					return blockEl;
				};

				this.openSettingsModal = function(item) {
					var templateUrl;
					switch (item.type) {
						case 'block':
							templateUrl = 'Rbs/Admin/js/directives/structure-editor-settings-block.twig';
							break;
						case 'row':
							templateUrl = 'Rbs/Admin/js/directives/structure-editor-settings-row.twig';
							break;
						case 'cell':
							templateUrl = 'Rbs/Admin/js/directives/structure-editor-settings-cell.twig';
							break;
						default:
							return;
					}

					currentSettingsModal = proximisModalStack.open({
						templateUrl: templateUrl,
						size: 'lg',
						controller: 'RbsStructureEditorSettingsController',
						resolve: {
							modalData: function() {
								return {
									item: item,
									blockMethods: getBlockMethods(item.id),
									profile: self.mailSuitable ? 'Mail' : 'Website',
									substitutionVariables: scope.substitutionVariables,
									defaultDisplayColumnsFrom: self.defaultDisplayColumnsFrom
								};
							}
						}
					});

					function closeFunction() {
						currentSettingsModal = null;
					}

					currentSettingsModal.result.then(closeFunction, closeFunction);
				};

				this.closeSettingsModal = function() {
					if (currentSettingsModal) {
						currentSettingsModal.close();
						return true;
					}
					return false;
				};

				this.reselectBlock = function() {
					selectedBlock = null;
					if (selectedBlockId !== -1) {
						var item = self.getItemById(selectedBlockId);
						$timeout(function() {
							self.selectBlock(self.getBlockByItem(item));
							if (openSelectedBlockModal) {
								self.openSettingsModal(item);
								openSelectedBlockModal = false;
							}
						});

					}
				};

				/**
				 * Returns the selected block.
				 *
				 * @returns {Object|null}
				 */
				this.getSelectedBlock = function() {
					return selectedBlock;
				};

				/**
				 * Returns the selected block id.
				 *
				 * @returns {number|null}
				 */
				this.getSelectedBlockId = function() {
					return selectedBlockId;
				};

				/**
				 * Gets an item from its ID.
				 *
				 * @param id
				 * @returns {*|null}
				 */
				this.getItemById = function(id) {
					return scope.items[id] || null;
				};

				/**
				 * Creates a block in the given container, with the given `item` object (if any) and insert it
				 * at the given `atIndex`.
				 *
				 * @param container
				 * @param {Object=} item
				 * @param {int=} atIndex
				 * @returns {*}
				 */
				this.createBlock = function(container, item, atIndex) {
					var itemChooser;
					item = this.registerNewItem(item ? item : 'block-chooser');

					// If item is a container:
					if (item.type === 'cell') {
						itemChooser = this.registerNewItem('block-chooser');
						item.items = [itemChooser];
					}
					else if (item.type === 'row') {
						item.grid = item.grid || structureEditorService.getDefaultGridSize();
						if (!item.items || item.items.length === 0) {
							var itemCell = this.registerNewItem('cell', { 'size': structureEditorService.getDefaultGridSize() });
							item.items = [itemCell];
							itemCell.items = [this.registerNewItem('block-chooser')];
						}
					}

					return structureEditorService.initItem(scope, container, item, atIndex, this.isReadOnly());
				};

				function resolveSelectedContainer() {
					return selectedBlock.is('[data-rbs-cell]') ? selectedBlock.closest('[data-rbs-row]') : selectedBlock;
				}

				/**
				 * Creates a new block after the selected one.
				 */
				this.newBlockAfter = function() {
					var block = resolveSelectedContainer();
					this.selectBlock(this.createBlock(block.parent(), null, block.index() + 1));
				};

				/**
				 * Creates a new block after each other.
				 */
				this.newBlockBottom = function() {
					this.selectBlock(this.createBlock(resolveSelectedContainer().parent(), null, -1));
				};

				/**
				 * Creates a new block before the selected one.
				 */
				this.newBlockBefore = function() {
					var block = resolveSelectedContainer();
					this.selectBlock(this.createBlock(block.parent(), null, block.index()));
				};

				/**
				 * Creates a new block before each other.
				 */
				this.newBlockTop = function() {
					this.selectBlock(this.createBlock(resolveSelectedContainer().parent(), null, 0));
				};

				/**
				 * Registers a new item in the items registry (scope.items).
				 *
				 * @param item
				 * @param {Object=} defaults
				 * @returns {*}
				 */
				this.registerNewItem = function(item, defaults) {
					if (angular.isString(item)) {
						item = angular.extend(
							{ 'type': item },
							defaults
						);
					}

					// Check if an item with this ID is already registered.
					if (item.id && scope.items[item.id]) {
						throw new Error('Could not register item ' + item.id +
							': another item is registered with the same ID (' + scope.items[item.id].type + ').');
					}

					// Assign new unique ID and register the item.
					item.id = scope.getNextBlockId();
					scope.items[item.id] = item;

					return item;
				};

				/**
				 * @description
				 * Init children for a row or a cell.
				 *
				 * @param {Object} scope The scope of the container.
				 * @param {Object} elm The element of the container.
				 * @param {Object} item The item of the container.
				 * @param {boolean} readonly
				 */
				this.initChildren = function(scope, elm, item, readonly) {
					var container = elm.children('.children');

					if (!item.items || !item.items.length) {
						item.items = [this.registerNewItem('block-chooser')];
					}

					angular.forEach(item.items, function(child) {
						structureEditorService.initItem(scope, container, child, -1, readonly);
					});
				};

				/**
				 * Creates a new block on the left or on the right on the selected block.
				 *
				 * @param where 'left' or 'right'
				 */
				this.newBlockSideways = function(where) {
					var block = this.getSelectedBlock();
					var item = scope.items[block.data('id')];

					var newLeftCellItem = this.registerNewItem('cell', { 'size': structureEditorService.getDefaultGridSize() / 2 });
					var newRightCellItem = this.registerNewItem('cell', { 'size': structureEditorService.getDefaultGridSize() / 2 });
					var newBlockChooserItem = this.registerNewItem('block-chooser');

					if (where === 'left') {
						newLeftCellItem.items = [newBlockChooserItem];
						newRightCellItem.items = [item];
					}
					else {
						newLeftCellItem.items = [item];
						newRightCellItem.items = [newBlockChooserItem];
					}

					this.createBlock(block.parent(), {
						'type': 'row',
						'items': [newLeftCellItem, newRightCellItem]
					}, selectedBlock.index());
					block.remove();

					this.selectBlock(this.getBlockByItem(newBlockChooserItem));
				};

				/**
				 * Returns a block element from its corresponding item.
				 *
				 * @param {Object|number} item
				 * @returns {Object}
				 */
				this.getBlockByItem = function(item) {
					//noinspection JSUnresolvedVariable
					var itemId = (angular.isObject(item) && item.id) ? item.id : item;
					return $element.find('[data-id="' + itemId + '"]').first();
				};

				/**
				 * Removes an item from the items registry.
				 *
				 * @param item
				 */
				this.removeItem = function(item) {
					this.removeBlock($element.find('[data-id="' + item.id + '"]'));
				};

				/**
				 * Removes the given block.
				 *
				 * @param block
				 */
				this.removeBlock = function(block) {
					var id = block.data('id');

					// Close block's settings if the removed block was selected.
					if (block === selectedBlock) {
						self.clearEditorAside();
					}

					removeBlockScope(block);
					removeBlockMethods(id);

					delete scope.items[id];

					var parent = block.parent();
					block.remove();

					// If the parent container of the removed block is now empty, append a Block Chooser.
					if (parent.children().length === 0) {
						this.selectBlock(this.createBlock(parent, { type: 'block-chooser' }));
					}
				};

				// Utility functions -----------------------------------------------------------------------------------

				this.isInColumnLayout = function(el) {
					return el.closest('[data-rbs-cell]').length === 1;
				};

				this.selectParentCell = function(block) {
					this.selectBlock(block.closest('[data-rbs-cell]'), { 'highlight-column': block.closest('[data-rbs-cell]').index() });
				};

				function isContainer(block) {
					return block.is('[data-rbs-row]') || block.is('[data-rbs-cell]');
				}

				function parseEditableZone(zoneEl, output) {
					var id = zoneEl.data('id'), zoneItem;
					zoneItem = {
						id: id,
						grid: zoneEl.data('grid'),
						type: 'container',
						items: []
					};
					output[id] = zoneItem;

					parseChildBlocks(zoneEl, zoneItem.items);
				}

				function parseChildBlocks(parentBlockEl, parentItems) {
					if (parentBlockEl.is('[data-rbs-row], [data-rbs-cell]')) {
						parentBlockEl = parentBlockEl.children('.children');
					}
					parentBlockEl.children('[data-id]').each(function(index, block) {
						var blockEl = jQuery(block);
						var item = scope.items[blockEl.data('id')];
						if (!item || item.type === 'block-chooser') {
							return;
						}
						parentItems.push(item);

						if (isContainer(blockEl)) {
							// Clear any existing items and rebuild the array in parseChildBlocks().
							item.items = [];
							parseChildBlocks(blockEl, item.items);
						}
					});
				}

				// Changes detection and notification ------------------------------------------------------------------
				var isValid = true;
				scope.generateJSON = function() {
					var output = {};

					isValid = true;

					// Find editable zones
					$element.find('[data-editable-zone-id]').each(function(index, zoneEl) {
						parseEditableZone(jQuery(zoneEl), output);
					});

					scope.contentChanged(output, isValid);
					return output;
				};

				var undoDataItemId = 0;
				var operationIcons = {
					move: 'icon-move',
					changeSettings: 'icon-cog',
					changeText: 'icon-edit',
					create: 'icon-plus-sign',
					remove: 'icon-remove-sign',
					resize: 'icon-resize-full',
					visibility: 'icon-eye-open',
					decreaseOffset: 'icon-indent-left',
					increaseOffset: 'icon-indent-right'
				};

				this.notifyChange = function(operation, elementName, element) {
					var self = this;
					$timeout(function() {
						var item = self.getItemById(element.data('id'));
						scope.undoData.unshift({
							id: undoDataItemId++,
							label: (operation + ' ' + elementName),
							item: item,
							data: scope.generateJSON(),
							date: new Date(),
							icon: operationIcons[operation]
						});
					});
				};

				// Drag'n'drop -----------------------------------------------------------------------------------------

				// Draggable elements

				if (!this.isReadOnly()) {
					jQuery($element).on({
						dragstart: function(event) {
							draggedEl = jQuery(this).closest('.block-draggable');
							draggedEl.addClass('dragged');
							containerOfDraggedEl = draggedEl.parent();

							event.originalEvent.dataTransfer.setData('Text', draggedEl.data('id'));
							event.originalEvent.dataTransfer.effectAllowed = 'copyMove';
						},

						dragend: function() {
							draggedEl.removeClass('dragged');
						}

					}, '.block-handle');

					// Droppable elements

					jQuery($element).on({
						dragenter: function(event) {
							event.preventDefault();
							event.stopPropagation();
						},

						dragleave: function(event) {
							event.preventDefault();
							event.stopPropagation();
							dropZoneIndicator.hide();
						},

						dragover: function(event) {
							event.preventDefault();
							event.stopPropagation();

							if (event.originalEvent.dataTransfer.getData('Text') !== jQuery(this).data('id')) {
								var mouseY = event.originalEvent.pageY,
									sameParent = containerOfDraggedEl.data('id') === jQuery(this).data('id'),
									indicatorY = 0, i, midY, childEl, last, finalDropPosition;

								// Reset indicator position if drop zone has changed
								// so that it displays at the right position.
								if (dropTarget !== jQuery(this)) {
									lastIndicatorY = -1;
								}

								dropTarget = jQuery(this);
								dropPosition = -1;

								// Loop through all the children of the hovered element to determine
								// between which blocks the dragged block should be inserted.
								for (i = 0; i < dropTarget.children().length && dropPosition === -1; i++) {
									childEl = jQuery(dropTarget.children()[i]);
									midY = childEl.offset().top + (childEl.outerHeight() / 2);
									if (mouseY < midY) {
										finalDropPosition = dropPosition = i;
										indicatorY = childEl.offset().top;
									}
								}

								if (dropPosition === -1) {
									if (dropTarget.children().length) {
										last = dropTarget.children().last();
										indicatorY = last.offset().top + last.outerHeight();
									}
									else {
										indicatorY = dropTarget.offset().top;
									}
									finalDropPosition = dropTarget.children().length;
									if (sameParent) {
										finalDropPosition--;
									}
								}
								else {
									if (sameParent && dropPosition > draggedEl.index()) {
										finalDropPosition--;
									}
								}

								if (lastIndicatorY !== indicatorY) {
									dropZoneIndicator.find('.content').html(finalDropPosition + 1);
									dropZoneIndicator.css({
										'left': (dropTarget.offset().left - dropZoneIndicator.outerWidth() - 2) + 'px',
										'top': (indicatorY - dropZoneIndicator.outerHeight() / 2) + 'px'
									}).show();
									lastIndicatorY = indicatorY;
								}
							}
						},

						drop: function(event) {
							event.preventDefault();
							event.stopPropagation();

							dropZoneIndicator.hide();

							if (containerOfDraggedEl.data('id') !== jQuery(this).data('id') ||
								draggedEl.index() !== dropPosition) {
								if (dropTarget.is('.empty')) {
									dropTarget.html('');
									dropTarget.removeClass('empty');
								}

								if (dropPosition === -1) {
									dropTarget.append(draggedEl);
								}
								else {
									jQuery(dropTarget.children()[dropPosition]).before(draggedEl);
								}

								self.positionEditorAside(null); // update

								if (containerOfDraggedEl.children().length === 0) {
									self.createBlock(containerOfDraggedEl);
								}

								self.notifyChange('move', 'block', draggedEl, { from: containerOfDraggedEl, to: dropTarget });
							}
						}
					}, '.block-container');

					// Prevent drop on rbs-row (temporary?).
					jQuery($element).on({
						dragenter: function(e) {
							e.preventDefault();
							e.stopPropagation();
						},
						dragover: function(e) {
							e.preventDefault();
							e.stopPropagation();
						}
					}, '[data-rbs-row]');
				}
			}],

			/**
			 * structureEditor.link
			 *
			 * @param scope
			 * @param elm
			 * @param attrs
			 * @param controllers
			 */
			link: function seLinkFn(scope, elm, attrs, controllers) {
				var ngModel = controllers[0],
					ctrl = controllers[1],
					contentReady = false,
					templateInfo,
					originalValue,
					pendingBlockPropertySetter = null;

				scope.$on('Navigation.saveContext', function(event, args) {
					var contextData = {
						undoData: scope.undoData,
						blockId: ctrl.getSelectedBlockId(),
						originalValue: originalValue,
						openBlockModal: ctrl.closeSettingsModal()
					};
					args.context.savedData('pageEditor', contextData);
				});

				if (scope.contextData) {
					originalValue = scope.contextData.originalValue;
					scope.undoData = scope.contextData.undoData;
					scope.contextData = null;
				}

				function getDefaultEmptyContent() {
					var content = {}, blockId = 0;
					elm.find('[data-editable-zone-id]').each(function(index, el) {
						var zoneId = jQuery(el).attr('data-editable-zone-id');
						content[zoneId] = {
							id: zoneId,
							grid: structureEditorService.getDefaultGridSize(),
							type: 'container',
							items: [
								{
									id: ++blockId,
									type: 'block-chooser'
								}
							]
						};
					});

					return content;
				}

				scope.items = {};
				scope.blockIdCounter = 0;
				scope.undoData = [];

				scope.getNextBlockId = function() {
					return ++scope.blockIdCounter;
				};

				function layoutLoaded() {
					return elm.find('.structure-editor').children().length > 0;
				}

				attrs.$observe('template', function(template) {
					if (template) {
						templateInfo = JSON.parse(template);
						elm.find('.structure-editor').html(templateInfo.html);
						ctrl.defaultDisplayColumnsFrom = templateInfo.defaultDisplayColumnsFrom;
						ngModel.$render();
					}
				});

				attrs.$observe('substitutionVariables', function(value) {
					//noinspection JSCheckFunctionSignatures
					scope.substitutionVariables = value ? JSON.parse(value) : [];
				});

				function consumePendingBlockPropertySetter() {
					if (pendingBlockPropertySetter) {
						var item = ctrl.getItemById(pendingBlockPropertySetter.blockId);
						if (item) {
							var value = pendingBlockPropertySetter.value;
							item.parameters[pendingBlockPropertySetter.property] = Utils.isDocument(value) ? value.id : value;
						}
						pendingBlockPropertySetter = null;
					}
				}

				scope.undo = function(index) {
					ctrl.clearEditorAside();
					ngModel.$setViewValue(index < (scope.undoData.length - 1) ? scope.undoData[index + 1].data : originalValue);
					ngModel.$render();
					ArrayUtils.remove(scope.undoData, 0, index);
				};

				// Specify how UI should be updated.
				ngModel.$render = function() {
					var pageContent,
						newZones = [];

					// Parse all the hierarchical items and put them in a flat, ID-indexed array.
					function registerItems(container) {
						if (container.items) {
							angular.forEach(container.items, function(item) {
								scope.items[item.id] = item;
								scope.blockIdCounter = Math.max(item.id, scope.blockIdCounter);
								registerItems(item);
							});
						}
					}

					if (layoutLoaded()) {
						if (angular.isDefined(ngModel.$viewValue) && angular.isUndefined(originalValue)) {
							originalValue = ngModel.$viewValue;
						}

						try {
							pageContent = ngModel.$viewValue ? ngModel.$viewValue : getDefaultEmptyContent();
						}
						catch (e) {
							console.error('Got error:', e);
							pageContent = getDefaultEmptyContent();
						}

						// Create a container for each editable zone.
						angular.forEach(templateInfo.data, function(tplZone) {
							var zone, editableZone;

							// Check if the zone exists in the page.
							zone = pageContent[tplZone.id];
							if (zone) {
								editableZone = jQuery(elm).find('[data-editable-zone-id="' + tplZone.id + '"]');
								if (editableZone.length) {
									if (!zone.hasOwnProperty('items') || zone.items.length == 0) {
										zone.items = [{
											id: scope.getNextBlockId(),
											type: 'block-chooser'
										}];
									}

									registerItems(zone);
									editableZone.addClass('block-container');
									structureEditorService.initEditableZone(scope, editableZone, zone, ctrl.isReadOnly());
								}
								else {
									NotificationCenter.error('Bad template configuration',
										'Could not find editable zone "' + zone.id + '" in page template.');
								}
							}
							else {
								if (tplZone.type === 'container') {
									// Store the zones that are not found in the page to add them later.
									newZones.push(angular.copy(tplZone));
								}

							}
						});

						angular.forEach(newZones, function(zone) {
							var editableZone = jQuery(elm).find('[data-editable-zone-id="' + zone.id + '"]');

							if (editableZone.length) {
								zone.items = [{
									id: scope.getNextBlockId(),
									type: 'block-chooser'
								}];

								registerItems(zone);
								editableZone.addClass('block-container');
								structureEditorService.initEditableZone(scope, editableZone, zone, ctrl.isReadOnly());
							}
						});

						contentReady = true;

						consumePendingBlockPropertySetter();
						$timeout(ctrl.reselectBlock());
					}
				};

				scope.contentChanged = function(newContent, isValid) {
					ngModel.$setViewValue(newContent);
					ngModel.$setValidity('content', isValid);
				};

				scope.$on('$destroy', function() {
					ctrl.clearEditorAside();
				});
			}
		};
	}
})(window.jQuery);