/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function($) {
	"use strict";

	var app = angular.module('RbsChange');

	app.run(['$templateCache', function($templateCache) {
		if (!$templateCache.get('picker-item-default.html')) {
			$templateCache.put('picker-item-default.html',
				'(= item.label =) <small class="text-muted">(= item.model|rbsModelLabel =)</small>');
		}
	}]);

	/**
	 * @ngdoc directive
	 * @id RbsChange.directive:rbsTokenList
	 * @name rbsTokenList
	 * @restrict A
	 *
	 * @description
	 * Displays a list with multiple items inside that can be reordered with drag and drop.
	 *
	 * Items can be Documents objects.
	 *
	 * ### Defining the template for each item ###
	 *
	 * If the items are Documents, it is possible to define a template for each item, based on the Model
	 * of the Documents. In a JavaScript file of the plugin (ie. `admin.js`), add something like this:
	 * ```js
	 * app.run(['$templateCache', function ($templateCache) {
	 *    $templateCache.put(
	 *       // Template ID: 'picker-item-' + Document_Model_Name + '.html'
	 *       'picker-item-Rbs_Catalog_Product.html',
	 *       // The Document is available in this scope as 'item'
	 *       '<span style="line-height: 30px"><img rbs-storage-image="item.adminthumbnail" thumbnail="XS"/> (= item.label =)</span>'
	 *    );
	 * }]);
	 * ```
	 *
	 * @param {Array} items Array of items to display in the list (two-way data-binding).
	 * @param {boolean} disableReordering If true, the user cannot reorder the elements in the list.
	 */
	app.directive('rbsTokenList',
		['RbsChange.ArrayUtils', '$filter', '$templateCache', function(ArrayUtils, $filter, $templateCache) {
			return {
				restrict: 'A',
				replace: true,
				templateUrl: 'Rbs/Admin/js/directives/token-list.twig',

				// Create isolated scope.
				scope: {
					items: '=',
					openItem: '='
				},

				link: function linkFn(scope, elm, attrs) {
					var dragging, isHandle, startIndex, stopIndex,
						placeholder = $('<li class="sortable-placeholder"></li>');

					scope.readonly = attrs.readonly ? true : false;
					scope.disableReordering = attrs.disableReordering ? true : false;

					scope.getItemTemplateName = function(item) {
						if (attrs.itemTemplate) {
							return attrs.itemTemplate
						}
						if (!item || !item.model) {
							return 'picker-item-default.html';
						}

						var tplName = 'picker-item-' + item.model + '.html';
						return $templateCache.get(tplName) ? tplName : 'picker-item-default.html';
					};

					scope.remove = function(index) {
						ArrayUtils.remove(scope.items, index);
					};

					// Enable drag and drop to reorder the items.
					if (!scope.disableReordering) {
						elm.on({
							'mousedown': function() {
								isHandle = true;
							},
							'mouseup': function() {
								isHandle = false;
							}
						}, 'i.icon-reorder');

						elm.on({
							'dragstart': function(e) {
								if (!isHandle) {
									return false;
								}
								isHandle = false;
								var dt = e.originalEvent.dataTransfer;
								dt.effectAllowed = 'move';
								dragging = $(this);
								dragging.addClass('sortable-dragging');
								startIndex = dragging.index();
								dt.setData('Text', dragging.text());
							},

							'dragend': function() {
								dragging.removeClass('sortable-dragging').show();
								stopIndex = placeholder.index();
								placeholder.detach();
								if (startIndex !== stopIndex) {
									ArrayUtils.move(scope.items, startIndex, stopIndex);
									scope.$apply();
								}
								dragging = null;
							}
						}, 'li[data-id]');

						elm.on({
							'dragenter': function(e) {
								e.preventDefault();
								e.originalEvent.dataTransfer.dropEffect = 'move';
							},

							'dragover': function(e) {
								e.preventDefault();
								e.originalEvent.dataTransfer.dropEffect = 'move';

								if (dragging) {
									if (!$(this).is(placeholder)) {
										dragging.hide();
										placeholder.height(dragging.height());
										$(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
										if (placeholder.index() > startIndex) {
											placeholder.html(placeholder.index());
										}
										else {
											placeholder.html(placeholder.index() + 1);
										}
									}
								}
							},

							'drop': function(e) {
								e.stopPropagation();
								e.preventDefault();
								placeholder.after(dragging);
							}
						}, 'li[data-id], li.sortable-placeholder');
					}
				}
			}; // End of Directive object
		}]);
})(window.jQuery);