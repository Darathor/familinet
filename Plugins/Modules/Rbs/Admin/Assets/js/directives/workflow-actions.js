(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentWorkflowActions', ['$timeout', '$q', 'RbsChange.REST', 'RbsChange.Utils', 'RbsChange.i18n', '$location',
		function($timeout, $q, REST, Utils, i18n, $location) {
			return {
				restrict: 'A',
				replace: true,
				templateUrl: 'Rbs/Admin/js/directives/workflow-actions.twig',

				scope: {
					document: '=',
					onClose: '&'
				},

				link: function(scope, element, attrs) {
					var oldCssClass = null;

					scope.data = {
						loaded: false,
						rejectReason: '',
						contentAction: 'accept',
						action: '',
						useWorkflow: true,
						helpShown: false,
						error: null
					};

					scope.closable = element.is('[on-close]');

					function freezeUI() {
						element.find('button').attr('disabled', 'disabled');
					}

					function unfreezeUI() {
						element.find('button').removeAttr('disabled');
						scope.data.progress = undefined;

						REST.clearDocumentCache();
						angular.extend(scope.document, {
							META$: {
								links: {},
								actions: {},
								locales: [],
								correction: null,
								treeNode: null,
								tags: null
							},
							correction:false
						});
					}

					function unfreezeUIError(error) {
						scope.data.error = error;
						unfreezeUI();
					}

					function unfreezeUISuccess() {
						scope.data.error = null;
						unfreezeUI();
					}

					function accept(actionName, params) {
						freezeUI();
						REST.executeTaskByCodeOnDocument(actionName, scope.document, params).then(
							unfreezeUISuccess,
							unfreezeUIError
						);
					}

					function reject(actionName, reason) {
						freezeUI();
						REST.executeTaskByCodeOnDocument(actionName, scope.document, { 'reason': reason }).then(
							unfreezeUISuccess,
							unfreezeUIError
						);
					}

					function publicationDatesChanged() {
						return !angular.equals(scope.data.startPublication, scope.document.startPublication) ||
							!angular.equals(scope.data.endPublication, scope.document.endPublication);
					}

					function doSubmit() {
						if (scope.data.action === 'contentValidation' && scope.data.contentAction === 'reject') {
							reject(scope.data.action, scope.data.rejectReason);
						}
						else {
							accept(scope.data.action);
						}
					}

					function doSwitchStatus(params) {
						if (!params) {
							params = {}
						}
						params.documentId = scope.document.id;

						if (scope.document.LCID) {
							params.LCID = scope.document.LCID;
						}

						freezeUI();
						if (params.correctionId) {
							REST.postAction('mergeCorrection', null, params).then(
								unfreezeUISuccess,
								unfreezeUIError
							)
						}
						else if (scope.document.publicationStatus === 'PUBLISHABLE') {
							REST.postAction('freeze', null, params).then(
								unfreezeUISuccess,
								unfreezeUIError
							)
						}
						else {
							REST.postAction('publish', null, params).then(
								unfreezeUISuccess,
								unfreezeUIError
							)
						}
					}

					scope.$watch('document', function(doc) {
						if (Utils.isDocument(doc)) {
							REST.ensureLoaded(doc).then(
								function(doc) {
									if (scope.document !== doc) {
										angular.extend(scope.document, doc);
										return;
									}
									if (oldCssClass) {
										element.prev('.workflow-indicator').addBack().removeClass(oldCssClass);
									}
									element.prev('.workflow-indicator').addBack().addClass(doc.publicationStatus);
									oldCssClass = doc.publicationStatus;

									scope.data.action = null;
									var actions = ['requestValidation', 'contentValidation', 'publicationValidation', 'freeze', 'unfreeze',
										'switchStatus'];
									angular.forEach(actions, function(action) {
										if (doc.isActionAvailable(action)) {
											scope.data.action = action;
											if (action === 'switchStatus') {
												scope.data.useWorkflow = false;
											}
										}
									});
									if (scope.data.action === null && attrs.standalone === 'true') {
										$location.path(doc.url());
										return;
									}

									scope.data.loaded = true;
									if (Utils.hasCorrection(doc)) {
										scope.data.action = 'correction';
										updateCorrection(doc);
									}

									scope.data.startPublication = doc.startPublication;
									scope.data.endPublication = doc.endPublication;

									REST.modelInfo(doc).then(
										function(modelInfo) {
											scope.modelInfo = modelInfo;
										},
										function(result) { console.error(result); }
									);
								},
								function(result) { console.error(result); }
							);
						}
					}, true);

					scope.submit = function() {
						// When validating the publication, we need to save the 'startPublication' and 'endPublication'
						// properties on the Document before executing the workflow task.
						if (scope.data.action === 'publicationValidation' && publicationDatesChanged()) {
							scope.document.startPublication = scope.data.startPublication;
							scope.document.endPublication = scope.data.endPublication;
							REST.save(scope.document, null, ['startPublication', 'endPublication']).then(
								function(updated) {
									angular.extend(scope.document, updated);
									doSubmit();
								},
								function(result) { console.error(result); }
							);
						}
						else if (scope.data.action === 'switchStatus') {
							doSwitchStatus();
						}
						else {
							doSubmit();
						}
					};

					scope.closeWorkflow = function() {
						scope.onClose();
					};

					scope.runDirectPublication = function() {
						freezeUI();
						var defer = $q.defer();
						if (scope.document.META$.actions &&
							scope.document.META$.actions.hasOwnProperty('directPublication')) {
							REST.call(scope.document.META$.actions['directPublication'].href).then(
								function() {
									unfreezeUISuccess();
									defer.resolve();
								},
								function(result) { console.error(result); }
							);
						}

						return defer.promise;
					};

					//
					// Corrections
					//

					scope.correctionData = {};

					function updateCorrection(doc) {
						scope.correctionData.correctionInfo = angular.copy(doc.META$.correction);
						scope.correctionData.diff = [];
						scope.correctionData.advancedDiffs = true;

						scope.correctionData.params = {
							applyCorrectionWhen: scope.correctionData.correctionInfo.publicationDate ? 'planned' : 'now',
							plannedCorrectionDate: scope.correctionData.correctionInfo.publicationDate
						};

						scope.correctionData.diff.length = 0;
						if (scope.correctionData.correctionInfo) {
							angular.forEach(scope.correctionData.correctionInfo.propertiesNames, function(property) {
								scope.correctionData.diff.push({
									id: property,
									current: doc[property],
									original: scope.correctionData.correctionInfo.original[property]
								});
							});
						}
					}

					scope.correctionData.reject = false;

					scope.correctionData.deleteCorrection = function() {
						if (!window.confirm(i18n.trans('m.rbs.admin.admin.correction_confirm_delete'))) {
							return;
						}

						REST.modelInfo(scope.document.model).then(
							function(modelInfo) {
								var copy = angular.copy(scope.document);
								if (Utils.removeCorrection(copy, null)) {
									angular.forEach(copy, function(value, property) {
										if (!modelInfo.properties.hasOwnProperty(property) || !modelInfo.properties[property].localized) {
											delete copy[property];
										}
									});
									copy.id = scope.document.id;
									copy.model = scope.document.model;
									copy.LCID = scope.document.LCID;
									copy.refLCID = scope.document.refLCID;

									REST.save(copy).then(
										function(updated) {
											delete scope.document.META$.correction;
											angular.extend(scope.document, updated);
											$location.path(scope.document.url());
										},
										function(result) { console.error(result); }
									);
								}
							},
							function(result) { console.error(result); }
						);
					};

					scope.correctionData.canChooseDate = function() {
						if (!scope.correctionData.correctionInfo) {
							return false;
						}
						var cs = scope.correctionData.correctionInfo.status;
						return scope.correctionData.diff.length > 0 && (cs === 'DRAFT' || cs === 'VALIDATION' || cs === 'VALIDCONTENT');
					};

					scope.correctionData.requestValidation = function() {
						if (scope.data.useWorkflow) {
							accept('requestValidation');
						}
						else {
							var params = scope.correctionData.params;
							params.correctionId = scope.correctionData.correctionInfo.id;
							doSwitchStatus(params);
						}
					};

					scope.correctionData.contentValidation = function() {
						accept('contentValidation');
					};

					scope.correctionData.rejectContentValidation = function(message) {
						reject('contentValidation', message);
					};

					scope.correctionData.publicationValidation = function() {
						accept('publicationValidation');
					};
				}
			};
		}
	]);
})();