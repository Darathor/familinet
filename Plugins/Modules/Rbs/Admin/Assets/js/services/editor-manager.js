/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.factory('RbsChange.EditorManager', ['localStorageService', function(localStorageService) {
		var localCopyRepo;

		localCopyRepo = localStorageService.get('localCopy');

		if (!angular.isObject(localCopyRepo)) {
			localCopyRepo = {};
			commitLocalCopyRepository();
		}

		// Local copy methods.

		function commitLocalCopyRepository() {
			localStorageService.set('localCopy', localCopyRepo);
		}

		function makeLocalCopyKey(doc) {
			var key;
			if (doc.id < 0) {
				key = doc.model + '-' + 'new';
			}
			else {
				key = doc.model + '-' + doc.id;
			}
			if (doc.LCID) {
				key += '-' + doc.LCID;
			}
			return key;
		}

		return {
			// Local copy public API.

			'saveLocalCopy': function(doc, url) {
				var key = makeLocalCopyKey(doc);
				doc.META$.localCopy = {
					saveDate: moment().format(),
					documentVersion: doc.documentVersion,
					modificationDate: doc.modificationDate,
					publicationStatus: doc.publicationStatus,
					editorUrl: url || doc.url()
				};
				delete doc.documentVersion;
				delete doc.modificationDate;
				delete doc.publicationStatus;
				localCopyRepo[key] = doc;
				commitLocalCopyRepository();
			},

			'getLocalCopy': function(doc) {
				var key = makeLocalCopyKey(doc);
				return localCopyRepo.hasOwnProperty(key) ? localCopyRepo[key] : null;
			},

			'removeLocalCopy': function(doc) {
				var key = makeLocalCopyKey(doc);
				if (localCopyRepo.hasOwnProperty(key)) {
					delete localCopyRepo[key];
					delete doc.META$.localCopy;
					commitLocalCopyRepository();
				}
			},

			'removeCreationLocalCopy': function(doc) {
				var key = makeLocalCopyKey(doc);
				if (localCopyRepo.hasOwnProperty(key)) {
					delete localCopyRepo[key];
					delete doc.META$.localCopy;
					commitLocalCopyRepository();
				}
			},

			'removeAllLocalCopies': function() {
				for (var key in localCopyRepo) {
					if (localCopyRepo.hasOwnProperty(key)) {
						delete localCopyRepo[key];
					}
				}
				localStorageService.remove("temporaryId");
				commitLocalCopyRepository();
			},

			'getLocalCopies': function() {
				return localCopyRepo;
			}
		};
	}]);
})();