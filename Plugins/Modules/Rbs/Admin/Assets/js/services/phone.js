(function() {
	"use strict";
	var app = angular.module('RbsChange');

	app.provider('RbsChange.PhoneService', PhoneProvider);

	function PhoneProvider() {
		var regionNumberConfig = null;

		var parsedPhoneNumbers = {};

		this.$get = ['$rootScope', '$q', 'RbsChange.REST',
			function($rootScope, $q, REST) {

				function getRegionNumberConfig() {
					var deferred = $q.defer();
					if (regionNumberConfig !== null) {
						if (angular.isArray(regionNumberConfig)) {
							deferred.resolve(regionNumberConfig);
						}
						else {
							return regionNumberConfig.promise;
						}
					}
					else {
						regionNumberConfig = deferred;
						REST.apiGet('Rbs/Geo/phoneNumberConfiguration').then(
							function(data) {
								regionNumberConfig = data.items;
								deferred.resolve(regionNumberConfig);
							},
							function(result) { console.error(result); }
						)
							.catch(function(data, status) {
								console.error('getRegionNumberConfig', data, status);
								regionNumberConfig = [];
								deferred.resolve(regionNumberConfig);
							});
					}
					return deferred.promise;
				}

				function parseNumber(phoneNumber, regionCode) {
					var deferred = $q.defer();
					if (!angular.isString(phoneNumber) || (phoneNumber.length === 0)) {
						//Empty number
						deferred.resolve(null);
					}
					else if (phoneNumber.length < 5 || !regionCode) {
						deferred.reject('Too short number');
					}
					else if (!angular.isString(regionCode) || regionCode.length != 2) {
						deferred.reject('Invalid region code');
					}
					else {
						var key = phoneNumber + '.' + regionCode;
						if (parsedPhoneNumbers.hasOwnProperty(key)) {
							var parsedNumber = parsedPhoneNumbers[key];
							if (parsedNumber) {
								if (parsedNumber.promise) {
									return parsedNumber.promise;
								}
								deferred.resolve(parsedNumber);
							}
							else {
								deferred.reject('Invalid number');
							}
						}
						else {
							parsedPhoneNumbers[key] = deferred;
							REST.apiPost('Rbs/Geo/parsePhoneNumber', { number: phoneNumber, regionCode: regionCode }).then(
								function(data) {
									var common = data.dataSets.common;
									parsedPhoneNumbers[key] = common;
									parsedPhoneNumbers[common.national + '.' + common.regionCode] = common;
									deferred.resolve(common);
								},
								function(result) { console.error(result); }
							)
								.catch(function() {
									parsedPhoneNumbers[key] = false;
									deferred.reject('Invalid number');
								});
						}
					}
					return deferred.promise;
				}

				function parseMobileNumber(phoneNumber, regionCode) {
					return parseNumber(phoneNumber, regionCode).then(
						function(parsedNumber) {
							if (!parsedNumber || parsedNumber.type == 'MOBILE' || parsedNumber.type == 'FIXED_LINE_OR_MOBILE') {
								return parsedNumber;
							}
							return $q.reject('Invalid mobile number');
						},
						function(result) { console.error(result); }
					);
				}

				function parseFixedLineNumber(phoneNumber, regionCode) {
					return parseNumber(phoneNumber, regionCode).then(
						function(parsedNumber) {
							if (!parsedNumber || parsedNumber.type == 'FIXED_LINE' || parsedNumber.type == 'FIXED_LINE_OR_MOBILE') {
								return parsedNumber;
							}
							return $q.reject('Invalid fixed line number');
						},
						function(result) { console.error(result); }
					);
				}

				// Public API
				return {
					getRegionNumberConfig: getRegionNumberConfig,
					parseNumber: parseNumber,
					parseMobileNumber: parseMobileNumber,
					parseFixedLineNumber: parseFixedLineNumber
				};
			}
		]
	}
})();
