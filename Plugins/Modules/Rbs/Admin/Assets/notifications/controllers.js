/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {

	"use strict";

	var app = angular.module('RbsChange');

	app.config(['$routeProvider', function($routeProvider) {
		$routeProvider
			.when('/notifications/',
				{
					templateUrl: 'Rbs/Admin/notifications/list.twig',
					reloadOnSearch: false,
					labelKey: 'm.rbs.admin.admin.notifications|ucf'
				});
	}]);

	/**
	 * @name Rbs_Admin_NotificationsController
	 */
	function ChangeAdminNotificationsController($scope, $q, UserNotifications, User) {
		$scope.view = 'unread';
		$scope.showList = false;

		User.ready().then(function() {
			$scope.showList = true;
			$scope.$watch('view', function(view) {
				if (view === 'unread') {
					$scope.loadQuery = UserNotifications.getNotificationQuery('new');
				}
				else {
					$scope.loadQuery = UserNotifications.getNotificationQuery();
				}
			});
		});

		$scope.dlExt = {
			markAsRead: function(data) {
				if (angular.isArray(data)) {
					var promises = [];
					angular.forEach(data, function(n) {
						promises.push(UserNotifications.markAsRead(n, false));
					});
					var p = $q.all(promises);
					p.then(function() {
						UserNotifications.reload();
						$scope.dlExt.refresh();
					});
					return p;
				}
				else {
					return UserNotifications.markAsRead(data, true).then(function() {
						$scope.dlExt.refresh();
					});
				}
			},

			archive: function(data) {
				if (angular.isArray(data)) {
					var promises = [];
					angular.forEach(data, function(n) {
						promises.push(UserNotifications.archive(n, false));
					});
					var p = $q.all(promises);
					p.then(function() {
						UserNotifications.reload();
						$scope.dlExt.refresh();
					});
					return p;
				}
				else {
					return UserNotifications.archive(data, true).then(function() {
						$scope.dlExt.refresh();
					})
				}
			}
		};
	}

	ChangeAdminNotificationsController.$inject = [
		'$scope', '$q',
		'RbsChange.UserNotifications',
		'RbsChange.User'
	];
	app.controller('Rbs_Admin_NotificationsController', ChangeAdminNotificationsController);

})();