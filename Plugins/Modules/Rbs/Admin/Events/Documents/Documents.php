<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Events\Documents;

/**
 * @name \Rbs\Admin\Events\Documents\Documents
 */
class Documents
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onUserDelete($event)
	{
		$user = $event->getDocument();
		if ($user instanceof \Rbs\User\Documents\User)
		{
			$applicationServices = $event->getApplicationServices();
			$transactionManager = $applicationServices->getTransactionManager();

			try
			{
				// Deletion of all privileges accorded to user in deletion process
				$transactionManager->begin();
				$dbProvider = $applicationServices->getDbProvider();

				$deleteStatement = $dbProvider->getNewStatementBuilder();
				$fb = $deleteStatement->getFragmentBuilder();
				$deleteStatement->delete($fb->getPermissionRuleTable());
				$deleteStatement->where(
					$fb->eq($fb->column('accessor_id'), $user->getId())
				);

				$dbProvider->executeQuery($deleteStatement->deleteQuery());
				$transactionManager->commit();
			}
			catch (\RuntimeException $exc)
			{
				$applicationServices->getLogging()->error($exc->getMessage());
				$transactionManager->rollBack($exc);
			}
			catch (\LogicException $exc)
			{
				$applicationServices->getLogging()->error($exc->getMessage());
				$transactionManager->rollBack($exc);
			}
		}
	}
}