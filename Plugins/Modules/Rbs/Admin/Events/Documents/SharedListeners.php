<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Events\Documents;

/**
 * @name \Rbs\Admin\Events\Documents\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$events->attach('Rbs_User_User', 'documents.delete',
			function (\Change\Documents\Events\Event $event) { (new \Rbs\Admin\Events\Documents\Documents())->onUserDelete($event); }, 15);
	}

}