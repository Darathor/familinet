<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Actions;

/**
 * @name \Rbs\Admin\Http\Actions\GetHome
 */
class GetHome
{
	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function execute($event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$manager = $genericServices->getAdminManager();
		$OAuth = $event->getApplicationServices()->getOAuthManager();
		$consumer = $OAuth->getConsumerByApplication('Rbs_Admin');
		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$pluginManager = $applicationServices->getPluginManager();
		$LCID = $i18nManager->getLCID();
		$result = new \Rbs\Admin\Http\Result\Home();
		$templateFileName = \implode(DIRECTORY_SEPARATOR, [__DIR__, 'Assets', 'home.twig']);

		$devMode = $application->inDevelopmentMode();
		if ($devMode)
		{
			$event->setParam('ACTION_NAME', __METHOD__);
		}

		$applicationShortName = $i18nManager->trans('m.rbs.ua.common.ui_main_admin_panel', ['ucf']);
		$applicationPackageName = $i18nManager->trans('m.rbs.ua.common.application_package_name', ['ucf']);
		$attributes = [
			'applicationName' => $applicationShortName . ' - ' . $applicationPackageName,
			'applicationShortName' => $applicationShortName,
			'applicationPackageName' => $applicationPackageName,
			'application' => [
				'uuid' => $application->getConfiguration('Change/Application/uuid'),
				'env' => $application->getConfiguration('Change/Application/env')
			],
			'devMode' => $devMode,
			'angularDevMode' => $devMode && $application->getConfiguration('Change/Presentation/AngularDevMode'),
			'baseURL' => $event->getUrlManager()->getByPathInfo('/')->normalize()->toString(),
			'restURL' => $event->getUrlManager()->getByPathInfo('../rest.php/')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => \substr($LCID, 0, 2),
			'lowerHyphenLCID' => \strtolower(\str_replace('_', '-', $LCID)),
			'OAuth' => \array_merge($consumer->toArray(), ['realm' => 'Rbs_Admin']),
			'mainMenu' => $manager->getMainMenu(),
			'routes' => $this->getRoutes($manager),
			'i18n' => $this->getI18nKeys($pluginManager, $i18nManager, $LCID),
			'supportedLCIDs' => $i18nManager->getSupportedLCIDs(),
			'blockDefinitions' => $this->getBlockDefinitions($applicationServices->getBlockManager()),
			'modelDefinitions' => $this->getModelDefinitions($applicationServices->getModelManager(), $i18nManager)
		];
		$attributes['__change'] = $attributes;

		$resources = new \Rbs\Admin\Http\Resources($application);
		$assetManager = $resources->getNewAssetManager();
		$resources->registerPlugins($assetManager, $applicationServices->getPluginManager());

		$attributes['scripts'] = $resources->getScriptsUrl($assetManager);
		$attributes['styles'] = $resources->getStylesUrl($assetManager);

		$eventManager = $manager->getEventManager();
		$args = $eventManager->prepareArgs(['attributes' => $attributes]);

		$manager->getEventManager()->trigger('getHomeAttributes', $manager, $args);

		$html = $manager->renderTemplateFile($templateFileName, $args['attributes']);
		$renderer = function () use ($html) {
			return $html;
		};
		$result->setRenderer($renderer);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Presentation\Blocks\BlockManager $blockManager
	 * @return array
	 */
	protected function getBlockDefinitions($blockManager)
	{
		$array = [];
		$names = $blockManager->getBlockNames();
		foreach ($names as $name)
		{
			$information = $blockManager->getBlockInformation($name);
			if ($information)
			{
				[$v, $m, $b] = \explode('_', $name);
				$data = [
					'name' => $information->getName(),
					'label' => $information->getLabel(),
					'template' => 'Block/' . $v . '/' . $m . '/' . $b . '/parameters.twig',
					'mailSuitable' => $information->isMailSuitable(),
					'defaultTTL' => $information->getDefaultTTL()
				];

				// Default template information.
				$templateInformation = $information->getDefaultTemplateInformation();
				if ($templateInformation && \count($templateInformation->getParametersInformation()) > 0)
				{
					$data['defaultTemplate']['hasParameter'] = true;
				}

				$array[$information->getSection()][] = $data;
			}
		}
		\ksort($array);
		return $array;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param string $LCID
	 * @return array
	 */
	protected function getI18nKeys($pluginManager, $i18nManager, $LCID)
	{
		$modules = $pluginManager->getModules();
		$packages = [];

		// Core.
		foreach (['filesize'] as $subPackage)
		{
			$packageName = \implode('.', ['c', $subPackage]);
			$keys = $i18nManager->getTranslationsForPackage($packageName, $LCID);
			if (\is_array($keys))
			{
				$package = [];
				foreach ($keys as $key => $value)
				{
					$package[$key] = $value;
				}
				$packages[$packageName] = $package;
			}
		}

		// Modules.
		foreach ($modules as $module)
		{
			if ($module->isAvailable())
			{
				foreach (['admin', 'documents'] as $subPackage)
				{
					$packageName = \implode('.', ['m', \strtolower($module->getVendor()), \strtolower($module->getShortName()), $subPackage]);
					$keys = $i18nManager->getTranslationsForPackage($packageName, $LCID);
					if (\is_array($keys))
					{
						$package = [];
						foreach ($keys as $key => $value)
						{
							$package[$key] = $value;
						}
						$packages[$packageName] = $package;
					}
				}
			}
		}

		return $packages;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return array
	 */
	protected function getModelDefinitions($modelManager, $i18nManager)
	{
		$models = [];

		foreach ($modelManager->getModelsNames() as $modelName)
		{
			$model = $modelManager->getModelByName($modelName);
			if (!$model)
			{
				continue;
			}

			$pluginKey = \strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
			$pluginLabel = $i18nManager->trans($pluginKey, ['ucf']);
			if ($pluginKey === $pluginLabel)
			{
				continue;
			}

			$models[] = [
				'name' => $model->getName(),
				'label' => $i18nManager->trans($model->getLabelKey(), ['ucf']),
				'leaf' => !$model->hasDescendants(),
				'root' => !$model->hasParent(),
				'abstract' => $model->isAbstract(),
				'inline' => $model->isInline(),
				'publishable' => $model->isPublishable(),
				'localized' => $model->isLocalized(),
				'activable' => $model->isActivable(),
				'useCorrection' => $model->useCorrection(),
				'editable' => $model->isEditable(),
				'plugin' => $pluginLabel,
				'descendants' => $model->getDescendantsNames(),
				'ancestors' => $model->getAncestorsNames(),
				'compatible' => \array_merge([$model->getName()], $model->getAncestorsNames())
			];
		}

		return $models;
	}

	/**
	 * @param \Rbs\Admin\AdminManager $adminManager
	 * @return array
	 */
	protected function getRoutes(\Rbs\Admin\AdminManager $adminManager)
	{
		$routes = $adminManager->getRoutesHelper()->getRoutes();

		//WildCard :IDENTIFIER at last
		\krsort($routes);

		return $routes;
	}
}