<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Actions;

/**
 * @name \Rbs\Admin\Http\Actions\GetHtmlBlockParameters
 */
class GetHtmlBlockParameters
{
	/**
	 * Use Required Event Params: vendor, shortModuleName, shortBlockName
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		$result = new \Rbs\Admin\Http\Result\Renderer();
		$vendor = $event->getParam('vendor');
		$shortModuleName = $event->getParam('shortModuleName');
		$shortBlockName = $event->getParam('shortBlockName');

		$blockName = $vendor . '_' . $shortModuleName . '_' . $shortBlockName;
		$information = $event->getApplicationServices()->getBlockManager()->getBlockInformation($blockName);

		if ($information instanceof \Change\Presentation\Blocks\Information)
		{
			$plugin = $event->getApplicationServices()->getPluginManager()->getModule($vendor, $shortModuleName);
			if ($plugin && $plugin->isAvailable())
			{
				$fullyQualifiedTemplateName = $event->getRequest()->getQuery('fullyQualifiedTemplateName');
				$workspace = $event->getApplication()->getWorkspace();

				if ($fullyQualifiedTemplateName)
				{
					$templateInformation = $information->getTemplateInformation($fullyQualifiedTemplateName);
					if ($templateInformation)
					{
						$filePath = $workspace->pluginsModulesPath('Rbs', 'Admin', 'Assets', 'block-template-parameters.twig');
						$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);

						$genericServices = $event->getServices('genericServices');
						if ($genericServices instanceof \Rbs\Generic\GenericServices)
						{
							$manager = $genericServices->getAdminManager();
						}
						else
						{
							throw new \RuntimeException('GenericServices not set', 999999);
						}

						$defaultValues = [];
						foreach ($templateInformation->getParametersInformation() as $parameter)
						{
							if ($parameter->getDefaultValue() !== null)
							{
								$defaultValues[$parameter->getName()] = $parameter->getDefaultValue();
							}
						}

						$attributes = [
							'templateInformation' => $templateInformation,
							'information' => $information,
							'defaultValues' => $defaultValues
						];
						$renderer = function () use ($filePath, $manager, $attributes) {
							return $manager->renderTemplateFile($filePath, $attributes);
						};
						$result->setRenderer($renderer);
						$event->setResult($result);
						return;
					}
				}
				else
				{
					$filePath = $workspace->composePath($plugin->getAssetsPath(), 'Admin', 'Blocks', $shortBlockName . '.twig');
					if (is_readable($filePath))
					{
						$moduleName = $plugin->getName();
						$pathName = $workspace->composePath('Blocks', $shortBlockName . '.twig');
					}
					else
					{
						$moduleName = 'Rbs_Admin';
						$pathName = 'block-parameters.twig';
					}

					$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);

					$genericServices = $event->getServices('genericServices');
					if ($genericServices instanceof \Rbs\Generic\GenericServices)
					{
						$manager = $genericServices->getAdminManager();
					}
					else
					{
						throw new \RuntimeException('GenericServices not set', 999999);
					}

					$defaultValues = [];
					foreach ($information->getParametersInformation() as $parameter)
					{
						if ($parameter->getDefaultValue() !== null)
						{
							$defaultValues[$parameter->getName()] = $parameter->getDefaultValue();
						}
					}

					$directiveName = 'data-custom-block-parameters-' .
						\Change\Stdlib\StringUtils::snakeCase(\Change\Stdlib\StringUtils::camelCase($information->getName()), '-');

					$attributes = [
						'information' => $information,
						'directiveName' => $directiveName,
						'defaultTemplateInformation' => $information->getDefaultTemplateInformation(),
						'defaultValues' => $defaultValues,
						'templatesByTheme' => $this->getTemplatesByTheme($information, $event)
					];

					$renderer = function () use ($moduleName, $pathName, $manager, $attributes) {
						return $manager->renderModuleTemplateFile($moduleName, $pathName, $attributes);
					};
					$result->setRenderer($renderer);
					$event->setResult($result);
					return;
				}
			}
		}

		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		$result->setRenderer(function () {
			return null;
		});
		$event->setResult($result);
	}

	/**
	 * @param \Change\Presentation\Blocks\Information $information
	 * @param \Change\Http\Event $event
	 * @return array
	 */
	protected function getTemplatesByTheme($information, $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$byThemeName = [];

		$themePrefix = $i18nManager->trans('m.rbs.theme.documents.theme', ['ucf', 'lab']);
		$themeManager = $event->getApplicationServices()->getThemeManager();
		foreach ($information->getTemplatesInformation() as $templateInformation)
		{
			$themeName = $templateInformation->getThemeName() ?: 'common';
			$byThemeName[$themeName][] = [
				'value' => $templateInformation->getFullyQualifiedTemplateName(),
				'information' => $templateInformation,
				'label' => $templateInformation->getLabel()
			];
		}

		$commonLabel = $i18nManager->trans('m.rbs.admin.admin.common_templates', ['ucf']);
		$byThemeLabel = [];
		foreach ($byThemeName as $name => $templates)
		{
			if ($name == 'common')
			{
				$label = $commonLabel;
			}
			else
			{
				$theme = $themeManager->getByName($name);
				$label = $themePrefix . ' ' . (($theme instanceof \Rbs\Theme\Documents\Theme) ? $theme->getLabel() : $name);
			}
			$byThemeLabel[$label] = $templates;
		}

		$callback = function ($a, $b) use ($commonLabel) {
			if ($a == $b)
			{
				return 0;
			}
			elseif ($a == $commonLabel)
			{
				return -1;
			}
			elseif ($b == $commonLabel)
			{
				return 1;
			}
			else
			{
				return $a < $b ? -1 : 1;
			}
		};

		uksort($byThemeLabel, $callback);
		return $byThemeLabel;
	}
}