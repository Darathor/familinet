<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Actions;

/**
 * @name \Rbs\Admin\Http\Actions\Preview
 */
class Preview
{
	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function execute($event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$manager = $genericServices->getAdminManager();

		$result = new \Rbs\Admin\Http\Result\Renderer();
		$templateFileName = implode(DIRECTORY_SEPARATOR, [__DIR__, 'Assets', 'preview.twig']);
		$html = $manager->renderTemplateFile($templateFileName, []);

		$renderer = function () use ($html) {
			return $html;
		};
		$result->setRenderer($renderer);
		$event->setResult($result);
	}
}