<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http;

/**
 * @name \Rbs\Admin\Http\Resolver
 */
class Resolver extends \Change\Http\BaseResolver
{
	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	public function resolve($event)
	{
		$request = $event->getRequest();
		$path = $request->getPath();
		if (\strpos($path, '//') !== false)
		{
			return;
		}
		if ($path === $request->getServer('SCRIPT_NAME'))
		{
			$path = '/';
		}

		if ($path === '/')
		{
			$action = static function ($event) {
				(new \Rbs\Admin\Http\Actions\GetHome())->execute($event);
			};
			$event->setAction($action);
			return;
		}

		$relativePath = $this->getRelativePath($path);
		$event->setParam('relativePath', $relativePath);
		if ($relativePath === 'Rbs/Admin/preview.html')
		{
			$action = static function ($event) {
				(new \Rbs\Admin\Http\Actions\Preview())->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if (\preg_match('/^Block\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/parameters\.twig$/', $relativePath, $matches))
		{
			[, $vendor, $shortModuleName, $shortBlockName] = $matches;
			$event->setParam('vendor', $vendor);
			$event->setParam('shortModuleName', $shortModuleName);
			$event->setParam('shortBlockName', $shortBlockName);
			$action = static function ($event) {
				(new \Rbs\Admin\Http\Actions\GetHtmlBlockParameters())->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if (\preg_match('/^Document\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)\.twig$/', $relativePath, $matches))
		{
			[, $vendor, $shortModuleName, $shortDocumentName, $baseFileName] = $matches;
			$event->setParam('resourcePath', \implode('/', [$vendor, $shortModuleName, 'Documents', $shortDocumentName, $baseFileName . '.twig']));
			$event->setParam('vendor', $vendor);
			$event->setParam('shortModuleName', $shortModuleName);

			$modelManager = $event->getApplicationServices()->getModelManager();
			$model = $modelManager->getModelByName($vendor . '_' . $shortModuleName . '_' . $shortDocumentName);
			if ($model)
			{
				$event->setParam('model', $model);
				$event->setParam('view', $baseFileName);
			}

			$action = static function ($event) {
				(new \Rbs\Admin\Http\Actions\GetHtmlFragment())->execute($event);
			};
			$event->setAction($action);
			return;
		}
		if (\preg_match('/^([A-Z][A-Za-z0-9]+)\/([A-Z][A-Za-z0-9]+)\/(.+)\.([a-z]+)$/', $relativePath, $matches))
		{
			$event->setParam('resourcePath', $relativePath);
			[, $vendor, $shortModuleName, $subPath, $extension] = $matches;
			$event->setParam('vendor', $vendor);
			$event->setParam('shortModuleName', $shortModuleName);
			$event->setParam('modulePath', $subPath);
			$event->setParam('extension', $extension);

			if ($extension === 'twig')
			{
				$action = static function ($event) {
					(new \Rbs\Admin\Http\Actions\GetHtmlFragment())->execute($event);
				};
				$event->setAction($action);
				return;
			}
			$action = static function ($event) {
				(new \Rbs\Admin\Http\Actions\GetResource())->execute($event);
			};
			$event->setAction($action);
			return;
		}
	}

	/**
	 * @param string $path
	 * @return string
	 */
	protected function getRelativePath($path)
	{
		if ($path && \strpos($path, '/') === 0)
		{
			$path = \substr($path, 1);
		}
		return $path;
	}
}