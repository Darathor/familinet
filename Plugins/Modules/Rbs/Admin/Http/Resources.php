<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http;

/**
 * @name \Rbs\Admin\Http\Resources
 */
class Resources extends \Change\Http\AbstractResources
{
	/**
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Application $application)
	{
		$this->setApplication($application);
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function registerPlugins(\Assetic\AssetManager $assetManager, \Change\Plugins\PluginManager $pluginManager)
	{
		$jsAssets = new \Assetic\Asset\AssetCollection();
		$cssAssets = new \Assetic\Asset\AssetCollection();
		foreach ($pluginManager->getModules() as $modulePlugin)
		{
			$this->doRegisterPlugin($assetManager, $jsAssets, $cssAssets, $modulePlugin);
		}
		$this->doRegisterJsAssets($assetManager, $jsAssets, 'Modules');
		$this->doRegisterCssAssets($assetManager, $cssAssets, 'Modules');
		return $this;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Assetic\Asset\AssetCollection $modulesJsAssets
	 * @param \Assetic\Asset\AssetCollection $modulesCssAssets
	 * @param \Change\Plugins\Plugin $plugin
	 * @return $this
	 */
	protected function doRegisterPlugin(\Assetic\AssetManager $assetManager, \Assetic\Asset\AssetCollection $modulesJsAssets,
		\Assetic\Asset\AssetCollection $modulesCssAssets, \Change\Plugins\Plugin $plugin)
	{
		$name = $plugin->getName();
		switch ($name)
		{
			case 'Rbs_Admin':
				$this->registerAdminAssets($assetManager, $plugin);
				break;
			case 'Rbs_Ua':
				$this->registerUaAssets($assetManager, $plugin);
				$this->registerStandardPluginAssets($modulesJsAssets, $modulesCssAssets, $plugin);
				break;
			default:
				$this->registerStandardPluginAssets($modulesJsAssets, $modulesCssAssets, $plugin);
				break;
		}

		return $this;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Assetic\Asset\AssetCollection $jsAssets
	 * @param string $name
	 */
	protected function doRegisterJsAssets($assetManager, $jsAssets, $name)
	{
		if ($jsAssets->all())
		{
			if (!$this->devMode)
			{
				$jsAssets->ensureFilter(new \Change\Presentation\Themes\JSMinFilter());
			}
			$jsAssets->setTargetPath('Rbs/Admin/js/' . $name . '.js');
			$assetManager->set('JS_' . $name, $jsAssets);
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Assetic\Asset\AssetCollection $cssAssets
	 * @param string $name
	 */
	protected function doRegisterCssAssets($assetManager, $cssAssets, $name)
	{
		if ($cssAssets->all())
		{
			$cssAssets->setTargetPath('Rbs/Admin/css/' . $name . '.css');
			$assetManager->set('CSS_' . $name, $cssAssets);
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerAdminAssets(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$pluginAssetsPath = $plugin->getAssetsPath();

		// JS assets.
		$jsAssets = new \Assetic\Asset\AssetCollection();
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginAssetsPath . '/js/rbschange.js'));
		$jsAssets->add(new \Assetic\Asset\GlobAsset($pluginAssetsPath . '/js/*/*.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginAssetsPath . '/clipboard/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginAssetsPath . '/tasks/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginAssetsPath . '/notifications/controllers.js'));
		$jsAssets->add(new \Assetic\Asset\FileAsset($pluginAssetsPath . '/js/routes.js'));
		$this->doRegisterJsAssets($assetManager, $jsAssets, 'Rbs_Admin');

		// CSS assets.
		$cssAssets = new \Assetic\Asset\AssetCollection();
		$this->addCSSAsset($cssAssets, $pluginAssetsPath . '/admin.');
		$cssAssets->add(new \Assetic\Asset\GlobAsset($pluginAssetsPath . '/css/*.css'));
		$this->doRegisterCssAssets($assetManager, $cssAssets, 'Rbs_Admin');
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerUaAssets(\Assetic\AssetManager $assetManager, \Change\Plugins\Plugin $plugin)
	{
		$pluginAssetsPath = $plugin->getAssetsPath();

		// JS assets.
		$jsAssets = new \Assetic\Asset\AssetCollection();
		$genericAssetsPath = $pluginAssetsPath . '/ProximisModal';
		if (\is_dir($genericAssetsPath))
		{
			$jsAssets->add(new \Assetic\Asset\GlobAsset([$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js']));
		}

		$genericAssetsPath = $pluginAssetsPath . '/ProximisPolyfill';
		if (\is_dir($genericAssetsPath))
		{
			$jsAssets->add(new \Assetic\Asset\GlobAsset([$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js']));
		}

		$genericAssetsPath = $pluginAssetsPath . '/ProximisIntl';
		if (\is_dir($genericAssetsPath))
		{
			$jsAssets->add(new \Assetic\Asset\GlobAsset([$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js']));
		}

		$genericAssetsPath = $pluginAssetsPath . '/ProximisCore';
		if (\is_dir($genericAssetsPath))
		{
			$jsAssets->add(new \Assetic\Asset\GlobAsset([$genericAssetsPath . '/*.js', $genericAssetsPath . '/*/*.js']));
		}
		$this->doRegisterJsAssets($assetManager, $jsAssets, 'Rbs_Ua');

		// CSS assets.
		$cssAssets = new \Assetic\Asset\AssetCollection();
		$moduleAssetsPath = $pluginAssetsPath . '/Proximis';
		$this->addCSSAsset($cssAssets, $moduleAssetsPath . '/proximis.');
		$cssAssets->add(new \Assetic\Asset\GlobAsset($moduleAssetsPath . '/*/*.css'));
		$this->doRegisterCssAssets($assetManager, $cssAssets, 'Rbs_Ua');
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $jsAssets
	 * @param \Assetic\Asset\AssetCollection $cssAssets
	 * @param \Change\Plugins\Plugin $plugin
	 */
	protected function registerStandardPluginAssets(\Assetic\Asset\AssetCollection $jsAssets, \Assetic\Asset\AssetCollection $cssAssets,
		\Change\Plugins\Plugin $plugin)
	{
		$adminAssetsPath = $plugin->getAssetsPath() . '/Admin';
		if (\is_dir($adminAssetsPath))
		{
			$jsAsset = new \Assetic\Asset\GlobAsset([$adminAssetsPath . '/*.js', $adminAssetsPath . '/Documents/*/*.js']);
			if ($jsAsset->all())
			{
				$jsAssets->add($jsAsset);
			}

			$cssAsset = new \Assetic\Asset\GlobAsset($adminAssetsPath . '/*.css');
			if ($cssAsset->all())
			{
				$cssAssets->add($cssAsset);
			}
		}
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @return string[]
	 */
	public function getScriptsUrl(\Assetic\AssetManager $assetManager)
	{
		$scripts = [];
		$names = $assetManager->getNames();
		if (\count(\array_intersect(['JS_Rbs_Admin'], $names)) !== 1)
		{
			return $scripts;
		}
		$scripts[] = $this->assetsBasePath . $assetManager->get('JS_Rbs_Ua')->getTargetPath();
		$scripts[] = $this->assetsBasePath . $assetManager->get('JS_Rbs_Admin')->getTargetPath();
		foreach (\array_diff($names, ['JS_Rbs_Admin', 'JS_Rbs_Ua']) as $name)
		{
			if (\strpos($name, 'JS_') === 0)
			{
				$scripts[] = $this->assetsBasePath . $assetManager->get($name)->getTargetPath();
			}
		}
		return $scripts;
	}

	/**
	 * @param \Assetic\AssetManager $assetManager
	 * @return string[]
	 */
	public function getStylesUrl(\Assetic\AssetManager $assetManager)
	{
		$styles = [];
		$names = $assetManager->getNames();
		if (\count(\array_intersect(['CSS_Rbs_Admin', 'CSS_Rbs_Ua'], $names)) !== 2)
		{
			return $styles;
		}

		$styles[] = $this->assetsBasePath . $assetManager->get('CSS_Rbs_Ua')->getTargetPath();
		$styles[] = $this->assetsBasePath . $assetManager->get('CSS_Rbs_Admin')->getTargetPath();

		foreach (\array_diff($names, ['CSS_Rbs_Admin', 'CSS_Rbs_Ua']) as $name)
		{
			if (\strpos($name, 'CSS_') === 0)
			{
				$styles[] = $this->assetsBasePath . $assetManager->get($name)->getTargetPath();
			}
		}
		return $styles;
	}
}