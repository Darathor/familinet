<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Rest\Actions;

/**
 * @name \Rbs\Admin\Http\Rest\Actions\GetCurrentUser
 */
class GetCurrentUser
{
	/**
	 * TODO WWW-Authenticate: OAuth realm="Rbs_Admin"
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$user = $event->getAuthenticationManager()->getCurrentUser();
		$properties = [
			'id' => $user->getId(),
			'pseudonym' => $user->getName()
		];

		$result = new \Change\Http\Rest\V1\Resources\DocumentResult($event->getUrlManager(),
			$event->getApplicationServices()->getDocumentManager()->getDocumentInstance($user->getId()));
		$result->setProperties($properties);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);

		$profileManager = $event->getApplicationServices()->getProfileManager();
		$props = [];
		$profile = $profileManager->loadProfile($user, 'Change_User');
		if ($profile)
		{
			foreach ($profile->getPropertyNames() as $name)
			{
				$v = $profile->getPropertyValue($name);
				if ($v === null)
				{
					if ($name === 'LCID')
					{
						$v = $event->getApplicationServices()->getI18nManager()->getLCID();
					}
					elseif ($name === 'TimeZone')
					{
						$tz = $event->getApplicationServices()->getI18nManager()->getTimeZone();
						$v = $tz->getName();
					}
				}
				$props[$name] = $v;
			}
		}

		$profile = $profileManager->loadProfile($user, 'Rbs_Admin');
		if ($profile)
		{
			foreach ($profile->getPropertyNames() as $name)
			{
				$v = $profile->getPropertyValue($name);
				if ($v instanceof \DateTime)
				{
					$v = $v->format(\DateTime::ATOM);
				}
				$props[$name] = $v;
			}
		}
		$result->setProperty('profile', $props);

		$privileges = $event->getApplicationServices()->getPermissionsManager()->getUserPrivileges();
		$result->setProperty('rules', $privileges);

		$event->setResult($result);
	}
}