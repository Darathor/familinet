<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Http\Rest;

/**
 * @name \Rbs\Admin\Http\Rest\AdminResolver
 */
class AdminResolver implements \Change\Http\Rest\V1\NameSpaceDiscoverInterface
{
	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	protected $resolver;

	/**
	 * @param \Change\Http\Rest\V1\Resolver $resolver
	 */
	public function __construct(\Change\Http\Rest\V1\Resolver $resolver)
	{
		$this->resolver = $resolver;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string[] $namespaceParts
	 * @return string[]
	 */
	public function getNextNamespace($event, $namespaceParts)
	{
		return ['admin.currentUser', 'admin.currentTasks', 'admin.tagsInfo', 'admin.documentPreview', 'admin.documentList', 'admin.searchDocuments',
			'admin.changeVersion'];
	}

	/**
	 * Set Event params: resourcesActionName, documentId, LCID
	 * @param \Change\Http\Event $event
	 * @param array $resourceParts
	 * @param $method
	 * @return void
	 */
	public function resolve($event, $resourceParts, $method)
	{
		$nbParts = count($resourceParts);
		if ($nbParts == 0 && $method === \Change\Http\Rest\Request::METHOD_GET)
		{
			array_unshift($resourceParts, 'admin');
			$event->setParam('namespace', implode('.', $resourceParts));
			$event->setParam('resolver', $this);
			$action = function ($event)
			{
				$action = new \Change\Http\Rest\V1\DiscoverNameSpace();
				$action->execute($event);
			};
			$event->setAction($action);
			$event->setAuthorization(null);
			return;
		}
		if ($nbParts == 1)
		{
			$actionName = $resourceParts[0];
			if ($actionName === 'currentUser')
			{
				if ($method === \Change\Http\Rest\Request::METHOD_GET)
				{
					$action = new \Rbs\Admin\Http\Rest\Actions\GetCurrentUser();
					$event->setAction(function ($event) use ($action)
					{
						$action->execute($event);
					});
				}
				elseif ($method === \Change\Http\Rest\Request::METHOD_PUT)
				{
					$action = new \Rbs\Admin\Http\Rest\Actions\UpdateCurrentUser();
					$event->setAction(function ($event) use ($action)
					{
						$action->execute($event);
					});
				}
			}
			elseif ($actionName === 'currentTasks')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\CurrentTasks())->execute($event);
				});
			}
			elseif ($actionName === 'tagsInfo')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\TagsInfo())->execute($event);
				});
			}
			elseif ($actionName === 'documentPreview')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\DocumentPreview())->execute($event);
				});
			}
			elseif ($actionName === 'documentList')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\DocumentList())->execute($event);
				});
			}
			elseif ($actionName === 'searchDocuments')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\SearchDocuments())->execute($event);
				});
			}
			elseif ($actionName === 'changeVersion')
			{
				$event->setAction(function ($event)
				{
					(new \Rbs\Admin\Http\Rest\Actions\ChangeVersion())->execute($event);
				});
				$event->setAuthorization(null);
			}
		}
	}
}