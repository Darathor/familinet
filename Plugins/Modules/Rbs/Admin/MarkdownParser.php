<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin;

/**
 * @name \Rbs\Admin\MarkdownParser
 */
class MarkdownParser extends \Change\Presentation\RichText\MarkdownParser implements \Change\Presentation\RichText\ParserInterface
{
	/**
	 * @param string $rawText
	 * @param array $context
	 * @return string
	 */
	public function parse($rawText, $context)
	{
		//replace @xxx and @+xxx by a linked user or user group if exist.
		$rawText = preg_replace_callback('/\B(@\+?)([a-z0-9_\-]+)/i', function ($matches)
		{
			//                                1111  222222222222
			if ($matches[1] === '@+')
			{
				$dqb = $this->applicationServices->getDocumentManager()->getNewQuery('Rbs_User_Group');
				$dqb->andPredicates($dqb->eq('identifier', $matches[2]));
				$result = $dqb->getFirstDocument();
			}
			else
			{
				$dqb = $this->applicationServices->getDocumentManager()->getNewQuery('Rbs_User_User');
				$dqb->andPredicates($dqb->eq('login', $matches[2]));
				$dqb->andPredicates($dqb->activated());
				$result = $dqb->getFirstDocument();
			}

			if ($result)
			{
				return '[' . $matches[1] . $matches[2] . '](' . $result->getId() . ',public-profile "' . $matches[1] . $matches[2]
				. '")';
			}
			return $matches[1] . $matches[2];
		}, $rawText);

		//replace #xxx by a linked resource if exist.
		$rawText = preg_replace_callback('/\B(#)(\d+)\b/', function ($matches)
		{
			//                                1  222
			$document = $this->applicationServices->getDocumentManager()->getDocumentInstance($matches[2]);
			if ($document)
			{
				return '[' . $matches[1] . $matches[2] . '](' . $document->getId() . ' "' . $matches[1] . $matches[2] . '")';
			}
			return $matches[1] . $matches[2];
		}, $rawText);
		return $this->transform($rawText);
	}

	/**
	 * @param $matches
	 * @return string
	 */
	protected function _doAnchors_inline_callback($matches)
	{
		$link_text = $this->runSpanGamut($matches[2]);
		$url = $matches[3] == '' ? $matches[4] : $matches[3];
		$params = [];
		if (!preg_match('/^(\d+)(,([a-z]{2}_[A-Z]{2}))?(,([a-z0-9\-_]+))?$/i', $url, $params))
			//              111    33333333333333333      555555555555
		{
			return parent::_doAnchors_inline_callback($matches);
		}
		$id = $params[1];
		$lcid = $params[3];
		$route = $params[5];

		/* @var $document \Change\Documents\AbstractDocument */
		$document = $this->applicationServices->getDocumentManager()->getDocumentInstance($id);

		if (!$document)
		{
			return $this->hashPart('<span class="label label-important">Invalid Document: ' . $url . '</span>');
		}

		$result =
			'<a href rbs-document-popover rbs-document-href="' . $document->getDocumentModelName() . ',' . $document->getId();
		if ($lcid)
		{
			$result .= ',' . $lcid;
		}
		if ($route)
		{
			$result .= ',' . $route;
		}
		$result .= '"';

		$link_text = $this->runSpanGamut($link_text);
		if (!$link_text && $document instanceof \Change\Documents\Interfaces\Editable)
		{
			/* @var $document \Change\Documents\Interfaces\Editable */
			$link_text = $document->getLabel();
		}
		$result .= '>' . $link_text . '</a>';

		return $this->hashPart($result);
	}

	/**
	 * This method is overridden to add the "table" class on the table (cf Bootstrap markup).
	 * @param $matches
	 * @return string
	 */
	protected function _doTable_callback($matches)
	{
		$head = $matches[1];
		$underline = $matches[2];
		$content = $matches[3];

		# Remove any tailing pipes for each line.
		$head = preg_replace('/[|] *$/m', '', $head);
		$underline = preg_replace('/[|] *$/m', '', $underline);
		$content = preg_replace('/[|] *$/m', '', $content);

		# Reading alignment from header underline.
		$separators = preg_split('/ *[|] */', $underline);
		$attr = [];
		foreach ($separators as $n => $s)
		{
			if (preg_match('/^ *-+: *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('right');
			}
			else if (preg_match('/^ *:-+: *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('center');
			}
			else if (preg_match('/^ *:-+ *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('left');
			}
			else
			{
				$attr[$n] = '';
			}
		}

		# Parsing span elements, including code spans, character escapes,
		# and inline HTML tags, so that pipes inside those gets ignored.
		$head = $this->parseSpan($head);
		$headers = preg_split('/ *[|] */', $head);
		$col_count = count($headers);
		$attr = array_pad($attr, $col_count, '');

		# Write column headers.
		$text = "<table class=\"table\">\n";
		$text .= "<thead>\n";
		$text .= "<tr>\n";
		foreach ($headers as $n => $header)
		{
			$text .= "  <th$attr[$n]>" . $this->runSpanGamut(trim($header)) . "</th>\n";
		}
		$text .= "</tr>\n";
		$text .= "</thead>\n";

		# Split content by row.
		$rows = explode("\n", trim($content, "\n"));

		$text .= "<tbody>\n";
		foreach ($rows as $row)
		{
			# Parsing span elements, including code spans, character escapes,
			# and inline HTML tags, so that pipes inside those gets ignored.
			$row = $this->parseSpan($row);

			# Split row by cell.
			$row_cells = preg_split('/ *[|] */', $row, $col_count);
			$row_cells = array_pad($row_cells, $col_count, '');

			$text .= "<tr>\n";
			foreach ($row_cells as $n => $cell)
			{
				$text .= "  <td$attr[$n]>" . $this->runSpanGamut(trim($cell)) . "</td>\n";
			}
			$text .= "</tr>\n";
		}
		$text .= "</tbody>\n";
		$text .= '</table>';

		return $this->hashBlock($text) . "\n";
	}
}