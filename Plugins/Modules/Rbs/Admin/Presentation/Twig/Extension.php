<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Presentation\Twig;

/**
 * @name \Rbs\Admin\Presentation\Twig\Extension
 */
class Extension extends \Rbs\Ua\Presentation\Twig\Extension
{
	/**
	 * @var \Rbs\Admin\AdminManager
	 */
	protected $adminManager;

	/**
	 * @param \Rbs\Admin\AdminManager $adminManager
	 * @param \Change\Application $application
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param \Change\Documents\ModelManager $modelManager
	 */
	public function __construct(\Rbs\Admin\AdminManager $adminManager, \Change\Application $application, \Change\I18n\I18nManager $i18nManager,
		\Change\Documents\ModelManager $modelManager)
	{
		parent::__construct($application, $i18nManager, $modelManager);
		$this->adminManager = $adminManager;
	}


	/**
	 * Returns a list of functions to add to the existing list.
	 * @return array An array of functions
	 */
	public function getFunctions()
	{
		$functions = parent::getFunctions();
		$functions[] = new \Twig\TwigFunction('namedURL', [$this, 'namedURL']);
		return $functions;
	}

	/**
	 * @param string $model
	 * @param string $name
	 * @return string|null
	 */
	public function namedURL($model, $name)
	{
		$route = $this->adminManager->getRoutesHelper()->getNamedRoute($model, $name);
		if (\is_array($route))
		{
			if (\strpos($route['path'], '/:'))
			{
				return null;
			}
			$path = $route['rule']['redirectTo'] ?? $route['path'];
			return \substr($path, 1);
		}
		return null;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Rbs_Admin';
	}
}