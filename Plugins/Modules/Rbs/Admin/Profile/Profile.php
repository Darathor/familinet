<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Admin\Profile;

/**
 * @name \Rbs\Admin\Profile\Profile
 */
class Profile extends \Change\User\AbstractProfile
{
	public function __construct()
	{
		$this->properties = [
			'avatar' => null,
			'pagingSize' => 10,
			'documentListViewMode' => 'list',
			'sendNotificationMailImmediately' => false,
			'notificationMailInterval' => '',
			'notificationMailAt' => '',
			'dateOfLastNotificationMailSent' => null
		];
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Rbs_Admin';
	}

	/**
	 * @return string[]
	 */
	public function getPropertyNames()
	{
		return ['avatar', 'pagingSize', 'documentListViewMode', 'dashboard', 'sendNotificationMailImmediately', 'notificationMailInterval',
			'notificationMailAt', 'dateOfLastNotificationMailSent'];
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 */
	public function setPropertyValue($name, $value)
	{
		if ($name === 'dateOfLastNotificationMailSent')
		{
			if ($value && is_string($value))
			{
				$value = new \DateTime($value);
			}
			elseif (!($value instanceof \DateTime))
			{
				$value = null;
			}
		}
		parent::setPropertyValue($name, $value);
		return $this;
	}
}