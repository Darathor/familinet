(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsCollectionObjectCollectionNew', ['$compile', 'RbsChange.Utils', '$timeout', Editor]);
	app.directive('rbsDocumentEditorRbsCollectionObjectCollectionEdit', ['$compile', 'RbsChange.Utils', '$timeout', Editor]);

	function Editor($compile, Utils, $timeout) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, element) {

				scope.onReady = function() {
					if (!angular.isArray(scope.document.items)) {
						scope.document.items = [];
					}
				};

				function redrawConfigurationOptions($compile, scope, itemsDefinition) {
					var container = element.find('.collection-items');
					
					container.children().remove();

					if (itemsDefinition && scope.itemsManager && scope.itemsManager.LCID) {
						container.html('<div ' + Utils.normalizeAttrName(itemsDefinition) + '=""></div>');
						$compile(container.children())(scope);
					}
				}

				scope.$watch('document.itemsDefinition', function(itemsDefinition) {
					redrawConfigurationOptions($compile, scope, itemsDefinition);
				});

				scope.$watch('itemsManager.LCID', function(LCID) {
					redrawConfigurationOptions($compile, scope, null);
					if (LCID) {
						$timeout(function() {
							redrawConfigurationOptions($compile, scope, scope.document.itemsDefinition);
						});
					}
				});
			}
		};
	}

	app.directive('rbsCollectionObjectCollectionDefinitionColor', function () {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Collection/Documents/ObjectCollection/definition-color.twig'
		};
	});

	app.directive('rbsCollectionObjectCollectionDefinitionColorVisual', function () {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Collection/Documents/ObjectCollection/definition-color-visual.twig'
		};
	});
})();