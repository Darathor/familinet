<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Collection;

/**
 * @name \Rbs\Collection\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addObjectCollectionDefinitions(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'rbsCollectionObjectCollectionDefinitionColor' =>
					new \Change\I18n\I18nString($i18n, 'm.rbs.collection.admin.definition_color', ['ucf']),
				'rbsCollectionObjectCollectionDefinitionColorVisual' =>
					new \Change\I18n\I18nString($i18n, 'm.rbs.collection.admin.definition_color_visual', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Collection_ObjectCollectionDefinitions', $collection);
			$event->setParam('collection', $collection);
		}
	}
}