<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Documents;

/**
 * @name \Rbs\Collection\Documents\ObjectCollection
 * @method \Change\Documents\InlineArrayProperty|\Rbs\Collection\Documents\ObjectCollectionItem[] getItems()
 */
class ObjectCollection extends \Compilation\Rbs\Collection\Documents\ObjectCollection
	implements \Change\Collection\CollectionInterface
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		$document = $event->getDocument();
		if (!$document instanceof \Rbs\Collection\Documents\ObjectCollection)
		{
			return;
		}

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$collectionManager = $event->getApplicationServices()->getCollectionManager();
			$collection = $collectionManager->getCollection('Rbs_Collection_ObjectCollectionDefinitions');
			$restResult->setProperty('itemsDefinition', $document->getItemsDefinition());
			if ($collection)
			{
				$item = $collection->getItemByValue($document->getItemsDefinition());
				if ($item)
				{
					$restResult->setProperty('itemsDefinitionLabel', $item->getLabel());
				}
			}
		}
	}

	/**
	 * @return \Rbs\Collection\Documents\ObjectCollectionItem
	 */
	public function newCollectionItem()
	{
		return $this->getDocumentManager()->getNewInlineInstanceByModelName('Rbs_Collection_ObjectCollectionItem', true);
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		if (\Change\Stdlib\StringUtils::isEmpty($this->getCode()))
		{
			$this->setCode(uniqid('OBJECT-COLLECTION-', false));
		}
	}
}
