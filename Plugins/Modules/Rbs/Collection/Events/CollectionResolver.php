<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Events;

/**
 * @name \Rbs\Collection\Events\CollectionResolver
 */
class CollectionResolver
{
	const CACHE_KEY = 'CollectionCodes';

	/**
	 * @param \Change\Cache\CacheManager $cacheManager
	 */
	public function invalidateCodesCache(\Change\Cache\CacheManager $cacheManager)
	{
		$namespace = $cacheManager->buildMemoryNamespace('Collection');
		$cacheManager->removeEntry($namespace, self::CACHE_KEY);
		$cacheManager->removeEntry('prefetch', self::CACHE_KEY);
	}

	public function getCodesFromCache(\Change\Cache\CacheManager $cacheManager)
	{
		$namespace = $cacheManager->buildMemoryNamespace('Collection');
		$codes = $cacheManager->getEntry($namespace, self::CACHE_KEY);
		return ($codes === null) ? $cacheManager->getEntry('prefetch', self::CACHE_KEY) : $codes;
	}

	public function setCodesInCache(\Change\Cache\CacheManager $cacheManager, $codes)
	{
		$namespace = $cacheManager->buildMemoryNamespace('Collection');
		$cacheManager->setEntry($namespace, self::CACHE_KEY, $codes, []);
		$cacheManager->setEntry('prefetch', self::CACHE_KEY, $codes, ['ttl' => 3600 * 12]);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function getCollection(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$code = $event->getParam('code');
		$cacheManager = $applicationServices->getCacheManager();
		$codes = $this->getCodesFromCache($cacheManager);
		if ($codes === null)
		{
			$qb = $applicationServices->getDbProvider()->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->alias($fb->getDocumentColumn('id'), 'i'), $fb->alias($fb->getDocumentColumn('code'), 'c'));
			$qb->from($fb->getDocumentTable('Rbs_Collection_Collection'));
			$select = $qb->query();
			$codes = $select->getResults($select->getRowsConverter()->addIntCol('i')->addStrCol('c')->indexBy('c')->singleColumn('i'));
			$this->setCodesInCache($cacheManager, $codes ?: []);
		}

		if (isset($codes[$code]))
		{
			$collection = $applicationServices->getDocumentManager()->getDocumentInstance($codes[$code]);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function getCollectionList(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
			$qb = $query->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->addColumn($fb->alias($fb->getDocumentColumn('code'), 'code'));
			$qb->addColumn($fb->alias($fb->getDocumentColumn('label'), 'label'));
			$sq = $qb->query();
			$collectionItems = $sq->getResults($sq->getRowsConverter()->addStrCol('code', 'label')->singleColumn('label')
				->indexBy('code'));
			$collection = new \Change\Collection\CollectionArray($event->getParam('code'), $collectionItems);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function getCodes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$codes = $event->getParam('codes');
			if (!is_array($codes))
			{
				$codes = [];
			}

			$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
			$qb = $query->dbQueryBuilder();
			$qb->addColumn($qb->getFragmentBuilder()->alias($query->getColumn('code'), 'code'));
			foreach ($qb->query()->getResults() as $row)
			{
				$codes[] = $row['code'];
			};
			$codes = array_unique($codes);
			$event->setParam('codes', $codes);
		}
	}
}