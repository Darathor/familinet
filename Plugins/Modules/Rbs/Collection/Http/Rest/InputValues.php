<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Http\Rest;

use Change\Http\Rest\V1\ArrayResult;

/**
 * @name \Rbs\Collection\Http\Rest\InputValues
 */
class InputValues
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$inputValues = [];
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($event->getParam('documentId'));
		if ($document instanceof \Rbs\Collection\Documents\Collection)
		{
			$inputValues = $document->getInputValues();
		}
		$result = new ArrayResult();
		$result->setArray($inputValues);
		$event->setResult($result);
	}
}