<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Presentation;

/**
 * @name \Rbs\Collection\Presentation\CollectionDataComposer
 */
class CollectionDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Rbs\Collection\Documents\Collection
	 */
	protected $collection;

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function __construct(\Change\Documents\Events\Event $event)
	{
		$this->collection = $event->getDocument();

		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	protected function generateDataSets()
	{
		$collection = $this->collection;

		$this->dataSets = [
			'common' => [
				'id' => $collection->getId(),
				'code' => $collection->getCode()
			],
			'items' => []
		];

		if ($collection instanceof \Rbs\Collection\Documents\ObjectCollection)
		{
			$this->dataSets['common']['itemsDefinition'] = $collection->getItemsDefinition();
		}

		$restrictToItems = null;
		if (isset($this->data['restrictToItems']) && is_array($this->data['restrictToItems']))
		{
			$restrictToItems = $this->data['restrictToItems'];
		}

		$itemContext = $this->getItemContext();
		foreach ($collection->getItems() as $item)
		{
			if (!$restrictToItems || in_array($item->getValue(), $restrictToItems))
			{
				$data = $item->getAJAXData($itemContext);
				if ($data)
				{
					$this->dataSets['items'][] = $data;
				}
			}
		}

		$this->generateTypologyDataSet($this->collection);
	}

	/**
	 * @return array
	 */
	protected function getItemContext()
	{
		$data = $this->data;
		if ($this->collection instanceof \Rbs\Collection\Documents\ObjectCollection)
		{
			$data['itemsDefinition'] = $this->collection->getItemsDefinition();
		}
		return ['visualFormats' => $this->visualFormats, 'URLFormats' => $this->URLFormats, 'dataSetNames' => $this->dataSetNames,
			'website' => $this->website, 'websiteUrlManager' => $this->websiteUrlManager, 'section' => $this->section,
			'data' => $data, 'detailed' => $this->detailed];
	}
}