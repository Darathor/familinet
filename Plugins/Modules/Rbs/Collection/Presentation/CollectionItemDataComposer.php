<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Collection\Presentation;

/**
 * @name \Rbs\Collection\Presentation\CollectionItemDataComposer
 */
class CollectionItemDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Rbs\Collection\Documents\CollectionItem
	 */
	protected $item;

	/**
	 * @param \Change\Events\Event $event
	 */
	public function __construct(\Change\Events\Event $event)
	{
		$this->item = $event->getParam('document');

		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	protected function generateDataSets()
	{
		$item = $this->item;

		$this->dataSets = [
			'common' => [
				'value' => $item->getValue(),
				'title' => $item->getTitle()
			]
		];

		if ($item instanceof \Rbs\Collection\Documents\ObjectCollectionItem)
		{
			$this->dataSets['data'] = $item->getData();
			$contextData = $this->data;
			if (($contextData['itemsDefinition'] ?? null) == 'rbsCollectionObjectCollectionDefinitionColorVisual')
			{
				$data = &$this->dataSets;
				$visualFormats = $this->visualFormats;
				if ($visualFormats && ($data['data']['visual'] ?? null))
				{
					$visual = $this->documentManager->getDocumentInstance($data['data']['visual']);
					if ($visual instanceof \Rbs\Media\Documents\Image && $visual->activated())
					{
						$imagesFormats = new \Rbs\Media\Http\Ajax\V1\ImageFormats($visual);
						$formats = $imagesFormats->getFormatsData($visualFormats);
						if (count($formats))
						{
							$data['data']['visual'] = $formats;
						}
					}
				}
			}
		}
	}
}