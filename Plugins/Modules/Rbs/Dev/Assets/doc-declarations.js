/**
 * This file is just for PhpStorm to not show warnings when these types are used in JS comments...
 */

// Standard JS API.

/**
 * @ngdoc object
 * @name DOMDocument
 */
/**
 * @ngdoc object
 * @name DOMElement
 */
/**
 * @ngdoc object
 * @name DOMNode
 */

// Used in Angular.

/**
 * @ngdoc object
 * @name Promise
 */
/**
 * @ngdoc object
 * @name HttpPromise
 */