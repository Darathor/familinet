<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
require_once(#projectPath# . '/Change/Application.php');

$application = new \Change\Application();
$application->useSession(false);
$application->start();

$controller = new \Rbs\Ua\Http\API\Controller($application);
$controller->setActionResolver(new \Rbs\Ua\Http\API\Resolver());
$request = new \Change\Http\Request();

$allow = $application->inDevelopmentMode();
$anonymous = function (\Change\Http\Event $event) use ($allow)
{
	$event->getPermissionsManager()->allow($allow);
	$userId = $event->getRequest()->getQuery('userId');
	if ($allow && $userId)
	{
		$user = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($userId);
		if ($user instanceof \Rbs\User\Documents\User)
		{
			$event->getApplicationServices()->getAuthenticationManager()->setCurrentUser(new \Rbs\User\Events\AuthenticatedUser($user));
			$event->getPermissionsManager()->allow(false);
		}
	}
};

$controller->getEventManager()->attach(\Change\Http\Event::EVENT_REQUEST, $anonymous, 1);
$response = $controller->handle($request);
$response->send();