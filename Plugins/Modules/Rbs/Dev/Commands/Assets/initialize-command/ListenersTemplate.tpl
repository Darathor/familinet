<?php
/**
 * Copyright (C) #copyrightYear# Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace #namespace#;

/**
 * @name \#namespace#\Listeners
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('config', function (\Change\Commands\Events\Event $event)
		{
			$commandConfigPath = __DIR__ . '/Assets/config.json';
			if (is_file($commandConfigPath))
			{
				return \Zend\Json\Json::decode(file_get_contents($commandConfigPath), \Zend\Json\Json::TYPE_ARRAY);
			}
			return null;
		});

		$this->listeners[] = $events->attach('#package#:#cmdName#', function ($event)
		{
			$cmd = new \#namespace#\#className#();
			$cmd->execute($event);
		};);
	}
}