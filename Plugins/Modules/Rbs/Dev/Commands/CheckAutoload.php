<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Dev\Commands;

/**
 * @name \Rbs\Dev\Commands\CheckAutoload
 */
class CheckAutoload
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$verbose = $event->getParam('verbose');

		$response = $event->getCommandResponse();
		$workspace = $event->getApplication()->getWorkspace();
		$path = $workspace->projectPath('composer.json');
		if ($verbose)
		{
			$response->addCommentMessage('Check ' . $path . ' files for autoload...');
		}
		if (!\is_readable($path))
		{
			$response->addErrorMessage($path . ' file is not readable');
			return;
		}
		$composer = \json_decode(file_get_contents($path), true);
		if (!isset($composer['require']) || !\is_array($composer['require']))
		{
			$response->addErrorMessage($path . ' file is not valid');
			return;
		}

		$repositories = $composer['repositories'] ?? [];
		$addZendFrameworkPackages = true;
		foreach ($repositories as $repository)
		{
			$url = $repository['url'] ?? null;
			if ($url === 'https://packages.zendframework.com/')
			{
				$addZendFrameworkPackages = false;
			}
		}

		if ($addZendFrameworkPackages)
		{
			$response->addInfoMessage('Add composer repository : https://packages.zendframework.com/');
			$composer['repositories'][] = ['type' => 'composer', 'url' => 'https://packages.zendframework.com/'];
		}

		$updateAutoload = false;
		if (!($composer['autoload']['psr-4']['Compilation\\'] ?? false))
		{
			$updateAutoload = true;
			$composer['autoload']['psr-4']['Compilation\\'] = 'Compilation';
			$response->addInfoMessage('Add autoload/psr-4/Compilation\\: Compilation');
		}

		$ignoreProjectPlugin = $composer['extra']['ignore-project-plugin'] ?? false;
		if ($verbose)
		{
			$response->addCommentMessage('Ignore project plugins autoload: ' . ($ignoreProjectPlugin ? 'true' : 'false'));
		}

		$pluginManager = $event->getApplicationServices()->getPluginManager();
		foreach ($pluginManager->getPlugins() as $plugin)
		{
			$isModule = $plugin->isModule();
			$isTheme = $plugin->isTheme();

			if ($plugin->isProjectPath())
			{
				if ($ignoreProjectPlugin)
				{
					continue;
				}
				if ($isModule)
				{
					$namespace = 'Project\\' . $plugin->getShortName() . '\\';
					if (!($composer['autoload']['psr-4'][$namespace] ?? false))
					{
						$updateAutoload = true;
						$composer['autoload']['psr-4'][$namespace] = 'App/Modules/Project/' . $plugin->getShortName();
						$response->addInfoMessage('Add autoload/psr-4/' . $namespace . ': App/Modules/Project/' . $plugin->getShortName());
					}
				}
				elseif ($isTheme)
				{
					$namespace = 'Themes\\Project\\' . $plugin->getShortName() . '\\';
					if (!($composer['autoload']['psr-4'][$namespace] ?? false))
					{
						$updateAutoload = true;
						$composer['autoload']['psr-4'][$namespace] = 'App/Themes/Project/' . $plugin->getShortName();
						$response->addInfoMessage('Add autoload/psr-4/' . $namespace . ': App/Themes/Project/' . $plugin->getShortName());
					}
				}
			}
			elseif ($isModule)
			{
				$pPath = $plugin->getAbsolutePath() . '/composer.json';
				if ($verbose)
				{
					$response->addCommentMessage('Check ' . $pPath . ' files for autoload...');
				}
				if (!\is_readable($pPath))
				{
					$response->addErrorMessage($pPath . ' file is not readable');
					return;
				}
				$pComposer = \json_decode(file_get_contents($pPath), true);
				if (!\is_array($pComposer))
				{
					$response->addErrorMessage($pPath . ' file is not valid');
					return;
				}
				$namespace = $plugin->getVendor() . '\\' . $plugin->getShortName() . '\\';
				if (!($pComposer['autoload']['psr-4'][$namespace] ?? false))
				{
					$pComposer['autoload']['psr-4'][$namespace] = '.';
					$response->addInfoMessage('Add autoload/psr-4/' . $namespace . ': .');
					$response->addInfoMessage('Update ' . $pPath);
					file_put_contents($pPath, $this->encode($pComposer));
				}
			}
			elseif ($isTheme)
			{
				$pPath = $plugin->getAbsolutePath() . '/composer.json';
				if ($verbose)
				{
					$response->addCommentMessage('Check ' . $pPath . ' files for autoload...');
				}
				if (!\is_readable($pPath))
				{
					$response->addErrorMessage($pPath . ' file is not readable');
					return;
				}
				$pComposer = \json_decode(file_get_contents($pPath), true);
				if (!\is_array($pComposer))
				{
					$response->addErrorMessage($pPath . ' file is not valid');
					return;
				}
				$namespace = 'Themes\\' . $plugin->getVendor() . '\\' . $plugin->getShortName() . '\\';
				if (!($pComposer['autoload']['psr-4'][$namespace] ?? false))
				{
					$pComposer['autoload']['psr-4'][$namespace] = '.';
					$response->addInfoMessage('Add autoload/psr-4/' . $namespace . ': .');
					$response->addInfoMessage('Update ' . $pPath);
					file_put_contents($pPath, $this->encode($pComposer));
				}
			}
		}
		if ($updateAutoload || $addOMNPackages || $addZendFrameworkPackages)
		{
			$response->addInfoMessage('Update ' . $path);
			file_put_contents($path, $this->encode($composer));
			if ($updateAutoload)
			{
				$response->addWarningMessage('Please execute: php composer.phar dump-autoload');
			}
		}
		$response->addInfoMessage('done.');
	}

	/**
	 * @param array $composer
	 * @return string
	 */
	protected function encode(array $composer)
	{
		return str_replace('    ', '	', \json_encode($composer, JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES + JSON_PRETTY_PRINT));
	}
}