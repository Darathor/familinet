<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Commands;

/**
 * @name \Rbs\Dev\Commands\Devrest
 */
class Devrest
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		$application = $event->getApplication();
		$workspace = $application->getWorkspace();

		$webBaseDirectory = $application->getConfiguration('Change/Install/webBaseDirectory');
		$devRestPath = $workspace->composeAbsolutePath($webBaseDirectory, 'devrest.php');
		$omsRestPath = $workspace->composeAbsolutePath($webBaseDirectory, 'devuaRest.php');


		$controllers = 'devrest.php and devuaRest.php';

		$response->addInfoMessage('Path to devrest.php: ' . $devRestPath);
		if ($event->getParam('mode') === 'up')
		{
			if ($webBaseDirectory && !$workspace->isAbsolutePath($webBaseDirectory))
			{
				$requirePath =
					implode(DIRECTORY_SEPARATOR, array_fill(0, count(explode(DIRECTORY_SEPARATOR, $webBaseDirectory)), '..'));
			}
			else
			{
				$requirePath = $workspace->projectPath();
			}

			$content = file_get_contents(__DIR__ . '/Assets/devrest.tpl');
			$content = str_replace('#projectPath#', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($devRestPath, $content);

			$content = file_get_contents(__DIR__ . '/Assets/devuaRest.tpl');
			$content = str_replace('#projectPath#', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($omsRestPath, $content);

			$response->addInfoMessage($controllers . ' successfully enabled.');
		}
		else
		{
			if (file_exists($omsRestPath))
			{
				unlink($omsRestPath);
			}

			if (file_exists($devRestPath))
			{
				unlink($devRestPath);
				$response->addInfoMessage($controllers . ' successfully disabled.');
			}
			else
			{
				$response->addInfoMessage($controllers . ' was already disabled.');
			}
		}
	}
}