<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Dev\Commands;

/**
 * @name \Rbs\Dev\Commands\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('config', function (\Change\Commands\Events\Event $event)
		{
			$commandConfigPath = __DIR__ . '/Assets/config.json';
			if (is_file($commandConfigPath))
			{
				return json_decode(file_get_contents($commandConfigPath), true);
			}
			return null;
		});

		$this->listeners[] = $events->attach('rbs_dev:initialize-block', function ($event)
		{
			(new \Rbs\Dev\Commands\InitializeBlock())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:initialize-model', function ($event)
		{
			(new \Rbs\Dev\Commands\InitializeModel())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:initialize-plugin', function ($event)
		{
			(new \Rbs\Dev\Commands\InitializePlugin())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:initialize-command', function ($event)
		{
			(new \Rbs\Dev\Commands\InitializeCommand())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:admin-routes', function ($event)
		{
			(new \Rbs\Dev\Commands\AdminRoutes())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:initialize-view', function ($event)
		{
			(new \Rbs\Dev\Commands\InitializeView())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:devrest', function ($event)
		{
			(new \Rbs\Dev\Commands\Devrest())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:check-autoload', function ($event)
		{
			(new \Rbs\Dev\Commands\CheckAutoload())->execute($event);
		});

		$this->listeners[] = $events->attach('rbs_dev:normalize-ua-routes', function ($event)
		{
			(new \Rbs\Dev\Commands\NormalizeUaRoutes())->execute($event);
		});
	}
}