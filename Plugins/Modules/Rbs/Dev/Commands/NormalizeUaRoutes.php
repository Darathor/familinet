<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Commands;

/**
 * @name \Rbs\Dev\Commands\NormalizeUaRoutes
 */
class NormalizeUaRoutes
{
	/**
	 * @param \Change\Commands\Events\Event $event
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function execute(\Change\Commands\Events\Event $event)
	{
		$response = $event->getCommandResponse();

		[$vendorName, $moduleName] = explode('_', $event->getParam('moduleName'));
		$module = $event->getApplicationServices()->getPluginManager()->getModule($vendorName, $moduleName);
		if (!$module)
		{
			$response->addErrorMessage('There is no module ' . $vendorName . '_' . $moduleName . '!');
			return;
		}
		$assetsPath = $module->getAssetsPath() . '/Ua/';

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$uaManager = $genericServices->getUaManager();

		$file = $event->getParam('fileName');
		if ($file)
		{
			if (!is_readable($assetsPath . $file))
			{
				$response->addErrorMessage('File not found: ' . $assetsPath . $file);
				return;
			}
			$this->normalizeFile($response, $uaManager, $vendorName, $moduleName, $assetsPath . $file);
		}
		else
		{
			$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($assetsPath,
				\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
			while ($it->valid())
			{
				/* @var $item \RecursiveDirectoryIterator */
				$item = $it->current();
				if ($item->isFile() && $item->getExtension() === 'json' && \Change\Stdlib\StringUtils::beginsWith($item->getFilename(), 'routes.'))
				{
					$this->normalizeFile($response, $uaManager, $vendorName, $moduleName, $assetsPath . $item->getFilename());
				}
				$it->next();
			}
		}

		$response->addInfoMessage('Done.');
	}

	/**
	 * @param \Change\Commands\Events\CommandResponseInterface $response
	 * @param \Rbs\Ua\UaManager $uaManager
	 * @param string $vendorName
	 * @param string $moduleName
	 * @param string $filePath
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	protected function normalizeFile($response, \Rbs\Ua\UaManager $uaManager, $vendorName, $moduleName, $filePath)
	{
		$response->addInfoMessage('Normalize ' . $filePath);

		if (!preg_match('#routes\.([a-zA-Z0-9]+)\.json#', $filePath, $matches))
		{
			throw new \RuntimeException('Invalid routes file: ' . $filePath);
		}
		$resources = $uaManager->getResources($vendorName, $moduleName, $matches[1]);

		$routes = json_decode(\Change\Stdlib\FileUtils::read($filePath), true);
		foreach ($routes as $path => $route)
		{
			// Validation.
			if (!isset($route['module']) && !isset($route['model']))
			{
				$response->addErrorMessage('Missing "module" or "model" parameter in the route ' . $path);
				continue;
			}
			if (!isset($route['name']))
			{
				$response->addErrorMessage('Missing "name" parameter in the route ' . $path);
				continue;
			}
			if (!isset($route['rule']))
			{
				$response->addErrorMessage('Missing "rule" parameter in the route ' . $path);
				continue;
			}
			if (!isset($route['rule']['redirectTo']) && !isset($route['rule']['templateUrl']))
			{
				$response->addErrorMessage('Missing "rule/redirectTo" or "rule/templateUrl" parameter in the route ' . $path);
				continue;
			}
			if (isset($route['rule']['templateUrl']) && (!isset($route['rule']['labelKey']) && !isset($route['rule']['labelParam'])))
			{
				$response->addErrorMessage('Missing "rule/labelKey" or "rule/labelParam" parameter in the route ' . $path);
				continue;
			}

			// Cleanup label key.
			if (isset($route['rule']['labelKey']) && \Change\Stdlib\StringUtils::endsWith($route['rule']['labelKey'], 'ucf'))
			{
				$route['rule']['labelKey'] = preg_replace('#\s*\|\s*ucf#i', '', $route['rule']['labelKey']);
			}

			// Template check.
			if (isset($route['rule']['templateUrl']))
			{
				$templateUrl = $route['rule']['templateUrl'];
				$parts = explode('/', $templateUrl);
				$templateModuleName = array_shift($parts) . '_' . array_shift($parts);
				$subPath = implode('/', $parts);
				$rendered = $resources->renderModuleTemplateFile($templateModuleName, $subPath, []);

				$foundControllers = [];
				preg_match_all('#ng-controller="([^"]+)"#m', $rendered, $matches);
				foreach ($matches[1] as $controller)
				{
					if (!is_array($route['controllers']) || !in_array($controller, $route['controllers']))
					{
						$route['controllers'][] = $controller;
						$response->addCommentMessage('Add controller ' . $controller . ' to the route ' . $path);
					}
					$foundControllers[$controller] = true;
				}

				foreach (array_diff($route['controllers'], array_keys($foundControllers)) as $notFound)
				{
					$response->addWarningMessage('Controller ' . $notFound . ' not found in the template for the route ' . $path);
				}
			}

			$routes[$path] = $route;
		}

		$json = json_encode($routes, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES);
		\Change\Stdlib\FileUtils::write($filePath, str_replace('    ', '	', $json));
	}
}