<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Events;

/**
 * @name \Rbs\Dev\Events\AssetDevUpdater
 */
class AssetDevUpdater
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onAdminHome(\Change\Events\Event $event)
	{
		$application = $event->getApplication();
		$configuration = $application->getConfiguration();
		if (!$configuration->inDevelopmentMode())
		{
			return;
		}

		if (($actionName = $event->getParam('ACTION_NAME')) && ($actionName === 'Rbs\Admin\Http\Actions\GetHome::execute')
			&& $configuration->getEntry('Rbs/Dev/Resources/UI')
		)
		{
			$application->getLogging()->debug(__METHOD__, 'START', $actionName);

			$pluginManager = $event->getApplicationServices()->getPluginManager();
			if ($configuration->getEntry('Rbs/Dev/Resources/UI.common'))
			{
				/* @var $genericServices \Rbs\Generic\GenericServices */
				$genericServices = $event->getServices('genericServices');
				$this->installUACommonAssets($genericServices->getUaManager());
			}

			$resources = new \Rbs\Admin\Http\Resources($application);
			$assetManager = $resources->getNewAssetManager();
			$resources->registerPlugins($assetManager, $pluginManager);
			$resources->write($assetManager);

			$application->getLogging()->debug(__METHOD__, 'END', $actionName);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onUAHome(\Change\Events\Event $event)
	{
		$application = $event->getApplication();
		$configuration = $application->getConfiguration();
		if (!$configuration->inDevelopmentMode())
		{
			return;
		}

		if (($actionName = $event->getParam('ACTION_NAME')) && $configuration->getEntry('Rbs/Dev/Resources/UI'))
		{
			$pluginManager = $event->getApplicationServices()->getPluginManager();
			foreach ($pluginManager->getModules() as $module)
			{
				(new \Rbs\Ua\Setup\Compilation())->compileAngularControllersMapping($module, $event->getApplication());
			}

			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$uaManager = $genericServices->getUaManager();
			$applicationName = $event->getParam('applicationName');

			if ($actionName === 'Rbs\Seo\Http\UI\Home::execute')
			{
				$application->getLogging()->debug(__METHOD__, 'START', $actionName);

				$this->registerUaAssets('Rbs', 'Seo', $applicationName, $uaManager, $configuration);

				$application->getLogging()->debug(__METHOD__, 'END', $actionName);
			}
			elseif ($actionName === 'Rbs\Dev\Http\UI\Home::execute')
			{
				$application->getLogging()->debug(__METHOD__, 'START', $actionName);

				$this->registerUaAssets('Rbs', 'Dev', $applicationName, $uaManager, $configuration);

				$application->getLogging()->debug(__METHOD__, 'END', $actionName);
			}
		}
	}

	/**
	 * @param string $vendorName
	 * @param string $moduleName
	 * @param string $applicationName
	 * @param \Rbs\Ua\UaManager $uaManager
	 * @param \Change\Configuration\Configuration $configuration
	 */
	protected function registerUaAssets($vendorName, $moduleName, $applicationName, $uaManager, \Change\Configuration\Configuration $configuration)
	{
		$resources = $uaManager->getResources($vendorName, $moduleName, $applicationName);
		$assetManager = $resources->getNewAssetManager();
		if ($configuration->getEntry('Rbs/Dev/Resources/UI.common'))
		{
			$resources->registerCommonLibAssets($assetManager);
		}
		$resources->registerAssets($assetManager);
		$resources->write($assetManager);
	}

	/**
	 * @param \Rbs\Ua\UaManager $uaManager
	 */
	protected function installUACommonAssets($uaManager)
	{
		$manager = $uaManager->getResources('Rbs', 'Ua', 'common');
		$assetManager = $manager->getNewAssetManager();
		$manager->registerAssets($assetManager);
		$manager->registerCommonLibAssets($assetManager);
		$manager->write($assetManager);
	}

	/**
	 * @var \Change\Presentation\Interfaces\Theme[]
	 */
	protected $themes = [];

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onAddTheme(\Change\Events\Event $event)
	{
		$application = $event->getApplication();
		$configuration = $application->getConfiguration();
		if (!$configuration->inDevelopmentMode())
		{
			return;
		}

		/** @var \Change\Presentation\Interfaces\Theme $theme */
		if (($theme = $event->getParam('theme')) && $configuration->getEntry('Rbs/Dev/Resources/Theme'))
		{
			$themeName = $theme->getName();
			if (!isset($this->themes[$themeName]))
			{
				$this->themes[$themeName] = true;
				[$vendor, $shortName] = explode('_', $themeName);
				$pluginManager = $event->getApplicationServices()->getPluginManager();

				$plugin = $pluginManager->getTheme($vendor, $shortName);
				if ($plugin)
				{
					$application->getLogging()->debug(__METHOD__, 'START', $themeName);

					$workspace = $application->getWorkspace();
					$themeAssets = new \Change\Presentation\Themes\ThemeAssets($application);

					$pluginTheme = new \Change\Plugins\PluginTheme($theme->getName(), $workspace);
					$themeAssets->installPluginTemplates($plugin, $pluginTheme, false);

					if (!isset($this->themes['Rbs_Base']) && $configuration->getEntry('Rbs/Dev/Resources/Theme.base'))
					{
						$this->themes['Rbs_Base'] = true;
						$application->getLogging()->debug(__METHOD__, 'START DEFAULT THEME');
						$defaultTheme = new \Change\Plugins\PluginTheme('Rbs_Base', $workspace);
						$themeAssets->installPluginTemplates($pluginManager->getTheme('Rbs', 'Base'), $defaultTheme, false);
						foreach ($pluginManager->getModules() as $module)
						{
							if ($module->isAvailable())
							{
								$themeAssets->installPluginTemplates($module, $defaultTheme, false);
							}
						}
						$application->getLogging()->debug(__METHOD__, 'END DEFAULT THEME');
					}

					$application->getLogging()->debug(__METHOD__, 'END', $themeName);
				}
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onAddPageResources(\Change\Events\Event $event)
	{
		$application = $event->getApplication();
		if (!$application->inDevelopmentMode())
		{
			return;
		}

		$themeManager = $event->getApplicationServices()->getThemeManager();

		$theme = $themeManager->getCurrent();
		$themeName = $theme->getName();
		$application->getLogging()->debug(__METHOD__, 'START', $themeName);
		if ($themeName === \Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME)
		{
			return;
		}

		$pluginManager = $event->getApplicationServices()->getPluginManager();
		[$vendor, $shortName] = explode('_', $themeName);
		$plugin = $pluginManager->getTheme($vendor, $shortName);

		$workspace = $application->getWorkspace();

		$webBaseDirectory = $pluginManager->getWebBaseDirectory();
		$webThemeAssetsBaseDirectory = $workspace->composePath($pluginManager->getWebAssetsBaseDirectory(), 'Theme');
		$themeAssets = new \Change\Presentation\Themes\ThemeAssets($application);

		$pluginTheme = $this->getPluginThemeTree($plugin, $themeAssets, $pluginManager, $workspace);

		$templates = $themeAssets->getNgTemplatesPath($pluginTheme);
		$pt = $pluginTheme->getParentTheme();
		while ($pt)
		{
			$templates = array_merge($templates, $themeAssets->getNgTemplatesPath($pt));
			$pt = $pt->getParentTheme();
		}
		$themeAssets->writeNgTemplates($pluginTheme, $templates);

		$pluginTheme->writeConfiguration($pluginTheme->getAssetConfiguration());

		$assetBaseDir = $workspace->composePath($webThemeAssetsBaseDirectory, $pluginTheme->getVendor(), $pluginTheme->getShortName());
		$themeAssets->installPluginAssets($plugin, $assetBaseDir);

		$configuration = $application->getConfiguration();
		$imageFormats = $configuration->getEntry('Rbs/Media/namedImageFormats');

		$imageLessFilePath = $workspace->compilationPath('Themes', 'compiled-image-formats.less');
		file_put_contents($imageLessFilePath, $themeAssets->generateImageLessContent($imageFormats));

		$imageScssFilePath = $workspace->compilationPath('Themes', 'compiled-image-formats.scss');
		file_put_contents($imageScssFilePath, $themeAssets->generateImageScssContent($imageFormats));

		$configuration = $themeManager->getAssetConfiguration($theme, $event->getParam('template')->getCode());
		$am = $themeAssets->getAsseticManager($pluginTheme, $configuration);

		$application->getLogging()->debug(__METHOD__, 'WRITING', $themeName);

		$writer = new \Assetic\AssetWriter($webBaseDirectory);
		$writer->writeManagerAssets($am);

		$application->getLogging()->debug(__METHOD__, 'END', $themeName);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Presentation\Themes\ThemeAssets $themeAssets
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Workspace $workspace
	 * @return \Change\Plugins\PluginTheme
	 */
	protected function getPluginThemeTree($plugin, $themeAssets, $pluginManager, $workspace)
	{
		$parentPluginTheme = null;
		$parentPluginThemeName = null;
		$config = $plugin->getConfiguration();
		if (isset($config['extends']) && $config['extends'])
		{
			$parentPluginThemeName = (string)$config['extends'];
			$nameParts = explode('_', $parentPluginThemeName);
			if (count($nameParts) != 2)
			{
				throw new \InvalidArgumentException('Theme "' . $plugin->getName() . '" as invalid extends: ' . $parentPluginThemeName, 999999);
			}
			[$vendor, $shortName] = $nameParts;
			$parentPlugin = $pluginManager->getTheme($vendor, $shortName);
			if (!$parentPlugin)
			{
				throw new \InvalidArgumentException('Theme "' . $plugin->getName() . '" as undefined extends: ' . $parentPluginThemeName, 999999);
			}
			$parentPluginTheme = $this->getPluginThemeTree($parentPlugin, $themeAssets, $pluginManager, $workspace);
		}
		else
		{
			$parentPluginTheme = $this->getDefaultThemePlugin($themeAssets, $workspace, $pluginManager->getModules());
		}
		$pluginTheme = new \Change\Plugins\PluginTheme($plugin->getName(), $workspace, $parentPluginTheme);

		$assetConfiguration = $themeAssets->appendConfiguration(
			$parentPluginTheme->getAssetConfiguration(),
			$themeAssets->buildAssetConfiguration($pluginTheme)
		);

		$pluginTheme->setAssetConfiguration($assetConfiguration);
		return $pluginTheme;
	}

	/**
	 * @param \Change\Presentation\Themes\ThemeAssets $themeAssets
	 * @param \Change\Workspace $workspace
	 * @param \Change\Plugins\Plugin[]|null $modules
	 * @return \Change\Plugins\PluginTheme
	 */
	protected function getDefaultThemePlugin($themeAssets, $workspace, array $modules = null)
	{
		$defaultTheme = new \Change\Plugins\PluginTheme(\Change\Presentation\Themes\ThemeManager::DEFAULT_THEME_NAME, $workspace);
		$defaultTheme->setAssetConfiguration($themeAssets->buildAssetConfiguration($defaultTheme, $modules));
		return $defaultTheme;
	}
}