<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Events;

/**
 * @name \Rbs\Dev\Events\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$assetDevUpdater = new \Rbs\Dev\Events\AssetDevUpdater();

		$events->attach('Http.Admin', 'http.result', [$assetDevUpdater, 'onAdminHome'], 50);

		$events->attach('Http.UA.UI', 'http.result', [$assetDevUpdater, 'onUAHome'], 50);

		$events->attach('Presentation.Themes', 'addTheme', [$assetDevUpdater, 'onAddTheme']);

		$events->attach('Presentation.Themes', 'addPageResources', [$assetDevUpdater, 'onAddPageResources'], 10);

		$events->attach('SmsManager', 'sendMessage', static function ($event) { (new SmsManager())->onSendMessage($event); }, -5);

		$events->attach('SviManager', 'sendMessage', static function ($event) { (new SviManager())->onSendMessage($event); }, -5);

		$events->attach('Statistics', 'flush', static function ($event) { (new StatisticsManager())->onFlush($event); }, -1);

		$events->attach('PageManager', 'buildTemplateReplacements', static function (\Change\Events\Event $event)
		{
			$configuration = $event->getApplication()->getConfiguration();
			if (!$configuration->inDevelopmentMode() || $configuration->getEntry('Change/Presentation/AngularDevMode'))
			{
				return;
			}

			$templateReplacements = $event->getParam('templateReplacements');
			if (isset($templateReplacements['<!-- jsFooter -->']))
			{
				$script = '<script type="text/javascript">';
				$script .= 'console.warn(
					"Development mode is disabled in AngularJS." +
					" To enable it, set Change/Presentation/AngularDevMode to true in the project configuration." +
					"\nMore info here: https://code.angularjs.org/" + angular.version.full + "/docs/guide/production#disabling-debug-data"
				);';
				$script .='</script>';
				$templateReplacements['<!-- jsFooter -->'] .= $script;
				$event->setParam('templateReplacements', $templateReplacements);
			}
		}, 0);
	}
}