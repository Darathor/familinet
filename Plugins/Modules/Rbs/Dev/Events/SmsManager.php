<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Events;

/**
 * @name \Rbs\Dev\Events\SmsManager
 */
class SmsManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSendMessage(\Change\Events\Event $event)
	{
		if (!$event->getApplication()->inDevelopmentMode())
		{
			return;
		}

		$messageIdentifier = $event->getParam('messageIdentifier');
		$mobilePhone = $event->getParam('mobilePhone');
		$message = $event->getParam('message');

		if (!$messageIdentifier)
		{
			$messageIdentifier = uniqid('sms', false);
			$event->setParam('messageIdentifier', $messageIdentifier);
		}

		$event->getApplication()->getLogging()->info('send SMS Id:', $messageIdentifier, 'to:', $mobilePhone, 'msg:', $message);
	}
}