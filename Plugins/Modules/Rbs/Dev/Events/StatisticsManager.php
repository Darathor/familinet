<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Dev\Events;

/**
 * @name \Rbs\Dev\Events\StatisticsManager
 */
class StatisticsManager
{
	public function onFlush(\Change\Events\Event $event)
	{
		$eventsData = $event->getParam('eventsData');
		if (!$eventsData || !$event->getApplication()->inDevelopmentMode())
		{
			return;
		}
		$logging = $event->getApplication()->getLogging();
		$logger = $logging->getLoggerByName('statistics');
		$extra = ['userId' => $logging->getUser() ? $logging->getUser()->getId() : 0];
		foreach ($eventsData as $eventData)
		{
			$logger->info('Statistics event recorded: '. json_encode($eventData, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE), $extra);
		}
	}
}