<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Events\UaManager;

/**
 * @name \Rbs\Dev\Events\UaManager\Ua
 */
class Ua
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetResources(\Change\Events\Event $event)
	{
		if ($event->getParam('resources') || $event->getParam('vendorName') !== 'Rbs' || $event->getParam('moduleName') !== 'Dev')
		{
			return;
		}

		$event->setParam('resources', new \Rbs\Dev\Http\UI\Resources($event->getParam('applicationName')));
	}
}