<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\API;

/**
 * @name \Rbs\Dev\Http\API\ApiManager
 */
class ApiManager
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function getOrderManager(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.orderManager.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'orderManager';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Order Manager API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getLogistics(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.logistics.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'logistics';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Logistics API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getReturnManager(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.returnManager.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'returnManager';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Return Manager API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getFinancial(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.financial.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'financial';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Financial API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getCustomerSupport(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.customerSupport.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'customerSupport';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Customer Support API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getStoreSeller(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.storeSeller.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'storeSeller';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Store seller API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getStoreManager(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.storeManager.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'storeManager';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Store Manager API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getSeoManager(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Seo')->getAssetsPath() . '/Ua/rest.seoManager.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'seoManager';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Seo Manager API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getPerformanceManager(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Performance')->getAssetsPath() . '/Ua/rest.performanceManager.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'performanceManager';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Performance Manager API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getServer(\Change\Http\Event $event)
	{
		$pluginManager = $event->getApplicationServices()->getPluginManager();
		$definitions = $this->getUaCommonDefinitions($pluginManager);

		$definitions->initFromFile($pluginManager->getModule('Rbs', 'Oms')->getAssetsPath() . '/Ua/rest.server.json');

		$apiModels = $definitions->getModels();
		$apiModels->compile();

		$info = $definitions->getInfo();
		$info['resourcePath'] = 'server';
		$info['basePath'] = '/devuaRest.php/' . $info['resourcePath'];
		$info['title'] = $definitions->info('title', 'Server API');
		$definitions->setInfo($info);

		$fallbackContext = new \Rbs\Dev\Http\API\SampleFallbackContext($event, $apiModels);
		$swagger = $this->buildPaths($definitions, $fallbackContext);

		$result = new \Rbs\Ua\Http\API\ArrayResult($swagger);
		$event->setResult($result);
	}

	/**
	 * @param \Rbs\Dev\Http\API\ Definitions $definitions
	 * @param \Rbs\Dev\Http\API\SampleFallbackContext $fallbackContext
	 * @return array
	 */
	protected function buildPaths($definitions, $fallbackContext)
	{
		$apiModels = $definitions->getModels();
		$swagger = [
			'swagger' => '2.0',
			'info' => [
				'title' => $definitions->info('title'),
				'version' => $definitions->info('version', '1.0.0'),
				'description' => $definitions->info('description', '')
			],
			'consumes' => [
				'application/json'
			],
			'produces' => [
				'application/json'
			],
			'securityDefinitions' => [
				'api_key' => [
					'name' => 'userId',
					'in' => 'query',
					'type' => 'apiKey'
				]
			],
			'basePath' => $definitions->info('basePath'),
			'tags' => [
			],
			'paths' => [

			],
			'definitions' => [

			]
		];
		$api = [];
		foreach ($definitions->info('sections', []) as $name => $description)
		{
			$swagger['tags'][] = ['name' => $name, 'description' => $description];
		}

		foreach ($definitions->getDefinitions() as $def)
		{
			foreach ($def->getOperations() as $operation)
			{
				$operation->compile($apiModels);

				$d = ['operationId' => $operation->getMethod() . '.' . $def->getName(),
					'parameters' => [], 'description' => '',
					'security' => ['api_key' => []],
					'produces' => ['application/json'],
					'responses' => []];
				if ($operation->getDescription())
				{
					$d['description'] = $operation->getDescription();
				}
				$d['description'] .= '  ' . PHP_EOL . 'Proximis internal name: __' . $def->getName() . '__';
				$fields = [];
				foreach ($operation->getResponses() as $response)
				{
					$rm = $response->getModel();
					if ($rm)
					{
						$rf = $response->getFields();
						if ($rf)
						{
							foreach ($rf->getProperties() as $name => $property)
							{
								$fields[$name] = $property;
								$rm->setProperty($name, $property);
							}
						}
						if ($rm->getType() === 'ResponseProperties')
						{
							$type = $def->getName() . '.' . ucfirst(strtolower($operation->getMethod()))
								. 'Response.' . ($response->getHttpStatus() ?? '200');
							$rm->setType($type);
						}

						$apiModels->addModel($rm);
						$d['responses'][(string)$response->getHttpStatus()] = [
							'description' => $response->getDescription() ?? 'Default result',
							'schema' => $fallbackContext->buildSchema($rm, $addedDefinition)
						];
						if ($addedDefinition && !isset($swagger['definitions'][$addedDefinition]))
						{
							$swagger['definitions'][$addedDefinition] = [];
						}
					}
				}

				if ($tags = $def->getSections())
				{
					$d['tags'] = $tags;
				}
				$body = [];

				foreach ($operation->getParameters() as $p)
				{
					$param = ['name' => $p->getName()];
					$m = $apiModels->getModel($p->getType());
					if ($p->getSrc() === 'query')
					{
						$param['in'] = 'query';
						$param = array_merge($param, $fallbackContext->buildSchema($m, $definition));
						if ($p->getDescription())
						{
							$param['description'] = $p->getDescription();
						}
						$type = $param['type'] ?? null;
						if ($type == 'array')
						{
							$param['name'] .= '[]';
							$param['collectionFormat'] = 'multi';
						}
						elseif ($type == 'object' || $m instanceof \Rbs\Ua\Model\HashTableModel)
						{
							$param['type'] = 'string';
							unset($param['$ref']);
							$dv = $p->getDefaultValue();
							if ($dv !== null && !is_string($dv))
							{
								$p->setDefaultValue(json_encode($dv));
							}
							$description = $param['description'] ?? '';
							if ($description)
							{
								$description .= '  ' . PHP_EOL;
							}
							$description .= 'JSON String serialized representation  ' . PHP_EOL .
								'```json' . PHP_EOL . '{}' . PHP_EOL . '```';
							$param['description'] = $description;
						}
						elseif ($m instanceof \Rbs\Ua\Model\ObjectModel)
						{
							$param['type'] = 'string';
							unset($param['$ref']);
							$dv = $p->getDefaultValue();
							if ($dv !== null && !is_string($dv))
							{
								$p->setDefaultValue(json_encode($dv));
							}
							$default = [];
							foreach ($m->getProperties() as $pn => $property)
							{
								$pModel = $apiModels->getModel($property['type']);
								$default[$pn] = $property['defaultValue'] ?? $fallbackContext->getFakeValue($pModel, $property['required'] ?? false);
							}
							$description = $param['description'] ?? '';
							if ($description)
							{
								$description .= '  ' . PHP_EOL;
							}
							$description .= 'JSON String serialized representation  ' . PHP_EOL .
								'```json' . PHP_EOL . json_encode($default, JSON_PRETTY_PRINT) . PHP_EOL . '```';
							$param['description'] = $description;
						}
						if ($definition && !isset($swagger['definitions'][$definition]))
						{
							$swagger['definitions'][$definition] = [];
						}
					}
					elseif ($p->getSrc() === 'body')
					{
						$property = ['type' => $p->getType(), 'required' => $p->getRequired(),
							'defaultValue' => $p->getDefaultValue(), 'description' => $p->getDescription()];
						if ($enum = $p->getEnum())
						{
							$property['enum'] = $enum;
						}
						$body[$p->getName()] = $property;
						continue;
					}
					elseif ($p->getSrc() === 'path')
					{
						$param['in'] = 'path';
						$param = array_merge($param, $fallbackContext->buildSchema($m, $definition));
						if ($definition && !isset($swagger['definitions'][$definition]))
						{
							$swagger['definitions'][$definition] = [];
						}
					}
					if ($enum = $p->getEnum())
					{
						$param['enum'] = $enum;
					}
					if ($p->getRequired())
					{
						$param['required'] = true;
					}
					if (($dv = $p->getDefaultValue()) !== null)
					{
						$param['default'] = $dv;
					}
					if ($p->getDescription())
					{
						$param['description'] = $p->getDescription();
					}
					$d['parameters'][] = $param;
				}

				if ($body)
				{
					$added = [];
					$type = $d['operationId'] . 'Body';
					$model = new \Rbs\Ua\Model\ObjectModel($type, ['properties' => $body]);
					$model->compile($apiModels);
					$param = ['name' => '_body', 'in' => 'body',
						'schema' => $fallbackContext->resolveObjectModelDefinition($model, $swagger['definitions'], $added)];
					if ($added)
					{
						$swagger['definitions'] = array_merge($added, $swagger['definitions']);
					}
					$param['description'] = 'The body content (look at the model on the right for details)';
					$d['parameters'][] = $param;
				}

				if ($fields)
				{
					$param = ['name' => 'fields[]', 'in' => 'query', 'type' => 'array',
						'uniqueItems' => true, 'collectionFormat' => 'multi',
						'description' => 'The optional response fields',
						'items' => ['type' => 'string', 'enum' => array_keys($fields)]];
					$d['parameters'][] = $param;
				}

				$api['/' . $def->getPath()][strtolower($operation->getMethod())] = $d;
			}
		}

		$swagger['definitions'] = $fallbackContext->resolveDefinitions($swagger['definitions']);
		$swagger['paths'] = $api;
		return $swagger;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return Definitions
	 */
	protected function getUaCommonDefinitions($pluginManager)
	{
		$definitions = new Definitions();
		$module = $pluginManager->getModule('Rbs', 'Ua');
		$filePath = $module->getAssetsPath() . '/rest.global.json';
		$definitions->initFromFile($filePath);
		return $definitions;
	}
}