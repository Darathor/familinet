<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\API;

/**
 * @name \Rbs\Dev\Http\API\Definition
 * @method \Rbs\Dev\Http\API\Operation[] getOperations()
 */
class Definition extends \Rbs\Ua\API\Definition
{
	/**
	 * @var array
	 */
	protected $sections;

	/**
	 * @var string
	 */
	protected $description;

	/**
	 * @return array
	 */
	public function getSections()
	{
		return $this->sections;
	}

	/**
	 * @param array $sections
	 * @return $this
	 */
	public function setSections($sections)
	{
		$this->sections = $sections;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		if ($description)
		{
			foreach ($this->getOperations() as $operation)
			{
				if (!$operation->getDescription())
				{
					$operation->setDescription($description);
				}
			}
		}
		return $this;
	}
}