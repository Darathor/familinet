<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\API;

/**
 * @name \Rbs\Dev\Http\API\Definitions
 * @method \Rbs\Dev\Http\API\Definition[] getDefinitions()
 */
class Definitions extends \Rbs\Ua\API\Definitions
{
	/**
	 * @var array
	 */
	protected $info = [];

	/**
	 * @param string $filePath
	 * @param array $data
	 */
	protected function parseData($filePath, $data)
	{
		$info = $data['info'] ?? [];
		if ($info)
		{
			$this->info = array_merge_recursive($info, $this->info);
		}
		parent::parseData($filePath, $data);
	}

	/**
	 * @return array
	 */
	public function getInfo()
	{
		return $this->info;
	}

	/**
	 * @param array $info
	 * @return $this
	 */
	public function setInfo($info)
	{
		$this->info = $info;
		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function info($key, $default = null)
	{
		return $this->info[$key] ?? $default;
	}

	/**
	 * @param array $api
	 */
	public function initFromArray(array $api)
	{
		foreach ($api as $def)
		{
			if (is_array($def) && isset($def['name'], $def['path'], $def['operations']) && is_string($def['path']) && is_array($def['operations']))
			{
				if (isset($this->definitions[$def['path']]))
				{
					throw new \LogicException('Duplicate declaration for path ' . $def['path']);
				}

				$operations = [];
				foreach ($def['operations'] as $opeData)
				{
					if (is_array($opeData) && isset($opeData['method'], $opeData['action']))
					{
						if (isset($operations[$opeData['method']]))
						{
							throw new \LogicException('Duplicate method ' . $opeData['method'] . ' for path ' . $def['path']);
						}

						$parameters = [];
						if (isset($opeData['parameters']) && is_array($opeData['parameters']))
						{
							$parameters = $this->buildParameters($opeData['parameters'], ['path' => $def['path'], 'method' => $opeData['method']]);
						}
						$method = $opeData['method'];
						$action = $opeData['action'];
						$responses = $this->buildResponses($opeData['responses'] ?? [], ['path' => $def['path'], 'method' => $opeData['method']]);

						$operation = new Operation($method, $action, $parameters, $responses);
						if (array_key_exists('authorization', $opeData))
						{
							$operation->setAuthorization($opeData['authorization']);
						}
						$operation->setDescription($opeData['description'] ?? null);
						$operations[$operation->getMethod()] = $operation;
					}
				}
				$definition = new Definition($def['name'], $def['path'], $operations);
				if (isset($def['sections']) && is_array($def['sections']))
				{
					$definition->setSections($def['sections']);
				}
				$this->definitions[$definition->getPath()] = $definition;
			}
		}
	}

	/**
	 * @param array $parametersData
	 * @param array $operation
	 * @return \Rbs\Dev\Http\API\Parameter[]
	 */
	protected function buildParameters(array $parametersData, array $operation)
	{
		$parameters = [];
		foreach ($parametersData as $paramData)
		{
			if (is_array($paramData) && isset($paramData['name'], $paramData['type'], $paramData['src']))
			{
				if (isset($parameters[$paramData['name']]))
				{
					throw new \LogicException('Duplicate parameter ' . $paramData['name'] . ' for path ' . $operation['path'] . ' and method '
						. $operation['method']);
				}

				$parameter = new Parameter($paramData['name'], $paramData['type'], $paramData['src']);
				$parameter->setPattern($paramData['pattern'] ?? null);
				$parameter->setDefaultValue($paramData['defaultValue'] ?? null);
				$parameter->setDescription($paramData['description'] ?? null);
				$parameter->setRequired($parameter->getRequired() || ($paramData['required'] ?? false));
				if (($enum = $paramData['enum'] ?? false) && is_array($enum))
				{
					$parameter->setEnum($enum);
				}
				$parameters[$parameter->getName()] = $parameter;
			}
		}
		return $parameters;
	}

	/**
	 * @param array $responsesData
	 * @param array $operation
	 * @return \Rbs\Dev\Http\API\Response[]
	 */
	protected function buildResponses(array $responsesData, array $operation)
	{
		$responseStatuses = [];
		$responses = [];
		foreach ($responsesData as $responseData)
		{
			$httpStatus = $responseData['httpStatus'] ?? 200;
			if (in_array($httpStatus, $responseStatuses))
			{
				throw new \LogicException('Duplicate response status ' . $httpStatus . ' for path ' . $operation['path'] . ' and method '
					. $operation['method']);
			}
			$responseStatuses[] = $httpStatus;

			$response = new \Rbs\Dev\Http\API\Response($httpStatus);
			$response->setDescription($responseData['description'] ?? null);
			if (isset($responseData['predefinedType']))
			{
				switch ($responseData['predefinedType'])
				{
					case 'Success':
						$response->setModel($this->getSuccessModel());
						if (!$response->getDescription())
						{
							$response->setDescription('Standard success response');
						}
						$responses[] = $response;
						break;
					case 'Error':
						$response->setModel($this->getErrorModel());
						if (!$response->getDescription())
						{
							$response->setDescription('Standard error response');
						}
						$responses[] = $response;
						break;
					default:
						break;
				}
				continue;
			}

			$propertiesData = $responseData['properties'] ?? [];
			if (!$propertiesData)
			{
				continue;
			}
			$properties = [];

			$fields = [];
			foreach ($propertiesData as $name => $propertyData)
			{
				if (isset($properties[$name]))
				{
					throw new \LogicException('Duplicate property ' . $name . ' for path ' . $operation['path'] . ', method '
						. $operation['method'] . ' and status ' . $httpStatus);
				}

				$isField = $propertyData['fields'] ?? false;
				if ($isField)
				{
					unset($propertyData['required']);
					$propertyData['fields'] = true;
					$fields[$name] = $propertyData;
				}
				else
				{
					unset($propertyData['fields']);
					$propertyData['required'] = true;
					$properties[$name] = $propertyData;
				}
			}
			if ($properties)
			{
				$response->setModel(new \Rbs\Ua\Model\ObjectModel('ResponseProperties', ['properties' => $properties]));
				if ($fields)
				{
					$response->setFields(new \Rbs\Ua\Model\ObjectModel('ResponseFields', ['properties' => $fields]));
				}
				$responses[] = $response;
			}
		}
		return $responses;
	}
}