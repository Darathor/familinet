<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\API;

/**
 * @name \Rbs\Dev\Http\API\SampleFallbackContext
 */
class SampleFallbackContext implements \Rbs\Ua\Model\FallbackContextInterface
{
	/**
	 * @var \Rbs\Ua\Model\Models
	 */
	protected $models;

	public function __construct(\Change\Http\Event $event, \Rbs\Ua\Model\Models $models)
	{
		$this->models = $models;
	}

	public function resolveProperty($object, $objectModel, $propertyName, &$resolved)
	{
		$resolved = true;
		$p = $objectModel->getProperty($propertyName);
		if (isset($p['defaultValue']))
		{
			return $p['defaultValue'];
		}
		$propertyModel = $this->models->getModel($p['type']);
		$value = $this->getFakeValue($propertyModel, $p['required'] ?? false);
		if ($value !== null)
		{
			return $value;
		}

		$resolved = false;
		return null;
	}

	/**
	 * @param \Rbs\Ua\Model\ModelInterface $propertyModel
	 * @param boolean $required
	 * @return array|bool|int|\DateTime|null
	 */
	public function getFakeValue(\Rbs\Ua\Model\ModelInterface $propertyModel = null, $required)
	{
		if (!$propertyModel)
		{
			return null;
		}
		if ($propertyModel instanceof \Rbs\Ua\Model\ScalarModel)
		{
			switch ($propertyModel->getType())
			{
				case \Rbs\Ua\Model\ScalarModel::Boolean:
					return $required ? true : false;
				case \Rbs\Ua\Model\ScalarModel::Integer:
					return $required ? 1 : 0;
				case \Rbs\Ua\Model\ScalarModel::Float:
					return $required ? 1.1 : 0.1;
				case \Rbs\Ua\Model\ScalarModel::String:
					return $required ? 'REQUIRED STRING' : 'STRING';
				case \Rbs\Ua\Model\ScalarModel::DateTime:
					return $required ? new \DateTime() : new \DateTime('1970-01-01T00:00:00+00:00');
			}
		}
		elseif ($propertyModel instanceof \Rbs\Ua\Model\EnumModel)
		{
			return array_values($propertyModel->getEnum())[0];
		}
		elseif ($propertyModel instanceof \Rbs\Ua\Model\ObjectModel)
		{
			return ['__TYPE' => ($required ? '*' : '') . $propertyModel->getType()];
		}
		elseif ($propertyModel instanceof \Rbs\Ua\Model\ArrayModel)
		{
			return [$this->getFakeValue($this->models->getModel($propertyModel->getItemType()), $required)];
		}
		elseif ($propertyModel instanceof \Rbs\Ua\Model\HashTableModel)
		{
			return $required ? ['requiredProperty' => $this->getFakeValue($this->models->getModel($propertyModel->getItemType()), $required)] :
				['property' => $this->getFakeValue($this->models->getModel($propertyModel->getItemType()), $required)];
		}
		return null;
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @return mixed
	 */
	public function normalizeObject($object, $objectModel)
	{
		return $object;
	}

	/**
	 * @param \Rbs\Ua\Model\ModelInterface|null $model
	 * @param string|null $definition
	 * @return array
	 */
	public function buildSchema(\Rbs\Ua\Model\ModelInterface $model = null, &$definition)
	{
		$definition = null;
		if ($model instanceof \Rbs\Ua\Model\ScalarModel)
		{
			switch ($model->getType())
			{
				case \Rbs\Ua\Model\ScalarModel::Boolean:
					return ['type' => 'boolean'];
				case \Rbs\Ua\Model\ScalarModel::Integer:
					return ['type' => 'integer', 'format' => 'int64'];
				case \Rbs\Ua\Model\ScalarModel::Float:
					return ['type' => 'number', 'format' => 'double'];
				case \Rbs\Ua\Model\ScalarModel::String:
					return ['type' => 'string'];
				case \Rbs\Ua\Model\ScalarModel::DateTime:
					return ['type' => 'string', 'format' => 'date-time'];
			}
		}
		elseif ($model instanceof \Rbs\Ua\Model\EnumModel)
		{
			if ($model->getEnumType() === \Rbs\Ua\Model\ScalarModel::Integer)
			{
				return ['type' => 'integer', 'format' => 'int64', 'enum' => array_values($model->getEnum())];
			}
			return ['type' => 'string', 'enum' => array_values($model->getEnum())];
		}
		elseif ($model instanceof \Rbs\Ua\Model\ArrayModel)
		{
			$itemModel = $this->models->getModel($model->getItemType());
			return ['type' => 'array', 'items' => $this->buildSchema($itemModel, $definition)];
		}
		elseif ($model instanceof \Rbs\Ua\Model\HashTableModel)
		{
			$definition = $model->getType();
			return ['$ref' => '#/definitions/' . $definition];
		}
		elseif ($model instanceof \Rbs\Ua\Model\VariantObjectModel)
		{
			return ['type' => 'object'];
		}
		elseif ($model instanceof \Rbs\Ua\Model\ObjectModel)
		{
			$definition = $model->getType();
			$schema = ['$ref' => '#/definitions/' . $definition];
			if ($description = $model->getDescription())
			{
				$schema['description'] = $description;
			}
			return $schema;
		}
		return ['type' => 'string'];
	}

	/**
	 * @param \Rbs\Ua\Model\ObjectModel $model
	 * @param array $definitions
	 * @param array $added
	 * @return array
	 */
	public function resolveObjectModelDefinition(\Rbs\Ua\Model\ObjectModel $model, array $definitions, array &$added)
	{
		$definition = ['type' => 'object', 'properties' => []];
		if ($description = $model->getDescription())
		{
			$definition['description'] = $description;
		}
		foreach ($model->getProperties() as $n => $d)
		{
			$m = $this->models->getModel($d['type']);
			if ($d['required'] ?? false)
			{
				$definition['required'][] = $n;
			}
			$definition['properties'][$n] = $this->addPropertySchema($d, $this->buildSchema($m, $subDef));

			if ($subDef && !isset($definitions[$subDef]) && !isset($added[$subDef]))
			{
				$added[$subDef] = [];
			}
		}
		while ($pm = $model->getExtends())
		{
			$model = $pm;
			foreach ($model->getProperties() as $pn => $pd)
			{
				if (isset($definition['properties'][$pn]))
				{
					continue;
				}
				$m = $this->models->getModel($pd['type']);
				if ($pd['required'] ?? false)
				{
					$definition['required'][] = $pn;
				}
				$definition['properties'][$pn] = $this->addPropertySchema($pd, $this->buildSchema($m, $subDef));
				if ($subDef && !isset($definitions[$subDef]) && !isset($added[$subDef]))
				{
					$added[$subDef] = [];
				}
			}
		}
		return $definition;
	}

	/**
	 * @param array $property
	 * @param array $schemaDefinition
	 * @return array
	 */
	protected function addPropertySchema(array $property, array $schemaDefinition)
	{
		if ($description = $property['description'] ?? null)
		{
			$schemaDefinition['description'] = $description;
		}
		if (($default = $property['defaultValue'] ?? null) !== null)
		{
			$schemaDefinition['default'] = $default;
		}
		return $schemaDefinition;
	}

	/**
	 * @param array $definitions
	 * @return array
	 */
	public function resolveDefinitions(array $definitions)
	{
		$added = [];
		foreach ($definitions as $type => &$definition)
		{
			if (!$definition)
			{
				$model = $this->models->getModel($type);
				if ($model instanceof \Rbs\Ua\Model\ObjectModel)
				{
					$definition = $this->resolveObjectModelDefinition($model, $definitions, $added);
				}
				elseif ($model instanceof \Rbs\Ua\Model\HashTableModel)
				{
					$itemModel = $this->models->getModel($model->getItemType());
					$definition = ['type' => 'object',
						'additionalProperties' => $this->buildSchema($itemModel, $subDef)];
					if ($subDef && !isset($definitions[$subDef]) && !isset($added[$subDef]))
					{
						$added[$subDef] = [];
					}
					$definition['properties']['_KEY_'] = $this->buildSchema($itemModel, $subDef);
				}
				else
				{
					$definition = ['type' => 'string'];
				}
			}
		}
		unset($definition);

		if ($added)
		{
			return $this->resolveDefinitions(array_merge($added, $definitions));
		}
		return $definitions;
	}
}