<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\UI;

/**
 * @name \Rbs\Dev\Http\UI\Home
 */
class Home
{
	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function execute(\Change\Http\Event $event)
	{
		$applicationName = $event->getParam('applicationName');

		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');

		$i18nManager = $applicationServices->getI18nManager();
		$resources = $genericServices->getUaManager()->getResources('Rbs', 'Dev', 'apiExplorer');

		$devMode = $application->inDevelopmentMode();
		if ($devMode)
		{
			$event->setParam('ACTION_NAME', get_class($this) . '::execute');
		}

		$LCID = $i18nManager->getLCID();
		$urlManager = $event->getUrlManager();

		$attributes = [
			'devMode' => $devMode,
			'applicationCode' => 'apiExplorer',
			'applicationName' => $applicationName,
			'angularDevMode' => $devMode && $application->getConfiguration('Change/Presentation/AngularDevMode'),
			'baseURL' => $urlManager->getByPathInfo(null)->normalize()->toString(),
			'applicationsMenuURL' => $urlManager->getByPathInfo('../')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => substr($LCID, 0, 2),
			'supportedLCIDs' => $i18nManager->getSupportedLCIDs(),
			'vendorName' => 'Rbs',
			'moduleName' => 'Dev',
			'navigationContext' => [
				'assetBasePath' => $resources->getAssetsBasePath()
			]
		];

		$attributes['__change'] = $attributes;

		$html = $resources->renderModuleTemplateFile('Rbs_Dev', 'Ua/home.twig', $attributes);
		$result = new \Rbs\Ua\Http\UI\HtmlResult($html);
		$event->setResult($result);
	}
}