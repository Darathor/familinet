<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\UI;

/**
 * @name \Rbs\Dev\Http\UI\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->onAction($event); });
		$this->listeners[] = $events->attach('ApplicationsMenu', function ($event) { $this->onApplicationsMenu($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onApplicationsMenu(\Change\Http\Event $event)
	{
		if (!$event->getApplication()->inDevelopmentMode())
		{
			return;
		}

		$applicationsMenu = $event->getParam('applicationsMenu');

		$applicationsMenu['administration']['items'][] = [
			'position' => 90,
			'name' => 'apiExplorer',
			'realm' => 'Rbs_Dev_apiExplorer',
			'title' => 'API Explorer'
		];

		$event->setParam('applicationsMenu', $applicationsMenu);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onAction(\Change\Http\Event $event)
	{
		if ($event->getAction())
		{
			return;
		}
		$applicationName = $event->getParam('applicationName');
		if ($applicationName === 'apiExplorer')
		{
			$event->setAction([new \Rbs\Dev\Http\UI\Home(), 'execute']);
		}
	}
}