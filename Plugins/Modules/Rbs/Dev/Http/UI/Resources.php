<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Http\UI;

/**
 * @name \Rbs\Dev\Http\UI\Resources
 */
class Resources extends \Rbs\Ua\Http\UI\Resources
{
	/**
	 * @return string
	 */
	protected function getModule()
	{
		return 'Dev';
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerCommonLibAssets($assetsManager)
	{
		$plugin = $this->pluginManager->getPlugin('module', 'Rbs', 'Dev');
		$srcPath = $plugin->getAssetsPath() . '/lib';
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::SELF_FIRST);
		$targetBasePath = 'Rbs/Dev/lib';
		foreach ($iterator as $fileInfo)
		{
			/* @var $fileInfo \SplFileInfo */
			if ($fileInfo->isFile())
			{
				$targetPath = str_replace($srcPath, $targetBasePath, $fileInfo->getPathname());
				$asset = new \Assetic\Asset\FileAsset($fileInfo->getPathname());
				$asset->setTargetPath($targetPath);
				$assetsManager->set(preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
			}
		}
	}
}