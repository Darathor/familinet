<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev\Setup;

/**
 * @name \Rbs\Dev\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	use \Rbs\Ua\Setup\Dependency;

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Change/Events/ListenerAggregateClasses/Rbs_Dev', \Rbs\Dev\Events\SharedListeners::class);
		$configuration->addPersistentEntry('Change/Events/Commands/Rbs_Dev', \Rbs\Dev\Commands\Listeners::class);
		$configuration->addPersistentEntry('Rbs/Ua/Events/UaManager/Rbs_Dev', \Rbs\Dev\Events\UaManager\Listeners::class);

		$configuration->addPersistentEntry('Rbs/Dev/Resources/UI', true);
		$configuration->addPersistentEntry('Rbs/Dev/Resources/UI.common', false);
		$configuration->addPersistentEntry('Rbs/Dev/Resources/Theme', true);
		$configuration->addPersistentEntry('Rbs/Dev/Resources/Theme.base', false);

		$configuration->addPersistentEntry('Change/Events/Http/UA/UI/Rbs_Dev', \Rbs\Dev\Http\UI\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/UA/API/Rbs_Dev', \Rbs\Dev\Http\API\Listeners::class);

		$configuration->addPersistentEntry('Change/Session/Config/cookie_secure', false);
		$configuration->addPersistentEntry('Change/Session/Config/cookie_httponly', true);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		parent::executeThemeAssets($plugin, $pluginManager, $application);
		(new \Rbs\Ua\Setup\Compilation())->compileAngularControllersMapping($plugin, $application);
		$manager = new \Rbs\Dev\Http\UI\Resources('apiExplorer');
		$manager->setApplication($application)->setPluginManager($pluginManager);
		$assetManager = $manager->getNewAssetManager();
		$manager->registerCommonLibAssets($assetManager);
		$manager->registerAssets($assetManager);
		$manager->write($assetManager);
	}
}
