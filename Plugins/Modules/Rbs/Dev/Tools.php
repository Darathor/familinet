<?php
/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Dev;

/**
 * @name \Rbs\Dev\Tools
 */
class Tools
{
	/** @var \Change\Application */
	protected static $application;

	/**
	 * @return \Change\Logging\Logging
	 */
	public static function getLogging()
	{
		if (!self::$application)
		{
			self::init();
		}
		return self::$application->getLogging();
	}

	protected static function init()
	{
		self::$application = new \Change\Application();
	}
}