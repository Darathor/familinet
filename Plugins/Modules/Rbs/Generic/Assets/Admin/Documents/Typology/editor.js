(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsGenericTypologyNew', ['RbsChange.Models', Editor]);
	app.directive('rbsDocumentEditorRbsGenericTypologyEdit', ['RbsChange.Models', Editor]);

	function Editor(Models) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (scope.document.modelName) {
						scope.modelLabel = Models.getModelLabel(scope.document.modelName);
					}
				};

				scope.selectAll = function(group, contextName) {
					if (!angular.isObject(scope.document.visibilities[contextName].attributes)) {
						scope.document.visibilities[contextName].attributes = {};
					}
					angular.forEach (group.attributes, function (attribute) {
						scope.document.visibilities[contextName].attributes[attribute.id + ''] = true;
					});
				};

				scope.deselectAll = function(group, contextName) {
					if (!angular.isObject(scope.document.visibilities[contextName].attributes)) {
						scope.document.visibilities[contextName].attributes = {};
					}
					angular.forEach (group.attributes, function (attribute) {
						scope.document.visibilities[contextName].attributes[attribute.id + ''] = false;
					});
				};

				scope.checkDeleteContext = function (inlineDoc) {
					return !inlineDoc.locked;
				};
			}
		};
	}
})();