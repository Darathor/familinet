<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\Attribute
 */
class Attribute extends \Change\Documents\Attributes\Attribute
{
	/**
	 * @var integer
	 */
	protected $id;

	/**
	 * @var callable|null
	 */
	protected $formatter;

	/**
	 * @param \Rbs\Generic\Documents\Attribute|array|null $attribute
	 */
	public function __construct($attribute = null)
	{
		if ($attribute instanceof \Rbs\Generic\Documents\Attribute)
		{
			$this->fromDocument($attribute);
		}
		else
		{
			parent::__construct($attribute);
		}
	}

	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 */
	protected function fromDocument(\Rbs\Generic\Documents\Attribute $attribute)
	{
		$this->id = $attribute->getId();
		$this->name = $attribute->getName() ?: 'attr_' . $attribute->getId();
		$this->type = $attribute->getValueType();
		$this->title = $attribute->getCurrentLocalization()->getTitle();
		$this->description = $attribute->getCurrentLocalization()->getDescription();
		$this->renderingMode = $attribute->getRenderingMode();
		$this->localized = $attribute->getLocalizedValue();
		$this->indexed = $attribute->getIndexed();
		$this->formatter = $attribute->getAJAXFormatter();
	}

	/**
	 * @param array $attribute
	 */
	protected function fromArray(array $attribute)
	{
		$this->id = $attribute['id'] ?? null;
		parent::fromArray($attribute);
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$array = parent::toArray();
		$array['id'] = $this->getId();
		return $array;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return callable|null If callable, it should have two arguments: mixed $value and array $context
	 */
	public function getAJAXFormatter()
	{
		return $this->formatter;
	}

	/**
	 * @param callable|null $formatter If callable, it should have two arguments: mixed $value and array $context
	 */
	public function setAJAXFormatter($formatter)
	{
		$this->formatter = $formatter;
	}

	/**
	 * @param mixed $value
	 * @return mixed
	 */
	public function normalizeValue($value)
	{
		switch ($this->getType())
		{
			case self::TYPE_BOOLEAN:
				return (bool)$value;
				break;

			case self::TYPE_INTEGER:
				if (is_numeric($value))
				{
					return (int)$value;
				}
				break;

			case self::TYPE_FLOAT:
				if (is_numeric($value))
				{
					return (float)$value;
				}
				break;

			case self::TYPE_DOCUMENT_ID:
				if ($value instanceof \Change\Documents\AbstractDocument)
				{
					return $value->getId();
				}
				$value = (int)$value;
				return $value > 0 ? $value : null;
				break;

			case self::TYPE_DOCUMENT_ID_ARRAY:
				if (is_array($value))
				{
					$validValues = [];
					foreach ($value as $v)
					{
						if ($v instanceof \Change\Documents\AbstractDocument)
						{
							$validValues[] = $v->getId();
						}
						else
						{
							$v = (int)$v;
							if ($v > 0)
							{
								$validValues[] = $v;
							}
						}
					}

					if (count($validValues))
					{
						return $validValues;
					}
				}
				break;

			case self::TYPE_DATETIME:
				$v = $value;
				/** @noinspection NotOptimalIfConditionsInspection */
				if (is_array($v) && isset($v['date'], $v['timezone']) && $v['timezone'] instanceof \DateTimeZone)
				{
					$v = new \DateTime($v['date'], $v['timezone']);
				}
				elseif (is_string($v))
				{
					$v = new \DateTime($v);
				}

				if ($v instanceof \DateTime)
				{
					$v->setTimezone(new \DateTimeZone('UTC'));
					return $v->format(\DateTime::ATOM);
				}
				break;

			case self::TYPE_STRING:
				$value = (string)$value;
				if ($value !== '')
				{
					return $value;
				}
				break;

			case self::TYPE_RICHTEXT:
				$v = new \Change\Documents\RichtextProperty($value);
				if (!$v->isEmpty())
				{
					return $v->toArray();
				}
				break;

			case self::TYPE_JSON:
				return $value && is_array($value) ? $value : null;

			default:
				break;
		}
		return null;
	}

	/**
	 * @param mixed $value
	 * @return mixed
	 */
	public function toPHPValue($value)
	{
		if ($value)
		{
			switch ($this->getType())
			{
				case self::TYPE_DATETIME:
					return new \DateTime($value);
					break;

				case self::TYPE_RICHTEXT:
					return new \Change\Documents\RichtextProperty($value);
					break;
			}
		}
		elseif ($this->getType() === self::TYPE_DOCUMENT_ID_ARRAY)
		{
			return [];
		}
		return $value;
	}
}