<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\Context
 */
class Context extends \Change\Documents\Attributes\Context
{
	/**
	 * @param \Rbs\Generic\Documents\AttributeContext|array|null $context
	 */
	public function __construct($context = null)
	{
		if ($context instanceof \Rbs\Generic\Documents\AttributeContext)
		{
			$this->fromDocument($context);
		}
		else
		{
			parent::__construct($context);
		}
	}

	/**
	 * @param \Rbs\Generic\Documents\AttributeContext $context
	 */
	protected function fromDocument(\Rbs\Generic\Documents\AttributeContext $context)
	{
		$this->name = $context->getName();
		$this->title = $context->getCurrentLocalization()->getTitle();
		$this->exposedInAjaxAPI = $context->getExposedInAjaxAPI();
	}
}