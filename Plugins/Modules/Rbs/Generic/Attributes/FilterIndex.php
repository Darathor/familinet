<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\FilterIndex
 */
class FilterIndex implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'AttributeFilterIndex';

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Generic/Events/AttributeFilterIndex');
	}

	/**
	 * @api
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('save', function ($event) { $this->onSave($event); }, 5);
		$eventManager->attach('save', function ($event) { $this->onDoSave($event); }, 1);
	}

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param \Change\Application $application
	 */
	public function __construct(\Change\Db\DbProvider $dbProvider, \Change\Application $application)
	{
		$this->dbProvider = $dbProvider;
		$this->application = $application;
	}

	/**
	 * @return \Change\Db\DbProvider
	 */
	public function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider($dbProvider)
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @api
	 * @param integer $documentId
	 */
	public function delete($documentId)
	{
		$this->deleteValues($documentId);
	}

	/**
	 * @api
	 * @param integer $documentId
	 */
	public function save($documentId)
	{
		$args = $this->getEventManager()->prepareArgs(['documentId' => $documentId]);
		$this->getEventManager()->trigger('save', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onSave(\Change\Events\Event $event)
	{
		if ($event->getParam('done') !== null || $event->getParam('valuesToIndex') !== null)
		{
			return;
		}
		$documentId = $event->getParam('documentId');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$typology = $documentManager->getTypologyByDocument($documentId);
		$values = $documentManager->getAttributeValues($documentId);

		if ($values instanceof \Change\Documents\Attributes\AttributeValues)
		{
			$values = $values->getValues();
		}

		if (!$typology || !is_array($values) || !count($values))
		{
			$event->setParam('valuesToIndex', []);
			return;
		}

		$valuesToIndex = [];
		foreach ($values as $name => $value)
		{
			$attribute = $typology->getAttributeByName($name);
			if (!($attribute instanceof \Rbs\Generic\Attributes\Attribute))
			{
				continue;
			}

			$dispatchedValue = $this->dispatchValue($attribute, $value);
			if (!$dispatchedValue)
			{
				continue;
			}

			$attributeId = $attribute->getId();
			if (isset($valuesToIndex[$attributeId]))
			{
				$valuesToIndex[$attributeId] = array_merge($valuesToIndex[$attributeId], $dispatchedValue);
			}
			else
			{
				$valuesToIndex[$attributeId] = $dispatchedValue;
			}
		}
		$event->setParam('valuesToIndex', $valuesToIndex);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onDoSave(\Change\Events\Event $event)
	{
		$valuesToIndex = $event->getParam('valuesToIndex');
		if ($event->getParam('done') !== null || !is_array($valuesToIndex))
		{
			return;
		}

		$documentId = $event->getParam('documentId');
		if (!$valuesToIndex)
		{
			$this->delete($documentId);
			$event->setParam('done', true);
			return;
		}

		$defined = $this->getList($documentId);
		foreach ($valuesToIndex as $attributeId => $attributeValues)
		{
			$index = 0;
			$existingIndexes = isset($defined[$attributeId]) && is_array($defined[$attributeId]) ? $defined[$attributeId] : [];
			$nextIndex = count($existingIndexes) ? (max($existingIndexes) + 1) : 0;
			foreach ($attributeValues as $value)
			{
				if (count($existingIndexes) > $index)
				{
					$this->updateValue($documentId, $attributeId, $value, $existingIndexes[$index]);
					$index++;
				}
				else
				{
					$this->insertValue($documentId, $attributeId, $value, $nextIndex);
					$nextIndex++;
				}
			}

			if (count($existingIndexes) > $index)
			{
				$this->deleteValue($documentId, $attributeId, array_slice($existingIndexes, $index));
			}
		}

		$this->deleteValues($documentId, array_keys($valuesToIndex));
		$event->setParam('done', true);
	}

	/**
	 * @param integer $documentId
	 * @return array
	 */
	protected function getList($documentId)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('attribute_id'), $fb->column('index'));
			$qb->from($fb->table('rbs_generic_dat_attributes_index'));
			$qb->where($fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')));
		}
		$query = $qb->query();
		$query->bindParameter('document_id', $documentId);
		$result = [];
		foreach ($query->getResults($query->getRowsConverter()->addIntCol('attribute_id', 'index')) as $row)
		{
			$result[$row['attribute_id']][] = $row['index'];
		}
		return $result;
	}

	/**
	 * @param integer $documentId
	 * @param integer $attributeId
	 * @param array $value
	 * @param integer $index
	 */
	protected function insertValue($documentId, $attributeId, $value, $index)
	{
		list($integerValue, $floatValue, $dateValue, $stringValue) = $value;
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->insert($fb->table('rbs_generic_dat_attributes_index'),
				$fb->column('document_id'), $fb->column('attribute_id'), $fb->column('index'),
				$fb->column('integer_value'), $fb->column('float_value'), $fb->column('date_value'),
				$fb->column('string_value'));
			$qb->addValues($fb->integerParameter('documentId'), $fb->integerParameter('attributeId'),
				$fb->integerParameter('index'), $fb->integerParameter('integerValue'), $fb->decimalParameter('floatValue'),
				$fb->dateTimeParameter('dateValue'), $fb->parameter('stringValue'));
		}
		$is = $qb->insertQuery();
		$is->bindParameter('documentId', $documentId)
			->bindParameter('attributeId', $attributeId)
			->bindParameter('index', $index)
			->bindParameter('integerValue', $integerValue)
			->bindParameter('floatValue', $floatValue)
			->bindParameter('dateValue', $dateValue)
			->bindParameter('stringValue', $stringValue);
		$is->execute();
	}

	/**
	 * @param integer $documentId
	 * @param integer $attributeId
	 * @param array $value
	 * @param integer $index
	 */
	protected function updateValue($documentId, $attributeId, $value, $index)
	{
		list($integerValue, $floatValue, $dateValue, $stringValue) = $value;
		$qb = $this->getDbProvider()->getNewStatementBuilder(__METHOD__);
		if (!$qb->isCached())
		{
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->table('rbs_generic_dat_attributes_index'));
			$qb->assign($fb->column('integer_value'), $fb->integerParameter('integerValue'));
			$qb->assign($fb->column('float_value'), $fb->decimalParameter('floatValue'));
			$qb->assign($fb->column('date_value'), $fb->dateTimeParameter('dateValue'));
			$qb->assign($fb->column('string_value'), $fb->parameter('stringValue'));
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('document_id'), $fb->integerParameter('documentId')),
				$fb->eq($fb->column('attribute_id'), $fb->integerParameter('attributeId')),
				$fb->eq($fb->column('index'), $fb->integerParameter('index'))
			));
		}
		$uq = $qb->updateQuery();
		$uq->bindParameter('documentId', $documentId)
			->bindParameter('attributeId', $attributeId)
			->bindParameter('index', $index)
			->bindParameter('integerValue', $integerValue)
			->bindParameter('floatValue', $floatValue)
			->bindParameter('dateValue', $dateValue)
			->bindParameter('stringValue', $stringValue);
		$uq->execute();
	}

	/**
	 * @param integer $documentId
	 * @param integer $attributeId
	 * @param integer[] $indexes
	 */
	protected function deleteValue($documentId, $attributeId, $indexes = [])
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table('rbs_generic_dat_attributes_index'));
		if (count($indexes))
		{
			$in = [];
			foreach ($indexes as $id)
			{
				$in[] = $fb->number($id);
			}
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')),
				$fb->eq($fb->column('attribute_id'), $fb->integerParameter('attribute_id')),
				$fb->in($fb->column('index'), $in)
			));
		}
		else
		{
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')),
				$fb->eq($fb->column('attribute_id'), $fb->integerParameter('attribute_id'))
			));
		}
		$dq = $qb->deleteQuery();
		$dq->bindParameter('document_id', $documentId);
		$dq->bindParameter('attribute_id', $attributeId);
		$dq->execute();
	}

	/**
	 * @param integer $documentId
	 * @param integer[] $excludeAttrIds
	 */
	protected function deleteValues($documentId, $excludeAttrIds = [])
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->delete($fb->table('rbs_generic_dat_attributes_index'));
		if (count($excludeAttrIds))
		{
			$notIn = [];
			foreach ($excludeAttrIds as $id)
			{
				$notIn[] = $fb->number($id);
			}
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')),
				$fb->notIn($fb->column('attribute_id'), $notIn)
			));
		}
		else
		{
			$qb->where($fb->eq($fb->column('document_id'), $fb->integerParameter('document_id')));
		}
		$dq = $qb->deleteQuery();
		$dq->bindParameter('document_id', $documentId);
		$dq->execute();
	}

	/**
	 * @param \Rbs\Generic\Attributes\Attribute $attribute
	 * @param mixed $value
	 * @return array
	 */
	protected function dispatchValue($attribute, $value)
	{
		$result = [];
		if ($value === null || (is_array($value) && !count($value)))
		{
			return $result;
		}

		if (!is_array($value))
		{
			$value = [$value];
		}

		foreach ($value as $singleValue)
		{
			//integer_value, float_value, date_value, string_value
			switch ($attribute->getFilterType())
			{
				case 'integer':
					$result[] = [$singleValue, null, null, null];
					break;
				case 'float':
					$result[] = [null, $singleValue, null, null];
					break;
				case 'date':
					$result[] = [null, null, $singleValue, null];
					break;
				case 'string':
					$result[] = [null, null, null, $singleValue];
					break;
			}
		}

		return $result;
	}
}