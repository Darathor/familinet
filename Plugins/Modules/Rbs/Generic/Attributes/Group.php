<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\Group
 */
class Group extends \Change\Documents\Attributes\Group
{
	/**
	 * @param \Rbs\Generic\Documents\AttributeGroup|array|null $group
	 */
	public function __construct($group = null)
	{
		if ($group instanceof \Rbs\Generic\Documents\AttributeGroup)
		{
			$this->fromDocument($group);
		}
		else
		{
			parent::__construct($group);
		}
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Attribute|\Rbs\Generic\Documents\Attribute|array $attribute
	 * @return \Rbs\Generic\Attributes\Attribute|null
	 */
	public function getNewAttribute($attribute)
	{
		if ($attribute instanceof \Rbs\Generic\Attributes\Attribute)
		{
			return $attribute;
		}
		elseif ($attribute instanceof \Change\Documents\Attributes\Interfaces\Attribute)
		{
			return new \Rbs\Generic\Attributes\Attribute($attribute->toArray());
		}
		elseif (is_array($attribute) || $attribute instanceof \Rbs\Generic\Documents\Attribute)
		{
			return new \Rbs\Generic\Attributes\Attribute($attribute);
		}
		return null;
	}

	/**
	 * @param \Rbs\Generic\Documents\AttributeGroup $group
	 */
	protected function fromDocument(\Rbs\Generic\Documents\AttributeGroup $group)
	{
		$this->name = $group->getName();
		$this->title = $group->getCurrentLocalization()->getTitle();
		$this->setAttributes($group->getAttributes());
	}
}