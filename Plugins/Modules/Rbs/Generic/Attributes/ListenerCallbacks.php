<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\ListenerCallbacks
 */
class ListenerCallbacks
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function onIndexForFilters(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();

		$transactionManager = $applicationServices->getTransactionManager();
		$filterIndex = new \Rbs\Generic\Attributes\FilterIndex($applicationServices->getDbProvider(), $event->getApplication());
		$documentIds = $job->getArgument('toIndex');
		try
		{
			$transactionManager->begin();

			foreach ($documentIds as $documentId)
			{
				$filterIndex->save($documentId);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument|\Change\Documents\AbstractInline $document
	 * @param array $jsonDocument
	 * @param \Rbs\Generic\Json\Import $import
	 * @param \Change\Documents\DocumentCodeManager $documentCodeManager
	 */
	public function preSaveImport($document, $jsonDocument, $import, $documentCodeManager)
	{
		if (!($document instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}

		$contextId =  $import->getContextId();
		$visibilities = [];
		foreach ($document->getVisibilities() as $name => $context)
		{
			$visibilities[$name] = [];
			foreach ($context['attributes'] as $code => $visibility)
			{
				$docs = $documentCodeManager->getDocumentsByCode($code, $contextId);
				if (count($docs) && $docs[0] instanceof \Rbs\Generic\Documents\Attribute)
				{
					$visibilities[$name]['attributes'][(string)$docs[0]->getId()] = $visibility;
				}
			}
		}
		$document->setVisibilities($visibilities);
	}
}