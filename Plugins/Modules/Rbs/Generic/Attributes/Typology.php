<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Attributes;

/**
 * @name \Rbs\Generic\Attributes\Typology
 */
class Typology extends \Change\Documents\Attributes\Typology
{
	/**
	 * @param \Rbs\Generic\Documents\Typology|array|null $typology
	 */
	public function __construct($typology = null)
	{
		if ($typology instanceof \Rbs\Generic\Documents\Typology)
		{
			$this->fromDocument($typology);
		}
		else
		{
			parent::__construct($typology);
		}
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Group|\Rbs\Generic\Documents\AttributeGroup|array $group
	 * @return \Rbs\Generic\Attributes\Group|null
	 */
	public function getNewGroup($group)
	{
		if ($group instanceof \Rbs\Generic\Attributes\Group)
		{
			return $group;
		}
		if ($group instanceof \Change\Documents\Attributes\Interfaces\Group)
		{
			return new \Rbs\Generic\Attributes\Group($group->toArray());
		}
		if (\is_array($group) || $group instanceof \Rbs\Generic\Documents\AttributeGroup)
		{
			return new \Rbs\Generic\Attributes\Group($group);
		}
		return null;
	}

	/**
	 * @param \Change\Documents\Attributes\Interfaces\Group|\Rbs\Generic\Documents\AttributeGroup|array $context
	 * @return \Rbs\Generic\Attributes\Context|null
	 */
	public function getNewContext($context)
	{
		if ($context instanceof \Rbs\Generic\Attributes\Context)
		{
			return $context;
		}
		if ($context instanceof \Change\Documents\Attributes\Interfaces\Context)
		{
			return new \Rbs\Generic\Attributes\Context($context->toArray());
		}
		if (\is_array($context) || $context instanceof \Rbs\Generic\Documents\AttributeContext)
		{
			return new \Rbs\Generic\Attributes\Context($context);
		}
		return null;
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology $typology
	 */
	protected function fromDocument(\Rbs\Generic\Documents\Typology $typology)
	{
		$this->id = $typology->getId();
		$this->name = $typology->getName();
		$this->title = $typology->getCurrentLocalization()->getTitle();
		$this->modelName = $typology->getModelName();
		$this->setGroups($typology->getGroups());
		$this->setContexts($typology->getContexts());
		$this->setVisibilities($this->getVisibilitiesFromDocument($typology));
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology $typology
	 * @return array
	 */
	protected function getVisibilitiesFromDocument($typology)
	{
		/** @var \Rbs\Generic\Documents\Attribute[] $attributes */
		$attributes = [];
		foreach ($typology->getGroups() ?? [] as $group)
		{
			foreach ($group->getAttributes() ?? [] as $attribute)
			{
				$attributes[$attribute->getId()] = $attribute;
			}
		}

		$visibilities = [];
		foreach ($typology->getVisibilities() ?? [] as $contextName => $context)
		{
			if (!isset($context['attributes']) || !$context['attributes'])
			{
				continue;
			}

			$visibilities[$contextName] = ['attributes' => []];
			foreach ($context['attributes'] as $attributeId => $visible)
			{
				if (isset($attributes[$attributeId]))
				{
					$attribute = $attributes[$attributeId];
					$key = $attribute->getName() ?: ('attr_' . $attribute->getId());
					$visibilities[$contextName]['attributes'][$key] = $visible;
				}
			}
		}
		return $visibilities;
	}

	/**
	 * @param array $rawValues
	 * @param \Change\Documents\Attributes\AttributeValues $newValues
	 * @param string|null $LCID the current LCID or null if the document is not localized
	 * @return array
	 */
	public function normalizeValues($rawValues, $newValues, $LCID)
	{
		$normalizedValues = [];
		foreach ($this->getGroups() ?? [] as $group)
		{
			foreach ($group->getAttributes() ?? [] as $attribute)
			{
				if (!($attribute instanceof \Rbs\Generic\Attributes\Attribute))
				{
					continue;
				}

				$name = $attribute->getName();
				$key = (($LCID && $attribute->getLocalized()) ? $LCID : '_') . '.' . $attribute->getId();
				if ($newValues && $newValues->has($name))
				{
					$normalizedValues[$key] = $attribute->normalizeValue($newValues->get($name));
				}
				else
				{
					$normalizedValues[$key] = $rawValues[$key] ?? null;
				}
			}
		}

		if ($LCID)
		{
			foreach ($rawValues as $key => $value)
			{
				if (\strpos($key, '_.') !== 0 && \strpos($key, $LCID . '.') !== 0)
				{
					$normalizedValues[$key] = $value;
				}
			}
		}

		// Sort values to detect easily *real* changes.
		\ksort($normalizedValues);

		return $normalizedValues;
	}
}