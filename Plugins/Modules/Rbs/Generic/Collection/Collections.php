<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Collection;

use Change\I18n\I18nString;

/**
 * @name \Rbs\Generic\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addSortDirections(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'asc' => new I18nString($i18n, 'm.rbs.generic.front.ascending', ['ucf']),
				'desc' => new I18nString($i18n, 'm.rbs.generic.front.descending', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_SortDirections', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addPermissionRoles(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'*' => new I18nString($i18n, 'm.rbs.generic.admin.any_role', ['ucf']),
				'Consumer' => new I18nString($i18n, 'm.rbs.generic.admin.role_consumer', ['ucf']),
				'Creator' => new I18nString($i18n, 'm.rbs.generic.admin.role_creator', ['ucf']),
				'Editor' => new I18nString($i18n, 'm.rbs.generic.admin.role_editor', ['ucf']),
				'Publisher' => new I18nString($i18n, 'm.rbs.generic.admin.role_publisher', ['ucf']),
				'Administrator' => new I18nString($i18n, 'm.rbs.generic.admin.role_administrator', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_PermissionRoles', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addPermissionPrivileges(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();

			$modelsNames = $applicationServices->getModelManager()->getModelsNames();
			$modelsTitles = [];
			foreach ($modelsNames as $modelName)
			{
				$model = $applicationServices->getModelManager()->getModelByName($modelName);
				$modelsTitles[] = $model->getVendorName() . ' > ' . $model->getShortModuleName() . ' > '
						. $event->getApplicationServices()->getI18nManager()->trans($model->getLabelKey(), ['ucf']);
			}
			$modelsTitles[] = new I18nString($i18n, 'm.rbs.generic.admin.website_tree', ['ucf']);

			$modelsNames = $applicationServices->getModelManager()->getModelsNames();
			$modelsNames[] = 'Rbs_Website';
			$collection = array_combine($modelsNames, $modelsTitles);
			$collection['*'] = new I18nString($i18n, 'm.rbs.generic.admin.any_privilege', ['ucf']);
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_PermissionPrivileges', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addTimeZones(\Change\Events\Event $event)
	{
		$items = [];
		foreach (\DateTimeZone::listIdentifiers() as $timeZoneName)
		{
			$now = new \DateTime('now', new \DateTimeZone($timeZoneName));
			$items[$timeZoneName] = $timeZoneName . ' (' . $now->format('P') . ')';
		}

		$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_TimeZones', $items);
		$event->setParam('collection', $collection);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addLanguages(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$ucFirst = $event->getParam('ucFirst');
			$ucFirst = $ucFirst === 'true' || $ucFirst === true;
			$items = [];
			foreach ($applicationServices->getI18nManager()->getSupportedLCIDs() as $lcid)
			{
				$label = $applicationServices->getI18nManager()->transLCID($lcid);
				if ($ucFirst)
				{
					$label = \Change\Stdlib\StringUtils::ucfirst($label);
				}
				$items[$lcid] = $label;
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_Languages', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAddressFields(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$docQuery = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_AddressFields');
			$qb = $docQuery->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$query = $qb->addColumn($fb->alias($docQuery->getColumn('id'), 'id'))
				->addColumn($fb->alias($docQuery->getColumn('label'), 'label'))->query();

			$items = [];
			foreach ($query->getResults() as $row)
			{
				$items[$row['id']] = $row['label'];
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Collection_AddressFields', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addTypologies(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$modelName = $event->getParam('modelName');
			$docQuery = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Generic_Typology');
			if ($modelName)
			{
				$model = $applicationServices->getModelManager()->getModelByName($modelName);
				if ($model)
				{
					$modelNames = $model->getAncestorsNames();
					$modelNames[] = $modelName;
					$pb2 = $docQuery->getPredicateBuilder();
					$docQuery->andPredicates($pb2->in('modelName', $modelNames));
				}
			}
			$docQuery->addOrder('label');
			$qb = $docQuery->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$query = $qb->addColumn($fb->alias($docQuery->getColumn('id'), 'id'))
				->addColumn($fb->alias($docQuery->getColumn('label'), 'label'))->query();

			$items = [];
			foreach ($query->getResults() as $row)
			{
				$items[$row['id']] = $row['label'];
			}

			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_Typologies', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAttributesForModel(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$documentModel = $event->getParam('modelName');
		$valueTypeParam = $event->getParam('valueType');
		$valueTypeArray = $valueTypeParam ? explode(',', $valueTypeParam) : null;

		if ($applicationServices && is_string($documentModel))
		{
			$attributes = [];
			$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Generic_Typology');
			$builder = $query->dbQueryBuilder();
			$fb = $builder->getFragmentBuilder();
			$builder->andWhere($fb->eq($fb->getDocumentColumn('modelname'), $fb->string($documentModel)));
			$typologies = $query->getDocuments();
			/** @var \Rbs\Generic\Documents\Typology $typology */
			foreach ($typologies as $typology)
			{
				foreach ($typology->getGroups() as $group)
				{
					foreach ($group->getAttributes() as $attribute)
					{
						if (!$valueTypeArray || in_array($attribute->getValueType(), $valueTypeArray, null))
						{
							$attributes[$attribute->getId()] = $attribute->getLabel();
						}
					}
				}
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_AttributesForModel', $attributes);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAttributeValueTypes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$types = [
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY,
				\Change\Documents\Attributes\Interfaces\Attribute::TYPE_JSON
			];

			$items = [];
			foreach ($types as $type)
			{
				$items[$type] = new I18nString($i18n, 'm.rbs.generic.admin.attribute_type_' . strtolower($type), ['ucf']);
			}

			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_AttributeValueTypes', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAttributeCollections(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$docQuery = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Collection_Collection');
			$qb = $docQuery->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$query = $qb->addColumn($fb->alias($docQuery->getColumn('code'), 'code'))
				->addColumn($fb->alias($docQuery->getColumn('label'), 'label'))->query();
			$items = [];
			foreach ($query->getResults() as $row)
			{
				$items[$row['code']] = $row['label'];
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_AttributeCollections', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addAttributeRenderingModes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18nManager = $applicationServices->getI18nManager();
			$items = [];

			$valueType = $event->getParam('valueType');
			if ($valueType == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID
				|| $valueType == \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY
			)
			{
				$model =
					$event->getApplicationServices()->getModelManager()->getModelByName($event->getParam('documentType'));
				if ($model)
				{
					$compatibleNames = $model->getDescendantsNames();
					$compatibleNames[] = $model->getName();
					if (in_array('Rbs_Media_Image', $compatibleNames, true))
					{
						$items['imageFormatted'] =
							$i18nManager->trans('m.rbs.generic.admin.attribute_rendering_image_formatted', ['ucf']);
						$items['imageFull'] =
							$i18nManager->trans('m.rbs.generic.admin.attribute_rendering_image_full', ['ucf']);
						$items['file'] = $i18nManager->trans('m.rbs.generic.admin.attribute_rendering_file', ['ucf']);
					}
					elseif (in_array('Rbs_Media_Video', $compatibleNames, true))
					{
						$items['video'] = $i18nManager->trans('m.rbs.generic.admin.attribute_rendering_video', ['ucf']);
						$items['file'] = $i18nManager->trans('m.rbs.generic.admin.attribute_rendering_file', ['ucf']);
					}
					elseif (in_array('Rbs_Media_File', $compatibleNames, true))
					{
						$items['file'] = $i18nManager->trans('m.rbs.generic.admin.attribute_rendering_file', ['ucf']);
					}
					elseif (in_array('Rbs_Website_Text', $compatibleNames, true) || in_array('Rbs_Website_HtmlFragment', $compatibleNames, true))
					{
						$items['htmlFragment'] = $i18nManager->trans('m.rbs.generic.admin.attribute_rendering_html_fragment', ['ucf']);
					}
				}
			}

			$collection = new \Change\Collection\CollectionArray('Rbs_Generic_AttributeRenderingModes', $items);
			$event->setParam('collection', $collection);
		}
	}
}