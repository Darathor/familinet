<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Documents;

/**
 * @name \Rbs\Generic\Documents\Attribute
 */
class Attribute extends \Compilation\Rbs\Generic\Documents\Attribute
{
	/**
	 * @var  \Change\Collection\CollectionInterface|null|false
	 */
	protected $collection = false;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getCollection', [$this, 'onDefaultGetCollection'], 5);
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'documentType' && is_array($value))
		{
			$value = $value['name'];
		}
		return parent::processRestData($name, $value, $event);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$extraColumn = $event->getParam('extraColumn');
			if (in_array('valueTypeFormatted', $extraColumn))
			{
				$types =
					$event->getApplicationServices()->getCollectionManager()->getCollection('Rbs_Generic_AttributeValueTypes');
				$item = $types->getItemByValue($this->getValueType());
				if ($item)
				{
					$restResult->setProperty('valueTypeFormatted', $item->getLabel());
				}
				else
				{
					$restResult->setProperty('valueTypeFormatted', $this->getValueType());
				}
			}
		}
	}

	/**
	 * @return \Change\Collection\CollectionInterface|null
	 */
	protected function getCollection()
	{
		if ($this->collection === false)
		{
			$documentEvent = new \Change\Documents\Events\Event('getCollection', $this, []);
			$this->getEventManager()->triggerEvent($documentEvent);
			$collection = $documentEvent->getParam('collection');
			$this->collection = $collection instanceof \Change\Collection\CollectionInterface ? $collection : null;
		}
		return $this->collection;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultGetCollection(\Change\Documents\Events\Event $event)
	{
		if ($this !== $event->getDocument() || $event->getParam('collection') !== null || !$this->getCollectionCode())
		{
			return;
		}

		$collection = $event->getApplicationServices()->getCollectionManager()->getCollection($this->getCollectionCode());
		$event->setParam('collection', $collection);
	}

	/**
	 * @return callable|null
	 */
	public function getAJAXFormatter()
	{
		$collection = $this->getCollectionCode() ? $this->getCollection() : null;
		if (!$collection)
		{
			return null;
		}

		return function ($value, $context) use ($collection)
		{
			$item = $collection->getItemByValue($value);
			if (!$item)
			{
				return null;
			}

			if ($collection instanceof \Rbs\Collection\Documents\ObjectCollection
				&& $item instanceof \Rbs\Collection\Documents\ObjectCollectionItem)
			{
				$context['data']['itemsDefinition'] = $collection->getItemsDefinition();
				return $item->getAJAXData($context);
			}
			return ['common' => ['value' => $item->getValue(), 'title' => $item->getTitle()]];
		};
	}

	/**
	 * @return array
	 */
	public function getDistinctInputValues()
	{
		$extractor = new \Rbs\Generic\Attributes\DistinctValuesExtractor();
		return $extractor->execute($this, $this->getDbProvider());
	}
}
