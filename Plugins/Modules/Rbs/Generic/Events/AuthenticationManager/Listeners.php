<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\AuthenticationManager;

/**
 * @name \Rbs\Generic\Events\AuthenticationManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\User\AuthenticationManager::EVENT_LOGIN, static function (\Change\Events\Event $event)
		{
			(new \Rbs\User\Events\Login())->execute($event);
		}, 10);

		$this->listeners[] = $events->attach(\Change\User\AuthenticationManager::EVENT_LOGIN, static function ($event)
		{
			(new \Rbs\User\Events\Login())->saveLastLoginDate($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\User\AuthenticationManager::EVENT_BY_USER_ID, static function (\Change\Events\Event $event)
		{
			(new \Rbs\User\Events\Login())->execute($event);
		}, 10);

		$this->listeners[] = $events->attach(\Change\User\AuthenticationManager::EVENT_INVALIDATE_TOKENS, static function ($event)
		{
			(new \Rbs\User\Events\Login())->onInvalidateAutoLogin($event);
		});
	}
}