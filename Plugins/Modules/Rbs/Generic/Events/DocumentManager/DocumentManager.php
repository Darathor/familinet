<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\DocumentManager;

use Change\Documents\Attributes\Interfaces\Attribute as AttributeInterface;

/**
 * @name \Rbs\Generic\Events\DocumentManager\DocumentManager
 */
class DocumentManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetTypology($event)
	{
		if ($event->getParam('typology') !== null || !($typologyId = (int)$event->getParam('typologyId')))
		{
			return;
		}
		$typology = false;
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$documentManager->preLoad([[$typologyId, 'Rbs_Generic_Typology']]);
		$document = $documentManager->getDocumentInstance($typologyId);
		if ($document instanceof \Rbs\Generic\Documents\Typology)
		{
			$document->preLoadAttributes();
			$typology = new \Rbs\Generic\Attributes\Typology($document);
		}
		$event->setParam('typology', $typology?: null);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultGetAttributeValues($event)
	{
		if ($event->getParam('attributeValues') !== null)
		{
			return;
		}

		$rawValues = $event->getParam('rawValues');
		$document = $event->getParam('document');
		if (!($document instanceof \Change\Documents\AbstractDocument) || !is_array($rawValues) || !count($rawValues))
		{
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$typologyId = $event->getParam('typologyId');
		$typology = $documentManager->getTypology($typologyId);
		if (!($typology instanceof \Rbs\Generic\Attributes\Typology))
		{
			return;
		}

		$LCID = $documentManager->getLCID();
		$values = new \Change\Documents\Attributes\AttributeValues($LCID);

		$refDocIds = $event->getParam('refDocIds', []);

		/** @var \Rbs\Generic\Attributes\Attribute[] $attributes */
		$attributes = [];
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				if ($attribute instanceof \Rbs\Generic\Attributes\Attribute)
				{
					$attributes[$attribute->getId()] = $attribute;

					// For document id arrays, set the default value to empty array.
					if ($attribute->getType() == AttributeInterface::TYPE_DOCUMENT_ID_ARRAY)
					{
						$values->set($attribute->getName(), []);
					}
				}
			}
		}

		foreach ($rawValues as $key => $value)
		{
			list ($prefix, $id) = explode('.', $key);
			if (($prefix == '_' || $prefix == $LCID) && isset($attributes[$id]))
			{
				$attr = $attributes[$id];
				$attrVal = $attr->toPHPValue($value);
				if ($attrVal)
				{
					$attrType = $attr->getType();
					if ($attrType === AttributeInterface::TYPE_DOCUMENT_ID)
					{
						$refDocIds[] = (int)$attrVal;
					}
					elseif ($attrType === AttributeInterface::TYPE_DOCUMENT_ID_ARRAY)
					{
						foreach ($attrVal as $refDocId)
						{
							$refDocIds[] = (int)$refDocId;
						}
					}
				}
				$values->set($attr->getName(), $attrVal);
			}
		}
		if ($refDocIds)
		{
			$event->setParam('refDocIds', $refDocIds);
		}

		$event->setParam('attributeValues', $values);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onDefaultSaveAttributeValues($event)
	{
		if ($event->getParam('rawValues') !== null)
		{
			return;
		}

		$document = $event->getParam('document');
		if (!($document instanceof \Change\Documents\AbstractDocument))
		{
			return;
		}

		$typology = $event->getParam('typology');
		if (!($typology instanceof \Rbs\Generic\Attributes\Typology))
		{
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $document->getDocumentModel()->isLocalized() ? $documentManager->getLCID() : null;
		$values = $typology->normalizeValues($event->getParam('oldRawValues'), $event->getParam('values'), $LCID);
		$rawValues = $values;
		$event->setParam('rawValues', $rawValues);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetDisplayableDocument($event)
	{
		if ($event->getParam('displayableDocument') !== null)
		{
			return;
		}

		/** @var \Change\Documents\DocumentManager $documentManager */
		$documentManager = $event->getTarget();
		$document = $documentManager->getDocumentInstance($event->getParam('documentId'));
		if ($document instanceof \Change\Documents\AbstractDocument)
		{
			/** @var \Change\Http\Web\Event $httpEvent */
			$httpEvent = $event->getParam('httpEvent');
			$website = $httpEvent->getWebsite();
			$q = $documentManager->getNewQuery('Rbs_Website_StaticPage');
			$q->andPredicates($q->eq('displayDocument', $document));
			/** @var \Rbs\Website\Documents\StaticPage $staticPage */
			foreach ($q->getDocuments() as $staticPage)
			{
				$section = $staticPage->getSection();
				if ($section && $section->getWebsite() === $website)
				{
					$event->setParam('displayableDocument', $document);
					return;
				}
			}
		}
	}
}