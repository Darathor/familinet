<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Documents;

/**
 * @name \Rbs\Generic\Events\Documents\LockedContextSetter
 */
class LockedContextSetter
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 * @param string $contextName
	 * @param boolean $exposedInAjaxAPI
	 * @param string $label
	 * @param string $title
	 */
	public function set(\Change\Documents\Events\Event $event, $contextName, $exposedInAjaxAPI, $label, $title = null)
	{
		$document = $event->getDocument();
		if (!($document instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}

		$contexts = $event->getParam('contexts');

		$found = false;
		foreach ($contexts as $key => $context)
		{
			if ($context['name'] == $contextName)
			{
				$contexts[$key]['locked'] = true;
				$found = true;
				break;
			}
		}

		if (!$found)
		{
			$refLCID = $document->getRefLCID();
			$contexts[] = [
				'model' => 'Rbs_Generic_AttributeContext',
				'name' => $contextName,
				'exposedInAjaxAPI' => $exposedInAjaxAPI,
				'locked' => true,
				'refLCID' => $refLCID,
				'label' => $label,
				'LCID' => [
					$refLCID => [
						'title' => $title ?: $label,
						'LCID' => $refLCID
					]
				]
			];
		}

		$event->setParam('contexts', $contexts);
	}
}