<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Documents;

/**
 * @name \Rbs\Generic\Events\Documents\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$callback = function (\Change\Documents\Events\Event $event)
		{
			$website = $event->getDocument();
			if ($website instanceof \Rbs\Website\Documents\Website)
			{
				(new \Rbs\Website\Events\WebsiteResolver())->changed($event->getApplicationServices()->getCacheManager());
			}
		};
		$events->attach('Rbs_Website_Website', 'documents.created', $callback, 5);
		$events->attach('Rbs_Website_Website', 'documents.updated', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Website\Events\PageResolver())->resolve($event);
		};
		$events->attach('Documents', 'http.web.displayPage', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Workflow\Tasks\PublicationProcess\Start())->execute($event);
		};
		$events->attach('Documents', 'documents.created', $callback, 5);
		$events->attach('Documents', 'documents.localized.created', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Workflow\Tasks\CorrectionPublicationProcess\Start())->execute($event);
		};
		$events->attach('Documents', 'correction.created', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Workflow\Http\Rest\Actions\ExecuteTask())->addTasks($event);
		};
		$events->attach('Documents', 'updateRestResult', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Website\Events\PathRuleBuilder())->updatePathRules($event);
		};
		$events->attach('Documents', 'documents.updated', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Generic\Events\Documents\Documents())->onDefaultGetAJAXData($event);
		};
		$events->attach('Documents', 'getAJAXData', $callback, 1);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Generic\Events\Documents\Documents())->onDefaultSetLockedVisibilityContexts($event);
		};
		$events->attach('Documents', 'setLockedVisibilityContexts', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Seo\Documents\PublishableListeners())->onSave($event);
		};
		$events->attach('Documents', 'documents.create', $callback, 5);
		$events->attach('Documents', 'documents.update', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Seo\Documents\PublishableListeners())->onGetPublicationMetas($event);
		};
		$events->attach('Documents', 'getPublicationMetas', $callback, 5);

		$callback = function (\Change\Documents\Events\Event $event)
		{
			(new \Rbs\Seo\Documents\PublishableListeners())->onGetPublicationSitemap($event);
		};
		$events->attach('Documents', 'getPublicationSitemap', $callback, 5);
	}
}