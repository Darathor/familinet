<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Http\Ajax;

/**
 * @name \Rbs\Generic\Events\Http\Ajax\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->registerActions($event); });

		$callback = function (\Change\Http\Event $event) {
			(new \Rbs\User\Http\Ajax\Authentication())->authenticate($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_AUTHENTICATE, $callback, 10);

		$callback = function (\Change\Http\Event $event) {
			(new \Rbs\User\Http\Ajax\Authentication())->authenticateFromCookie($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_AUTHENTICATE, $callback, 5);

		$callback = function (\Change\Http\Event $event) {
			(new \Rbs\Website\Http\Ajax\V1\PreviewContext())->onSetPreviewContext($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_REQUEST, $callback, 1);
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function registerActions(\Change\Http\Event $event)
	{
		$actionPath = $event->getParam('actionPath');
		$request = $event->getRequest();
		if ('Rbs/User/Login' === $actionPath)
		{
			if ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->login($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/User/Logout' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->logout($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/User/Info' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->info($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/User/User/Profiles' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->getProfiles($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			elseif ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->setProfiles($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/User/RevokeToken' === $actionPath)
		{
			if ($request->isDelete())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->revokeToken($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_DELETE]));
			}
			$event->setAuthorization(
				function () use ($event) {
					return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
				}
			);
		}
		elseif ('Rbs/User/CheckEmailAvailability' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->checkEmailAvailability($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/User/User/AccountRequest' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->checkAccountRequest($event);
					}
				);
			}
			elseif ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->createAccountRequest($event);
					}
				);
			}
			elseif ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->confirmAccountRequest($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET,
					\Zend\Http\Request::METHOD_POST, \Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/User/User/ChangeEmail' === $actionPath)
		{
			if ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->createChangeEmailRequest($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			if ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->confirmChangeEmailRequest($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_POST,
					\Zend\Http\Request::METHOD_PUT]));
			}
		}

		elseif ('Rbs/User/User/ChangeMobilePhoneNumber' === $actionPath)
		{
			if ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->initChangeMobilePhoneRequest($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			elseif ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->validateChangeMobilePhoneRequest($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_POST,
					\Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/User/User/ResetPasswordRequest' === $actionPath)
		{
			if ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->createResetPasswordRequest($event);
					}
				);
			}
			elseif ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->confirmResetPasswordRequest($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_POST,
					\Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/User/User/ChangePassword' === $actionPath)
		{
			if ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\User())->changePassword($event);
					}
				);
				$event->setAuthorization(
					function () use ($event) {
						return $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser()->authenticated();
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/User/validateMobilePhone' === $actionPath)
		{
			if ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\User\Http\Ajax\Authentication())->validateMobilePhone($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_PUT]));
			}
		}
		elseif ('Rbs/Geo/Address/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->getList($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			elseif ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->addAddress($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET,
					\Zend\Http\Request::METHOD_POST]));
			}
		}
		elseif (\preg_match('#^Rbs/Geo/Address/(\d+)$#', $actionPath, $matches))
		{
			$event->setParam('addressId', (int)$matches[1]);
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->getAddress($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			elseif ($request->isPut())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->updateAddress($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			elseif ($request->isDelete())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->deleteAddress($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET,
					\Zend\Http\Request::METHOD_PUT, \Zend\Http\Request::METHOD_DELETE]));
			}
		}
		elseif ('Rbs/Geo/ValidateAddress' === $actionPath)
		{
			if ($request->isPost())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->validateAddress($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET,
					\Zend\Http\Request::METHOD_POST]));
			}
		}
		elseif ('Rbs/Geo/AddressFieldsCountries/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->getAddressFieldsCountries($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif (\preg_match('#^Rbs/Geo/AddressFields/(\d+)$#', $actionPath, $matches))
		{
			if ($request->isGet())
			{
				$event->setParam('addressFieldsId', (int)$matches[1]);
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->getAddressFieldsData($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/CityAutoCompletion/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->cityAutoCompletion($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/AddressCompletion/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Address())->addressCompletion($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/Points/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Points())->getList($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/Phone/NumberConfiguration/' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Phone())->getNumberConfiguration($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/Phone/ParsePhoneNumber' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Phone())->parsePhoneNumber($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif ('Rbs/Geo/CoordinatesByAddress' === $actionPath)
		{
			if ($request->isGet())
			{
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Geo\Http\Ajax\Points())->getCoordinatesByAddress($event);
					}
				);
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
		elseif (\preg_match('#^Document/(\d+)$#', $actionPath, $matches))
		{
			if ($request->isGet())
			{
				$event->setParam('documentId', (int)$matches[1]);
				$event->setAction(
					function (\Change\Http\Event $event) {
						(new \Rbs\Generic\Http\Ajax\V1\Document())->getData($event);
					}
				);
				$event->setAuthorization(function () { return true; });
			}
			else
			{
				$event->setResult($event->getController()->notAllowedError($request->getMethod(), [\Zend\Http\Request::METHOD_GET]));
			}
		}
	}
}