<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Http\Rest;

/**
 * @name \Rbs\Generic\Events\Http\Rest\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_REQUEST, static function (\Change\Events\Event $event) {
			$controller = $event->getTarget();
			if ($controller instanceof \Change\Http\Rest\V1\Controller)
			{
				$resolver = $controller->getActionResolver();
				if ($resolver instanceof \Change\Http\Rest\V1\Resolver)
				{
					$resolver->addResolverClasses('admin', \Rbs\Admin\Http\Rest\AdminResolver::class);
					$resolver->addResolverClasses('plugins', \Rbs\Plugins\Http\Rest\PluginsResolver::class);
					$resolver->addResolverClasses('user', \Rbs\User\Http\Rest\UserResolver::class);
					$resolver->addResolverClasses('ua', \Rbs\Ua\Http\Rest\UaResolver::class);
				}
			}
		});

		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->registerActions($event); }, 5);
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_RESULT, function ($event) { $this->registerResult($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function registerActions(\Change\Http\Event $event)
	{

		if ($event->getAction())
		{
			if ($event->getRequest()->isDelete())
			{
				$relativePath = $event->getParam('pathInfo');
				if (preg_match('#^resources/Rbs/Website/Topic/(\d+)$#', $relativePath, $matches))
				{
					$originalAction = $event->getAction();
					$event->setAction(static function ($event) use ($originalAction) {
						(new \Rbs\Website\Http\Rest\Actions\TopicSafeDelete($originalAction))->delete($event);
					});
				}
			}
			return;
		}

		$relativePath = $event->getParam('pathInfo');
		switch ($relativePath)
		{
			case 'Rbs/Website/FunctionsList' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Website\Http\Rest\Actions\FunctionsList())->execute($event);
				});
				break;
			case 'Rbs/Website/PagesForFunction' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Website\Http\Rest\Actions\PagesForFunction())->execute($event);
				});
				break;
			case 'Rbs/Website/InheritedFunctions' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Website\Http\Rest\Actions\InheritedFunctions())->execute($event);
				});
				break;
			case 'Rbs/Avatar' :
				$event->setAction(static function (\Change\Http\Event $event) {
					$event->setParam('size', $event->getRequest()->getQuery('size'));
					$event->setParam('email', $event->getRequest()->getQuery('email'));
					$event->setParam('userId', $event->getRequest()->getQuery('userId'));
					$event->setParam('params', $event->getRequest()->getQuery('params'));

					(new \Rbs\Media\Http\Rest\Actions\Avatar())->execute($event);
				});
				break;
			case 'Rbs/Geo/AddressLines' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Geo\Http\Rest\AddressLines())->execute($event);
				});
				break;
			case 'Rbs/Geo/CoordinatesByAddress' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Geo\Http\Rest\CoordinatesByAddress())->execute($event);
				});
				break;
			case 'Rbs/Mail/AddMailVariation' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Mail\Http\Rest\AddMailVariation())->execute($event);
				});
				break;
			case 'Rbs/Generic/DocumentCodeContextExist' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Generic\Http\Rest\DocumentCodeContextExist())->execute($event);
				});
				break;
			case 'Rbs/Generic/GetDocumentsByCodes' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Generic\Http\Rest\GetDocumentsByCodes())->execute($event);
				});
				break;
			case 'Rbs/Geo/addressFiltersDefinition' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Geo\Http\Rest\AddressFiltersDefinition())->execute($event);
				});
				break;
			case 'Rbs/Geo/phoneNumberConfiguration' :
				$event->setAction(static function (\Change\Http\Event $event) {
					(new \Rbs\Geo\Http\Ajax\Phone())->getNumberConfiguration($event);
				});
				break;
			case 'Rbs/Geo/parsePhoneNumber' :
				$event->setAction(static function (\Change\Http\Event $event) {
					$event->setParam('data', $event->getRequest()->getPost());
					(new \Rbs\Geo\Http\Ajax\Phone())->parsePhoneNumber($event);
				});
				break;
		}

		if ($event->getAction())
		{
			return;
		}

		if (preg_match('#^resources/Rbs/Media/Image/(\d+)/resize$#', $relativePath, $matches))
		{
			$event->setParam('documentId', (int)$matches[1]);
			$event->setAction(static function ($event) {
				(new \Rbs\Media\Http\Rest\Actions\Resize())->resize($event);
			});
			$event->setAuthorization(null);
			return;
		}
		if (preg_match('#^resources/Rbs/User/User/(\d+)/Profiles/$#', $relativePath, $matches))
		{
			$event->setParam('documentId', (int)$matches[1]);
			if ($event->getRequest()->isGet())
			{
				$event->setAction(static function ($event) {
					(new \Rbs\User\Http\Rest\Actions\Profiles())->execute($event);
				});
			}
			elseif ($event->getRequest()->isPut())
			{
				$event->setParam('profiles', $event->getRequest()->getPost('profiles', []));
				$event->setAction(static function ($event) {
					(new \Rbs\User\Http\Rest\Actions\Profiles())->update($event);
				});
			}

			return;
		}
		if (preg_match('#^resources/Rbs/User/User/(\d+)/LoginDates/$#', $relativePath, $matches))
		{
			$event->setParam('documentId', (int)$matches[1]);
			if ($event->getRequest()->isGet())
			{
				$event->setAction(static function ($event) {
					(new \Rbs\User\Http\Rest\Actions\LoginDates())->execute($event);
				});
			}

			return;
		}
		if (preg_match('#^resources/Rbs/Collection/Collection/(\d+)/InputValues/$#', $relativePath, $matches))
		{
			$event->setParam('documentId', (int)$matches[1]);
			$event->setAction(static function ($event) {
				(new \Rbs\Collection\Http\Rest\InputValues())->execute($event);
			});
			return;
		}
		if (preg_match('#^resources/Rbs/Collection/ObjectCollection/(\d+)/InputValues/$#', $relativePath, $matches))
		{
			$event->setParam('documentId', (int)$matches[1]);
			$event->setAction(static function ($event) {
				(new \Rbs\Collection\Http\Rest\InputValues())->execute($event);
			});
			return;
		}
		if (preg_match('#^resources/Rbs/Workflow/Task/(\d+)/(execute|executeAll)$#', $relativePath, $matches))
		{
			$task = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($matches[1]);
			if ($task instanceof \Rbs\Workflow\Documents\Task)
			{
				$event->setParam('modelName', $task->getDocumentModelName());
				$event->setParam('documentId', $task->getId());
				$event->setParam('executeAll', $matches[2] === 'executeAll');
				$event->setParam('task', $task);
				$event->setAction(static function ($event) {
					(new \Rbs\Workflow\Http\Rest\Actions\ExecuteTask())->executeTask($event);
				});

				$event->setAuthorization(static function ($event) {
					return (new \Rbs\Workflow\Http\Rest\Actions\ExecuteTask())->canExecuteTask($event);
				});
			}
			return;
		}
		if (preg_match('#^resources/Rbs/Tag/Tag/(\d+)/documents#', $relativePath, $matches))
		{
			if ($event->getParam('isDirectory'))
			{
				$method = $event->getRequest()->getMethod();
				$tagId = (int)$matches[1];
				$model = $event->getApplicationServices()->getModelManager()->getModelByName('Rbs_Tag_Tag');
				$tag = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($tagId, $model);
				if ($tag !== null)
				{
					$event->setParam('tagId', $tagId);
					$event->setParam('requestedModelName', $event->getRequest()->getQuery('model'));
					if ($method === \Change\Http\Rest\Request::METHOD_GET)
					{
						$event->setAction(static function ($event) {
							(new \Rbs\Tag\Http\Rest\Actions\GetTaggedDocuments())->execute($event);
						});
					}
				}
			}
			return;
		}
		if (preg_match('#^resources/([A-Z][a-z0-9]+)/([A-Z][a-z0-9]+)/([A-Z][A-Za-z0-9]+)/(\d+)(?:/([a-z]{2}_[A-Z]{2}))?/tags#',
			$relativePath, $matches)
		)
		{
			if ($event->getParam('isDirectory'))
			{
				$method = $event->getRequest()->getMethod();
				$modelName = $matches[1] . '_' . $matches[2] . '_' . $matches[3];
				$docId = (int)$matches[4];
				$model = $event->getApplicationServices()->getModelManager()->getModelByName($modelName);
				if ($model !== null)
				{
					$doc = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($docId, $model);
					if ($doc !== null)
					{
						$event->setParam('docId', $docId);
						$event->setParam('modelName', $modelName);
						if ($method === \Change\Http\Rest\Request::METHOD_GET)
						{
							$event->setAction(static function ($event) {
								(new \Rbs\Tag\Http\Rest\Actions\GetDocumentTags())->execute($event);
							});
						}
						elseif ($method === \Change\Http\Rest\Request::METHOD_POST)
						{
							$event->setParam('tags', $event->getRequest()->getPost()->get('ids'));
							$event->setAction(static function ($event) {
								(new \Rbs\Tag\Http\Rest\Actions\SetDocumentTags())->execute($event);
							});
						}
						elseif ($method === \Change\Http\Rest\Request::METHOD_PUT)
						{
							$event->setParam('addIds', $event->getRequest()->getPost()->get('addIds'));
							$event->setAction(static function ($event) {
								(new \Rbs\Tag\Http\Rest\Actions\AddDocumentTags())->execute($event);
							});
						}
					}
				}
			}
			return;
		}
		if (preg_match('#^resources/([A-Z][a-z0-9]+)/([A-Z][a-z0-9]+)/([A-Z][A-Za-z0-9]+)/(\d+)(?:/([a-z]{2}_[A-Z]{2}))?/preview#',
			$relativePath, $matches)
		)
		{
			$previewId = (int)$matches[4];
			$event->setParam('previewId', $previewId);
			if (count($matches) > 5 && !$event->getParam('LCID'))
			{
				$event->setParam('LCID', $matches[5]);
			}

			$modelName = $matches[1] . '_' . $matches[2] . '_' . $matches[3];
			$authorisation = static function (\Change\Http\Event $event) use ($previewId, $modelName) {
				return $event->getPermissionsManager()->isAllowed('Consumer', $previewId, $modelName);
			};
			$event->setAuthorization($authorisation);
			$event->setAction(static function ($event) {
				(new \Rbs\Website\Http\Rest\V1\Preview())->execute($event);
			});
			return;
		}
		if (preg_match('#^resources/Rbs/Simpleform/(\d+)/Export$#', $relativePath, $matches))
		{
			$event->setParam('formId', (int)$matches[1]);
			$event->setAction(static function ($event) {
				(new \Rbs\Simpleform\Http\Rest\AnswersExport())->execute($event);
			});
			return;
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	protected function registerResult(\Change\Http\Event $event)
	{
		$result = $event->getResult();
		if ($result instanceof \Change\Http\Rest\V1\Models\ModelResult)
		{
			$metaArray = $result->getMetas();
			if (isset($metaArray['name']) && !isset($metaArray['filtersDefinition']))
			{
				$filtersDefinition = [];

				$model = $event->getApplicationServices()->getModelManager()->getModelByName($metaArray['name']);
				if ($model)
				{
					$filtersDefinition = $event->getApplicationServices()->getModelManager()->getFiltersDefinition($model);
					if (count($filtersDefinition))
					{
						foreach ($filtersDefinition as $key => $definition)
						{
							if (!isset($definition['directiveName']) && isset($definition['config']['propertyType']))
							{
								switch ($definition['config']['propertyType'])
								{
									case \Change\Documents\Property::TYPE_FLOAT:
									case \Change\Documents\Property::TYPE_DECIMAL:
									case \Change\Documents\Property::TYPE_INTEGER:
										$filtersDefinition[$key]['directiveName'] = 'rbs-document-filter-property-number';
										break;
									case \Change\Documents\Property::TYPE_STRING:
										$filtersDefinition[$key]['directiveName'] = 'rbs-document-filter-property-string';
										break;
									case \Change\Documents\Property::TYPE_BOOLEAN:
										$filtersDefinition[$key]['directiveName'] = 'rbs-document-filter-property-boolean';
										break;
									case \Change\Documents\Property::TYPE_DATETIME:
									case \Change\Documents\Property::TYPE_DATE:
										$filtersDefinition[$key]['directiveName'] = 'rbs-document-filter-property-datetime';
										break;
									case \Change\Documents\Property::TYPE_DOCUMENT:
									case \Change\Documents\Property::TYPE_DOCUMENTID:
									case \Change\Documents\Property::TYPE_DOCUMENTARRAY:
										$filtersDefinition[$key]['directiveName'] = 'rbs-document-filter-property-document';
										if (isset($definition['config']['documentType']))
										{
											$filtersDefinition[$key]['config']['selectModel'] =
												json_encode(['name' => $definition['config']['documentType']]);
										}
										else
										{
											$filtersDefinition[$key]['config']['selectModel'] =
												json_encode(['editable' => true, 'abstract' => false]);
										}
										break;
								}
							}
						}

						$i18nManager = $event->getApplicationServices()->getI18nManager();
						array_unshift($filtersDefinition, ['name' => 'group', 'directiveName' => 'rbs-filter-group-and-or',
							'config' => [
								'listLabel' => $i18nManager->trans('m.rbs.admin.admin.group_filter', ['ucf']),
								'andLabel' => $i18nManager->trans('m.rbs.admin.admin.filter_group_and_title', ['ucf', 'lab']),
								'orLabel' => $i18nManager->trans('m.rbs.admin.admin.filter_group_or_title', ['ucf', 'lab'])
							]]);

						usort($filtersDefinition, static function ($a, $b) {
							$grpA = $a['config']['group'] ?? '';
							$grpB = $b['config']['group'] ?? '';
							if ($grpA === $grpB)
							{
								$labA = $a['config']['listLabel'] ?? '';
								$labB = $b['config']['listLabel'] ?? '';
								if ($labA === $labB)
								{
									return 0;
								}
								return strcmp($labA, $labB);
							}
							return strcmp($grpA, $grpB);
						});
					}
				}

				$result->setMeta('filtersDefinition', $filtersDefinition);
			}
		}
	}
}