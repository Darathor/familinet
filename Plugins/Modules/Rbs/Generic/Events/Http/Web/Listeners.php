<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Http\Web;

/**
 * @name \Rbs\Generic\Events\Http\Web\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Web\Event::EVENT_ACTION, [$this, 'registerActions'], 10);

		$callback = static function (\Change\Http\Web\Event $event) {
			(new \Rbs\User\Http\Ajax\Authentication())->authenticate($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\Event::EVENT_AUTHENTICATE, $callback, 10);

		$callback = static function (\Change\Http\Web\Event $event) {
			(new \Rbs\User\Http\Ajax\Authentication())->authenticateFromCookie($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\Event::EVENT_AUTHENTICATE, $callback, 5);

		$callback = static function (\Change\Http\Web\Event $event) {
			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$extension = new \Rbs\Generic\Presentation\Twig\Extension($event->getApplication(), $event->getApplicationServices(),
				$genericServices, $event->getUrlManager());

			$event->getApplication()->getSharedEventManager()->attach('TemplateManager', 'registerExtensions',
				function (\Change\Events\Event $event) use ($extension) {
					/** @var \ArrayObject $extensions */
					$extensions = $event->getParam('extensions');
					$extensions->append($extension);
				}, 5);

			$prefetchCache = $event->getApplicationServices()->getCacheManager()->isValidNamespace('prefetch');
			if ($prefetchCache)
			{
				$event->getApplicationServices()->getDocumentManager()->usePersistentCache(true);
			}

			$websiteResolver = new \Rbs\Website\Events\WebsiteResolver();
			$websiteResolver->resolve($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\Event::EVENT_REQUEST, $callback, 5);
	}

	/**
	 * @param \Change\Http\Web\Event $event
	 */
	public function registerActions(\Change\Http\Web\Event $event)
	{
		if ($event->getAction() === null)
		{
			$relativePath = $event->getParam('relativePath');
			if ($relativePath === 'robots.txt')
			{
				$action = static function ($event) {
					(new \Rbs\Seo\Http\Web\RobotsTxt())->execute($event);
				};
				$event->setAction($action);
				$event->stopPropagation();
				return;
			}

			if ($relativePath === 'Assets/Rbs/Seo/sitemap-index.xml')
			{
				$event->setParam('host', $event->getRequest()->getUri()->getHost());
				$action = static function ($event) {
					(new \Rbs\Seo\Http\Web\Sitemap())->getIndex($event);
				};
				$event->setAction($action);
				$event->stopPropagation();
				return;
			}

			if (\preg_match('/^Assets\/Rbs\/Seo\/([\d]+)\/([a-z]{2}_[A-Z]{2})\/([a-zA-Z0-9_\-\.]+\.xml)$/', $relativePath, $matches))
			{
				$event->setParam('websiteId', $matches[1]);
				$event->setParam('LCID', $matches[2]);
				$event->setParam('fileName', $matches[3]);
				$action = static function ($event) {
					(new \Rbs\Seo\Http\Web\Sitemap())->getSitemap($event);
				};
				$event->setAction($action);
				$event->stopPropagation();
				return;
			}
		}
	}
}