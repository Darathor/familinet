<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\JobManager;

use Change\Job\Event;

/**
 * @name \Rbs\Generic\Events\JobManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		// Core.
		$this->listeners[] = $events->attach('process_Change_Document_CleanUp', function (Event $event)
		{
			(new \Rbs\Seo\Job\DocumentCleanUp())->cleanUp($event);
			(new \Rbs\Workflow\Job\DocumentCleanUp())->cleanUp($event);
			(new \Rbs\Website\Job\DocumentCleanUp())->cleanUp($event);
			(new \Rbs\User\Job\ProfileCleanup())->cleanUp($event);
		}, 10);

		$this->listeners[] = $events->attach('process_Change_Correction_Filed',
			function ($event) { (new \Rbs\Workflow\Job\DocumentCleanUp())->onCorrectionFiled($event); }, 5);

		$this->listeners[] = $events->attach('process_Change_Document_LocalizedCleanUp',
			function (Event $event) { (new \Rbs\Workflow\Job\DocumentCleanUp())->localizedCleanUp($event); }, 5);

		// Generic.
		$this->listeners[] = $events->attach('process_Rbs_Generic_Attributes_IndexForFilters',
			function ($event) { (new \Rbs\Generic\Attributes\ListenerCallbacks())->onIndexForFilters($event); }, 5);

		// Notification.
		$this->listeners[] = $events->attach('process_Rbs_Notification_SendMails',
			function ($event) { (new \Rbs\Notification\Job\SendMails())->execute($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Notification_sendMailImmediately',
			function ($event) { (new \Rbs\Notification\Job\SendMails())->sendMailImmediately($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Notification_Send',
			function ($event) { (new \Rbs\Notification\Job\NotificationSend())->execute($event); }, 5);

		// Timeline.
		$this->listeners[] = $events->attach('process_Rbs_Timeline_SendMessageMail',
			function ($event) { (new \Rbs\Timeline\Job\SendMessageMail())->execute($event); }, 5);

		// Seo.
		$this->listeners[] = $events->attach('process_Rbs_Seo_GenerateSitemap',
			function ($event) { (new \Rbs\Seo\Job\GenerateSitemap())->execute($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Seo_SaveDeletedPathRules',
			function (Event $event) { (new \Rbs\Seo\Job\SaveDeletedPathRules())->execute($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Seo_Import404FromLogs',
			function (Event $event) { (new \Rbs\Seo\Job\Import404FromLogs())->execute($event); }, 5);
		$this->listeners[] = $events->attach('throw_Rbs_Seo_Import404FromLogs',
			function (Event $event) { (new \Rbs\Seo\Job\Import404FromLogs())->onThrow($event); }, 5);

		// User.
		$this->listeners[] = $events->attach('process_Rbs_User_CleanRequestTables',
			function ($event) { (new \Rbs\User\Job\CleanRequestTables())->execute($event); }, 5);
		$this->listeners[] = $events->attach('throw_Rbs_User_CleanRequestTables',
			function (Event $event) { (new \Rbs\User\Job\CleanRequestTables())->onThrow($event); }, 5);

		// Website.
		$this->listeners[] = $events->attach('process_Rbs_Website_GeneratePathRulesByModel',
			function ($event) { (new \Rbs\Website\Job\GeneratePathRulesByModel())->execute($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Website_TopicMoved',
			function ($event) { (new \Rbs\Seo\Job\TopicMoved())->execute($event); }, 5);

		// Simpleform.
		$this->listeners[] = $events->attach('process_Rbs_Simpleform_Response_Added',
			function ($event) { (new \Rbs\Simpleform\Job\ResponseAdded())->execute($event); }, 5);

		$this->listeners[] = $events->attach('process_Rbs_Simpleform_AnswersExport',
			function (Event $event) { (new \Rbs\Simpleform\Job\SimpleformAnswersExport())->execute($event); }, 5);

		// Workflow.
		$this->listeners[] = $events->attach('process_Rbs_Workflow_ExecuteDeadLineTask',
			function (Event $event) { (new \Rbs\Workflow\Job\ExecuteDeadLineTask())->execute($event); }, 5);
	}
}