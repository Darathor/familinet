<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Mail;

/**
 * @name \Rbs\Generic\Events\Mail\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach('buildHtmlContent', function ($event) { $this->onBuildHtmlContent($event); }, 10);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildHtmlContent($event)
	{
		$website = $event->getParam('website');
		$application = $event->getApplication();
		if (!($website instanceof \Change\Presentation\Interfaces\Website))
		{
			$mailConfig = $application->getConfiguration('Rbs/Mail');
			$defaultBaseUrl = $mailConfig['defaultBaseUrl'] ?? null;
			$defaultSender = $mailConfig['defaultSender'] ?? null;
			$website = new \Rbs\Mail\DefaultWebsite($defaultBaseUrl, $event->getParam('LCID'), $defaultSender);
			$event->setParam('website',  $website);
		}
		$applicationServices = $event->getApplicationServices();
		$templateManager = $applicationServices->getTemplateManager();
		$extension = new \Rbs\Mail\Presentation\Twig\Extension($website, $application, $applicationServices);
		$templateManager->addExtension($extension);
	}
}