<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\ModelManager;

/**
 * @name \Rbs\Generic\Events\ModelManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Generic\Events\ModelManager\ModelManager())->getFiltersDefinition($event);
			(new \Rbs\Tag\Events\ModelManager())->getFiltersDefinition($event);
		};
		$this->listeners[] = $events->attach('getFiltersDefinition', $callback, 1);

		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Generic\Events\ModelManager\ModelManager())->getRestriction($event);
			(new \Rbs\Tag\Events\ModelManager())->getRestriction($event);
		};
		$this->listeners[] = $events->attach('getRestriction', $callback, 1);
	}
}