<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\ModelManager;

/**
 * @name \Rbs\Generic\Events\ModelManager\ModelManager
 */
class ModelManager
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function getFiltersDefinition(\Change\Events\Event $event)
	{
		$model = $event->getParam('model');
		$filtersDefinition = $event->getParam('filtersDefinition');
		if (!($model instanceof \Change\Documents\AbstractModel) || !is_array($filtersDefinition))
		{
			return;
		}

		// Filter by typology.
		$filtersDefinition[] = $this->getTypologyDefinition($model->getName(), $event);

		// Add filters for each attributes available on the model.
		$attributes = $this->getAttributesForModelName($model->getName(), $event);
		foreach ($attributes as $attribute)
		{
			$definition = $this->getDefinition($attribute, $event);
			if (isset($definition['directiveName']))
			{
				$filtersDefinition[] = $definition;
			}
		}

		// On attributes, add a specific filter to filter them by model.
		if ($model->getName() === 'Rbs_Generic_Attribute')
		{
			$filtersDefinition[] = $this->getAttributeForModelDefinition($event);
		}

		$event->setParam('filtersDefinition', $filtersDefinition);
	}

	/**
	 * @param string $modelName
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	protected function getTypologyDefinition($modelName, $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$definition = [
			'name' => 'hasTypology',
			'directiveName' => 'data-rbs-document-filter-typology',
			'parameters' => ['modelName' => $modelName, 'restriction' => 'hasTypology'],
			'config' => [
				'label' => $i18nManager->trans('m.rbs.generic.admin.by_typology', ['ucf']),
				'listLabel' => $i18nManager->trans('m.rbs.generic.admin.find_by_typology', ['ucf']),
				'group' => $i18nManager->trans('m.rbs.admin.admin.common_filter_group', ['ucf'])
			]
		];

		return $definition;
	}

	/**
	 * @param string $modelName
	 * @param \Change\Events\Event $event
	 * @return \Rbs\Generic\Documents\Attribute[]
	 */
	protected function getAttributesForModelName($modelName, $event)
	{
		$attributes = [];
		$collectionManager = $event->getApplicationServices()->getCollectionManager();
		$typologyCollection = $collectionManager->getCollection('Rbs_Generic_Typologies', ['modelName' => $modelName]);
		if (!$typologyCollection || count($typologyCollection->getItems()) == 0)
		{
			return $attributes;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		foreach ($typologyCollection->getItems() as $item)
		{
			$typology = $documentManager->getDocumentInstance($item->getValue());
			if (!($typology instanceof \Rbs\Generic\Documents\Typology))
			{
				continue;
			}

			foreach ($typology->getGroups() as $group)
			{
				foreach ($group->getAttributes() as $attribute)
				{
					$attributes[$attribute->getId()] = $attribute;
				}
			}
		}
		return $attributes;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	protected function getAttributeForModelDefinition($event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$definition = [
			'name' => 'attributeForModel',
			'directiveName' => 'data-rbs-document-filter-model',
			'parameters' => ['restriction' => 'attributeForModel'],
			'config' => [
				'label' => $i18nManager->trans('m.rbs.generic.admin.attribute_for_model', ['ucf']),
				'listLabel' => $i18nManager->trans('m.rbs.generic.admin.find_attribute_for_model', ['ucf']),
				'group' => $i18nManager->trans('m.rbs.admin.admin.common_filter_group', ['ucf']),
				'modelFilters' => ['abstract' => false, 'editable' => true, 'inline' => false]
			]
		];

		return $definition;
	}

	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param \Change\Events\Event $event
	 * @return array
	 */
	protected function getDefinition($attribute, $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$collectionManager = $event->getApplicationServices()->getCollectionManager();

		$definition = [
			'name' => 'attribute' . $attribute->getId(),
			'parameters' => ['restriction' => 'attributeValue', 'attributeId' => $attribute->getId()],
			'config' => [
				'listLabel' => $attribute->getLabel(),
				'group' => $i18nManager->trans('m.rbs.generic.admin.attribute_filter_group', ['ucf']),
				'valueType' => $attribute->getValueType(),
				'label' => $attribute->getLabel()
			]
		];

		switch ($attribute->getValueType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
				$definition['directiveName'] = 'data-rbs-document-filter-property-boolean';
				break;

			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				$definition['directiveName'] = 'data-rbs-document-filter-property-number';
				break;

			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				$definition['directiveName'] = 'data-rbs-document-filter-property-datetime';
				break;

			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
				$definition['directiveName'] = 'data-rbs-document-filter-property-string';
				break;

			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				$definition['directiveName'] = 'data-rbs-document-filter-property-document';
				$definition['config']['selectModel'] = $attribute->getDocumentType();
				break;
		}

		if ($attribute->getCollectionCode())
		{
			$collection = $collectionManager->getCollection($attribute->getCollectionCode());
			if ($collection)
			{
				$values = [];
				foreach ($collection->getItems() as $item)
				{
					$values[] = ['value' => $item->getValue(), 'label' => $item->getLabel()];
				}
				if (count($values))
				{
					$definition['config']['possibleValues'] = $values;
				}
			}
		}
		return $definition;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function getRestriction(\Change\Events\Event $event)
	{
		$filter = $event->getParam('filter');
		if (!isset($filter['parameters']) || !is_array($filter['parameters']))
		{
			return;
		}

		$parameters = $filter['parameters'];
		$restriction = $parameters['restriction'] ?? null;
		if ($restriction === 'attributeForModel')
		{
			$parameters += ['operator' => 'eq', 'value' => null];
			$value = $parameters['value'];
			if (is_string($value))
			{
				$attributes = $this->getAttributesForModelName($value, $event);
				$attributeIds = [];
				foreach ($attributes as $attribute)
				{
					$attributeIds[] = $attribute->getId();
				}

				/** @var $documentQuery \Change\Documents\Query\Query */
				$documentQuery = $event->getParam('documentQuery');
				$fragmentBuilder = $documentQuery->getFragmentBuilder();
				if (count($attributeIds))
				{
					$restriction = $fragmentBuilder->in($documentQuery->getColumn('id'), $attributeIds);
				}
				else
				{
					$restriction = $fragmentBuilder->eq($documentQuery->getColumn('id'), $fragmentBuilder->number(0));
				}
				$event->setParam('restriction', $restriction);
				$event->stopPropagation();
			}
		}
		elseif ($restriction === 'attributeValue')
		{
			$parameters += ['attributeId' => null, 'operator' => null, 'value' => null];
			$attributeId = $parameters['attributeId'];
			$attribute = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($attributeId);
			if (!($attribute instanceof \Rbs\Generic\Documents\Attribute))
			{
				return;
			}

			$operator = $parameters['operator'];
			$value = $parameters['value'];
			if ($operator === 'isNull' || $operator === 'isNotNull' || (isset($value) && is_string($operator)))
			{
				/** @var $documentQuery \Change\Documents\Query\Query */
				$documentQuery = $event->getParam('documentQuery');
				$fragmentBuilder = $documentQuery->getFragmentBuilder();
				$restriction = $this->buildPredicate($documentQuery, $fragmentBuilder, $attribute, $operator, $value);
				if ($restriction)
				{
					$event->setParam('restriction', $restriction);
					$event->stopPropagation();
				}
			}
		}
		elseif ($restriction === 'hasTypology')
		{
			$parameters += ['operator' => null, 'value' => null];
			$operator = $parameters['operator'];
			$value = $parameters['value'];
			if ($operator === 'isNull' || $operator === 'isNotNull' || (isset($value) && is_string($operator)))
			{
				/** @var $documentQuery \Change\Documents\Query\Query */
				$documentQuery = $event->getParam('documentQuery');
				$fragmentBuilder = $documentQuery->getFragmentBuilder();
				$restriction = $this->buildTypologyPredicate($documentQuery, $fragmentBuilder, $operator, $value);
				if ($restriction)
				{
					$event->setParam('restriction', $restriction);
					$event->stopPropagation();
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Query\Query $documentQuery
	 * @param \Change\Db\Query\SQLFragmentBuilder $fragmentBuilder
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param string $operator
	 * @param string|null $value
	 * @return \Change\Db\Query\Predicates\Exists|null
	 */
	protected function buildPredicate($documentQuery, $fragmentBuilder, $attribute, $operator, $value)
	{
		$valueType = $attribute->getValueType();
		$attributeTable = $fragmentBuilder->table('rbs_generic_dat_attributes_index');
		switch ($valueType)
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				$valueColumn = $fragmentBuilder->column('integer_value', $attributeTable);
				$paramType = \Change\Documents\Property::TYPE_INTEGER;
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				$valueColumn = $fragmentBuilder->column('float_value', $attributeTable);
				$paramType = \Change\Documents\Property::TYPE_FLOAT;
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				$valueColumn = $fragmentBuilder->column('date_value', $attributeTable);
				$paramType = \Change\Documents\Property::TYPE_DATETIME;
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
				$valueColumn = $fragmentBuilder->column('string_value', $attributeTable);
				$paramType = \Change\Documents\Property::TYPE_STRING;
				break;
			default:
				return null;
		}

		$exists = true;
		switch ($operator)
		{
			case 'eq':
				$valueRestriction = $fragmentBuilder->eq($valueColumn, $documentQuery->getValueAsParameter($value, $paramType));
				break;
			case 'neq':
				$valueRestriction = $fragmentBuilder->neq($valueColumn, $documentQuery->getValueAsParameter($value, $paramType));
				break;
			case 'contains':
				$valueRestriction = $fragmentBuilder->like($valueColumn, $documentQuery->getValueAsParameter($value, $paramType),
					\Change\Db\Query\Predicates\Like::ANYWHERE);
				break;
			case 'beginsWith':
				$valueRestriction = $fragmentBuilder->like($valueColumn, $documentQuery->getValueAsParameter($value, $paramType),
					\Change\Db\Query\Predicates\Like::BEGIN);
				break;
			case 'endsWith':
				$valueRestriction = $fragmentBuilder->like($valueColumn, $documentQuery->getValueAsParameter($value, $paramType),
					\Change\Db\Query\Predicates\Like::END);
				break;
			case 'gte':
				$valueRestriction = $fragmentBuilder->gte($valueColumn, $documentQuery->getValueAsParameter($value, $paramType));
				break;
			case 'lte':
				$valueRestriction = $fragmentBuilder->lte($valueColumn, $documentQuery->getValueAsParameter($value, $paramType));
				break;
			case 'isNull':
				$exists = false;
				break;
			case 'isNotNull':
				break;
			default:
				return null;
		}

		$sq = new \Change\Db\Query\SelectQuery();
		$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
		$sq->setFromClause(new \Change\Db\Query\Clauses\FromClause($attributeTable));
		$docEq = $fragmentBuilder->eq($documentQuery->getColumn('id'), $fragmentBuilder->column('document_id', $attributeTable));
		$attrEq = $fragmentBuilder->eq($fragmentBuilder->column('attribute_id', $attributeTable),
			$documentQuery->getValueAsParameter($attribute->getId(), \Change\Documents\Property::TYPE_INTEGER));

		if (isset($valueRestriction))
		{
			$sq->setWhereClause(
				new \Change\Db\Query\Clauses\WhereClause($fragmentBuilder->logicAnd($docEq, $attrEq, $valueRestriction))
			);
		}
		else
		{
			$sq->setWhereClause(
				new \Change\Db\Query\Clauses\WhereClause($fragmentBuilder->logicAnd($docEq, $attrEq))
			);
		}

		return $exists ? $fragmentBuilder->exists($sq) : $fragmentBuilder->notExists($sq);
	}

	/**
	 * @param \Change\Documents\Query\Query $documentQuery
	 * @param \Change\Db\Query\SQLFragmentBuilder $fragmentBuilder
	 * @param string $operator
	 * @param string|null $value
	 * @return \Change\Db\Query\Predicates\Exists|null
	 */
	protected function buildTypologyPredicate($documentQuery, $fragmentBuilder, $operator, $value)
	{
		$attributeTable = $fragmentBuilder->table('change_document_attributes');
		$valueColumn = $fragmentBuilder->column('typology_id', $attributeTable);

		$exists = true;
		switch ($operator)
		{
			case 'eq':
				$valueRestriction = $fragmentBuilder->eq($valueColumn, $documentQuery->getValueAsParameter($value,
					\Change\Documents\Property::TYPE_INTEGER));
				break;
			case 'neq':
				$valueRestriction = $fragmentBuilder->neq($valueColumn, $documentQuery->getValueAsParameter($value,
					\Change\Documents\Property::TYPE_INTEGER));
				break;
			case 'isNull':
				$exists = false;
				break;
			case 'isNotNull':
				break;
			default:
				return null;
		}

		$sq = new \Change\Db\Query\SelectQuery();
		$sq->setSelectClause(new \Change\Db\Query\Clauses\SelectClause());
		$sq->setFromClause(new \Change\Db\Query\Clauses\FromClause($attributeTable));
		$docEq = $fragmentBuilder->eq($documentQuery->getColumn('id'),
			$fragmentBuilder->column('document_id', $attributeTable));

		if (isset($valueRestriction))
		{
			$sq->setWhereClause(
				new \Change\Db\Query\Clauses\WhereClause($fragmentBuilder->logicAnd($docEq, $valueRestriction))
			);
		}
		else
		{
			$sq->setWhereClause(
				new \Change\Db\Query\Clauses\WhereClause($docEq)
			);
		}

		return $exists ? $fragmentBuilder->exists($sq) : $fragmentBuilder->notExists($sq);
	}
} 