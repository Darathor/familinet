<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\NotificationManager;

/**
 * @name \Rbs\Generic\Events\NotificationManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$notificationEvents = new \Rbs\Generic\Notification\NotificationEvents();
		$events->attach('resolveNotification', [$notificationEvents, 'onResolveNotification'], 1);
		$events->attach('initContext', [$notificationEvents, 'onInitContext'], 1);
		$events->attach('getSubstitutionLabels', [$notificationEvents, 'onGetSubstitutionLabels'], 1);
		$events->attach('buildSubstitutionValues', [$notificationEvents, 'onBuildSubstitutionValues'], 1);
	}
}