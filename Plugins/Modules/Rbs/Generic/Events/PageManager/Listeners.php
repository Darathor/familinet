<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\PageManager;

/**
 * @name \Rbs\Generic\Events\PageManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Presentation\Pages\PageManager::EVENT_GET_FUNCTIONS, static function ($event) {
			(new \Rbs\Generic\Presentation\PageFunctions())->addFunctions($event);
		}, 5);

		$this->listeners[] = $events->attach(\Change\Presentation\Pages\PageManager::EVENT_GET_PAGE_RESULT, static function ($event) {
			(new \Rbs\User\Presentation\PageManager())->addUserContext($event);
		}, 10);
	}
}
