<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\PathRuleManager;

/**
 * @name \Rbs\Generic\Events\PathRuleManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Seo\Std\PathTemplateComposer())->onPopulatePathRule($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\PathRuleManager::EVENT_POPULATE_PATH_RULE, $callback, 15);

		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Website\Events\PageResolver())->onPopulatePathRule($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\PathRuleManager::EVENT_POPULATE_PATH_RULE, $callback, 10);

		$callback = function (\Change\Events\Event $event) {
			(new \Rbs\Seo\Std\PathTemplateComposer())->cleanRelativePath($event);
		};
		$this->listeners[] = $events->attach(\Change\Http\Web\PathRuleManager::EVENT_POPULATE_PATH_RULE, $callback, 1);
	}
}