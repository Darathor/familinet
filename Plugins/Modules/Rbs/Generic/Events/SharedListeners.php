<?php
/**
 * Copyright (C) 2014 Ready Business System, Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events;

/**
 * @name \Rbs\Generic\Events\SharedListeners
 */
class SharedListeners
{
	/**
	 * Attach one or more listeners
	 * Implementors may add an optional $priority argument; the SharedEventManager
	 * implementation will pass this to the aggregate.
	 * @param \Zend\EventManager\SharedEventManagerInterface $events
	 */
	public function attachShared(\Zend\EventManager\SharedEventManagerInterface $events)
	{
		$events->attach('Application', 'setServices', function ($event)
		{
			if ($event instanceof \Change\Events\Event)
			{
				$genericServices = new \Rbs\Generic\GenericServices($event->getApplication(), $event->getApplicationServices());
				$event->getServices()->set('genericServices', $genericServices);
			}
			return true;
		}, 9998);
	}
}