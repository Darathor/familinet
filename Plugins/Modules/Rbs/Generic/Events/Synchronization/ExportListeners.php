<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\Synchronization;

/**
 * @name \Rbs\Generic\Events\Synchronization\ExportListeners
 * @ignore
 */
class ExportListeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$export = new \Rbs\Generic\Synchronization\Export();

		$this->listeners[] = $events->attach(\Change\Synchronization\ExportEngine::EVENT_RESOLVE,
			function (\Change\Synchronization\ExportEvent $event) use ($export)
			{
				$export->onResolve($event);
			}, 10);

		$this->listeners[] = $events->attach(\Change\Synchronization\ExportEngine::EVENT_POPULATE,
			function (\Change\Synchronization\ExportEvent $event) use ($export)
			{
				$export->onPopulate($event);
			}, 10);
	}
}