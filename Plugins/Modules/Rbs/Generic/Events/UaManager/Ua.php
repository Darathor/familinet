<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\UaManager;

/**
 * @name \Rbs\Generic\Events\UaManager\Ua
 */
class Ua
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetResources(\Change\Events\Event $event)
	{
		if ($event->getParam('resources') || $event->getParam('vendorName') !== 'Rbs')
		{
			return;
		}

		if ($event->getParam('moduleName') === 'Seo')
		{
			$event->setParam('resources', new \Rbs\Seo\Http\UI\Resources($event->getParam('applicationName')));
		}

	}
}