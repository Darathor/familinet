<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Events\WorkflowManager;

/**
 * @name \Rbs\Generic\Events\WorkflowManager\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			$resolver = new \Rbs\Workflow\Events\WorkflowResolver();
			$resolver->examine($event);
		};
		$this->listeners[] = $events->attach(\Change\Workflow\WorkflowManager::EVENT_EXAMINE, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$resolver = new \Rbs\Workflow\Events\WorkflowResolver();
			$resolver->process($event);
		};
		$this->listeners[] = $events->attach(\Change\Workflow\WorkflowManager::EVENT_PROCESS, $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			(new \Rbs\Workflow\Http\Rest\Actions\ExecuteTask())->executeAll($event);
		};
		$this->listeners[] = $events->attach(\Change\Workflow\WorkflowManager::EVENT_EXECUTE_ALL, $callback, 5);
	}
}