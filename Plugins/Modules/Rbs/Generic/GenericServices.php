<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic;

/**
 * @name \Rbs\Generic\GenericServices
 */
class GenericServices extends \Zend\ServiceManager\ServiceManager
{
	use \Change\Services\ServicesCapableTrait;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	public function setApplicationServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
		return $this;
	}

	/**
	 * @return \Change\Services\ApplicationServices
	 */
	protected function getApplicationServices()
	{
		return $this->applicationServices;
	}

	/**
	 * @return array<alias => className>
	 */
	protected function loadInjectionClasses()
	{
		$classes = $this->getApplication()->getConfiguration('Rbs/Generic/Services');
		return is_array($classes) ? $classes : [];
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Services\ApplicationServices $applicationServices
	 */
	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices)
	{
		$this->setApplication($application);
		$this->setApplicationServices($applicationServices);

		parent::__construct(['shared_by_default' => true]);

		// SeoManager: Application, DocumentManager, TransactionManager, DbProvider
		$class = $this->getInjectedClassName('SeoManager', \Rbs\Seo\SeoManager::class);
		$this->setAlias('SeoManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Seo\SeoManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setTransactionManager($this->applicationServices->getTransactionManager())
				->setDbProvider($this->applicationServices->getDbProvider());
			return $manager;
		});

		// AvatarManager: Application
		$class = $this->getInjectedClassName('AvatarManager', \Rbs\Media\Avatar\AvatarManager::class);
		$this->setAlias('AvatarManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Media\Avatar\AvatarManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		// FieldManager: Application, ConstraintsManager
		$class = $this->getInjectedClassName('FieldManager', \Rbs\Simpleform\Field\FieldManager::class);
		$this->setAlias('FieldManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Simpleform\Field\FieldManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setConstraintsManager($this->applicationServices->getConstraintsManager());
			return $manager;
		});

		// SecurityManager: Application
		$class = $this->getInjectedClassName('SecurityManager', \Rbs\Simpleform\Security\SecurityManager::class);
		$this->setAlias('SecurityManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Simpleform\Security\SecurityManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		// GeoManager: Application
		$class = $this->getInjectedClassName('GeoManager', \Rbs\Geo\GeoManager::class);
		$this->setAlias('GeoManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Geo\GeoManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application);
			return $manager;
		});

		// AdminManager: Application, i18nManager, ModelManager, PluginManager
		$class = $this->getInjectedClassName('AdminManager', \Rbs\Admin\AdminManager::class);
		$this->setAlias('AdminManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Admin\AdminManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setI18nManager($this->applicationServices->getI18nManager())
				->setModelManager($this->applicationServices->getModelManager())
				->setPluginManager($this->applicationServices->getPluginManager());
			return $manager;
		});

		// UserManager: Application, DbProvider, DocumentManager
		$class = $this->getInjectedClassName('UserManager', \Rbs\User\UserManager::class);
		$this->setAlias('UserManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\User\UserManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setDbProvider($this->applicationServices->getDbProvider())
				->setDocumentManager($this->applicationServices->getDocumentManager());
			return $manager;
		});

		// NotificationManager: Application, CacheManager, DocumentManager, I18nManager, AuthenticationManager, JobManager, MailManager, SmsManager, SviManager
		$class = $this->getInjectedClassName('NotificationManager', \Rbs\Generic\Notification\Manager::class);
		$this->setAlias('NotificationManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Generic\Notification\Manager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setCacheManager($this->applicationServices->getCacheManager())
				->setDocumentManager($this->applicationServices->getDocumentManager())
				->setI18nManager($this->applicationServices->getI18nManager())
				->setAuthenticationManager($this->applicationServices->getAuthenticationManager())
				->setJobManager($this->applicationServices->getJobManager())
				->setMailManager($this->applicationServices->getMailManager())
				->setSmsManager($this->applicationServices->getSmsManager())
				->setSviManager($this->applicationServices->getSviManager());
			return $manager;
		});

		// UaManager: Application
		$class = $this->getInjectedClassName('UaManager', \Rbs\Ua\UaManager::class);
		$this->setAlias('UaManager', $class);
		$this->setFactory($class, function (\Interop\Container\ContainerInterface $container, $requestedName)
		{
			/** @var \Rbs\Ua\UaManager $manager */
			$manager = new $requestedName;
			$manager->setApplication($this->application)
				->setI18nManager($this->applicationServices->getI18nManager())
				->setPluginManager($this->applicationServices->getPluginManager())
				->setModelManager($this->applicationServices->getModelManager());
			return $manager;
		});
	}

	/**
	 * @api
	 * @return \Rbs\Seo\SeoManager
	 */
	public function getSeoManager()
	{
		return $this->get('SeoManager');
	}

	/**
	 * @api
	 * @return \Rbs\Media\Avatar\AvatarManager
	 */
	public function getAvatarManager()
	{
		return $this->get('AvatarManager');
	}

	/**
	 * @api
	 * @return \Rbs\Simpleform\Field\FieldManager
	 */
	public function getFieldManager()
	{
		return $this->get('FieldManager');
	}

	/**
	 * @api
	 * @return \Rbs\Simpleform\Security\SecurityManager
	 */
	public function getSecurityManager()
	{
		return $this->get('SecurityManager');
	}

	/**
	 * @api
	 * @return \Rbs\Geo\GeoManager
	 */
	public function getGeoManager()
	{
		return $this->get('GeoManager');
	}

	/**
	 * @api
	 * @return \Rbs\Admin\AdminManager
	 */
	public function getAdminManager()
	{
		return $this->get('AdminManager');
	}

	/**
	 * @api
	 * @return \Rbs\User\UserManager
	 */
	public function getUserManager()
	{
		return $this->get('UserManager');
	}

	/**
	 * @api
	 * @return \Rbs\Generic\Notification\Manager
	 */
	public function getNotificationManager()
	{
		return $this->get('NotificationManager');
	}

	/**
	 * @api
	 * @return \Rbs\Ua\UaManager
	 */
	public function getUaManager()
	{
		return $this->get('UaManager');
	}
}