<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Notification;

/**
 * @name \Rbs\Generic\Notification\NotificationEvents
 */
class NotificationEvents
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onResolveNotification(\Change\Events\Event $event)
	{
		if ($event->getParam('notification') !== null)
		{
			return;
		}
		$configuration = $event->getParam('configuration');
		if ($configuration instanceof \Rbs\Notification\Documents\Configuration)
		{
			$code = $event->getParam('code');
			if ($configuration->getType() === 'user')
			{
				$notification = new User($code, $configuration->getConfigData() ?? []);
				/** @var \Rbs\Notification\Manager $notificationManager */
				$notificationManager = $event->getTarget();
				$notification->setManager($notificationManager);
				$event->setParam('notification', $notification);
			}
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInitContext(\Change\Events\Event $event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		/** @var \Rbs\Notification\NotificationInterface $notification */
		$notification = $event->getTarget();
		$notificationCode = $notification->getCode();
		switch ($notificationCode)
		{
			case 'rbs_user_account_request':
				if ($customerMail = $event->getParam('email'))
				{
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('Email not found');
				}
				$this->initWebsiteContext($event);
				break;
			case 'rbs_user_account_valid':
				$userId = $event->getParam('userId');
				$user = $documentManager->getDocumentInstance($userId);
				if ($user instanceof \Rbs\User\Documents\User && ($customerMail = $user->getEmail()))
				{
					$event->setParam('user', $user);
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('User not found');
				}
				$this->initWebsiteContext($event);
				break;
			case 'rbs_user_change_email_request':
				$userId = $event->getParam('userId');
				$user = $documentManager->getDocumentInstance($userId);
				if ($user instanceof \Rbs\User\Documents\User && ($customerMail = $event->getParam('email')))
				{
					$event->setParam('user', $user);
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('User not found');
				}
				$this->initWebsiteContext($event);
				break;
			case 'rbs_user_mail_changed':
				$userId = $event->getParam('userId');
				$user = $documentManager->getDocumentInstance($userId);
				if ($user instanceof \Rbs\User\Documents\User && ($customerMail = $user->getEmail()))
				{
					$event->setParam('user', $user);
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('User not found');
				}
				$this->initWebsiteContext($event);
				break;
			case 'rbs_user_reset_password_request':
				$userId = $event->getParam('userId');
				$user = $documentManager->getDocumentInstance($userId);
				if ($user instanceof \Rbs\User\Documents\User && ($customerMail = $user->getEmail()))
				{
					$event->setParam('user', $user);
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('User not found');
				}
				$this->initWebsiteContext($event);
				break;
			case 'rbs_user_valid_mobile_phone_request':
				$mobilePhone = $event->getParam('mobilePhone');
				$token = $event->getParam('token');
				if ($mobilePhone && $token)
				{
					$event->setParam('__customerSMS', $mobilePhone);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('MobilePhone or token not found');
				}
				break;
			case 'rbs_user_suggest_account_creation':
				if ($customerMail = $event->getParam('email'))
				{
					$event->setParam('__customerMail', $customerMail);
					$event->setParam('__customerLCID', $event->getParam('LCID'));
				}
				else
				{
					throw new \Rbs\Notification\Exceptions\InvalidContext('User not found');
				}
				$this->initWebsiteContext($event);
				break;
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function initWebsiteContext(\Change\Events\Event $event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$website = $documentManager->getDocumentInstance($event->getParam('websiteId'));
		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$event->setParam('website', $website);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onGetSubstitutionLabels(\Change\Events\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$notificationCode = $event->getParam('notificationCode');
		$substitutionLabels = $event->getParam('substitutionLabels') ?? [];
		switch ($notificationCode)
		{
			case 'rbs_user_account_request':
				$substitutionLabels['link'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$substitutionLabels['email'] = $i18nManager->trans('m.rbs.generic.admin.substitution_email');
				$substitutionLabels['token'] = $i18nManager->trans('m.rbs.generic.admin.substitution_token');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
			case 'rbs_user_account_valid':
				$substitutionLabels['link'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$substitutionLabels['email'] = $i18nManager->trans('m.rbs.generic.admin.substitution_email');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
			case 'rbs_user_change_email_request':
				$substitutionLabels['confirmationUrl'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$substitutionLabels['email'] = $i18nManager->trans('m.rbs.generic.admin.substitution_email');
				$substitutionLabels['token'] = $i18nManager->trans('m.rbs.generic.admin.substitution_token');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
			case 'rbs_user_mail_changed':
				$substitutionLabels['link'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
			case 'rbs_user_reset_password_request':
				$substitutionLabels['token'] = $i18nManager->trans('m.rbs.generic.admin.substitution_token');
				$substitutionLabels['link'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
			case 'rbs_user_valid_mobile_phone_request':
				$substitutionLabels['token'] = $i18nManager->trans('m.rbs.generic.admin.substitution_token');
				break;
			case 'rbs_user_suggest_account_creation':
				$substitutionLabels['email'] = $i18nManager->trans('m.rbs.generic.admin.substitution_email');
				$substitutionLabels['link'] = $i18nManager->trans('m.rbs.generic.admin.substitution_process_link');
				$this->addWebsiteReplacement($substitutionLabels, $i18nManager);
				break;
		}
		$event->setParam('substitutionLabels', $substitutionLabels);
	}

	/**
	 * @param array $substitutionLabels
	 * @param \Change\I18n\I18nManager $i18nManager
	 */
	protected function addWebsiteReplacement(array &$substitutionLabels, \Change\I18n\I18nManager $i18nManager)
	{
		$substitutionLabels['website'] = $i18nManager->trans('m.rbs.generic.admin.substitution_website');
		$substitutionLabels['websiteLink'] = $i18nManager->trans('m.rbs.generic.admin.substitution_website_link');
		$substitutionLabels['websiteHref'] = $i18nManager->trans('m.rbs.generic.admin.substitution_website_href');
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function onBuildSubstitutionValues(\Change\Events\Event $event)
	{
		$notification = $event->getTarget();
		if ($notification instanceof \Rbs\Notification\NotificationInterface)
		{
			switch ($notification->getCode())
			{
				case 'rbs_user_account_request';
					$this->onBuildUserAccountRequest($event);
					break;
				case 'rbs_user_account_valid';
					$this->onBuildUserAccountValid($event);
					break;
				case 'rbs_user_change_email_request';
					$this->onBuildUserChangeEmailRequest($event);
					break;
				case 'rbs_user_mail_changed';
					$this->onBuildUserMailChanged($event);
					break;
				case 'rbs_user_reset_password_request';
					$this->onBuildUserResetPasswordRequest($event);
					break;
				case 'rbs_user_suggest_account_creation';
					$this->onBuildUserSuggestAccountCreation($event);
					break;
				case 'rbs_user_valid_mobile_phone_request';
				default:
					return;
			}
			$this->onWebsiteSubstitutionParams($event);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onWebsiteSubstitutionParams(\Change\Events\Event $event)
	{
		$website = $event->getParam('website');
		if ($website instanceof \Change\Presentation\Interfaces\Website)
		{
			$event->setParam('website', $website->getTitle());
			$event->setParam('websiteId', $website->getId());
			$href = $website->getUrlManager(null)->getByPathInfo('/')->normalize()->toString();
			$event->setParam('websiteHref', $href);
			$event->setParam('websiteLink', '<a href="' . \Change\Stdlib\StringUtils::attrEscape($href) . '">'
				. \Change\Stdlib\StringUtils::htmlEscape($website->getTitle()) . '</a>');
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserAccountRequest(\Change\Events\Event $event)
	{
		$link = $event->getParam('link');
		$website = $event->getParam('website');
		if ($website instanceof \Rbs\Website\Documents\Website && !$link)
		{
			$email = $event->getParam('email');
			$token = $event->getParam('token');

			$urlManager = $website->getUrlManager(null);
			$linkParams = ['token' => (string)$token, 'email' => (string)$email];
			if (!$event->getParam('confirmationPage'))
			{
				$location = $urlManager->getByFunction('Rbs_User_CreateAccount', $linkParams);
				$link = $location ? $location->normalize()->toString() : null;
			}
			if (!$link)
			{
				$link = $urlManager->getActionURL('Rbs_User', 'CreateAccountConfirmation', $linkParams);
			}
			$event->setParam('link', $link ?: null);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserAccountValid(\Change\Events\Event $event)
	{
		$user = $event->getParam('user');
		if ($user instanceof \Rbs\User\Documents\User)
		{
			$email = $user->getEmail();
			$event->setParam('email', $email);
		}

		$website = $event->getParam('website');
		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$urlManager = $website->getUrlManager(null);
			$uri = $urlManager->getByFunction('Rbs_User_Login');
			$event->setParam('link', $uri ? $uri->normalize()->toString() : '');
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserChangeEmailRequest(\Change\Events\Event $event)
	{
		if ($confirmationUrl = $event->getParam('confirmationUrl'))
		{
			$email = $event->getParam('email');
			$token = $event->getParam('token');
			$uri = (new \Zend\Uri\Http())->parse($confirmationUrl);
			$uri->setQuery(array_merge($uri->getQueryAsArray(), ['changeEmail' => ['token' => $token, 'email' => $email]]));
			$event->setParam('confirmationUrl', $uri->normalize()->toString());
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserMailChanged(\Change\Events\Event $event)
	{
		$website = $event->getParam('website');
		if ($website instanceof \Rbs\Website\Documents\Website)
		{
			$urlManager = $website->getUrlManager(null);
			$uri = $urlManager->getByFunction('Rbs_User_Login');
			$event->setParam('link', $uri ? $uri->normalize()->toString() : '');
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserResetPasswordRequest(\Change\Events\Event $event)
	{
		$website = $event->getParam('website');
		if (($website instanceof \Rbs\Website\Documents\Website) && !$event->getParam('link') && ($token = $event->getParam('token')))
		{
			$urlManager = $website->getUrlManager(null);
			$uri = $urlManager->getByFunction('Rbs_User_ResetPassword', ['token' => $token]);
			$event->setParam('link', $uri ? $uri->normalize()->toString() : '');
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildUserSuggestAccountCreation(\Change\Events\Event $event)
	{
		$website = $event->getParam('website');
		$link = $event->getParam('link');
		if (!$link && ($website instanceof \Rbs\Website\Documents\Website))
		{
			$urlManager = $website->getUrlManager(null);
			$uri = $urlManager->getByFunction('Rbs_User_CreateAccount');
			$event->setParam('link', $uri ? $uri->normalize()->toString() : '');
		}
	}
}