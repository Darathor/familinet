<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Presentation\Twig;

/**
 * @name \Rbs\Generic\Presentation\Twig\Extension
 */
class Extension implements \Change\Presentation\Templates\Twig\ExtensionInterface
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\Services\ApplicationServices
	 */
	protected $applicationServices;

	/**
	 * @var \Rbs\Generic\GenericServices
	 */
	protected $genericServices;

	/**
	 * @var \Change\Http\Web\UrlManager
	 */
	protected $urlManager;

	/**
	 * @var string|null
	 */
	protected $assetsBaseUrl;

	/**
	 * @param \Change\Application $application
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param \Rbs\Generic\GenericServices $genericServices
	 * @param \Change\Http\Web\UrlManager $urlManager
	 */
	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices,
		\Rbs\Generic\GenericServices $genericServices, \Change\Http\Web\UrlManager $urlManager)
	{
		$this->application = $application;
		$this->applicationServices = $applicationServices;
		$this->genericServices = $genericServices;
		$this->urlManager = $urlManager;
	}

	/**
	 * Returns the name of the extension.
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'Rbs_Generic';
	}

	/**
	 * Initializes the runtime environment.
	 * This is where you can load some file that contains filter functions for instance.
	 * @param \Twig\Environment $environment The current \Twig\Environment instance
	 */
	public function initRuntime(\Twig\Environment $environment)
	{
	}

	/**
	 * Returns the token parser instances to add to the existing list.
	 * @return array An array of Twig_TokenParserInterface or Twig_TokenParserBrokerInterface instances
	 */
	public function getTokenParsers()
	{
		return [];
	}

	/**
	 * Returns the node visitor instances to add to the existing list.
	 * @return array An array of Twig_NodeVisitorInterface instances
	 */
	public function getNodeVisitors()
	{
		return [];
	}

	/**
	 * Returns a list of filters to add to the existing list.
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new \Twig\TwigFilter('richText', [$this, 'richText'], ['is_safe' => ['all']])
		];
	}

	/**
	 * Returns a list of tests to add to the existing list.
	 * @return array An array of tests
	 */
	public function getTests()
	{
		return [];
	}

	/**
	 * Returns a list of functions to add to the existing list.
	 * @return array An array of functions
	 */
	public function getFunctions()
	{
		return [
			new \Twig\TwigFunction('imageURL', [$this, 'imageURL']),
			new \Twig\TwigFunction('imageAlt', [$this, 'imageAlt']),
			new \Twig\TwigFunction('canonicalURL', [$this, 'canonicalURL']),
			new \Twig\TwigFunction('contextualURL', [$this, 'contextualURL']),
			new \Twig\TwigFunction('currentURL', [$this, 'currentURL']),
			new \Twig\TwigFunction('ajaxURL', [$this, 'actionURL']), //Deprecated use actionURL
			new \Twig\TwigFunction('actionURL', [$this, 'actionURL']),
			new \Twig\TwigFunction('functionURL', [$this, 'functionURL']),
			new \Twig\TwigFunction('resourceURL', [$this, 'resourceURL']),
			new \Twig\TwigFunction('avatarURL', [$this, 'avatarURL']),
			new \Twig\TwigFunction('collectionItems', [$this, 'collectionItems']),
			new \Twig\TwigFunction('captchaId', [$this, 'captchaId']),
			new \Twig\TwigFunction('captchaVisual', [$this, 'captchaVisual'], ['is_safe' => ['all']]),
			new \Twig\TwigFunction('attributesByVisibility', [$this, 'attributesByVisibility']),
			new \Twig\TwigFunction('attributeByName', [$this, 'attributeByName']),
			new \Twig\TwigFunction('ajaxData', [$this, 'ajaxData'])
		];
	}

	/**
	 * Returns a list of operators to add to the existing list.
	 * @return array An array of operators
	 */
	public function getOperators()
	{
		return [];
	}

	/**
	 * Returns a list of global variables to add to the existing list.
	 * @return array An array of global variables
	 */
	public function getGlobals()
	{
		return [];
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication(\Change\Application $application)
	{
		$this->application = $application;
		return $this;
	}

	/**
	 * @return \Change\Application
	 */
	public function getApplication()
	{
		return $this->application;
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @return $this
	 */
	public function setApplicationServices(\Change\Services\ApplicationServices $applicationServices)
	{
		$this->applicationServices = $applicationServices;
		return $this;
	}

	/**
	 * @return \Change\Services\ApplicationServices
	 */
	public function getApplicationServices()
	{
		return $this->applicationServices;
	}

	/**
	 * @param \Change\Http\Web\UrlManager $urlManager
	 * @return $this
	 */
	public function setUrlManager(\Change\Http\Web\UrlManager $urlManager)
	{
		$this->urlManager = $urlManager;
		return $this;
	}

	/**
	 * @param \Rbs\Generic\GenericServices $genericServices
	 * @return $this
	 */
	public function setGenericServices($genericServices)
	{
		$this->genericServices = $genericServices;
		return $this;
	}

	/**
	 * @return \Change\Http\Web\UrlManager
	 */
	public function getUrlManager()
	{
		return $this->urlManager;
	}

	/**
	 * @return \Rbs\Generic\GenericServices
	 */
	public function getGenericServices()
	{
		return $this->genericServices;
	}

	/**
	 * @param \Rbs\Media\Documents\Image|integer|string $image
	 * @param integer|array $maxWidth The max width or an array containing max width and max height.
	 * @param integer $maxHeight
	 * @return string
	 */
	public function imageURL($image, $maxWidth = 0, $maxHeight = 0)
	{
		if (is_array($maxWidth))
		{
			$size = array_values($maxWidth);
			$maxWidth = $size[0] ?? 0;
			$maxHeight = $size[1] ?? $maxHeight;
		}
		$maxHeight = $maxHeight ?: (int)$this->application->getConfiguration('Rbs/Media/FullPage/height');
		$maxWidth = $maxWidth ?: (int)$this->application->getConfiguration('Rbs/Media/FullPage/width');

		if (is_string($image))
		{
			if (strpos($image, 'change://') === 0)
			{
				$storageManager = $this->getApplicationServices()->getStorageManager();
				$storageURI = new \Zend\Uri\Uri($image);
				$storageURI->setQuery(['max-width' => (int)$maxWidth, 'max-height' => (int)$maxHeight]);
				$url = $storageURI->normalize()->toString();
				return $storageManager->getPublicURL($url);
			}
			return $image;
		}

		$doc = $image;
		if (is_numeric($doc))
		{
			$doc = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($doc);
		}

		if ($doc instanceof \Rbs\Media\Documents\Image && $doc->activated())
		{
			return $doc->getPublicURL((int)$maxWidth, (int)$maxHeight);
		}
		return '';
	}

	/**
	 * @param \Rbs\Media\Documents\Image|integer $image
	 * @return string
	 */
	public function imageAlt($image)
	{
		$doc = $image;
		if (is_numeric($doc))
		{
			$doc = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($doc);
		}

		if ($doc instanceof \Rbs\Media\Documents\Image && $doc->activated())
		{
			return $doc->getCurrentLocalization()->getAlt();
		}
		return '';
	}

	/**
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param array $query
	 * @param string|null $LCID
	 * @return string|null
	 */
	public function canonicalURL($document, $query = [], $LCID = null)
	{
		if (is_numeric($document) || $document instanceof \Change\Documents\AbstractDocument)
		{
			return $this->getUrlManager()->getCanonicalByDocument($document, $query, $LCID)->normalize()->toString();
		}
		return null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param \Change\Presentation\Interfaces\Section|integer|null $section
	 * @param array $query
	 * @param string|null $LCID
	 * @return string|null
	 */
	public function contextualURL($document, $section = null, $query = [], $LCID = null)
	{
		if (is_numeric($document) || $document instanceof \Change\Documents\AbstractDocument)
		{
			if (is_numeric($section))
			{
				$section = $this->getApplicationServices()->getDocumentManager()->getDocumentInstance($section);
			}

			if ($section === null || $section instanceof \Change\Presentation\Interfaces\Section)
			{
				return $this->getUrlManager()->getByDocument($document, $section, $query, $LCID)->normalize()->toString();
			}
		}
		return null;
	}

	/**
	 * @param array $query
	 * @param string|null $fragment
	 * @return string
	 */
	public function currentURL($query = [], $fragment = null)
	{
		$uri = $this->getUrlManager()->getSelf();
		$oldQuery = $uri->getQueryAsArray();
		unset($oldQuery['errId']);

		if (is_array($query) && count($query))
		{
			$oldQuery = array_merge($oldQuery, $query);
		}
		$uri->setQuery($oldQuery);
		if ($fragment)
		{
			$uri->setFragment($fragment);
		}
		return $uri->normalize()->toString();
	}

	/**
	 * @param string $module
	 * @param string $action
	 * @param array $query
	 * @return string
	 */
	public function actionURL($module, $action, $query = [])
	{
		return $this->getUrlManager()->getActionURL($module, $action, $query);
	}

	/**
	 * @param string $functionCode
	 * @param array $query
	 * @return null|string
	 */
	public function functionURL($functionCode, $query = [])
	{
		$uri = $this->getUrlManager()->getByFunction($functionCode, $query);
		return $uri ? $uri->normalize()->toString() : null;
	}

	/**
	 * @return string
	 */
	public function getAssetsVersion()
	{
		return trim((string)$this->getApplication()->getConfiguration('Change/Install/assetsVersion'), '/');
	}

	/**
	 *
	 * @param string $relativePath
	 * @return string
	 */
	public function resourceURL($relativePath)
	{
		if ($this->assetsBaseUrl === null)
		{
			$assetsVersion = $this->getAssetsVersion();
			$this->assetsBaseUrl = $assetsVersion ? '/Assets/' . $assetsVersion . '/' : '/Assets/';
		}

		if (strpos($relativePath, '/') !== 0)
		{
			return $this->assetsBaseUrl . $relativePath;
		}
		return $relativePath;
	}

	/**
	 * @param integer $size
	 * @param string $email
	 * @param \Rbs\User\Documents\User $user
	 * @param mixed[] $params
	 * @return null|string
	 */
	public function avatarURL($size, $email, $user = null, array $params = [])
	{
		$avatarManager = $this->getGenericServices()->getAvatarManager();
		$avatarManager->setUrlManager($this->getUrlManager());
		return $avatarManager->getAvatarUrl($size, $email, $user, $params);
	}

	/**
	 * @param string $code
	 * @param mixed[] $params
	 * @return \Change\Collection\ItemInterface[]
	 */
	public function collectionItems($code, array $params = [])
	{
		$collection = $this->getApplicationServices()->getCollectionManager()->getCollection($code, $params);
		return $collection ? $collection->getItems() : [];
	}

	/**
	 * @return string
	 */
	public function captchaId()
	{
		return $this->getGenericServices()->getSecurityManager()->getCaptchaId();
	}

	/**
	 * @param mixed[] $params
	 * @return string|null
	 */
	public function captchaVisual(array $params = [])
	{
		return $this->getGenericServices()->getSecurityManager()->renderCaptcha($params);
	}

	/**
	 * @param \Change\Documents\RichtextProperty|array $richText
	 * @return string
	 */
	public function richText($richText)
	{
		if ($richText instanceof \Change\Documents\RichtextProperty)
		{
			$context = [
				'website' => $this->getUrlManager()->getWebsite(),
				'currentURI' => $this->getUrlManager()->getSelf()
			];
			return $this->getApplicationServices()->getRichTextManager()->render($richText, 'Website', $context);
		}
		if (is_array($richText) && array_key_exists('h', $richText) && isset($richText['e'], $richText['t']))
		{
			$context = [
				'website' => $this->getUrlManager()->getWebsite(),
				'currentURI' => $this->getUrlManager()->getSelf()
			];
			$richText = new \Change\Documents\RichtextProperty($richText);
			return $this->getApplicationServices()->getRichTextManager()->render($richText, 'Website', $context);
		}
		return htmlspecialchars((string)$richText);
	}

	/**
	 * @param string $name
	 * @param array $documentData
	 * @param array|null $parentData
	 * @return array|null
	 */
	public function attributeByName($name, $documentData, $parentData = null)
	{
		if (!is_array($documentData))
		{
			return null;
		}

		if (isset($documentData['typology']['attributes'][$name]))
		{
			return $documentData['typology']['attributes'][$name];
		}

		if (is_array($parentData) && isset($parentData['typology']['attributes'][$name]))
		{
			return $parentData['typology']['attributes'][$name];
		}
		return null;
	}

	/**
	 * @param string $visibility
	 * @param array $documentData
	 * @param array|null $parentData
	 * @return array
	 */
	public function attributesByVisibility($visibility, $documentData, $parentData = null)
	{
		$sections = [];
		if (is_array($parentData))
		{
			$sections = $this->attributesByVisibility($visibility, $parentData);
		}

		if (is_array($documentData) && isset($documentData['typology']['visibilities'][$visibility]))
		{
			foreach ($documentData['typology']['visibilities'][$visibility] as $group)
			{
				$section = $group['key'];
				foreach ($group['items'] as $attribute)
				{
					$key = $attribute['key'];
					$sections[$section][$key] = $documentData['typology']['attributes'][$key];
				}
			}
		}
		return $sections;
	}

	/**
	 * @param \Change\Documents\AbstractDocument|integer $document
	 * @param array $context
	 * @return array
	 */
	public function ajaxData($document, array $context = null)
	{
		$documentManager = $this->getApplicationServices()->getDocumentManager();
		$doc = is_numeric($document) ? $documentManager->getDocumentInstance($document) : $document;
		if ($doc instanceof \Change\Documents\AbstractDocument)
		{
			if (!is_array($context))
			{
				$context = [];
			}

			$urlManager = $this->urlManager;
			$context += ['detailed' => true, 'website' => $urlManager->getWebsite(), 'section' => $urlManager->getSection()];
			$context = (new \Change\Http\Ajax\V1\Context($this->application, $documentManager, $context))->toArray();
			return $doc->getAJAXData($context);
		}
		return [];
	}
}