<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic;

/**
 * @name \Rbs\Generic\ServicesStatus
 */
class ServicesStatus
{

	/** @var \Change\Db\DbProvider $dbProvider */
	protected $dbProvider;

	/** @var  \Change\Configuration\Configuration $configuration */
	protected $configuration;

	/** @var \Change\Storage\StorageManager $storageManager */
	protected $storageManager;

	/** @var array $errors */
	public $errors = [];

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @param $error
	 * @return $this;
	 */
	protected function addError($error)
	{
		$this->errors[] = $error;
		return $this;
	}

	public function __construct(\Change\Application $application, \Change\Services\ApplicationServices $applicationServices)
	{
		$this->dbProvider = $applicationServices->getDbProvider();
		$this->configuration = $application->getConfiguration();
		$this->storageManager = $applicationServices->getStorageManager();
		$this->errors = [];
	}

	public function checkDb()
	{
		try
		{
			if ($this->dbProvider instanceof \Change\Db\Mysql\DbProvider)
			{
				$pdo = $this->dbProvider->getDriver();
				$pdo->exec('show VARIABLES where variable_name = \'version\'');
			}
		}
		catch(\Exception $e)
		{
			$this->addError($e->getMessage());
		}
	}

	public function checkRedis()
	{
		$errstr = 'Redis : Can\'t connect to service or connection timeout';
		try
		{
			$host = $this->configuration->getEntry('Change/Cache/Adapter/redis/server/host');
			$port = $this->configuration->getEntry('Change/Cache/Adapter/redis/server/port');
			if ($host && $port)
			{
				$fp = fsockopen($host, $port, $errno, $errstr, 5);
				if (!$fp)
				{
					$this->addError($errstr);
				}
			}
		}
		catch (\Exception $e)
		{
			$this->addError($e->getMessage());
		}
	}

	public function checkStorage()
	{
		$path = $this->configuration->getEntry('Change/Storage/images/basePath');
		$errstr = 'Storage : Can\'t access to image storage at ' . $path;
		if (!$path || !is_dir($path))
		{
			$this->addError($errstr);
		}
	}

}