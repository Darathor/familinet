<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Setup;

/**
 * @name \Rbs\Generic\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$this->tables['rbs_generic_dat_attributes_index'] = $td = $schemaManager->newTableDefinition('rbs_generic_dat_attributes_index');
			$td->addField($schemaManager->newIntegerFieldDefinition('attribute_id')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('document_id')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('index')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newIntegerFieldDefinition('integer_value')->setNullable(true))
				->addField($schemaManager->newFloatFieldDefinition('float_value')->setNullable(true))
				->addField($schemaManager->newDateFieldDefinition('date_value')->setNullable(true))
				->addField($schemaManager->newCharFieldDefinition('string_value')->setNullable(true))
				->addKey($this->newPrimaryKey()->addField($td->getField('attribute_id'))->addField($td->getField('document_id'))
					->addField($td->getField('index')));

			$this->tables['rbs_generic_imp_metas'] = $td = $schemaManager->newTableDefinition('rbs_generic_imp_metas');
			$td->addField($schemaManager->newVarCharFieldDefinition('code')->setNullable(false))
					->addField($schemaManager->newVarCharFieldDefinition('lcid')->setLength(5)->setNullable(false))
					->addField($schemaManager->newVarCharFieldDefinition('group')->setNullable(false)->setDefaultValue(0))
					->addField($schemaManager->newVarCharFieldDefinition('meta_type')->setNullable(false))
					->addField($schemaManager->newVarCharFieldDefinition('meta_name')->setNullable(true))
					->addField($schemaManager->newVarCharFieldDefinition('meta_value')->setNullable(true));
		}
		return $this->tables;
	}
}
