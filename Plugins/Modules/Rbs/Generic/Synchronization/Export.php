<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Synchronization;

/**
 * @name \Rbs\Generic\Synchronization\Export
 */
class Export
{
	/**
	 * @param \Change\Synchronization\ExportEvent $event
	 */
	public function onResolve($event)
	{
	}

	/**
	 * @param \Change\Synchronization\ExportEvent $event
	 */
	public function onPopulate($event)
	{
		$document = $event->getItem();
		if ($document instanceof \Change\Documents\AbstractDocument)
		{
			$property = $document->getDocumentModel()->getProperty('authorId');
			if ($property && $property->getType() === \Change\Documents\Property::TYPE_DOCUMENTID)
			{
				$property->setDocumentType('Rbs_User_User');
			}

		}
	}
}