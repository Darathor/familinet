<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Generic\Synchronization;

use Change\Synchronization\ImportException;

/**
 * @name \Rbs\Generic\Synchronization\Import
 */
class Import
{
	protected $typologiesByCode = [];

	/**
	 * Event input params: itemData
	 * Event output params: item
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onResolve(\Change\Synchronization\ImportEvent $event)
	{
		$model = $event->getItemDataModel();
		$id = $event->getItemDataId();
		if ($model === 'Rbs_Generic_Typology' && $event->hasItemDataProperty('modelName'))
		{
			$modelName = $event->getItemDataProperty('modelName');
			$code = $id . '_' . $modelName;

			if (!array_key_exists($code, $this->typologiesByCode))
			{
				$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Generic_Typology');
				$query->andPredicates($query->eq('name', $id), $query->eq('modelName', $modelName));
				$typology = $query->getFirstDocument();
				$this->typologiesByCode[$code] = $typology ? $typology->getId() : 0;
				$event->setItem($typology);
			}
			else
			{
				$typologyId = $this->typologiesByCode[$code];
				$typology = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($typologyId);
				$event->setItem($typology ?: false);
			}
		}
	}

	/**
	 * Event input params: itemData
	 * Event output params: item
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onCreate(\Change\Synchronization\ImportEvent $event)
	{
	}

	/**
	 * @param \Rbs\Generic\Documents\Typology $typology
	 * @return \Rbs\Generic\Documents\Attribute[]
	 */
	protected function extractNamedAttributes($typology)
	{
		$attributes = [];
		foreach ($typology->getGroups() as $group)
		{
			foreach ($group->getAttributes() as $attribute)
			{
				$name = $attribute->getName();
				if ($name)
				{
					$attributes[$name] = $attribute;
				}
			}
		}
		return $attributes;
	}

	/**
	 * Event input params: item, itemData
	 * Event output params: typology, typologyValues
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onPopulateDocument(\Change\Synchronization\ImportEvent $event)
	{
		$item = $event->getItem();
		if ($item instanceof \Rbs\Media\Documents\File
			|| $item instanceof \Rbs\Media\Documents\Image
			|| $item instanceof \Rbs\Media\Documents\Video
		)
		{
			$this->onPopulateMediaDocumentPath($event);
		}
		elseif ($item instanceof \Rbs\Website\Documents\Topic)
		{
			$this->onPopulateTopicDocument($event);
		}
		elseif ($item instanceof \Rbs\User\Documents\User)
		{
			if ($event->hasItemDataProperty('profile'))
			{
				$profile = new \Rbs\User\Profile\Profile();
				$userProfile = $event->getItemDataProperty('profile');
				if (is_array($userProfile))
				{
					$invalidProperties =  array_diff(array_keys($userProfile), $profile->getPropertyNames());
					if (!$invalidProperties)
					{
						$event->setParam('userProfile', $userProfile);
						$event->removeItemDataProperty('profile');
					}
					else
					{
						$invalidProperty = array_shift($invalidProperties);
						throw new \Change\Synchronization\ImportException('Unsupported property profile.' . $invalidProperty , 13);
					}
				}
			}
		}
		elseif ($item instanceof \Rbs\Geo\Documents\Address)
		{
			$itemData = $event->getItemData();
			$fields = $itemData['fields'] ?? [];
			if (is_array($fields))
			{
				$fieldsProperty = \Rbs\Geo\Documents\Address::$FIELDS_PROPERTY;
				foreach ($fields as $fieldName => $fieldValue)
				{
					if (is_string($fieldName) && !in_array($fieldName, $fieldsProperty))
					{
						$item->setFieldValue($fieldName, $fieldValue);
					}
				}
				unset($itemData['fields']);
			}

			$userData = $itemData['user'] ?? null;
			$user = $event->getImportEngine()->resolveItem($userData);
			if (!$user instanceof \Rbs\User\Documents\User)
			{
				throw new ImportException('Property "user" can not be null', 11);
			}

			$item->setOwnerId($user->getId());
			unset($itemData['user'], $itemData['lines']);
			if (isset($itemData['default']))
			{
				$event->setParam('default', (bool)$itemData['default']);
				unset($itemData['default']);
			}
			$event->setItemData($itemData);
		}
	}

	/**
	 * Event input params: item, itemData
	 * Event output params: typology, typologyValues
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onPopulateTypology(\Change\Synchronization\ImportEvent $event)
	{
		$item = $event->getItem();
		$itemData = $event->getItemData();
		if (!($item instanceof \Change\Documents\AbstractDocument) || !isset($itemData['__typology']['__id']))
		{
			return;
		}
		$itemTypologyData = $itemData['__typology'];
		$itemTypologyData['__model'] = 'Rbs_Generic_Typology';
		$itemTypologyData['modelName'] = $item->getDocumentModelName();

		$importEngine = $event->getImportEngine();
		$itemTypologyData['__property'] = '__typology';
		$typology = $importEngine->resolveItem($itemTypologyData);
		if (!($typology instanceof \Rbs\Generic\Documents\Typology))
		{
			throw new ImportException('Invalid Typology', 7, null, $itemTypologyData);
		}
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$LCID = $itemData['refLCID'] ?? $documentManager->getLCID();

		$typologyValues = [];
		$attributeValues = new \Change\Documents\Attributes\AttributeValues($LCID);
		$attributesByName = $this->extractNamedAttributes($typology);
		foreach ($attributesByName as $attributeName => $attribute)
		{
			if (array_key_exists($attributeName, $itemTypologyData))
			{
				$attributeValues->set($attributeName,
					$this->normalizeAttributeValue($attribute, $itemTypologyData[$attributeName], $importEngine, $itemData));
			}
		}
		$typologyValues[] = $attributeValues;

		if (isset($itemData['LCID']) && is_array($itemData['LCID']))
		{
			$jsonLCID = $itemData['LCID'];
			foreach ($jsonLCID as $LCID => $localizedData)
			{
				if (is_array($localizedData)
					&& array_key_exists('__typology', $localizedData)
					&& is_array($localizedData['__typology'])
				)
				{
					$attributeValues = new \Change\Documents\Attributes\AttributeValues($LCID);
					$jsonAttributesValue = $localizedData['__typology'];
					foreach ($attributesByName as $attributeName => $attribute)
					{
						if (array_key_exists($attributeName, $jsonAttributesValue) && $attribute->getLocalizedValue())
						{
							$attributeValues->set($attributeName,
								$this->normalizeAttributeValue($attribute, $jsonAttributesValue[$attributeName], $importEngine,
									$itemData));
						}
					}
					$typologyValues[] = $attributeValues;
				}
			}
			$event->updateItemDataProperty('LCID', $jsonLCID);
		}

		$event->setParam('typology', $typology);
		$event->setParam('typologyValues', $typologyValues);
	}

	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param mixed $jsonValue
	 * @param \Change\Synchronization\ImportEngine $importEngine
	 * @param array $itemData
	 * @return mixed
	 */
	protected function normalizeAttributeValue($attribute, $jsonValue, $importEngine, $itemData)
	{
		$name = $attribute->getName();
		if ($jsonValue === null && $attribute->getRequiredValue())
		{
			throw new ImportException('Required Typology attribute "' . $name . '"', 15);
		}

		switch ($attribute->getValueType())
		{
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_BOOLEAN:
				if ($jsonValue === true || $jsonValue === false)
				{
					return $jsonValue;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_RICHTEXT:
				if ($jsonValue === null)
				{
					return $jsonValue;
				}
				elseif (is_string($jsonValue) || (is_array($jsonValue) && isset($jsonValue['e'], $jsonValue['t'])))
				{
					return new \Change\Documents\RichtextProperty($jsonValue);
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_FLOAT:
				if ($jsonValue === null || is_int($jsonValue) || is_float($jsonValue))
				{
					return $jsonValue;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DATETIME:
				if (is_string($jsonValue))
				{
					$jsonValue = new \DateTime($jsonValue);
				}
				if ($jsonValue === null || $jsonValue instanceof \DateTime)
				{
					return $jsonValue;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_INTEGER:
				if ($jsonValue === null || is_int($jsonValue))
				{
					return $jsonValue;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_STRING:
				if ($jsonValue === null || is_string($jsonValue))
				{
					return $jsonValue;
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID:
				if ($jsonValue === null)
				{
					return null;
				}
				if (is_array($jsonValue))
				{
					if (!isset($jsonValue['__model']))
					{
						$jsonValue['__model'] = $attribute->getDocumentType();
					}
					$jsonValue['__property'] = $name;
					$doc = $importEngine->resolveItem($jsonValue);
					if ($doc instanceof \Change\Documents\AbstractDocument)
					{
						return $doc->getId();
					}
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_DOCUMENT_ID_ARRAY:
				if ($jsonValue === null || (is_array($jsonValue) && count($jsonValue) === 0))
				{
					return null;
				}
				if (is_array($jsonValue))
				{
					$docIds = [];
					foreach ($jsonValue as $jsonDoc)
					{
						if (!is_array($jsonDoc))
						{
							break;
						}
						if (!isset($jsonDoc['__model']))
						{
							$jsonDoc['__model'] = $attribute->getDocumentType();
						}
						$jsonDoc['__property'] = $name;
						$doc = $importEngine->resolveItem($jsonDoc);
						if ($doc instanceof \Change\Documents\AbstractDocument)
						{
							$docIds[] = $doc->getId();
						}
					}
					if (count($docIds) === count($jsonValue))
					{
						return $docIds;
					}
				}
				break;
			case \Change\Documents\Attributes\Interfaces\Attribute::TYPE_JSON:
				return $jsonValue && is_array($jsonValue) ? $jsonValue : null;
			default:
				throw new ImportException('Invalid Typology attribute "' . $name . '" type', 17);
		}
		throw new ImportException('Invalid Typology attribute "' . $name . '" value', 16);
	}

	/**
	 * Event input params: item, typology, typologyValues
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onSaveTypology(\Change\Synchronization\ImportEvent $event)
	{
		$document = $event->getItem();
		$typologyDocument = $event->getParam('typology');
		$typologyValues = $event->getParam('typologyValues');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		if ($document instanceof \Change\Documents\AbstractDocument
			&& $typologyDocument instanceof \Rbs\Generic\Documents\Typology
			&& is_array($typologyValues)
			&& count($typologyValues)
		)
		{
			$typology = new \Rbs\Generic\Attributes\Typology($typologyDocument);
			foreach ($typologyValues as $attributeValues)
			{
				if ($attributeValues instanceof \Change\Documents\Attributes\AttributeValues)
				{
					try
					{
						$documentManager->pushLCID($attributeValues->getLCID());
						$documentManager->saveAttributeValues($document, $typology, $attributeValues);
						$documentManager->popLCID();
					}
					catch (\Exception $e)
					{
						$documentManager->popLCID($e);
					}
				}
			}
		}
	}

	/**
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onSaveUrlRewriting(\Change\Synchronization\ImportEvent $event)
	{
		$item = $event->getItem();
		if (!($item instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$documentId = $item->getId();
		$entries = $event->getParam('urlRewritingEntries');

		if (!is_array($entries))
		{
			$publicationSections = $event->getItemDataProperty('publicationSections');
			if (!$publicationSections || !is_array($publicationSections))
			{
				return;
			}

			$entries = [];
			foreach ($item->getPublicationSections() as $idx => $section)
			{
				if (!($section instanceof \Rbs\Website\Documents\Section) || !$section->getWebsite()
					|| !isset($publicationSections[$idx]) || !is_array($publicationSections[$idx]))
				{
					return;
				}

				$sectionId = $section->getId();
				$websiteId = $section->getWebsite()->getId();
				if ($sectionId == $websiteId)
				{
					$sectionId = 0;
				}

				if (isset($publicationSections[$idx]['__url']) && is_array($publicationSections[$idx]['__url']))
				{
					foreach ($publicationSections[$idx]['__url'] as $LCID => $paths)
					{
						if (!is_array($paths) || strlen($LCID) != 5)
						{
							throw new \Change\Synchronization\ImportException('Invalid publicationSections[' . $idx
								. '].__url property', 100);
						}
						if (isset($paths['contextual']))
						{
							$path = ltrim((string)$paths['contextual'], '/');
							if ($path)
							{
								$entries[$websiteId][$sectionId][$LCID] = $path;
							}
							else
							{
								throw new \Change\Synchronization\ImportException('Invalid publicationSections[' . $idx
									. '].__url[' . $LCID . '].contextual property', 100);
							}
						}
						if (isset($paths['canonical']))
						{
							$path = ltrim((string)$paths['canonical'], '/');
							if ($path)
							{
								$entries[$websiteId][0][$LCID] = $path;
							}
							else
							{
								throw new \Change\Synchronization\ImportException('Invalid publicationSections[' . $idx
									. '].__url[' . $LCID . '].canonical property', 101);
							}
						}
					}
				}
			}
		}

		if ($entries)
		{
			$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
			$this->checkPathRules($documentId, $entries, $pathRuleManager);
		}
	}

	/**
	 * @param integer $documentId
	 * @param array $entries
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 */
	protected function checkPathRules($documentId, $entries, $pathRuleManager)
	{
		foreach ($entries as $websiteId => $sectionData)
		{
			foreach ($sectionData as $sectionId => $pathData)
			{
				foreach ($pathData as $LCID => $relativePath)
				{
					$oldRules = $pathRuleManager->findPathRules($websiteId, $LCID, $documentId, $sectionId);
					foreach ($oldRules as $oldRule)
					{
						if ($oldRule->getDocumentAliasId() == $documentId)
						{
							continue;
						}
					}

					$rule = $pathRuleManager->getPathRule($websiteId, $LCID, $relativePath);
					if ($rule && $rule->getHttpStatus() != 200)
					{
						$pathRuleManager->updateRuleStatus($rule->getRuleId(), 404);
						$rule = null;
					}

					if (!$rule)
					{
						$rule = $pathRuleManager->getNewRule($websiteId, $LCID, $relativePath, $documentId, 200, $sectionId, null, true);
						$pathRuleManager->insertPathRule($rule);
						foreach ($oldRules as $oldRule)
						{
							$pathRuleManager->updateRuleStatus($oldRule->getRuleId(), 301);
						}
					}
					elseif ($rule->getDocumentId() != $documentId || $rule->getSectionId() != $sectionId)
					{
						$conflict = '(' . $sectionId . ' / ' . $documentId . ' -> ' . $rule->getSectionId() . ' / '
							. $rule->getDocumentId() . ')';
						throw new \Change\Synchronization\ImportException('Duplicate rule path /' . $relativePath . ' '
							. $conflict, 101);
					}
				}
			}
		}
	}

	/**
	 * Event input params: item, default
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onSaveAddress(\Change\Synchronization\ImportEvent $event)
	{
		$address = $event->getItem();
		$default = $event->getParam('default');
		if ($address instanceof \Rbs\Geo\Documents\Address && is_bool($default))
		{
			$user = $address->getOwnerIdInstance();
			if ($user instanceof \Rbs\User\Documents\User)
			{
				$defaultId = $user->getMeta('Rbs_Geo_DefaultAddressId');
				if ($default && $defaultId != $address->getId())
				{
					$user->setMeta('Rbs_Geo_DefaultAddressId', $address->getId());
					$user->saveMetas();
				}
				elseif (!$default && $defaultId == $address->getId())
				{
					$user->setMeta('Rbs_Geo_DefaultAddressId', null);
					$user->saveMetas();
				}
			}
		}
	}

	/**
	 * Event input params: item, itemData, refLCID, localizations
	 * @param \Change\Synchronization\ImportEvent $event
	 * @throws \Exception
	 */
	public function onUpdatePublicationStatus(\Change\Synchronization\ImportEvent $event)
	{
		/** @var \Change\Documents\AbstractDocument $document */
		$document = $event->getItem();
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			if ($event->hasItemDataProperty('publicationStatus'))
			{
				$refLCID = $event->getParam('refLCID');
				$publicationStatus = $event->getItemDataProperty('publicationStatus');
				if ($publicationStatus !== \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE
					&& $publicationStatus !== \Change\Documents\Interfaces\Publishable::STATUS_FROZEN
				)
				{
					throw new ImportException('Invalid property "publicationStatus" value', 10);
				}
				$this->publish($document, $refLCID, $publicationStatus, $documentManager);
			}

			$localisations = $event->getParam('localizations');
			if ($localisations && is_array($localisations))
			{
				$localizedItemData = (array)$event->getItemDataProperty('LCID');
				foreach ($localisations as $LCID)
				{
					if (isset($localizedItemData[$LCID]['publicationStatus']))
					{
						$publicationStatus = $localizedItemData[$LCID]['publicationStatus'];
						if ($publicationStatus !== \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE
							&& $publicationStatus !== \Change\Documents\Interfaces\Publishable::STATUS_FROZEN
						)
						{
							throw new ImportException('Invalid property "LCID.' . $LCID . '.publicationStatus" value', 10);
						}
						$this->publish($document, $LCID, $publicationStatus, $documentManager);
					}
				}
			}
		}
	}

	/**
	 * Event input params: item, typology, typologyValues
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onSaveUserProfile(\Change\Synchronization\ImportEvent $event)
	{
		$user = $event->getItem();
		$userProfileData = $event->getParam('userProfile');
		if ($user instanceof \Rbs\User\Documents\User && is_array($userProfileData))
		{
			$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
			$profileManager = $event->getApplicationServices()->getProfileManager();
			$userProfile = $profileManager->loadProfile($authenticatedUser, 'Rbs_User');
			$save = false;
			foreach ($userProfileData as $name => $value)
			{
				if ($name === 'fullName')
				{
					continue;
				}

				if ($name === 'birthDate' && $value)
				{
					$iv = new \DateTime((string)$value);
					$value = \DateTime::createFromFormat('Y-m-d', $iv->format('Y-m-d'), new \DateTimeZone('UTC'))->setTime(0, 0);
				}

				$oldValue = $userProfile->getPropertyValue($name);
				if ($oldValue != $value)
				{
					$save = true;
					$userProfile->setPropertyValue($name, $value);
				}
			}

			if ($save)
			{
				$profileManager->saveProfile($authenticatedUser, $userProfile);
			}
		}
	}

	/**
	 * @var string[]
	 */
	protected static $publicationTaskCodes = ['requestValidation', 'contentValidation', 'publicationValidation'];

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @param string|null $LCID
	 * @param string $publicationStatus
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @throws \Exception
	 */
	public function publish(\Change\Documents\AbstractDocument $document, $LCID, $publicationStatus, $documentManager)
	{
		try
		{
			if ($LCID)
			{
				$documentManager->pushLCID($LCID);
			}

			$currentStatus = $document->getDocumentModel()->getPropertyValue($document, 'publicationStatus');

			if ($publicationStatus === \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE)
			{
				if ($currentStatus === \Change\Documents\Interfaces\Publishable::STATUS_DRAFT)
				{
					$this->executePublicationTask($document, $LCID, static::$publicationTaskCodes, $documentManager);
				}
				elseif ($currentStatus === \Change\Documents\Interfaces\Publishable::STATUS_FROZEN)
				{
					$this->executePublicationTask($document, $LCID, ['unfreeze'], $documentManager);
				}
			}
			elseif ($publicationStatus === \Change\Documents\Interfaces\Publishable::STATUS_FROZEN)
			{
				if ($currentStatus == \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE)
				{
					$this->executePublicationTask($document, $LCID, ['freeze'], $documentManager);
				}
				elseif ($currentStatus === \Change\Documents\Interfaces\Publishable::STATUS_DRAFT)
				{
					$this->executePublicationTask($document, $LCID, static::$publicationTaskCodes, $documentManager);
					$this->executePublicationTask($document, $LCID, ['freeze'], $documentManager);
				}
			}

			if ($LCID)
			{
				$documentManager->popLCID();
			}
		}
		catch (\Exception $e)
		{
			if ($LCID)
			{
				$documentManager->popLCID($e);
			}
			throw $e;
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @param string|null $LCID
	 * @param array $publicationTaskCodes
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	protected function executePublicationTask(\Change\Documents\AbstractDocument $document, $LCID, array $publicationTaskCodes,
		$documentManager)
	{
		$query = $documentManager->getNewQuery('Rbs_Workflow_Task');
		$query->andPredicates($query->in('taskCode', $publicationTaskCodes),
			$query->eq('document', $document),
			$LCID ? $query->eq('documentLCID', $LCID) : $query->isNull('documentLCID'),
			$query->eq('status', \Change\Workflow\Interfaces\WorkItem::STATUS_ENABLED));

		$task = $query->getFirstDocument();
		if ($task instanceof \Rbs\Workflow\Documents\Task)
		{
			$userId = 0;
			$context = [];
			$workflowInstance = $task->execute($context, $userId);
			if ($workflowInstance)
			{
				$this->executePublicationTask($document, $LCID, $publicationTaskCodes, $documentManager);
			}
		}
	}

	/**
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	protected function onPopulateMediaDocumentPath(\Change\Synchronization\ImportEvent $event)
	{
		/** @var \Rbs\Media\Documents\File|\Rbs\Media\Documents\Image|\Rbs\Media\Documents\Video $item */
		$item = $event->getItem();
		$storageManager = $event->getApplicationServices()->getStorageManager();
		$src = $event->removeItemDataProperty('src');
		/** @var array $parsedURL */
		$parsedURL = $src ? parse_url($src) : false;
		if (!$parsedURL || !isset($parsedURL['path']) || strlen($parsedURL['path']) < 2)
		{
			$parsedURL = false;
		}

		$path = $item->getPath();
		if ($event->hasItemDataProperty('path'))
		{
			$path = $event->getItemDataProperty('path');
		}

		if (!$path && $parsedURL)
		{
			if (isset($parsedURL['query']) && $parsedURL['query'])
			{
				$path = 'change://images/' . md5($parsedURL['query'], false) . $parsedURL['path'];
			}
			else
			{
				$path = 'change://images' . $parsedURL['path'];
			}
		}

		if (is_string($path))
		{
			$event->updateItemDataProperty('path', $path);

			$itemInfo = $storageManager->getItemInfo($path);
			if ($itemInfo && $itemInfo->isFile())
			{
				@touch($path);
				return;
			}

			if ($parsedURL)
			{
				$data = @file_get_contents(str_replace(' ', '%20', $src));
				if ($data === false)
				{
					throw new ImportException('Unable to download: "' . $src . '"', 18);
				}

				$contentLength = @file_put_contents($path, $data);
				if ($contentLength === false)
				{
					throw new ImportException('Unable to save: "' . $path . '"', 19);
				}
				return;
			}
		}
		throw new ImportException('Invalid property "path" value', 10);
	}

	/**
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	protected function onPopulateTopicDocument(\Change\Synchronization\ImportEvent $event)
	{
		/** @var \Rbs\Website\Documents\Topic $topic */
		$topic = $event->getItem();
		$itemData = $event->getItemData();
		foreach (['indexPage', 'website'] as $excludedProperty)
		{
			if (array_key_exists($excludedProperty, $itemData))
			{
				throw new ImportException('Invalid property "' . $excludedProperty . '" value', 10);
			}
		}

		if (isset($itemData['title']) && !isset($itemData['label']))
		{
			$event->updateItemDataProperty('label', $itemData['title']);
		}

		$parentSection = null;
		if ($event->hasItemDataProperty('parentSection'))
		{
			$jsonParentSection = $event->removeItemDataProperty('parentSection');

			if (is_array($jsonParentSection) || isset($jsonParentSection['__id']))
			{
				if (!isset($jsonParentSection['__model']))
				{
					$jsonParentSection['__model'] = 'Rbs_Website_Section';
				}
				$jsonParentSection['__property'] = 'parentSection';

				/** @var \Rbs\Website\Documents\Section $parentSection */
				$parentSection = $event->getImportEngine()->resolveItem($jsonParentSection);
				if (isset($jsonParentSection['__url']) && is_array($jsonParentSection['__url']))
				{
					$website = $parentSection->getWebsite();
					if ($website)
					{
						$entries = [];
						$websiteId = $website->getId();
						foreach ($jsonParentSection['__url'] as $LCID => $paths)
						{
							if (isset($paths['canonical']))
							{
								$path = ltrim((string)$paths['canonical'], '/');
								if ($path)
								{
									$entries[$websiteId][0][$LCID] = $path;
								}
								else
								{
									throw new \Change\Synchronization\ImportException('Invalid parentSection.__url[' . $LCID
										. '].canonical property', 101);
								}
							}
						}
						if ($entries)
						{
							$event->setParam('urlRewritingEntries', $entries);
						}
					}
				}
			}
		}
		else
		{
			$treeManager = $event->getApplicationServices()->getTreeManager();
			$topicNode = $treeManager->getNodeByDocument($topic);
			if ($topicNode)
			{
				$parentSectionNode = $topicNode->getParentId();
				if ($parentSectionNode)
				{
					$parentSection = $treeManager->getDocumentByNode($parentSectionNode);
				}
			}
		}

		if ($parentSection instanceof \Rbs\Website\Documents\Section)
		{
			$topic->setSection($parentSection);
			$topic->setWebsite($parentSection->getWebsite());
		}
		else
		{
			throw new ImportException('Invalid property "parentSection" value', 10);
		}

		$jsonPagesFunction = $event->removeItemDataProperty('pagesFunction');
		if (is_array($jsonPagesFunction))
		{
			$pagesFunction = [];
			foreach ($jsonPagesFunction as $function => $jsonPage)
			{
				if (is_string($function) && $function && is_array($jsonPage) && isset($jsonPage['__id']))
				{
					if (!isset($jsonPage['__model']))
					{
						$jsonPage['__model'] = 'Rbs_Website_Page';
					}
					$jsonPage['__property'] = 'pagesFunction[' . $function . ']';
					$page = $event->getImportEngine()->resolveItem($jsonPage);
					if ($page instanceof \Rbs\Website\Documents\Page)
					{
						$pagesFunction[$function] = $page;
					}
				}
			}

			if (count($pagesFunction) === count($jsonPagesFunction))
			{
				$event->setParam('pagesFunction', $pagesFunction);
			}
		}
		elseif ($jsonPagesFunction !== null)
		{
			throw new ImportException('Invalid property "pagesFunction" value', 10);
		}
	}

	/**
	 * Event input params: item, pagesFunction
	 * @param \Change\Synchronization\ImportEvent $event
	 */
	public function onSaveTopic(\Change\Synchronization\ImportEvent $event)
	{
		$topic = $event->getItem();
		if ($topic instanceof \Rbs\Website\Documents\Topic)
		{
			/** @var \Rbs\Website\Documents\Page[] $pagesFunction */
			$pagesFunction = $event->getParam('pagesFunction');
			if ($pagesFunction)
			{
				$documentManager = $event->getApplicationServices()->getDocumentManager();
				foreach ($pagesFunction as $function => $page)
				{
					$query = $documentManager->getNewQuery('Rbs_Website_SectionPageFunction');
					$sectionPageFunction = $query->andPredicates($query->eq('functionCode', $function),
						$query->eq('section', $topic))->getFirstDocument();
					if (!$sectionPageFunction)
					{
						/* @var $sectionPageFunction \Rbs\Website\Documents\SectionPageFunction */
						$sectionPageFunction = $documentManager->getNewDocumentInstanceByModelName('Rbs_Website_SectionPageFunction');
						$sectionPageFunction->setSection($topic);
						$sectionPageFunction->setFunctionCode($function);
					}
					$sectionPageFunction->setPage($page);
					if ($sectionPageFunction->isPropertyModified('page'))
					{
						$sectionPageFunction->save();
					}
				}
			}
		}
	}
}