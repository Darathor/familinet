<?php
/**
 * Copyright (C) 2017 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Geo\Address;

/**
 * @name \Rbs\Geo\Address\NormalizeAddress
 */
class NormalizeAddress
{

	const SUCCESS = 'success';
	const ERROR = 'error';

	/**
	 * @var \Rbs\Geo\Address\BaseAddress
	 */
	protected $address;

	/**
	 * @var string
	 */
	protected $status = self::ERROR;

	/**
	 * @var array
	 */
	protected $messages = [];

	/**
	 * @param \Rbs\Geo\Address\AddressInterface $data
	 */
	public function __construct($data = null)
	{
		if ($data !== null)
		{
			$this->address = new \Rbs\Geo\Address\BaseAddress($data);
			$this->status = static::SUCCESS;
		}
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return $this
	 */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return \Rbs\Geo\Address\BaseAddress
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param \Rbs\Geo\Address\BaseAddress $address
	 * @return $this
	 */
	public function setAddress($address)
	{
		$this->address = $address;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getMessages()
	{
		return $this->messages;
	}

	/**
	 * @param array $messages
	 * @return $this
	 */
	public function setMessages($messages)
	{
		$this->messages = $messages;
		return $this;
	}

	/**
	 * @param string $message
	 * @return $this
	 */
	public function addMessage($message)
	{
		$this->messages[] = $message;
		return $this;
	}

}