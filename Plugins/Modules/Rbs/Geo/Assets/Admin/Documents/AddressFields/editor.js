(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsGeoAddressFieldsNew', rbsDocumentEditorRbsGeoAddressFieldsNew);
	function rbsDocumentEditorRbsGeoAddressFieldsNew() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onLoad = function() {
					var LCID = scope.document.refLCID;
					scope.document.fields = [];
					angular.forEach(['street', 'zipCode', 'locality', 'countryCode'], function(name) {
						var f = {
							model: 'Rbs_Geo_AddressField', locked: true, code: name, label: name,
							required: true, collectionCode: null, defaultValue: null, match: null,
							LCID: {}
						};
						f.LCID[LCID] = {
							title: name, matchErrorMessage: null, LCID: LCID
						};
						if (name === 'countryCode') {
							f.collectionCode = 'Rbs_Geo_Collection_Countries';
						}
						scope.document.fields.push(f);
					});
					scope.document.fieldsLayout = [['street'], ['zipCode', 'locality'], ['country']];
				};
			}
		};
	}

	app.directive('rbsDocumentEditorRbsGeoAddressFieldsEdit', rbsDocumentEditorRbsGeoAddressFieldsEdit);
	function rbsDocumentEditorRbsGeoAddressFieldsEdit() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.onReady = function() {
					if (!angular.isArray(scope.document.fields)) {
						scope.document.fields = [];
					}
					if (!angular.isArray(scope.document.fieldsLayout)) {
						scope.document.fieldsLayout = [];
					}
				};

				scope.canDeleteItem = function(field) {
					return field && !field.locked;
				}
			}
		};
	}

	app.directive('rbsAddressComposer', ['$rootScope', 'RbsChange.ArrayUtils', rbsAddressComposer]);
	function rbsAddressComposer($rootScope, ArrayUtils) {
		return {
			restrict: 'E',
			templateUrl: 'Rbs/Geo/Documents/AddressFields/address-composer.twig',
			scope: {
				'fields': "=",
				'fieldsLayout': "="
			},
			link: function(scope) {
				scope.templateLayout = [];
				scope.sandBoxFields = [];

				scope.draggedElement = { name: null };

				scope.$watch('fields', function() {
					buildSandBoxFields();
				}, true);

				scope.$watch('fieldsLayout', function() {
					buildTemplateLayout();
				});

				function buildTemplateLayout() {
					scope.templateLayout = [];
					if (angular.isArray(scope.fieldsLayout)) {
						for (var i = 0; i < scope.fieldsLayout.length; i++) {
							scope.templateLayout.push([]);
							var row = scope.fieldsLayout[i];
							for (var y = 0; y < row.length; y++) {
								var name = row[y], field = { name: name, label: name, row: i, col: y };
								for (var z = 0; z < scope.sandBoxFields.length; z++) {
									if (name == scope.sandBoxFields[z].code) {
										field.label = scope.sandBoxFields[z].label;
										break;
									}
								}
								scope.templateLayout[i].push(field);
							}
						}
					}
				}

				function buildSandBoxFields() {
					scope.sandBoxFields = [];
					angular.forEach(scope.fields, function(field) {
						var sandBoxField = { code: field.code, label: field.label};
						if (sandBoxField.code == 'countryCode') {
							sandBoxField.code = 'country';
						}
						scope.sandBoxFields.push(sandBoxField);
						if (field.collectionCode && field.collectionCode && field.code != 'countryCode') {
							var fieldValue = { code: field.code + 'Value' , label: field.label + ' (Val)'};
							scope.sandBoxFields.push(fieldValue);
						}
					})
				}

				scope.onDrop = function(name, fromRow, fromCol, rowIndex, colIndex) {
					var s1 = [], i, added = false;
					if (name == 'countryCode') {
						name = 'country';
					}

					if (fromRow != -1) {
						//Remove
						ArrayUtils.remove(scope.fieldsLayout[fromRow], fromCol, fromCol);
						if (fromRow == rowIndex && colIndex > fromCol) {
							colIndex--;
						}
					}
					if (rowIndex == -2) {
						//Add in first line
						s1.push([name]);
						for (i = 0; i < scope.fieldsLayout.length; i++) {
							s1.push(scope.fieldsLayout[i]);
						}
						scope.fieldsLayout = s1;
					} else if (rowIndex != -1) {
						if (colIndex == -1) {
							//Insert new Line
							for (i = 0; i < scope.fieldsLayout.length; i++) {
								if (rowIndex == i) {
									s1.push([name]);
									added = true;
								}
								s1.push(scope.fieldsLayout[i]);
							}
							if (!added) {
								s1.push([name]);
							}
							scope.fieldsLayout = s1;
						}
						else {
							for (i = 0; i < scope.fieldsLayout[rowIndex].length; i++) {
								if (colIndex == i) {
									s1.push(name);
									added = true;
								}
								s1.push(scope.fieldsLayout[rowIndex][i]);
							}
							if (!added) {
								s1.push(name);
							}
							scope.fieldsLayout[rowIndex] = s1;
						}
					}
					s1 = [];
					for (i = 0; i < scope.fieldsLayout.length; i++) {
						if (scope.fieldsLayout[i].length) {
							s1.push(scope.fieldsLayout[i]);
						}
					}
					scope.fieldsLayout = s1;

					$rootScope.$digest();
				}
			}
		};
	}

	app.directive('addressField', ['$rootScope', addressField]);
	function addressField($rootScope) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				angular.element(element).attr('draggable', 'true');

				element.bind('dragstart', function(event) {
					var id = scope.field.code || scope.field.name;
					scope.draggedElement.name = id;
					if (attrs.sandBox) {
						scope.draggedElement.mode = 'copy';
						scope.draggedElement.row = -1;
						scope.draggedElement.col = -1;
					}
					else {
						scope.draggedElement.mode = 'move';
						scope.draggedElement.row = parseInt(attrs.fieldRow, 10);
						scope.draggedElement.col = parseInt(attrs.fieldCol, 10);
					}

					event.originalEvent.dataTransfer.setData('text', id);
					$rootScope.$emit('ADDR-DRAG-START');
				});

				element.bind('dragend', function() {
					$rootScope.$emit('ADDR-DRAG-END');
					scope.draggedElement.name = null;
					scope.draggedElement.mode = null;
				});
			}
		};
	}

	app.directive('fieldRowDrop', ['$rootScope', fieldRowDrop]);
	function fieldRowDrop($rootScope) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var rowIndex = parseInt(attrs.fieldRow, 10);
				var colIndex = parseInt(attrs.fieldCol, 10);

				element.bind('dragenter', function(event) {
					event.preventDefault();
					event.stopPropagation();
					angular.element(element).addClass('drop-over');
				});

				element.bind('dragleave', function() {
					angular.element(element).removeClass('drop-over');
				});

				element.bind('dragover', function(event) {
					if (scope.draggedElement.name) {
						if (scope.draggedElement.row != rowIndex) {
							event.preventDefault();
							event.originalEvent.dataTransfer.dropEffect = scope.draggedElement.mode;
							angular.element(element).addClass('drop-over');
							return;
						}
						else {
							if (scope.draggedElement.col != colIndex && scope.draggedElement.col + 1 != colIndex) {
								event.preventDefault();
								event.originalEvent.dataTransfer.dropEffect = scope.draggedElement.mode;
								angular.element(element).addClass('drop-over');
								return;
							}
						}
					}
					angular.element(element).removeClass('drop-over');
				});

				element.bind("drop", function(event) {
					event.preventDefault();
					angular.element(element).removeClass('drop-over');
					scope.onDrop(scope.draggedElement.name, scope.draggedElement.row, scope.draggedElement.col, rowIndex,
						colIndex);
				});

				$rootScope.$on('ADDR-DRAG-START', function() {
				});

				$rootScope.$on('ADDR-DRAG-END', function() {
					angular.element(element).removeClass("drop-over");
				});
			}
		};
	}
})();