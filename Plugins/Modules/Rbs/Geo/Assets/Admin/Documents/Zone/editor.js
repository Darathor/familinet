(function (jQuery) {
	"use strict";

	var app = angular.module('RbsChange');

	function rbsDocumentEditorRbsGeoZone(REST) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, element, attrs, editorCtrl) {

				REST.call(REST.getBaseUrl('Rbs/Geo/addressFiltersDefinition')).then(function (data) {
					scope.addressFiltersDefinition = data;
				}, function (error) {
					scope.addressFiltersDefinition = null;
				});
				scope.onLoad = function() {
					if (!angular.isObject(scope.document.addressFilterData) || angular.isArray(scope.document.addressFilterData)) {
						scope.document.addressFilterData = {};
					}
				};
			}
		}
	}
	rbsDocumentEditorRbsGeoZone.$inject = ['RbsChange.REST'];
	app.directive('rbsDocumentEditorRbsGeoZoneNew', rbsDocumentEditorRbsGeoZone);
	app.directive('rbsDocumentEditorRbsGeoZoneEdit', rbsDocumentEditorRbsGeoZone);

	app.directive('rbsGeoAddressFilterCountryField', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl : 'Rbs/Geo/Documents/Zone/rbs-geo-address-filter-country-field.twig',
			scope: {
				filter : '=', contextKey: "@"
			},
			link: function(scope, element, attrs, containerController) {

				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						return !!(scope.filter.parameters.value);
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});
			}
		};
	});

	app.directive('rbsGeoAddressFilterZipField', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl : 'Rbs/Geo/Documents/Zone/rbs-geo-address-filter-zip-field.twig',
			scope: {
				filter : '=', contextKey: "@"
			},
			link: function(scope, element, attrs, containerController) {
				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'match';
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						return !!(scope.filter.parameters.value);
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});
			}
		};
	});

	app.directive('rbsGeoAddressFilterField', function() {
		return {
			restrict: 'A',
			require: '^rbsFiltersContainer',
			templateUrl : 'Rbs/Geo/Documents/Zone/rbs-geo-address-filter-field.twig',
			scope: {
				filter : '=', contextKey: "@"
			},
			link: function(scope, element, attrs, containerController) {

				if (scope.filter) {
					if (!scope.filter.parameters.operator) {
						scope.filter.parameters.operator = 'eq';
					}
					if (!scope.filter.parameters.fieldName) {
						scope.filter.parameters.fieldName = null;
					}
					containerController.linkNode(scope);
				}

				scope.isConfigured = function() {
					if (scope.filter) {
						var p = scope.filter.parameters;
						return !!(p.operator && p.fieldName && (p.operator === 'isNull' || p.value));
					}
					return false;
				};

				scope.$watch('isConfigured()', function(configured, old) {
					containerController.applyConfigured(element, configured, old);
				});

				scope.$watch("filter.parameters.operator", function(op) {
					if (op === 'isNull') {
						scope.filter.parameters.value = null;
					}
				})
			}
		};
	});
})(window.jQuery);