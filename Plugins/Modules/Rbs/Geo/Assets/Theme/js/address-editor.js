(function() {
	"use strict";
	var app = angular.module('RbsChangeApp');

	app.directive('rbsGeoAddressEditor', ['RbsChange.AjaxAPI', rbsGeoAddressEditor]);

	var blockId = 0;

	function rbsGeoAddressEditor(AjaxAPI) {
		return {
			restrict: 'A',
			scope: {
				'address': '=',
				'valid': '=',
				'zoneCode': '=',
				'addresses': '<?',
				'precheckSaveAddress': '<?'
			},
			templateUrl: '/rbs-geo-address-editor.twig',

			link: function(scope, element, attributes) {
				scope.blockId = blockId++;
				scope.manageName = 'none';
				scope.countries = [];
				scope.fieldsDef = [];

				if (!scope.address) {
					scope.address = { common: { addressFieldsId: null }, fields: { countryCode: null } };
				}
				else {
					if (!angular.isObject(scope.address.common)) {
						scope.address.common = { addressFieldsId: null };
					}
					if (!angular.isObject(scope.address.fields)) {
						scope.address.fields = { countryCode: null };
					}
				}

				if (attributes.hasOwnProperty('manageName')) {
					if (attributes.manageName == 'optional' || attributes.manageName == 'none') {
						scope.manageName = attributes.manageName;
						if (scope.precheckSaveAddress && attributes.manageName == 'optional') {
							scope.address.common.useName = true;
						}
					}
					else {
						scope.manageName = 'required';
					}
				}
				else {
					scope.manageName = 'none';
				}

				if (scope.precheckSaveAddress) {
					if (!scope.address.common.name) {
						var defaultAddressName = element.find('[data-default-address-name]').attr('data-default-address-name');
						scope.address.common.name = generateAddressName(defaultAddressName, 1);
					}
				}

				function generateAddressName(defaultAddressName, increment) {
					var addressName = defaultAddressName + (increment > 1 ? ' ' + increment : '');
					if (angular.isArray(scope.addresses)) {
						for (var i = 0; i < scope.addresses.length; i++) {
							if (addressName == scope.addresses[i].common.name) {
								return generateAddressName(defaultAddressName, increment + 1);
							}
						}
					}
					return addressName;
				}

				scope.$watch('zoneCode', function(zoneCode) {
					AjaxAPI.getData('Rbs/Geo/AddressFieldsCountries/', { zoneCode: zoneCode }).then(
						function(result) {
							scope.countries = result.data.items;
							var countryCode = scope.address.fields.countryCode;
							if (countryCode) {
								scope.address.common.addressFieldsId = scope.getAddressFieldsId(countryCode);
							}
						},
						function(result) {
							console.log('addressFieldsCountries error', result);
							scope.countries = [];
						}
					);
				});

				scope.countryTitle = function(countryCode) {
					for (var i = 0; i < scope.countries.length; i++) {
						if (scope.countries[i]['common'].code == countryCode) {
							return scope.countries[i]['common'].title;
						}
					}
					return countryCode;
				};

				scope.getAddressFieldsId = function(countryCode) {
					for (var i = 0; i < scope.countries.length; i++) {
						if (scope.countries[i]['common'].code == countryCode) {
							return scope.countries[i]['common'].addressFieldsId;
						}
					}
					return null;
				};

				scope.$watch('countries', function(newValue) {
					if (angular.isArray(newValue) && newValue.length) {
						var addressFieldsId = null, code = null;
						angular.forEach(newValue, function(country) {
							if (code === null) {
								code = country.common.code;
							}
							else if (code !== country.common.code) {
								code = false;
							}

							if (addressFieldsId === null) {
								addressFieldsId = country.common.addressFieldsId;
							}
							else if (addressFieldsId !== country.common.addressFieldsId) {
								addressFieldsId = false;
							}
						});

						if (code) {
							scope.address.fields.countryCode = code;
						}
						else if (addressFieldsId) {
							scope.address.common.addressFieldsId = addressFieldsId;
						}
					}
				});

				scope.$watch('address.fields.countryCode', function(newValue) {
					if (newValue) {
						var addressFieldsId = scope.getAddressFieldsId(newValue);
						if (addressFieldsId) {
							scope.address.common.addressFieldsId = addressFieldsId;
						}
					}
				});

				scope.$watch('address.common.addressFieldsId', function(newValue) {
					if (newValue) {
						AjaxAPI.getData('Rbs/Geo/AddressFields/' + newValue, {}).then(
							function(result) {
								scope.generateFieldsEditor(result.data.dataSets);
							},
							function(result) {
								console.log('addressFields error', result);
								scope.fieldsDef = [];
							}
						);
					}
				});

				scope.$watch('addressForm.$invalid', function(newValue) {
					scope.valid = !newValue;
				});

				scope.generateFieldsEditor = function(addressFields) {
					var fieldsDef = addressFields.fields;
					if (angular.isObject(fieldsDef)) {
						scope.fieldsDef = [];
						var field;
						for (var i = 0; i < fieldsDef.length; i++) {
							field = fieldsDef[i];
							if (field.name != 'countryCode') {
								scope.fieldsDef.push(field);
								var v = null;
								if (scope.address.fields.hasOwnProperty(field.name)) {
									v = scope.address.fields[field.name];
								}
								if (v === null) {
									v = field.defaultValue;
									scope.address.fields[field.name] = v;
								}
							}
						}
					}
				};
			}
		}
	}

	app.directive('rbsGeoAddressUniqueName', function() {
		return {
			restrict: 'A',
			scope: false,
			require: 'ngModel',
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$validators.geoAddressUniqueName = function(modelValue) {
					if (!angular.isArray(scope.addresses)) {
						return true;
					}
					for (var i = 0; i < scope.addresses.length; i++) {
						if (scope.address == scope.addresses[i]) {
							continue;
						}

						if (modelValue == scope.addresses[i].common.name) {
							return false;
						}
					}
					return true;
				};
			}
		};
	});

})();