(function() {
	"use strict";

	var app = angular.module('RbsChangeApp');
	app.controller('rbsGeoManageAddressesController', ['$scope', 'RbsChange.AjaxAPI', '$element', rbsGeoManageAddressesController]);
	function rbsGeoManageAddressesController(scope, AjaxAPI, element) {
		scope.data = {
			addresses: [],
			newAddress: null,
			isNewAddressValid: false,
			editedAddress: null,
			isEditedAddressValid: false
		};

		function loadAddresses() {
			AjaxAPI.getData('Rbs/Geo/Address/', {}).then(
				function(result) {
					scope.data.addresses = result.data.items;
				},
				function(result) {
					scope.data.addresses = [];
					console.log('loadAddresses error', result);
				}
			);
		}

		scope.openEditAddressForm = function(address) {
			scope.data.editedAddress = address;
		};

		scope.cancelEdition = function() {
			scope.data.editedAddress = null;
		};

		scope.setDefaultAddress = function(address, defaultFor) {
			var id = address.common.id;
			address.default[defaultFor] = true;
			AjaxAPI.putData('Rbs/Geo/Address/' + id, address).then(
				function() {
					loadAddresses();
				},
				function(result) {
					console.log('setDefaultAddress error', result);
				}
			);
		};

		scope.updateAddress = function() {
			var id = scope.data.editedAddress.common.id;
			AjaxAPI.putData('Rbs/Geo/Address/' + id, scope.data.editedAddress).then(
				function(result) {
					var addedAddress = result.data.dataSets;
					var addresses = [];
					angular.forEach(scope.data.addresses, function(address) {
						if (address.common.id == id) {
							addresses.push(addedAddress);
						}
						else {
							addresses.push(address);
						}
					});
					scope.data.addresses = addresses;
					scope.data.editedAddress = null;
				},
				function(result) {
					console.log('updateAddress error', result);
				}
			);
		};

		scope.deleteAddress = function(address) {
			var id = address.common.id;
			AjaxAPI.deleteData('Rbs/Geo/Address/' + id, scope.data.editedAddress).then(
				function() {
					var addresses = [];
					angular.forEach(scope.data.addresses, function(address) {
						if (address.common.id != id) {
							addresses.push(address);
						}
					});
					scope.data.addresses = addresses;
				},
				function(result) {
					console.log('deleteAddress error', result);
				}
			);
		};

		scope.openNewAddressForm = function() {
			scope.data.newAddress = {
				common: { name: null },
				fields: {}
			};
			var offset = element.offset();
			if (offset && offset.hasOwnProperty('top')) {
				jQuery('html, body').animate({ scrollTop: offset.top - 20 }, 500);
			}
		};

		scope.clearAddress = function() {
			scope.data.newAddress.fields = {
				countryCode: scope.data.newAddress.fields.countryCode
			};
		};

		scope.cancelCreation = function() {
			scope.data.newAddress = null;
		};

		scope.addNewAddress = function() {
			AjaxAPI.postData('Rbs/Geo/Address/', scope.data.newAddress).then(
				function(result) {
					var addedAddress = result.data.dataSets;
					scope.data.addresses.push(addedAddress);
					scope.data.newAddress = null;
				},
				function(result) {
					console.log('addNewAddress error', result);
				}
			);
		};

		loadAddresses();
	}
})();

