<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Geo\Documents;

use Change\Documents\Events\Event as DocumentEvent;
use Change\Documents\Events;

/**
 * @name \Rbs\Geo\Documents\Address
 */
class Address extends \Compilation\Rbs\Geo\Documents\Address implements \Rbs\Geo\Address\AddressInterface
{

	public static $FIELDS_PROPERTY = ['zipCode', 'locality', 'countryCode', 'territorialUnitCode'];

	/**
	 * @var array
	 */
	private $fieldsValue;

	/**
	 * @return array
	 */
	protected function getFieldsValue()
	{
		if ($this->fieldsValue === null)
		{
			$this->fieldsValue = $this->getFieldsData() ?: [];
		}
		return $this->fieldsValue;
	}

	/**
	 * @return null|string
	 */
	public function getStreet()
	{
		$fieldsValue = $this->getFieldsValue();

		return $fieldsValue[\Rbs\Geo\Address\AddressInterface::STREET_FIELD_NAME] ?? null;
	}

	/**
	 * @param array $fieldsValue
	 * @return $this
	 */
	protected function setFieldsValue($fieldsValue)
	{
		$this->fieldsValue = $fieldsValue;
		return $this;
	}

	/**
	 * @api
	 * @param $fieldName
	 * @param $fieldValue
	 */
	public function setFieldValue($fieldName, $fieldValue)
	{
		if (is_string($fieldName))
		{
			if (in_array($fieldName, static::$FIELDS_PROPERTY))
			{
				$this->getDocumentModel()->setPropertyValue($this, $fieldName, $fieldValue);
			}
			else
			{
				$fieldsValue = $this->getFieldsValue();
				if ($fieldValue === null)
				{
					unset($fieldsValue[$fieldName]);
				}
				else
				{
					$fieldsValue[$fieldName] = $fieldValue;
				}
				$this->setFieldsValue($fieldsValue);
			}
		}
	}

	/**
	 * @api
	 * @return $this
	 */
	public function clearFieldsValue()
	{
		return $this->setFieldsValue([]);
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(DocumentEvent::EVENT_CREATE, [$this, 'onDefaultSave'], 10);
		$eventManager->attach(DocumentEvent::EVENT_UPDATE, [$this, 'onDefaultSave'], 10);
	}

	protected function setWrappedFields()
	{
		if (is_array($this->fieldsValue))
		{
			$this->setFieldsData($this->fieldsValue ?: null);
			$this->fieldsValue = null;
		}
	}

	/**
	 * @api
	 * @return string[]
	 */
	public function getModifiedPropertyNames()
	{
		$this->setWrappedFields();
		return parent::getModifiedPropertyNames();
	}

	/**
	 * @param DocumentEvent $event
	 */
	public function onDefaultSave(DocumentEvent $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}
		$modifiedPropertyNames = $this->getModifiedPropertyNames() ?: [];
		if ($modifiedPropertyNames
			&& array_intersect(array_merge(static::$FIELDS_PROPERTY, ['fieldsData', 'addressFields']), $modifiedPropertyNames)
		)
		{
			$fields = $this->getFields();
			$cleanFieldsValue = [];
			$af = $this->getAddressFields();
			if ($af)
			{
				$constraintManager = $event->getApplicationServices()->getConstraintsManager();
				$i18nManager = $event->getApplicationServices()->getI18nManager();
				$propertiesErrors = $event->getParam('propertiesErrors');
				if (!is_array($propertiesErrors))
				{
					$propertiesErrors = [];
				}

				foreach ($af->getFields() as $addressField)
				{
					$fieldName = $addressField->getCode();

					$value = $fields[$fieldName] ?? null;
					if ($value === null && $addressField->getRequired())
					{
						$propertiesErrors[$fieldName][] = $i18nManager->trans('c.constraints.isempty', ['ucf']);
						continue;
					}

					$match = $addressField->getMatch();
					if ($match && $value !== null)
					{
						$c = $constraintManager->matches('/' . str_replace('/', '\/', $match) . '/');
						if (!$c->isValid($value))
						{
							foreach ($c->getMessages() as $error)
							{
								if ($error !== null)
								{
									$propertiesErrors[$fieldName][] = $error;
								}
							}
							continue;
						}
					}

					if (in_array($fieldName, static::$FIELDS_PROPERTY))
					{
						$this->getDocumentModel()->setPropertyValue($this, $fieldName, $value);
					}
					elseif ($value !== null)
					{
						$cleanFieldsValue[$fieldName] = $value;
					}
				}
				$event->setParam('propertiesErrors', count($propertiesErrors) ? $propertiesErrors : null);
			}

			$genericServices = $event->getServices('genericServices');
			if ($genericServices instanceof \Rbs\Generic\GenericServices)
			{
				$cleanFieldsValue['__lines'] = $genericServices->getGeoManager()->getFormattedAddress($this);
			}

			$this->setFieldsValue(null);
			$this->setFieldsData($cleanFieldsValue ?: null);
		}
	}

	/**
	 * @return array
	 */
	public function getFields()
	{
		$array = $this->getFieldsValue();
		unset($array['__lines']);
		$array['countryCode'] = $this->getCountryCode();
		$array['zipCode'] = $this->getZipCode();
		$array['locality'] = $this->getLocality();
		$array['territorialUnitCode'] = $this->getTerritorialUnitCode();
		return $array;
	}

	/**
	 * @return string
	 */
	public function getLabel()
	{
		return $this->getName();
	}

	/**
	 * @param string $value
	 * @return $this
	 */
	public function setLabel($value)
	{
		// not implemented
		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getLines()
	{
		$values = $this->getFieldsValue();
		return (isset($values['__lines']) && is_array($values['__lines'])) ? $values['__lines'] : [];
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$fields = $this->getFields();
		$array = ['common' => ['id' => $this->getId(), 'addressFieldsId' => $this->getAddressFieldsId(),
			'name' => $this->getName()]];
		$array['fields'] = $fields;
		$array['lines'] = $this->getLines();
		return $array;
	}

	/**
	 * @return array
	 */
	public function toFlatArray()
	{
		$array = $this->getFields();
		$array['__id'] = $this->getId();
		$array['__addressFieldsId'] = $this->getAddressFieldsId();
		$array['__lines'] = $this->getLines();
		$array['__name'] = $this->getName();
		return $array;
	}

	/**
	 * @param Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);
		$address = $event->getDocument();
		if (!$address instanceof Address)
		{
			return;
		}
		$documentResult = $event->getParam('restResult');
		if ($documentResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$pc = new \Change\Http\Rest\V1\ValueConverter($documentResult->getUrlManager(),
				$event->getApplicationServices()->getDocumentManager());
			$documentResult->setProperty('fieldValues',
				$pc->toRestValue($address->getFields(), \Change\Documents\Property::TYPE_JSON));
			$documentResult->setProperty('lines', $address->getLines());

			$properties = $documentResult->getProperties();
			foreach (static::$FIELDS_PROPERTY as $fieldProperty)
			{
				unset($properties[$fieldProperty]);
			}
			$documentResult->setProperties($properties);
		}
		elseif ($documentResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$extraColumn = $event->getParam('extraColumn');
			if (is_array($extraColumn) && in_array('fieldValues', $extraColumn))
			{
				$pc = new \Change\Http\Rest\V1\ValueConverter($documentResult->getUrlManager(),
					$event->getApplicationServices()->getDocumentManager());
				$documentResult->setProperty('fieldValues',
					$pc->toRestValue($address->getFields(), \Change\Documents\Property::TYPE_JSON));
			}
		}
	}

	/**
	 * Process the incoming REST data $name and set it to $value
	 * @param string $name
	 * @param mixed $value
	 * @param \Change\Http\Event $event
	 * @return boolean
	 */
	protected function processRestData($name, $value, \Change\Http\Event $event)
	{
		if ($name === 'fieldValues')
		{
			$pc = new \Change\Http\Rest\V1\ValueConverter($event->getUrlManager(),
				$event->getApplicationServices()->getDocumentManager());
			$fieldValues = $pc->toPropertyValue($value, \Change\Documents\Property::TYPE_JSON);
			$fieldValues = is_array($fieldValues) ? $fieldValues : [];
			foreach ($fieldValues as $n => $v)
			{
				$this->setFieldValue($n, $v);
			}
			return true;
		}
		if (in_array($name, static::$FIELDS_PROPERTY))
		{
			return true;
		}
		return parent::processRestData($name, $value, $event);
	}
}
