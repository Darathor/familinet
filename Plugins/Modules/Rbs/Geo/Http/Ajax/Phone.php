<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Geo\Http\Ajax;

/**
 * @name \Rbs\Geo\Http\Ajax\Phone
 */
class Phone
{
	/**
	 * Default actionPath: Rbs/Geo/Phone/NumberConfiguration/
	 * Event params:
	 * @param \Change\Http\Event $event
	 */
	public function getNumberConfiguration(\Change\Http\Event $event)
	{
		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$geoManager = $genericServices->getGeoManager();
		$config = $geoManager->getNumberConfiguration($event);
		ksort($config);
		$data = [];
		foreach ($config as $item) {
			$data[] = array_values($item);
		}
		$result = new \Change\Http\Ajax\V1\ItemsResult('Rbs/Geo/Phone/MobileConfiguration/', $data);
		$result->setPaginationCount(count($config));
		$event->setResult($result);
	}

	/**
	 * Default actionPath: Rbs/Geo/Phone/ParsePhoneNumber
	 * Event params:
	 *  -data
	 *    -number
	 *    -[regionCode]
	 * @param \Change\Http\Event $event
	 */
	public function parsePhoneNumber(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$data = $event->getParam('data');
		if ($data && isset($data['number']) && $data['number'])
		{
			/** @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$geoManager = $genericServices->getGeoManager();
			$parsedNumber = $geoManager->parsePhoneNumber($i18nManager, $data['number'], $data['regionCode']);
			if ($parsedNumber)
			{
				$dataSet = ['common' => $parsedNumber];
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/Geo/Phone/parsePhoneNumber', $dataSet);
				$event->setResult($result);
				return;
			}
		}
		$message = $i18nManager->trans('m.rbs.geo.front.invalid_mobile_number', ['ucf']);
		$result =
			new \Change\Http\Ajax\V1\ErrorResult('INVALID_MOBILE_NUMBER', $message, \Zend\Http\Response::STATUS_CODE_409);
		$event->setResult($result);
	}
}