<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Geo\Setup;

/**
 * @name \Rbs\Geo\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		parent::executeApplication($plugin, $application, $configuration);
		// Patch.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Geo', \Rbs\Geo\Setup\Patch\Listeners::class);
		$configuration->addPersistentEntry('Rbs/Geo/OSM/url', null);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Rbs_Geo_Collection_UnitType') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				$i18n = $applicationServices->getI18nManager();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Territorial Unit Types');
				$collection->setCode('Rbs_Geo_Collection_UnitType');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('STATE');
				$item->getCurrentLocalization()
					->setTitle($i18n->trans('m.rbs.geo.documents.territorialunit_unit_state', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('DEPARTEMENT');
				$item->getCurrentLocalization()
					->setTitle($i18n->trans('m.rbs.geo.documents.territorialunit_unit_departement', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('REGION');
				$item->getCurrentLocalization()
					->setTitle($i18n->trans('m.rbs.geo.documents.territorialunit_unit_region', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('COUNTY');
				$item->getCurrentLocalization()
					->setTitle($i18n->trans('m.rbs.geo.documents.territorialunit_unit_county', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('PROVINCE');
				$item->getCurrentLocalization()
					->setTitle($i18n->trans('m.rbs.geo.documents.territorialunit_unit_province', ['ucf']));

				$collection->getItems()->add($item);

				$collection->save();

				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}

		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			$path = dirname(__DIR__) . '/Collection/Assets/countries.json';

			$allCountries = json_decode(file_get_contents($path), true);
			$activable = ['FR', 'DE', 'CH', 'BE', 'LU', 'IT', 'ES', 'GB', 'US', 'CA', 'PT', 'NL', 'AT'];
			foreach ($allCountries as $code => $data)
			{
				$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_Country');
				$query->andPredicates($query->eq('code', $code));
				/* @var $country \Rbs\geo\Documents\Country */
				$country = $query->getFirstDocument();
				if ($country === null)
				{
					$country = $applicationServices->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Geo_Country');
					$country->setCode($code);
					$country->setIsoCode($data['isoCode']);
					$country->setLabel(mb_strtoupper($data['label']));
					if (!in_array($code, $activable))
					{
						$country->setActive(false);
					}
					$country->save();
				}
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$models = json_decode(file_get_contents(__DIR__ . '/Assets/addressFields.json'), 1);
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			foreach ($models as $model)
			{
				$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_AddressFields');
				$query->andPredicates($query->eq('label', $model['label']));
				if (!$query->getFirstDocument())
				{
					/* @var $fields \Rbs\geo\Documents\AddressFields */
					$fields =
						$applicationServices->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Geo_AddressFields');

					$fields->setLabel($model['label']);
					foreach ($model['fields'] as $fieldData)
					{
						$field = $fields->newAddressField();
						foreach ($fieldData as $propertyName => $propertyValue)
						{
							$field->getDocumentModel()->setPropertyValue($field, $propertyName, $propertyValue);
							if ($propertyName == 'title')
							{
								$field->getDocumentModel()->setPropertyValue($field, 'label', $propertyValue);
							}
						}
						$fields->getFields()->add($field);
					}
					if (isset($model['fieldsLayoutData']))
					{
						$fields->setFieldsLayoutData($model['fieldsLayoutData']);
					}
					$fields->create();

					if (isset($model['countryCode']))
					{
						if ($model['countryCode'] === true)
						{
							$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_Country');
							$query->andPredicates($query->isNull('addressFields'));

							/* @var $country \Rbs\geo\Documents\Country */
							foreach ($query->getDocuments() as $country)
							{
								$country->setAddressFields($fields);
								$country->update();
							}
						}
						else
						{
							$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_Country');
							$query->andPredicates($query->eq('code', $model['countryCode']));

							/* @var $country \Rbs\geo\Documents\Country */
							$country = $query->getFirstDocument();
							if ($country)
							{
								$country->setAddressFields($fields);
								$country->update();
								$this->addDefaultZone($applicationServices, $country);
							}
						}
					}
				}
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param \Rbs\geo\Documents\Country $country
	 */
	protected function addDefaultZone($applicationServices, $country)
	{
		if ($country && $country->getCode() === 'FR')
		{
			$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Geo_Zone');
			$query->andPredicates($query->eq('code', 'FRC'));
			$FRCZone = $query->getFirstDocument();

			if (!$FRCZone)
			{
				/** @var $FRCZone \Rbs\Geo\Documents\Zone */
				$FRCZone = $applicationServices->getDocumentManager()->getNewDocumentInstanceByModelName('Rbs_Geo_Zone');
				$FRCZone->setCode('FRC');
				$FRCZone->setLabel('France continentale');
				$FRCZone->setCountry($country);
				$FRCZone->setAddressFilterData(json_decode('{"name":"group","parameters":{"operator":"AND"},"filters":[{"name":"countryCode","parameters":{"fieldName":"countryCode","operator":"eq","value":"FR"}},{"name":"zipCode","parameters":{"fieldName":"zipCode","operator":"match","value":"^((0[1-9])|([1345678][0-9])|(9[0-5])|(2[1-9]))[0-9]{3}$"}}]}',
					true));
				$FRCZone->save();
			}
		}
	}
}
