<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Documents;

/**
 * @name \Rbs\Mail\Documents\Mail
 */
class Mail extends \Compilation\Rbs\Mail\Documents\Mail implements \Change\Presentation\Interfaces\Page, \Change\Mail\MailInterface
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onNormalizeEditableContent($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onNormalizeEditableContent($event); }, 5);
		$eventManager->attach('getSubstitutions', function ($event) { $this->onGetSubstitutions($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onNormalizeEditableContent(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this || !$this->isPropertyModified('editableContent'))
		{
			return;
		}
		$contentLayout = new \Change\Presentation\Layout\Layout($this->getCurrentLocalization()->getEditableContent());
		$blocks = $contentLayout->getBlocks();
		$event->getApplicationServices()->getBlockManager()->normalizeBlocksParameters($blocks);
		$this->getCurrentLocalization()->setEditableContent($contentLayout->toArray());
	}

	/**
	 * @return string
	 */
	public function getIdentifier()
	{
		return $this->getId() . ',' . $this->getCurrentLCID();
	}

	/**
	 * @return \Datetime|null
	 */
	public function getModificationDate()
	{
		return $this->getCurrentLocalization()->getModificationDate();
	}

	/**
	 * @return \Change\Presentation\Layout\Layout
	 */
	public function getContentLayout()
	{
		$defaultDisplayColumnsFrom = $this->getTemplate() ? $this->getTemplate()->getDefaultDisplayColumnsFrom() : 'M';
		return new \Change\Presentation\Layout\Layout($this->getCurrentLocalization()->getEditableContent(), $defaultDisplayColumnsFrom);
	}

	/**
	 * @return string|null
	 */
	public function getTitle()
	{
		return $this->getCurrentLocalization()->getSubject();
	}

	/**
	 * @return \Change\Presentation\Interfaces\Section
	 */
	public function getSection()
	{
		//FIXME how to find the correct website in websites?
		return count($this->getWebsites()) ? $this->getWebsites()[0] : null;
	}

	protected $substitutions = null;
	/**
	 * @return array
	 */
	public function getSubstitutions()
	{
		if ($this->substitutions === null)
		{
			$this->substitutions = [];
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['substitutions' => null]);
			$eventManager->trigger('getSubstitutions', $this, $args);
			$this->substitutions = $args['substitutions'] ?? [];
		}
		return $this->substitutions;
	}

	/**
	 * Event param: array substitutions
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetSubstitutions(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('substitutions') !== null)
		{
			return;
		}
		$substitutions = [];
		$genericServices = $event->getServices('genericServices');
		if ($genericServices instanceof \Rbs\Generic\GenericServices)
		{
			$id= $this->getId();
			$notificationManager = $genericServices->getNotificationManager();

			$q = $this->getDocumentManager()->getNewQuery('Rbs_Notification_Configuration');
			/** @var \Rbs\Notification\Documents\Configuration[] $configurations */
			$configurations = $q->getDocuments()->preLoad()->toArray();
			foreach ($configurations as $configuration)
			{
				if (in_array($id, $configuration->getTemplateIds()))
				{
					$substitutions = array_merge($substitutions, $notificationManager->getSubstitutionLabels($configuration->getCode()));
				}
			}
		}
		$event->setParam('substitutions', $substitutions);
	}

	/**
	 * @param array $substitutions
	 * @return $this
	 */
	public function setSubstitutions($substitutions)
	{
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getIsVariation()
	{
		return $this->getVariantOfId() !== 0;
	}

	/**
	 * @param boolean $isVariation
	 * @return $this
	 */
	public function setIsVariation($isVariation)
	{
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->getCurrentLocalization()->getSubject() ?? $this->getRefLocalization()->getSubject();
	}

	/**
	 * @param \Rbs\Website\Documents\Website|int $website
	 * @return \Rbs\Mail\Documents\Mail
	 */
	public function getMailForWebsite($website)
	{
		if ((is_int($website) || $website instanceof \Rbs\Website\Documents\Website) && !$this->getVariantOf())
		{
			$q = $this->getDocumentManager()->getNewQuery($this->getDocumentModelName());
			$q->andPredicates($q->eq('variantOf', $this), $q->eq('websites', $website));
			/** @var \Rbs\Mail\Documents\Mail $mail */
			if ($mail = $q->getFirstDocument())
			{
				return $mail;
			}
		}
		return $this;
	}
}
