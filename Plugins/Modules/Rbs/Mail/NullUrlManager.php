<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail;

/**
 * @name \Rbs\Mail\NullUrlManager
 */
class NullUrlManager extends \Change\Http\Web\UrlManager
{
	/**
	 * @param \Change\Documents\AbstractDocument|int $document
	 * @param array $query
	 * @param null $LCID
	 * @return \Rbs\Mail\NullUri
	 */
	public function getCanonicalByDocument($document, $query = [], $LCID = null)
	{
		return new \Rbs\Mail\NullUri();
	}

	/**
	 * @param \Change\Documents\AbstractDocument|int $document
	 * @param string $functionName
	 * @param null $section
	 * @param array $query
	 * @param null $LCID
	 * @return \Rbs\Mail\NullUri
	 */
	public function getFunctionalByDocument($document, $functionName, $section = null, $query = [], $LCID = null)
	{
		return new \Rbs\Mail\NullUri();
	}

	/**
	 * @param \Change\Documents\AbstractDocument|int $document
	 * @param \Change\Presentation\Interfaces\Section|null $section
	 * @param array $query
	 * @param null $LCID
	 * @return \Rbs\Mail\NullUri
	 */
	public function getByDocument($document, $section, $query = [], $LCID = null)
	{
		return new \Rbs\Mail\NullUri();
	}

	/**
	 * @param string $functionCode
	 * @param array $query
	 * @param null $LCID
	 * @return null
	 */
	public function getByFunction($functionCode, $query = [], $LCID = null)
	{
		return null;
	}
}

class NullUri extends \Zend\Uri\Http
{
	/**
	 * @return $this
	 */
	public function normalize()
	{
		return $this;
	}

	/**
	 * @return string
	 */
	public function toString()
	{
		return '';
	}
}