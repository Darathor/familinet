<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Mail\Setup;

/**
 * @name \Rbs\Mail\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		parent::executeApplication($plugin, $application, $configuration);
		// Add patches.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Mail', \Rbs\Mail\Setup\Patch\Listeners::class);
	}

}
