(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsMediaFileNew', rbsDocumentEditorRbsMediaFile);
	app.directive('rbsDocumentEditorRbsMediaFileEdit', rbsDocumentEditorRbsMediaFile);

	function rbsDocumentEditorRbsMediaFile() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.$on('rbsUploader.loaded', function(event, data) {
					//noinspection JSUnresolvedVariable
					var fileName = data.fileName;
					if (fileName && !scope.document.label) {
						var array = fileName.split(new RegExp("[.]+", "g"));
						array.pop();
						scope.document.label = array.join('.');
					}
				});
			}
		};
	}
})();