(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsMediaImageNew', rbsDocumentEditorRbsMediaImage);
	app.directive('rbsDocumentEditorRbsMediaImageEdit', rbsDocumentEditorRbsMediaImage);

	function rbsDocumentEditorRbsMediaImage() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.$on('rbsUploader.loaded', function (event, data) {
					//noinspection JSUnresolvedVariable
					var fileName = data.fileName;
					if (fileName && !scope.document.label) {
						scope.document.label = fileName.replace(/(\.png|\.gif|\.jpg|\.jpeg)$/i, '');
					}
				});
			}
		};
	}
})();