(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsMediaVideoNew', rbsDocumentEditorRbsMediaVideo);
	app.directive('rbsDocumentEditorRbsMediaVideoEdit', rbsDocumentEditorRbsMediaVideo);

	function rbsDocumentEditorRbsMediaVideo() {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.$on('rbsUploader.loaded', function(event, data) {
					//noinspection JSUnresolvedVariable
					var fileName = data.fileName;
					if (fileName && !scope.document.label) {
						scope.document.label = fileName.replace(/(\.ogg|\.mp4|\.webm)$/i, '');
					}
				});
			}
		};
	}
})();