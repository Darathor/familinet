(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.run(['$templateCache', function($templateCache) {
		$templateCache.put(
			'picker-item-Rbs_Media_Image.html',
			'<span style="line-height: 30px"><img data-rbs-storage-image="item" data-thumbnail="XS"/> (= item.label =) ((= item.width =) &times; (= item.height =)) <small class="text-muted">(= item.model|rbsModelLabel =)</small></span>'
		);
	}]);
})();