(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsAvatar', ['RbsChange.REST', function(REST) {
		return {
			restrict: 'AE',
			templateUrl: 'Rbs/Media/avatar.twig',
			replace: 'true',
			scope: {
				size: '@',
				email: '@',
				userId: '@'
			},

			link: function(scope, elm, attrs) {
				var myRegex = /ng[A-Z].*/;

				var options = {};
				for (var key in attrs) {
					if (attrs.hasOwnProperty(key)) {
						var value = attrs[key];
						if (angular.isString(value) && key != 'size' && key != 'userId' && key != 'email' && !myRegex.test(key)) {
							options[key] = value;
						}
					}
				}

				var params = {
					size: scope.size,
					email: scope.email,
					userId: scope.userId,
					params: options
				};

				REST.call(REST.getBaseUrl('Rbs/Avatar'), params).then(
					function(response) {scope.src = response.href},
					function(result) { console.error(result); }
				);
			}
		};
	}]);
})();