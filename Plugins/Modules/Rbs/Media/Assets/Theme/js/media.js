(function(jQuery) {
	"use strict";

	var app = angular.module('RbsChangeApp');

	/**
	 * @ngdoc directive
	 * @name RbsChangeApp.directive:rbsMediaPictograms
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * Renders a set of pictograms over its container (for example over the visual).
	 *
	 * @param {Array} rbsMediaPictograms The pictograms array.
	 * @param {string=} pictogramFormat The pictograms format name (`pictogram` by default).
	 * @param {string=} pictogramPosition The pictograms position in the container. The position is composed in two distinct
	 *    values for the vertical (`top`or `bottom`) and horizontal (`left`, `center` or `right`) positions separated by a space
	 *    (`bottom right` by default).
	 *
	 * @example
	 * ```html
	 * <div data-rbs-media-pictograms="pictograms" data-pictogram-format="myFormat" data-pictogram-position="top left"></div>
	 * ```
	 */
	app.directive('rbsMediaPictograms', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-media-pictograms.twig',
			scope: {
				mediaPictograms: '=rbsMediaPictograms'
			},
			link: function(scope, elm, attrs) {
				scope.format = angular.isString(attrs['pictogramFormat']) ? attrs['pictogramFormat'] : 'pictogram';

				scope.position = {
					vertical: 'bottom',
					horizontal: 'right'
				};

				scope.$watch('mediaPictograms', function(pictograms) {
					scope.pictograms = [];
					angular.forEach(pictograms, function(pictogram) {
						if (!angular.isObject(pictogram)) {
							return;
						}
						if (angular.isObject(pictogram['formats'])) {
							pictogram = pictogram['formats'];
						}
						if (angular.isString(pictogram[scope.format])) {
							scope.pictograms.push({
								'src': pictogram[scope.format],
								'alt': angular.isString(pictogram.alt) ? pictogram.alt : null
							});
						}
					});
				});

				var positionParts = angular.isString(attrs['pictogramPosition']) ? attrs['pictogramPosition'].split(' ') : [];
				for (var i = 0; i < positionParts.length; i++) {
					switch (positionParts[i]) {
						case 'top':
						case 'bottom':
							scope.position.vertical = positionParts[i];
							break;

						case 'left':
						case 'center':
						case 'right':
							scope.position.horizontal = positionParts[i];
							break;
					}
				}

				scope.ngClasses = {
					main: {},
					size: {},
					maxSize: {}
				};
				scope.ngClasses.main['media-pictograms-' + scope.position.vertical] = true;
				scope.ngClasses.main['media-pictograms-' + scope.position.horizontal] = true;
				scope.ngClasses.main['media-pictograms-format-' + scope.format] = true;
				scope.ngClasses.size['image-format-' + scope.format + '-size'] = true;
				scope.ngClasses.maxSize['image-format-' + scope.format + '-max-size'] = true;
			}
		}
	});

	/**
	 * @ngdoc directive
	 * @name RbsChangeApp.directive:rbsMediaVisuals
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * Renders a set of visuals with thumbnails, zoom and pictograms.
	 *
	 * @param {Array} rbsMediaVisuals The visuals array.
	 * @param {Array} pictograms The pictograms array (may be empty).
	 * @param {string=} enableZoom Set to `false` to disable zoom.
	 * @param {string=} visualFormat The main visual format name (`detail` by default).
	 * @param {string=} thumbnailFormat The thumbnails format name (`detailThumbnail` by default).
	 * @param {string=} thumbnailPosition The thumbnailPosition position around the main visual.
	 *    Allowed values are `top`, `right`, `bottom` and `left` (`right`by default).
	 * @param {string=} pictogramFormat The pictograms format name (`pictogram` by default).
	 * @param {string=} pictogramPosition The pictograms position over the main visual. The position is composed in two distinct
	 *    values for the vertical (`top`or `bottom`) and horizontal (`left`, `center` or `right`) positions separated by a space
	 *    (`bottom right` by default).
	 *
	 * @example
	 * ```html
	 * <div data-rbs-media-visuals="visuals" data-pictograms="[]" data-visual-format="myFormat" data-thumbnail-position="left" data-enable-zoom="false"></div>
	 * ```
	 */
	app.directive('rbsMediaVisuals', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-media-visuals.twig',
			scope: {
				mediaVisuals: '=rbsMediaVisuals',
				pictograms: '='
			},
			link: function(scope, elm, attrs) {
				scope.visualFormat = angular.isString(attrs['visualFormat']) ? attrs['visualFormat'] : 'detail';
				scope.thumbnailFormat = angular.isString(attrs['thumbnailFormat']) ? attrs['thumbnailFormat'] : 'detailThumbnail';
				scope.thumbnailPosition = angular.isString(attrs['thumbnailPosition']) ? attrs['thumbnailPosition'] : 'right';
				scope.pictogramFormat = angular.isString(attrs['pictogramFormat']) ? attrs['pictogramFormat'] : 'pictogram';
				scope.pictogramPosition = angular.isString(attrs['pictogramPosition']) ? attrs['pictogramPosition'] : null;

				scope.shownIndex = 0;
				scope.zoom = {
					enabled: attrs['enableZoom'] && attrs['enableZoom'] !== 'false',
					shown: false
				};

				scope.$watch('mediaVisuals', function(visuals) {
					scope.shownIndex = 0;
					scope.visuals = [];
					angular.forEach(visuals, function(visual) {
						if (!angular.isObject(visual)) {
							return;
						}
						if (angular.isObject(visual['formats'])) {
							visual = visual['formats'];
						}
						if (angular.isString(visual[scope.visualFormat]) && angular.isString(visual[scope.thumbnailFormat])) {
							scope.visuals.push({
								'title': angular.isString(visual.title) ? visual.title : (angular.isString(
									visual.alt) ? visual.alt : null),
								'src': visual[scope.visualFormat],
								'thumbnailSrc': visual[scope.thumbnailFormat],
								'originalSrc': angular.isString(visual['original']) ? visual['original'] : null,
								'alt': angular.isString(visual.alt) ? visual.alt : (angular.isString(
									visual.title) ? visual.title : null )
							});
						}
					});

					scope.ngClasses = {
						main: {}
					};
					if (angular.isArray(scope.visuals) && scope.visuals.length > 1) {
						scope.ngClasses.main['media-visuals-multiple'] = true;
						scope.ngClasses.main['media-visuals-multiple-' + scope.thumbnailPosition] = true;
					}
					else {
						scope.ngClasses.main['media-visuals-single'] = true;
					}
					scope.ngClasses.main['media-visuals-format-' + scope.visualFormat + '-' + scope.thumbnailFormat] = true;
				});

				scope.showVisual = function(event, index) {
					scope.shownIndex = index;
					event.preventDefault();
				};

				scope.startZoom = function() {
					scope.zoom.shown = true;

					var linkNode = elm.find('.media-visuals-main [data-index=' + scope.shownIndex + '] a');
					var image = linkNode.find('img');

					var zoomDiv = elm.find('.media-visuals-zoom');
					var bigImage = jQuery('<img src="' + linkNode.attr('href') + '" alt="" />');
					zoomDiv.append(bigImage);

					image.mousemove(function(event) {
						var scaleX = (bigImage.width() / image.width());
						var scaleY = (bigImage.height() / image.height());
						var offset = image.offset();
						bigImage.css({
							top: Math.max(zoomDiv.height() - bigImage.height(),
								Math.min(0, zoomDiv.height() / 2 - (event.pageY - offset.top) * scaleY)),
							left: Math.max(zoomDiv.width() - bigImage.width(),
								Math.min(0, zoomDiv.width() / 2 - (event.pageX - offset.left) * scaleX))
						});
					});

					image.mouseout(function() {
						scope.zoom.shown = false;
						bigImage.remove();
						scope.$digest();

						image.unbind('mousemove');
						image.unbind('mouseout');
					});

					// Disable the link on the image.
					linkNode.click(function(event) { event.preventDefault(); });
				};
			}
		}
	});

	/**
	 * @ngdoc directive
	 * @name RbsChangeApp.directive:rbsMediaSliderVisuals
	 * @restrict A
	 * @scope
	 *
	 * @description
	 * Renders a set of visuals as a slider.
	 *
	 * @param {Array} rbsMediaSliderVisuals The visuals array.
	 * @param {Array} pictograms The pictograms array (may be empty).
	 * @param {int=} interval The main visual format name (`5000` by default).
	 * @param {string=} visualFormat The main visual format name (`detailCompact` by default).
	 * @param {string=} pictogramFormat The pictograms format name (`pictogram` by default).
	 * @param {string=} pictogramPosition The pictograms position over the main visual. The position is composed in two distinct
	 *    values for the vertical (`top`or `bottom`) and horizontal (`left`, `center` or `right`) positions separated by a space
	 *    (`bottom right` by default).
	 *
	 * @example
	 * ```html
	 * <div data-rbs-media-slider-visuals="visuals" data-pictograms="[]" data-visual-format="myFormat" data-thumbnail-position="left"></div>
	 * ```
	 */
	app.directive('rbsMediaSliderVisuals', function() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-media-slider-visuals.twig',
			scope: {
				mediaVisuals: '=rbsMediaSliderVisuals',
				pictograms: '='
			},
			link: function(scope, elm, attrs) {
				scope.interval = angular.isString(attrs['interval']) ? attrs['interval'] : 5000;
				scope.visualFormat = angular.isString(attrs['visualFormat']) ? attrs['visualFormat'] : 'detailCompact';
				scope.pictogramFormat = angular.isString(attrs['pictogramFormat']) ? attrs['pictogramFormat'] : 'pictogram';
				scope.pictogramPosition = angular.isString(attrs['pictogramPosition']) ? attrs['pictogramPosition'] : null;

				var carouselElm = elm.find('.carousel');

				scope.left = function(event) {
					event.preventDefault();
					carouselElm.carousel('prev');
				};

				scope.right = function(event) {
					event.preventDefault();
					carouselElm.carousel('next');
				};

				scope.goTo = function(index) {
					carouselElm.carousel(index);
				};

				scope.$watch('mediaVisuals', function(visuals) {
					scope.visuals = [];
					angular.forEach(visuals, function(visual) {
						if (!angular.isObject(visual)) {
							return;
						}
						if (angular.isObject(visual['formats'])) {
							visual = visual['formats'];
						}
						if (angular.isString(visual[scope.visualFormat])) {
							scope.visuals.push({
								'src': visual[scope.visualFormat],
								'alt': angular.isString(visual.alt) ? visual.alt : null
							});
						}
					});

					scope.ngClasses = {
						main: {}
					};
					if (angular.isArray(scope.visuals) && scope.visuals.length > 1) {
						scope.ngClasses.main['media-slider-visuals-multiple'] = true;
					}
					else {
						scope.ngClasses.main['media-slider-visuals-single'] = true;
					}
					scope.ngClasses.main['media-slider-visuals-format-' + scope.visualFormat] = true;
				});
			}
		}
	});
})(jQuery);