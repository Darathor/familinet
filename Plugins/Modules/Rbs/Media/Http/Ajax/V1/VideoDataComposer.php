<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Media\Http\Ajax\V1;

/**
 * @name \Rbs\Media\Http\Ajax\V1\VideoDataComposer
 */
class VideoDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Rbs\Media\Documents\Video
	 */
	protected $document;

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function __construct(\Change\Documents\Events\Event $event)
	{
		$this->document = $event->getDocument();

		$context = $event->getParam('context');
		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	protected function generateDataSets()
	{
		if (!$this->document)
		{
			$this->dataSets = [];
			return;
		}

		$alt = $this->document->getCurrentLocalization()->getAlt();
		$extension = $this->document->getExtension();
		$mimeType = $this->document->getMimeType();
		$fileSize = $this->document->getContentLength();
		$this->dataSets = [
			'common' => [
				'id' => $this->document->getId(),
				'title' => $alt ?: $this->document->getFileName(),
				'URL' => ['canonical' => $this->document->getPublicURL()],
				'mimeType' => $mimeType,
				'extension' => $extension,
				'alt' => $alt,
				'size' => $fileSize,
				'formattedSize' => $this->i18nManager->transFileSize($fileSize)
			]
		];

		$formattedExtension = $this->getFormattedValueFromCollection(strtolower($extension), 'Rbs_Media_FileExtensions');
		if ($formattedExtension)
		{
			$this->dataSets['common']['formattedExtension'] = $formattedExtension;
		}

		$formattedMimeType = $this->getFormattedValueFromCollection(strtolower($mimeType), 'Rbs_Media_MimeTypes');
		if ($formattedMimeType)
		{
			$this->dataSets['common']['formattedMimeType'] = $formattedMimeType;
		}

		if ($this->websiteUrlManager)
		{
			$this->dataSets['common']['URL']['download'] =
				$this->websiteUrlManager->getActionURL('Rbs_Media', 'Download', ['documentId' => $this->document->getId()]);
		}

		$this->generateTypologyDataSet($this->document);
	}

	/**
	 * @return array
	 */
	protected function getItemContext()
	{
		return ['visualFormats' => $this->visualFormats, 'URLFormats' => $this->URLFormats, 'dataSetNames' => $this->dataSetNames,
			'website' => $this->website, 'websiteUrlManager' => $this->websiteUrlManager, 'section' => $this->section,
			'data' => $this->data, 'detailed' => false, 'level' => $this->level + 1, 'referrer' => $this->document];
	}

	/**
	 * @param mixed $value
	 * @param string $collectionCode
	 * @return array|null
	 */
	protected function getFormattedValueFromCollection($value, $collectionCode)
	{
		$collection = $this->collectionManager->getCollection($collectionCode);
		if (!$collection)
		{
			return null;
		}

		$item = $collection->getItemByValue($value);
		if ($item instanceof \Rbs\Collection\Documents\CollectionItem)
		{
			return $this->dataSets['common']['formattedMimeType'] = $item->getAJAXData($this->getItemContext());
		}
		elseif ($item)
		{
			return ['common' => ['value' => $item->getValue(), 'title' => $item->getTitle()]];
		}
		return null;
	}
}