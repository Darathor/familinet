(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsNotificationConfigurationNew', ['$compile', edit]);
	app.directive('rbsDocumentEditorRbsNotificationConfigurationEdit', ['$compile', edit]);

	function edit($compile) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, elm) {

				var contentScopes = {};

				scope.onLoad = function() {
					if (angular.isArray(scope.document.adminConfig) || !angular.isObject(scope.document.adminConfig)) {
						scope.document.adminConfig = {};
					}
				};

				function drawConfigurationParameterize($compile, scope, directiveName) {
					var container = elm.find('#RbsNotificationConfigurationParametersData');
					container.children().remove();
					if (directiveName) {

						container.html('<div ' + directiveName + '="" data-params="document.adminConfig"></div>');
						contentScopes = scope.$new();
						$compile(container.children())(contentScopes);
					}
				}

				scope.$watch('document.type', function(type) {
					drawConfigurationParameterize($compile, scope, 'rbs-notification-configuration-' + type);
				});
			}
		};
	}
})();