(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsNotificationConfigurationUser', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Notification/Configuration/user.twig',
			scope: { params: '=' }
		}
	});
})();