<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Commands;

use Change\Commands\Events\Event;

/**
 * @name \Rbs\Mail\Notification\InstallNotifications
 */
class InstallNotifications
{
	/**
	 * @param Event $event
	 * @throws \Exception
	 */
	public function execute(Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$response = $event->getCommandResponse();

		$force = $event->getParam('force');

		$filePaths = [];

		$pluginManager = $applicationServices->getPluginManager();
		$plugins = $pluginManager->getInstalledPlugins();
		foreach ($plugins as $plugin)
		{
			if($plugin->getActivated())
			{
				$filePath = $plugin->getAssetsPath() . DIRECTORY_SEPARATOR . 'notifications.json';
				if (file_exists($filePath))
				{
					$filePaths[] = $filePath;
				}
			}
		}

		if ($filePaths)
		{
			$templateCode = $event->getParam('template');
			if ($templateCode)
			{
				$dqb = $applicationServices->getDocumentManager()->getNewQuery('Rbs_Theme_Template');
				$dqb->andPredicates($dqb->eq('code', $templateCode), $dqb->eq('mailSuitable', true));
				$template = $dqb->getFirstDocument();
				if ($template instanceof \Rbs\Theme\Documents\Template)
				{

					$jsonArray = ['documents' => []];
					foreach ($filePaths as $filePath)
					{
						$json = json_decode(file_get_contents($filePath), true);
						if ($json)
						{
							$jsonArray['documents'] = array_merge($jsonArray['documents'], $json['documents']);
						}
					}
					$jsonArray['contextId'] = 'Rbs Notifications Install';

					$import = new \Rbs\Generic\Json\Import($applicationServices->getDocumentManager());
					$import->addOnly(!$force);
					$import->setDocumentCodeManager($applicationServices->getDocumentCodeManager());

					$resolveDocument = function ($id, $contextId) use ($template)
					{
						switch ($id)
						{
							case 'mail_template':
								return $template;
								break;
						}
						return null;
					};
					$import->getOptions()->set('resolveDocument', $resolveDocument);

					$transactionManager = $applicationServices->getTransactionManager();
					try
					{
						$transactionManager->begin();
						$import->fromArray($jsonArray);
						$transactionManager->commit();
					}
					catch (\Exception $e)
					{
						throw $transactionManager->rollBack($e);
					}
					$response->addInfoMessage('Notifications template installed');
				}
				else
				{
					$response->addErrorMessage('Template suitable for mail with code: ' . $templateCode . ' not found');
				}
			}
			else
			{
				$response->addErrorMessage('No template found for mail');
			}
		}
		else
		{
			$response->addInfoMessage('No notifications template files found');
		}
	}
}