<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Documents;

/**
 * @name \Rbs\Notification\Documents\Notification
 */
class Notification extends \Compilation\Rbs\Notification\Documents\Notification
{
	const STATUS_NEW = 'new';
	const STATUS_READ = 'read';
	const STATUS_DELETED = 'deleted';

	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, [$this, 'onDefaultCreated'], 5);
	}

	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() === $this)
		{
			$documentResult = $event->getParam('restResult');
			if ($documentResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
			{
				$extraColumn = $event->getParam('extraColumn');
				if (is_array($extraColumn) && in_array('target', $extraColumn))
				{
					$um = $documentResult->getUrlManager();
					$params = $this->getParams();
					if (is_array($params) && isset($params['targetId']))
					{
						$dm = $event->getApplicationServices()->getDocumentManager();
						$target = $dm->getDocumentInstance($params['targetId']);
						if ($target)
						{
							$vc = new \Change\Http\Rest\V1\ValueConverter($um, $dm);

							$documentResult->setProperty('target', $vc->toRestValue($target, \Change\Documents\Property::TYPE_DOCUMENT));
						}
					}
				}
			}
		}
		parent::onDefaultUpdateRestResult($event);
	}

	public function setStatus($status)
	{
		switch ($status)
		{
			case static::STATUS_NEW:
			case static::STATUS_READ:
			case static::STATUS_DELETED:
			{
				parent::setStatus($status);
				break;
			}
			default:
				throw new \Exception('status must be: "' . static::STATUS_NEW . '", "' .
					static::STATUS_READ . '" or "' . static::STATUS_DELETED . '". "' . $status . '" given', 999999);
		}
	}

	protected function onCreate()
	{
		if (!$this->getStatus())
		{
			$this->setStatus(static::STATUS_NEW);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultCreated(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		$user = $this->getUserIdInstance();
		if ($user && $user->activated())
		{
			$applicationServices = $event->getApplicationServices();
			$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
			$adminProfile = $applicationServices->getProfileManager()->loadProfile($authenticatedUser, 'Rbs_Admin');
			if ($adminProfile && $adminProfile->getPropertyValue('sendNotificationMailImmediately'))
			{
				$arguments = ['notificationId' => $this->getId()];
				$event->getApplicationServices()->getJobManager()->createNewJob('Rbs_Notification_sendMailImmediately', $arguments);
			}
		}
	}
}
