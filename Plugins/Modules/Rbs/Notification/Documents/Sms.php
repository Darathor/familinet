<?php
namespace Rbs\Notification\Documents;

/**
 * @name \Rbs\Notification\Documents\Sms
 */
class Sms extends \Compilation\Rbs\Notification\Documents\Sms
{
	/**
	 * @return array
	 */
	public function getSubstitutions()
	{
		// TODO: Implement getSubstitutions() method.
		return [];
	}

	/**
	 * @param array $substitutions
	 * @return $this
	 */
	public function setSubstitutions($substitutions)
	{
		return $this;
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onReplaceSpecialChar($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onReplaceSpecialChar($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onReplaceSpecialChar(\Change\Documents\Events\Event $event)
	{
		$authorizeReplace = $event->getApplication()->getConfiguration()->getEntry('Rbs/Notification/Sms/ReplaceSpecialChar');
		$content = $this->getCurrentLocalization()->getContent();
		if ($authorizeReplace)
		{
			$search =
				['°', '¶', '¦', '«', '»', '’', '`', '´', '·', 'À', 'Á', 'Â', 'Ã', 'ã', 'È', 'Ê', 'Ë', 'ç', 'ê', 'ë', 'Ì', 'Í', 'Î', 'Ï', 'í',
					'î', 'ï', 'Ò', 'Ó', 'Ô', 'Õ', 'ó', 'ô', 'õ', 'Ù', 'Ú', 'Û', 'ú', 'û', 'Ý', 'ý', 'ÿ', '  '];
			$replacement =
				[' ', '', '|', '"', '"', '\'', '\'', '\'', '-', 'A', 'A', 'A', 'A', 'a', 'E', 'E', 'E', 'c', 'e', 'e', 'I', 'I', 'I', 'I', 'i',
					'i', 'i', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'U', 'U', 'U', 'u', 'u', 'Y', 'y', 'y', ' '];
			$this->getCurrentLocalization()->setContent(str_replace($search, $replacement, $content));
		}
	}
}
