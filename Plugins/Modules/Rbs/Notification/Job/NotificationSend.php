<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Job;

/**
 * @name \Rbs\Notification\Job\NotificationSend
 */
class NotificationSend
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$parameters = $job->getArguments() ?? [];
		$notificationCode = $parameters['__notificationCode'];
		if (!$notificationCode)
		{
			$event->failed('Invalid job parameters');
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		try
		{
			$transactionManager->begin();
			$sendResult = $genericServices->getNotificationManager()->send($notificationCode, $parameters);
			$event->setResultArgument('sendResult', $sendResult);
			$event->success();
			$transactionManager->commit();
		}
		catch (\Rbs\Notification\Exceptions\InvalidContext $e)
		{
			$transactionManager->rollBack($e);
			$event->setResultArgument('InvalidContext', $e->getMessage());
			$event->success();
		}
		catch (\Exception $e)
		{
			$event->getApplication()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}
	}
}