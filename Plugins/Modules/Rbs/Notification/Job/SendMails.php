<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification\Job;

/**
 * @name \Rbs\Notification\Job\SendMails
 */
class SendMails
{
	public function sendMailImmediately(\Change\Job\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		$documentManager = $applicationServices->getDocumentManager();
		$job = $event->getJob();
		$notificationId = $job->getArgument('notificationId');
		$notification = $documentManager->getDocumentInstance($notificationId);

		if ($notification instanceof \Rbs\Notification\Documents\Notification && $notification->getStatus() == \Rbs\Notification\Documents\Notification::STATUS_NEW)
		{
			$user = $notification->getUserIdInstance();
			if ($user instanceof \Rbs\User\Documents\User)
			{
				$profileManager = $applicationServices->getProfileManager();
				$i18nManager = $applicationServices->getI18nManager();
				$configuration = $event->getApplication()->getConfiguration();

				$fromEmail = $configuration->getEntry('Rbs/Notification/fromEmail');
				$adminURL = $configuration->getEntry('Rbs/Notification/adminURL');

				$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
				$userProfile = $profileManager->loadProfile($authenticatedUser, 'Change_User');
				$LCID = $userProfile->getPropertyValue('LCID');
				if (!$LCID)
				{
					$LCID = $i18nManager->getDefaultLCID();
				}

				$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'notification-immediately-mail-' . $LCID . '.twig';
				if (is_file($filePath))
				{
					$mailManager = $applicationServices->getMailManager();
					$templateManager = $applicationServices->getTemplateManager();

					$timezone = $userProfile->getPropertyValue('TimeZone');
					if (!$timezone)
					{
						$timezone = $i18nManager->getTimeZone()->getName();
					}

					$params = ['timezone' => $timezone, 'LCID' => $LCID, 'adminURL' => $adminURL];

					$documentManager->pushLCID($LCID);

					$l = $notification->getCurrentLocalization();
					if ($l->isNew())
					{
						$l = $notification->getRefLocalization();
					}
					$params['notification'] = ['message' => $l->getMessage(), 'creationDate' => $l->getCreationDate()];

					$html = $templateManager->renderTemplateFile($filePath, $params);
					$subject = $i18nManager->transForLCID($LCID, 'm.rbs.notification.admin.notification_immediately_mail_subject');

					$message = $mailManager->prepareMessage([$fromEmail], [$user->getEmail()], $subject, $html);

					$documentManager->popLCID();

					$mailManager->send($message);
				}
			}
		}
	}


	public function execute(\Change\Job\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		$documentManager = $applicationServices->getDocumentManager();
		$transactionManager = $applicationServices->getTransactionManager();
		$profileManager = $applicationServices->getProfileManager();
		$i18nManager = $applicationServices->getI18nManager();
		$mailManager = $applicationServices->getMailManager();
		$templateManager = $applicationServices->getTemplateManager();

		$configuration = $event->getApplication()->getConfiguration();
		$fromEmail = $configuration->getEntry('Rbs/Notification/fromEmail');
		$adminURL = $configuration->getEntry('Rbs/Notification/adminURL');

		$userQuery = $documentManager->getNewQuery('Rbs_User_User');
		$userQuery->andPredicates($userQuery->activated());
		$userQuery->addOrder('id');
		$adminProfileQuery = $userQuery->getPropertyModelBuilder('id', 'Rbs_User_Profile', 'user');
		$adminProfileQuery->andPredicates($adminProfileQuery->isNotNull('notificationMailInterval'));
		$builder = $userQuery->dbQueryBuilder();
		$fb = $builder->getFragmentBuilder();
		$builder->addColumn($fb->alias($userQuery->getColumn('id'), 'id'));
		$builder->andWhere($fb->gt($userQuery->getColumn('id'), $fb->integerParameter('userId')));
		$select = $builder->query();

		$chuckSize = 10;
		$select->setMaxResults($chuckSize);

		$userId = 0;
		do
		{
			try
			{
				$transactionManager->begin();

				$select->bindParameter('userId', $userId);
				$userIds = $select->getResults($select->getRowsConverter()->addIntCol('id')->singleColumn('id'));

				foreach ($userIds as $userId)
				{
					$user = $documentManager->getDocumentInstance($userId);
					if ($user instanceof \Rbs\User\Documents\User)
					{
						$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);
						$adminProfile = $profileManager->loadProfile($authenticatedUser, 'Rbs_Admin');

						if (($lastNotificationMailSentDate = $this->checkSendNotification($adminProfile)) !== null)
						{
							$userProfile = $profileManager->loadProfile($authenticatedUser, 'Change_User');
							$LCID = $userProfile->getPropertyValue('LCID');
							if (!$LCID)
							{
								$LCID = $i18nManager->getDefaultLCID();
							}

							$filePath = __DIR__ . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'notification-mail-' . $LCID . '.twig';
							if (is_file($filePath))
							{
								/* @var $user \Rbs\User\Documents\User */
								$userQuery = $documentManager->getNewQuery('Rbs_Notification_Notification');
								$userQuery->andPredicates(
									$userQuery->eq('userId', $user->getId()),
									$userQuery->eq('status', \Rbs\Notification\Documents\Notification::STATUS_NEW),
									$userQuery->gt('creationDate', $lastNotificationMailSentDate)
								)->addOrder('id', false);

								$countNewNotifications = $userQuery->getCountDocuments();
								if ($countNewNotifications > 0)
								{
									$timezone = $userProfile->getPropertyValue('TimeZone');
									if (!$timezone)
									{
										$timezone = $i18nManager->getTimeZone()->getName();
									}

									$params = ['timezone' => $timezone, 'LCID' => $LCID, 'adminURL' => $adminURL,
										'countNewNotifications' => $countNewNotifications];

									$documentManager->pushLCID($LCID);

									$notifications = $userQuery->getDocuments(0, 10);
									$params['countEmbedNotifications'] = $notifications->count();

									/* @var $notification \Rbs\Notification\Documents\Notification */
									foreach ($notifications as $notification)
									{
										$l = $notification->getCurrentLocalization();
										if ($l->isNew())
										{
											$l = $notification->getRefLocalization();
										}
										$params['notifications'][] = ['message' => $l->getMessage(),
											'creationDate' => $l->getCreationDate()];
									}

									$html = $templateManager->renderTemplateFile($filePath, $params);
									$subject = $i18nManager->transForLCID($LCID, 'm.rbs.notification.admin.notification_mail_subject');

									$message = $mailManager->prepareMessage([$fromEmail], [$user->getEmail()], $subject, $html);

									$documentManager->popLCID();

									$mailManager->send($message);
								}
							}
							//set date of the last sent mail, useful to compare with the interval next we want send mail
							$adminProfile->setPropertyValue('dateOfLastNotificationMailSent', new \DateTime());
							$profileManager->saveProfile($authenticatedUser, $adminProfile);
						}
					}
				}

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$event->getApplication()->getLogging()->exception($e);
				$transactionManager->rollBack($e);
				$userIds = array_fill(0, $chuckSize, $userId);
			}
		}
		while (count($userIds) == $chuckSize);

		//reschedule the job in 5 minutes
		$reportDate = (new \DateTime())->add(new \DateInterval('PT5M'));
		$event->reported($reportDate);
	}

	/**
	 * @param \Change\User\ProfileInterface $adminProfile
	 * @return \DateTime|null
	 */
	public function checkSendNotification(\Change\User\ProfileInterface $adminProfile)
	{
		$sendNotification = false;
		$notificationMailInterval = $adminProfile->getPropertyValue('notificationMailInterval');
		$notificationMailAt = $adminProfile->getPropertyValue('notificationMailAt');
		if (!$notificationMailAt)
		{
			$notificationMailAt = '08:00';
		}
		$pv = $adminProfile->getPropertyValue('dateOfLastNotificationMailSent');
		$lastNotificationMailSentDate = $pv ?: new \DateTime('2015-01-01T00:00:0+00:00');

		if ($notificationMailInterval)
		{
			$interval = new \DateInterval($notificationMailInterval);
			$now = new \DateTime();

			list($hour, $minute) = explode(':', $notificationMailAt);
			$nextSend = clone $lastNotificationMailSentDate;
			$nextSend->add($interval);

			//if interval concerning "day" set time (hour and minute), else that mean concerning "hour" so set only minute
			$interval->d ? $nextSend->setTime((int)$hour, (int)$minute) : $nextSend->setTime($nextSend->format('H'),
				$minute);

			//check if we can send him an e-mail, comparing the interval between now, user settings, and the last time a mail has been sent
			$sendNotification = $nextSend->getTimestamp() < $now->getTimestamp();

			return $sendNotification ? $lastNotificationMailSentDate : null;
		}
		return $sendNotification ? $lastNotificationMailSentDate : null;
	}
}