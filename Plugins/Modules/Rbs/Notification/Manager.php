<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Notification;

/**
 * @name \Rbs\Notification\Manager
 */
class Manager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	/**
	 * @var \Change\Cache\CacheManager
	 */
	protected $cacheManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\User\AuthenticationManager
	 */
	protected $authenticationManager;

	/**
	 * @var \Change\Job\JobManager
	 */
	protected $jobManager;

	/**
	 * @var \Change\Mail\MailManager
	 */
	protected $mailManager;

	/**
	 * @var \Change\Sms\SmsManager
	 */
	protected $smsManager;

	/**
	 * @var \Change\Svi\SviManager
	 */
	protected $sviManager;

	/**
	 * @var array
	 */
	protected $notificationByCodes = [];

	/**
	 * @return string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return ['NotificationManager'];
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Notification/Events/NotificationManager');
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('resolveNotification', function ($event) { $this->onResolveNotification($event); }, 5);
		$eventManager->attach('initContext', function ($event) { $this->onInitContext($event); }, 5);
		$eventManager->attach('buildSubstitutionValues', function ($event) { $this->onBuildSubstitutionValues($event); }, 5);
	}

	/**
	 * @return \Change\Logging\Logging
	 */
	protected function getLogging()
	{
		return $this->getApplication()->getLogging();
	}

	/**
	 * @return \Change\Cache\CacheManager
	 */
	protected function getCacheManager()
	{
		return $this->cacheManager;
	}

	/**
	 * @param \Change\Cache\CacheManager $cacheManager
	 * @return $this
	 */
	public function setCacheManager($cacheManager)
	{
		$this->cacheManager = $cacheManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\User\AuthenticationManager
	 */
	protected function getAuthenticationManager()
	{
		return $this->authenticationManager;
	}

	/**
	 * @param \Change\User\AuthenticationManager $authenticationManager
	 * @return $this
	 */
	public function setAuthenticationManager($authenticationManager)
	{
		$this->authenticationManager = $authenticationManager;
		return $this;
	}

	/**
	 * @return \Change\Job\JobManager
	 */
	protected function getJobManager()
	{
		return $this->jobManager;
	}

	/**
	 * @param \Change\Job\JobManager $jobManager
	 * @return $this
	 */
	public function setJobManager($jobManager)
	{
		$this->jobManager = $jobManager;
		return $this;
	}

	/**
	 * @return \Change\Mail\MailManager
	 */
	protected function getMailManager()
	{
		return $this->mailManager;
	}

	/**
	 * @param \Change\Mail\MailManager $mailManager
	 * @return $this
	 */
	public function setMailManager($mailManager)
	{
		$this->mailManager = $mailManager;
		return $this;
	}

	/**
	 * @return \Change\Sms\SmsManager
	 */
	protected function getSmsManager()
	{
		return $this->smsManager;
	}

	/**
	 * @param \Change\Sms\SmsManager $smsManager
	 * @return $this
	 */
	public function setSmsManager($smsManager)
	{
		$this->smsManager = $smsManager;
		return $this;
	}

	/**
	 * @return \Change\Svi\SviManager
	 */
	protected function getSviManager()
	{
		return $this->sviManager;
	}

	/**
	 * @param \Change\Svi\SviManager $sviManager
	 * @return $this
	 */
	public function setSviManager($sviManager)
	{
		$this->sviManager = $sviManager;
		return $this;
	}

	/**
	 * @param string $code
	 * @return \Rbs\Notification\NotificationInterface|null
	 */
	public function resolveNotification(string $code)
	{
		$notification = $this->notificationByCodes[$code] ?? null;
		if ($notification === null)
		{
			$this->notificationByCodes[$code] = false;
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['code' => $code, 'notification' => null]);
			$eventManager->trigger('resolveNotification', $this, $args);
			$notification = $args['notification'];
			if ($notification instanceof \Rbs\Notification\NotificationInterface)
			{
				$this->notificationByCodes[$code] = $notification;
			}
			else
			{
				$notification = null;
			}
		}

		return $notification ?: null;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onResolveNotification(\Change\Events\Event $event)
	{
		if ($event->getParam('notification') !== null)
		{
			return;
		}
		$cacheOptions = ['ttl' => 36000];
		$notificationCodes = $this->getCacheManager()->getEntry('prefetch', 'notificationCodes', $cacheOptions);
		if ($notificationCodes === null)
		{
			$q = $this->getDocumentManager()->getNewQuery('Rbs_Notification_Configuration');
			$qb = $q->dbQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->addColumn($fb->alias($q->getColumn('id'), 'id'))->addColumn($fb->alias($q->getColumn('code'), 'code'));
			$select = $qb->query();
			$notificationCodes =
				$select->getResults($select->getRowsConverter()->addIntCol('id')->addStrCol('code')->indexBy('code')->singleColumn('id'));
			$this->getCacheManager()->setEntry('prefetch', 'notificationCodes', $notificationCodes, $cacheOptions);
		}

		$code = $event->getParam('code');
		$configurationId = $notificationCodes[$code] ?? 0;
		if ($configurationId)
		{
			$configuration = $this->getDocumentManager()->getDocumentInstance($configurationId);
			if ($configuration instanceof \Rbs\Notification\Documents\Configuration)
			{
				$event->setParam('configuration', $configuration);
			}
		}
	}

	/**
	 * @param $notificationCode
	 * @param array $parameters
	 * @param \DateTime|null $executeAt
	 * @return \Change\Job\JobInterface|null
	 */
	public function addJob(string $notificationCode, array $parameters = [], \DateTime $executeAt = null)
	{
		if ($notification = $this->resolveNotification($notificationCode))
		{
			$parameters['__notificationCode'] = $notificationCode;
			$parameters['LCID'] = $parameters['LCID'] ?? $this->getDocumentManager()->getLCID();
			$parameters['currentUserId'] = $parameters['currentUserId'] ?? $this->getAuthenticationManager()->getCurrentUser()->getId();
			return $this->getJobManager()->createNewJob('Rbs_Notification_Send', $parameters, $executeAt);
		}
		return null;
	}

	/**
	 * @param string $notificationCode
	 * @param array $parameters
	 * @throws \Exception
	 * @return false|mixed
	 */
	public function send(string $notificationCode, array $parameters)
	{
		$result = false;
		if ($notification = $this->resolveNotification($notificationCode))
		{
			$eventManager = $this->getEventManager();
			$parameters['__sendParameters'] = $parameters;
			$event = new \Change\Events\Event('initContext', $notification, $parameters);

			$eventManager->triggerEvent($event);

			$sendParameters = $event->paramsToArray();
			$result = $notification->send($sendParameters);
			try
			{
				$event->setParam('sendResult', $result);
				$event->setName('postSend');
				$eventManager->triggerEvent($event);
			}
			catch (\Throwable $t)
			{
				$this->getLogging()->throwable($t);
				$result['postSend'] = $t->getMessage();
			}
			$event->setName('postSend');
		}
		return $result;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onInitContext(\Change\Events\Event $event)
	{
		$notification = $event->getTarget();
		if ($notification instanceof \Rbs\Notification\NotificationInterface)
		{
			if ($currentUserId = $event->getParam('currentUserId'))
			{
				if ($currentUserId != $this->getAuthenticationManager()->getCurrentUser()->getId())
				{
					$this->getAuthenticationManager()->getById($currentUserId);
				}
			}
		}
	}

	/**
	 * @param string|null $LCID
	 * @return $this
	 */
	public function setLCIDContext($LCID = null)
	{
		if ($LCID)
		{
			$this->getI18nManager()->setLCID($LCID);
		}
		return $this;
	}

	/**
	 * @param \Rbs\Notification\NotificationInterface $notification
	 * @param array $parameters
	 * @return array
	 */
	public function buildSubstitutionValues(\Rbs\Notification\NotificationInterface $notification, array $parameters)
	{
		$eventManager = $this->getEventManager();
		$event = new \Change\Events\Event('buildSubstitutionValues', $notification, $parameters);
		$eventManager->triggerEvent($event);
		return $event->paramsToArray();
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onBuildSubstitutionValues(\Change\Events\Event $event)
	{
		$notification = $event->getTarget();
		if ($notification instanceof \Rbs\Notification\NotificationInterface)
		{
			$event->setName($notification->getCode());
			$this->getEventManager()->triggerEvent($event);
		}
	}

	/**
	 * @param string $notificationCode
	 * @return array
	 */
	public function getSubstitutionLabels(string $notificationCode)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['notificationCode' => $notificationCode, 'substitutionLabels' => null]);
		$eventManager->trigger('getSubstitutionLabels', $this, $args);
		$substitutionLabels = $args['substitutionLabels'];
		return is_array($substitutionLabels) ? $substitutionLabels : [];
	}

	/**
	 * @param int $mailId
	 * @param int $websiteId
	 * @return null|\Rbs\Mail\Documents\Mail
	 */
	public function resolveMail($mailId, $websiteId)
	{
		$mail = $this->getDocumentManager()->getDocumentInstance($mailId);
		if (!($mail instanceof \Rbs\Mail\Documents\Mail))
		{
			return null;
		}
		$mail = $mail->getMailForWebsite($websiteId);
		return $mail->activated() ? $mail : null;
	}

	/**
	 * @param string $email
	 * @param \Rbs\Mail\Documents\Mail $mail
	 * @param array $substitutions
	 * @return bool|string
	 */
	public function sendMail(string $email, \Rbs\Mail\Documents\Mail $mail, array $substitutions)
	{
		$emails = ['to' => [$email], 'cc' => [], 'bcc' => [], 'reply-to' => []];
		$attachments = $substitutions['__attachments'] ?? [];
		$mailManager = $this->getMailManager();
		/** @var \Rbs\Website\Documents\Website|null $website */
		$website = $this->getDocumentManager()->getDocumentInstance($substitutions['websiteId']);
		$LCID = $this->getI18nManager()->getLCID();

		$html = $this->getMailManager()->buildHtmlContent($mail, $website, $LCID, $substitutions);

		$from = $this->getFrom($mail, $website);
		$subject = \Change\Stdlib\StringUtils::getSubstitutedString($mail->getSubject(), $substitutions);
		$message = $mailManager->prepareMessage($from, $emails['to'], $subject, $html, null,
			$emails['cc'], $emails['bcc'], $emails['reply-to'], null, $attachments);

		if ($message)
		{
			$mailManager->send($message);
			return $email;
		}
		return false;
	}

	/**
	 * @param \Rbs\Mail\Documents\Mail $mail
	 * @param \Change\Presentation\Interfaces\Website $website
	 * @return array
	 */
	protected function getFrom($mail, $website)
	{
		if ($mail->getCurrentLocalization()->getSenderMail())
		{
			return [['name' => $mail->getCurrentLocalization()->getSenderName(),
				'email' => $mail->getCurrentLocalization()->getSenderMail()]];
		}

		if (($website instanceof \Change\Presentation\Interfaces\Website) && $email = $website->getMailSender())
		{
			if (strpos($email, '<'))
			{
				$senderName = trim(substr($email, 0, strpos($email, '<')));
				$senderMail = trim(substr($email, strpos($email, '<') + 1));
				$senderMail = trim(substr($senderMail, 0, strrpos($senderMail, '>')));
			}
			else
			{
				$senderName = '';
				$senderMail = $email;
			}
			return [['name' => $senderName, 'email' => $senderMail]];
		}
		return [$this->getApplication()->getConfiguration('Rbs/Mail/defaultSender')];
	}

	/**
	 * @param $smsId
	 * @return \Rbs\Notification\Documents\Sms|null
	 */
	public function resolveSMS($smsId)
	{
		$sms = $this->getDocumentManager()->getDocumentInstance($smsId);
		if (!($sms instanceof \Rbs\Notification\Documents\Sms))
		{
			return null;
		}
		return $sms->activated() ? $sms : null;
	}

	/**
	 * @param string $smsNumber
	 * @param \Rbs\Notification\Documents\Sms $sms
	 * @param $substitutions
	 * @return false|string
	 */
	public function sendSMS($smsNumber, \Rbs\Notification\Documents\Sms $sms, array $substitutions)
	{
		$message = $sms->getCurrentLocalization()->getContent();
		$message = \Change\Stdlib\StringUtils::getSubstitutedString($message, $substitutions);
		if ($smsNumber && $message)
		{
			return $this->getSmsManager()->sendMessage($smsNumber, $message) ?? false;
		}
		return false;
	}

	/**
	 * @param $sviId
	 * @return \Rbs\Notification\Documents\Svi|null
	 */
	public function resolveSVI($sviId)
	{
		$svi = $this->getDocumentManager()->getDocumentInstance($sviId);
		if (!($svi instanceof \Rbs\Notification\Documents\Svi))
		{
			return null;
		}
		return $svi->activated() ? $svi : null;
	}

	/**
	 * @param string $sviNumber
	 * @param \Rbs\Notification\Documents\Svi $svi
	 * @param $substitutions
	 * @return false|string
	 */
	public function sendSVI($sviNumber, \Rbs\Notification\Documents\Svi $svi, array $substitutions)
	{
		$message = $svi->getCurrentLocalization()->getContent();
		$message = \Change\Stdlib\StringUtils::getSubstitutedString($message, $substitutions);
		if ($sviNumber && $message)
		{
			return $this->getSviManager()->sendMessage($sviNumber, $message, $this->getI18nManager()->getLCID()) ?? false;
		}
		return false;
	}
}