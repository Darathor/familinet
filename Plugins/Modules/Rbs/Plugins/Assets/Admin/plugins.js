(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.service('RbsChange.Plugins', ['$http', '$q', 'RbsChange.REST', changePluginsServiceFn]);

	function changePluginsServiceFn($http, $q, REST) {
		this.getNew = function() {
			var deferred = $q.defer();
			var url = REST.getBaseUrl('plugins/newPlugins');
			$http.get(url).then(
				function(result) {
					deferred.resolve(result.data);
				},
				function(result) {
					deferred.reject(result.data);
				}
			);
			return deferred.promise;
		};

		this.getRegistered = function() {
			var deferred = $q.defer();
			var url = REST.getBaseUrl('plugins/registeredPlugins');
			$http.get(url).then(
				function(result) {
					deferred.resolve(result.data);
				},
				function(result) {
					deferred.reject(result.data);
				}
			);
			return deferred.promise;
		};

		this.getInstalled = function() {
			var deferred = $q.defer();
			var url = REST.getBaseUrl('plugins/installedPlugins');
			$http.get(url).then(function(result) {
					deferred.resolve(result.data);
				},
				function(result) {
					deferred.reject(result.data);
				}
			);
			return deferred.promise;
		};

		this.activateChange = function(plugin) {
			var base = !plugin.activated ? 'commands/change/disable-plugin' : 'commands/change/enable-plugin';
			var url = REST.getBaseUrl(base);
			$http.post(url, { 'type': plugin.type, 'vendor': plugin.vendor, 'name': plugin.shortName });
		};
	}
})();