(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('head', ['$rootScope', function($rootScope) {
		return {
			restrict: 'E',
			scope: false,
			link: function() {
				var jqTitle;
				$rootScope.$on('RbsSeoUpdateMetas', function(event, args) {
					if (angular.isObject(args) && args.title) {
						if (!jqTitle) {
							jqTitle = jQuery('head title');
						}
						jqTitle.text(args.title);
					}
				});
			}
		};
	}]);
})(window.jQuery);