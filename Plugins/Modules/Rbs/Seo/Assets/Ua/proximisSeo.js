/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisSeo
	 *
	 * @description
	 * The proximisSeo module contains all components for Proximis Seo application.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisSeo` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisSeo` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxSeo`
	 */
	var app = angular.module('proximisSeo', ['proximis', 'ngResource', 'ngSanitize', 'ngTouch', 'ngCookies', 'ngAnimate',
		'ngMessages', 'ui.bootstrap']);

	// Configuration.

	app.config(['$compileProvider', '$locationProvider', '$interpolateProvider', 'localStorageServiceProvider', 'proximisRestOAuthProvider',
		'proximisRestApiProvider', 'proximisRouteProvider', 'proximisRestApiDefinitionProvider', 'proximisUserProvider', 'ProximisGlobal',
		'ProximisRestGlobal',
		function($compileProvider, $locationProvider, $interpolateProvider, localStorageServiceProvider, proximisRestOAuthProvider, proximisRestApiProvider, proximisRouteProvider, proximisRestApiDefinitionProvider, proximisUserProvider, ProximisGlobal, ProximisRestGlobal) {
			$locationProvider.html5Mode(true);
			$interpolateProvider.startSymbol('(=').endSymbol('=)');

			/** @var {string} ProximisGlobal.role */
			localStorageServiceProvider.setPrefix('_Seo_' + ProximisGlobal.role + '_');

			var restUrl = ProximisRestGlobal.restURL;
			var oauthUrl = restUrl + 'OAuth/';
			proximisRestOAuthProvider.setBaseUrl(oauthUrl);
			proximisRestOAuthProvider.setRealm(ProximisRestGlobal.OAuth.realm);
			// Sign all the requests on our REST services...
			proximisRestOAuthProvider.setSignedUrlPatternInclude(restUrl);
			// ... but do NOT sign OAuth requests.
			proximisRestOAuthProvider.setSignedUrlPatternExclude(oauthUrl);

			proximisRestApiProvider.setBaseUrl(restUrl);
			proximisRestApiDefinitionProvider.applyDefinition(ProximisRestGlobal.restApi);

			proximisRouteProvider.setRoutesDefaultModule('Rbs_Seo');
			proximisRouteProvider.applyRoutes(ProximisGlobal.routes);

			proximisUserProvider.setProfileNames(['Change_User', 'Rbs_Seo']);
		}
	]);

	// Controllers.

	/**
	 * This Controller is bound to the <body/> tag and is, thus, the "root Controller".
	 * Mostly, it deals with user authentication and settings.
	 */
	app.controller('ProximisSeoRootController', ['$scope', '$rootScope', '$location', 'proximisUser', 'ProximisRestEvents',
		function(scope, $rootScope, $location, proximisUser, ProximisRestEvents) {
			var initialized = false;
			scope.loading = true;
			scope.showMainMenu = true;

			$rootScope.$on('$locationChangeSuccess', function() {
				if (!initialized) {
					initialized = true;
					proximisUser.init();
				}
				else if ($location.path() === '/OAuth/Authorize') {
					scope.loading = false;
				}
			});

			proximisUser.ready().then(function() {
				scope.loading = false;
			});

			$rootScope.$on(ProximisRestEvents.AuthenticationSuccess, function(event, args) {
				proximisUser.load().then(function() {
					if (args['callback']) {
						$location.url(args['callback']);
					}
				});
			});

			scope.logout = function() {
				$rootScope.$emit(ProximisRestEvents.Logout);
			};
		}
	]);
})();