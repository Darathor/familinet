/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoDashboardController', ['$scope', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages',
		'proximisErrorFormatter', 'proximisCoreI18n', ProximisSeoDashboardController]);
	function ProximisSeoDashboardController(scope, proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisErrorFormatter, proximisCoreI18n) {
		scope.loading = true;
		scope.error = false;

		var params = {
			websiteId: null
		};
		proximisRestApi.sendData(proximisRestApiDefinition.getData('dashboard.getData', 'GET', params)).then(
			function(result) {
				scope.loading = false;
				scope.error = false;
				scope.data = result.data.item;
			},
			function(result) {
				scope.loading = false;
				scope.error = true;
				var title = proximisCoreI18n.trans('m.rbs.ua.common.loading_dashboard_error | ucf');
				proximisMessages.error(title, proximisErrorFormatter.format(result.data), false, 60000);
			}
		);
	}
})();

