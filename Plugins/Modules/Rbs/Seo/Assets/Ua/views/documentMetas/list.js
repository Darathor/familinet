/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoDocumentMetasListController', ['$scope', '$rootScope', 'proximisModalStack', 'proximisDocumentModels',
		ProximisSeoDocumentMetasListController]);

	function ProximisSeoDocumentMetasListController(scope, $rootScope, proximisModalStack, proximisDocumentModels) {
		scope.globalData = {
			languages: [],
			models: proximisDocumentModels.getByFilter({ publishable: true }),
			restParams: {
				model: null,
				LCID: null
			}
		};

		$rootScope.$watch('user.settings.LCID', function(LCID) {
			if (LCID) {
				scope.globalData.restParams.LCID = LCID;
			}
		});

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/documentMetas/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoDocumentMetasDetailController',
					resolve: {
						parentData: function() {
							return {
								LCID: scope.globalData.restParams.LCID,
								documentId: document._meta.id
							};
						},
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return scope.globalData.restParams.model && scope.globalData.restParams.LCID;
				}
			},
			label: {
				isValid: function(filter) {
					return scope.globalData.restParams.model && scope.globalData.restParams.LCID && filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);

			scope.$watch('globalData.restParams', function(restParams) {
				if (restParams && restParams.model && restParams.LCID && scope.actionMethods.refresh) {
					scope.actionMethods.refresh();
				}
			}, true);
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - LCID
	 */
	app.controller('ProximisSeoDocumentMetasDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', 'proximisCoreArray',
		ProximisSeoDocumentMetasDetailController]);

	function ProximisSeoDocumentMetasDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n, proximisCoreArray) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			metas: null,
			language: {},
			defaultGroups: null,
			variables: [],
			addGroupIds: []
		};

		var params = { documentId: parentData.documentId, LCID: parentData.LCID, fields: ['variable'] };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentMetas.detail', 'GET', params)).then(
			function(result) {
				var mode = result.data.item.i18n.mode;
				scope.modalData.metas = result.data.item;
				scope.modalData.variables = result.data.variable;
				angular.forEach(scope.modalData.metas.languages, function(language) {
					if (language.LCID === params.LCID) {
						scope.modalData.language = language
					}
				});
				if (mode === 'auto') {
					scope.modalData.defaultGroups = angular.copy(scope.modalData.metas.i18n.metaGroups);
					scope.modalData.loadingData = false;
				}
				else {
					var confParams = { modelMetaId: scope.modalData.metas.common['modelConfigurationId'], LCID: parentData.LCID };
					proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelMetas.detail', 'GET', confParams)).then(
						function(result) {
							scope.modalData.defaultGroups = result.data.item.i18n['metaGroups'];
							if (mode === 'none') {
								scope.modalData.metas.i18n.metaGroups = angular.copy(scope.modalData.defaultGroups);
							}
							scope.modalData.loadingData = false;
						},
						function(result) {
							scope.modalData.errorMessage = result.data.status.message;
							console.error('Unable to load data related to ' + confParams.id + '".', result);
						}
					);
				}
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.addGroups = function() {
			var groupIds = scope.modalData.addGroupIds;
			//noinspection JSUnresolvedVariable
			for (var i = 0; i < scope.modalData.metas.i18n.metaGroups.length; i++) {
				//noinspection JSUnresolvedVariable
				proximisCoreArray.removeValue(groupIds, scope.modalData.metas.i18n.metaGroups[i]._meta.id);
			}

			var params = { documentId: scope.modalData.metas._meta.id, LCID: parentData.LCID, groupIds: scope.modalData.addGroupIds };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentMetas.newValueGroups', 'GET', params)).then(
				function(result) {
					angular.forEach(result.data.items, function(group) {
						//noinspection JSUnresolvedVariable
						scope.modalData.metas.i18n.metaGroups.push(group);
					});
					scope.modalData.addGroupIds = [];
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to load data related to ' + params.id + '".', result);
				}
			);
		};

		scope.removeGroup = function(index) {
			//noinspection JSUnresolvedVariable
			delete scope.modalData.metas.i18n.metaGroups.splice(index, 1);
		};

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { documentId: scope.modalData.metas._meta.id, LCID: parentData.LCID, metas: scope.modalData.metas };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_DocumentMetas.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					scope.cancel();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();