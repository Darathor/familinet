/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoMetaDefinitionListController', ['$scope', 'proximisCoreI18n', 'proximisRestApi', 'proximisRestApiDefinition',
		'proximisModalStack', 'proximisMessages', ProximisSeoMetaDefinitionListController]);

	function ProximisSeoMetaDefinitionListController(scope, proximisCoreI18n, proximisRestApi, proximisRestApiDefinition, proximisModalStack,
		proximisMessages) {
		scope.openAddMetaDefinitionModal = function() {
			proximisModalStack.open({
				templateUrl: 'Rbs/Seo/Ua/views/metaDefinition/detail-modal-content.twig',
				size: 'lg',
				controller: 'ProximisSeoAddMetaDefinitionController',
				resolve: {
					refreshParent: function() { return scope.actionMethods.refresh; }
				}
			});
		};

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/metaDefinition/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoMetaDefinitionDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id}; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			},
			delete: function(document) {
				if (confirm(proximisCoreI18n.trans('m.rbs.seo.ua.confirm_deleting_meta_definition', ['ucf']))) {
					var params = { metaDefinitionId: document._meta.id };
					var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaDefinition.detail', 'DELETE', params));
					scope.actionMethods.refresh(promise);
				}
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			label: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});
	}

	app.controller('ProximisSeoAddMetaDefinitionController', ['$uibModalInstance', '$scope', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoAddMetaDefinitionController]);

	function ProximisSeoAddMetaDefinitionController(modalInstance, scope, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: false,
			savingData: false,
			errorMessage: null,
			detail: {
				common: {
					label: null,
					type: null,
					name: null,
					valueType: null,
					defaultValue: null,
					collectionCode: null
				}
			}
		};

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaDefinition.new', 'POST', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable create new meta definition.', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}

	/**
	 * parentData:
	 *  - documentId
	 */
	app.controller('ProximisSeoMetaDefinitionDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoMetaDefinitionDetailController]);

	function ProximisSeoMetaDefinitionDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack,
		proximisRestApi, proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			detail: null
		};

		var params = { metaDefinitionId: parentData.documentId };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaDefinition.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.detail = result.data.item;
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { metaDefinitionId: scope.modalData.detail._meta.id, data: scope.modalData.detail.common };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_MetaDefinition.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();