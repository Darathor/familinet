/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoModelMetasListController', ['$scope', '$rootScope', 'proximisModalStack', ProximisSeoModelMetasListController]);

	function ProximisSeoModelMetasListController(scope, $rootScope, proximisModalStack) {
		scope.globalData = {
			languages: [],
			restParams: {
				LCID: null
			}
		};

		$rootScope.$watch('user.settings.LCID', function(LCID) {
			if (LCID) {
				scope.globalData.restParams.LCID = LCID;
			}
		});

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/modelMetas/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoModelMetasDetailController',
					resolve: {
						parentData: function() {
							var language = null;
							angular.forEach(scope.globalData.languages, function (item) {
								if (item.value === scope.globalData.restParams.LCID) {
									language = item;
								}
							});
							return {
								documentId: document._meta.id,
								LCID: scope.globalData.restParams.LCID,
								LCIDLabel: language ? language.label : null
							};
						},
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return scope.globalData.restParams.LCID;
				}
			},
			label: {
				isValid: function(filter) {
					return scope.globalData.restParams.LCID && filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);

			scope.$watch('globalData.restParams.LCID', function(LCID) {
				if (LCID && scope.actionMethods.refresh) {
					scope.actionMethods.refresh();
				}
			});
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - LCID
	 *  - LCIDLabel
	 */
	app.controller('ProximisSeoModelMetasDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', 'proximisCoreArray',
		ProximisSeoModelMetasDetailController]);

	function ProximisSeoModelMetasDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n, proximisCoreArray) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			LCID: parentData.LCID,
			LCIDLabel: parentData.LCIDLabel,
			metas: null,
			variables: [],
			addGroupIds: []
		};

		var params = { modelMetaId: parentData.documentId, LCID: parentData.LCID, fields: ['variable'] };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelMetas.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.metas = result.data.item;
				scope.modalData.variables = result.data.variable;
				angular.forEach(scope.modalData.metas, function(language) {
					if (language.LCID === params.LCID) {
						scope.modalData.language = language
					}
				});
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.addGroups = function() {
			var groupIds = scope.modalData.addGroupIds;
			for (var i = 0; i < scope.modalData.metas.i18n.metaGroups.length; i++) {
				proximisCoreArray.removeValue(groupIds, scope.modalData.metas.i18n.metaGroups[i]._meta.id);
			}

			var params = { modelMetaId: parentData.documentId, LCID: parentData.LCID, groupIds: scope.modalData.addGroupIds };
			var requestData = proximisRestApiDefinition.getData('Rbs_Seo_ModelMetas.newValueGroups', 'GET', params);
			proximisRestApi.sendData(requestData).then(
				function(result) {
					angular.forEach(result.data.items, function(group) {
						//noinspection JSUnresolvedVariable
						scope.modalData.metas.i18n.metaGroups.push(group);
					});
					scope.modalData.addGroupIds = [];
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to load data related to ' + params.id + '".', result);
				}
			);
		};

		scope.removeGroup = function(index) {
			delete scope.modalData.metas.i18n.metaGroups.splice(index, 1);
		};

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { modelMetaId: parentData.documentId, LCID: parentData.LCID, metas: scope.modalData.metas };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelMetas.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();