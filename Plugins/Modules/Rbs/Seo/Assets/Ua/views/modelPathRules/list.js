/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisSeo');

	app.controller('ProximisSeoModelPathRulesListController',
		['$scope', '$rootScope', 'proximisModalStack', ProximisSeoModelPathRulesListController]);

	function ProximisSeoModelPathRulesListController(scope, $rootScope, proximisModalStack) {
		scope.globalData = {
			languages: [],
			restParams: {
				LCID: null
			}
		};

		$rootScope.$watch('user.settings.LCID', function(LCID) {
			if (LCID) {
				scope.globalData.restParams.LCID = LCID;
			}
		});

		// Actions.
		scope.actionMethods = {
			open: function(document) {
				proximisModalStack.open({
					templateUrl: 'Rbs/Seo/Ua/views/modelPathRules/detail-modal-content.twig',
					size: 'lg',
					controller: 'ProximisSeoModelPathRulesDetailController',
					resolve: {
						parentData: function() { return { documentId: document._meta.id, LCID: scope.globalData.restParams.LCID }; },
						refreshParent: function() { return scope.actionMethods.refresh; }
					}
				});
			}
		};

		// Filters.
		scope.staticFilterMethods = {
			none: {
				isValid: function() {
					return scope.globalData.restParams.LCID;
				}
			},
			label: {
				isValid: function(filter) {
					return scope.globalData.restParams.LCID && filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return null;
					}
					return {
						name: 'group',
						parameters: { operator: 'AND' },
						filters: [{
							name: 'label',
							parameters: {
								propertyName: 'label', operator: 'contains', value: filter.parameters.label
							}
						}]
					};
				}
			}
		};

		scope.$on('pxUaEntityList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);

			scope.$watch('globalData.restParams.LCID', function(LCID) {
				if (LCID && scope.actionMethods.refresh) {
					scope.actionMethods.refresh();
				}
			});
		});
	}

	/**
	 * parentData:
	 *  - documentId
	 *  - LCID
	 */
	app.controller('ProximisSeoModelPathRulesDetailController', ['$uibModalInstance', '$scope', 'parentData', 'refreshParent', 'proximisModalStack',
		'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages', 'proximisCoreI18n', ProximisSeoModelPathRulesDetailController]);

	function ProximisSeoModelPathRulesDetailController(modalInstance, scope, parentData, refreshParent, proximisModalStack, proximisRestApi,
		proximisRestApiDefinition, proximisMessages, proximisCoreI18n) {
		scope.modalData = {
			loadingData: true,
			savingData: false,
			errorMessage: null,
			LCID: null,
			document: null,
			pathRules: null,
			variables: [],
			addGroupIds: []
		};

		var params = { modelPathRulesId: parentData.documentId, LCID: parentData.LCID, fields: ['variable'] };
		proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelPathRules.detail', 'GET', params)).then(
			function(result) {
				scope.modalData.pathRules = result.data.item;
				scope.modalData.variables = result.data.variable;
				angular.forEach(result.data.item.languages, function(language) {
					if (language.LCID === params.LCID) {
						scope.modalData.language = language
					}
				});
				scope.modalData.loadingData = false;
			},
			function(result) {
				scope.modalData.errorMessage = result.data.status.message;
				console.error('Unable to load data related to ' + params.id + '".', result);
			}
		);

		scope.save = function() {
			scope.modalData.savingData = true;
			var params = { modelPathRulesId: parentData.documentId, LCID: parentData.LCID, pathRules: scope.modalData.pathRules };
			proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Seo_ModelPathRules.detail', 'PUT', params)).then(
				function(result) {
					var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
					proximisMessages.success(title, result.data.status.message, true, 10000);
					refreshParent();
					proximisModalStack.close();
				},
				function(result) {
					scope.modalData.errorMessage = result.data.status.message;
					console.error('Unable to save data related to ' + params.id + '".', result);
				}
			);
		};

		scope.cancel = function() {
			modalInstance.dismiss('cancel');
		};

		var closeFunction = function() {
			proximisModalStack.showPrevious();
		};
		modalInstance.result.then(closeFunction, closeFunction);
	}
})();