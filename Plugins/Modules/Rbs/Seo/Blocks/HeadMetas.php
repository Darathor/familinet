<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Blocks;

/**
 * @name \Rbs\Seo\Blocks\HeadMetas
 */
class HeadMetas extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('documentId');
		$parameters->setLayoutParameters($event->getBlockLayout());

		$page = $event->getParam('page');
		$document = $event->getParam('document');
		if ($document instanceof \Change\Documents\AbstractDocument)
		{
			$parameters->setParameterValue('documentId', $document->getId());
			if ($document != $page)
			{
				$pageFunction = $event->getHttpRequest()->getQuery('sectionPageFunction');
				if (!$pageFunction)
				{
					$pageFunction = $event->getHttpRequest()->getPost('sectionPageFunction');
				}
				$uri = $event->getHttpRequest()->getUriString();
				if (preg_match('/(\?|&)pageNumber\-/', $uri) || preg_match('/(\?|&)sortBy\-/', $uri))
				{
					$parameters->setParameterValue('robots', 'noindex, follow');
				}
				if (!$pageFunction && $page instanceof \Change\Presentation\Interfaces\Page)
				{
					$parameters->setParameterValue('isDetailPage', true);
					$section = $page->getSection();
					if ($section)
					{
						$website = $page->getSection()->getWebsite();
						if ($website instanceof \Change\Presentation\Interfaces\Website)
						{
							$parameters->setParameterValue('websiteId', $website->getId());
						}
					}
				}
			}
		}
		elseif ($page instanceof \Rbs\Website\Documents\Page)
		{
			$parameters->setParameterValue('documentId', $page->getId());
		}

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$genericServices = $event->getServices('genericServices');
		if ($genericServices instanceof \Rbs\Generic\GenericServices)
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$parameters = $event->getBlockParameters();

			$document = $documentManager->getDocumentInstance($parameters->getParameter('documentId'));
			if ($document instanceof \Change\Documents\AbstractDocument)
			{
				$attributes['document'] = $document;
			}

			if ($parameters->getParameter('robots'))
			{
				$attributes['robots'] = $parameters->getParameter('robots');
			}

			if ($parameters->getParameter('isDetailPage') && $parameters->getParameter('websiteId'))
			{
				$website = $documentManager->getDocumentInstance($parameters->getParameter('websiteId'));
				if ($website instanceof \Rbs\Website\Documents\Website)
				{
					$canonical = $website->getUrlManager($documentManager->getLCID())->getCanonicalByDocument($document);
					$canonicalURL = $canonical->normalize()->toString();
					$attributes['canonicalURL'] = $canonicalURL;
				}
			}
			elseif ($document instanceof \Rbs\Website\Documents\StaticPage)
			{
				$section = $document->getSection();
				if ($section && $section->getIndexPageId() === $document->getId())
				{
					$canonical = $section->getWebsite()->getUrlManager($documentManager->getLCID())->getCanonicalByDocument($section);
					$canonicalURL = $canonical->normalize()->toString();
					$attributes['canonicalURL'] = $canonicalURL;
				}
			}

			return 'head-metas.twig';
		}
		return null;
	}
}