<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Collection;

/**
 * @name \Rbs\Seo\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addMetaValueTypes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();

			$items = [];
			$items[\Rbs\Seo\Documents\Meta::TYPE_BOOLEAN] = new \Change\I18n\I18nString($i18n, 'm.rbs.seo.admin.meta_type_boolean', ['ucf']);
			$items[\Rbs\Seo\Documents\Meta::TYPE_INTEGER] = new \Change\I18n\I18nString($i18n, 'm.rbs.seo.admin.meta_type_integer', ['ucf']);
			$items[\Rbs\Seo\Documents\Meta::TYPE_FLOAT] = new \Change\I18n\I18nString($i18n, 'm.rbs.seo.admin.meta_type_float', ['ucf']);
			$items[\Rbs\Seo\Documents\Meta::TYPE_DATETIME] = new \Change\I18n\I18nString($i18n, 'm.rbs.seo.admin.meta_type_datetime', ['ucf']);
			$items[\Rbs\Seo\Documents\Meta::TYPE_STRING] = new \Change\I18n\I18nString($i18n, 'm.rbs.seo.admin.meta_type_string', ['ucf']);

			$collection = new \Change\Collection\CollectionArray('Rbs_Seo_MetaValueTypes', $items);
			$event->setParam('collection', $collection);
		}
	}
}