<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Documents;

/**
 * @name \Rbs\Seo\Documents\PublishableListeners
 */
class PublishableListeners
{
	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onSave(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$this->fixPublicationData($document, $documentManager);
	}

	/**
	 * @param \Change\Documents\Interfaces\Publishable $document
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function fixPublicationData(\Change\Documents\Interfaces\Publishable $document, \Change\Documents\DocumentManager $documentManager)
	{
		$publicationData = $document->getPublicationData();
		if (!is_array($publicationData))
		{
			$publicationData = [];
		}

		// Model configuration id.
		if (!isset($publicationData['modelConfigurationId']))
		{
			$dqb = $documentManager->getNewQuery('Rbs_Seo_ModelConfiguration');
			$dqb->andPredicates($dqb->eq('modelName', $document->getDocumentModelName()));
			/** @var \Rbs\Seo\Documents\ModelConfiguration $modelConfiguration */
			$modelConfiguration = $dqb->getFirstDocument();
			$publicationData['modelConfigurationId'] = $modelConfiguration->getId();
		}

		// Metas.
		$LCID = $documentManager->getLCID();
		$publicationMetas = $document->getPublicationMetas();
		if ($publicationMetas instanceof \Rbs\Seo\Std\PublicationMetas)
		{
			$publicationData['metas'][$LCID] = $publicationMetas->toArray();
		}
		else
		{
			if (!isset($publicationData['metas'][$LCID]))
			{
				$publicationData['metas'][$LCID] = [];
			}
			if (!isset($publicationData['metas'][$LCID]['mode']))
			{
				$defaultMode = null;
				$modelConfig = $documentManager->getDocumentInstance($publicationData['modelConfigurationId']);
				if ($modelConfig instanceof \Rbs\Seo\Documents\ModelConfiguration)
				{
					$publicationMetas = $modelConfig->getDefaultPublicationMetas();
					if ($publicationMetas)
					{
						$defaultMode = $publicationMetas->getMode();
					}
				}
				$publicationData['metas'][$LCID]['mode'] = $defaultMode ?: 'auto';
			}
		}

		// Sitemap.
		$publicationSitemap = $document->getPublicationSitemap();
		if ($publicationSitemap instanceof \Rbs\Seo\Std\PublicationSitemap)
		{
			$publicationData['sitemap'] = $publicationSitemap->toArray();
		}

		$document->setPublicationData($publicationData);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onGetPublicationMetas(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('metas') !== null)
		{
			return;
		}

		$metasData = $event->getParam('metasData');
		if (!$metasData)
		{
			$metasData = [];
		}

		$event->setParam('metas', new \Rbs\Seo\Std\PublicationMetas($metasData));
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onGetPublicationSitemap(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('sitemap') !== null)
		{
			return;
		}

		$event->setParam('sitemap', new \Rbs\Seo\Std\PublicationSitemap($event->getParam('sitemapData')));
	}
}