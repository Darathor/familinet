<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API;

/**
 * @name \Rbs\Seo\Http\API\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->onAction($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onAction(\Change\Http\Event $event)
	{
		$applicationName = $event->getParam('applicationName');
		if ($applicationName === 'seoManager')
		{
			$event->setParam('serverRealm', 'Rbs_Seo_' . $applicationName);
			if ($event->getAction())
			{
				return;
			}

			/** @var \Rbs\Ua\API\Definitions $definitions */
			$definitions = $event->getParam('definitions');
			$filePath = __DIR__ . '/../../Assets/Ua/rest.' . $applicationName . '.json';
			$definitions->initFromFile($filePath);
			$definitions->resolveApi($event);
		}
	}
}