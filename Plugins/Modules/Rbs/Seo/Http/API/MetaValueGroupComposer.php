<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API;

/**
 * @name \Rbs\Seo\Http\API\MetaValueGroupComposer
 */
class MetaValueGroupComposer
{
	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 */
	public function __construct(\Change\I18n\I18nManager $i18nManager)
	{
		$this->i18nManager = $i18nManager;
	}

	/**
	 * @param \Rbs\Seo\Std\PublicationMeta[] $indexedMetaValues
	 * @return array|null
	 */
	public function getDefaultMetaValueGroupData($indexedMetaValues)
	{
		return [
			'id' => 0,
			'model' => 'Rbs_Seo_MetaGroup',
			'label' => $this->i18nManager->trans('m.rbs.seo.ua.standard_metas', ['ucf']),
			'metas' => [
				[
					'id' => 0,
					'model' => 'Rbs_Seo_Meta',
					'label' => $this->i18nManager->trans('m.rbs.seo.ua.property_description', ['ucf']),
					'type' => 'name',
					'name' => 'description',
					'valueType' => 'String',
					'value' => $this->resolveMetaValue('name', 'description', $indexedMetaValues)
				],
				[
					'id' => 0,
					'model' => 'Rbs_Seo_Meta',
					'label' => $this->i18nManager->trans('m.rbs.seo.ua.property_keywords', ['ucf']),
					'type' => 'name',
					'name' => 'keywords',
					'valueType' => 'String',
					'value' => $this->resolveMetaValue('name', 'keywords', $indexedMetaValues)
				]
			]
		];
	}

	/**
	 * @param \Rbs\Seo\Documents\MetaGroup|null $group
	 * @param \Rbs\Seo\Std\PublicationMeta[] $indexedMetaValues
	 * @param boolean $useDefault
	 * @return array|null
	 */
	public function getMetaValueGroupData($group, $indexedMetaValues, $useDefault = false)
	{
		if ($group instanceof \Rbs\Seo\Documents\MetaGroup)
		{
			$groupData = [
				'id' => $group->getId(),
				'model' => $group->getDocumentModelName(),
				'label' => $group->getLabel(),
				'metas' => []
			];

			foreach ($group->getContent() as $metaDefinition)
			{
				$groupData['metas'][] = [
					'id' => $metaDefinition->getId(),
					'model' => $metaDefinition->getDocumentModelName(),
					'label' => $metaDefinition->getLabel(),
					'type' => $metaDefinition->getType(),
					'name' => $metaDefinition->getName(),
					'valueType' => $metaDefinition->getValueType(),
					'collectionCode' => $metaDefinition->getCollectionCode(),
					'value' => $this->resolveMetaDefinitionValue($metaDefinition, $indexedMetaValues, $useDefault)
				];
			}

			if (count($groupData['metas']))
			{
				return $groupData;
			}
		}
		return null;
	}

	/**
	 * @param string $type
	 * @param string $name
	 * @param \Rbs\Seo\Std\PublicationMeta[]|null $metaValues
	 * @return mixed
	 */
	protected function resolveMetaValue($type, $name, $metaValues)
	{
		if (isset($metaValues[$type . '=' . $name]))
		{
			$metaValue = $metaValues[$type . '=' . $name];
			return $metaValue->getValue();
		}
		return null;
	}

	/**
	 * @param \Rbs\Seo\Documents\Meta $metaDefinition
	 * @param \Rbs\Seo\Std\PublicationMeta[]|null $metaValues
	 * @param boolean $useDefault
	 * @return mixed
	 */
	protected function resolveMetaDefinitionValue(\Rbs\Seo\Documents\Meta $metaDefinition, $metaValues, $useDefault)
	{
		$type = $metaDefinition->getType();
		$name = $metaDefinition->getName();
		$value = $this->resolveMetaValue($type, $name, $metaValues);
		if ($value === null && $useDefault)
		{
			$value = $metaDefinition->getDefaultValue();
		}
		return $value;
	}

	/**
	 * @param \Change\Presentation\Interfaces\PublicationMetas|null $publicationMetas
	 * @param \Change\Presentation\Interfaces\PublicationMetas|null $defaultPublicationMetas
	 * @return \Rbs\Seo\Std\PublicationMeta[]
	 */
	public function prepareMetaValues($publicationMetas = null, $defaultPublicationMetas = null)
	{
		$indexedMetaValues = [];

		if ($defaultPublicationMetas instanceof \Change\Presentation\Interfaces\PublicationMetas)
		{
			$defaultMetaValues = $defaultPublicationMetas->getMetas();
			foreach ($defaultMetaValues as $metaValue)
			{
				$indexedMetaValues[$metaValue->getType() . '=' . $metaValue->getName()] = $metaValue;
			}
		}

		if ($publicationMetas instanceof \Change\Presentation\Interfaces\PublicationMetas)
		{
			$metasValues = $publicationMetas->getMetas();
			foreach ($metasValues as $metaValue)
			{
				$indexedMetaValues[$metaValue->getType() . '=' . $metaValue->getName()] = $metaValue;
			}
		}
		return $indexedMetaValues;
	}
}