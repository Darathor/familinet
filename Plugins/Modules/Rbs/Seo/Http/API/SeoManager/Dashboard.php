<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\Dashboard
 */
class Dashboard
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/** @var array */
	protected $websitesLabel = [];
	protected $sectionsLabel = [];

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getData($event)
	{
		$data = $this->getStatusCounters($event);

		$data['url404All'] = $this->getUrl404CountersSince($event, null);
		$since = new \DateTime();
		$since->sub(new \DateInterval('P1M'));
		$data['url404Month'] = $this->getUrl404CountersSince($event, $since);
		$since = new \DateTime();
		$since->sub(new \DateInterval('P7D'));
		$data['url404Week'] = $this->getUrl404CountersSince($event, $since);
		$since = new \DateTime();
		$since->sub(new \DateInterval('P1D'));
		$data['url404Day'] = $this->getUrl404CountersSince($event, $since);

		$event->setResult($this->buildArrayResult($event, ['item' => $data]));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return array
	 */
	protected function getStatusCounters($event)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$dbp = $event->getApplicationServices()->getDbProvider();
		$inputParameters = $event->getParam('inputParameters')->toArray();

		$websiteId = null;
		if (isset($inputParameters['websiteId']))
		{
			$websiteId = $inputParameters['websiteId'];
		}

		$qb = $dbp->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->count($fb->column('rule_id')), 'count'),
			$fb->alias($fb->column('website_id'), 'websiteId'),
			$fb->alias($fb->column('http_status'), 'httpStatus')
		);

		$qb->from($fb->table('change_path_rule'));

		if ($websiteId != null)
		{
			$qb->where($fb->eq($fb->column('website_id'), $fb->number($websiteId)));
		}

		$qb->group($fb->column('website_id'));
		$qb->group($fb->column('http_status'));

		$select = $qb->query();
		$queryData = $select->getResults($select->getRowsConverter()->addIntCol('count')->addIntCol('websiteId')->addIntCol('httpStatus'));

		$data = [
			'statusAll' => ['total' => 0, 'subCounters' => []],
			'status200' => ['total' => 0, 'subCounters' => []],
			'status301' => ['total' => 0, 'subCounters' => []],
			'status302' => ['total' => 0, 'subCounters' => []]
		];
		foreach ($queryData as $row)
		{
			$websiteId = $row['websiteId'];
			if (!isset($this->websitesLabel[$websiteId]))
			{
				/** @var \Rbs\Website\Documents\Website $website */
				$website = $documentManager->getDocumentInstance($websiteId, 'Rbs_Website_Website');
				$this->websitesLabel[$websiteId] = $website ? $website->getLabel() : '';
			}

			if (!$this->websitesLabel[$websiteId])
			{
				continue;
			}

			$key = 'status' . $row['httpStatus'];
			$data[$key]['subCounters'][$websiteId] = ['label' => $this->websitesLabel[$websiteId], 'count' => $row['count']];
			$data[$key]['total'] += $row['count'];

			if (!isset($data['statusAll']['subCounters'][$websiteId]))
			{
				$data['statusAll']['subCounters'][$websiteId] = ['label' => $this->websitesLabel[$websiteId], 'count' => $row['count']];
			}
			else
			{
				$data['statusAll']['subCounters'][$websiteId]['count'] += $row['count'];
			}
			$data['statusAll']['total'] += $row['count'];
		}

		return $data;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \DateTime|null $since
	 * @return array
	 */
	protected function getUrl404CountersSince($event, \DateTime $since = null)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->count($fb->column('rule_id')), 'count'),
			$fb->alias($fb->column('website_id'), 'websiteId')
		);

		$qb->from($fb->table('rbs_seo_dat_404'));

		$qb->where($fb->eq($fb->column('hidden'), $fb->number(0))); // Retrieve only non-hidden 404.

		if ($since)
		{
			$qb->andWhere($fb->gt($fb->column('insertion_date'), $fb->dateTimeParameter('insertion_date')));
		}

		$qb->group($fb->column('website_id'));

		$select = $qb->query();
		if ($since)
		{
			$select->bindParameter('insertion_date', $since);
		}

		$queryData = $select->getResults($select->getRowsConverter()->addIntCol('count')->addIntCol('websiteId'));

		$data = ['total' => 0, 'subCounters' => []];
		foreach ($queryData as $row)
		{
			$websiteId = $row['websiteId'];
			if (!isset($this->websitesLabel[$websiteId]))
			{
				/** @var \Rbs\Website\Documents\Website $website */
				$website = $documentManager->getDocumentInstance($websiteId, 'Rbs_Website_Website');
				$this->websitesLabel[$websiteId] = $website ? $website->getLabel() : '';
			}

			if (!$this->websitesLabel[$websiteId])
			{
				continue;
			}

			$data['subCounters'][$websiteId] = ['label' => $this->websitesLabel[$websiteId], 'count' => $row['count']];
			$data['total'] += $row['count'];
		}
		return $data;
	}
}