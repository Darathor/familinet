<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\DocumentForSelector
 */
class DocumentForSelector
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function search($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$searchString = $inputParameters->get('search');
		if (!$searchString)
		{
			return;
		}

		$model = $event->getApplicationServices()->getModelManager()->getModelByName($inputParameters->get('model'));
		if (!$model)
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$i18nManager = $applicationServices->getI18nManager();

		$responseGenerator = new \Rbs\Ua\API\DocumentsForSelectorResponseData($model->getName(), (int)$inputParameters->get('offset', 0),
			(int)$inputParameters->get('limit', 10), (string)$inputParameters->get('sort'), (bool)$inputParameters->get('desc', true));
		$responseGenerator->setDocumentManager($documentManager)->setI18nManager($i18nManager);
		$responseGenerator->setFilters($responseGenerator->buildSearchStringFilter($searchString));
		$responseGenerator->setWebsiteUrlManager($this->getUrlManager($event, $inputParameters));

		$event->setResult($this->buildArrayResult($event, $responseGenerator));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function load($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$i18nManager = $applicationServices->getI18nManager();

		$responseGenerator = new \Rbs\Ua\API\DocumentsForSelectorResponseData(null);
		$responseGenerator->setDocumentManager($documentManager)->setI18nManager($i18nManager)->setIds($inputParameters->get('documentIds'));
		$responseGenerator->setWebsiteUrlManager($this->getUrlManager($event, $inputParameters));

		$event->setResult($this->buildArrayResult($event, $responseGenerator));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Zend\Stdlib\Parameters $inputParameters
	 * @return \Change\Http\Web\UrlManager
	 */
	protected function getUrlManager($event, $inputParameters)
	{
		$context = $inputParameters->get('urlContext');
		if (is_array($context) && isset($context['websiteId'], $context['LCID']))
		{
			$documentManager = $event->getApplicationServices()->getDocumentManager();
			$website = $documentManager->getDocumentInstance($context['websiteId']);
			$LCID = $context['LCID'];
			if ($website instanceof \Rbs\Website\Documents\Website && $LCID)
			{
				$urlManager = $website->getUrlManager($LCID);
				if (isset($context['sectionId']))
				{
					$section = $documentManager->getDocumentInstance($context['sectionId']);
					if ($section instanceof \Change\Presentation\Interfaces\Section)
					{
						$urlManager->setSection($section);
					}
				}
				return $urlManager;
			}
		}
		return null;
	}
}