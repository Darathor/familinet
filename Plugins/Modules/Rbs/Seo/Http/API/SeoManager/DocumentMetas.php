<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\DocumentMetas
 */
class DocumentMetas
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$model = $applicationServices->getModelManager()->getModelByName($inputParameters->get('model'));
		if ($model && $model->isPublishable())
		{
			$query = $documentManager->getNewQuery($model, $LCID);
			$query->andPredicates($query->isNotNull('publicationStatus'));

			$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

			$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
		}

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$data = ['item' => $document];

		$inputFields = (array)$event->getParam('inputFields');
		if (\in_array('variable', $inputFields, true))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$seoManager = $genericServices->getSeoManager();
			$data['variable'] = $this->extractVariables($seoManager, $document);
		}

		$event->setResult($this->buildArrayResult($event, $data, new \Rbs\Seo\Http\API\Models($event), $inputFields));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$resolver = new \Rbs\Seo\Http\API\MetaValueResolver();
			$metasData = $document->getPublicationMetas();
			$metasData = $metasData instanceof \Rbs\Seo\Std\PublicationMetas ? $metasData : new \Rbs\Seo\Std\PublicationMetas();
			$oldValues = $resolver->indexMetaValues($metasData);

			$metas = $inputParameters->get('metas');
			$mode = $metas['i18n']['mode'];
			$metasData->setMode($mode);
			$groupIds = [];
			$metasArray = [];
			switch ($mode)
			{
				case 'none':
				case 'auto':
					$metasData->setTitle(null);
					break;

				case 'manual':
					$metasData->setTitle($metas['i18n']['title'] ?? null);

					$metaGroups = $metas['i18n']['metaGroups'] ?? [];
					foreach ($metaGroups as $metaGroup)
					{
						if ($metaGroup['_meta']['id'] === 0)
						{
							$newValues = $resolver->indexMetaValues($metaGroup['metas']);
							$metasArray[] = $resolver->resolveMeta('name', 'description', $newValues, $oldValues);
							$metasArray[] = $resolver->resolveMeta('name', 'keywords', $newValues, $oldValues);
							continue;
						}

						$group = $documentManager->getDocumentInstance($metaGroup['_meta']['id']);
						if (!($group instanceof \Rbs\Seo\Documents\MetaGroup))
						{
							continue;
						}
						$groupIds[] = $group->getId();

						$newValues = $resolver->indexMetaValues($metaGroup['metas']);
						foreach ($group->getContent() as $meta)
						{
							$metasArray[] = $resolver->resolveMeta($meta->getType(), $meta->getName(), $newValues, $oldValues);
						}
					}
					break;

				default:
					$mode = 'auto';
					$metasData->setMode($mode);
					$metasData->setTitle(null);
					break;
			}
			$metasData->setGroupIds($groupIds);
			$metasData->setMetas($metasArray);

			$document->setPublicationMetas($metasData);
			$document->save();

			$message = $i18nManager->trans('m.rbs.seo.ua.success_saving_document_metas', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function variables($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$variables = $this->extractVariables($seoManager, $documentManager->getDocumentInstance($inputParameters->get('documentId')));

		$event->setResult($this->buildArrayResult($event, ['items' => $variables], new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getNewValueGroups($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$groupsData = [];
		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if ($document instanceof \Change\Documents\Interfaces\Publishable && $inputParameters->get('groupIds'))
		{
			$groupComposer = new \Rbs\Seo\Http\API\MetaValueGroupComposer($i18nManager);
			$publicationMeta = $document->getPublicationMetas();
			$config = $documentManager->getDocumentInstance($document->getModelConfigurationId());
			$modelPublicationMetas = ($config instanceof \Rbs\Seo\Documents\ModelConfiguration) ? $config->getDefaultPublicationMetas() : null;
			$indexedMetaValues = $groupComposer->prepareMetaValues($publicationMeta, $modelPublicationMetas);
			foreach ($inputParameters->get('groupIds') as $groupId)
			{
				/** @noinspection PhpParamsInspection */
				$groupData = $groupComposer->getMetaValueGroupData($documentManager->getDocumentInstance($groupId), $indexedMetaValues, true);
				if ($groupData)
				{
					$groupsData[] = $groupData;
				}
			}
		}

		$event->setResult($this->buildArrayResult($event, ['items' => $groupsData], new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function extractVariables($seoManager, $document): array
	{
		$variables = [];
		if ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			$model = $document->getDocumentModel();
			$modelNames = array_merge($model->getAncestorsNames(), [$model->getName()]);
			foreach ($seoManager->getMetaVariables($modelNames) as $code => $label)
			{
				$variables[] = ['label' => $label, 'code' => $code];
			}
		}
		return $variables;
	}
}