<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\DocumentPathRules
 */
class DocumentPathRules
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();

		$model = $applicationServices->getModelManager()->getModelByName($inputParameters->get('model'));
		if ($model && $model->isPublishable())
		{
			$query = $documentManager->getNewQuery($model);
			$query->andPredicates($query->isNotNull('publicationStatus'));

			$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

			$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, ['locationGroups' => $this->getLocationGroups($event, $document)]);

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$document = $documentManager->getDocumentInstance($inputParameters->get('documentId'));
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			/** @var array $pathRules */
			$pathRules = $inputParameters->get('documentPathRules');
			$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();

			$this->updatePathRules($pathRules, $document->getId(), $pathRuleManager);

			$message = $i18nManager->trans('m.rbs.seo.ua.success_saving_document_path_rules', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\Interfaces\Publishable $document
	 * @return array
	 */
	protected function getLocationGroups($event, $document)
	{
		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$rules = [];
		foreach ($pathRuleManager->getAllForDocumentId($document->getId()) as $pathRule)
		{
			\Rbs\Ua\Model\ObjectModel::addAPIProperties($pathRule, ['id' => $pathRule->getRuleId()]);
			$key = $pathRule->getSectionId() . '-' . $pathRule->getWebsiteId() . '-' . $pathRule->getLCID();
			switch ($pathRule->getHttpStatus())
			{
				case 200:
					$rules[$key]['urls'][] = $pathRule;
					break;
				case 301:
				case 302:
					$rules[$key]['redirects'][] = $pathRule;
					break;
			}
		}

		$locationGroups = [];
		$sections = $document->getPublicationSections();
		$sections = ($sections instanceof \Change\Documents\DocumentArrayProperty) ? $sections->toArray() : $sections;
		$isPage = $document instanceof \Change\Presentation\Interfaces\Page;
		$isPageOrSection = $isPage || $document instanceof \Change\Presentation\Interfaces\Section;

		/* @var $section \Rbs\Website\Documents\Section */
		foreach ($sections as $section)
		{
			$website = $section->getWebsite();
			if (!($website instanceof \Rbs\Website\Documents\Website))
			{
				continue;
			}

			$sectionId = $isPageOrSection ? 0 : $section->getId();

			$LCIDs = $website->getLCIDArray();
			$LCIDs = $document instanceof \Change\Documents\Interfaces\Localizable ? array_intersect($LCIDs, $document->getLCIDArray()) : $LCIDs;
			foreach ($LCIDs as $LCID)
			{
				try
				{
					$documentManager->pushLCID($LCID);

					$canonical = $document->getCanonicalSection($website) == $section;
					$location = [
						'baseUrl' => $website->getUrlManager($LCID)->getByPathInfo(null)->normalize()->toString(),
						'urls' => [],
						'redirects' => []
					];

					if ($canonical)
					{
						$cLocation = $this->addDefaultUrl($location, $document, 0, $website, $LCID, $pathRuleManager, $rules);
						$cLocation['sectionId'] = 0;
						$cLocation['sectionLabel'] = '-';
						$cLocation['canonical'] = true;
						$this->addToGroup($locationGroups, $website, $LCID, $cLocation, $i18nManager);
					}

					if ($sectionId)
					{
						$sLocation = $this->addDefaultUrl($location, $document, $sectionId, $website, $LCID, $pathRuleManager, $rules);
						$sLocation['sectionId'] = $sectionId;
						$sLocation['sectionLabel'] = $section->getLabel();
						$sLocation['canonical'] = false;
						$this->addToGroup($locationGroups, $website, $LCID, $sLocation, $i18nManager);
					}

					$documentManager->popLCID();
				}
				catch (\Exception $e)
				{
					$documentManager->popLCID($e);
				}
			}
		}

		return $locationGroups;
	}

	/**
	 * @param array $location
	 * @param \Change\Documents\Interfaces\Publishable $document
	 * @param integer $sectionId
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $LCID
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @param array $rules
	 * @return array
	 */
	protected function addDefaultUrl($location, $document, $sectionId, $website, $LCID, $pathRuleManager, $rules)
	{
		$key = $sectionId . '-' . $website->getId() . '-' . $LCID;

		/* @var $document \Change\Documents\AbstractDocument */
		$pathRule = new \Change\Http\Web\PathRule();
		$pathRule->setWebsiteId($website->getId())->setLCID($LCID)->setDocumentId($document->getId())->setSectionId($sectionId)
			->setHttpStatus(200);

		$generatedPathRule = $pathRuleManager->populatePathRuleByDocument($pathRule, $document);
		if ($generatedPathRule)
		{
			$location['defaultRelativePath'] = $generatedPathRule->getRelativePath();
		}

		if (isset($rules[$key]['urls']))
		{
			$location['urls'] = array_merge($location['urls'], $rules[$key]['urls']);
		}

		if (isset($rules[$key]['redirects']))
		{
			$location['redirects'] = array_merge($location['redirects'], $rules[$key]['redirects']);
		}

		return $location;
	}

	/**
	 * @param array $locationGroups
	 * @param \Rbs\Website\Documents\Website $website
	 * @param string $LCID
	 * @param array $location
	 * @param \Change\I18n\I18nManager $i18nManager
	 */
	protected function addToGroup(&$locationGroups, $website, $LCID, $location, \Change\I18n\I18nManager $i18nManager)
	{
		$key = $website->getId() . '-' . $LCID;
		if (!isset($locationGroups[$key]))
		{
			$locationGroups[$key] = [
				'websiteId' => $website->getId(),
				'websiteLabel' => $website->getLabel(),
				'LCID' => $LCID,
				'LCIDLabel' => $i18nManager->transLCID($LCID),
				'locations' => []
			];
		}
		$locationGroups[$key]['locations'][] = $location;
	}

	/**
	 * @param array $pathRules
	 * @param integer $documentId
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @throws \Exception
	 */
	protected function updatePathRules($pathRules, $documentId, $pathRuleManager)
	{
		$toAdd = [];
		$toUpdate = [];
		/** @var \Change\Http\Web\PathRule[] $existingRules */
		$existingRules = [];
		$pathRuleDocumentId = $documentId;
		$pathRuleDocumentAliasId = 0;

		foreach ($pathRuleManager->getAllForDocumentId($pathRuleDocumentId) as $pathRule)
		{
			$existingRules[$pathRule->getRuleId()] = $pathRule;

			if ($pathRule->getHttpStatus() === 200)
			{
				$pathRuleDocumentId = $pathRule->getDocumentId();
			}

			if ($pathRule->getDocumentAliasId() && (!$pathRuleDocumentAliasId || $pathRule->getHttpStatus() === 200))
			{
				$pathRuleDocumentAliasId = $pathRule->getDocumentAliasId();
			}
		}

		foreach ($pathRules['locationGroups'] as $locationGroup)
		{
			foreach ($locationGroup['locations'] as $location)
			{
				foreach (['urls', 'redirects'] as $type)
				{
					foreach ($location[$type] as $rule)
					{
						$ruleId = (int)$rule['id'];
						if (isset($existingRules[$ruleId]) && $existingRules[$ruleId]->getHttpStatus() !== $rule['httpStatus'])
						{
							$toUpdate[] = $rule;
						}
						elseif ($ruleId < 0 && $rule['relativePath'])
						{
							$defaultRelativePath = $pathRuleManager->getDefaultRelativePath($documentId, $location['sectionId']);
							if ($defaultRelativePath != $rule['relativePath'])
							{
								$rule['websiteId'] = $locationGroup['websiteId'];
								$rule['LCID'] = $locationGroup['LCID'];
								$rule['sectionId'] = $location['sectionId'];
								$toAdd[] = $rule;
							}
						}
					}
				}
			}
		}

		if (count($toUpdate) || count($toAdd))
		{
			foreach ($toUpdate as $rule)
			{
				$pathRuleManager->updateRuleStatus($rule['id'], $rule['httpStatus']);
			}

			foreach ($toAdd as $rule)
			{
				$pathRule = $pathRuleManager->getNewRule($rule['websiteId'], $rule['LCID'], $rule['relativePath'],
					$pathRuleDocumentId, $rule['httpStatus'], $rule['sectionId'], $rule['query'] ?? null, true);
				$pathRule->setDocumentAliasId($pathRuleDocumentAliasId);
				$pathRuleManager->insertPathRule($pathRule);
			}
		}
	}
}