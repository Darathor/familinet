<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\MetaGroup
 */
class MetaGroup
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Seo_MetaGroup');
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('metaGroupId'));
		if (!($document instanceof \Rbs\Seo\Documents\MetaGroup))
		{
			return;
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, [
			'content' => $document->getContentIds()
		]);

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function insert($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$newData = $inputParameters->get('data');

			/** @var \Rbs\Seo\Documents\MetaGroup $document */
			$document = $documentManager->getNewDocumentInstanceByModelName('Rbs_Seo_MetaGroup');
			$document->setLabel($newData['label']);
			$content = [];
			foreach ($newData['content'] as $metaId)
			{
				$meta = $documentManager->getDocumentInstance($metaId);
				if ($meta instanceof \Rbs\Seo\Documents\Meta)
				{
					$content[] = $meta;
				}
			}
			$document->setContent($content);
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_creating_meta_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('metaGroupId'));
		if (!($document instanceof \Rbs\Seo\Documents\MetaGroup))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$newData = $inputParameters->get('data');
			$document->setLabel($newData['label']);
			$content = [];
			foreach ($newData['content'] as $metaId)
			{
				$meta = $documentManager->getDocumentInstance($metaId);
				if ($meta instanceof \Rbs\Seo\Documents\Meta)
				{
					$content[] = $meta;
				}
			}
			$document->setContent($content);
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_updating_meta_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function delete($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('metaGroupId'));
		if (!($document instanceof \Rbs\Seo\Documents\MetaGroup))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$document->delete();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_deleting_meta_group', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}
}