<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\ModelMetas
 */
class ModelMetas
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$query = $documentManager->getNewQuery('Rbs_Seo_ModelConfiguration');
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('modelMetaId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		$data = ['item' => $document];

		$inputFields = (array)$event->getParam('inputFields');
		if (in_array('variable', $inputFields))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$seoManager = $genericServices->getSeoManager();
			$modelManager = $event->getApplicationServices()->getModelManager();
			$data['variable'] = $this->extractVariables($seoManager, $modelManager, $document);
		}

		$event->setResult($this->buildArrayResult($event, $data, new \Rbs\Seo\Http\API\Models($event), $inputFields));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('modelMetaId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$resolver = new \Rbs\Seo\Http\API\MetaValueResolver();
			$defaultMetaData = $document->getDefaultMetasData();
			$metasData = $defaultMetaData['metas'][$LCID] ?? [];
			$oldValues = $resolver->indexMetaValues($metasData['metas']);

			$metas = $inputParameters->get('metas');
			$metasData['title'] = $metas['i18n']['title'] ?? null;
			$metasData['mode'] = $metas['i18n']['mode'];
			$metasData['groupIds'] = [];
			$metasData['metas'] = [];

			$metaGroups = $metas['i18n']['metaGroups'] ?? [];
			foreach ($metaGroups as $metaGroup)
			{
				if ($metaGroup['_meta']['id'] === 0)
				{
					$newValues = $resolver->indexMetaValues($metaGroup['metas']);
					$metasData['metas'][] = $resolver->resolveMeta('name', 'description', $newValues, $oldValues)->toArray();
					$metasData['metas'][] = $resolver->resolveMeta('name', 'keywords', $newValues, $oldValues)->toArray();
					continue;
				}

				$group = $documentManager->getDocumentInstance($metaGroup['_meta']['id']);
				if (!($group instanceof \Rbs\Seo\Documents\MetaGroup))
				{
					continue;
				}
				$metasData['groupIds'][] = $group->getId();

				$newValues = $resolver->indexMetaValues($metaGroup['metas']);
				foreach ($group->getContent() as $meta)
				{
					$metasData['metas'][] = $resolver->resolveMeta($meta->getType(), $meta->getName(), $newValues, $oldValues)->toArray();
				}
			}

			$defaultMetaData[$LCID] = $metasData;
			$document->setDefaultMetasData($defaultMetaData);
			$document->save();

			$message = $i18nManager->trans('m.rbs.seo.ua.success_saving_model_metas', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function variables($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$modelManager = $event->getApplicationServices()->getModelManager();
		$variables = $this->extractVariables($seoManager, $modelManager, $documentManager->getDocumentInstance($inputParameters->get('modelMetaId')));

		$event->setResult($this->buildArrayResult($event, ['items' => $variables], new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getNewValueGroups($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$groupsData = [];
		$document = $documentManager->getDocumentInstance($inputParameters->get('modelMetaId'));
		if ($inputParameters->get('groupIds') && $document instanceof \Rbs\Seo\Documents\ModelConfiguration)
		{
			$publicationMeta = $document->getDefaultPublicationMetas();
			$groupComposer = new \Rbs\Seo\Http\API\MetaValueGroupComposer($i18nManager);
			$indexedMetaValues = $groupComposer->prepareMetaValues($publicationMeta);
			foreach ($inputParameters->get('groupIds') as $groupId)
			{
				/** @noinspection PhpParamsInspection */
				$groupData = $groupComposer->getMetaValueGroupData($documentManager->getDocumentInstance($groupId), $indexedMetaValues, true);
				if ($groupData)
				{
					$groupsData[] = $groupData;
				}
			}
		}

		$event->setResult($this->buildArrayResult($event, ['items' => $groupsData], new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @param \Change\Documents\ModelManager $modelManager
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function extractVariables($seoManager, $modelManager, $document): array
	{
		$variables = [];
		if ($document instanceof \Rbs\Seo\Documents\ModelConfiguration)
		{
			$model = $modelManager->getModelByName($document->getModelName());
			$modelNames = array_merge($model->getAncestorsNames(), [$model->getName()]);
			foreach ($seoManager->getMetaVariables($modelNames) as $code => $label)
			{
				$variables[] = ['label' => $label, 'code' => $code];
			}
			return $variables;
		}
		return $variables;
	}
}