<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\ModelPathRules
 */
class ModelPathRules
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$query = $documentManager->getNewQuery('Rbs_Seo_ModelConfiguration');
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('modelPathRulesId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		$data = ['item' => $document];

		$inputFields = (array)$event->getParam('inputFields');
		if (in_array('variable', $inputFields))
		{
			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$seoManager = $genericServices->getSeoManager();
			$data['variable'] = $this->extractVariables($seoManager, $document);
		}

		$event->setResult($this->buildArrayResult($event, $data, new \Rbs\Seo\Http\API\Models($event), $inputFields));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		$document = $documentManager->getDocumentInstance($inputParameters->get('modelPathRulesId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$data = $inputParameters->get('pathRules')['common'];
			$pathTemplate = $data['pathTemplate'] ?? null;
			$document->getCurrentLocalization()->setPathTemplate($pathTemplate ?: null);
			$document->save();

			$message = $i18nManager->trans('m.rbs.seo.ua.success_saving_model_path_rules', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));

		$documentManager->popLCID();
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getVariables($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$LCID = $inputParameters->get('LCID');
		$documentManager->pushLCID($LCID);

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();
		$variables = $this->extractVariables($seoManager, $documentManager->getDocumentInstance($inputParameters->get('modelPathRulesId')));

		$event->setResult($this->buildArrayResult($event, ['items' => $variables]));

		$documentManager->popLCID();
	}

	/**
	 * @param \Rbs\Seo\SeoManager $seoManager
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array
	 */
	protected function extractVariables($seoManager, $document): array
	{
		$variables = [];
		if ($document instanceof \Rbs\Seo\Documents\ModelConfiguration)
		{
			foreach ($seoManager->getPathVariables($document->getModelName()) as $code => $label)
			{
				$variables[] = ['label' => $label, 'code' => $code];
			}
		}
		return $variables;
	}
}