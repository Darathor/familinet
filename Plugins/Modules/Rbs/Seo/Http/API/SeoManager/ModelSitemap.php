<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\ModelSitemap
 */
class ModelSitemap
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$query = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Seo_ModelConfiguration');
		$value = $this->getDocumentListResult($event->getApplicationServices()->getModelManager(), $inputParameters->toArray(), $query);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('modelSitemapId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($document, [
			'data' => [
				'frequency' => $document->getSitemapDefaultChangeFrequency(),
				'priority' => $document->getSitemapDefaultPriority()
			]
		]);

		$event->setResult($this->buildArrayResult($event, ['item' => $document], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function update($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('modelSitemapId'));
		if (!($document instanceof \Rbs\Seo\Documents\ModelConfiguration))
		{
			return;
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$newSitemapData = $inputParameters->get('data');
			if (array_key_exists('frequency', $newSitemapData))
			{
				$document->setSitemapDefaultChangeFrequency($newSitemapData['frequency'] ?: null);
			}

			if (array_key_exists('priority', $newSitemapData))
			{
				$document->setSitemapDefaultPriority($newSitemapData['priority'] ?: null);
			}
			$document->save();

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_saving_model_sitemap', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}
}