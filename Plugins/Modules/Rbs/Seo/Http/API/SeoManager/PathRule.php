<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\PathRule
 */
class PathRule
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$context = $inputParameters->toArray();
		$dbProvider = $event->getApplicationServices()->getDbProvider();

		$qb = $this->getQuery($dbProvider);
		$rowConverter = $qb->query()->getRowsConverter()
			->addIntCol('id', 'websiteId', 'documentId', 'documentAliasId', 'sectionId', 'httpStatus')
			->addStrCol('lcid', 'relativePath')->addTxtCol('query')->addBoolCol('userEdited');
		
		$value = $this->getListResult($context, $this->getQueryCount($dbProvider), $qb, $rowConverter);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function get($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$query = $this->getQuery($event->getApplicationServices()->getDbProvider(), $inputParameters->get('pathRuleId', 0));
		$row = $query->query()->getFirstResult();
		if ($row)
		{
			$event->setResult($this->buildArrayResult($event, ['item' => $row], new \Rbs\Seo\Http\API\Models($event)));
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function delete($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
			$rule = $pathRuleManager->getPathRuleById($inputParameters->get('pathRuleId'));
			if ($rule)
			{
				$rule->setHttpStatus(404);
				$event->getApplicationServices()->getPathRuleManager()->updatePathRule($rule);
			}

			$message = $event->getApplicationServices()->getI18nManager()->trans('m.rbs.seo.ua.success_deleting_path_rule', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function insert($event)
	{
		$i18n = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		$documentManager = $event->getApplicationServices()->getDocumentManager();

		$website = $documentManager->getDocumentInstance($inputParameters->get('websiteId'));
		$targetType = $inputParameters->get('targetType');
		$targetId = $inputParameters->get('targetId');

		$documentId = null;
		$sectionId = 0;
		$query = null;
		if ($targetType === 'document')
		{
			$documentId = $targetId;
		}
		elseif ($targetType === 'rule')
		{
			$targetRule = $pathRuleManager->getPathRuleById($targetId);
			if ($targetRule)
			{
				$documentId = $targetRule->getDocumentId();
				$sectionId = $targetRule->getSectionId();
				$query = $targetRule->getQuery();
			}
		}

		if (!$website || !$documentId)
		{
			throw new \RuntimeException('You must specify a valid target!', 999999);
		}

		$LCID = $inputParameters->get('LCID');
		$relativePath = $inputParameters->get('relativePath');
		$httpStatus = $inputParameters->get('httpStatus');

		$existingRule = $pathRuleManager->getPathRule($website->getId(), $LCID, $relativePath);
		if ($existingRule)
		{
			throw new \RuntimeException($i18n->trans('m.rbs.seo.ua.this_url_already_exists', ['ucf']));
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$rule = $pathRuleManager->getNewRule($website->getId(), $LCID, $relativePath, $documentId, $httpStatus, $sectionId, $query, true);


			$pathRuleManager->insertPathRule($rule);
			$message = $i18n->trans('m.rbs.seo.ua.success_redirecting_rule', ['ucf']);

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function search($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$searchString = $inputParameters->get('search');
		if (!$searchString)
		{
			return;
		}

		$context = $inputParameters->toArray();

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$query = $this->getQuery($dbProvider);
		$fb = $query->getFragmentBuilder();
		$query->where($fb->like('relative_path', $fb->string($searchString)));

		$rowConverter = $query->query()->getRowsConverter()
			->addIntCol('id', 'websiteId', 'documentId', 'documentAliasId', 'sectionId', 'httpStatus')
			->addStrCol('lcid', 'relativePath')->addTxtCol('query')->addBoolCol('userEdited');

		$queryCount = $this->getQueryCount($dbProvider);
		$fb = $queryCount->getFragmentBuilder();
		$queryCount->where($fb->logicAnd(
			$fb->like('relative_path', $fb->string($searchString)),
			$fb->eq('http_status', $fb->number(200))
		));

		$value = $this->getListResult($context, $queryCount, $query, $rowConverter);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function check($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$pathRuleManager = $event->getApplicationServices()->getPathRuleManager();
		$rule = $pathRuleManager->getPathRule($inputParameters->get('websiteId'), $inputParameters->get('LCID'),
			$inputParameters->get('relativePath'));
		$value = $rule ? ['used' => true, 'rule' => $rule->toArray()] : ['used' => false];

		$event->setResult($this->buildArrayResult($event, ['item' => $value], new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @param integer|null $id
	 * @return \Change\Db\Query\Builder
	 */
	protected function getQuery($dbProvider, $id = null)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->column('rule_id'), 'id'),
			$fb->alias($fb->column('website_id'), 'websiteId'),
			$fb->alias($fb->column('lcid'), 'lcid'),
			$fb->alias($fb->column('relative_path'), 'relativePath'),
			$fb->alias($fb->column('document_id'), 'documentId'),
			$fb->alias($fb->column('document_alias_id'), 'documentAliasId'),
			$fb->alias($fb->column('section_id'), 'sectionId'),
			$fb->alias($fb->column('http_status'), 'httpStatus'),
			$fb->alias($fb->column('query'), 'query'),
			$fb->alias($fb->column('user_edited'), 'userEdited')
		);

		$qb->from($fb->table('change_path_rule'));

		if ($id !== null)
		{
			$qb->where($fb->eq($fb->column('rule_id'), $fb->number($id)));
		}

		return $qb;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Change\Db\Query\Builder
	 */
	protected function getQueryCount($dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->count($fb->column('rule_id')), 'count')
		);

		$qb->from($fb->table('change_path_rule'));

		return $qb;
	}
}