<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\Url404
 */
class Url404
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list($event)
	{
		$dbProvider = $event->getApplicationServices()->getDbProvider();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$context = $inputParameters->toArray();
		$query = $this->getQuery($dbProvider);
		$queryCount = $this->getQueryCount($dbProvider);
		$rowConverter = $query->query()->getRowsConverter()->addIntCol('id', 'websiteId', 'documentId', 'hits')->addStrCol('lcid', 'relativePath')
			->addLobCol('ruleData', 'documentData')->addDtCol('insertionDate')->addBoolCol('hidden');

		$value = $this->getListResult($context, $queryCount, $query, $rowConverter);

		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Http\Event $event
	 */
	public function related($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$context = $inputParameters->toArray();
		$ruleId = $inputParameters->get('ruleId');

		$dbProvider = $event->getApplicationServices()->getDbProvider();
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('document_id'));
		$qb->from($fb->table('rbs_seo_dat_404'));
		$qb->where($fb->eq($fb->column('rule_id'), $fb->number($ruleId)));
		$sq = $qb->query();
		$documentId = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('document_id'));

		if ($documentId)
		{
			$query = $this->getQuery($dbProvider);
			$fb = $query->getFragmentBuilder();
			$query->where($fb->logicAnd(
				$fb->neq($fb->column('rule_id'), $ruleId),
				$fb->eq($fb->column('document_id', 'rbs_seo_dat_404'), $documentId)
			));

			$queryCount = $this->getQueryCount($dbProvider);
			$fb = $queryCount->getFragmentBuilder();
			$queryCount->where($fb->logicAnd(
				$fb->neq($fb->column('rule_id'), $ruleId),
				$fb->eq($fb->column('document_id', 'rbs_seo_dat_404'), $documentId)
			));

			$rowConverter = $query->query()->getRowsConverter()->addIntCol('id', 'websiteId', 'documentId', 'hits')->addStrCol('lcid', 'relativePath')
				->addLobCol('ruleData', 'documentData')->addDtCol('insertionDate')->addBoolCol('hidden');

			$value = $this->getListResult($context, $queryCount, $query, $rowConverter);
		}
		else
		{
			$value = ['items' => [], 'pagination' => $this->resolvePagination(null, 0)];
		}
		$event->setResult($this->buildArrayResult($event, $value, new \Rbs\Seo\Http\API\Models($event)));
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Change\Db\Query\Builder
	 */
	protected function getQuery($dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->column('rule_id'), 'id'),
			$fb->alias($fb->column('website_id'), 'websiteId'),
			$fb->alias($fb->column('lcid'), 'lcid'),
			$fb->alias($fb->column('relative_path'), 'relativePath'),
			$fb->alias($fb->column('document_id', 'rbs_seo_dat_404'), 'documentId'),
			$fb->alias($fb->column('rule_data'), 'ruleData'),
			$fb->alias($fb->column('hits'), 'hits'),
			$fb->alias($fb->column('insertion_date'), 'insertionDate'),
			$fb->alias($fb->column('hidden'), 'hidden'),
			$fb->alias($fb->column('datas', 'change_document_deleted'), 'documentData')
		);

		$qb->from($fb->table('rbs_seo_dat_404'));

		$qb->leftJoin(
			$fb->table('change_document_deleted'),
			$fb->eq($fb->column('document_id', 'change_document_deleted'), $fb->column('document_id', 'rbs_seo_dat_404'))
		);
		return $qb;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return \Change\Db\Query\Builder
	 */
	protected function getQueryCount($dbProvider)
	{
		$qb = $dbProvider->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select(
			$fb->alias($fb->count($fb->column('rule_id')), 'count')
		);

		$qb->from($fb->table('rbs_seo_dat_404'));
		return $qb;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function hide($event)
	{
		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();

		$i18n = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$ruleIds = $inputParameters->get('ruleIds');
		if (!$ruleIds)
		{
			throw new \RuntimeException('You must specify at least one rule to hide!', 999999);
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$seoManager->hide404Rules($ruleIds);
			if (count($ruleIds) > 1)
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_hiding_rules', ['ucf']);
			}
			else
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_hiding_rule', ['ucf']);
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function show($event)
	{
		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();

		$i18n = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$ruleIds = $inputParameters->get('ruleIds');
		if (!$ruleIds)
		{
			throw new \RuntimeException('You must specify at least one rule to show!', 999999);
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$seoManager->show404Rules($ruleIds);
			if (count($ruleIds) > 1)
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_showing_rules', ['ucf']);
			}
			else
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_showing_rule', ['ucf']);
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function redirectToDocument($event)
	{
		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();

		$i18n = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$ruleIds = $inputParameters->get('ruleIds');
		if (!$ruleIds)
		{
			throw new \RuntimeException('You must specify at least one rule to redirect!', 999999);
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$target = $documentManager->getDocumentInstance($inputParameters->get('targetId'));
		$website = $documentManager->getDocumentInstance($inputParameters->get('websiteId'));
		$LCID = $inputParameters->get('LCID');
		if (!$target || !$website || !$LCID)
		{
			throw new \RuntimeException('You must specify a valid target!', 999999);
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$seoManager->redirect404RulesToDocument($ruleIds, $target->getId(), $LCID, $website->getId());
			if (count($ruleIds) > 1)
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_redirecting_rules', ['ucf']);
			}
			else
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_redirecting_rule', ['ucf']);
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->setResult($this->buildSuccessResult($event, $message));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function redirectToPathRule($event)
	{
		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();

		$i18n = $event->getApplicationServices()->getI18nManager();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$ruleIds = $inputParameters->get('ruleIds');
		if (!$ruleIds)
		{
			throw new \RuntimeException('You must specify at least one rule to redirect!', 999999);
		}

		$target = $event->getApplicationServices()->getPathRuleManager()->getPathRuleById($inputParameters->get('targetId'));
		if (!$target)
		{
			throw new \RuntimeException('You must specify a valid target!', 999999);
		}

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$seoManager->redirect404RulesToPathRule($ruleIds, $target);
			if (count($ruleIds) > 1)
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_redirecting_rules', ['ucf']);
			}
			else
			{
				$message = $i18n->trans('m.rbs.seo.ua.success_redirecting_rule', ['ucf']);
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}
		$event->setResult($this->buildSuccessResult($event, $message));
	}
}