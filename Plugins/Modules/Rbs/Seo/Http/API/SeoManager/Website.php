<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\API\SeoManager;

/**
 * @name \Rbs\Seo\Http\API\SeoManager\Website
 */
class Website
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getList($event)
	{
		$data = [];
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$websites = $documentManager->getNewQuery('Rbs_Website_Website')->getDocuments()->preLoad();
		foreach ($websites as $website)
		{
			/** @var \Rbs\Website\Documents\Website $website */
			foreach ($website->getLCIDArray() as $LCID)
			{
				try
				{
					$documentManager->pushLCID($LCID);

					$baseUrl = $website->getBaseurl();
					if ($baseUrl)
					{
						$data[] = [
							'label' => $website->getLabel() . ' - ' . $i18nManager->transLCID($LCID),
							'id' => $website->getId(),
							'LCID' => $LCID,
							'baseUrl' => $baseUrl
						];
					}

					$documentManager->popLCID();
				}
				catch (\Exception $e)
				{
					$documentManager->popLCID($e);
				}
			}
		}

		$event->setResult($this->buildArrayResult($event, ['items' => $data]));
	}
}