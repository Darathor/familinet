<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\UI;

/**
 * @name \Rbs\Seo\Http\UI\Home
 */
class Home extends \Rbs\Ua\Http\UI\HomeBase
{
	/**
	 * @return string
	 */
	protected function getVendor()
	{
		return 'Rbs';
	}

	/**
	 * @return string
	 */
	protected function getModule()
	{
		return 'Seo';
	}
}