<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\UI;

/**
 * @name \Rbs\Seo\Http\UI\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$this->listeners[] = $events->attach(\Change\Http\Event::EVENT_ACTION, function ($event) { $this->onAction($event); });
		$this->listeners[] = $events->attach('ApplicationsMenu', function ($event) { $this->onApplicationsMenu($event); });
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onApplicationsMenu(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$applicationsMenu = $event->getParam('applicationsMenu');

		$applicationsMenu['contents']['items'][] = [
			'position' => 20,
			'name' => 'seoManager',
			'realm' => 'Rbs_Seo_seoManager',
			'title' => $i18nManager->trans('m.rbs.seo.ua.seoManager_title', ['ucf'])
		];

		$event->setParam('applicationsMenu', $applicationsMenu);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return void
	 */
	protected function onAction(\Change\Http\Event $event)
	{
		if ($event->getAction())
		{
			return;
		}
		$applicationName = $event->getParam('applicationName');
		if ($applicationName === 'seoManager')
		{
			$event->setAction([new \Rbs\Seo\Http\UI\Home(), 'execute']);
		}
	}
}