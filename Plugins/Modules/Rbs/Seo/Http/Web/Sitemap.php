<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Http\Web;

/**
 * @name \Rbs\Seo\Http\Web\Sitemap
 */
class Sitemap
{
	/**
	 * @param \Change\Http\Web\Event $event
	 */
	public function getIndex(\Change\Http\Web\Event $event)
	{
		$host = $event->getParam('host');
		$uri = $event->getRequest()->getUri();

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$storageManager =  $event->getApplicationServices()->getStorageManager();

		$sitemapGenerator = new \Rbs\Seo\Std\SitemapGenerator();
		$websiteIds = $sitemapGenerator->getWebsiteIds($host, $documentManager);
		$sitemapIndex = [];
		foreach ($websiteIds as $websiteData)
		{
			list($websiteId, $LCID) = $websiteData;
			$baseDir = $storageManager->buildChangeURI('files', '/Rbs/Seo/' . $websiteId . '/' . $LCID . '/')->normalize()->toString();
			if (is_dir($baseDir))
			{
				foreach (scandir($baseDir) as $fileName)
				{
					$filePath = $baseDir . $fileName;
					if (is_file($filePath) && substr($fileName, -4) === '.xml')
					{
						$path = '/Assets/Rbs/Seo/' . $websiteId . '/' . $LCID . '/' . $fileName;
						$loc = $uri->setPath($path)->normalize()->toString();
						$lastMod = \DateTime::createFromFormat('U', filemtime($filePath));
						$sitemapIndex[] = ['loc' => $loc, 'lastmod' => $lastMod->format(\DateTime::W3C)];
					}
				}
			}
		}

		$result = new \Change\Http\Web\Result\RawResult(\Zend\Http\Response::STATUS_CODE_200);
		$result->getHeaders()->addHeaderLine('Content-type', 'text/xml');
		$xml = $sitemapGenerator->generateSitemapIndex($sitemapIndex);
		$result->setContent($xml->saveXML());
		$event->setResult($result);
	}


	/**
	 * @param \Change\Http\Web\Event $event
	 */
	public function getSitemap(\Change\Http\Web\Event $event)
	{
		$storageManager =  $event->getApplicationServices()->getStorageManager();
		$websiteId = $event->getParam('websiteId');
		$LCID =  $event->getParam('LCID');
		$fileName =  $event->getParam('fileName');
		$storageURI = $storageManager->buildChangeURI('files', '/Rbs/Seo/' . $websiteId . '/' . $LCID . '/' . $fileName)->normalize()->toString();
		$itemInfo = $storageManager->getItemInfo($storageURI);
		if ($itemInfo && $itemInfo->isReadable())
		{
			$event->setParam('itemInfo', $itemInfo);
			$result = new \Change\Http\Result(\Zend\Http\Response::STATUS_CODE_200);
			$result->setHeaderLastModified(\DateTime::createFromFormat('U', $itemInfo->getMTime()));

			$event->getController()->getEventManager()
				->attach(\Change\Http\Event::EVENT_RESPONSE, function ($event) {
					return $this->onResultContent($event);
			}, 100);
		}
		else
		{
			$result = new \Change\Http\Result(\Zend\Http\Response::STATUS_CODE_404);
		}
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onResultContent($event)
	{
		/** @var \Change\Storage\ItemInfo $itemInfo */
		$itemInfo = $event->getParam('itemInfo');

		/* @var $result \Change\Http\Result */
		$result = $event->getResult();
		$resultNotModified = $event->getController()->resultNotModified($event->getRequest(), $result);
		if ($resultNotModified)
		{
			$response = $event->getController()->createResponse();
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_304);
		}
		else
		{
			$response = new \Change\Http\StreamResponse();
			$response->setStatusCode(\Zend\Http\Response::STATUS_CODE_200);
			$response->setHeaders($result->getHeaders());
			$response->getHeaders()->addHeaderLine('Content-Type', 'text/xml');
			$response->setUri($itemInfo->getPathname());
		}
		return $response;
	}
}