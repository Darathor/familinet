<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
 * @name \Rbs\Seo\Job\DocumentCleanUp
 */
class DocumentCleanUp
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function cleanUp(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();
		$documentId = $job->getArgument('id');
		$modelName = $job->getArgument('model');
		if (!is_numeric($documentId) || !is_string($modelName))
		{
			$event->failed('Invalid Arguments ' . $documentId . ', ' . $modelName);
			return;
		}

		$model = $applicationServices->getModelManager()->getModelByName($modelName);
		if (!$model)
		{
			$event->failed('Document Model ' . $modelName . ' not found');
			return;
		}

		if ($model->isInstanceOf('Rbs_Website_Website'))
		{
			$transactionManager = $applicationServices->getTransactionManager();
			try
			{
				$transactionManager->begin();

				/* @var $genericServices \Rbs\Generic\GenericServices */
				$genericServices = $event->getServices('genericServices');
				$genericServices->getSeoManager()->delete404RulesByWebsiteId($documentId);

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}
		elseif ($model->isPublishable())
		{
			$qb = $applicationServices->getDbProvider()->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select($fb->column('rule_id'), $fb->column('relative_path'), $fb->column('hash'), $fb->column('website_id'), $fb->column('lcid'),
				$fb->column('section_id'), $fb->column('document_id'), $fb->column('document_alias_id'), $fb->column('http_status'),
				$fb->column('query'), $fb->column('user_edited'));
			$qb->from($qb->getSqlMapping()->getPathRuleTable());
			$qb->where(
				$fb->logicAnd(
					$fb->isNull($fb->column('query')),
					$fb->logicOr(
						$fb->eq($fb->column('document_id'), $fb->integerParameter('documentId')),
						$fb->eq($fb->column('document_alias_id'), $fb->integerParameter('documentAliasId'))
					)
				)
			);
			$sq = $qb->query();
			$sq->bindParameter('websiteId', $documentId);
			$sq->bindParameter('documentId', $documentId);
			$sq->bindParameter('documentAliasId', $documentId);

			$rules = $sq->getResults($sq->getRowsConverter()
				->addIntCol('rule_id', 'website_id', 'section_id', 'document_id', 'document_alias_id', 'http_status')
				->addTxtCol('relative_path', 'hash', 'lcid', 'query')
				->addBoolCol('user_edited'));

			if ($rules)
			{
				$applicationServices->getJobManager()->createNewJob('Rbs_Seo_SaveDeletedPathRules', ['rules' => $rules]);
			}
		}
	}
}