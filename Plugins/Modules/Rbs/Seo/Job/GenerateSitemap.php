<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
 * @see http://www.sitemaps.org/protocol.html
 * @name \Rbs\Seo\Job\GenerateSitemap
 */
class GenerateSitemap
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$LCID = $event->getJob()->getArgument('LCID');
		if (!$LCID)
		{
			$event->failed('GenerateSitemap: Parameter LCID not found');
			return;
		}
		$websiteId = (int)$event->getJob()->getArgument('websiteId');
		if (!$websiteId)
		{
			$event->failed('GenerateSitemap: Parameter websiteId not found');
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$applicationServices->getI18nManager()->setLCID($LCID);

		$documentManager = $applicationServices->getDocumentManager();

		/** @var \Rbs\Website\Documents\Website $website */
		$website = $documentManager->getDocumentInstance($websiteId);
		if (!$website instanceof \Rbs\Website\Documents\Website)
		{
			$event->failed('GenerateSitemap: Website not found');
			return;
		}
		unset($website);

		$storageManager = $event->getApplicationServices()->getStorageManager();
		$tmpBaseDir = $storageManager->buildChangeURI('tmp', '/Rbs.Seo.' . $websiteId . '.' . $LCID . '/')->normalize()->toString();
		\Change\Stdlib\FileUtils::mkdir($tmpBaseDir);

		$sitemapGenerator = new \Rbs\Seo\Std\SitemapGenerator();
		$fileNames = $sitemapGenerator->generateSitemap($tmpBaseDir, $websiteId, $documentManager);

		$timeInterval = null;
		$transactionManager = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$siteMaps = [];
			/** @var \Rbs\Website\Documents\Website $website */
			$website = $applicationServices->getDocumentManager()->getDocumentInstance($websiteId);
			foreach ($website->getSitemaps() as $sitemap)
			{
				if ($sitemap['LCID'] === $LCID)
				{
					$urlManager = $website->getUrlManager($LCID);
					// Keep the sitemap for this job for future usage.
					$timeInterval = $sitemap['timeInterval'];
					$uri = $urlManager->getByPathInfo('/');
					$sitemap['url'] = $uri->setPath($sitemapGenerator->getIndexPath())->normalize()->toString();
				}
				$siteMaps[] = $sitemap;
			}
			$website->setSitemaps($siteMaps);
			$website->save();

			$baseDir = $storageManager->buildChangeURI('files', '/Rbs/Seo/' . $websiteId . '/' . $LCID . '/')->normalize()->toString();

			$this->moveFiles($tmpBaseDir, $baseDir, $fileNames);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		if ($timeInterval)
		{
			// Reschedule the job with time interval.
			$event->reported((new \DateTime())->add(new \DateInterval($timeInterval)));
		}
	}

	/**
	 * @param string $sourceDir
	 * @param string $targetDir
	 * @param string[] $fileNames
	 */
	protected function moveFiles($sourceDir, $targetDir, $fileNames)
	{
		if (is_dir($targetDir))
		{
			foreach (scandir($targetDir) as $fileName)
			{
				if ($fileName === '.' || $fileName === '..')
				{
					continue;
				}
				$filePath = $targetDir . $fileName;
				if (is_file($filePath) && substr($fileName, -4) === '.xml')
				{
					@unlink($filePath);
				}
			}
		}
		elseif ($fileNames)
		{
			\Change\Stdlib\FileUtils::mkdir($targetDir);
		}

		if (!$fileNames)
		{
			return;
		}

		foreach ($fileNames as $fileName)
		{
			$sourceFilePath = $sourceDir . $fileName;
			$targetFilePath = $targetDir . $fileName;
			copy($sourceFilePath, $targetFilePath);
			unlink($sourceFilePath);
		}
	}
}