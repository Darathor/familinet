<?php
/**
 * Copyright (C) 2016 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Job;

/**
 * @name \Rbs\Seo\Job\SaveDeletedPathRules
 */
class SaveDeletedPathRules
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$rules = $job->getArgument('rules');
		if (!$rules || !is_array($rules))
		{
			$event->failed('Invalid rules argument: ' . $rules);
			return;
		}

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$seoManager = $genericServices->getSeoManager();

		$transactionManager = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$transactionManager->begin();

			foreach ($rules as $rule)
			{
				$seoManager->saveDeletedPathRule($rule);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}