<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Profile;

/**
 * @name \Rbs\Seo\Profile\Profile
 */
class Profile extends \Change\User\AbstractProfile
{
	public function __construct()
	{
		$this->properties = [
			'pagingSize' => 10
		];
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Rbs_Seo';
	}

	/**
	 * @return string[]
	 */
	public function getPropertyNames()
	{
		return ['pagingSize'];
	}
}