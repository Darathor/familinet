<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Setup;

/**
 * @name \Rbs\Seo\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	use \Rbs\Ua\Setup\Dependency;

	/**
	 * @var array
	 */
	protected $configuration404;

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	public function onSetupServices(\Change\Events\Event $event)
	{
		$this->configuration404 = $event->getApplication()->getConfiguration('Rbs/Seo/404LogImport');
		parent::onSetupServices($event);
		(new \Rbs\Seo\Std\ModelConfigurationGenerator())->onPluginSetupServices($event);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		// Add a collection for sitemap change frequency.
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Rbs_Seo_Collection_SitemapChangeFrequency') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				$i18n = $applicationServices->getI18nManager();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Sitemap Change Frequency');
				$collection->setCode('Rbs_Seo_Collection_SitemapChangeFrequency');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('always');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_always', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('hourly');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_hourly', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('daily');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_daily', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('weekly');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_weekly', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('monthly');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_monthly', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('yearly');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_yearly', ['ucf']));
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('never');
				$item->getCurrentLocalization()->setTitle($i18n->trans('m.rbs.seo.admin.documentseo_sitemap_change_frequency_never', ['ucf']));
				$collection->getItems()->add($item);

				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}

		if ($cm->getCollection('Rbs_Seo_MetaTypes') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();

				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $applicationServices->getDocumentManager()
					->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');
				$collection->setLabel('Types de métas de page');
				$collection->setCode('Rbs_Seo_MetaTypes');
				$collection->setLocked(true);

				$item = $collection->newCollectionItem();
				$item->setValue('name');
				$item->getCurrentLocalization()->setTitle('name');
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('property');
				$item->getCurrentLocalization()->setTitle('property');
				$collection->getItems()->add($item);

				$item = $collection->newCollectionItem();
				$item->setValue('itemprop');
				$item->getCurrentLocalization()->setTitle('itemprop');
				$collection->getItems()->add($item);

				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}

		$registration = new \Rbs\Ua\Setup\Registration();
		$registration->installApplication($plugin, ['seoManager'], $applicationServices);

		if (isset($this->configuration404['credentialsPath'], $this->configuration404['projectId'], $this->configuration404['forwardedRuleName']))
		{
			$jobManager = $applicationServices->getJobManager();
			$job = null;
			foreach ($jobManager->getJobIdsByName('Rbs_Seo_Import404FromLogs') as $jobId)
			{
				$job = $jobManager->getJob($jobId);
				if ($job->getStatus() === \Change\Job\JobInterface::STATUS_WAITING || $job->getStatus() === \Change\Job\JobInterface::STATUS_RUNNING)
				{
					return;
				}
			}

			if ($job)
			{
				$jobManager->restartJob($job);
				return;
			}

			$jobManager->createNewJob('Rbs_Seo_Import404FromLogs');
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		parent::executeThemeAssets($plugin, $pluginManager, $application);

		$compilation = new \Rbs\Ua\Setup\Compilation();
		$compilation->compileAngularControllersMapping($pluginManager->getModule('Rbs', 'Ua'), $application);
		$compilation->compileAngularControllersMapping($plugin, $application);

		$manager = new \Rbs\Seo\Http\UI\Resources('seoManager');
		$manager->setApplication($application)->setPluginManager($pluginManager);
		$assetManager = $manager->getNewAssetManager();
		$manager->registerAssets($assetManager);
		$manager->write($assetManager);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		parent::executeDbSchema($plugin, $schemaManager);
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$workspace = $application->getWorkspace();
		$configFile = $workspace->appPath('Config', 'seo.pathrulecleaner.json');
		if (!file_exists($configFile))
		{
			$srcPath = __DIR__ . '/Assets/seo.pathrulecleaner.json';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$rootPath = $workspace->appPath('Config');
			\Change\Stdlib\FileUtils::write($rootPath . DIRECTORY_SEPARATOR . basename($srcPath), $content);
		}

		$configuration->addPersistentEntry('Change/Events/Http/UA/UI/Rbs_Seo', \Rbs\Seo\Http\UI\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/Http/UA/API/Rbs_Seo', \Rbs\Seo\Http\API\Listeners::class);
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_Seo', \Rbs\Seo\Setup\Patch\Listeners::class);

		$configuration->addPersistentEntry('Rbs/Seo/404LogImport/credentialsPath', null);
		$configuration->addPersistentEntry('Rbs/Seo/404LogImport/projectId', null);
		$configuration->addPersistentEntry('Rbs/Seo/404LogImport/forwardedRuleName', null);
		$configuration->addPersistentEntry('Rbs/Seo/404LogImport/urlPattern', '#(/|\\.html)$#');
	}
}
