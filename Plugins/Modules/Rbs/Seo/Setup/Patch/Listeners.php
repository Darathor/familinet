<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Setup\Patch;

/**
 * @name \Rbs\Seo\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		//Rbs_Seo_0001 v1.6 Update seo manager realm.
		//Rbs_Seo_0002 v1.6 Migrate data in ModelConfiguration documents.
		//Rbs_Seo_0003 v1.6 Initialize publication data in documents.
		//Rbs_Seo_0004 v1.6 Migrate data from old DocumentSeo to the new publicationData property in publishable documents.
		//Rbs_Seo_0005 v1.6 Purge Seo documents.

		//Rbs_Seo_0006 v1.8 Delete corrections on model configurations.

		//Rbs_Seo_0010 v2.1 Remove old model "Rbs_Event_Category" from the document table "Rbs_Seo_ModelConfiguration".
	}
}