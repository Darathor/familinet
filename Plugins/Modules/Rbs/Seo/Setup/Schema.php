<?php
namespace Rbs\Seo\Setup;

/**
 * @name \Rbs\Seo\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			$this->tables['rbs_seo_dat_404'] = $td = $schemaManager->newTableDefinition('rbs_seo_dat_404');
			$td->addField($schemaManager->newIntegerFieldDefinition('rule_id')->setNullable(false)->setAutoNumber(true))
				->addField($schemaManager->newIntegerFieldDefinition('website_id')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newVarCharFieldDefinition('lcid', ['length' => 5])->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('hash', ['length' => 40])->setNullable(false))
				->addField($schemaManager->newTextFieldDefinition('relative_path')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('document_id')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newLobFieldDefinition('rule_data')->setNullable(true))
				->addField($schemaManager->newBooleanFieldDefinition('hidden')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newDateFieldDefinition('insertion_date')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('hits')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newDateFieldDefinition('last_hit_date')->setNullable(true))
				->addKey($this->newPrimaryKey()->addField($td->getField('rule_id')))
				->setOption('AUTONUMBER', 1);
		}
		return $this->tables;
	}
}
