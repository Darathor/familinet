<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Seo\Std;

/**
 * @name \Rbs\Seo\Std\PublicationMetas
 */
class PublicationMetas implements \Change\Presentation\Interfaces\PublicationMetas
{
	/**
	 * @var integer[]
	 */
	protected $groupIds = [];

	/**
	 * @var string
	 */
	protected $mode;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var \Rbs\Seo\Std\PublicationMeta[]
	 */
	protected $metas = [];

	/**
	 * @param array $data
	 */
	public function __construct(array $data = [])
	{
		$this->setMode($data['mode'] ?? null);
		if (isset($data['title']))
		{
			$this->setTitle($data['title']);
		}
		if (isset($data['groupIds']) && is_array($data['groupIds']))
		{
			$this->setGroupIds($data['groupIds']);
		}
		if (isset($data['metas']) && is_array($data['metas']))
		{
			$this->setMetas($data['metas']);
		}
	}

	/**
	 * @param string $mode 'manual', 'auto' or 'none'
	 * @return $this
	 */
	public function setMode($mode)
	{
		$this->mode = (string)($mode ?: 'auto');
		return $this;
	}

	/**
	 * @return string 'manual', 'auto' or 'none'
	 */
	public function getMode()
	{
		return $this->mode;
	}

	/**
	 * @param string|null $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title === null ? null : (string)$title;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param integer[] $groupIds
	 * @return $this
	 */
	public function setGroupIds(array $groupIds)
	{
		$this->groupIds = [];
		foreach ($groupIds as $groupId)
		{
			$groupId = (int)$groupId;
			if ($groupId)
			{
				$this->groupIds[] = $groupId;
			}
		}
		return $this;
	}

	/**
	 * @return \integer[]
	 */
	public function getGroupIds()
	{
		return $this->groupIds;
	}

	/**
	 * @param array $metasData
	 * @return $this
	 */
	public function setMetas(array $metasData)
	{
		$this->metas = [];
		foreach ($metasData as $metaData)
		{
			if ($metaData instanceof \Rbs\Seo\Std\PublicationMeta)
			{
				$this->metas[] = $metaData;
			}
			else if (is_array($metaData))
			{
				$this->metas[] = new \Rbs\Seo\Std\PublicationMeta($metaData);
			}
		}
		return $this;
	}

	/**
	 * @return \Rbs\Seo\Std\PublicationMeta[]
	 */
	public function getMetas()
	{
		return $this->metas;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$array = [
			'groupIds' => $this->groupIds,
			'mode' => $this->mode,
			'title' => $this->title,
			'metas' => []
		];
		foreach ($this->metas as $meta)
		{
			$array['metas'][] = $meta->toArray();
		}
		return $array;
	}
}