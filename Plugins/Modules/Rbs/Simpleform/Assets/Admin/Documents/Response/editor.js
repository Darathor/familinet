(function() {
	"use strict";
	angular.module('RbsChange').controller('RbsSimpleformFormResponsesController', [
		'$scope', '$routeParams',
		function ($scope, $routeParams) {
			if ($routeParams.id) {
				$scope.filter = {
					filters : [
						{ name: 'form', parameters : { operator: 'eq', propertyName: 'form', value: parseInt($routeParams.id, 10) }}
					],
					name: 'group', parameters: { operator: 'AND'}
				}
			}
		}
	]);


	function rbsDocumentEditorRbsSimpleformResponseEdit(REST) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope, elm, attrs, editorCtrl) {
				scope.onLoad = function() {
				};
			}
		};
	}

	rbsDocumentEditorRbsSimpleformResponseEdit.$inject = ['RbsChange.REST'];
	angular.module('RbsChange').directive('rbsDocumentEditorRbsSimpleformResponseEdit', rbsDocumentEditorRbsSimpleformResponseEdit);
})();