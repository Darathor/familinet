<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\DateTime
 */
class DateTime extends \Rbs\Simpleform\Converter\AbstractConverter
{
	/**
	 * @param array $value
	 * @return string|\Rbs\Simpleform\Converter\Validation\Error
	 */
	public function parseFromUI($value)
	{
		if (!is_array($value))
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_array', ['ucf']);
			return new Validation\Error([$message]);
		}

		if (!isset($value['date']) || trim($value['date']) === '')
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.must_select_date_and_time', ['ucf']);
			return new Validation\Error([$message]);
		}
		if (!isset($value['time']) || trim($value['time']) === '')
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.must_select_date_and_time', ['ucf']);
			return new Validation\Error([$message]);
		}

		$time = trim($value['time']);
		if (\strlen($time) === 5)
		{
			$time .= ':00';
		}
		$validator = new \Zend\Validator\Date(['format' => 'Y-m-d H:i:s']);
		$dateString = \trim($value['date']) . ' ' . $time;
		$errorMessages = $this->getErrorMessages($validator, $dateString);

		return count($errorMessages) ? new Validation\Error($errorMessages) : $dateString;
	}

	/**
	 * @param array $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value)
	{
		return (!isset($value['date']) || trim($value['date']) === '') && (!isset($value['time']) || trim($value['time']) === '');
	}

	/**
	 * @param array $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return $this->getI18nManager()->transDateTime(new \DateTime($value));
	}
}