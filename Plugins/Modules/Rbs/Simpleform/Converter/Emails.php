<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\Emails
 */
class Emails extends \Rbs\Simpleform\Converter\AbstractConverter
{
	/**
	 * @param string $value
	 * @return string[]|\Rbs\Simpleform\Converter\Validation\Error
	 */
	public function parseFromUI($value)
	{
		$emails = array_map('trim', explode(',', $value));
		$validator = new \Zend\Validator\EmailAddress();
		$validator->setTranslatorTextDomain('m.rbs.simpleform.constraints');
		$errorMessages = [];
		foreach ($emails as $email)
		{
			if (!$validator->isValid($email))
			{
				$errorMessages = array_merge($errorMessages, array_values($validator->getMessages()));
			}
		}
		return count($errorMessages) ? new Validation\Error($errorMessages) : $emails;
	}

	/**
	 * @param string $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value)
	{
		return trim($value) === '';
	}

	/**
	 * @param array $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return implode(', ', $value);
	}
}