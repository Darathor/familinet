<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\File
 */
class File extends \Rbs\Simpleform\Converter\AbstractConverter
{
	/**
	 * @var \Change\Storage\StorageManager
	 */
	protected $storageManager;

	public function __construct($i18nManager, $parameters, $storageManager)
	{
		parent::__construct($i18nManager, $parameters);
		$this->storageManager = ($storageManager instanceof \Change\Storage\StorageManager) ? $storageManager : null;
	}

	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return $this
	 */
	public function setStorageManager(\Change\Storage\StorageManager $storageManager = null)
	{
		$this->storageManager = $storageManager;
		return $this;
	}

	/**
	 * @param string $value
	 * @return array|\Rbs\Simpleform\Converter\Validation\Error
	 */
	public function parseFromUI($value)
	{
		$file = new \Rbs\Simpleform\Converter\File\TmpFile($value);
		if ($file->getError() !== 0)
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_file', ['ucf']);
			return new Validation\Error([$message]);
		}
		elseif (!$this->storageManager)
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_storage_manager', ['ucf']);
			return new Validation\Error([$message]);
		}

		try
		{
			$changeURI = $this->storeFile($this->getParameters()->get('folder', 'form'), $file->getPath(), $file->getName());
		}
		catch (\Exception $e)
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_file_storage', ['ucf']);
			return new Validation\Error([$message]);
		}

		return ['name' => $file->getName(), 'changeURI' => $changeURI];
	}

	/**
	 * @param string $folder
	 * @param string $tmpPath
	 * @param string $fileName
	 * @throws \RuntimeException
	 * @return string
	 */
	protected function storeFile($folder, $tmpPath, $fileName)
	{
		$storageName = 'Rbs_Simpleform';
		$storageEngine = $this->storageManager->getStorageByName($storageName);
		$resourceParts = [$folder, uniqid('', false) . '_' . trim($fileName)];
		$storagePath = $storageEngine->normalizePath(implode('/', $resourceParts));
		$destinationPath = \Change\Storage\StorageManager::DEFAULT_SCHEME . '://' . $storageName . '/' . $storagePath;

		if (move_uploaded_file($tmpPath, $destinationPath))
		{
			if ( $this->storageManager->getItemInfo($destinationPath) === null)
			{
				throw new \RuntimeException('Unable to find: ' . $destinationPath, 999999);
			}
		}
		else
		{
			throw new \RuntimeException('Unable to move "' . $tmpPath . '" in "' . $destinationPath . '"', 999999);
		}
		return $destinationPath;
	}

	/**
	 * @param string $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value)
	{
		return !(is_array($value) && isset($value['size']) && $value['size'] > 0);
	}

	/**
	 * @param array $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return is_array($value) && isset($value['name']) ? $value['name'] : '';
	}
}