<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\Numeric
 */
class Numeric extends \Rbs\Simpleform\Converter\Trim
{
	/**
	 * @param string $value
	 * @return float|\Rbs\Simpleform\Converter\Validation\Error
	 */
	protected function doParseFromUI($value)
	{
		if (is_numeric($value))
		{
			$parsed = (float)$value;
		}
		elseif (!$this->i18nManager)
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_float', ['ucf']);
			return new Validation\Error([$message]);
		}
		else
		{
			$formatter = new \NumberFormatter($this->i18nManager->getLCID(), \NumberFormatter::DEFAULT_STYLE);
			$position = 0;
			$parsed = $formatter->parse($value, \NumberFormatter::TYPE_DOUBLE, $position);
			if ($parsed === false || $position != strlen($value))
			{
				$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_float', ['ucf']);
				return new Validation\Error([$message]);
			}
		}

		$errorMessages = [];
		foreach ($this->getParameters() as $name => $param)
		{
			if ($param === '' || $param === null)
			{
				continue;
			}

			$validator = null;
			switch ($name)
			{
				case 'max':
					$validator = new \Zend\Validator\LessThan(['max' => (float)$param, 'inclusive' => true]);
					break;

				case 'min':
					$validator = new \Zend\Validator\GreaterThan(['min' => (float)$param, 'inclusive' => true]);
					break;
			}
			$errorMessages = array_merge($errorMessages, $this->getErrorMessages($validator, $parsed));
		}

		return count($errorMessages) ? new Validation\Error($errorMessages) : $parsed;
	}

	/**
	 * @param float $value
	 * @return string
	 */
	public function formatValue($value)
	{
		if (!$this->i18nManager)
		{
			return (string)$value;
		}
		$formatter = new \NumberFormatter($this->i18nManager->getLCID(), \NumberFormatter::DEFAULT_STYLE);
		return $formatter->format($value);
	}
}