<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\Trim
 */
class Trim extends \Rbs\Simpleform\Converter\AbstractConverter
{
	/**
	 * @param string $value
	 * @return string|\Rbs\Simpleform\Converter\Validation\Error
	 */
	public function parseFromUI($value)
	{
		if (!is_string($value))
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_string', array('ucf'));
			return new Validation\Error(array($message));
		}
		return $this->doParseFromUI(trim($value));
	}

	/**
	 * @param string $value
	 * @return string|\Rbs\Simpleform\Converter\Validation\Error
	 */
	protected function doParseFromUI($value)
	{
		return $value;
	}

	/**
	 * @param string $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value)
	{
		return trim((string)$value) === '';
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return $value;
	}
}