<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Converter;

/**
 * @name \Rbs\Simpleform\Converter\TrimArray
 */
class TrimArray extends \Rbs\Simpleform\Converter\AbstractConverter
{
	/**
	 * @param array $value
	 * @return string|\Rbs\Simpleform\Converter\Validation\Error
	 */
	public function parseFromUI($value)
	{
		if (!is_array($value))
		{
			$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_array', ['ucf']);
			return new Validation\Error([$message]);
		}
		$parsed = [];
		foreach ($value as $singleValue)
		{
			if (!is_string($singleValue))
			{
				$message = $this->getI18nManager()->trans('m.rbs.simpleform.front.invalid_array', ['ucf']);
				return new Validation\Error([$message]);
			}
			$singleValue = trim($singleValue);
			if ($singleValue !== '')
			{
				$parsed[] = $this->doParseValueFromUI(trim($singleValue));
			}
		}

		$errorMessages = [];
		foreach ($this->getParameters() as $name => $param)
		{
			if ($param === '' || $param === null)
			{
				continue;
			}

			$validator = null;
			switch ($name)
			{
				case 'maxCount':
					$max = intval($param);
					if (count($parsed) > $max)
					{
						$errorMessages[] = $this->getI18nManager()->trans('m.rbs.simpleform.front.not_more_than_values',
							['ucf'], ['max' => $max]);
					}
					break;

				case 'minCount':
					$min = intval($param);
					if (count($parsed) < $min)
					{
						$errorMessages[] = $this->getI18nManager()->trans('m.rbs.simpleform.front.not_less_than_values',
							['ucf'], ['min' => $min]);
					}
					break;
			}
		}

		return count($errorMessages) ? new Validation\Error($errorMessages) : $parsed;
	}

	/**
	 * @param string $value
	 * @return string|\Rbs\Simpleform\Converter\Validation\Error
	 */
	protected function doParseValueFromUI($value)
	{
		return $value;
	}

	/**
	 * @param array $value
	 * @return boolean
	 */
	public function isEmptyFromUI($value)
	{
		if (is_array($value))
		{
			foreach ($value as $singleValue)
			{
				if (trim(strval($singleValue)) !== '')
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function formatValue($value)
	{
		return implode(', ', $value);
	}
}