<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Documents;

/**
 * @name \Rbs\Simpleform\Documents\Form
 */
class Form extends \Compilation\Rbs\Simpleform\Documents\Form
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DELETED, [$this, 'onDefaultDeleted'], 5);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'form' . $this->getId();
	}

	/**
	 * @return \Rbs\Simpleform\Field\FieldInterface[]
	 */
	public function getValidFields()
	{
		$fields = [];
		foreach ($this->getFields() as $field)
		{
			/* @var $field \Rbs\Simpleform\Documents\FormField */
			if (!$field->getCurrentLocalization()->isNew())
			{
				$fields[] = $field;
			}
		}
		return $fields;
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onCreate()
	{
		$this->fillFieldNames();
	}

	/**
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	protected function onUpdate()
	{
		$this->fillFieldNames();
	}

	/**
	 * Fills empty field names.
	 */
	protected function fillFieldNames()
	{
		foreach ($this->getFields() as $field)
		{
			if (\Change\Stdlib\StringUtils::isEmpty($field->getName()))
			{
				$field->setName(uniqid('field', false));
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() === $this)
		{
			$restResult = $event->getParam('restResult');
			if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
			{
				$q = $event->getApplicationServices()->getDocumentManager()->getNewQuery('Rbs_Simpleform_Response');
				$q->andPredicates($q->eq('form', $this));
				$restResult->setProperty('responseCount', $q->getCountDocuments());

				$exportData = $this->getMeta('export');
				if (is_array($exportData) && count($exportData))
				{
					$restResult->setProperty('export', $exportData);
				}
				else
				{
					$restResult->setProperty('export', null);
				}

				$genData = $this->getMeta('jobGen');
				if (is_array($genData) && count($genData))
				{
					$restResult->setProperty('jobGen', $genData);
				}
				else
				{
					$restResult->setProperty('jobGen', ['jobId' => null]);
				}
				$um = $restResult->getUrlManager();
				$selfLinks = $restResult->getRelLink('self');
				$selfLink = array_shift($selfLinks);
				if ($selfLink instanceof \Change\Http\Rest\V1\Link)
				{
					$this->addLinkOnResult($restResult, $um, $selfLink->getPathInfo());
				}
			}
		}
		return parent::onDefaultUpdateRestResult($event);
	}

	/**
	 * @param \Change\Http\Rest\V1\Resources\DocumentResult $documentResult
	 * @param \Change\Http\UrlManager $urlManager
	 * @param string $baseUrl
	 */
	protected function addLinkOnResult($documentResult, $urlManager, $baseUrl)
	{
		$documentResult->addLink(new \Change\Http\Rest\V1\Link($urlManager, $baseUrl . '/Export', 'export'));
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws \RuntimeException
	 * @throws \Exception
	 */
	public function onDefaultDeleted(\Change\Documents\Events\Event $event)
	{
		/** @var \Rbs\Simpleform\Documents\Form $document */
		$document = $event->getDocument();
		$exportData = $document->getMeta('export');

		if (is_array($exportData) && count($exportData) && $exportData['fileURI'])
		{
			$arguments = ['storageURI' => $exportData['fileURI']];
			$job = $event->getApplicationServices()->getJobManager()
				->createNewJob('Change_Storage_URICleanUp', $arguments);
			if ($job->getId() !== 0)
			{
				$document->setMeta('export', null);
				$document->saveMetas();
			}
		}
	}
}
