<?php

/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Rbs\Simpleform\Http\Rest;

class AnswersExport
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \LogicException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$result = new \Change\Http\Rest\V1\ArrayResult();
		$formId = $event->getParam('formId');
		$applicationServices = $event->getApplicationServices();
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();
		$dm = $applicationServices->getDocumentManager();
		$tm = $applicationServices->getTransactionManager();
		try
		{
			$tm->begin();
			$form = $dm->getDocumentInstance($formId);
			if ($form instanceof \Rbs\SimpleForm\Documents\Form)
			{
				$arguments = ['formId' => $formId, 'callerId' => $userId];
				$job = $event->getApplicationServices()->getJobManager()
					->createNewJob('Rbs_Simpleform_AnswersExport', $arguments);
				$form->setMeta('export', ['jobId' => $job->getId()]);
				$form->saveMetas();
				$result->setArray(['jobId' => $job->getId()]);
				$event->setResult($result);
			}
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}
	}
}