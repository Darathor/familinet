<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Http\Web;

use Change\Http\Web\Event;
use Zend\Http\Response as HttpResponse;

/**
 * @name \Rbs\Simpleform\Http\Web\Image
 */
class Image extends \Change\Http\Web\Actions\AbstractAjaxAction
{
	/**
	 * @param Event $event
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function execute(Event $event)
	{
		$request = $event->getRequest();
		$arguments = $request->getQuery()->toArray();
		if (!isset($arguments['i']))
		{
			throw new \RuntimeException('No image', 999999);
		}
		$key = 'captcha_' . str_replace('.', '_', substr($arguments['i'], 1));
		$content = $event->getApplicationServices()->getCacheManager()->getEntry('temporaryDownload', $key, ['ttl' => 1200]);
		if ($content)
		{
			$result = new \Change\Http\Web\Result\RawResult(HttpResponse::STATUS_CODE_200);
			$result->setContent($content);
			$result->getHeaders()->addHeaderLine('Content-Type', 'image/png');
			$result->getHeaders()->addHeaderLine('Content-Length', strlen($content));
			$event->setResult($result);
			$event->getController()->getEventManager()->attach(\Change\Http\Event::EVENT_RESPONSE, [$this, 'onResultContent'], 10);
		}
		else
		{
			$event->getController()->getEventManager()->attach(\Change\Http\Event::EVENT_RESULT,
				function (\Change\Http\Web\Event $event)
				{
					if ($event->getResult() instanceof \Change\Http\Web\Result\AjaxResult)
					{
						$event->setResult($event->getController()->notFound($event));
					}
				},
				0
			);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function onResultContent($event)
	{
		$resource = $event->getResult();
		if ($resource instanceof \Change\Http\Web\Result\RawResult)
		{
			$response = new \Zend\Http\PhpEnvironment\Response();
			$response->setStatusCode(HttpResponse::STATUS_CODE_200);
			$response->setContent($resource->getContent());
			$response->getHeaders()->clearHeaders();
			$response->getHeaders()->addHeaders($resource->getHeaders());
			return $response;
		}
		return null;
	}
}