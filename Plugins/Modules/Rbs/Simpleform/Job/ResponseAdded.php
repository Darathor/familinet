<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Job;

/**
* @name \Rbs\Simpleform\Job\ResponseAdded
*/
class ResponseAdded
{
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationServices = $event->getApplicationServices();
		$documentManager = $applicationServices->getDocumentManager();
		$transactionManager = $applicationServices->getTransactionManager();
		$profileManager = $applicationServices->getProfileManager();
		$i18nManager = $applicationServices->getI18nManager();
		$responseId = $job->getArgument('responseId');
		$response = $documentManager->getDocumentInstance($responseId);
		if ($response instanceof \Rbs\Simpleform\Documents\Response && $response->getForm())
		{
			$event->setParam('response', $response);
			try
			{
				$transactionManager->begin();
				$this->addUserNotification($response, $documentManager, $profileManager, $i18nManager);
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$event->getApplication()->getLogging()->exception($e);
				$transactionManager->rollBack($e);
			}
		}
	}

	/**
	 * @param \Rbs\Simpleform\Documents\Response $response
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\User\ProfileManager $profileManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 */
	public function addUserNotification(
		\Rbs\Simpleform\Documents\Response $response,
		\Change\Documents\DocumentManager $documentManager,
		\Change\User\ProfileManager $profileManager,
		\Change\I18n\I18nManager $i18nManager)
	{
		$form = $response->getForm();

		$users = $form->getReceiverUsers();

		$groups = $form->getReceiverGroups();
		if ($groups->count())
		{
			$allUsers = $users;
			foreach ($groups as $group)
			{
				$query = $documentManager->getNewQuery('Rbs_User_User');
				$query->andPredicates($query->eq('groups', $group));
				foreach ($query->getDocuments() as $user)
				{
					$allUsers[] = $user;
				}
			}

			$users = [];
			foreach ($allUsers as $user)
			{
				$users[$user->getId()] = $user;
			}

			$allUsers = null;
			$users = array_values($users);
		}

		$params = [
			'RESPONSE_LABEL' => $response->getLabel(),
			'FORM_LABEL' => $form->getLabel(),
			'targetId' => $response->getId()
		];


		/* @var $user \Rbs\User\Documents\User */
		foreach ($users as $user)
		{
			$authenticatedUser = new \Rbs\User\Events\AuthenticatedUser($user);

			$userProfile = $profileManager->loadProfile($authenticatedUser, 'Change_User');
			$LCID = $userProfile->getPropertyValue('LCID');
			if (!$LCID)
			{
				$LCID = $documentManager->getLCID();
			}

			try
			{
				$documentManager->pushLCID($LCID);
				$notification = $documentManager->getNewDocumentInstanceByModelName('Rbs_Notification_Notification');

				/* @var $notification \Rbs\Notification\Documents\Notification */
				$notification->setUserId($user->getId());
				$notification->setCode('Rbs_Simpleform_Response');
				$notification->getCurrentLocalization()->setMessage($i18nManager->transForLCID($LCID,
					'm.rbs.simpleform.admin.response_notification_message', ['ucf'], $params));
				$notification->setParams($params);
				$notification->save();

				$documentManager->popLCID();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
			}
		}
	}
}