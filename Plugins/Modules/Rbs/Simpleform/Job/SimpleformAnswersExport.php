<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Simpleform\Job;

/**
 * @name \Rbs\Simpleform\Job\SimpleformAnswersExport
 */
class SimpleformAnswersExport
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \InvalidArgumentException
	 * @throws \LogicException
	 */
	public function execute(\Change\Job\Event $event)
	{
		$job = $event->getJob();
		$applicationService = $event->getApplicationServices();
		$dm = $applicationService->getDocumentManager();
		$applicationService->getStorageManager();
		$tm = $applicationService->getTransactionManager();
		$formId = $job->getArgument('formId');
		$applicationService->getLogging()->info('Generate export formId : ' . $formId);
		$exportDate = (new \DateTime())->format(\DateTime::ATOM);
		$chunk = 100;
		$currentUUID = 0;
		/** @var \Rbs\Simpleform\Documents\Form $form */
		$form = $dm->getDocumentInstance($formId, 'Rbs_Simpleform_Form');

		if (!$form)
		{
			$applicationService->getLogging()->error('Cannot found form with id : ' . $formId);
			return;
		}

		$fields = $form->getFields();
		$header = [];
		$fieldsName = [];
		foreach ($fields as $field)
		{
			/* @var $field \Rbs\Simpleform\Documents\FormField */
			$header[] = $field->getLabel();
			$fieldsName[] = $field->getName();
		}

		$formName = $form->getName();
		$csvStorageURI = 'change://Rbs_Simpleform/' . $formName . '/export/export_' . $formId . '_' . $exportDate . '.csv';
		$csvFileHandle = fopen($csvStorageURI, 'w+');

		if (!fputcsv($csvFileHandle, $header, ';', '"', '\\'))
		{
			$applicationService->getLogging()->error('cannot create header');
			$event->setResultArgument('error', ' File Generation Failure Append data');
			$event->success();
			return;
		}

		do
		{
			$q = $dm->getNewQuery('Rbs_Simpleform_Response');
			$q->andPredicates([
				$q->eq('form', $formId),
				$q->gt('id', $currentUUID)
			]);
			$q->addOrder('id');

			$answers = $q->getDocuments(0, $chunk);
			$count = count($answers);
			foreach ($answers as $answer)
			{
				/** @var \Rbs\Simpleform\Documents\Response $answer */
				$fieldsInfos = $answer->getFieldsInfos();
				$currentUUID = $answer->getId();
				$csvData = [];
				foreach ($fieldsName as $field)
				{
					/** @var string $field */
					if (is_array($fieldsInfos[$field]))
					{
						if (isset($fieldsInfos[$field]['formattedValue']))
						{
							$csvData[] = $fieldsInfos[$field]['formattedValue'];
						}
						else
						{
							$csvData[] = '';
						}
					}
					else
					{
						$csvData[] = '';
					}
				}

				if (!fputcsv($csvFileHandle, $csvData, ';', '"', '\\'))
				{
					$applicationService->getLogging()->error('cannot insert line');
					$event->setResultArgument('error', ' File Generation Failure Append data');
					$event->success();
					return;
				}
			}
		}
		while ($count === $chunk);
		fclose($csvFileHandle);
		try
		{
			$tm->begin();
			$metaArray = $form->getMeta('export');
			if (!$metaArray)
			{
				$metaArray = [];
			}
			$metaArray['exportDate'] = $exportDate;
			$metaArray['fileURI'] = $csvStorageURI;
			$form->setMeta('export', $metaArray);
			$form->saveMetas();
			$tm->commit();
		}
		catch (\Exception $e)
		{
			$tm->rollBack($e);
		}
		$event->setResultArgument('csvFile', $csvStorageURI);
		$event->success();
	}
}