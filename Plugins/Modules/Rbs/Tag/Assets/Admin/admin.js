(function() {
	'use strict';

	var app = angular.module('RbsChange');

	//-------------------------------------------------------------------------
	//
	// TagService
	//
	//-------------------------------------------------------------------------

	app.service('RbsChange.TagService', ['$rootScope', 'RbsChange.REST', 'RbsChange.Utils', '$q', '$http',
		function($rootScope, REST, Utils, $q, $http) {
			function getIdArray(data) {
				var ids = [];
				angular.forEach(data, function(item) {
					ids.push(item.id);
				});
				return ids;
			}

			return {
				getList: function(deferred) {
					var tags = [];
					var tagQuery = {
						model: 'Rbs_Tag_Tag',
						where: {
							and: [
								{
									or: [
										{
											op: 'eq',
											lexp: { property: 'authorId' },
											rexp: { value: $rootScope.user.id }
										},
										{
											op: 'eq',
											lexp: { property: 'userTag' },
											rexp: { value: 0 }
										}
									]
								},
								{
									or: [
										{
											op: 'eq',
											lexp: { property: 'module' },
											rexp: { value: $rootScope.rbsCurrentPluginName }
										},
										{
											op: 'isNull',
											exp: { property: 'module' }
										}
									]
								}
							]
						},
						order: [
							{
								property: 'userTag',
								order: 'desc'
							},
							{
								property: 'label',
								order: 'asc'
							}
						],
						offset: 0
					};

					REST.query(tagQuery, { column: ['color', 'userTag'], limit: 1000 }).then(function(result) {
						angular.forEach(result.resources, function(r) {
							tags.push(r);
						});
						if (deferred) {
							deferred.resolve(tags);
						}
					});
					return tags;
				},

				create: function(tag) {
					tag.model = 'Rbs_Tag_Tag';
					tag.id = REST.getTemporaryId();
					var promise = REST.save(tag);
					promise.then(function(created) {
						angular.extend(tag, created);
						delete tag.unsaved;
					});
					return promise;
				},

				setDocumentTags: function(doc, tags) {
					var q = $q.defer();
					$http.post(doc.getTagsUrl(), { ids: getIdArray(tags) }, REST.getHttpConfig()).then(
						function() {
							q.resolve(doc);
						},
						function errorCallback(result) {
							result.data.httpStatus = result.status;
							q.reject(result.data);
						}
					);
					return q.promise;
				}
			};
		}
	]);

	app.controller('Rbs_Tag_Tag_MyTagsController', ['$rootScope', '$scope', function($rootScope, $scope) {
		$scope.filter = {
			name: 'group', parameters: { operator: 'AND' },
			filters: [
				{ name: 'userTag', parameters: { propertyName: 'userTag', operator: 'eq', value: true } },
				{ name: 'authorId', parameters: { propertyName: 'authorId', operator: 'eq', value: $rootScope.user.id } }
			]
		};
	}]);

	app.controller('Rbs_Tag_Tag_TagsController', ['$rootScope', '$scope', function($rootScope, $scope) {
		$scope.filter = {
			name: 'group', parameters: { operator: 'AND' },
			filters: [
				{ name: 'userTag', parameters: { propertyName: 'userTag', operator: 'eq', value: false } }
			]
		};
	}]);
})();