<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Documents;

/**
 * @name \Rbs\Tag\Documents\Tag
 */
class Tag extends \Compilation\Rbs\Tag\Documents\Tag
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATED, function ($event) { $this->setSearchTag($event); });
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATED, function ($event) { $this->updateSearchTag($event); });
	}

	protected function onUpdate()
	{
		if ($this->isPropertyModified('module'))
		{
			if ($this->getModule() == '')
			{
				$this->setModule(null);
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws
	 */
	protected function setSearchTag($event)
	{
		/* @var $tag \Rbs\Tag\Documents\Tag */
		$tag = $event->getDocument();
		$appServices = $event->getApplicationServices();

		$transactionManager = $appServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$stmt = $appServices->getDbProvider()->getNewStatementBuilder();
			$fb = $stmt->getFragmentBuilder();

			$stmt->insert($fb->table('rbs_tag_search'), 'tag_id', 'search_tag_id');
			$stmt->addValues($fb->integerParameter('tagId'), $fb->integerParameter('searchTagId'));
			$iq = $stmt->insertQuery();

			foreach ($tag->getSearchTagIds() as $searchTagId)
			{
				$iq->bindParameter('tagId', $searchTagId);
				$iq->bindParameter('searchTagId', $tag->getId());
				$iq->execute();
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws
	 */
	protected function updateSearchTag($event)
	{
		/* @var $tag \Rbs\Tag\Documents\Tag */
		$tag = $event->getDocument();

		// TODO Check if "children" property has been modified.

		$appServices = $event->getApplicationServices();
		$transactionManager = $appServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$stmt = $appServices->getDbProvider()->getNewStatementBuilder();
			$fb = $stmt->getFragmentBuilder();

			$stmt->delete($fb->table('rbs_tag_search'));
			$stmt->where($fb->eq($fb->column('search_tag_id'), $fb->integerParameter('searchTagId')));
			$dq = $stmt->deleteQuery();
			$dq->bindParameter('searchTagId', $tag->getId());
			$dq->execute();

			$stmt->insert($fb->table('rbs_tag_search'), 'tag_id', 'search_tag_id');
			$stmt->addValues($fb->integerParameter('tagId'), $fb->integerParameter('searchTagId'));
			$iq = $stmt->insertQuery();

			foreach ($tag->getSearchTagIds() as $searchTagId)
			{
				$iq->bindParameter('tagId', $searchTagId);
				$iq->bindParameter('searchTagId', $tag->getId());
				$iq->execute();
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @return array
	 */
	protected function getSearchTagIds()
	{
		$ids = [$this->getId()];
		foreach ($this->getChildren() as $subTag)
		{
			$ids = array_merge($ids, $subTag->getSearchTagIds());
		}
		return $ids;
	}

}
