<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Http\Rest\Actions;

/**
 * @name \Rbs\Tag\Http\Rest\Actions\GetTaggedDocuments
 */
class GetTaggedDocuments
{
	/**
	 * Use Event Params: documentId, modelName, LCID
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute($event)
	{
		$this->generateResult($event);
	}

	/**
	 * @param \Change\Http\Rest\V1\CollectionResult $result
	 * @return array
	 */
	protected function buildQueryArray($result)
	{
		$array = ['limit' => $result->getLimit(), 'offset' => $result->getOffset()];
		if ($result->getSort())
		{
			$array['sort'] = $result->getSort();
			$array['desc'] = $result->getDesc() ? 'true' : 'false';
		}
		return $array;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \Change\Http\Rest\V1\CollectionResult
	 */
	protected function generateResult($event)
	{
		$urlManager = $event->getUrlManager();
		$result = new \Change\Http\Rest\V1\CollectionResult();
		if (($offset = $event->getRequest()->getQuery('offset')) !== null)
		{
			$result->setOffset((int)$offset);
		}
		if (($limit = $event->getRequest()->getQuery('limit')) !== null)
		{
			$result->setLimit((int)$limit);
		}
		if (($sort = $event->getRequest()->getQuery('sort')) !== null)
		{
			$result->setSort($sort);
		}

		if (($desc = $event->getRequest()->getQuery('desc')) !== null)
		{
			$result->setDesc($desc);
		}

		$selfLink = new \Change\Http\Rest\V1\Link($urlManager, $event->getRequest()->getPath());
		$selfLink->setQuery($this->buildQueryArray($result));
		$result->addLink($selfLink);

		$requestedModelName = $event->getParam('requestedModelName');

		$tagId = $event->getParam('tagId');

		$qb = $event->getApplicationServices()->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select(
			$fb->alias($fb->getDocumentColumn('id'), 'id'),
			$fb->alias($fb->func('count', $fb->getDocumentColumn('id')), 'count')
		);
		$qb->from($fb->getDocumentIndexTable());
		$qb->innerJoin($fb->alias($fb->table('rbs_tag_document'), 'tag'), $fb->eq($fb->column('doc_id', 'tag'), 'id'));

		if ($requestedModelName)
		{
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('tag_id', 'tag'), $fb->integerParameter('tagId')),
				$fb->eq($fb->getDocumentColumn('model'), $fb->parameter('model'))
			));
		}
		else
		{
			$qb->where($fb->eq($fb->column('tag_id', 'tag'), $fb->integerParameter('tagId')));
		}

		$sc = $qb->query();
		$sc->bindParameter('tagId', $tagId);
		if ($requestedModelName)
		{
			$sc->bindParameter('model', $requestedModelName);
		}

		$row = $sc->getFirstResult();
		if ($row && $row['count'])
		{
			$result->setCount((int)$row['count']);
		}

		if ($result->getOffset())
		{
			$prevLink = clone $selfLink;
			$prevOffset = max(0, $result->getOffset() - $result->getLimit());
			$query = $this->buildQueryArray($result);
			$query['offset'] = $prevOffset;
			$prevLink->setQuery($query);
			$prevLink->setRel('prev');
			$result->addLink($prevLink);
		}

		if ($result->getCount())
		{
			if ($result->getCount() > $result->getOffset() + $result->getLimit())
			{
				$nextLink = clone $selfLink;
				$nextOffset = min($result->getOffset() + $result->getLimit(), $result->getCount() - 1);
				$query = $this->buildQueryArray($result);
				$query['offset'] = $nextOffset;
				$nextLink->setQuery($query);
				$nextLink->setRel('next');
				$result->addLink($nextLink);
			}

			$qb = $event->getApplicationServices()->getDbProvider()->getNewQueryBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->select(
				$fb->alias($fb->getDocumentColumn('id'), 'id'),
				$fb->alias($fb->getDocumentColumn('model'), 'model')
			);
			$qb->from($fb->getDocumentIndexTable());
			$qb->innerJoin($fb->alias($fb->table('rbs_tag_document'), 'tag'), $fb->eq($fb->column('doc_id', 'tag'), 'id'));
			if ($requestedModelName)
			{
				$qb->where($fb->logicAnd(
					$fb->eq($fb->column('tag_id', 'tag'), $fb->integerParameter('tagId')),
					$fb->eq($fb->getDocumentColumn('model'), $fb->parameter('model'))
				));
			}
			else
			{
				$qb->where($fb->eq($fb->column('tag_id', 'tag'), $fb->integerParameter('tagId')));
			}
			$sc = $qb->query();
			$sc->bindParameter('tagId', $tagId);
			if ($requestedModelName)
			{
				$sc->bindParameter('model', $requestedModelName);
			}
			$sc->setMaxResults($result->getLimit());
			$sc->setStartIndex($result->getOffset());
			$collection = new \Change\Documents\DocumentCollection($event->getApplicationServices()->getDocumentManager(), $sc->getResults());
			foreach ($collection as $document)
			{
				if ($document)
				{
					$result->addResource(new \Change\Http\Rest\V1\Resources\DocumentLink($urlManager, $document,
						\Change\Http\Rest\V1\Resources\DocumentLink::MODE_PROPERTY));
				}
			}
		}

		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
		return $result;
	}
}
