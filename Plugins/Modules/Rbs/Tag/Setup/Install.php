<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Setup;

/**
 * @name \Rbs\Tag\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$this->createDefaultTags($applicationServices);
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws
	 */
	private function createDefaultTags($applicationServices)
	{
		$tagModel = $applicationServices->getModelManager()->getModelByName('Rbs_Tag_Tag');
		$documentManager = $applicationServices->getDocumentManager();

		$query = $applicationServices->getDocumentManager()->getNewQuery($tagModel);
		if ($query->getCountDocuments())
		{
			return;
		}

		//$lcid = $applicationServices->getI18nManager()->getLCID();
		$i18nManager = $applicationServices->getI18nManager();

		//Default tags
		$tags = [
			['code' => 'Tag_Large_Picture', 'label' => $i18nManager->trans('m.rbs.tag.setup.setup_large_picture', ['ucf']),
				'color' => 'gray', 'module' => 'Rbs_Media'],
			['code' => 'Tag_Medium_Picture', 'label' => $i18nManager->trans('m.rbs.tag.setup.setup_medium_picture', ['ucf']),
				'color' => 'gray', 'module' => 'Rbs_Media'],
			['code' => 'Tag_To_Translate', 'label' => $i18nManager->trans('m.rbs.tag.setup.setup_to_translate', ['ucf']),
				'color' => 'red', 'module' => null]
		];

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			foreach ($tags as $defaultTag)
			{
				/* @var $tag \Rbs\Tag\Documents\Tag */
				$tag = $documentManager->getNewDocumentInstanceByModel($tagModel);
				$tag->setLabel($defaultTag['label']);
				$tag->setColor($defaultTag['color']);
				$tag->setModule($defaultTag['module']);
				$tag->create();
				$applicationServices->getDocumentCodeManager()->addDocumentCode($tag, $defaultTag['code'], 'Rbs_Setup');
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}
}
