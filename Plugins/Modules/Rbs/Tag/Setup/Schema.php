<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Tag\Setup;

/**
 * @name \Rbs\Tag\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();
			// Create table tag <-> doc
			$this->tables['rbs_tag_document'] = $td = $schemaManager->newTableDefinition('rbs_tag_document');
			$td->addField($tagIdField = $schemaManager->newIntegerFieldDefinition('tag_id')->setNullable(false)->setDefaultValue('0'));
			$td->addField($docIdField = $schemaManager->newIntegerFieldDefinition('doc_id')->setNullable(false)->setDefaultValue('0'));
			$td->addKey($this->newPrimaryKey()->addField($tagIdField)->addField($docIdField));

			// Create table tag_search
			$this->tables['rbs_tag_search'] = $td = $schemaManager->newTableDefinition('rbs_tag_search');
			$td->addField($schemaManager->newIntegerFieldDefinition('tag_id')->setNullable(false)->setDefaultValue('0'));
			$td->addField($schemaManager->newIntegerFieldDefinition('search_tag_id')->setNullable(false)->setDefaultValue('0'));
		}
		return $this->tables;
	}
}
