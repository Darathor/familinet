(function(jQuery) {
	'use strict';

	var app = angular.module('RbsChange');

	app.directive('rbsDocumentEditorRbsThemeTemplateEdit', ['$timeout', 'RbsChange.Blocks', 'proximisModalStack', rbsDocumentEditorRbsThemeTemplateEdit]);

	function rbsDocumentEditorRbsThemeTemplateEdit($timeout, Blocks, proximisModalStack) {
		return {
			restrict: 'A',
			require: '^rbsDocumentEditorBase',

			link: function(scope) {
				scope.select = { websiteId: null };
				scope.blockList = [];

				scope.$on('Navigation.saveContext', function(event, args) {
					var data = {
						select: scope.select,
						blockList: scope.blockList,
						editedBlockIndex: editedBlockIndex
					};
					scope.closeSettingsModal();
					args.context.savedData('templateEditor', data);
				});

				scope.onRestoreContext = function(currentContext) {
					var data = currentContext.savedData('templateEditor');
					if (data) {
						scope.select = data.select;
						scope.blockList = data.blockList;
						if (data.editedBlockIndex) {
							// Wait for the next digest to open the modal to make sure that all is fully initialized.
							$timeout(function () { scope.openSettingsModal(data.editedBlockIndex); });
						}
					}
				};

				scope.onLoad = function() {
					if (!angular.isObject(scope.document.editableContent) || angular.isArray(scope.document.editableContent)) {
						scope.document.editableContent = {}
					}
					if (!angular.isObject(scope.document.contentByWebsite) || angular.isArray(scope.document.contentByWebsite)) {
						scope.document.contentByWebsite = {}
					}
				};

				scope.onReady = function() {
					if (scope.blockList.length == 0) {
						scope.buildBlockList();
					}

					var contentByWebsite = {};
					var replace = false;
					angular.forEach(scope.document.contentByWebsite, function(data, id) {
						if (angular.isObject(data) && !angular.isArray(data)) {
							contentByWebsite[id] = data;
						}
						else {
							replace = true;
						}
					});
					if (replace) {
						scope.document.contentByWebsite = contentByWebsite;
					}
				};

				scope.onReload = function() {
					scope.select = { websiteId: null };
					scope.buildBlockList();
				};

				scope.buildBlockList = function() {
					var editableContent = scope.document.editableContent,
						contentByWebsite = scope.document.contentByWebsite,
						blockList = [], webBlocks = {}, byWebsite, row;

					byWebsite = scope.select.websiteId != null;

					if (byWebsite && contentByWebsite.hasOwnProperty(scope.select.websiteId)) {
						webBlocks = contentByWebsite[scope.select.websiteId];
					}

					angular.forEach(editableContent, function(value, key) {
						if (value.type == 'block') {
							row = { id: value.id, name: value.name, override: false, block: { name: '' } };
							if (webBlocks.hasOwnProperty(key)) {
								row.name = webBlocks[key].name;
								row.override = true
							}
							blockList.push(row);
						}
					});
					scope.blockList = blockList;
				};

				scope.$watch('select.websiteId', function(newValue, oldValue) {
					if (newValue !== oldValue) {
						scope.buildBlockList();
					}
				});

				scope.showBlockSelector = function(row) {
					return !scope.select.websiteId || row.override;
				};

				scope.getBlockById = function(id) {
					var blockList;
					if (scope.select.websiteId) {
						if (scope.document.contentByWebsite.hasOwnProperty(scope.select.websiteId)) {
							blockList = scope.document.contentByWebsite[scope.select.websiteId];
						}
					}
					else {
						blockList = scope.document.editableContent;
					}
					if (blockList && blockList.hasOwnProperty(id)) {
						return blockList[id];
					}
					return null;
				};

				var editedBlockIndex;
				var currentSettingsModal;

				scope.canParametrizeBlock = function(row) {
					return row.block && row.block.template && scope.getBlockById(row.id) != null;
				};

				scope.openSettingsModal = function(index) {
					var row = scope.blockList[index];
					var item = scope.getBlockById(row.id);

					if (item.name !== row.name) {
						item.name = row.name;
						item.parameters = {};
					}

					editedBlockIndex = index;
					currentSettingsModal = proximisModalStack.open({
						templateUrl: 'Rbs/Admin/js/directives/structure-editor-settings-block.twig',
						size: 'lg',
						controller: 'RbsStructureEditorSettingsController',
						resolve: {
							modalData: function() {
								return {
									item: item,
									blockMethods: null,
									profile: scope.document.mailSuitable ? 'Mail' : 'Website',
									substitutionVariables: {},
									defaultDisplayColumnsFrom: null
								};
							}
						}
					});

					function closeFunction() {
						currentSettingsModal = null;
						editedBlockIndex = null;
					}

					currentSettingsModal.result.then(closeFunction, closeFunction);
				};

				scope.closeSettingsModal = function() {
					if (currentSettingsModal) {
						currentSettingsModal.close();
						return true;
					}
					return false;
				};

				scope.canEmptyBlock = function(row) {
					return row.name != '' && scope.getBlockById(row.id) != null;
				};

				scope.emptyBlock = function(row) {
					var block = scope.getBlockById(row.id);
					block.name = '';
					block.parameters = {};
					row.name = '';
					row.block = { name: '' };
				};

				scope.canOverrideBlock = function(row) {
					if (scope.select.websiteId) {
						return !row.override;
					}
					return false;
				};

				scope.addBlockOverride = function(row) {
					var contentByWebsite = scope.document.contentByWebsite;
					if (!contentByWebsite.hasOwnProperty(scope.select.websiteId)) {
						contentByWebsite[scope.select.websiteId] = {};
					}
					var block = {};
					angular.copy(scope.document.editableContent[row.id], block);
					contentByWebsite[scope.select.websiteId][row.id] = block;
					row.override = true;
				};

				scope.canRemoveOverrideBlock = function(row) {
					if (scope.select.websiteId) {
						return row.override;
					}
					return false;
				};

				scope.removeBlockOverride = function(row) {
					delete scope.document.contentByWebsite[scope.select.websiteId][row.id];
					row.name = scope.document.editableContent[row.id].name;
					row.block = Blocks.getBlock(scope.row.name);
					row.override = false;
				};

				scope.$watch('document.mailSuitable', function(value) {
					scope.existingBlocks = Blocks.getList(value);
				});
			}
		};
	}

	app.directive('rbsThemeBlockSelector', ['RbsChange.Blocks', function(Blocks) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Theme/Documents/Template/block-selector.twig',

			link: function(scope) {
				scope.$watch('row.name', function() {
					if (scope.row.name && scope.existingBlocks.length > 0 && (!scope.row.block || scope.row.block.name !== scope.row.name)) {
						scope.row.block = Blocks.getBlock(scope.row.name);

						var block = scope.getBlockById(scope.row.id);
						if (block) {
							block.name = scope.row.name;
						}
					}
				});
			}
		};
	}]);
})(window.jQuery);