<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Theme\Documents;

use Change\Documents\Events\Event;
use Change\Presentation\Layout\Layout;
use Zend\Json\Json;

/**
 * @name \Rbs\Theme\Documents\Template
 */
class Template extends \Compilation\Rbs\Theme\Documents\Template implements \Change\Presentation\Interfaces\Template
{
	const FILE_META_KEY = 'fileMeta';

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(Event::EVENT_CREATE, [$this, 'onNormalizeEditableContent'], 5);
		$eventManager->attach(Event::EVENT_UPDATE, [$this, 'onNormalizeEditableContent'], 5);
	}

	/**
	 * @param Event $event
	 */
	public function onNormalizeEditableContent(Event $event)
	{
		if ($event->getDocument() !== $this)
		{
			return;
		}

		$modifiedPropertyNames = $this->getModifiedPropertyNames();
		if (in_array('editableContent', $modifiedPropertyNames))
		{
			$blockManager = $event->getApplicationServices()->getBlockManager();
			$contentLayout = new Layout($this->getEditableContent());
			$blocks = $contentLayout->getBlocks();
			$blockManager->normalizeBlocksParameters($blocks);
			$this->setEditableContent($contentLayout->toArray());

			$editableContent = $this->getEditableContent();
			$contentByWebsite = $this->getContentByWebsite();
			if (is_array($contentByWebsite))
			{
				foreach ($contentByWebsite as $websiteId => &$editableContentForWebsite)
				{
					foreach ($editableContentForWebsite as $name => &$data)
					{
						$data['display'] = $editableContent[$name]['display'] ?? null;
						$data['visibility'] =
							$editableContent[$name]['visibility'] ?? null;
					}
				}
			}
			$this->setContentByWebsite($contentByWebsite);
		}

		if (in_array('contentByWebsite', $modifiedPropertyNames))
		{
			$contentByWebsite = $this->getContentByWebsite();
			if (is_array($contentByWebsite))
			{
				$normalizedContentByWebsite = [];
				$blockManager = $event->getApplicationServices()->getBlockManager();
				foreach ($contentByWebsite as $websiteId => $editableContent)
				{
					$contentLayout = new Layout($editableContent);
					$blocks = $contentLayout->getBlocks();
					$blockManager->normalizeBlocksParameters($blocks);
					$editableContent = $contentLayout->toArray();
					if ($editableContent)
					{
						$normalizedContentByWebsite[$websiteId] = $editableContent;
					}
				}
				$this->setContentByWebsite($normalizedContentByWebsite ?: null);
			}
		}
	}

	/**
	 * @param integer $websiteId
	 * @return \Change\Presentation\Layout\Layout
	 */
	public function getContentLayout($websiteId = null)
	{
		$editableContent = $this->getEditableContent();
		if ($this->getApplication()->inDevelopmentMode())
		{
			$fileMetas = $this->getMeta(static::FILE_META_KEY);
			if (isset($fileMetas['json']))
			{
				$filePath = $fileMetas['json'];
				if (file_exists($filePath) && filemtime($filePath) > $this->getModificationDate()->getTimestamp())
				{
					$jsonData = Json::decode(file_get_contents($filePath), Json::TYPE_ARRAY);
					if (isset($jsonData['editableContent']) && is_array($jsonData['editableContent']))
					{
						$editableContent = array_merge($jsonData['editableContent'], $editableContent);
					}
				}
			}
		}
		if ($websiteId)
		{
			$contentByWebsite = $this->getContentByWebsite();
			if (is_array($contentByWebsite) && isset($contentByWebsite[$websiteId]) && is_array($contentByWebsite[$websiteId]))
			{
				$editableContent = array_merge($editableContent, $contentByWebsite[$websiteId]);
			}
		}
		return new Layout($editableContent, $this->getDefaultDisplayColumnsFrom());
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		if ($this->getApplication()->inDevelopmentMode())
		{
			$fileMetas = $this->getMeta(static::FILE_META_KEY);
			if (isset($fileMetas['html']))
			{
				$filePath = $fileMetas['html'];
				if (file_exists($filePath) && filemtime($filePath) > $this->getModificationDate()->getTimestamp())
				{
					$this->application->getLogging()->warn('Please update template ' . $this->getName());
					return file_get_contents($filePath);
				}
			}
		}
		return $this->getHtmlData();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->getId();
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentLink)
		{
			$documentLink = $restResult;
			/* @var $pageTemplate \Rbs\Theme\Documents\Template */
			$pageTemplate = $documentLink->getDocument();
			$theme = $pageTemplate->getTheme();
			if ($theme)
			{
				$documentLink->setProperty('label', $theme->getLabel() . ' > ' . $pageTemplate->getLabel());
				$documentLink->setProperty('themeId', $theme->getId());
			}
			$documentLink->setProperty('mailSuitable', $pageTemplate->getMailSuitable());
			$documentLink->setProperty('defaultDisplayColumnsFrom', $pageTemplate->getDefaultDisplayColumnsFrom());
			$documentLink->setProperty('categoryName', $pageTemplate->getMailSuitable() ? 'MailTemplates' : 'PageTemplates');
			$restResult->removeRelAction('delete');
		}
		elseif ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			/* @var $pageTemplate \Rbs\Theme\Documents\Template */
			$pageTemplate = $restResult->getDocument();
			$theme = $pageTemplate->getTheme();
			if ($theme)
			{
				$restResult->setProperty('themeId', $theme->getId());
			}
			$restResult->setProperty('categoryName', $pageTemplate->getMailSuitable() ? 'MailTemplates' : 'PageTemplates');
			$restResult->removeRelAction('delete');
		}
	}

	/**
	 * @return bool
	 */
	public function isMailSuitable()
	{
		return $this->getMailSuitable();
	}
}