<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Theme\Events;

/**
 * @name \Rbs\Theme\Events\ThemeResolver
 */
class ThemeResolver
{
	/**
	 * @param \Change\Events\Event $event
	 * @return \Rbs\Theme\Documents\Theme|null
	 */
	public function resolve($event)
	{
		$themeName = $event->getParam('themeName');
		if (!$themeName || $event->getParam('theme') !== null)
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$cacheOptions = ['ttl' => 3600];
		$cacheManager = $applicationServices->getCacheManager();
		$documentManager = $applicationServices->getDocumentManager();
		$item = $cacheManager->getEntry('WebsiteResolver', 'ThemesData', $cacheOptions);
		if ($item === null)
		{
			$item = [];

			$query = $documentManager->getNewQuery('Rbs_Theme_Theme');
			$themes = $query->getDocuments()->preLoad()->toArray();

			/** @var \Rbs\Theme\Documents\Theme $theme */
			foreach ($themes as $theme)
			{
				$item[$theme->getName()] = $theme->getId();
			}
			$cacheManager->setEntry('WebsiteResolver', 'ThemesData', $item,  $cacheOptions);
		}
		if (isset($item[$themeName]))
		{
			$event->setParam('theme', $documentManager->getDocumentInstance($item[$themeName]));
		}
	}
}