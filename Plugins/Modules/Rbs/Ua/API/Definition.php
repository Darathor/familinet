<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\Definition
 */
class Definition
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var \Rbs\Ua\API\Operation[]
	 */
	protected $operations = [];

	/**
	 * Definition constructor.
	 * @param string $name
	 * @param string $path
	 * @param \Rbs\Ua\API\Operation[] $operations
	 */
	public function __construct($name, $path, array $operations)
	{
		$this->name = $name;
		$this->path = $path;
		$this->operations = $operations;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @return Operation[]
	 */
	public function getOperations()
	{
		return $this->operations;
	}
}