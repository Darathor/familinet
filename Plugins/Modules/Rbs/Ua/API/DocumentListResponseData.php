<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\DocumentListResponseGenerator
 */
class DocumentListResponseData extends \Rbs\Ua\API\ListResponseData
{
	/**
	 * @var int|null
	 */
	protected $count;

	/**
	 * @var string
	 */
	protected $modelName;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var array
	 */
	protected $filters;

	/**
	 * @var array|null
	 */
	protected $ids;

	/**
	 * @param string $modelName
	 * @param int $offset
	 * @param int $limit
	 * @param string|null $sortOn
	 * @param bool $sortDesc
	 */
	public function __construct($modelName, $offset = 0, $limit = 10, $sortOn = null, $sortDesc = true)
	{
		parent::__construct($offset, $limit, $sortOn, $sortDesc);
		$this->modelName = $modelName;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return array
	 */
	protected function getFilters()
	{
		return $this->filters;
	}

	/**
	 * @param array $filters
	 * @return $this
	 */
	public function setFilters($filters)
	{
		$this->filters = $filters;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getIds()
	{
		return $this->ids;
	}

	/**
	 * @param array|null $ids
	 * @return $this
	 */
	public function setIds($ids)
	{
		$this->ids = $ids;
		if (is_array($ids))
		{
			$this->count = count($ids);
			$this->offset = 0;
			$this->limit = max($this->limit, $this->count);
		}
		return $this;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return mixed
	 */
	protected function buildItem($document)
	{
		return $document;
	}

	/**
	 * @return \Generator
	 */
	public function getItems()
	{
		if (is_array($this->ids))
		{
			foreach ($this->ids as $id)
			{
				$document = $this->getDocumentManager()->getDocumentInstance($id);
				$item = $this->buildItem($document);
				if ($item)
				{
					yield $item;
				}
			}
		}
		elseif ($this->getCount())
		{
			$query = $this->getDocumentManager()->getNewQuery($this->modelName);
			$this->completeQuery($query);
			if ($this->sortOn)
			{
				$this->applySort($query, $this->sortOn, $this->sortDesc);
			}
			$query->addOrder('id', !$this->sortDesc);

			/** @var \Change\Documents\AbstractDocument $document */
			foreach ($query->getDocuments($this->offset, $this->limit)->preLoad()->toArray() as $document)
			{
				$item = $this->buildItem($document);
				if ($item)
				{
					yield $item;
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\Query\Query $query
	 */
	protected function completeQuery(\Change\Documents\Query\Query $query)
	{
		if ($this->filters)
		{
			$this->getDocumentManager()->getModelManager()->applyDocumentFilter($query, $this->filters);
		}
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		if ($this->count === null)
		{
			$query = $this->getDocumentManager()->getNewQuery($this->modelName);
			$this->completeQuery($query);
			$this->count = $query->getCountDocuments();

			if ($this->offset > $this->count)
			{
				$this->offset = 0;
			}
		}
		return $this->count;
	}

	/**
	 * @param \Change\Documents\Query\Query $query
	 * @param string $sortOn
	 * @param boolean $sortDesc
	 */
	protected function applySort($query, $sortOn, $sortDesc)
	{
		if (strpos($sortOn, '.') > 0)
		{
			[$property, $sortOn] = explode('.', $sortOn);
			$subQuery = $query->getPropertyBuilder($property);
			if ($subQuery->getModel()->getProperty($sortOn))
			{
				$query->addOrder($subQuery->getColumn($sortOn), !$sortDesc);
			}
			else
			{
				throw new \RuntimeException('Property ' . $sortOn . ' does not exist on ' . $subQuery->getModel()->getName());
			}
		}
		else
		{
			if ($query->getModel()->getProperty($sortOn))
			{
				$query->addOrder($sortOn, !$sortDesc);
			}
			else
			{
				throw new \RuntimeException('Property ' . $sortOn . ' does not exist on ' . $query->getModel()->getName());
			}
		}
	}

	/**
	 * @param array $filters
	 * @return array
	 */
	public function buildAndFilters(array $filters)
	{
		return [
			'name' => 'group',
			'parameters' => ['operator' => 'AND'],
			'filters' => $filters
		];
	}

	/**
	 * @param array $filters
	 * @return array
	 */
	public function buildOrFilters(array $filters)
	{
		return [
			'name' => 'group',
			'parameters' => ['operator' => 'OR'],
			'filters' => $filters
		];
	}
}