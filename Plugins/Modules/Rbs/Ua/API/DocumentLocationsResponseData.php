<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\DocumentLocationsResponseGenerator
 */
class DocumentLocationsResponseData
{
	/**
	 * @var \Change\Documents\AbstractDocument
	 */
	protected $document;

	/**
	 * @var \Change\Http\Web\PathRuleManager
	 */
	protected $pathRuleManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 */
	public function __construct(\Change\Documents\AbstractDocument $document)
	{
		$this->document = $document;
	}

	/**
	 * @return \Change\Http\Web\PathRuleManager
	 */
	protected function getPathRuleManager()
	{
		return $this->pathRuleManager;
	}

	/**
	 * @param \Change\Http\Web\PathRuleManager $pathRuleManager
	 * @return $this
	 */
	public function setPathRuleManager($pathRuleManager)
	{
		$this->pathRuleManager = $pathRuleManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	public function __invoke()
	{
		$document = $this->document;
		if (!($document instanceof \Change\Documents\Interfaces\Publishable))
		{
			return;
		}

		$documentManager = $this->getDocumentManager();
		$i18nManager = $this->getI18nManager();
		foreach ($document->getCachedPathRules() as $key => $pathRules)
		{
			$websiteId = (int)substr($key, 0, -5);
			$LCID = substr($key, -5);

			/** @var $website \Rbs\Website\Documents\Website */
			$website = $documentManager->getDocumentInstance($websiteId);
			if (!$website instanceof \Rbs\Website\Documents\Website)
			{
				continue;
			}

			try
			{
				$documentManager->pushLCID($LCID);

				$location = [
					'websiteId' => $websiteId,
					'websiteLabel' => $website->getLabel(),
					'LCID' => $LCID,
					'LCIDLabel' => $i18nManager->transLCID($LCID, ['ucf']),
					'pathRules' => []
				];

				$urlManager = $website->getUrlManager($LCID);
				foreach ($pathRules as $sectionId => $relativePath)
				{
					/** @var \Rbs\Website\Documents\Section|null $section */
					$section = $documentManager->getDocumentInstance($sectionId);
					$location['pathRules'][] = $this->getRuleData($relativePath, $document, $website, $section, $urlManager);
				}

				yield $location;

				$documentManager->popLCID();
			}
			catch (\Exception $e)
			{
				$documentManager->popLCID($e);
			}
		}
	}

	/**
	 * @param string $relativePath
	 * @param \Change\Documents\Interfaces\Publishable|\Change\Documents\AbstractDocument $document
	 * @param \Rbs\Website\Documents\Website $website
	 * @param \Rbs\Website\Documents\Section|null $section
	 * @param \Change\Http\Web\UrlManager $urlManager
	 * @return array
	 */
	protected function getRuleData(string $relativePath, \Change\Documents\Interfaces\Publishable $document,
		\Rbs\Website\Documents\Website $website, \Rbs\Website\Documents\Section $section = null, \Change\Http\Web\UrlManager $urlManager)
	{
		$canonical = !$section;
		$sectionId = $canonical ? 0 : $section->getId();
		/** @var \Rbs\Website\Documents\Section $section */
		$section = $canonical ? $document->getCanonicalSection($website) : $section;
		$LCID = $urlManager->getLCID();
		$ruleData = [
			'canonical' => $canonical,
			'relativePath' => $relativePath,
			'url' => $urlManager->getByPathInfo($relativePath),
			'sectionId' => $sectionId,
			'sectionLabel' => $canonical ? null : $section->getLabel()
		];

		if (!$canonical)
		{
			$sectionPath = [];
			foreach ($section->getSectionPath() as $sectionInPath)
			{
				if ($sectionInPath instanceof \Rbs\Website\Documents\Topic)
				{
					$sectionPath[] = $sectionInPath->getLabel();
				}
			}
			$ruleData['sectionLabel'] = $section->getLabel();
			$ruleData['sectionPath'] = $sectionPath;
		}

		$i18nManager = $this->getI18nManager();

		$published = true;
		if ($document->published())
		{
			$ruleData['publication'][] = [
				'ok' => true,
				'message' => $i18nManager->trans('m.rbs.admin.admin.document_published_in_lang')
			];
		}
		else
		{
			$published = false;
			$ruleData['publication'][] = [
				'ok' => false,
				'message' => $i18nManager->trans('m.rbs.admin.admin.document_not_published_in_lang')
			];
		}

		if ($section->published())
		{
			$ruleData['publication'][] = [
				'ok' => true,
				'message' => $i18nManager->trans('m.rbs.admin.admin.section_published_in_lang')
			];
		}
		else
		{
			$published = false;
			$ruleData['publication'][] = [
				'ok' => false,
				'message' => $i18nManager->trans('m.rbs.admin.admin.section_not_published_in_lang')
			];
		}

		if (!($document instanceof \Change\Presentation\Interfaces\Page))
		{
			/* @var $document \Change\Documents\AbstractDocument */
			$pathRule = $this->getPathRuleManager()->getNewRule($website->getId(), $LCID, 'NULL', $document->getId(), 200, $sectionId);
			$params = ['website' => $website, 'pathRule' => $pathRule];
			$documentEvent = new \Change\Documents\Events\Event(\Change\Documents\Events\Event::EVENT_DISPLAY_PAGE, $document, $params);
			$document->getEventManager()->triggerEvent($documentEvent);
			$page = $documentEvent->getParam('page');
			if ($page instanceof \Change\Presentation\Interfaces\Page)
			{
				if (!($page instanceof \Change\Documents\Interfaces\Publishable) || $page->published())
				{
					$ruleData['publication'][] = [
						'ok' => true,
						'message' => $i18nManager->trans('m.rbs.admin.admin.detail_function_provided_in_lang')
					];
				}
				else
				{
					$published = false;
					$ruleData['publication'][] = [
						'ok' => false,
						'message' => $i18nManager->trans('m.rbs.admin.admin.detail_function_not_published_in_lang')
					];
				}
			}
			else
			{
				$published = false;
				$ruleData['publication'][] = [
					'ok' => false,
					'message' => $i18nManager->trans('m.rbs.admin.admin.detail_function_not_provided_in_lang')
				];
			}
		}

		$ruleData['published'] = $published;
		return $ruleData;
	}
}