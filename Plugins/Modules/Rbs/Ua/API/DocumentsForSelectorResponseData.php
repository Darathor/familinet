<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\DocumentsForSelectorResponseGenerator
 */
class DocumentsForSelectorResponseData extends \Rbs\Ua\API\DocumentListResponseData
{
	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Http\Web\UrlManager|null $websiteUrlManager
	 */
	protected $websiteUrlManager;

	/**
	 * @var callable|null
	 */
	protected $onGetRestrictionAttached;

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\Http\Web\UrlManager|null
	 */
	protected function getWebsiteUrlManager()
	{
		return $this->websiteUrlManager;
	}

	/**
	 * @param \Change\Http\Web\UrlManager|null $websiteUrlManager
	 * @return $this
	 */
	public function setWebsiteUrlManager($websiteUrlManager)
	{
		$this->websiteUrlManager = $websiteUrlManager;
		return $this;
	}

	/**
	 * @param \Change\Documents\Query\Query $query
	 */
	protected function completeQuery(\Change\Documents\Query\Query $query)
	{
		if (!$this->onGetRestrictionAttached)
		{
			$this->onGetRestrictionAttached = $this->getDocumentManager()->getModelManager()->getEventManager()->attach('getRestriction',
				function ($event) { $this->onGetRestriction($event); });
		}
		parent::completeQuery($query);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetRestriction(\Change\Events\Event $event)
	{
		$filter = $event->getParam('filter');
		if (($filter['name'] ?? null) !== 'search' || ($filter['value'] ?? null) === null)
		{
			return;
		}

		/** @var \Change\Documents\Query\Query $query */
		$query = $event->getParam('documentQuery');
		$property = $query->getModel()->getProperty('label');
		if ($property && !$property->getStateless())
		{
			$query->andPredicates($query->like('label', $filter['value']));
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array|null
	 */
	protected function buildItem($document)
	{
		$documentModel = $document->getDocumentModel();
		$labelProperty = $documentModel->getProperty('label');
		if ($labelProperty)
		{
			$data = [
				'_meta' => new \Rbs\Ua\API\DocumentMetaResponseData($document),
				'label' => $labelProperty->getValue($document),
				'modelLabel' => $this->i18nManager->trans($document->getDocumentModel()->getLabelKey())
			];

			if ($document instanceof \Change\Documents\Interfaces\Publishable && $this->websiteUrlManager && $document->published())
			{
				$data['publishedUrl'] = $this->websiteUrlManager->getByDocument($document, null)->normalize()->toString();
			}

			if ($document instanceof \Rbs\Media\Documents\Image)
			{
				$data['visualUrl'] = $document->getPublicURL(57, 32);
			}
			else
			{
				$property = $documentModel->getProperty('visual');
				if (!$property)
				{
					$property = $documentModel->getProperty('listVisual');
				}
				if ($property)
				{
					$image = $property->getValue($document);
					if ($image instanceof \Rbs\Media\Documents\Image)
					{
						$data['visualUrl'] = $image->getPublicURL(57, 32);
					}
				}
			}

			return $data;
		}
		return null;
	}

	/**
	 * @param $searchString
	 * @return array
	 */
	public function buildSearchStringFilter($searchString)
	{
		return ['name' => 'search', 'value' => $searchString];
	}
}