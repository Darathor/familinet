<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\ListResponseGenerator
 */
abstract class ListResponseData
{
	/**
	 * @var int
	 */
	protected $offset;

	/**
	 * @var int
	 */
	protected $limit;

	/**
	 * @var string
	 */
	protected $sortOn;

	/**
	 * @var bool
	 */
	protected $sortDesc;

	/**
	 * @param int $offset
	 * @param int $limit
	 * @param string|null $sortOn
	 * @param bool $sortDesc
	 */
	public function __construct($offset = 0, $limit = 10, $sortOn = null, $sortDesc = true)
	{
		$this->offset = $offset;
		$this->limit = $limit;
		$this->sortOn = $sortOn;
		$this->sortDesc = $sortDesc;
	}

	/**
	 * @return \Generator
	 */
	abstract public function getItems();

	/**
	 * @return int
	 */
	abstract public function getCount();

	/**
	 * @return array
	 */
	public function getPagination()
	{
		return ['count' => $this->getCount(), 'offset' => $this->offset, 'limit' => $this->limit];
	}
}