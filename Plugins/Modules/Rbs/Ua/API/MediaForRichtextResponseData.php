<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\MediaForRichtextResponseGenerator
 */
class MediaForRichtextResponseData extends \Rbs\Ua\API\DocumentListResponseData
{
	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array|null
	 */
	protected function buildItem($document)
	{
		if ($document instanceof \Rbs\Media\Documents\Image)
		{
			return [
				'id' => $document->getId(),
				'model' => $document->getDocumentModelName(),
				'label' => $document->getLabel(),
				'src' => $document->getPublicURL(),
				'title' => $document->getCurrentLocalization()->getTitle(),
				'alt' => $document->getCurrentLocalization()->getAlt(),
				'width' => $document->getWidth(),
				'height' => $document->getHeight()
			];
		}
		if ($document instanceof \Rbs\Media\Documents\Video)
		{
			return [
				'id' => $document->getId(),
				'model' => $document->getDocumentModelName(),
				'label' => $document->getLabel(),
				'src' => $document->getPublicURL(),
				'alt' => $document->getCurrentLocalization()->getAlt()
			];
		}
		if ($document instanceof \Rbs\Media\Documents\File)
		{
			return [
				'id' => $document->getId(),
				'model' => $document->getDocumentModelName(),
				'label' => $document->getLabel(),
				'title' => $document->getCurrentLocalization()->getTitle()
			];
		}
		return null;
	}

	/**
	 * @return \Generator
	 */
	public function getVariables()
	{
		if (is_array($this->ids))
		{
			foreach ($this->ids as $id)
			{
				$document = $this->getDocumentManager()->getDocumentInstance($id);
				$item = $this->buildVariables($document);
				if ($item)
				{
					yield $item;
				}
			}
		}
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return array|null
	 */
	protected function buildVariables($document)
	{
		if ($document instanceof \Rbs\Media\Documents\Image)
		{
			$label = $document->getLabel();
			$localizedImage = $document->getCurrentLocalization();
			$title = $localizedImage->getTitle();
			$alt = $localizedImage->getAlt();
			$variables = [['label' => $label]];
			if ($title && $title !== $label)
			{
				$variables[] = ['label' => $title];
			}
			if ($alt && $alt !== $label && $alt !== $title)
			{
				$variables[] = ['label' => $alt];
			}
			return $variables;
		}
		if ($document instanceof \Rbs\Media\Documents\Video)
		{
			$label = $document->getLabel();
			$alt = $document->getCurrentLocalization()->getAlt();
			$variables = [['label' => $label]];
			if ($alt && $alt !== $label)
			{
				$variables[] = $alt;
			}
			return $variables;
		}
		if ($document instanceof \Rbs\Media\Documents\File)
		{
			$label = $document->getLabel();
			$title = $document->getCurrentLocalization()->getTitle();
			$variables = [['label' => $label]];
			if ($title && $title !== $label)
			{
				$variables[] = ['label' => $title];
			}
			return $variables;
		}
		return null;
	}
}