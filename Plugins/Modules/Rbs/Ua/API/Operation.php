<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\Operation
 */
class Operation
{
	/**
	 * @var string
	 */
	protected $method = 'GET';

	/**
	 * @var string
	 */
	protected $action;

	/**
	 * @var \Rbs\Ua\API\Parameter[]
	 */
	protected $parameters = [];

	/**
	 * @var \Rbs\Ua\API\Response[]
	 */
	protected $responses = [];

	/**
	 * @var string|boolean
	 */
	protected $authorization = false;

	/**
	 * Operation constructor.
	 * @param string $method
	 * @param string $action
	 * @param Parameter[] $parameters
	 * @param \Rbs\Ua\API\Response[] $responses
	 */
	public function __construct($method, $action, array $parameters, array $responses)
	{
		$this->method = $method;
		$this->action = $action;
		$this->parameters = $parameters;
		$this->responses = array_values($responses);
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * @return bool|string
	 */
	public function getAuthorization()
	{
		return $this->authorization;
	}

	/**
	 * @param bool|string $authorization
	 * @return $this
	 */
	public function setAuthorization($authorization)
	{
		$this->authorization = $authorization;
		return $this;
	}

	/**
	 * @return Parameter[]
	 */
	public function getParameters()
	{
		return $this->parameters;
	}

	/**
	 * @param string $requestPath
	 * @param string $definitionPath
	 * @param array $pathParameters
	 * @return boolean
	 */
	public function checkPath($requestPath, $definitionPath, &$pathParameters)
	{
		if (preg_match_all('/{(\w+)}/', $definitionPath, $paramMatches, PREG_PATTERN_ORDER))
		{
			$pathParamNames = array_flip($paramMatches[1]);
			$matches = [];
			foreach ($this->parameters as $parameter)
			{
				if ($parameter->getSrc() == 'path')
				{
					if ($parameter->getPattern())
					{
						$pattern = $parameter->getPattern();
					}
					else
					{
						$pattern = $parameter->getType() === 'Integer' ? '(\d+)' : '([^/]+)';
					}
					$definitionPath = str_replace('{' . $parameter->getName() . '}', $pattern, $definitionPath);
				}
			}

			if (!preg_match('/^' . str_replace('/', '\/', $definitionPath) . '$/', $requestPath, $matches))
			{
				return false;
			}

			foreach ($pathParamNames as $name => $index)
			{
				$parameter = $this->parameters[$name];
				$value =  $matches[$index + 1];
				$pathParameters[$name] =  $parameter->getType() === 'Integer' ? (int)$value : (string)$value;
			}
		}
		elseif ($requestPath !== $definitionPath)
		{
			return false;
		}
		return true;
	}

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		foreach ($this->responses as $response)
		{
			$response->compile($models);
		}
	}

	/**
	 * @param \Rbs\Ua\API\Response[] $responses
	 * @return $this
	 */
	public function setResponses(array $responses)
	{
		$this->responses = $responses;
		return $this;
	}

	/**
	 * @return \Rbs\Ua\API\Response[]
	 */
	public function getResponses()
	{
		return $this->responses;
	}

	/**
	 * @return string[]
	 */
	public function getFieldsNames()
	{
		$fieldsNames = [];
		foreach ($this->responses as $response)
		{
			if ($f = $response->getFields())
			{
				$fieldsNames = array_merge($fieldsNames, $f->getProperties());
			}
		}
		return $fieldsNames ? array_keys($fieldsNames) : [];
	}

	/**
	 * @param string[] $inputFields
	 * @param int $httpStatus
	 * @return null|\Rbs\Ua\Model\ObjectModel
	 */
	public function getResponseModel(array $inputFields = [], $httpStatus = 200)
	{
		foreach ($this->responses as $response)
		{
			if ($response->getHttpStatus() == $httpStatus)
			{
				$model = $response->getModel();
				if ($model)
				{
					if ($inputFields && $fieldsModel = $response->getFields())
					{
						foreach ($inputFields as $name)
						{
							$property = $fieldsModel->getProperty($name);
							if ($property)
							{
								$model->setProperty($name, $property);
							}
						}
					}
					return $model;
				}
			}
		}
		return null;
	}
}