<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\QueryObjectFallback
 */
class QueryObjectFallback implements \Rbs\Ua\Model\FallbackContextInterface
{

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @return mixed
	 */
	public function normalizeObject($object, $objectModel)
	{
		if ($object && is_string($object))
		{
			return json_decode($object, true);
		}
		return $object;
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @param string $propertyName
	 * @param boolean $resolved
	 * @return mixed
	 */
	public function resolveProperty($object, $objectModel, $propertyName, &$resolved)
	{
		$resolved = false;
		return null;
	}
}