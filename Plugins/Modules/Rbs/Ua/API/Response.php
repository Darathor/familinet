<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\API;

/**
 * @name \Rbs\Ua\API\Response
 */
class Response
{
	/**
	 * @var integer
	 */
	protected $httpStatus;

	/**
	 * @var \Rbs\Ua\Model\ObjectModel|null
	 */
	protected $model;

	/**
	 * @var \Rbs\Ua\Model\ObjectModel|null
	 */
	protected $fields;

	/**
	 * @param int $httpStatus
	 */
	public function __construct($httpStatus = 200)
	{
		$this->httpStatus = (int)$httpStatus;
	}

	/**
	 * @return integer
	 */
	public function getHttpStatus()
	{
		return $this->httpStatus;
	}

	/**
	 * @param integer $httpStatus
	 * @return $this
	 */
	public function setHttpStatus($httpStatus)
	{
		$this->httpStatus = $httpStatus;
		return $this;
	}

	/**
	 * @return \Rbs\Ua\Model\ObjectModel|null
	 */
	public function getModel()
	{
		return $this->model;
	}

	/**
	 * @param \Rbs\Ua\Model\ObjectModel $model
	 * @return $this
	 */
	public function setModel(\Rbs\Ua\Model\ObjectModel $model)
	{
		$this->model = $model;
		return $this;
	}

	/**
	 * @return null|\Rbs\Ua\Model\ObjectModel
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @param null|\Rbs\Ua\Model\ObjectModel $fields
	 * @return $this
	 */
	public function setFields(\Rbs\Ua\Model\ObjectModel $fields)
	{
		$this->fields = $fields;
		return $this;
	}

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		if ($this->model)
		{
			$this->model->compile($models);
		}
		if ($this->fields)
		{
			$this->fields->compile($models);
		}
	}
}