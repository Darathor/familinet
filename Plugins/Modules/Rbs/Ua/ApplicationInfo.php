<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua;

/**
 * @name \Rbs\Ua\ApplicationInfo
 */
class ApplicationInfo
{
	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $realm;

	/**
	 * @var string
	 */
	protected $role;

	/**
	 * @param string $name
	 * @param string $realm
	 * @param string $role
	 */
	public function __construct($name, $realm = null, $role = null)
	{
		$this->name = $name;
		$this->realm = $realm ?: $name;
		$this->role = $role ?: $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRealm()
	{
		return $this->realm;
	}

	/**
	 * @param string $realm
	 * @return $this
	 */
	public function setRealm($realm)
	{
		$this->realm = $realm;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRole()
	{
		return $this->role;
	}

	/**
	 * @param string $role
	 * @return $this
	 */
	public function setRole($role)
	{
		$this->role = $role;
		return $this;
	}
}