(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc controller
	 * @id proximis.controller:ProximisProfileController
	 * @name ProximisProfileController
	 *
	 * @description
	 * A controller for the current users profile edition.
	 *
	 * This controller sets the following stuff in the scope:
	 *
	 * | Variable            | Type             | Details                                                                  |
	 * |---------------------|------------------|--------------------------------------------------------------------------|
	 * | settings            | {@type Object}   | The current user settings.                                               |
	 * | predefinedPageSizes | {@type Array}    | The predefined page size.                                                |
	 * | revert              | {@type Function} | Reverts profiles modifications.                                          |
	 * | save                | {@type Function} | Saves profile modifications.                                             |
	 * | isUnchanged         | {@type Function} | Returns true if there is at least one modification, false else.          |
	 */
	app.controller('ProximisProfileController',
		['$scope', 'proximisUser', 'proximisPagination', ProximisProfileController]);

	function ProximisProfileController(scope, proximisUser, proximisPagination) {
		scope.predefinedPageSizes = proximisPagination.predefinedPageSizes;

		scope.revert = initUser;

		scope.saveProgress = {
			running: false,
			success: false,
			error: false
		};

		scope.save = function() {
			scope.saveProgress.running = true;
			scope.saveProgress.success = false;
			scope.saveProgress.error = false;

			proximisUser.saveProfile(scope.user).then(
				function() {
					initUser();
					scope.saveProgress.running = false;
					scope.saveProgress.success = true;
				},
				function(result) {
					console.error('ProximisProfileController.save', result);
					scope.saveProgress.running = success;
					scope.saveProgress.error = true;
				}
			);
		};

		scope.isUnchanged = function() {
			return angular.equals(proximisUser.get(), scope.user);
		};

		proximisUser.ready().then(initUser);

		function initUser() {
			scope.user = angular.copy(proximisUser.get());
			scope.form.$setPristine();
		}
	}
})();