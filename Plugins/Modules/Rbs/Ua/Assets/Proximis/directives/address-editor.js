/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxAddressEditor
	 * @name pxAddressEditor
	 * @restrict A
	 *
	 * @description
	 * Displays the input fields to create/update an address.
	 *
	 * @param {ChangeDocument} address Address Document.
	 * @param {string=} mode The rendering mode ('horizontal'|'vertical', 'horizontal' by default).
	 */
	app.directive('pxAddressEditor', ['proximisRestApiDefinition', 'proximisRestApi', pxAddressEditor]);

	function pxAddressEditor(proximisRestApiDefinition, proximisRestApi) {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				'address': '=',
				'mode': '='
			},
			templateUrl: 'Rbs/Ua/Proximis/directives/address-editor.twig',

			link: function(scope, elm, attrs, ngModel) {
				scope.fieldDefinitions = [];
				scope.fieldValues = {};
				scope.countries = [];
				scope.addressModels = [];

				// Loads countries with one address model assigned to it
				proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Geo_AddressFields.countries', 'GET', { zoneCode: scope.zoneCode })).then(
					function(result) {
						scope.countries = result.data.items;
						if (!scope.fieldDefinitions.length && scope.address.addressModel) {
							scope.fieldDefinitions = angular.copy(scope.address.addressModel.fields);
						}
					}
				);

				// Updates the fields to fill with defined address model fields
				scope.generateFieldsEditor = function(addressModel) {
					scope.fieldDefinitions = angular.copy(addressModel.fields);
					scope.address.addressModel = addressModel;
					if (angular.isArray(scope.fieldDefinitions)) {
						if (!angular.isObject(ngModel.$viewValue)) {
							ngModel.$setViewValue({});
						}

						var field = scope.fieldDefinitions.find(function(element) {
							return element.code === 'countryCode';
						});
						if (field) {
							scope.fieldDefinitions.splice(scope.fieldDefinitions.indexOf(field), 1);
							scope.fieldDefinitions.unshift(field);
						}
					}
				};

				// Loads new address model data if not already loaded
				scope.getAddressModel = function(addressModelId) {
					var params = { addressModelId: addressModelId };
					proximisRestApi.sendData(proximisRestApiDefinition.getData('Rbs_Geo_AddressFields.detail', 'GET', params)).then(
						function(result) {
							// Stores address model to avoid multiple server calls
							scope.addressModels[scope.fieldValues.countryCode] = result.data.item;
							scope.generateFieldsEditor(scope.addressModels[scope.fieldValues.countryCode]);
						},
						function() {
							proximisMessages.error(
								proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'),
								proximisCoreI18n.trans('m.rbs.ua.common.error_while_loading_data | ucf'), true);
							scope.fieldDefinitions = [];
						}
					);
				};

				// Watchers

				// Updates address model when country is modified
				scope.$watch('address.fields.countryCode', function(newValue, oldValue) {
					if (newValue && newValue !== oldValue) {
						var country = scope.countries.find(function(element) {
							return element.code === newValue;
						});

						if (country) {
							scope.address.common.addressFieldsId = country.addressFieldsId;
						}
					}
				});

				// Updates address model when ID is modified
				scope.$watch('address.common.addressFieldsId', function(newValue, oldValue) {
					if (newValue && newValue !== oldValue) {
						scope.getAddressModel(newValue);
					}
				});

				// Rendering

				// Sets collections initial value for the current address
				ngModel.$render = function ngModelRenderFn() {
					if (ngModel.$viewValue) {
						scope.fieldValues = ngModel.$viewValue;
					}
				};
			}
		};
	}
})();
