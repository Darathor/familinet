/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxBarcode
	 * @name pxBarcode
	 *
	 * @description
	 * Generated the src for a barcode representing the text.
	 *
	 * @param {Object} options The barcode rendering options.
	 *    The available options are:
	 *    * `format` - {@type string} default `code128` - one of the barcode formats provided by ({@link http://framework.zend.com/manual/current/en/modules/zend.barcode.objects.html Zend Framework 2}
	 *    * `factor` - {@type number} default `1` - a size multiplier.
	 *    * `barHeight` - {@type number} default `50` - the bars height.
	 *    * `drawText` - {@type boolean} default `true` - set to false to not print the encoded text.
	 *
	 *    And if `drawText` is set to `true`, the additional following ones:
	 *    * `stretchText` - {@type boolean} default `false` - set to true to stretch to the entire barcode width.
	 *    * `font` - {@type string} - a path to a font used to write the text.
	 *    * `fontSize` - {@type string} - the font size.
	 */
	app.directive('pxBarcode', ['proximisRestApi', 'proximisRestApiDefinition', pxBarcode]);

	function pxBarcode(proximisRestApi, proximisRestApiDefinition) {
		return {
			restrict: 'A',
			scope: {
				pxBarcode: '=',
				format: '@',
				factor: '@',
				barHeight: '@',
				drawText: '@',
				stretchText: '@',
				font: '@',
				fontSize: '@'
			},
			link: function(scope, element) {
				scope.$watch('pxBarcode', update);

				function update(barcode) {
					if (barcode) {
						var params = {
							texts: [barcode],
							config: {
								format: scope.format || 'code128',
								factor: scope.factor || 1,
								barHeight: scope.barHeight || 50,
								drawText: scope.drawText !== 'false',
								font: scope.font || null,
								fontSize: scope.fontSize || 0
							}
						};
						var requestData = proximisRestApiDefinition.getData('common.barcodes', 'GET', params);
						proximisRestApi.get(requestData).then(
							function(result) {
								element.attr('src', result.data.items[0]);
							},
							function(result) {
								console.error('pxBarcode', result);
								element.attr('src', '')
							}
						);
					}
					else {
						element.attr('src', '');
					}
				}
			}
		}
	}
})();