/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxBreadcrumb
	 * @name pxBreadcrumb
	 *
	 * @description
	 * A directive showing the breadcrumb.
	 */
	app.directive('pxBreadcrumb', ['$rootScope', '$route', '$location', '$routeParams', '$document', 'proximisCoreI18n',
		'ProximisGlobal', pxBreadcrumb]);

	function pxBreadcrumb($rootScope, $route, $location, $routeParams, $document, proximisCoreI18n, ProximisGlobal) {
		return {
			restrict: 'A',
			scope: false,
			templateUrl: 'Rbs/Ua/Proximis/directives/breadcrumb.twig',
			link: function(scope) {
				scope.breadcrumb = {
					home: null,
					entries: [],
					current: null,
					functions: {}
				};

				if (!angular.isObject(scope.breadcrumbFunctions)) {
					scope.breadcrumbFunctions = {};
				}

				updateItems();
				$rootScope.$on('$routeChangeSuccess', updateItems);
				$rootScope.$on('breadcrumb.refresh', updateItems);

				function updateItems() {
					if (!scope.breadcrumb.home) {
						scope.breadcrumb.home = findRoute('/');
					}
					var currentPath = $location.path(),
						parts = currentPath.split('/'), part,
						partialPath = '/',
						entry;

					if (currentPath !== '/') {
						scope.breadcrumb.current = findRoute(currentPath);
						if (scope.breadcrumb.current) {
							resolveLabel(scope.breadcrumb.current);
							if (angular.isObject(scope.breadcrumb.current.route.options)) {
								angular.forEach(scope.breadcrumb.current.route.options, function(v, k) {
									if (!$routeParams.hasOwnProperty(k)) {
										$routeParams[k] = v;
									}
								})
							}
						}
					}
					else {
						scope.breadcrumb.current = null;
					}

					scope.breadcrumb.entries.length = 0;
					for (var i = 0; i < parts.length; i++) {
						part = parts[i];
						if (part.length) {
							partialPath += part;
							entry = findRoute(partialPath);
							if (entry) {
								if ((!scope.breadcrumb.current || entry.route !== scope.breadcrumb.current.route)) {
									if (!entry.route.redirectTo || entry.label) {
										resolveLabel(entry);
										scope.breadcrumb.entries.push(entry);
									}
								}
							}
							partialPath += '/';
						}
					}

					updatePageTitle();
				}

				function updatePageTitle() {
					var title = ProximisGlobal.applicationShortName;
					angular.forEach(scope.breadcrumb.entries, function(entry) {
						if (entry.label) {
							title += ' / ' + entry.label;
						}
					});
					if (scope.breadcrumb.current && scope.breadcrumb.current.label) {
						title += ' / ' + scope.breadcrumb.current.label;
					}
					$document[0].title = title + ' - ' + ProximisGlobal.applicationPackageName;
				}

				function resolveLabel(entry) {
					var route = entry.route;
					if (angular.isString(route['labelParam'])) {
						if (!entry.params.hasOwnProperty(route['labelParam'])) {
							console.error('pxBreadcrumb.resolveLabel', 'param not found ' + route['labelParam']);
							return;
						}
						entry.label = entry.params[route['labelParam']];
					}
					else if (angular.isString(route['labelFunc'])) {
						var func = scope.breadcrumbFunctions[route['labelFunc']];
						if (!angular.isFunction(func)) {
							console.error('pxBreadcrumb.resolveLabel', 'function not found ' + route['labelFunc']);
							return;
						}
						entry.label = func(entry);
					}
				}

				function findRoute(searchPath) {
					var params, entry = null;
					angular.forEach($route.routes, function(route) {
						if (!entry && (params = switchRouteMatcher(searchPath, route))) {
							if (route.redirectTo === (route.originalPath + '/')) {
								var redirectRoute = $route.routes[route.redirectTo];
								if (redirectRoute) {
									route = redirectRoute;
									searchPath += '/';
								}
							}

							entry = {
								label: null,
								route: route,
								path: searchPath,
								params: params,
								url: function() {
									var url = this.path.substring(1);
									return url ? url : ProximisGlobal.baseURL;
								}
							};

							if (route.hasOwnProperty('label')) {
								entry.label = route.label;
							}
						}
					});
					return entry;
				}

				/**
				 * @param {string} on The current url
				 * @param {Object} route The route regexp to match the url against
				 * @return {Object}
				 *
				 * @description
				 * Check if the route matches the current url.
				 *
				 * Inspired by match in visionmedia/express/lib/router/router.js.
				 */
				function switchRouteMatcher(on, route) {
					var keys = route.keys,
						params = {};

					if (!route.regexp) {
						return null;
					}

					var m = route.regexp.exec(on);
					if (!m) {
						return null;
					}

					for (var i = 1, len = m.length; i < len; ++i) {
						var key = keys[i - 1];
						var val = 'string' === typeof m[i] ? decodeURIComponent(m[i]) : m[i];
						if (key && val) {
							params[key.name] = val;
						}
					}
					return params;
				}
			}
		}
	}
})();