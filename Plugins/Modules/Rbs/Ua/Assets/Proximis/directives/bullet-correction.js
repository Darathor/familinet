/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxBulletCorrection
	 * @name pxBulletCorrection
	 * @restrict A
	 *
	 * @description
	 * Displays a bullet that indicates a correction on a document.
	 *
	 * @param {boolean} pxBulletCorrection `true` if there is a correction.
	 * @param {boolean=} showText If set to `'true'`, the tooltip will be displayed next to the bullet.
	 */
	app.directive('pxBulletCorrection', ['proximisCoreI18n', function(proximisCoreI18n) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/bullet-correction.twig',
			scope: {
				correction: '=pxBulletCorrection'
			},

			link: function(scope, element, attrs) {
				scope.showText = attrs['showText'] === 'true';
				scope.$watch('correction', function() {
					scope.tooltip = scope.correction ? proximisCoreI18n.trans('m.rbs.ua.common.has_current_correction|ucf') :
						proximisCoreI18n.trans('m.rbs.ua.common.has_no_correction|ucf');
				});
			}
		};
	}]);
})();