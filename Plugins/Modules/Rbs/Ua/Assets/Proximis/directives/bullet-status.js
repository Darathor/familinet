/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxBulletStatus
	 * @name pxBulletStatus
	 * @restrict A
	 *
	 * @description
	 * Displays a bullet that indicates the publication status of a document.
	 *
	 * @param {string} pxBulletStatus The publication status.
	 * @param {boolean=} showText If set to `'true'`, the tooltip will be displayed next to the bullet.
	 */
	app.directive('pxBulletStatus', ['proximisCoreI18n', function(proximisCoreI18n) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/bullet-status.twig',
			scope: {
				status: '=pxBulletStatus'
			},

			link: function(scope, element, attrs) {
				scope.showText = attrs['showText'] === 'true';
				scope.$watch('status', function() {
					scope.tooltip = scope.status ? proximisCoreI18n.trans('m.rbs.ua.common.status_' + scope.status + '|ucf') : null;
				});
			}
		};
	}]);
})();