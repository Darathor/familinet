/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxDateTimeSelector
	 * @name pxDateTimeSelector
	 *
	 * @description
	 * A directive to select a date and time.
	 *
	 * To select only a date, use {@link proximis.directive:pxDateSelector `pxDateSelector`}.
	 *
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 * @param {Date=} minDate If set, dates before minDate will be disabled.
	 * @param {Date=} maxDate If set, dates after maxDate will be disabled.
	 * @param {Array=} disabledDates If set, all dates in the disabledDates array will be disabled.
	 * @param {boolean=} readOnlyTimeZone If set to true, user can not be redirect to timezone management
	 */
	app.directive('pxDateTimeSelector', ['proximisSettings', 'proximisRoute', function(proximisSettings, proximisRoute) {
		return selector(false, proximisSettings, proximisRoute);
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxDateSelector
	 * @name pxDateSelector
	 *
	 * @description
	 * A directive to select a date.
	 *
	 * To select date and time, use {@link proximis.directive:pxDateTimeSelector `pxDateTimeSelector`}.
	 *
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 * @param {Date=} minDate If set, dates before minDate will be disabled.
	 * @param {Date=} maxDate If set, dates after maxDate will be disabled.
	 * @param {Array=} disabledDates If set, all dates in the disabledDates array will be disabled.
	 * @param {boolean=} readOnlyTimeZone If set to true, user can not be redirect to timezone management
	 */
	app.directive('pxDateSelector', ['proximisSettings', 'proximisRoute', function(proximisSettings, proximisRoute) {
		return selector(true, proximisSettings, proximisRoute);
	}]);

	function selector(hideTime, proximisSettings, proximisRoute) {
		return {
			restrict: 'A',
			scope: {
				fieldName: '@',
				minDate: '=',
				maxDate: '=',
				disabledDates: '=',
				readOnlyTimeZone: '='
			},
			templateUrl: 'Rbs/Ua/Proximis/directives/date-time-selector.twig',
			require: 'ngModel',
			link: function(scope, element, attrs, ngModel) {
				scope.currentModuleName = proximisRoute.getRoutesDefaultModule();
				scope.data = {
					hideTime: hideTime,
					opened: false,
					date: null,
					timeZone: null
				};
				scope.hideNow = attrs.hideNow === 'true';
				scope.hideClear = attrs.hideClear === 'true';

				scope.options = {
					minDate: scope.minDate ? moment(scope.minDate).toDate() : null,
					maxDate: scope.maxDate ? moment(scope.maxDate).toDate() : null,
					dateDisabled: disabled
				};

				function disabled(data) {
					if (scope.disabledDates) {
						var date = moment(data.date);
						var mode = data.mode;
						if (mode === 'day') {
							for (var i = 0; i < scope.disabledDates.length; i++) {
								if (date.isSame(scope.disabledDates[i], 'day')) {
									return true;
								}
							}
						}
					}
					return false;
				}

				scope.data.timeZone = proximisSettings.get('TimeZone');

				// modelValue => viewValue
				// string => Date (local TimeZone)
				ngModel.$formatters.unshift(function(modelValue) {
					if (modelValue) {
						var d = moment(modelValue);
						d = d.tz(scope.data.timeZone);
						d.set({ second: 0, millisecond: 0 });
						if (scope.data.hideTime) {
							d.set({ hour: 0, minute: 0 });
						}
						return new Date(d.format('YYYY-MM-DDTHH:mm:ss') + d.local().format('Z'));
					}
				});

				scope.$on('user.settings.changed', function(event, settings) {
					scope.data.timeZone = settings.TimeZone;
					ngModel.$render();
				});

				scope.updateDate = function() {
					scope.$evalAsync(function() {
						ngModel.$setViewValue(scope.data.date ? angular.copy(scope.data.date) : null);
					});
				};

				// viewValue => modelValue
				// Date => string
				ngModel.$parsers.unshift(function(viewValue) {
					if (viewValue instanceof Date && (!isNaN(viewValue.getTime()))) {
						var d = moment.tz(moment(viewValue).format('YYYY-MM-DDTHH:mm:ss'), scope.data.timeZone);
						d.set({ second: 0, millisecond: 0 });
						if (scope.data.hideTime) {
							return d.format('YYYY-MM-DD');
						}
						return d.utc().format();
					}
					return null;
				});

				ngModel.$render = function() {
					scope.data.date = ngModel.$viewValue;
				};

				scope.setDateToNow = function() {
					var f = 'YYYY-MM-DDTHH:mm:00';
					if (hideTime) {
						f = 'YYYY-MM-DDT00:00:00';
					}
					var now = moment();
					var s = now.tz(scope.data.timeZone).format(f) + now.local().format('Z');
					ngModel.$setViewValue(new Date(s));
					ngModel.$render();
				};

				scope.clearDate = function() {
					ngModel.$setViewValue(null);
					ngModel.$render();
				};

				scope.openPicker = function() {
					scope.data.opened = true;
				};
			}
		};
	}
})();