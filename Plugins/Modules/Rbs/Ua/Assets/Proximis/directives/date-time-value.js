/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxDateTimeValue
	 * @name pxDateTimeValue
	 *
	 * @description
	 * A directive to render the content of a time tag.
	 *
	 * @param {string} pxDateTimeValue The date.
	 * @param {string=} display The format: `relative`, `both` or `absolute` (`absolute` by default).
	 */
	app.directive('pxDateTimeValue', ['$filter', 'proximisSettings', function($filter, proximisSettings) {
		return {
			restrict: 'A',
			element: 'time',
			scope: {
				'dateTime': '=pxDateTimeValue',
				'display': '@'
			},

			link: function(scope, element, attrs) {
				var timeZone = proximisSettings.get('TimeZone');
				var dateTime;
				var content = element.html();

				scope.$watch('dateTime', watchDateTime);

				scope.$watch('display', update);

				function watchDateTime(value, oldValue) {
					if (!value) {
						element.attr('datetime', null);
					}
					else if (!dateTime || value !== oldValue) {
						element.attr('datetime', value);
						dateTime = moment(value);
						update();
					}
				}

				function update() {
					if (!dateTime) {
						element.html('');
						element.removeAttr('title');
						return;
					}

					var html, title;
					switch (attrs.display) {
						case 'relative':
							html = dateTime.fromNow();
							title = $filter('pxDateTime')(dateTime.format());
							break;
						case 'both':
							html = $filter('pxDateTime')(dateTime.format()) + ' (' + dateTime.fromNow() + ')';
							break;
						default :
							title = dateTime.fromNow();
							html = $filter('pxDateTime')(dateTime.format());
							break;
					}

					element.html(content ? content.replace(/\{time}/, html) : html);

					if (title) {
						element.attr('title', title);
					}
					else {
						element.removeAttr('title');
					}
				}
			}
		};
	}]);
})();