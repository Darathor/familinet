/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis:pxDocumentSelector
	 * @name pxDocumentSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string|Array|Object} acceptedModels A model name, a model array or an object describing model filters.
	 *
	 * List of available filters:
	 *  * `publishable` - {@type boolean}
	 *  * `activable` - {@type boolean}
	 *  * `editable` - {@type boolean}
	 *  * `instanceOf` - {@type boolean}
	 *
	 * **Examples:**
	 *  * `data-accepted-models="'Rbs_Website_StaticPage'"`
	 *  * `data-accepted-models="['Rbs_Website_StaticPage', 'Rbs_Website_Menu']"`
	 *  * `data-accepted-models="{ publishable: true }"`
	 *
	 * @param {string} searchRestService The webservice to search documents.
	 * @param {string} loadRestService The webservice to load documents.
	 * @param {Object=} restServiceParams Additional webservice parameters.
	 *
	 * The default REST services use an optional `urlContext` parameter to configure published document URLs generation.
	 * If it is provided, this parameter should be an object with the following properties:
	 *  * `websiteId` - {@type number} required
	 *  * `sectionId` - {@type number} optional
	 *  * `LCID` - {@type string} required
	 *
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to make the selector readonly.
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 * @param {string=} defaultModel the model name selected by default.
	 * @param {Object=} selectionModals used to define a selection modal (by its identifier) for a specific document model
	 *
	 * **Examples:**
	 *  * `data-selection-modals="{'Rbs_Media_Image': 'proximisUa/imageSelector'}"`
	 *
	 * @description
	 * Displays a Document selector to let the user select a single document.
	 */
	app.directive('pxDocumentSelector',
		['proximisDocumentModels', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisModalStack', 'proximisCoreArray',
			function(proximisDocumentModels, proximisRestApi, proximisRestApiDefinition, proximisModalStack, proximisCoreArray) {
				return {
					restrict: 'A',
					templateUrl: 'Rbs/Ua/Proximis/directives/document-selector.twig',
					require: 'ngModel',
					scope: true,
					controller: 'ProximisItemSelectorController',

					link: {
						pre: function(scope, element, attrs, ngModel) {
							documentSelectorFunction(scope, element, attrs, ngModel, false,
								proximisDocumentModels, proximisRestApi, proximisRestApiDefinition, proximisModalStack, proximisCoreArray);
						}
					}
				};
			}
		]);

	/**
	 * @ngdoc directive
	 * @id proximis:pxDocumentsSelector
	 * @name pxDocumentsSelector
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {string|Array|Object} acceptedModels A model name, a model array or an object describing model filters.
	 *
	 * List of available filters:
	 *  * `publishable` - {@type boolean}
	 *  * `activable` - {@type boolean}
	 *  * `editable` - {@type boolean}
	 *  * `instanceOf` - {@type string}
	 *
	 * **Examples:**
	 *  * `data-accepted-models="'Rbs_Website_StaticPage'"`
	 *  * `data-accepted-models="['Rbs_Website_StaticPage', 'Rbs_Website_Menu']"`
	 *  * `data-accepted-models="{ publishable: true }"`
	 *
	 * @param {string} searchRestService The webservice to search documents.
	 * @param {string} loadRestService The webservice to load documents.
	 * @param {Object=} restServiceParams Additional webservice parameters.
	 *
	 * The default REST service uses an optional `urlContext` parameter to configure published document URLs generation.
	 * If it is provided, this parameter should be an object with the following properties:
	 *  * `websiteId` - {@type number} required
	 *  * `sectionId` - {@type number} optional
	 *  * `LCID` - {@type string} required
	 *
	 * @param {string=} selectorTitle The placeholder of the selector.
	 * @param {boolean=} readonly Set to true to selector readonly.
	 * @param {string=} fieldName If set, a data-px-field="{value}" directive will be added to the main field.
	 * @param {string=} defaultModel the model name selected by default.
	 * @param {Object=} selectionModals used to define a selection modal (by its identifier) for a specific document model
	 *
	 * **Examples:**
	 *  * `data-selection-modals="{'Rbs_Media_Image': 'proximisUa/imageSelector'}"`
	 *
	 * @description
	 * Displays a Document selector to let the user select multiple documents.
	 */
	app.directive('pxDocumentsSelector',
		['proximisDocumentModels', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisModalStack', 'proximisCoreArray',
			function(proximisDocumentModels, proximisRestApi, proximisRestApiDefinition, proximisModalStack, proximisCoreArray) {
				return {
					restrict: 'A',
					templateUrl: 'Rbs/Ua/Proximis/directives/document-selector.twig',
					require: 'ngModel',
					scope: true,
					controller: 'ProximisItemSelectorController',

					link: {
						pre: function(scope, element, attrs, ngModel) {
							documentSelectorFunction(scope, element, attrs, ngModel, true,
								proximisDocumentModels, proximisRestApi, proximisRestApiDefinition, proximisModalStack, proximisCoreArray);
						}
					}
				};
			}
		]);

	function documentSelectorFunction(scope, element, attrs, ngModel, multiple,
		proximisDocumentModels, proximisRestApi, proximisRestApiDefinition, proximisModalStack, proximisCoreArray) {
		scope.multiple = multiple;
		scope.readonly = !!attrs.readonly;
		scope.disableReordering = !multiple || scope.readonly;
		scope.selectorTitle = attrs.selectorTitle;
		scope.selectorElement = element;
		scope.fieldName = attrs.fieldName || 'false';
		scope.defaultModel = attrs.defaultModel || null;
		scope.selectionModals = scope.$eval(attrs.selectionModals) || {};

		var searchRestService = attrs['searchRestService'];
		var loadRestService = attrs['loadRestService'];
		if (!searchRestService || !loadRestService) {
			console.error('Attributes data-search-rest-service and data-load-rest-service are required!')
		}

		scope.models = { filtered: [], selected: null };

		attrs.$observe('acceptedModels', function() {
			var value = scope.$eval(attrs['acceptedModels']);
			if (angular.isString(value)) {
				value = [value];
			}

			var filter = value;
			if (angular.isArray(filter)) {
				scope.models.filters = { name: filter };
			}
			else if (angular.isObject(filter)) {
				scope.models.filters = filter;
			}
			else {
				scope.models.filters = { abstract: false, editable: true };
			}
			scope.models.filtered = proximisDocumentModels.getByFilter(scope.models.filters);

			angular.forEach(scope.models.filtered, function(model) {
				angular.forEach(scope.selectionModals, function(modalName, modelName) {
					if (model.name === modelName) {
						model.selectionModal = modalName;
					}
				});
			});
		});

		scope.$watchCollection('models.filtered', function(modelsFiltered) {
			if (angular.isArray(modelsFiltered)) {
				if (scope.models.selected) {
					selectModelName(scope.models.selected.name);
				}
				//noinspection JSUnresolvedVariable
				if (!angular.isObject(scope.models.selected) && modelsFiltered.length) {
					selectModelName(scope.defaultModel ? scope.defaultModel : modelsFiltered[0].name);
				}
			}
		});

		function selectModelName(modelName) {
			if (!modelName) {
				return;
			}

			if (angular.isObject(scope.models.filtered)) {
				for (var i = 0; i < scope.models.filtered.length; i++) {
					if (scope.models.filtered[i].name === modelName) {
						scope.models.selected = scope.models.filtered[i];
						break;
					}
				}
			}
		}

		// viewValue => modelValue
		ngModel.$parsers.unshift(function(viewValue) {
			if (viewValue === undefined) {
				return viewValue;
			}

			var modelValue;
			if (multiple) {
				modelValue = [];
				angular.forEach(viewValue, function(item) {
					if (angular.isObject(item) && angular.isObject(item._meta) && item._meta.id) {
						modelValue.push(item._meta.id);
					}
					else {
						console.error('[pxDocumentsSelector] Invalid document', item);
					}
				});
			}
			else {
				modelValue = 0;
				if (angular.isObject(viewValue)) {
					if (angular.isObject(viewValue._meta) && viewValue._meta.id) {
						modelValue = viewValue._meta.id;
					}
					else {
						console.error('[pxDocumentSelector] Invalid document', viewValue);
					}
				}
			}
			return modelValue;
		});

		// modelValue => viewValue
		ngModel.$formatters.unshift(function(modelValue) {
			if (modelValue === undefined) {
				return modelValue;
			}

			var viewValue = multiple ? [] : null;

			if (multiple) {
				if (angular.isArray(modelValue)) {
					viewValue = angular.copy(modelValue);
				}
			}
			else {
				if (angular.isNumber(modelValue) && modelValue > 0) {
					viewValue = modelValue
				}
			}

			return viewValue;
		});

		ngModel.$render = function() {
			var viewValue = ngModel.$viewValue;
			var oldList = scope.items.list;
			var itemList = [];

			if (multiple) {
				if (angular.isArray(viewValue)) {
					var ids = [], item;
					angular.forEach(viewValue, function(id) {
						if (angular.isNumber(id) && id > 0) {
							item = scope.tools.getItemById(oldList, id);
							if (item) {
								itemList.push(item);
							}
							else {
								ids.push(id);
							}
						}
						else {
							console.error('[pxDocumentsSelector] Invalid number value:', id);
						}
					});
					if (ids.length) {
						var params = completeParams({ documentIds: ids });
						proximisRestApi.sendData(proximisRestApiDefinition.getData(loadRestService, 'GET', params)).then(
							function(result) {
								angular.forEach(result.data.items, function(item) {
									itemList.push(item);
								});
							},
							function(error) {
								console.error('[pxDocumentsSelector]', error);
							}
						);
					}
				}
			}
			else if (angular.isNumber(viewValue) && viewValue > 0) {
				params = completeParams({ documentIds: [viewValue] });
				proximisRestApi.sendData(proximisRestApiDefinition.getData(loadRestService, 'GET', params)).then(
					function(result) {
						itemList.push(result.data.items[0]);
					},
					function(error) {
						console.error('[pxDocumentSelector]', error);
					}
				);
			}

			scope.items.list = itemList;
		};

		scope.openSelectionModal = function() {
			var config = { config: { selectionMode: multiple ? 'multiple' : 'single' } };
			proximisModalStack.open({ definitionName: scope.models.selected.selectionModal, resolve: config}).result.then(
				function(result) {
					angular.forEach(result.data.documentIds, function(docId) {
						if(multiple) {
							if (!proximisCoreArray.inArray(docId, ngModel.$modelValue)) {
								ngModel.$modelValue.push(docId);
							}
						}
						else {
							ngModel.$modelValue = docId;
						}
					});

					if (result.data.documentIds) {
						var params = completeParams({ documentIds: result.data.documentIds });
						proximisRestApi.sendData(proximisRestApiDefinition.getData(loadRestService, 'GET', params)).then(
							function(result) {
								scope.items.list = scope.items.list.concat(result.data.items);
								if (multiple) {
									ngModel.$setViewValue(scope.tools.arrayCopy(scope.items.list));
								}
								else {
									ngModel.$setViewValue(scope.items.list.length ? scope.items.list[0] : null);
								}
							},
							function(error) {
								console.error('[pxDocumentSelector]', error);
							}
						);
					}
				},
				function() {}
			);
		};

		scope.onItemListUpdated = function() {
			if (scope.items.list.length === 0 && ngModel.$viewValue === undefined) {
				return;
			}
			if (multiple) {
				ngModel.$setViewValue(scope.tools.arrayCopy(scope.items.list));
			}
			else {
				ngModel.$setViewValue(scope.items.list.length ? scope.items.list[0] : null);
			}
		};

		scope.canAutoComplete = function() {
			return scope.models.selected && scope.autoComplete.value.trim().length > 0;
		};

		scope.getAutoCompleteDefinition = function() {
			var value = scope.autoComplete.value.trim().toLowerCase();
			var params = { model: scope.models.selected.name, search: value, offset: scope.autoComplete.offset, limit: scope.autoComplete.limit };
			return proximisRestApiDefinition.getData(searchRestService, 'GET', completeParams(params));
		};

		scope.$watch('models.selected.name', function() {
			if (angular.isObject(scope.autoComplete) && angular.isFunction(scope.autoComplete.refresh)) {
				scope.autoComplete.refresh();
			}
		});

		function completeParams(params) {
			if (attrs['restServiceParams']) {
				angular.forEach(scope.$eval(attrs['restServiceParams']), function(value, key) {
					params[key] = value;
				});
			}
			return params;
		}
	}
})();