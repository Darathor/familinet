/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxUaEntityList
	 * @name pxUaEntityList
	 * @restrict A
	 * @scope
	 *
	 * @param {string} restService The service to retrieve the data.
	 * @param {Object=} restParams The service params to retrieve the data.
	 * @param {Object=} actionMethods An object providing the actions declared in {@link proximis.directive:pxListAction `pxListAction`} children.
	 *     If an object is passed, some methods will be automatically added to perform actions from containers:
	 *      * `refresh(promise)`: refreshes the list
	 *      * `toggleRelativeDates(column)`: toggles the date format in the named column
	 * @param {Object=} filter The filter for the list.
	 * @param {Object=} shared An object accessible in item template (not used by the list itself).
	 *
	 * @description
	 * Displays a list of entities in a table.
	 *
	 * Events emitted:
	 *   - pxUaEntityList.initialized when list completely initialized (listen filterUpdated event)
	 *   - pxUaEntityList.loaded when collection updated
	 *   - pxUaEntityList.explicitRefresh when refresh button clicked
	 *
	 * Events listened:
	 *   - filterUpdated to refresh the list using a new filter
	 *
	 * Children Directives:
	 *
	 * - {@link proximis.directive:pxListColumn `pxListColumn`}
	 * - {@link proximis.directive:pxListAction `pxListAction`}
	 *
	 * @example
	 * Directive usage:
	 *
	 * ```html
	 *     <div data-px-ua-entity-list="" data-rest-service="..." data-filter="filter">
	 *         <div data-px-list-column="" data-name="label" data-label="..."></div>
	 *         <div data-px-list-column="" ...></div>
	 *         ...
	 *         <div data-px-list-action="" data-name="..." data-icon="..." data-label="..."></div>
	 *     </div>
	 * ```
	 *
	 * Usage of `toggleRelativeDates` from `actionMethods` in a controller to use relative dates by default:
	 *
	 * ```js
	 *     var removeWatcher = scope.$watch('actionMethods.toggleRelativeDates', function (toggleRelativeDates) {
	 *         if (angular.isFunction(toggleRelativeDates)) {
	 *             toggleRelativeDates('myColumn');
	 *             removeWatcher();
	 *         }
	 *     });
	 *  ```
	 */
	app.directive('pxUaEntityList', ['$location', 'proximisCoreArray', 'proximisCoreI18n', 'proximisRestApi', 'proximisRestApiDefinition',
		'proximisMessages', 'proximisSettings', 'proximisErrorFormatter', 'localStorageService', '$timeout', pxUaEntityList]);

	function pxUaEntityList($location, proximisCoreArray, proximisCoreI18n, proximisRestApi, proximisRestApiDefinition,
		proximisMessages, proximisSettings, proximisErrorFormatter, localStorageService, $timeout) {

		function initActions(scope) {
			var actionNames = {};
			angular.forEach(scope.actions.list, function(action) {
				if (actionNames[action.name]) {
					throw new Error('Parameter "name" for actions defined in px-ua-entity-list should be unique.');
				}
				actionNames[action.name] = true;
			});

			if (scope.actions.list.length && !scope.actionMethods) {
				console.error('[pxUaEntityList] data-action-methods attribute is required when some actions (px-list-action) are defined.');
			}
		}

		function initColumns(scope) {
			if (!scope.columns) {
				throw new Error('Could not find any column for px-ua-entity-list directive. ' +
					'We are sure you want something to be displayed in this list :)');
			}
			scope.defaultSort = { column: 'id', descending: true};
			scope.colSpan = scope.columns.length;
			if (scope.actions.multiple.length) {
				scope.selection.enabled = true;
				scope.colSpan += 1;
			}
			if (scope.actions.single.length) {
				scope.colSpan += 1;
			}

			scope.imageFormats = [];
			scope.availableSortColums = [];
			angular.forEach(scope.columns, function(column) {
				if (column.thumbnailFormat) {
					scope.imageFormats.push(column.thumbnailFormat);
				}

				if (column.sort) {
					scope.availableSortColums.push(column.sort);

					if (column.defaultSort) {
						scope.defaultSort.column = column.sort;
						scope.defaultSort.descending = column.defaultSort === 'desc';
					}
				}
			});
		}

		/**
		 * px-ua-entity-list directive.
		 */
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/entity-list.twig',
			transclude: 'element',
			replace: true,
			scope: {
				actionMethods: '=',
				filter: '=',
				restService: '@',
				restParams: '=',
				shared: '='
			},

			controller: ['$scope', '$transclude',  function(scope, $transclude) {
				function handlePromise(promise) {
					promise.then(
						function(result) {
							var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
							proximisMessages.success(title, result.data.status.message, true, 10000);
							scope.actionMethods.refresh();
						},
						function(result) {
							var title = proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf');
							proximisMessages.error(title, result.data.status.message, true);
							scope.actionMethods.refresh();
						}
					);
				}

				//
				// Entity selection.
				//
				scope.selection = {
					enabled: false,
					all: false,
					indexes: [],
					uaEntities: [],
					clear: function() {
						if (scope.selection.all) {
							scope.selection.all = false;
						}
						else {
							setFullSelection(false);
						}
					}
				};

				function setFullSelection(makeSelected) {
					angular.forEach(scope.selection.indexes, function(selected, index) {
						scope.selection.indexes[index] = makeSelected;
					});
				}

				scope.$watch('selection.all', setFullSelection);

				//
				// Actions and columns.
				//
				scope.actions = {
					running: false,
					list: [],
					single: [],
					multiple: [],
					execute: function (action, doc) {
						if (angular.isObject(action) && scope.actionMethods.hasOwnProperty(action.name)) {
							var promise = scope.actionMethods[action.name](doc ? [doc] : scope.selection.uaEntities, scope.selection);
							if (angular.isObject(promise)) {
								handlePromise(promise);
							}
						}
					}
				};

				scope.columns = [];

				$transclude(scope, function(transEl) {
					//Ignore transcluded elements
				});

				initActions(scope);
				initColumns(scope);

				// Complete actionMethods object to share methods.
				if (angular.isObject(scope.actionMethods)) {
					// Refresh.
					if (!scope.actionMethods.hasOwnProperty('refresh')) {
						scope.actionMethods.refresh = function(promise) {
							if (angular.isObject(promise)) {
								handlePromise(promise);
							}
							else {
								scope.refresh();
							}
						}
					}

					// Toggle date format.
					if (!scope.actionMethods.hasOwnProperty('toggleRelativeDates')) {
						scope.actionMethods.toggleRelativeDates = function(column) {
							scope.toggleRelativeDates(column);
						}
					}
				}

				//
				// Pagination.
				//
				var search = $location.search();
				scope.pagination = {
					enabled: false,
					itemsPerPage: search.limit || proximisSettings.get('pagingSize', 10),
					currentPage: (search.page || 1),
					totalItems: 0
				};

				scope.$watch('pagination.itemsPerPage', function (value, oldValue) {
					if (value !== oldValue) {
						if (scope.context !== 'modal') {
							$location.search('limit', scope.pagination.itemsPerPage);
						}
						scope.refresh();
					}
				});
				scope.$watch('pagination.currentPage', function(value, oldValue) {
					if (value !== oldValue) {
						if (scope.context !== 'modal') {
							$location.search('page', scope.pagination.currentPage);
						}
						var offset = (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage;
						if (offset !== scope.pagination.offset) {
							scope.refresh();
						}
					}
				});

				//
				// Sort.
				//
				function getStoredSort() {
					if (scope.restService) {
						var local = localStorageService.get('list_sort_' + scope.restService);
						if (local) {
							var storedSort = angular.fromJson(local);
							return checkSort(storedSort) ? storedSort : null;
						}
					}
					return null;
				}

				function saveStoredSort(sort) {
					if (scope.restService) {
						if (!sort) {
							localStorageService.remove('list_sort_' + scope.restService);
						}
						else {
							localStorageService.set('list_sort_' + scope.restService, sort);
						}
					}
				}

				function checkSortColumn(column) {
					return proximisCoreArray.inArray(column, scope.availableSortColums);
				}

				function checkSort(sort) {
					return sort && checkSortColumn(sort.column);
				}

				function getEmptySort() {
					return scope.defaultSort;
				}

				scope.sort = getStoredSort() || getEmptySort();
				if (search.sort && checkSortColumn(search.sort)) {
					scope.sort.column = search.sort;
				}
				if (search.desc) {
					scope.sort.descending = search.desc === 'true';
				}

				scope.sortOn = function(column) {
					var emptySort = getEmptySort();
					if (scope.sort.column === column.sort) {
						if (!scope.sort.descending) {
							scope.sort.descending = true;
							$location.search('desc', 'true');
						}
						else {
							$location.search('sort', emptySort.column);
							scope.sort.column = emptySort.column;
							var desc = column.sort === emptySort.column ? !scope.sort.descending : emptySort.descending;
							scope.sort.descending = desc;
							$location.search('desc', desc ? 'true' : 'false');
						}
					}
					else {
						$location.search('sort', column.sort);
						$location.search('desc', 'false');
						scope.sort.column = column.sort;
						scope.sort.descending = false;
					}
					saveStoredSort(scope.sort);
					scope.refresh();
				};

				//
				// Resources loading.
				//
				scope.collection = [];

				scope.refresh =  function () {
					load();
				};

				var filter = scope.filter;
				if (filter === true)
				{
					var waitFilterUpdated;
					scope.$on('filterUpdated', function(event, newFilter) {
						if (waitFilterUpdated) {
							$timeout.cancel(waitFilterUpdated);
						}
						waitFilterUpdated = $timeout(function() {
							filter = angular.copy(newFilter);
							scope.refresh();
						}, 300);
					});
				}

				function load() {
					scope.pagination.loading = true;

					var params = {
						offset: (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage,
						limit: scope.pagination.itemsPerPage,
						sort: scope.sort.column,
						desc: scope.sort.descending
					};

					if (scope.imageFormats) {
						params.imageFormats = angular.copy(scope.imageFormats);
					}

					if (filter) {
						params.filter = filter;
					}

					if (scope.restParams) {
						angular.extend(params, scope.restParams);
					}

					var requestData = proximisRestApiDefinition.getData(scope.restService, 'GET', params);
					var promise = proximisRestApi.sendData(requestData);
					promise.then(
						function(result) {
							if (result.data !== null) {
								if (result.data.pagination) {
									scope.pagination.enabled = true;
									scope.pagination.offset = result.data.pagination.offset;
									scope.pagination.totalItems = result.data.pagination.count;
									scope.collection = result.data.items;
								}
								else {
									scope.pagination.enabled = false;
									scope.collection = result.data.items;
								}

								refreshActionsActivation(true);

								scope.$emit('pxUaEntityList.loaded', { collection: scope.collection });
							}
							scope.pagination.loading = false;
						},
						function(result) {
							if (!proximisMessages.displayError(result.data.status, result.status)) {
								var title = proximisCoreI18n.trans('m.rbs.ua.common.error_loading_list | ucf');
								proximisMessages.error(title, proximisErrorFormatter.format(result.data.status));
							}
							scope.pagination.loading = false;
						}
					);
					return promise;
				}

				function refreshActionsActivation(refreshSingle) {
					// Multiple actions.
					var selectionLength = scope.selection.uaEntities.length;
					angular.forEach(scope.actions.multiple, function(action) {
						if (!selectionLength) {
							action.isActiveMultiple = false;
							return;
						}
						action.isActiveMultiple = true;

						var method = action.activationMethod;
						if (method) {
							if (angular.isObject(scope.actionMethods) && scope.actionMethods.hasOwnProperty(method)) {
								if (action.atLeastOne) {
									action.isActiveMultiple = false;
									for (var i = 0; i < selectionLength; i++) {
										if (scope.actionMethods[method](scope.selection.uaEntities[i])) {
											action.isActiveMultiple = true;
											return;
										}
									}
								} else {
									for (var i = 0; i < selectionLength; i++) {
										if (!scope.actionMethods[method](scope.selection.uaEntities[i])) {
											action.isActiveMultiple = false;
											return;
										}
									}
								}
							}
							else {
								console.warn('[uaEntityList] Activation method not found: ' + method);
								action.isActiveMultiple = false;
							}
						}
					});

					// Single actions.
					if (refreshSingle) {
						var collectionLength = scope.collection.length;
						for (var i = 0; i < collectionLength; i++) {
							var doc = scope.collection[i];
							doc._isActiveSingle = {};
							angular.forEach(scope.actions.single, function(action) {
								var method = action.activationMethod;
								if (!method) {
									doc._isActiveSingle[action.name] = true;
								}
								else if (angular.isObject(scope.actionMethods) && scope.actionMethods.hasOwnProperty(method)) {
									doc._isActiveSingle[action.name] = scope.actionMethods[method](doc);
								}
								else {
									console.warn('[uaEntityList] Activation method not found: ' + method);
									doc._isActiveSingle[action.name] = false;
								}
							});
						}
					}
				}

				scope.refreshActionsActivation = refreshActionsActivation;

				if (filter !== true) {
					scope.refresh();
				}

				scope.explicitRefresh = function() {
					scope.$emit('pxUaEntityList.explicitRefresh');
					scope.refresh();
				};

				scope.$emit('pxUaEntityList.initialized');
			}],

			link: function(scope) {

				function uaEntitiesEquals(doc1, doc2) {
					if (!angular.isObject(doc1._meta) || !angular.isObject(doc2._meta)) {
						return false;
					}
					var equals = true;
					angular.forEach(doc1._meta, function(value, key) {
						if (doc2._meta[key] != value) {
							equals = false;
						}
					});
					return equals;
				}

				function onIndexesUpdated() {
					if (!scope.selection.enabled) {
						return;
					}
					scope.selection.uaEntities = [];
					var selected = [];
					angular.forEach(scope.collection, function(doc, index) {
						var isSelected = false;
						if (index < scope.selection.indexes.length && scope.selection.indexes[index]) {
							scope.selection.uaEntities.push(doc);
							isSelected = true;
						}
						selected.push(isSelected);
					});
					scope.selection.indexes = selected;
					scope.refreshActionsActivation(false);
				}

				function onCollectionUpdated() {
					if (!scope.selection.enabled) {
						return;
					}
					var oldUaEntities = angular.copy(scope.selection.uaEntities);
					scope.selection.uaEntities = [];
					var selected = [];
					angular.forEach(scope.collection, function(doc) {
						var isSelected = false;
						angular.forEach(oldUaEntities, function(oldDoc) {
							if (!isSelected) {
								isSelected = uaEntitiesEquals(oldDoc, doc);
							}
						});
						selected.push(isSelected);
					});
					scope.selection.indexes = selected;
				}


				scope.$watchCollection('collection', onCollectionUpdated);
				scope.$watch('selection.indexes', onIndexesUpdated, true);

				//
				// Dates display.
				//
				scope.dateDisplay = {};

				angular.forEach(scope.columns, function(column) {
					if (column.format === 'date' || column.format === 'date-interval') {
						//noinspection JSUnresolvedVariable
						scope.dateDisplay[column.name] = column.initialDisplay || '';
					}
				});

				scope.toggleRelativeDates = function(column) {
					scope.dateDisplay[column] = scope.dateDisplay[column] === 'relative' ? '' : 'relative';
				};

				scope.isSortable = function(column) {
					return column.sort;
				};

				scope.isSortedOn = function(column) {
					return scope.sort.column === column.sort
				};

				scope.isSortDescending = function() {
					return scope.sort.descending;
				};

				scope.classSelector = function(column, doc) {
					var classes = [];
					if (scope.isSortedOn(column)) {
						classes.push('sorted');
					}

					if (column.classSelector) {
						var parts = column.classSelector.split('.');
						var specificClasses = callMethod(scope, parts, column, doc);
						if (angular.isString(specificClasses)) {
							classes.push(specificClasses);
						}
					}

					return classes.join(' ');
				};

				function callMethod(object, parts, column, doc) {
					if (parts.length === 0 || !angular.isObject(object)) {
						return null;
					}
					else if (parts.length === 1 && angular.isFunction(object[parts[0]])) {
						return object[parts[0]](column, doc);
					}
					else {
						var propertyName = parts.shift();
						return callMethod(object[propertyName], parts, column, doc);
					}
				}
			}
		};
	}

	app.directive('pxUaEntityListCell', ['$compile', function($compile) {
		return {
			restrict: 'A',
			template: '<span></span>',
			link: function(scope, elm) {
				var content = scope.column.content;
				if (content) {
					elm.append($compile('<span>' + content + '</span>')(scope));
				}
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxListColumn
	 * @name pxListColumn
	 * @restrict A
	 *
	 * @param {string} pxListColumn The property related to this column (example: `common.label`).
	 * @param {string} label The human readable column's label.
	 * @param {boolean=} primary if set to `true`, adds a `primary-cell` class the cells and a link on the value (if no custom content is defined).
	 * @param {string=} sort The sort property related to this column. If this parameter is omitted, sorting won't be proposed.
	 * @param {string=} defaultSort Sort by default with this property, value should be 'asc' or 'desc'.
	 * @param {string=} align The column alignment: `center` or `right` (default `left`).
	 * @param {string=} width The column width (example: `50px`).
	 * @param {string=} height The column cells height (example: `50px`).
	 * @param {string=} format A format for this column. Available formats are:
	 * <ul>
	 *     <li>`number`</li>
	 *     <li>`date`</li>
	 *     <li>`thumbnail`</li>
	 * </ul>
	 * @param {string=} thumbnailFormat Only if the `format` is `thumbnail`. Specifies the image format to display (can be one of the formats defined in
	 *     {@link proximis.constant:ProximisImageFormats `ProximisImageFormats`} or a custom size like `57x32`).
	 * @param {string=} classSelector a function with 2 parameters representing the cell context:
	 * <ul>
	 *     <li>`column`: The column</li>
	 *     <li>`doc`: The document</li>
	 * </ul>
	 *
	 * @description
	 * Column definition in a {@link proximis.directive:pxUaEntityList `pxUaEntityList`}.
	 *
	 * The content on the column is automatically generated according to the parameters. To make a custom content, just describe
	 * it inside the node of this directive. The current document is store in the scope as `doc`.
	 * @example
	 * ###Column with class selector
	 * HTML:
	 * 
	 * ```html
	 * <div data-px-list-column="common.pickingDateLimit" data-label="common.label" data-class-selector="shared.dateLimit">
	 * </div>
	 * ```
	 * JavaScript:
	 *
	 * ```js
	 * shared.dateLimit = function(column, doc) {
	 * 	var dateLimit = (new Date(doc.common.pickingDateLimit)).toISOString();
	 * 	var dateNow = new Date();
	 * 	if (dateLimit < dateNow.toISOString()) {
	 * 		return 'danger';
	 * 	}
	 * 	dateNow.setMinutes(dateNow.getMinutes() + 15);
	 * 	if (dateLimit < dateNow.toISOString()) {
	 * 		return 'warning';
	 * 	}
	 * };
	 * ```
	 */
	app.directive('pxListColumn', ['ProximisImageFormats', 'proximisCoreI18n', function(ProximisImageFormats, proximisCoreI18n) {
		return {
			restrict: 'A',
			require: '^pxUaEntityList',
			compile: function(element) {
				var content = element.html().trim();
				element.attr('data-col-ctx', content);
				element.html('');
			},
			controller: ['$scope', '$element', '$attrs', function(scope, element, attrs) {
				var column = {
					name: attrs['pxListColumn'],
					endName: attrs['endName'] || null,
					label: attrs['label'],
					format: attrs['format'],
					class: attrs['class'] || null,
					primary: attrs['primary'] || false,
					align: attrs['align'] || null,
					width: attrs['width'] || null,
					height: attrs['height'] || null,
					sort: attrs['sort'] || null,
					defaultSort: attrs['defaultSort'] || null,
					classSelector: attrs['classSelector'] || null
				};
				switch (column.format) {
					case 'number':
						if (!column.class) {
							column.class = 'column-number';
						}
						break;
					case 'date':
						column.content = '<time data-display="(= dateDisplay[\'' + column.name + '\'] =)"' +
							' data-px-date-time-value="doc.' + column.name + '"></time>';
						if (!column.class) {
							column.class = 'column-date';
						}
						break;
					case 'date-interval':
						if (!column.endName) {
							console.warn('column format "date-interval" requires a "endName" attribute on column ' + column.name);
						}
						column.content = '<time data-display="(= dateDisplay[\'' + column.name + '\'] =)"' +
							' data-px-date-time-value="doc.' + column.name + '"></time>';
						column.content += '<small data-ng-if="doc.' + column.endName + ' && doc.' + column.name + ' !== doc.' + column.endName + '"' +
							' class="text-muted">' + proximisCoreI18n.trans('m.rbs.ua.common.date_interval_end_label|ucf|lab') + ' ' +
							'<time data-display="(= dateDisplay[\'' + column.name + '\'] =)" data-px-date-time-value="doc.' + column.endName + '">' +
							'</time></small>';
						if (!column.class) {
							column.class = 'column-date';
						}
						break;

					case 'thumbnail':
						column.thumbnailFormat = attrs['thumbnailFormat'] || 's';
						if (ProximisImageFormats.hasOwnProperty(column.thumbnailFormat)) {
							column.thumbnailFormat = ProximisImageFormats[column.thumbnailFormat];
						}
						var expression = /\d+x\d+/;
						if (!expression.test(column.thumbnailFormat)) {
							throw new Error('Invalid thumbnail format ' + column.thumbnailFormat +
								' in column ' + column.name);
						}
						var dimensions = column.thumbnailFormat.split('x');
						column.width = dimensions[0] + 'px';
						column.height = dimensions[1] + 'px';

						column.content = '<img data-px-storage-image="doc.' + column.name + '" data-format="' +
							column.thumbnailFormat + '"/>';
						if (!column.class) {
							column.class = 'column-thumbnail';
						}
						break;
					default:
						var content = element.attr('data-col-ctx') || '';
						if (!content.length) {
							content = '(= doc.' + column.name + ' =)';
							if (column.primary) {
								content = '<a href="" data-ng-href="(= doc._meta|pxRoute:\'detail\' =)"><strong>' + content + '</strong></a>';
							}
						}
						column.content = content;
				}

				column.thStyle = {};
				column.tdStyle = {};
				if (column.align) {
					column.tdStyle['text-align'] = column.align;
				}
				if (column.width) {
					column.thStyle['width'] = column.width;
				}
				if (column.height) {
					column.tdStyle['height'] = column.height;
				}
				if (column.defaultSort && !column.sort) {
					console.warn('default sort without sort on column ' + column.name);
					column.defaultSort = undefined;
				}
				else if (column.defaultSort && column.defaultSort !== 'asc' && column.defaultSort !== 'desc') {
					console.warn("invalid default sort ('" + column.defaultSort + "'), should be 'asc' or 'desc' on column " + column.name);
					column.defaultSort = undefined;
				}
				scope.columns.push(column);
			}]
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxListAction
	 * @name pxListAction
	 * @restrict A
	 *
	 * @param {string} pxListAction The action technical name.
	 * @param {string} label The human readable action's label.
	 * @param {string=} icon The action's icon, in form `icon-xxx` ({@link http://fortawesome.github.io/Font-Awesome/3.2.1/icons/ available icons}).
	 * @param {string=} activationMethod Optional method name to validate a document for this action. This method should be defined in the
	 *     `actionMethods` parameter of the list and will be called with a single `doc` parameter containing the document object to check.
	 * @param {string=} class Optional class(es) to add to the action button.
	 * @param {string=} mode Optional mode: `single`, `multiple`, `both` (`both` by default).
	 *
	 * @description
	 * Action definition in a {@link proximis.directive:pxUaEntityList `pxUaEntityList`}.
	 */
	app.directive('pxListAction', [function() {
		return {
			restrict: 'A',
			require: '^pxUaEntityList',
			controller: ['$scope', '$element', '$attrs', function(scope, element, attrs) {
				var action = {
					name: attrs['pxListAction'] || null,
					label: attrs['label'] || null,
					icon: attrs['icon'] || 'icon-cog',
					mode: attrs['mode'] || 'both',
					activationMethod: attrs['activationMethod'] || null,
					atLeastOne: attrs['atLeastOne'] || false,
					class: attrs['class'] || 'btn-default'
				};

				if (!action.name) {
					throw new Error('The directive px-list-action requires a value parameter.');
				}
				if (!action.label) {
					throw new Error('The directive px-list-action requires a "label" parameter.');
				}

				scope.actions.list.push(action);
				if (action.single = action.mode !== 'multiple') {
					scope.actions.single.push(action);
				}
				if (action.multiple = action.mode !== 'single') {
					scope.actions.multiple.push(action);
				}
			}]
		};
	}]);
})();