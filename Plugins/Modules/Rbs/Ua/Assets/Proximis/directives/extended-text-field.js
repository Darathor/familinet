/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis:pxExtendedTextField
	 * @name pxExtendedTextField
	 * @restrict A
	 *
	 * @param {string} ngModel Assignable Angular expression to data-bind to.
	 * @param {Array=} variables Each item in the array should be an object with the following properties:
	 *  <ul>
	 *      <li>`label` - {@type string} - a human readable label</li>
	 *      <li>`code` - {@type string} optional - the variable's code for dynamic usage</li>
	 *      <li>`value` - {@type string} optional - the variable's value for static usage</li>
	 *  </ul>
	 *
	 * @description
	 * A text field with a menu to insert predefined suggestions.
	 *
	 * On suggestion selection the three keys `value`, `code` and `label` are checked consecutively until a not null value is found.
	 */
	app.directive('pxExtendedTextField', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/extended-text-field.twig',
			require: 'ngModel',
			scope: {
				variables: '='
			},

			link: function(scope, element, attrs, ngModel) {
				scope.data = {};

				ngModel.$render = function() {
					scope.data.value = ngModel.$viewValue;
				};

				scope.$watch('data.value', function(newValue, oldValue) {
					if (newValue !== oldValue) {
						ngModel.$setViewValue(newValue);
					}
				});

				scope.appendSuggestion = function(suggestion) {
					function getValue() {
						return (!angular.isString(scope.data.value) || !scope.data.value) ? '' : (scope.data.value + ' ');
					}

					if (suggestion.value !== null && suggestion.value !== undefined) {
						scope.data.value = getValue() + suggestion.value;
					}
					else if (suggestion.code !== null && suggestion.code !== undefined) {
						scope.data.value = getValue() + '{' + suggestion.code + '}';
					}
					else {
						scope.data.value = getValue() + suggestion.label;
					}
				}
			}
		};
	});
})();