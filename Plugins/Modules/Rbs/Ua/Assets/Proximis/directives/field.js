/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @type {number} Global counter to generate unique field ids.
	 */
	var fieldIdCounter = 0;

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxField
	 * @name pxField
	 *
	 * @description
	 * A Directive to generate an id on a field and link to an DOM ancestor {@link proximis.directive:pxLabel `pxLabel`}.
	 *
	 * @param {string=} pxField A string that will be included in the field id (no real effect, just for code readability).
	 */
	app.directive('pxField', ['ProximisEvents', function(ProximisEvents) {
		return {
			restrict: 'A',
			scope: false,

			link: function(scope, element, attributes) {
				if (attributes['pxField'] === 'false') {
					return;
				}
				var id = 'proximis_field_' + (++fieldIdCounter) + (attributes['pxField'] ? ('_' + attributes['pxField']) : '');
				element.attr('id', id);

				// pxLabel not in ancestor scopes, use jQuery DOM event instead
				element.trigger(ProximisEvents.FieldIdCreated, {id:id})
			}
		}
	}]);
})();