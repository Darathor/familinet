/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxFileModel
	 * @name pxFileModel
	 *
	 * @description
	 * A file model to get a file from the file upload system modal.
	 *
	 * @param {string} pxFileModel The file ngModel to bind to.
	 */
	app.directive('pxFileModel', [function() {
		return {
			scope: {
				file: '=pxFileModel'
			},
			link: function(scope, element) {
				var watcher = false;
				element.bind('change', function(event) {
					scope.$apply(function() {
						scope.file = event.target.files[0];
						if (!watcher) {
							watcher = scope.$watch('file', function(newValue) {
								if (!newValue) {
									element.val('');
									watcher();
									watcher = false;
								}
							});
						}
					});
				});
			}
		}
	}]);
})();