/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	var counter = 0;

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxLanguageSelector
	 * @name pxLanguageSelector
	 *
	 * @description
	 * A language selector based on available languages in the project.
	 *
	 * @param {string} pxLanguageSelector The selector ngModel to bind to.
	 * @param {string=} fieldName If set, a name="{value}" attribute and data-px-field="{value}" directive will be added to the main field.
	 */
	app.directive('pxLanguageSelector', ['ProximisGlobal', function(ProximisGlobal) {
		return {
			restrict: 'A',
			scope: {
				model: '=pxLanguageSelector'
			},
			templateUrl: 'Rbs/Ua/Proximis/directives/language-selector.twig',
			link: {
				pre: function(scope, element, attrs) {
					scope.fieldName = attrs.fieldName || 'false';
					if (attrs.fieldName) {
						scope.name = attrs.fieldName;
					}
					else {
						scope.name = 'languageSelector_' + (counter++);
					}
					scope.availableLanguages = ProximisGlobal.availableLanguages;
					scope.required = attrs.required === 'required' || attrs.required === 'true';
				}
			}
		};
	}]);
})();