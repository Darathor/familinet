/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxMaxHeight
	 * @name pxMaxHeight
	 *
	 * @description
	 * A directive to show a text with a max height and a button to view the full text.
	 *
	 * @param {number} pxMaxHeight The max height in pixels.
	 * @param {boolean} customButton Set to `true` to remove the standard buttons.
	 * @param {boolean} deployed Set to `true` to force deploy the content.
	 */
	app.directive('pxMaxHeight', ['$interval', function($interval) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/max-height.twig',
			transclude: true,
			scope: {},
			link: function(scope, elm, attrs) {
				scope.maxHeight = parseInt(attrs['pxMaxHeight'], 10);
				if (isNaN(scope.maxHeight) || scope.maxHeight < 0) {
					scope.maxHeight = -1;
				}

				scope.customButton = attrs['customButton'] === 'true';
				if (scope.customButton) {
					attrs.$observe('deployed', function(value) {
						//noinspection JSValidateTypes
						scope.deployed = value === 'true';
						refreshStyles();
					});
				}

				scope.containerNode = elm.find('.max-height-container');
				scope.contentNode = elm.find('.max-height-content').get(0);
				scope.deployed = false;
				scope.showButtons = false;

				scope.toggle = function() {
					scope.deployed = !scope.deployed;
					refreshStyles();
				};

				var height;
				var checkHeightPromise = $interval(checkHeight, 100);
				scope.$on('$destroy', function() { $interval.cancel(checkHeightPromise); });

				function checkHeight() {
					var newHeight = scope.contentNode.offsetHeight;
					if (height !== newHeight) {
						height = newHeight;

						var newShowButtons;
						if (scope.maxHeight < 0) {
							newShowButtons = false;
						}
						else {
							newShowButtons = height > (scope.maxHeight ? scope.maxHeight + 30 : 0);
						}

						if (scope.showButtons !== newShowButtons || scope.deployed) {
							scope.showButtons = newShowButtons;
							refreshStyles();
						}
					}
				}

				function refreshStyles() {
					if (!scope.showButtons) {
						scope.containerNode.css({ overflow: 'visible', height: 'auto' });
					}
					else if (scope.deployed) {
						scope.containerNode.css({ overflow: 'hidden', height: height + 'px' });
					}
					else {
						scope.containerNode.css({ overflow: 'hidden', height: scope.maxHeight + 'px' });
					}
				}
			}
		};
	}]);
})();