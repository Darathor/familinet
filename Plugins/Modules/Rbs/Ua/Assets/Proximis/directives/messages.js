/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxMessages
	 * @name pxMessages
	 *
	 * @description
	 * A directive showing the messages stored by the service {@link proximisMessages `proximisMessages`}.
	 */
	app.directive('pxMessages', ['proximisMessages', function(proximisMessages) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/messages.twig',

			link: function(scope) {
				scope.messages = function () {
					return proximisMessages.getAll();
				};

				scope.remove = function (message) {
					proximisMessages.remove(message);
				}
			}
		};
	}]);
})();