(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxPageHeader
	 * @name pxPageHeader
	 *
	 * @description
	 * A directive showing a page header.
	 *
	 * @param {string} title The main page title.
	 * @param {string=} subTitle The page subtitle.
	 */
	app.directive('pxPageHeader', function() {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/page-header.twig',
			replace: true,
			scope: true,

			link: function(scope, element, attrs) {
				attrs.$observe('title', function(value) {
					scope.title = value;
				});

				attrs.$observe('subTitle', function(value) {
					scope.subTitle = value;
				});
			}
		};
	});
})();