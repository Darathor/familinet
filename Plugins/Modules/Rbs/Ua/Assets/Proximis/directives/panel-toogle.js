/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxPanelToggle
	 * @name pxPanelToggle
	 *
	 * @description
	 * Make toggle panel to show/hide panel body
	 *
	 * @param {string} pxPanelToggle 'collapsed' to start with the body collapsed, empty else.
	 * @param {boolean=} customToggle If set to `true`:
	 *  * the class `'px-panel-toggle-content'` won't be added automatically to the panel body and should be added manually to the zone to collapse.
	 *  * the class `'px-panel-toggle-collapsed'` won't be automatically toggled on click on the panel heading.
	 */
	app.directive('pxPanelToggle', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.addClass('px-panel-toggle');
				if (attrs['pxPanelToggle'] === 'collapsed') {
					element.addClass('px-panel-toggle-collapsed');
				}

				// The '>' in the selector avoids problems with nested panels.
				var heading = element.find('> .panel-heading .panel-title');
				if (!heading.length) {
					heading = element.find('> .panel-heading');
				}

				if (!attrs['customToggle'] || attrs['customToggle'] !== 'true') {
					element.find('.panel-body').addClass('px-panel-toggle-content');

					heading.bind('click', function() {
						element.toggleClass('px-panel-toggle-collapsed');
					});
				}

				var arrow = angular.element('<em class="glyphicon glyphicon-chevron-up pull-right px-panel-toggle-arrow"></em>');
				heading.append(arrow);
			}
		};
	});
})();