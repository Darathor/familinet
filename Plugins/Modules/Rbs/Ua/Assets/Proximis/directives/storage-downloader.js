/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxStorageDownloader
	 * @name pxStorageDownloader
	 *
	 * @description
	 * A directive to download a file stored in the storage system.
	 *
	 * @param {string} restService The service to retrieve the data.
	 * @param {string=} restParams The REST params.
	 * @param {string=} changeUri The storage URI. Used only if `restParams` is omitted.
	 * @param {string=} fileName The file name.
	 */
	app.directive('pxStorageDownloader', ['proximisRestApi', 'proximisRestApiDefinition', '$timeout', pxStorageDownloader]);

	function pxStorageDownloader(proximisRestApi, proximisRestApiDefinition, $timeout) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Ua/Proximis/directives/storage-downloader.twig',
			scope: {
				restService: '@',
				restParams: "<?",
				changeUri: "<?",
				fileName: "<?"
			},

			link: function(scope, element) {
				scope.downloadUrl = null;
				scope.checkingURL = false;

				scope.onDownload = function() {
					scope.downloadUrl = null;
					scope.checkingURL = true;

					if (!scope.restParams && angular.isString(scope.changeUri) && scope.changeUri.length > 10) {
						var parts = scope.changeUri.replace('change://', '').split('/');
						scope.restParams = { storageName: parts.shift(), relativePath: parts.join('/') };
					}

					var definition = proximisRestApiDefinition.getData(scope.restService, 'GET', scope.restParams);
					proximisRestApi.get(definition).then(
						function(result) {
							if (result.data && result.data.item && result.data.item.temporaryDownload) {
								scope.downloadUrl = result.data.item.temporaryDownload;
								element.append("<iframe src='" + result.data.item.temporaryDownload + "' style='display: none;' ></iframe>");
								$timeout(function() {
									scope.checkingURL = false;
								}, 1000);
							}
						},
						function(result) {
							scope.checkingURL = false;
							console.error(result);
						}
					);
				}
			}
		};
	}
})();