/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxStorageImage
	 * @name pxStorageImage
	 * @element img
	 * @restrict A
	 *
	 * @description
	 * Displays an image stored in the Storage manager.
	 *
	 * @param {number|Object} pxStorageImage The image document id or an object representing the image.
	 * @param {string} format Size of the image (can be one of the formats defined in
	 *     {@link proximis.constant:ProximisImageFormats `ProximisImageFormats`} or a custom size like `57x32`).
	 */
	app.directive('pxStorageImage',
		['proximisRestApi', 'proximisRestApiDefinition', 'ProximisImageFormats', pxStorageImage]);

	function pxStorageImage(proximisRestApi, proximisRestApiDefinition, ProximisImageFormats) {
		return {
			restrict: 'A',
			scope: {
				image: "=pxStorageImage"
			},

			link: function(scope, elm, attrs) {
				// Check if the directive is on an valid tag (<img/> only).
				if (!elm.is('img')) {
					throw new Error('px-storage-image directive must be used on <img/> elements only.');
				}

				var format = (attrs.format || 's').toLowerCase();
				if (ProximisImageFormats.hasOwnProperty(format)) {
					format = ProximisImageFormats[format];
				}

				var maxWidth, maxHeight;
				if (/^\d+x\d+$/.test(format)) {
					var dim = format.split('x');
					maxWidth = dim[0];
					maxHeight = dim[1];
					elm.css({
						'max-width': maxWidth + 'px',
						'max-height': maxHeight + 'px'
					});
				}
				else {
					throw new Error('Invalid format value ("' + attrs.format + '") for directive px-storage-image.');
				}

				scope.$watch('image', function(value) {
					elm.hide();
					if (angular.isObject(value)) {
						handleObject(value);
					}
					else if (angular.isNumber(value)) {
						handleId(value);
					}
				});

				function handleObject(value) {
					if (angular.isObject(value.urls) && value.urls[format]) {
						elm.attr('src', value.urls[format]);
						if (angular.isObject(value.common) && value.common.alt) {
							elm.attr('alt', value.common.alt);
						}
						else {
							elm.removeAttr('alt');
						}
						elm.show();
					}
					else {
						elm.removeAttr('src');
						elm.removeAttr('alt');
					}
				}

				function handleId(value) {
					var params = { imageId: value, maxHeight: maxHeight, maxWidth: maxWidth };
					var requestData = proximisRestApiDefinition.getData('Rbs_Media_Image.displayData', 'GET', params);
					proximisRestApi.get(requestData.path, requestData.params).then(
						function(result) {
							elm.attr('src', result.data.item.publicUrl);
							elm.attr('alt', result.data.item.alt);
							elm.show();
						},
						function(result) {
							console.error('Can\'t retrieve image data in pxStorageImage', result);
						}
					);
				}
			}
		};
	}
})();