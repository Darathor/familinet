/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxThumbnailList
	 * @name pxThumbnailList
	 * @restrict A
	 * @scope
	 *
	 * @param {string} restService The service to retrieve the thumbnails data.
	 * @param {Object=} restParams The service params to retrieve the thumbnails data.
	 * @param {Object=} actionMethods An object providing the actions declared in {@link proximis.directive:pxListAction `pxListAction`} children.
	 *     If an object is passed, some methods will be automatically added to perform actions from containers:
	 *      * `refresh(promise)`: refreshes the list
	 * @param {Object=} filter The filter for the list.
	 * @param {String=} context The display context of the list. Values handled: 'modal', 'default'.
	 *
	 * @description
	 * Displays a list of thumbnails in a grid.
	 *
	 * Events emitted:
	 *   - pxThumbnailList.initialized when list completely initialized (listen filterUpdated event)
	 *   - pxThumbnailList.loaded when collection updated
	 *
	 * Children Directives:
	 *
	 * - {@link proximis.directive:pxThumbnailListVisual `pxThumbnailListVisual`}
	 * - {@link proximis.directive:pxThumbnailListCaption `pxThumbnailListCaption`}
	 * - {@link proximis.directive:pxThumbnailListAction `pxThumbnailListAction`}
	 *
	 * @example
	 * Directive usage:
	 *
	 * ```html
	 *     <div data-px-thumbnail-list="" data-rest-service="..." data-filter="filter">
	 *         <div data-px-thumbnail-list-visual="" data-location="..." data-alt="..." data-tooltip="..."></div>
	 *         <div data-px-thumbnail-list-caption="" data-content="..."></div>
	 *         ...
	 *         <div data-px-thumbnail-list-action="" data-name="..." data-icon="..." data-label="..."></div>
	 *         <div data-px-thumbnail-list-action="" data-name="..." data-icon="..." data-label="..."></div>
	 *     </div>
	 * ```
	 */
	app.directive('pxThumbnailList',
		(['$location', '$timeout', 'proximisRestApiDefinition', 'proximisRestApi', 'proximisMessages', 'proximisCoreI18n', pxThumbnailList]));

	function pxThumbnailList($location, $timeout, proximisRestApiDefinition, proximisRestApi, proximisMessages, proximisCoreI18n) {
		/**
		 * px-thumbnail-list directive.
		 */
		return {
			restrict: 'A',
			transclude: true,
			replace: true,
			scope: {
				actionMethods: '=',
				filter: '=',
				restService: '=',
				restParams: '=',
				context: '@'
			},
			templateUrl: 'Rbs/Ua/Proximis/directives/thumbnail-list.twig',
			controller: ['$scope', function(scope) {
				function handlePromise(promise) {
					promise.then(
						function(result) {
							var title = proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf');
							proximisMessages.success(title, result.data.status.message, true, 10000);
							scope.actionMethods.refresh();
						},
						function(result) {
							var title = proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf');
							proximisMessages.error(title, result.data.status.message, true);
							scope.actionMethods.refresh();
						}
					);
				}

				// Complete actionMethods object to share methods.

				if (angular.isObject(scope.actionMethods)) {
					// Refresh.
					if (!scope.actionMethods.hasOwnProperty('refresh')) {
						scope.actionMethods.refresh = function(promise) {
							if (angular.isObject(promise)) {
								handlePromise(promise);
							}
							else {
								scope.pagination.currentPage = 1;
								scope.refresh();
							}
						}
					}
				}

				// Actions.

				scope.actions = {
					list: [],
					single: [],
					multiple: [],
					execute: function(action, thumbnail) {
						if (angular.isObject(action) && scope.actionMethods.hasOwnProperty(action.name)) {
							var promise = scope.actionMethods[action.name](thumbnail ? [thumbnail] : scope.selection);
							if (angular.isObject(promise)) {
								handlePromise(promise);
							}
						}
					}
				};

				function refreshActionsActivation(refreshSingle) {
					// Multiple actions.
					var selectionLength = scope.selection.length;
					angular.forEach(scope.actions.multiple, function(action) {
						if (!selectionLength) {
							action.isActiveMultiple = false;
							return;
						}
						action.isActiveMultiple = true;

						var method = action.activationMethod;
						if (method) {
							if (angular.isObject(scope.actionMethods) && scope.actionMethods.hasOwnProperty(method)) {
								for (var i = 0; i < selectionLength; i++) {
									if (!scope.actionMethods[method](scope.selection[i])) {
										action.isActiveMultiple = false;
										return;
									}
								}
							}
							else {
								console.warn('[uaEntityList] Activation method not found: ' + method);
								action.isActiveMultiple = false;
							}
						}
					});

					// Single actions.
					if (refreshSingle) {
						var collectionLength = scope.collection.length;
						for (var i = 0; i < collectionLength; i++) {
							var thumbnail = scope.collection[i];
							thumbnail._isActiveSingle = {};
							angular.forEach(scope.actions.single, function(action) {
								var method = action.activationMethod;
								if (!method) {
									thumbnail._isActiveSingle[action.name] = true;
								}
								else if (angular.isObject(scope.actionMethods) && scope.actionMethods.hasOwnProperty(method)) {
									thumbnail._isActiveSingle[action.name] = scope.actionMethods[method](doc);
								}
								else {
									console.warn('[uaEntityList] Activation method not found: ' + method);
									thumbnail._isActiveSingle[action.name] = false;
								}
							});
						}
					}
				}

				// Filters

				var filter = scope.filter;
				if (filter === true) {
					var waitFilterUpdated;
					scope.$on('filterUpdated', function(event, newFilter) {
						if (waitFilterUpdated) {
							$timeout.cancel(waitFilterUpdated);
						}
						waitFilterUpdated = $timeout(function() {
							filter = angular.copy(newFilter);
							scope.refresh();
						}, 300);
					});
				}

				scope.refreshActionsActivation = refreshActionsActivation;

				scope.$on('pxThumbnailListAction.updated', function() {
					refreshActionsActivation(true);
				});

				//
				// Pagination.
				//
				var search = $location.search();
				scope.pagination = {
					enabled: false,
					pageSizes: [6],
					itemsPerPage: search.limit || 6,
					currentPage: (search.page || 1),
					totalItems: 0
				};
				scope.sort = scope.defaultSort = { column: 'id', descending: true };

				scope.$watch('pagination.itemsPerPage', function(value, oldValue) {
					if (value !== oldValue) {
						if(scope.context !== 'modal') {
							$location.search('limit', scope.pagination.itemsPerPage);
						}
						scope.refresh();
					}
				});
				scope.$watch('pagination.currentPage', function(value, oldValue) {
					if (value !== oldValue) {
						if (scope.context !== 'modal') {
							$location.search('page', scope.pagination.currentPage);
						}
						var offset = (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage;
						if (offset !== scope.pagination.offset) {
							scope.refresh();
						}
					}
				});

				//
				// Thumbnail selection.
				//
				scope.selection = [];

				scope.toggleSelection = function(thumbnail) {
					if (scope.actions.multiple.length) {
						thumbnail.selected = !thumbnail.selected;
						if (thumbnail.selected) {
							scope.selection.push(thumbnail);
						}
						else {
							scope.selection.splice(scope.selection.indexOf(thumbnail), 1);
						}
						refreshActionsActivation(false);
					}
				};

				scope.refresh = function() {
					load();
				};

				function load() {
					scope.pagination.loading = true;

					var params = {
						offset: (scope.pagination.currentPage - 1) * scope.pagination.itemsPerPage,
						limit: scope.pagination.itemsPerPage,
						sort: scope.sort.column,
						desc: scope.sort.descending
					};

					if (scope.imageFormats) {
						params.imageFormats = angular.copy(scope.imageFormats);
					}

					if (filter) {
						params.filter = filter;
					}

					if (scope.restParams) {
						angular.extend(params, scope.restParams);
					}

					var requestData = proximisRestApiDefinition.getData(scope.restService, 'GET', params);
					var promise = proximisRestApi.sendData(requestData);
					promise.then(
						function(result) {
							if (result.data !== null) {
								if (result.data.pagination) {
									scope.pagination.enabled = true;
									scope.pagination.offset = result.data.pagination.offset;
									scope.pagination.totalItems = result.data.pagination.count;
									scope.collection = result.data.items;
								}
								else {
									scope.pagination.enabled = false;
									scope.collection = result.data.items;
								}

								// Keep selection while browsing pages.
								angular.forEach(scope.collection, function(thumbnail) {
									thumbnail.selected = scope.selection.find(function(element) { return thumbnail._meta.id === element._meta.id});
								});

								refreshActionsActivation(true);

								scope.$emit('pxThumbnailList.loaded', { collection: scope.collection });
							}
							scope.pagination.loading = false;
						},
						function(result) {
							if (!proximisMessages.displayError(result.data.status, result.status)) {
								var title = proximisCoreI18n.trans('m.rbs.ua.common.error_loading_list | ucf');
								proximisMessages.error(title, proximisErrorFormatter.format(result.data.status));
							}
							scope.pagination.loading = false;
						}
					);
					return promise;
				}

				scope.explicitRefresh = function() {
					scope.$emit('pxThumbnailList.explicitRefresh');
					scope.refresh();
				};

				load();

				scope.$emit('pxThumbnailList.initialized');
			}]
		};
	}

	app.directive('pxThumbnailListItem', ['$compile', function($compile) {
		return {
			link: function(scope, elt, attrs, ctrl, transclude) {
				transclude(function(transEl, transScope) {
					transScope.thumbnail = scope.thumbnail;
					transScope.actions = scope.actions;
					elt.append(transEl);
					$compile(transEl.contents())(transScope);
				});
			}
		};
	}]);

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxThumbnailListImage
	 * @name pxThumbnailListImage
	 * @restrict A
	 *
	 * @param {Object} thumbnail The image to display for the thumbnail.
	 *
	 * @description
	 * Image definition in a {@link proximis.directive:pxThumbnailList `pxThumbnailList`}.
	 */
	app.directive('pxThumbnailListImage', function() {
		return {
			restrict: 'A',
			require: '^pxThumbnailList',
			replace: true,
			template: '<img data-px-storage-image="thumbnail" data-format="(= imageFormat =)" />',
			controller: ['$scope', '$element', '$attrs', function(scope, element, attrs) {
				scope.imageFormat = attrs['imageFormat'];
			}]
		};
	});

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxThumbnailListCaption
	 * @name pxThumbnailListCaption
	 * @restrict A
	 *
	 * @param {string} title the title of the thumbnail caption.
	 * @param {string=} content the content of the thumbnail caption.
	 *
	 * @description
	 * Caption definition in a {@link proximis.directive:pxThumbnailList `pxThumbnailList`}.
	 */
	app.directive('pxThumbnailListCaption', function() {
		return {
			restirct: 'A',
			require: '^pxThumbnailList',
			scope: {
				title: "=",
				caption: '='
			},
			replace: true,
			template: '<div class="thumbnail-list-caption"><h4>(= title =)</h4><div data-ng-if="caption" data-ng-bind-html="caption | pxCoreTrustHtml"></div></div>'
		};
	});

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxThumbnailListAction
	 * @name pxThumbnailListAction
	 * @restrict A
	 *
	 * @param {string} pxListAction The action technical name.
	 * @param {string} label The human readable action's label.
	 * @param {string=} icon The action's icon, in form `icon-xxx` ({@link http://fortawesome.github.io/Font-Awesome/3.2.1/icons/ available icons}).
	 * @param {string=} activationMethod Optional method name to validate a document for this action. This method should be defined in the
	 *     `actionMethods` parameter of the list and will be called with a single `doc` parameter containing the document object to check.
	 * @param {string=} class Optional class(es) to add to the action button.
	 * @param {string=} mode Optional mode: `single`, `multiple`, `both` (`both` by default).
	 *
	 * @description
	 * Action definition in a {@link proximis.directive:pxThumbnailList `pxThumbnailList`}.
	 */
	app.directive('pxThumbnailListAction', ['$filter', function($filter) {
		return {
			restrict: 'A',
			require: '^pxThumbnailList',
			controller: ['$scope', '$element', '$attrs', function(scope, element, attrs) {
				var action = {
					name: attrs['pxThumbnailListAction'] || null,
					label: attrs['label'] || null,
					icon: attrs['icon'] || 'icon-cog',
					mode: scope.$eval(attrs['mode']) || 'both',
					activationMethod: attrs['activationMethod'] || null,
					class: attrs['class'] || 'btn-default'
				};

				if (!action.name) {
					throw new Error('The directive px-thumbnail-list-action requires a value parameter.');
				}
				if (!action.label) {
					throw new Error('The directive px-thumbnail-list-action requires a "label" parameter.');
				}

				var existentActions = $filter('filter')(scope.actions.list, { name: action.name });
				if (!existentActions.length) {
					scope.actions.list.push(action);
					if (action.single = action.mode !== 'multiple') {
						scope.actions.single.push(action);
					}
					if (action.multiple = action.mode !== 'single') {
						scope.actions.multiple.push(action);
					}
					scope.$emit('pxThumbnailListAction.updated');
				}
			}]
		};
	}]);
})();