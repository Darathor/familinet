/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(jQuery) {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc directive
	 * @id proximis.directive:pxVersion
	 * @name pxVersion
	 *
	 * @description
	 * A directive showing the application version.
	 */
	app.directive('pxVersion', ['ProximisGlobal', function(ProximisGlobal) {
		return {
			'restrict': 'A',
			link: function(scope, elm) {
				var versionData = '(AngularJS ' + angular.version.full + ' &mdash; jQuery ' + jQuery.fn.jquery + ')';
				if (ProximisGlobal.application.version !== null) {
					versionData = 'Version ' + ProximisGlobal.application.version + ' ' + versionData;
				}
				elm.html(versionData);
			}
		};
	}]);
})(window.jQuery);