/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxOrderBy
	 * @name pxOrderBy
	 * @function
	 *
	 * @description
	 * A fixed version of the standard orderBy filter, doing nothing if no sortPredicate.
	 *
	 * @param {Array} input The array to sort.
	 * @param {Function|string|Array=} sortPredicate A predicate to be used by the comparator to determine the order of elements.
	 *
	 *    Can be one of:
	 *
	 *    - `Function`: Getter function. The result of this function will be sorted using the `<`, `=`, `>` operator.
	 *    - `string`: An Angular expression. The result of this expression is used to compare elements
	 *      (for example `name` to sort by a property called `name` or `name.substr(0, 3)` to sort by
	 *      3 first characters of a property called `name`). The result of a constant expression
	 *      is interpreted as a property name to be used in comparisons (for example `"special name"`
	 *      to sort object by the value of their `special name` property). An expression can be
	 *      optionally prefixed with `+` or `-` to control ascending or descending sort order
	 *      (for example, `+name` or `-name`). If no property is provided, (e.g. `'+'`) then the array
	 *      element itself is used to compare where sorting.
	 *    - `Array`: An array of function or string predicates. The first predicate in the array
	 *      is used for sorting, but when two items are equivalent, the next predicate is used.
	 *
	 *    If the predicate is missing or empty then it defaults to `'+'`.
	 *
	 * @param {boolean=} reverseOrder Reverse the order of the array.
	 * @returns {Array} Sorted copy of the source array.
	 */
	app.filter('pxOrderBy', ['$filter', function($filter) {
		return function(input, sortPredicate, reverseOrder) {
			if (!sortPredicate) {
				return input;
			}
			return $filter('orderBy')(input, sortPredicate, reverseOrder);
		};
	}]);
})();