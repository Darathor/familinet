/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc filter
	 * @id proximis.filter:pxRoute
	 * @name pxRoute
	 * @function
	 *
	 * @description
	 * Returns the URL for the input Document, Model name or Plugin name.
	 *
	 * @param {Object|string} input A module name, a model name or an object with `id` and `model` properties.
	 * @param {string} routeName Route name.
	 * @param {Object=} params Route parameters.
	 *
	 * @example
	 * ```html
	 *   <a ng-href="(= 'Rbs_Catalog_Product' | pxRoute:'new' =)">Create new product</a>
	 *   <a ng-href="(= product | pxRoute:'edit' =)">Edit (= product.label =)</a>
	 * ```
	 */
	app.filter('pxRoute', ['proximisString', 'proximisRoute', 'ProximisGlobal',
		function (proximisString, proximisRoute, ProximisGlobal) {
			return function(input, routeName, params) {
				var url = false;

				if (angular.isObject(input)) {
					url = proximisRoute.get(input, routeName, params);
				}
				else if (proximisString.isModelName(input) || proximisString.isModuleName(input)) {
					url = proximisRoute.get(input, routeName, params);
				}

				// Remove starting slash.
				if (angular.isString(url)) {
					return (url !== '/') ? url.slice(1) : ProximisGlobal.baseURL;
				}
				return '';
			};
		}
	]);
})();
