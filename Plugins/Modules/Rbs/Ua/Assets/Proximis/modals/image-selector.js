/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	app.controller('ProximisUaModalsImageSelectorController', ['$scope', '$filter', 'proximisRichtextUaCKEditor', 'ProximisImageFormats', 'config',
		'proximisCoreI18n', ProximisUaModalsImageSelectorController]);

	function ProximisUaModalsImageSelectorController(scope, $filter, proximisRichtextUaCKEditor, ProximisImageFormats, config, proximisCoreI18n) {
		scope.config = config;
		scope.config.imageListService = proximisRichtextUaCKEditor.getImageListService();

		scope.restParams = { imageFormats: [ProximisImageFormats.l] };

		scope.staticFilterMethods = {
			label: {
				isValid: function(filter) {
					return filter.parameters && filter.parameters.label;
				},
				compile: function(filter) {
					if (!scope.staticFilterMethods.label.isValid(filter)) {
						return {};
					}
					return {
						name: 'label',
						parameters: { propertyName: 'label', operator: 'contains', value: filter.parameters.label }
					};
				}
			}
		};

		scope.$on('pxThumbnailList.initialized', function() {
			scope.$watch('filter.compiled', function(compiledFilter) {
				if (compiledFilter) {
					scope.$broadcast('filterUpdated', compiledFilter);
				}
			}, true);
		});

		scope.actionMethods = {
			select: function(selection) {
				scope.selectImages(selection);
			}
		};

		scope.buildCaption = function(thumbnail) {
			return $filter('number')(thumbnail.common.width) + ' &times; ' + $filter('number')(thumbnail.common.height) + ' &bull; ' +
				$filter('number')(thumbnail.common.width * thumbnail.common.height / 1000000) +
				' <abbr title="' + proximisCoreI18n.trans('m.rbs.ua.common.megapixels|ucf') + '">' +
				proximisCoreI18n.trans('m.rbs.ua.common.megapixels_abbr') + '</abbr>';
		};

		scope.data = {
			documentIds: []
		};

		scope.selectImages = function(selection) {
			var result = {
				documentIds: []
			};

			angular.forEach(selection, function(thumbnail) {
				result.documentIds.push(thumbnail._meta.id);
			});

			scope.$close({ action: 'insert', data: result });
		};
	}
})();