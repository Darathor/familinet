/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * parentData:
	 *  - filter: an object with a property "compiled".
	 *  - savingContextName: the saving context name.
	 */
	app.controller('ProximisSaveFilterController', ['$scope', 'parentData', 'proximisMessages', 'proximisFilters', 'proximisCoreI18n',
		ProximisSaveFilterController]);

	function ProximisSaveFilterController(scope, parentData, proximisMessages, proximisFilters, proximisCoreI18n) {
		scope.modalData = {
			label: ''
		};

		scope.save = function() {
			if (parentData.filter.compiled && scope.modalData.label) {
				proximisFilters.saveFilter(parentData.savingContextName, parentData.filter.compiled, scope.modalData.label);
			}
			else {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.save_filter_failed|ucf'));
			}
			scope.$close();
		};
	}
})();