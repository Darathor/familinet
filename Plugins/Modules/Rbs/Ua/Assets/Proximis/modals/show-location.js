/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	app.controller('ProximisModalsShowLocationController', ['$scope', 'locationData',
		ProximisModalsShowLocationController]);

	function ProximisModalsShowLocationController(scope, locationData) {
		scope.data = {
			location: locationData
		};
	}
})();