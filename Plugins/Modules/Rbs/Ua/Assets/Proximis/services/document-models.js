/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisDocumentModels
	 * @name proximisDocumentModels
	 *
	 * @description
	 * A service to retrieve existing document models.
	 */
	app.service('proximisDocumentModels', ['$filter', 'ProximisGlobal', 'proximisCoreArray', proximisDocumentModels]);

	function proximisDocumentModels($filter, ProximisGlobal, proximisCoreArray) {
		var allModels = $filter('orderBy')(ProximisGlobal.documentModelDefinitions, ['pluginLabel', 'label']);

		function applyFilter(filter) {
			var models = [];
			if (angular.isString(filter)) {
				filter = { name: [filter] };
			}
			else if (angular.isArray(filter)) {
				filter = { name: filter };
			}
			else if (!angular.isObject(filter)) {
				angular.forEach(allModels, function(testModel) {
					models.push(testModel);
				});
				return;
			}

			angular.forEach(allModels, function(testModel) {
				var valid = true;
				angular.forEach(filter, function(value, attr) {
					if (testModel.hasOwnProperty(attr)) {
						if (angular.isArray(value)) {
							if (angular.isArray(testModel[attr])) {
								if (proximisCoreArray.intersect(testModel[attr], value).length === 0) {
									valid = false;
								}
							}
							else if (!proximisCoreArray.inArray(testModel[attr], value)) {
								valid = false;
							}
						}
						else if (angular.isArray(testModel[attr])) {
							if (!proximisCoreArray.inArray(value, testModel[attr])) {
								valid = false;
							}
						}
						else if (testModel[attr] !== value) {
							valid = false;
						}
					}
					else {
						valid = false;
					}
				});
				if (valid) {
					models.push(testModel);
				}
			});

			return models;
		}

		// Public API.
		return {
			/**
			 * @ngdoc method
			 * @methodOf proximisDocumentModels
			 * @name getAll
			 *
			 * @description Get all document models.
			 */
			getAll: function() {
				return applyFilter();
			},

			/**
			 * @ngdoc method
			 * @methodOf proximisDocumentModels
			 * @name getByFilter
			 *
			 * @description Get a filtered set of models.
			 *
			 * @param {string|array|Object=} filter A model name, a model array or an object describing model filters.
			 *
			 * List of available filters:
			 *  * `publishable` - {@type boolean}
			 *  * `activable` - {@type boolean}
			 *  * `editable` - {@type boolean}
			 *  * `instanceOf` - {@type string}
			 *
			 * **Examples:**
			 *  * `'Rbs_Website_StaticPage'`
			 *  * `['Rbs_Website_StaticPage', 'Rbs_Website_Menu']`
			 *  * `{ publishable: true }`
			 *  * `{ publishable: true, instanceOf: 'Rbs_Website_Page' }`
			 */
			getByFilter: function(filter) {
				return applyFilter(filter);
			}
		};
	}
})();