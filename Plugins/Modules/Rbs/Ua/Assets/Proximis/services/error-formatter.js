/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc provider
	 * @id proximis.service:proximisErrorFormatterProvider
	 * @name proximisErrorFormatterProvider
	 *
	 * @description
	 * Use `proximisErrorFormatterProvider` to add some error handler to `proximisErrorFormatter` service.
	 */
	app.provider('proximisErrorFormatter', proximisErrorFormatterProvider);

	function proximisErrorFormatterProvider() {
		var errorHandlers = {
			'INVALID-LCID': function(data) {
				return data.message;
			},

			'UPDATE-ERROR': function(data) {
				return data.message;
			},

			'default': function(data) {
				//console.warn("No error handler defined for error '" + data.errorCode + "': using default message.");
				return (data.errorCode ? ('[' + data.errorCode + '] ') : '') + data.message;
			}
		};

		function addErrorHandler(errorCode, handler) {
			errorHandlers[errorCode] = handler;
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisErrorFormatterProvider
		 * @name addErrorHandler
		 *
		 * @description
		 * Adds and error handler to
		 *
		 * @param {string} errorCode The error code to handle.
		 * @param {Function} handler The handler, with two parameters:
		 * <ul>
		 *     <li>`data` - {@type Object} - The REST service error response.</li>
		 *     <li>`context` - {@type Object}</li>
		 * </ul>
		 */
		this.addErrorHandler = addErrorHandler;

		/**
		 * @ngdoc service
		 * @id proximis.service:proximisErrorFormatter
		 * @name proximisErrorFormatter
		 *
		 * @description
		 * The error formatter service.
		 */
		this.$get = ['proximisCoreI18n', function(proximisCoreI18n) {
			addErrorHandler('EXCEPTION-72002', function() {
				return proximisCoreI18n.trans('m.rbs.ua.common.error_formatter_exception_72002 | ucf');
			});

			return {
				/**
				 * @ngdoc method
				 * @methodOf proximisErrorFormatter
				 * @name format
				 *
				 * @description
				 * Sets the base URL for the proximisRestApi service.
				 *
				 * @param {Object} errorResponse The REST service error response.
				 * @param {Object=} context The handler, with two parameters.
				 * @returns {string} The error, formatted as an HTML string.
				 */
				format: function(errorResponse, context) {
					var handler = angular.isFunction(errorHandlers[errorResponse.errorCode]) ? errorResponse.errorCode : 'default';
					return errorHandlers[handler](errorResponse, context);
				}
			};
		}];
	}
})();