/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisFilters
	 * @name proximisFilters
	 *
	 * @description
	 * Displays messages to the user.
	 */
	app.service('proximisFilters', ['proximisCoreI18n', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisMessages',
		proximisFilters]);

	function proximisFilters(proximisCoreI18n, proximisRestApi, proximisRestApiDefinition, proximisMessages) {
		var filtersByKey = {};

		function getKey(contextName) {
			if (!contextName || !angular.isString(contextName)) {
				throw 'Missing context name!';
			}
			return contextName.replace(/\//g, '_');
		}

		function load(key) {
			if (!filtersByKey.hasOwnProperty(key)) {
				filtersByKey[key] = [];
				proximisRestApi.sendData(proximisRestApiDefinition.getData('common.filter.list', 'GET', { contextName: key })).then(
					function(result) {
						angular.forEach(result.data.items, function(filter) {
							filtersByKey[key].push(filter);
						});
					},
					function(result) {
						console.error('Unable to load filters.', result);
						var title = proximisCoreI18n.trans('m.rbs.ua.common.load_filter_failed|ucf');
						proximisMessages.error(title, result.data.status.message, 'proximisFilters.load.error');
					}
				);
			}
			return filtersByKey[key];
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisFilters
		 * @name getFilters
		 *
		 * @description
		 * Get the saved filters for the current route.
		 *
		 * @param {string} contextName The context name.
		 * @returns {Array} The filters for the current route.
		 */
		this.getFilters = function(contextName) {
			return load(getKey(contextName));
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisFilters
		 * @name saveFilter
		 *
		 * @description
		 * Saves a filter in the local storage.
		 *
		 * @param {string} contextName The context name.
		 * @param {Object} content The compiled filter data.
		 * @param {string} label The filter's label.
		 * @returns {Promise}
		 */
		this.saveFilter = function(contextName, content, label) {
			var key = getKey(contextName);
			var filters = load(key);
			var params = { contextName: key, content: content, label: label };
			var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('common.filter.new', 'POST', params));
			promise.then(
				function(result) {
					filtersByKey[key].push(result.data.item);
					var title = proximisCoreI18n.trans('m.rbs.ua.common.add_filter_success|ucf');
					proximisMessages.success(title, null, 'proximisFilters.saveFilter.success', 5000);
				},
				function(result) {
					console.error('Unable to load filters.', result);
					var title = proximisCoreI18n.trans('m.rbs.ua.common.add_filter_failed|ucf');
					proximisMessages.error(title, result.data.status.message, 'proximisFilters.saveFilter.error');
				}
			);
			return promise;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisFilters
		 * @name deleteFilter
		 *
		 * @description
		 * Delete filter by index.
		 *
		 * @param {string} contextName The context name.
		 * @param {Object} index The filter index.
		 * @returns {Promise}
		 */
		this.deleteFilter = function(contextName, index) {
			var key = getKey(contextName);
			var filters = load(key);
			var filter = filters[index];
			var params = { contextName: key, filterId: filter ? filter.id : 0 };
			var promise = proximisRestApi.sendData(proximisRestApiDefinition.getData('common.filter.detail', 'DELETE', params));
			promise.then(
				function(result) {
					filters.splice(index, 1);
					proximisMessages.success(result.data.status.message, null, 'proximisFilters.deleteFilter.success', 5000);
				},
				function(result) {
					console.error('Unable to delete filter.', result);
					var title = proximisCoreI18n.trans('m.rbs.ua.common.delete_filter_failed|ucf');
					proximisMessages.error(title, result.data.status.message, 'proximisFilters.deleteFilter.error');
				}
			);
			return promise;
		};
	}
})();