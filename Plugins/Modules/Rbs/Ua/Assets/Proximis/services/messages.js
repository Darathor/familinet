/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc service
	 * @id proximis.service:proximisMessages
	 * @name proximisMessages
	 *
	 * @description
	 * Displays messages to the user.
	 */
	app.service('proximisMessages', ['$rootScope', '$timeout', 'proximisCoreArray', 'proximisCoreI18n', 'proximisString', proximisMessages]);
	function proximisMessages($rootScope, $timeout, proximisCoreArray, proximisCoreI18n, proximisString) {
		var lastAutoId = 1;
		var messages = [];

		/**
		 * @param {string} id
		 * @returns {Object|null}
		 */
		function getMessageById(id) {
			for (var i = 0; i < messages.length; i++) {
				if (messages[i].id === id) {
					return messages[i];
				}
			}
			return null;
		}

		function pushMessage(message) {
			message.duplicateCount = 0;
			message.level = message.level || 'info';

			if (angular.isString(message.body)) {
				message.body = [message.body];
			}

			if (message.id === true) {
				message.id = 'm' + proximisString.hashCode(message.level + ' ' + message.title + ' ' + message.body);
			}
			else if (!message.id) {
				message.id = 'auto-' + lastAutoId++;
			}

			var existingMessage = getMessageById(message.id);
			if (existingMessage) {
				message.duplicateCount = existingMessage.duplicateCount + 1;
				removeMessage(existingMessage);
			}

			if (message.timeout) {
				message.timeoutId = $timeout(function() { removeMessage(message); }, message.timeout);
			}

			messages.push(message);
		}

		function removeMessage(message) {
			if (message.timeoutId) {
				$timeout.cancel(message.timeoutId);
			}
			proximisCoreArray.removeValue(messages, message);
		}

		function clearMessages() {
			var oldMessages = angular.copy(messages);
			proximisCoreArray.clear(messages);
			angular.forEach(oldMessages, function(message) {
				if (message.preserveOnNextRouteChange) {
					message.preserveOnNextRouteChange = false;
					pushMessage(message);
				}
			});
		}

		/**
		 * When the route changes, we need to clean up any cascading process.
		 */
		$rootScope.$on('$routeChangeSuccess', function() {
			clearMessages();
		});

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name info
		 *
		 * @description
		 * Displays an informational message to the user.
		 *
		 * @param {string} title The message's title.
		 * @param {string|null|Array=} body The message's main contents.
		 * @param {string|boolean=} id Multiple messages with the same ids will only be displayed once.
		 *     If set to `true` an id will be automatically generated as a hash of the title and message.
		 * @param {number=} timeout Timeout in millisecond after which the message is removed from the screen.
		 * @param {boolean=} preserveOnNextRouteChange Set to true to preserve the message on next route change.
		 */
		this.info = function(title, body, id, timeout, preserveOnNextRouteChange) {
			pushMessage({
				'title': title,
				'body': body,
				'level': 'info',
				'id': id,
				'timeout': timeout,
				'preserveOnNextRouteChange': preserveOnNextRouteChange === true
			});
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name success
		 *
		 * @description
		 * Displays a success message to the user.
		 *
		 * @param {string} title The message's title.
		 * @param {string|null|Array=} body The message's main contents.
		 * @param {string|boolean=} id Multiple messages with the same ids will only be displayed once.
		 *     If set to `true` an id will be automatically generated as a hash of the title and message.
		 * @param {number=} timeout Timeout in millisecond after which the message is removed from the screen.
		 * @param {boolean=} preserveOnNextRouteChange Set to true to preserve the message on next route change.
		 */
		this.success = function(title, body, id, timeout, preserveOnNextRouteChange) {
			pushMessage({
				'title': title,
				'body': body,
				'level': 'success',
				'id': id,
				'timeout': timeout,
				'preserveOnNextRouteChange': preserveOnNextRouteChange === true
			});
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name warning
		 *
		 * @description
		 * Displays a warning message to the user.
		 *
		 * @param {string} title The message's title.
		 * @param {string|Array=} body The message's main contents.
		 * @param {string|boolean=} id Multiple messages with the same ids will only be displayed once.
		 *     If set to `true` an id will be automatically generated as a hash of the title and message.
		 * @param {number=} timeout Timeout in millisecond after which the message is removed from the screen.
		 * @param {boolean=} preserveOnNextRouteChange Set to true to preserve the message on next route change.
		 */
		this.warning = function(title, body, id, timeout, preserveOnNextRouteChange) {
			pushMessage({
				'title': title,
				'body': body,
				'level': 'warning',
				'id': id,
				'timeout': timeout,
				'preserveOnNextRouteChange': preserveOnNextRouteChange === true
			});
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name error
		 *
		 * @description Displays an error message to the user.
		 *
		 * @param {string} title The message's title.
		 * @param {string|Array=} body The message's main contents.
		 * @param {string|boolean=} id Multiple messages with the same ids will only be displayed once.
		 *     If set to `true` an id will be automatically generated as a hash of the title and message.
		 * @param {number=} timeout Timeout in millisecond after which the message is removed from the screen.
		 * @param {boolean=} preserveOnNextRouteChange Set to true to preserve the message on next route change.
		 */
		this.error = function(title, body, id, timeout, preserveOnNextRouteChange) {
			pushMessage({
				'title': title,
				'body': body,
				'level': 'error',
				'id': id,
				'timeout': timeout,
				'preserveOnNextRouteChange': preserveOnNextRouteChange === true
			});
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name remove
		 *
		 * @description
		 * Removes the message.
		 *
		 * @param {Object} message The message to be removed.
		 */
		this.remove = removeMessage;

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name clearMessages
		 *
		 * @description
		 * Clear all messages
		 *
		 */
		this.clearMessages = clearMessages;

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name getAll
		 *
		 * @description
		 * Get all messages.
		 *
		 * @returns {Array} The messages.
		 */
		this.getAll = function() {
			return messages;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisMessages
		 * @name displayError
		 *
		 * @description
		 * Display standard error message
		 *
		 * @param {Object=} data The error data.
		 * @param {int} httpStatus The HTTP status code.
		 * @returns {boolean} return true if standard message displayed
		 */
		this.displayError = function(data, httpStatus) {
			if (httpStatus === 403) {
				this.error(
					proximisCoreI18n.trans('m.rbs.ua.common.error_forbidden_title|ucf'),
					proximisCoreI18n.trans('m.rbs.ua.common.error_forbidden_body|ucf'),
					'error_403'
				);
				return true;
			}
			else if (httpStatus === 401) {
				if (angular.isObject(data) && data.errorCode) {
					if (data.errorCode === 'EXCEPTION-72005') {
						var seconds = data.data.delay, messageBody;
						if (seconds > 0) {
							messageBody = proximisCoreI18n.trans('m.rbs.ua.common.error_invalid_positive_delay|ucf').replace('$SECONDS$', seconds);
						}
						else {
							messageBody = proximisCoreI18n.trans('m.rbs.ua.common.error_invalid_negative_delay|ucf').replace('$SECONDS$', -seconds);
						}
						this.error(proximisCoreI18n.trans('m.rbs.ua.common.error_invalid_timestamp_title|ucf'), messageBody, data.errorCode);
					}
					else if (data.errorCode === 'EXCEPTION-72000' || data.errorCode === 'EXCEPTION-72001' || data.errorCode === 'EXCEPTION-72004') {
						this.error(
							proximisCoreI18n.trans('m.rbs.ua.common.error_invalid_oauth_data_title|ucf'),
							proximisCoreI18n.trans('m.rbs.ua.common.error_invalid_oauth_data_body|ucf'),
							data.errorCode
						);
					}
				}
				return true;
			}
			else if (httpStatus === 500) {
				if (angular.isObject(data) && data.errorCode && data.message) {
					this.error(
						proximisCoreI18n.trans('m.rbs.ua.common.error_500_title|ucf'),
						proximisCoreI18n.trans('m.rbs.ua.common.error_500_body_message|ucf').replace('$MESSAGE$', data.message),
						data.errorCode
					);
				}
				else {
					this.error(
						proximisCoreI18n.trans('m.rbs.ua.common.error_500_title|ucf'),
						proximisCoreI18n.trans('m.rbs.ua.common.error_500_body|ucf'),
						(angular.isObject(data) && data.errorCode) ? data.errorCode : 'error_500'
					);
				}
				return true;
			}
			return false;
		};
	}
})();