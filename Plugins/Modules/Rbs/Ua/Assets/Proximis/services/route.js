/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	/**
	 * @ngdoc provider
	 * @id proximis.provider:proximisRouteProvider
	 * @name proximisRouteProvider
	 *
	 * @description
	 * Use `proximisRouteProvider` to configure routes and default module for `proximisRoute`.
	 */
	app.provider('proximisRoute', ['$routeProvider', proximisRouteProvider]);

	function proximisRouteProvider($routeProvider) {
		var routes = {};
		var ROUTES_DEFAULT_MODULE;

		function registerRoute(key, name, path) {
			if (!routes.hasOwnProperty(key)) {
				routes[key] = {};
			}
			routes[key][name] = path;
		}

		/**
		 * @ngdoc method
		 * @methodOf proximisRouteProvider
		 * @name setDefaultModule
		 *
		 * @description
		 * Sets the default module for proximisRoute service.
		 *
		 * @param {string} defaultModule The default module (e.g.: 'Rbs_Admin').
		 */
		this.setRoutesDefaultModule = function(defaultModule) {
			ROUTES_DEFAULT_MODULE = defaultModule;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRouteProvider
		 * @name applyRoutes
		 *
		 * @description
		 * Register routes for the proximisRoute service.
		 *
		 * @param {Object} routes The routes to apply.
		 */
		this.applyRoutes = function(routes) {
			angular.forEach(routes, function(route, path) {
				if (route.hasOwnProperty('rule')) {
					var rule = route.rule;
					var key = route.model || route.module;
					var name = route.name;
					if (!key || !name && (!rule.hasOwnProperty('redirectTo') || !rule.hasOwnProperty('templateUrl'))) {
						return;
					}

					registerRoute(key, name, path);

					rule.ruleName = name;
					rule.relatedModelName = key;

					if (route.hasOwnProperty('options')) {
						rule.options = route.options;
					}
					if (!rule.hasOwnProperty('redirectTo') && !rule.hasOwnProperty('reloadOnSearch')) {
						rule.reloadOnSearch = false;
					}
					$routeProvider.when(path, rule);
				}
			});

			$routeProvider.otherwise({ redirectTo: '/404' });
		};

		/**
		 * @ngdoc service
		 * @id proximis.service:proximisRoute
		 * @name proximisRoute
		 *
		 * @description
		 * Provides methods to get different URLs for the Backoffice UI.
		 */
		this.$get = ['$log', function($log) {
			var defaultParameters = {};

			function getRoute(target, name) {
				var key;
				if (angular.isString(target)) {
					key = target;
				}
				else if (angular.isObject(target) && angular.isDefined(target.model)) {
					key = target.model;
				}
				else {
					throw new Error('Could not determine the Model of Module of the given parameter: ' + target +
						'. Please provide a Module name (string), Model name (string) or a Document object.');
				}

				if (routes.hasOwnProperty(key) && name && routes[key].hasOwnProperty(name)) {
					return routes[key][name];
				}
				return null;
			}

			function replaceParams(urlTpl, routeParams, queryStringParams) {
				var tplParamRegexp = /:([a-z]+)/gi, tplParams = [], result;
				while (result = tplParamRegexp.exec(urlTpl)) {
					tplParams.push(result[1]);
				}

				angular.forEach(tplParams, function(paramName) {
					var v = '';
					if (routeParams.hasOwnProperty(paramName)) {
						v = routeParams[paramName];
						if (angular.isObject(v) && v.id) {
							v = v.id;
						}
						// Do NOT set a parameter in the query string if it has already been used in the route.
						if (queryStringParams.hasOwnProperty(paramName)) {
							delete queryStringParams[paramName];
						}
					}
					else {
						$log.error(paramName + ' can not be replaced in URL tpl ' + urlTpl);
					}
					urlTpl = urlTpl.replace(new RegExp(':' + paramName, 'g'), v);
				});

				urlTpl = urlTpl.replace(/\/+/g, '/');

				return makeUrl(urlTpl, queryStringParams);
			}

			/**
			 * Returns the URL of the given Document or Model name.
			 *
			 * @description
			 * If a Document is provided, the parameters in the URL templates are replaced to return
			 * the full URL ready to be used.
			 * If a Model name is provided, the returned String is the URL template, with parameters,
			 * as defined in the Module's configuration.
			 *
			 * @returns {string|boolean}
			 */
			function getNamedRoute(target, name, parameters) {
				var route = getRoute(target, name);
				if (route === null) {
					return false;
				}

				var params = angular.extend({}, parameters);

				// If `target` is an object, we try to replace the parameters in the `url` with
				// the corresponding properties of the object.
				if (angular.isObject(target)) {
					route = replaceParams(route, angular.extend({}, defaultParameters, target, params), params);
				}
				else {
					route = replaceParams(route, angular.extend({}, defaultParameters, params), params);
				}

				return route;
			}

			/**
			 * Complete the given `url` by adding `queryParams`.
			 *
			 * @param {string} url The URL to use. This URL is supposed to not have params yet.
			 * @param {Object} queryParams Hash object representing the parameters to append to the URL.
			 * @returns {string} The updated URL.
			 */
			function makeUrl(url, queryParams) {
				function convertValue(value) {
					if (angular.isDate(value)) {
						value = moment(value).format();
					}
					return encodeURIComponent(value);
				}

				var query = [];
				if (angular.isObject(queryParams)) {
					angular.forEach(queryParams, function(value, key) {
						if (angular.isDefined(value) && value !== null) {
							if (angular.isArray(value)) {
								for (var p = 0; p < value.length; p++) {
									query.push(key + '[]=' + convertValue(value[p]));
								}
							}
							else if (angular.isObject(value)) {
								angular.forEach(value, function(v, i) {
									query.push(key + '[' + i + ']=' + convertValue(v));
								});
							}
							else {
								query.push(key + '=' + convertValue(value));
							}
						}
					});
				}

				if (query.length) {
					url += '?' + query.join('&');
				}
				return url;
			}

			// Public API.
			return {
				/**
				 * @ngdoc method
				 * @methodOf proximisRoute
				 * @name get
				 *
				 * @description
				 * Returns the URL of the view that displays the editor of the given `doc`.
				 *
				 * @param {string|Object} target A module name, a model name or an object with `id` and `model` properties.
				 * @param {string=} name The route name.
				 * @param {Object=} params Parameters to append in the URL.
				 *
				 * @returns {string|bool}
				 */
				get: function(target, name, params) {
					return getNamedRoute(target, name, params);
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRoute
				 * @name setDefaultParameter
				 *
				 * @description
				 * Sets the value of a default parameter.
				 *
				 * @param {string} name The parameter name.
				 * @param {*} value The parameter value.
				 */
				setDefaultParameter: function(name, value) {
					if (angular.isString(name) && name.length) {
						if (angular.isUndefined(value)) {
							if (defaultParameters.hasOwnProperty(name)) {
								delete defaultParameters[name];
							}
						}
						else {
							defaultParameters[name] = value;
						}
					}
					else {
						throw 'The argument "name" should be a non-empty string.';
					}
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRoute
				 * @name getDefaultParameter
				 *
				 * @description
				 * Get the value of a default parameter.
				 *
				 * @param {string} name The parameter value.
				 */
				getDefaultParameter: function(name) {
					if (angular.isString(name) && name.length) {
						if (defaultParameters.hasOwnProperty(name)) {
							return defaultParameters[name];
						}
						return null;
					}
					else {
						throw 'The argument "name" should be a non-empty string.';
					}
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRoute
				 * @name clearDefaultParameters
				 *
				 * @description
				 * Clears the default parameters.
				 */
				clearDefaultParameters: function() {
					defaultParameters = {};
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRoute
				 * @name getRoutesDefaultModule
				 *
				 * @description
				 * Return default module name.
				 *
				 * @returns {string|null}
				 */
				getRoutesDefaultModule: function() {
					return ROUTES_DEFAULT_MODULE;
				}
			};
		}];
	}
})();
