/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximis');

	app.controller('ProximisAuthorizeController', ['$window', '$rootScope', '$routeParams', '$scope', '$element',
		'ProximisRestGlobal', 'ProximisRestEvents', 'proximisRestApi', 'proximisRestOAuth', 'proximisMessages',
		function($window, $rootScope, $routeParams, scope, element, ProximisRestGlobal, ProximisRestEvents, proximisRestApi, proximisRestOAuth, proximisMessages) {

			scope.submit = function() {
				var params = {
					oauth_token: $routeParams.oauth_token,
					realm: ProximisRestGlobal.OAuth.realm,
					login: scope.login,
					password: scope.password,
					device: scope.device
				};
				proximisRestApi.post('OAuth/Authorize', params).then(
					function(result) {
						var data = result.data;
						/** @var {string} data.error */
						if (data.error) {
							proximisMessages.error(data.error, null, true);
						}
						else {
							proximisRestOAuth.getAccessToken(data.oauth_token, data.oauth_verifier, data.oauth_callback);
						}
					},
					function(result) {
						console.error(result);
					}
				);
			};

			var parser = new UAParser();
			var ua = parser.getResult();

			scope.device = ua.browser.name + ' ' + element.attr('data-on-device') + ' ' + ua.os.name;

			$rootScope.$broadcast(ProximisRestEvents.AuthorizeLoaded);
		}
	]);
})();
