/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreBoolean
	 * @name pxCoreBoolean
	 *
	 * @description
	 * Formats a Boolean value with localized <em>yes</em> or <em>no</em>.
	 *
	 * @param {boolean} value The boolean value to format.
	 */
	app.filter('pxCoreBoolean', ['proximisCoreI18n', function(proximisCoreI18n) {
		return function(input) {
			return proximisCoreI18n.trans(input ? 'c.types.yes' : 'c.types.no');
		};
	}]);
})();