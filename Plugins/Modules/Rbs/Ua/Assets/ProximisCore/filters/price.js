/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCorePrice
	 * @name pxCorePrice
	 *
	 * @description
	 * Format price by currency.
	 *
	 * @param {number} input A value to format.
	 * @param {Object|string} currency An object containing 'code' (ex: `{code: 'EUR'}` ) or the string for the code (ex: `'EUR'`).
	 * @param {number=} fractionSize The fraction size (deducted from the locale if omitted).
	 *
	 * @example
	 * ```html
	 *   <span>(= 10.55 | pxCorePrice:'EUR' =)</span>
	 *   <span>(= 10.55 | pxCorePrice:{code: 'EUR'} =)</span>
	 * ```
	 */
	app.filter('pxCorePrice', ['$filter', 'proximisIntlFormatter', function($filter, proximisIntlFormatter) {
		var formatters = {};
		return function(input, currency, fractionSize) {
			if (!angular.isNumber(input)) {
				return input;
			}
			fractionSize = angular.isNumber(fractionSize) ? fractionSize : undefined;

			var currencyCode;
			if (angular.isObject(currency)) {
				currencyCode = currency.code;
			}
			else if (angular.isString(currency)) {
				currencyCode = currency;
			}
			else {
				console.error('[pxCorePrice] invalid currency', currency);
			}

			var key = currencyCode + '-' + fractionSize;
			if (!formatters.hasOwnProperty(key)) {
				var params = { style: 'currency', currency: currencyCode };
				if (fractionSize !== undefined) {
					params.minimumFractionDigits = fractionSize;
					params.maximumFractionDigits = fractionSize;
				}
				formatters[key] = proximisIntlFormatter.getNumberFormatter(params);
			}
			return formatters[key].format(input);
		};
	}]);
})();