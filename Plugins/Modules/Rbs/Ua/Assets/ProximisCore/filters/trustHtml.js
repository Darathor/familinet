/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisCore');

	/**
	 * @ngdoc filter
	 * @id proximisCore.filter:pxCoreTrustHtml
	 * @name pxCoreTrustHtml
	 *
	 * @description
	 * A simple wrapper for `$sce.trustAsHtml`.
	 *
	 * @param {string} html The string to display.
	 *
	 * @example
	 * ```html
	 *     <div data-ng-bind-html="myVar | pxCoreTrustHtml"></div>
	 * ```
	 */
	app.filter('pxCoreTrustHtml', ['$sce', function($sce) {
		return function(html) {
			return $sce.trustAsHtml(html);
		};
	}]);
})();