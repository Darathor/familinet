/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	// Initialize Intl.
	__change.RBS_Ua_Intl.initialize(__change.LCID);

	/**
	 * @ngdoc module
	 * @name proximisCore
	 *
	 * @description
	 * The ProximisCore module contains basic components.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisCore` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisCore` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxCore`
	 */
	var app = angular.module('proximisCore', ['proximisIntl']);

	// Configuration.

	app.config(['$compileProvider', 'ProximisCoreGlobal', function($compileProvider, ProximisCoreGlobal) {
		if (!ProximisCoreGlobal.angularDevMode) {
			$compileProvider.debugInfoEnabled(false);
			if (ProximisCoreGlobal.devMode) {
				console.warn(
					'Development mode is disabled in AngularJS.' +
					' To enable it, set Change/Presentation/AngularDevMode to true in the project configuration.' +
					'\nMore info here: https://code.angularjs.org/' + angular.version.full + '/docs/guide/production#disabling-debug-data'
				);
			}
		}
	}]);

	// Constants.

	/**
	 * @ngdoc constant
	 * @id proximisCore.constant:ProximisCoreGlobal
	 * @name ProximisCoreGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {boolean} angularDevMode True if the angular development mode should be enabled.
	 * @property {string} LCID The current UI LCID.
	 * @property {Object} i18n The i18n keys declaration. Do not access it directly, use the {@link proximisCore.service:proximisCoreI18n `proximisCoreI18n`} service.
	 */
	app.constant('ProximisCoreGlobal', __change);
})(window.__change);