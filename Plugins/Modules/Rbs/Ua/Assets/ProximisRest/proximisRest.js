/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisRest
	 *
	 * @description
	 * The ProximisRest module contains all components for rest api
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisRest` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisRest` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxRest`
	 */
	var app = angular.module('proximisRest', ['LocalStorageModule']);

	/**
	 * @ngdoc constant
	 * @id proximisRest.constant:ProximisRestEvents
	 * @name ProximisRestEvents
	 *
	 * @description
	 * Contains the name of the main standard events thrown or listened by the ProximisRest module.
	 *
	 * @property {string} AuthenticationSuccess
	 *     This event is broadcasted on the `$rootScope` when a the authentication succeeds.
	 *     This event has no parameter.
	 *
	 * @property {string} AuthorizeLoaded
	 *     This event can be emitted on the `$rootScope` when authorize form controller loaded
	 *     This event has no parameter.
	 *
	 * @property {string} Logout
	 *     This event can be emitted on the `$rootScope` to provoke a logout.
	 *
	 * @property {string} Unauthorized
	 *     This event can be emitted on the `$rootScope` on 401 http request status.
	 *
	 */
	app.constant('ProximisRestEvents', {
		AuthenticationSuccess: 'ProximisRest:AuthenticationSuccess',
		AuthorizeLoaded: 'ProximisRest:AuthorizeLoaded',
		Logout: 'ProximisRest:Logout',
		Unauthorized: 'ProximisRest:Unauthorized',
		RequestTokenSuccess: 'ProximisRest:RequestTokenSuccess',
		RequestTokenFailed: 'ProximisRest:RequestTokenFailed'
	});

	/**
	 * @ngdoc constant
	 * @id proximisRest.constant:ProximisRestGlobal
	 * @name ProximisRestGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {string} baseURL The base URL for interface files.
	 * @property {string} restURL The base URL for REST services.
	 * @property {string} LCID The current UI LCID.
	 * @property {Object} restApi The REST API definition. Do not access it directly, use the {@link proximisRest.service:proximisRestApiDefinition `proximisRestApiDefinition`} service.
	 * @property {Object} OAuth The OAuth parameters. Do not access it directly, use the {@link proximisRest.service:proximisRestOAuth `proximisRestOAuth`} service.
	 * @config {string} consumer_key
	 * @config {string} consumer_secret
	 */
	app.constant('ProximisRestGlobal', __change);

})(window.__change);