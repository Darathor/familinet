﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, change) {
	'use strict';

	var app = angular.module('proximisRichtext');

	var basePath = (change.navigationContext && change.navigationContext.assetBasePath) ? change.navigationContext.assetBasePath : '/Assets/';
	var pluginAssetsPath = basePath + 'Rbs/Ua/ProximisRichtext/CKEditor/proximisAnchor/';
	var HiDPI = CKEditor.env.hidpi ? 'hidpi/' : '';

	CKEditor.skin.addIcon('proximisAnchor', pluginAssetsPath + 'icons/' + HiDPI + 'proximisAnchor.png');
	CKEditor.skin.addIcon('proximisRemoveAnchor', pluginAssetsPath + 'icons/' + HiDPI + 'proximisRemoveAnchor.png');

	/**
	 * The following code is largely inspired by/copied from the native link module of CKEditor.
	 */
	app.run(['$timeout', 'proximisCoreI18n', 'proximisRichtextCKEditor',
		function($timeout, proximisCoreI18n, proximisRichtextCKEditor) {
			var anchorSelector = 'span[class="anchor"]';
			var anchorCondition = function(el) { return (el.is ? el.is('span') : el.getName() === 'span') && el.hasClass('anchor'); };

			function getExistingAnchors(editor, exclude) {
				var anchors = [];
				var editable = editor.editable();

				// The scope of search for anchors is the entire document for inline editors
				// and editor's editable for classic editor/divarea (http://dev.ckeditor.com/ticket/11359).
				var container = ( editable.isInline() && !editor.plugins.divarea ) ? editor.document : editable;

				// Retrieve all anchors within the scope.
				var anchorNodes = container.getElementsByTag('span');
				var anchorNodesCount = anchorNodes.count();
				for (var i = 0; i < anchorNodesCount; i++) {
					var item = anchorNodes.getItem(i++);
					var anchor = item.getAttribute('id');
					if (anchor && (!exclude || exclude !== anchor)) {
						anchors.push(anchor);
					}
				}

				// Retrieve all "fake anchors" within the scope.
				var images = container.getElementsByTag('img');
				var imagesCount = images.count();
				for (i = 0; i < imagesCount; i++) {
					item = images.getItem(i++);
					anchor = item.getAttribute('id');
					if (anchor && (!exclude || exclude !== anchor)) {
						anchors.push(anchor);
					}
				}
				return anchors;
			}

			function getSelectedElements(editor) {
				var selection = editor.getSelection();
				var ranges = selection.getRanges();
				var anchors = [];
				for (var i = 0; i < ranges.length; i++) {
					var range = selection.getRanges()[i];
					// Skip bogus to cover cases of multiple selection inside tables (#tp2245).
					range.shrink(CKEditor.SHRINK_TEXT, false, { skipBogus: true });
					var anchor = editor.elementPath(range.getCommonAncestor()).contains(anchorCondition, 1);
					if (anchor) {
						anchors.push(anchor);
					}
				}
				return anchors;
			}

			function applyAnchors(editor, anchors, result) {
				if (result.action === 'save') {
					var data = result.data;
					var attributes = {
						id: data.name,
						class: 'anchor'
					};

					if (!anchors.length) {
						var style = new CKEditor.style({ element: 'span', attributes: attributes });
						style.type = CKEditor.STYLE_INLINE; // need to override... dunno why.

						var ranges = editor.getSelection().getRanges();
						var range = ranges[0];
						if (range) {
							// Editable anchors nested within current range should be removed, so that the anchor is applied to whole selection.
							var nestedAnchors = range._find(anchorSelector);
							for (var j = 0; j < nestedAnchors.length; j++) {
								nestedAnchors[j].remove(true);
							}

							// If there is no selected text, insert the anchorText.
							if (range.collapsed && data.anchorText) {
								var textElement = new CKEditor.dom.text(data.anchorText, editor.document);
								range.insertNode(textElement);
								range.selectNodeContents(textElement);
							}

							// Apply style.
							style.applyToRange(range, editor);
							range.select();

							// Fix the first created anchor's id and remove the other anchors.
							range.enlarge(CKEditor.ENLARGE_INLINE); // Without that, the first anchor is not found...
							var newAnchors = range._find(anchorSelector);
							for (var k = 0; k < newAnchors.length; k++) {
								if (k === 0) {
									newAnchors[k].setAttribute('id', attributes.id);
								}
								else {
									newAnchors[k].remove(true);
								}
							}

							ranges = [range];
						}
					}
					else {
						ranges = updateElements(editor, anchors, attributes);
					}
					// We changed the content, so need to select it again.
					editor.getSelection().selectRanges(ranges);
				}
				else if (result.action === 'delete') {
					deleteAnchors(editor, true);
				}
			}

			function updateElements(editor, elements, attributes) {
				var ranges = [];
				for (var i = 0; i < elements.length; i++) {
					// We're only editing an existing element, so just overwrite the attributes.
					var element = elements[i];
					element.removeAttributes();
					element.setAttributes(attributes);
					var range = editor.createRange();
					range.setStartBefore(element);
					range.setEndAfter(element);
					ranges.push(range);
				}
				return ranges;
			}

			function deleteAnchors(editor, includePath) {
				var selection = editor.getSelection();
				var ranges = selection.getRanges();
				for (var i = 0; i < ranges.length; i++) {
					var range = selection.getRanges()[i];
					// Skip bogus to cover cases of multiple selection inside tables (#tp2245).
					range.shrink(CKEditor.SHRINK_TEXT, false, { skipBogus: true });
					var elements = range._find(anchorSelector);
					for (var j = 0; j < elements.length; j++) {
						elements[j].remove(true);
					}
				}
				if (includePath) {
					elements = getSelectedElements(editor);
					for (j = 0; j < elements.length; j++) {
						elements[j].remove(true);
					}
				}
			}

			CKEditor.plugins.add('proximisAnchor', {
				icons: 'proximisAnchor,proximisRemoveAnchor',
				hidpi: true,
				onLoad: function() {
					// Add the CSS styles for anchor placeholders.
					var iconPath = pluginAssetsPath + 'images/' + HiDPI + '/anchor.png';
					var baseStyle = 'background:url(' + iconPath + ') no-repeat %1 center;border:1px dotted #00f;background-size:16px;';

					var template = '.%2 span.anchor,' +
						'.cke_editable.%2 span.anchor' +
						'{' +
						baseStyle +
						'padding-%1:18px;' +
						// Show the arrow cursor for the anchor image (FF at least).
						'cursor:auto;' +
						'}' +
						'.%2 img.anchor' +
						'{' +
						baseStyle +
						'width:16px;' +
						'min-height:15px;' +
						// The default line-height on IE.
						'height:1.15em;' +
						// Opera works better with "middle" (even if not perfect)
						'vertical-align:text-bottom;' +
						'}';

					// Styles with contents direction awareness.
					function cssWithDir(dir) {
						return template.replace(/%1/g, dir === 'rtl' ? 'right' : 'left').replace(/%2/g, 'cke_contents_' + dir);
					}

					CKEditor.addCss(cssWithDir('ltr') + cssWithDir('rtl'));
				},

				init: function(editor) {
					editor.addCommand('proximisAnchor', new CKEditor.command(editor, {
						allowedContent: 'span[!id](!anchor)', // span tag with required "id" attribute and "anchor" class.
						async: true,
						exec: function(editor) {
							var cmd = this;
							var selection = editor.getSelection();
							var anchors = getSelectedElements(editor);
							var firstAnchor = anchors[0] || null;
							var data = {};
							if (firstAnchor) {
								data = {
									name: firstAnchor.getAttribute('id'),
									allowDeletion: true
								};
								// Don't change selection if some element is already selected.
								// For example - don't destroy fake selection.
								if (!selection.getSelectedElement() && !selection.isInTable()) {
									selection.selectElement(firstAnchor);
								}
							}

							proximisRichtextCKEditor.openCommandModal(
								editor,
								{
									definitionName: 'proximisRichtext/anchor',
									resolve: {
										anchorData: data,
										existingAnchors: function() { return getExistingAnchors(editor, data.name || null); }
									}
								},
								function(result) {
									applyAnchors(editor, anchors, result);
									editor.fire('afterCommandExec', {
										name: 'proximisAnchor',
										command: cmd
									});
								}
							);
						}
					}));

					editor.addCommand('proximisRemoveAnchor', new CKEditor.command(editor, {
						exec: function(editor) {
							deleteAnchors(editor, true);
						}
					}));

					if (editor.ui.addButton) {
						editor.ui.addButton('ProximisAnchor', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.insert_anchor|ucf'),
							command: 'proximisAnchor',
							toolbar: 'links,30'
						});
						editor.ui.addButton('ProximisRemoveAnchor', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.remove_anchor|ucf'),
							command: 'proximisRemoveAnchor',
							toolbar: 'links,40'
						});
					}

					editor.on('doubleclick', function(evt) {
						var anchors = getSelectedElements(editor);
						if (anchors.length && anchors[0]) {
							editor.pxDoubleClickOptions.push({
								labelKey: 'm.rbs.ua.richtext.modal_anchor',
								callback: function() {
									editor.execCommand('proximisAnchor');
									editor.getSelection().selectElement(anchors[0]);
								}
							});
						}
						else {
							var anchor = evt.data.element.getAscendant('span', 1);
							if (anchor && anchor.hasClass('anchor')) {
								editor.pxDoubleClickOptions.push({
									labelKey: 'm.rbs.ua.richtext.modal_anchor',
									callback: function() {
										editor.getSelection().selectElement(anchor);
										editor.execCommand('proximisAnchor');
									}
								});
							}
						}
					}, null, null, 0);

					// Do not remove anchors on styles cleanup.
					if (angular.isFunction(editor.addRemoveFormatFilter)) {
						editor.addRemoveFormatFilter(function(element) {
							return !anchorCondition(element);
						});
					}
				}
			});
		}
	]);
})(CKEDITOR, __change);
