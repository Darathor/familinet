﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, change) {
	'use strict';

	var app = angular.module('proximisRichtext');

	var basePath = (change.navigationContext && change.navigationContext.assetBasePath) ? change.navigationContext.assetBasePath : '/Assets/';
	var pluginAssetsPath = basePath + 'Rbs/Ua/ProximisRichtext/CKEditor/proximisClear/';
	var HiDPI = CKEditor.env.hidpi ? 'hidpi/' : '';

	CKEditor.skin.addIcon('proximisClear', pluginAssetsPath + 'icons/' + HiDPI + 'proximisClear.png');

	app.run(['proximisCoreI18n', function(proximisCoreI18n) {
		CKEditor.plugins.add('proximisClear', {
			icons: 'proximisClear',
			hidpi: true,
			init: function(editor) {
				var clear = editor.config.proximisClear;
				if (angular.isFunction(clear)) {
					editor.addCommand('proximisClear', new CKEditor.command(editor, {
						exec: function() {
							clear(editor);
						}
					}));

					// If the toolbar is available, create the "Source" button.
					if (editor.ui.addButton) {
						editor.ui.addButton('ProximisClear', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.clear_to_switch_mode|ucf'),
							command: 'proximisClear',
							toolbar: 'clear,15'
						});
					}
				}
			}
		});
	}]);
})(CKEDITOR, __change);
