﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor) {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.run(['proximisCoreI18n', 'proximisRichtextCKEditor', function(proximisCoreI18n, proximisRichtextCKEditor) {
		CKEditor.plugins.add('proximisDoubleClickSelect', {
			init: function(editor) {
				editor.on('doubleclick', function() {
					editor.pxDoubleClickOptions = [];
				}, null, null, -100);

				editor.on('doubleclick', function() {
					if (editor.pxDoubleClickOptions.length === 1) {
						editor.pxDoubleClickOptions[0].callback();
					}
					else if (editor.pxDoubleClickOptions.length > 1) {
						for (var i = 0; i < editor.pxDoubleClickOptions.length; i++) {
							editor.pxDoubleClickOptions[i].label = proximisCoreI18n.trans(editor.pxDoubleClickOptions[i].labelKey + '|ucf');
						}

						proximisRichtextCKEditor.openCommandModal(
							editor,
							{
								definitionName: 'proximisRichtext/doubleClickSelect',
								resolve: {
									options: function () { return editor.pxDoubleClickOptions; }
								}
							},
							function(result) {
								result.option.callback();
							}
						);
					}
				}, null, null, 100);
			}
		});
	}]);
})(CKEDITOR);
