﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor) {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.run(['proximisCoreI18n', function(proximisCoreI18n) {
		var langFixed = {};

		CKEditor.plugins.add('proximisI18nFix', {
			init: function() {
				// FIXME: there is probably a better way to fix translations than create a plugin but for now I didn't find it...
				if (CKEditor.lang.fr && !langFixed.fr) {
					langFixed.fr = true;
					CKEditor.lang.fr.pastetext.pasteNotification = proximisCoreI18n.trans('m.rbs.ua.richtext.paste_text_notification|ucf');
				}
			}
		});
	}]);
})(CKEDITOR);
