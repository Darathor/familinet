﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, change) {
	'use strict';

	var basePath = (change.navigationContext && change.navigationContext.assetBasePath) ? change.navigationContext.assetBasePath : '/Assets/';
	var pluginAssetsPath = basePath + 'Rbs/Ua/ProximisRichtext/CKEditor/proximisSource/';
	var HiDPI = CKEditor.env.hidpi ? 'hidpi/' : '';

	CKEditor.skin.addIcon('proximisSource', pluginAssetsPath + 'icons/' + HiDPI + 'proximisSource.png');

	var app = angular.module('proximisRichtext');

	app.run(['proximisCoreI18n', 'proximisRichtextCKEditor', function(proximisCoreI18n, proximisRichtextCKEditor) {
		CKEditor.plugins.add('proximisSource', {
			icons: 'proximisSource',
			hidpi: true,
			init: function(editor) {
				editor.addCommand('proximisSource', new CKEditor.command(editor, {
					async: true,
					exec: function(editor) {
						var cmd = this;
						proximisRichtextCKEditor.openCommandModal(
							editor,
							{
								definitionName: 'proximisRichtext/source',
								resolve: {
									editorData: function() { return editor.getData(); }
								}
							},
							function(result) {
								editor.setData(result.data.editorData);
								editor.fire('afterCommandExec', {
									name: 'proximisSource',
									command: cmd
								});
							}
						);
					}
				}));

				// If the toolbar is available, create the "Source" button.
				if (editor.ui.addButton) {
					editor.ui.addButton('ProximisSource', {
						label: proximisCoreI18n.trans('m.rbs.ua.richtext.source|ucf'),
						command: 'proximisSource',
						toolbar: 'document,10'
					});
				}
			}
		});
	}]);
})(CKEDITOR, __change);
