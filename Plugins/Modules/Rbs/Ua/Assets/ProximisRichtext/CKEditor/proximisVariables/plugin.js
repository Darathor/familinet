﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor) {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.run(['proximisCoreI18n', function(proximisCoreI18n) {
		CKEditor.plugins.add('proximisVariables', {
			init: function(editor) {
				var variables = editor.config.proximisVariables;
				if (angular.isArray(variables) && variables.length) {
					editor.ui.addRichCombo('Variables', {
						label: proximisCoreI18n.trans('m.rbs.ua.common.variables|ucf'),
						title: proximisCoreI18n.trans('m.rbs.ua.common.variables_title|ucf'),
						toolbar: 'variables,10',

						panel: {
							css: [CKEDITOR.skin.getPath('editor')].concat('.cke_panel_listItem a:focus:not(:hover) { background: transparent; }'),
							multiSelect: false,
							attributes: { 'aria-label': proximisCoreI18n.trans('m.rbs.ua.common.variables_title|ucf') }
						},

						init: function() {
							for (var i = 0; i < variables.length; i++) {
								var variable = variables[i];
								var value = variable.label;
								if (variable.value !== null && variable.value !== undefined) {
									value = variable.value;
								}
								else if (variable.code !== null && variable.code !== undefined) {
									value = '{' + variable.code + '}';
								}
								this.add(value, variable.label, variable.label);
							}
							this.commit();
						},

						onClick: function(value) {
							editor.insertText(value);
						}
					});
				}
			}
		});
	}]);
})(CKEDITOR);
