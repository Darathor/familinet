/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtext');

	/**
	 * @ngdoc directive
	 * @id proximisRichtext.directive:pxRichtextEditor
	 * @name pxRichtextEditor
	 * @restrict A
	 *
	 * @description
	 * Displays a richtext editor.
	 *
	 */
	app.directive('pxRichtextEditor', ['proximisCoreI18n', 'proximisModalStack', 'proximisRichtextCKEditor', pxRichtextEditor]);

	function pxRichtextEditor(proximisCoreI18n, proximisModalStack, proximisRichtextCKEditor) {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				profile: '@?',
				variables: '<?'
			},
			templateUrl: 'Rbs/Ua/ProximisRichtext/directives/editor.twig',

			link: function(scope, element, attrs, ngModel) {
				scope.data = {
					modeChosen: false,
					mode: null,
					content: '',
					availableModes: [
						{
							name: 'Html',
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.wysiwyg|ucf')
						}
					],
					clear: clear,
					clearFromWYSIWYG: clearFromWYSIWYG
				};

				ngModel.$render = function() {
					if ((!angular.isObject(ngModel.$viewValue) || !ngModel.$viewValue.rawText) && scope.data.availableModes.length === 1) {
						scope.data.modeChosen = true;
						scope.data.mode = scope.data.availableModes[0].name;
						scope.data.content = '';
					}
					else if (angular.isObject(ngModel.$viewValue)) {
						if (ngModel.$viewValue.rawText || scope.data.modeChosen) {
							scope.data.mode = ngModel.$viewValue.editor;
							scope.data.content = ngModel.$viewValue.rawText;
						}
						else {
							scope.data.mode = null;
							scope.data.content = '';
						}
					}
				};

				ngModel.$parsers.unshift(function(viewValue) {
					if (viewValue.rawText && viewValue.editor) {
						return angular.copy(viewValue);
					}
					return null;
				});

				scope.chooseType = function(editorMode) {
					scope.data.modeChosen = true;
					ngModel.$setViewValue({ editor: editorMode, rawText: '' });
					ngModel.$render();
				};

				var clearModalOptions = {
					definitionName: 'proximisRichtext/confirm',
					resolve: {
						config: function() { return { message: proximisCoreI18n.trans('m.rbs.ua.richtext.clear_confirm|ucf') }; }
					}
				};

				function onClearConfirmed() {
					scope.data.modeChosen = false;
					ngModel.$setViewValue({ editor: null, rawText: '' });
					ngModel.$render();
				}

				function clear() {
					proximisModalStack.open(clearModalOptions).result.then(
						onClearConfirmed,
						function(result) {
							// Nothing to do on cancel.
						}
					);
				}

				function clearFromWYSIWYG(editor) {
					proximisRichtextCKEditor.openCommandModal(
						editor,
						clearModalOptions,
						onClearConfirmed
					);
				}

				scope.$watch('data.content', function() {
					ngModel.$setViewValue({ editor: scope.data.mode, rawText: scope.data.content });
				});
			}
		};
	}
})();
