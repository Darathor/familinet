/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtext');

	/**
	 * @ngdoc directive
	 * @id proximisRichtext.directive:pxRichtextWysiwygEditor
	 * @name pxRichtextWysiwygEditor
	 * @restrict A
	 *
	 * @description
	 * The WYSIWYG editor.
	 *
	 * In most cases it shouldn't be used directly, prefer using the directive {@link proximisRichtext.directive:pxRichtext `pxRichtextEditor`}.
	 */
	app.directive('pxRichtextWysiwygEditor', ['proximisRichtextCKEditor', pxRichtextWysiwygEditor]);

	function pxRichtextWysiwygEditor(proximisRichtextCKEditor) {
		return {
			restrict: 'A',
			scope: {
				options: '<?',
				content: '=',
				profile: '@?',
				variables: '<?',
				clearFunction: '<?'
			},
			templateUrl: 'Rbs/Ua/ProximisRichtext/directives/wysiwyg-editor.twig',
			controller: ['$scope', function (scope) {
				scope.config = proximisRichtextCKEditor.getConfig(scope.profile || 'default');
				if (scope.options) {
					angular.extend(scope.config, scope.options);
				}
				if (scope.variables) {
					scope.config.proximisVariables = scope.variables;
				}
				if (angular.isFunction(scope.clearFunction)) {
					scope.config.proximisClear = scope.clearFunction;
				}
			}]
		};
	}
})();
