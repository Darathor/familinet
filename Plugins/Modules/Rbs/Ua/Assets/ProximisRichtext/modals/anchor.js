/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.controller('ProximisRichtextModalsAnchorController', ['$scope', 'anchorData', 'existingAnchors',
		ProximisRichtextModalsAnchorController]);

	function ProximisRichtextModalsAnchorController(scope, anchorData, existingAnchors) {
		scope.config = {
			existingAnchors: existingAnchors,
			allowDeletion: anchorData.allowDeletion
		};

		scope.data = {
			text: anchorData.text,
			name: anchorData.name
		};

		scope.ok = function() {
			var result = {
				name: scope.data.name
			};
			if (scope.config.needsText) {
				result.anchorText = scope.data.text;
			}

			scope.$close({ action: 'save', data: result });
		};

		scope.disableOkButton = function() {
			return scope.insertAnchorForm.$invalid || (scope.config.needsText && !scope.data.text) || (scope.data.type === 'internal' && !scope.data.documentId) ||
				(scope.data.type === 'external' && !scope.data.url) || (scope.data.type === 'anchor' && !scope.data.anchor);
		};

		scope.delete = function() {
			scope.$close({ action: 'delete' });
		};
	}
})();