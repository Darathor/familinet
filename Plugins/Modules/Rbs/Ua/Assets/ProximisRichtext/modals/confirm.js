/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.controller('ProximisRichtextModalsConfirmController', ['$scope', 'config', 'proximisCoreI18n', ProximisRichtextModalsConfirmController]);

	function ProximisRichtextModalsConfirmController(scope, config, proximisCoreI18n) {
		scope.data = {
			message: config.message,
			OKLabel: config.OKLabel || proximisCoreI18n.trans('m.rbs.ua.common.validate|ucf'),
			OKClass: config.OKClass || 'btn-primary',
			KOLabel: config.KOLabel || proximisCoreI18n.trans('m.rbs.ua.common.cancel|ucf'),
			KOClass: config.KOClass || 'btn-default'
		};
	}
})();