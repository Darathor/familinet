/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtext');

	app.controller('ProximisRichtextModalsSourceController', ['$scope', 'editorData', ProximisRichtextModalsSourceController]);

	function ProximisRichtextModalsSourceController(scope, editorData) {
		scope.editorData = editorData;

		scope.ok = function() {
			var result = {
				editorData: scope.editorData
			};
			scope.$close({ action: 'save', data: result });
		};

		scope.disableOkButton = function() {
			return editorData === scope.editorData;
		};
	}
})();