/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change, window) {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisRichtext
	 *
	 * @description
	 * The ProximisRichtext module contains all components for rest api
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisRichtext` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisRichtext` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxRichtext`
	 */
	var app = angular.module('proximisRichtext', ['ckeditor', 'proximisModal', 'proximisCore']);

	/**
	 * @ngdoc constant
	 * @id proximisRichtext.constant:ProximisRichtextGlobal
	 * @name ProximisRichtextGlobal
	 *
	 * @description
	 * A wrapper for the `__change` global variable.
	 *
	 * @property {string} lang The two-letters language code.
	 */
	app.constant('ProximisRichtextGlobal', __change);

	// Configuration.
	app.config(['proximisRichtextCKEditorProvider', function(proximisRichtextCKEditorProvider) {
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisI18nFix');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisDoubleClickSelect');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisAnchor');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisSource');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisVariables');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisClear');
	}]);

	// proximisCoreI18n isn't usable in config step, so use a simplified local implementation.
	window.proximisRichtextTranslate = function(string) {
		string = string.toLowerCase();
		var p = string.lastIndexOf('.');
		var path = string.substring(0, p);
		var key = string.substr(p + 1);

		// Search for the key in the global object.
		if (__change.i18n[path] && __change.i18n[path][key]) {
			string = __change.i18n[path][key];
		}
		return string;
	};
})(window.__change, window);