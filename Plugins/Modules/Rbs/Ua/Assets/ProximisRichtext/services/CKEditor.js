/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, trans) {
	'use strict';

	CKEditor.timestamp = undefined; // Remove the "?t=..." in the URL of each resources.
	CKEditor.config.stylesSet = 'default'; // Don't load styles.js.
	CKEditor.config.customConfig = false; // Don't load config.js.

	var app = angular.module('proximisRichtext');

	/**
	 * @ngdoc provider
	 * @id proximisRichtext.provider:proximisRichtextCKEditorProvider
	 * @name proximisRichtextCKEditorProvider
	 *
	 * @description
	 * Use `proximisRichtextCKEditorProvider` to configure `proximisRichtextCKEditor`.
	 */
	app.provider('proximisRichtextCKEditor', ['ProximisRichtextGlobal', proximisRichtextCKEditorProvider]);

	function proximisRichtextCKEditorProvider(ProximisRichtextGlobal) {
		var profiles = {};
		var plugins = [];
		var removeButtons = [];

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextCKEditorProvider
		 * @name setConfigProfile
		 *
		 * @description
		 * Sets the base URL for the proximisRichtextApi service.
		 *
		 * @param {string} name The profile name.
		 * @param {Object} config The config.
		 */
		this.setConfigProfile = function(name, config) {
			profiles[name] = config;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextCKEditorProvider
		 * @name addExtraPlugin
		 *
		 * @description
		 * Adds an extra CKEditor plugin.
		 *
		 * @param {string} name The plugin name.
		 */
		this.addExtraPlugin = function(name) {
			plugins.push(name);
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextCKEditorProvider
		 * @name removeButton
		 *
		 * @description
		 * Remove a button from CKEditor toolbar.
		 *
		 * @param {string} name The button name.
		 */
		this.removeButton = function(name) {
			removeButtons.push(name);
		};

		this.$get = ['proximisModalStack', function(proximisModalStack) {
			/**
			 * @ngdoc service
			 * @id proximisRichtext.service:proximisRichtextCKEditor
			 * @name proximisRichtextCKEditor
			 *
			 * @description
			 * Provides methods used to parameterize CKEditor.
			 */
			return {
				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextCKEditor
				 * @name getConfig
				 *
				 * @description
				 * Get the configuration for the given profile name.
				 *
				 * @param {string} profileName The profile name.
				 *
				 * @returns {Object|null} The config.
				 */
				getConfig: function(profileName) {
					if (profiles.hasOwnProperty(profileName)) {
						var config = angular.copy(profiles[profileName]);
						config.language = ProximisRichtextGlobal.lang;
						config.entities = false;
						config.extraPlugins = plugins.join(',');

						var removeButtonsParts = [];
						if (config.removeButtons) {
							removeButtonsParts.push(config.removeButtons);
						}
						if (removeButtons.length) {
							removeButtonsParts.push(removeButtons.join(','));
						}
						config.removeButtons = removeButtonsParts.join(',');
						return config;
					}
					return null;
				},

				/**
				 * @param {CKEDITOR.editor} editor
				 * @param {Object} modalOptions
				 * @param {function=} successCallback
				 * @param {function=} errorCallback
				 * @returns {Object|null} The modal instance or `null` in error case.
				 */
				openCommandModal: function(editor, modalOptions, successCallback, errorCallback) {
					var container = editor.container.getFirst(function(node) {
						return node.type === CKEditor.NODE_ELEMENT && node.hasClass('cke_inner');
					});
					var body = window.document.querySelector('body');
					var originalPaddingTop = body.style.paddingTop;
					if (container.hasClass('cke_maximized')) {
						container.hide();
						body.style.paddingTop = '10000px'; // Hack for Chrome...
					}

					var modalInstance = proximisModalStack.open(modalOptions);

					if (modalInstance) {
						modalInstance.result.then(
							function(result) {
								if (angular.isFunction(successCallback)) {
									successCallback(result);
								}
								if (container.hasClass('cke_maximized')) {
									container.show();
									body.style.paddingTop = originalPaddingTop; // Revert hack for Chrome...
								}
							},
							function(result) {
								if (angular.isFunction(errorCallback)) {
									errorCallback(result);
								}
								if (container.hasClass('cke_maximized')) {
									container.show();
									body.style.paddingTop = originalPaddingTop; // Revert hack for Chrome...
								}
							}
						);
					}

					return modalInstance;
				}
			};
		}];
	}

	// Configuration.

	app.config(['proximisRichtextCKEditorProvider', function(proximisRichtextCKEditorProvider) {
		proximisRichtextCKEditorProvider.setConfigProfile('default', {
			// The toolbar groups arrangement, optimized for a single toolbar row.
			toolbarGroups: [
				{ name: 'styles' },
				{ name: 'variables' },
				{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
				{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
				{ name: 'links' },
				{ name: 'insert' },
				{ name: 'tools' },
				{ name: 'others' },
				{ name: 'clipboard', groups: ['clipboard', 'undo', 'find', 'selection', 'spellchecker', 'mode', 'document', 'doctools'] },
				{ name: 'editing' },
				{ name: 'document' },
				{ name: 'clear' }
			],

			// The default plugins included in the basic setup define some buttons that
			// are not needed in a basic editor. They are removed here.
			//Undo,Redo,Anchor'
			removeButtons: 'Cut,Copy,Paste,Underline,Strike,Subscript,Superscript,Replace',

			// Core styles overrides (for backward compatibility with old rich-texts.
			coreStyles_bold: { element: 'b', overrides: 'strong' },
			coreStyles_italic: { element: 'i', overrides: 'em' }
		});

		// This file contains style definitions that can be used by CKEditor plugins.
		//
		// The most common use for it is the "stylescombo" plugin which shows the Styles drop-down
		// list containing all styles in the editor toolbar. Other plugins, like
		// the "div" plugin, use a subset of the styles for their features.
		//
		// If you do not have plugins that depend on this file in your editor build, you can simply
		// ignore it. Otherwise it is strongly recommended to customize this file to match your
		// website requirements and design properly.
		//
		// For more information refer to: http://docs.ckeditor.com/#!/guide/dev_styles-section-style-rules
		CKEditor.stylesSet.add('default', [
			/* Block styles */

			// These styles are already available in the "Format" drop-down list ("format" plugin),
			// so they are not needed here by default. You may enable them to avoid
			// placing the "Format" combo in the toolbar, maintaining the same features.
			{ name: trans('m.rbs.ua.richtext.paragraph'), element: 'p' },
			{ name: trans('m.rbs.ua.richtext.heading_1'), element: 'h1' },
			{ name: trans('m.rbs.ua.richtext.heading_2'), element: 'h2' },
			{ name: trans('m.rbs.ua.richtext.heading_3'), element: 'h3' },
			{ name: trans('m.rbs.ua.richtext.heading_4'), element: 'h4' },
			{ name: trans('m.rbs.ua.richtext.heading_5'), element: 'h5' },
			{ name: trans('m.rbs.ua.richtext.heading_6'), element: 'h6' },
			{ name: trans('m.rbs.ua.richtext.preFormatted_text'), element: 'pre' },
			{ name: trans('m.rbs.ua.richtext.address'), element: 'address' },

			/* Inline styles */

			// These are core styles available as toolbar buttons. You may opt enabling
			// some of them in the Styles drop-down list, removing them from the toolbar.
			// (This requires the "stylescombo" plugin.)
			/*
			{ name: 'Strong',			element: 'strong', overrides: 'b' },
			{ name: 'Emphasis',			element: 'em'	, overrides: 'i' },
			{ name: 'Underline',		element: 'u' },
			{ name: 'Strikethrough',	element: 'strike' },
			*/

			/*{ name: trans('m.rbs.ua.richtext.marker'), element: 'span', attributes: { 'class': 'marker' } },*/

			{ name: trans('m.rbs.ua.richtext.deleted_text'), element: 'del' },
			{ name: trans('m.rbs.ua.richtext.inserted_text'), element: 'ins' },

			{ name: trans('m.rbs.ua.richtext.subscript'), element: 'sub' },
			{ name: trans('m.rbs.ua.richtext.superscript'), element: 'sup' },

			{ name: trans('m.rbs.ua.richtext.cited_work'), element: 'cite' },
			{ name: trans('m.rbs.ua.richtext.inline_quotation'), element: 'q' },

			{ name: trans('m.rbs.ua.richtext.language_rtl'), element: 'span', attributes: { 'dir': 'rtl' } },
			{ name: trans('m.rbs.ua.richtext.language_ltr'), element: 'span', attributes: { 'dir': 'ltr' } },

			/* Object styles */

			{
				name: trans('m.rbs.ua.richtext.image_align_left'),
				element: 'img',
				attributes: { 'class': 'pull-left' }
			},

			{
				name: trans('m.rbs.ua.richtext.image_align_right'),
				element: 'img',
				attributes: { 'class': 'pull-right' }
			},

			/* Widget styles */

			{ name: 'Clean Image', type: 'widget', widget: 'image', attributes: { 'class': 'image-clean' } },
			{ name: 'Grayscale Image', type: 'widget', widget: 'image', attributes: { 'class': 'image-grayscale' } },

			{ name: 'Featured Snippet', type: 'widget', widget: 'codeSnippet', attributes: { 'class': 'code-featured' } },

			{ name: 'Featured Formula', type: 'widget', widget: 'mathjax', attributes: { 'class': 'math-featured' } },

			{ name: '240p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-240p' }, group: 'size' },
			{ name: '360p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-360p' }, group: 'size' },
			{ name: '480p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-480p' }, group: 'size' },
			{ name: '720p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-720p' }, group: 'size' },
			{ name: '1080p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-1080p' }, group: 'size' },

			// Adding space after the style name is an intended workaround. For now, there
			// is no option to create two styles with the same name for different widget types. See http://dev.ckeditor.com/ticket/16664.
			{ name: '240p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-240p' }, group: 'size' },
			{ name: '360p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-360p' }, group: 'size' },
			{ name: '480p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-480p' }, group: 'size' },
			{ name: '720p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-720p' }, group: 'size' },
			{ name: '1080p ', type: 'widget', widget: 'embed', attributes: { 'class': 'embed-1080p' }, group: 'size' }
		]);

		CKEditor.addCss('.pull-left { float: left; margin-right: 10px; }');
		CKEditor.addCss('.pull-right { float: right; margin-left: 10px; }');
	}]);
})(CKEDITOR, window.proximisRichtextTranslate);

