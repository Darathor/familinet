﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, change) {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	var basePath = (change.navigationContext && change.navigationContext.assetBasePath) ? change.navigationContext.assetBasePath : '/Assets/';
	var pluginAssetsPath = basePath + 'Rbs/Ua/ProximisRichtextUa/CKEditor/proximisLink/';
	var HiDPI = CKEditor.env.hidpi ? 'hidpi/' : '';

	CKEditor.skin.addIcon('proximisLink', pluginAssetsPath + 'icons/' + HiDPI + 'proximisLink.png');
	CKEditor.skin.addIcon('proximisUnlink', pluginAssetsPath + 'icons/' + HiDPI + 'proximisUnlink.png');

	/**
	 * The following code is largely inspired by/copied from the native link module of CKEditor.
	 */
	app.run(['$timeout', 'proximisCoreI18n', 'proximisRichtextCKEditor',
		function($timeout, proximisCoreI18n, proximisRichtextCKEditor) {
			var linkSelector = 'a';
			var linkCondition = function(el) { return el.is ? el.is('a') : el.getName() === 'a'; };

			function getExistingAnchors(editor, exclude) {
				var anchors = [];
				var editable = editor.editable();

				// The scope of search for anchors is the entire document for inline editors
				// and editor's editable for classic editor/divarea (http://dev.ckeditor.com/ticket/11359).
				var container = ( editable.isInline() && !editor.plugins.divarea ) ? editor.document : editable;

				// Retrieve all anchors within the scope.
				var links = container.getElementsByTag('span');
				var linksCount = links.count();
				for (var i = 0; i < linksCount; i++) {
					var item = links.getItem(i++);
					var anchor = item.getAttribute('id');
					if (anchor && (!exclude || exclude !== anchor)) {
						anchors.push(anchor);
					}
				}

				// Retrieve all "fake anchors" within the scope.
				var images = container.getElementsByTag('img');
				var imagesCount = images.count();
				for (i = 0; i < imagesCount; i++) {
					item = images.getItem(i++);
					anchor = item.getAttribute('id');
					if (anchor && (!exclude || exclude !== anchor)) {
						anchors.push(anchor);
					}
				}
				return anchors;
			}

			function getSelectedLinks(editor) {
				var selection = editor.getSelection();
				var ranges = selection.getRanges();
				var anchors = [];
				for (var i = 0; i < ranges.length; i++) {
					var range = selection.getRanges()[i];
					// Skip bogus to cover cases of multiple selection inside tables (#tp2245).
					range.shrink(CKEditor.SHRINK_TEXT, false, { skipBogus: true });
					var anchor = editor.elementPath(range.getCommonAncestor()).contains(linkCondition, 1);
					if (anchor) {
						anchors.push(anchor);
					}
				}
				return anchors;
			}

			function applyLinks(editor, links, result) {
				if (result.action === 'save') {
					var data = result.data;
					var attributes = {};
					if (data.documentId) {
						attributes['data-document-id'] = data.documentId;
					}
					if (data.href) {
						attributes.href = data.href;
					}
					if (data.title) {
						attributes.title = data.title;
					}
					if (data.target) {
						attributes.target = data.target;
					}

					if (!links.length) {
						var style = new CKEditor.style({ element: 'a', attributes: attributes });
						style.type = CKEditor.STYLE_INLINE; // need to override... dunno why.

						var ranges = editor.getSelection().getRanges();
						for (var i = 0; i < ranges.length; i++) {
							var range = ranges[i];

							// Editable links nested within current range should be removed, so that the link is applied to whole selection.
							var nestedAnchors = range._find(linkSelector);
							for (var j = 0; j < nestedAnchors.length; j++) {
								nestedAnchors[j].remove(true);
							}

							// If there is no selected text, insert the linkText.
							if (range.collapsed && data.linkText) {
								var textElement = new CKEditor.dom.text(data.linkText, editor.document);
								range.insertNode(textElement);
								range.selectNodeContents(textElement);
							}

							// Apply style.
							style.applyToRange(range, editor);
						}
					}
					else {
						ranges = updateElements(editor, links, attributes);
					}
					// We changed the content, so need to select it again.
					editor.getSelection().selectRanges(ranges);
				}
				else if (result.action === 'delete') {
					deleteLinks(editor, true);
				}
			}

			function updateElements(editor, elements, attributes) {
				var ranges = [];
				for (var i = 0; i < elements.length; i++) {
					// We're only editing an existing element, so just overwrite the attributes.
					var element = elements[i];
					element.removeAttributes();
					element.setAttributes(attributes);
					var range = editor.createRange();
					range.setStartBefore(element);
					range.setEndAfter(element);
					ranges.push(range);
				}
				return ranges;
			}

			function deleteLinks(editor, includePath) {
				var selection = editor.getSelection();
				var ranges = selection.getRanges();
				for (var i = 0; i < ranges.length; i++) {
					var range = selection.getRanges()[i];
					// Skip bogus to cover cases of multiple selection inside tables (#tp2245).
					range.shrink(CKEditor.SHRINK_TEXT, false, { skipBogus: true });
					var elements = range._find(linkSelector);
					for (var j = 0; j < elements.length; j++) {
						elements[j].remove(true);
					}
				}
				if (includePath) {
					elements = getSelectedLinks(editor);
					for (j = 0; j < elements.length; j++) {
						elements[j].remove(true);
					}
				}
			}

			CKEditor.plugins.add('proximisLink', {
				icons: 'proximisLink,proximisUnlink',
				hidpi: true,
				init: function(editor) {
					editor.addCommand('proximisLink', new CKEditor.command(editor, {
						allowedContent: 'a[title,href,data-document-id]',
						async: true,
						exec: function(editor) {
							var cmd = this;
							var selection = editor.getSelection();
							var links = getSelectedLinks(editor);
							var firstLink = links[0] || null;
							var data = {};
							if (firstLink && (firstLink.hasAttribute('href') || firstLink.hasAttribute('data-document-id'))) {
								data = {
									href: firstLink.getAttribute('href'),
									documentId: firstLink.getAttribute('data-document-id'),
									title: firstLink.getAttribute('title'),
									target: firstLink.getAttribute('target') === '_blank' ? '_blank' : ''
								};
								// Don't change selection if some element is already selected.
								// For example - don't destroy fake selection.
								if (!selection.getSelectedElement() && !selection.isInTable()) {
									selection.selectElement(firstLink);
								}
							}

							// Link text can be set only on link creation on an empty selection.
							data.needsText = !firstLink && !selection.getSelectedText();
							data.allowDeletion = !!firstLink;

							proximisRichtextCKEditor.openCommandModal(
								editor,
								{
									definitionName: 'proximisRichtextUa/link',
									resolve: {
										linkData: data,
										existingAnchors: function() { return getExistingAnchors(editor); }
									}
								},
								function(result) {
									applyLinks(editor, links, result);
									editor.fire('afterCommandExec', {
										name: 'proximisLink',
										command: cmd
									});
								}
							);
						}
					}));

					editor.addCommand('proximisUnlink', new CKEditor.command(editor, {
						exec: function(editor) {
							deleteLinks(editor, true);
						}
					}));

					editor.setKeystroke(CKEditor.CTRL + 76 /*L*/, 'proximisLink');

					if (editor.ui.addButton) {
						editor.ui.addButton('ProximisLink', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.insert_link|ucf'),
							command: 'proximisLink',
							toolbar: 'links,10'
						});
						editor.ui.addButton('ProximisUnlink', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.remove_link|ucf'),
							command: 'proximisUnlink',
							toolbar: 'links,20'
						});
					}

					editor.on('doubleclick', function(evt) {
						// If the link has descendants and the last part of it is also a part of a word partially
						// unlinked, clicked element may be a descendant of the link, not the link itself. (http://dev.ckeditor.com/ticket/11956)
						var links = getSelectedLinks(editor);
						if (links.length && links[0]) {
							editor.pxDoubleClickOptions.push({
								labelKey: 'm.rbs.ua.richtext.modal_link',
								callback: function() {
									editor.execCommand('proximisLink');
									editor.getSelection().selectElement(links[0]);
								}
							});
						}
						else {
							var link = evt.data.element.getAscendant('a', 1);
							if (link) {
								editor.pxDoubleClickOptions.push({
									labelKey: 'm.rbs.ua.richtext.modal_link',
									callback: function() {
										editor.getSelection().selectElement(link);
										editor.execCommand('proximisLink');
									}
								});
							}
						}
					}, null, null, 0);
				}
			});
		}
	]);
})(CKEDITOR, __change);
