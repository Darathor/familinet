﻿/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(CKEditor, change) {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	var basePath = (change.navigationContext && change.navigationContext.assetBasePath) ? change.navigationContext.assetBasePath : '/Assets/';
	var pluginAssetsPath = basePath + 'Rbs/Ua/ProximisRichtextUa/CKEditor/proximisMedia/';
	var HiDPI = CKEditor.env.hidpi ? 'hidpi/' : '';

	CKEditor.skin.addIcon('proximisInsertMedia', pluginAssetsPath + 'icons/' + HiDPI + 'proximisMedia.png');

	app.run(['$timeout', 'proximisRichtextCKEditor', 'proximisRichtextUaCKEditor', 'proximisRestApi', 'proximisRestApiDefinition', 'proximisCoreI18n',
		function($timeout, proximisRichtextCKEditor, proximisRichtextUaCKEditor, proximisRestApi, proximisRestApiDefinition, proximisCoreI18n) {
			function getSelectedImage(editor, element) {
				if (!element) {
					var sel = editor.getSelection();
					element = sel.getSelectedElement();
				}

				if (element && element.is('img') && element.hasAttribute('data-document-id') && !element.isReadOnly()) {
					return element;
				}
			}

			function deleteImage(editor, element) {
				element = getSelectedImage(editor, element);
				if (element) {
					element.remove(true);
				}
			}

			function updateElements(editor, elements, attributes) {
				var ranges = [];
				for (var i = 0; i < elements.length; i++) {
					// We're only editing an existing element, so just overwrite the attributes.
					var element = elements[i];
					element.removeAttributes();
					element.setAttributes(attributes);
					var range = editor.createRange();
					range.setStartBefore(element);
					range.setEndAfter(element);
					ranges.push(range);
				}
				return ranges;
			}

			function applyImage(editor, image, result) {
				if (result.action === 'save') {
					var data = result.data;
					var attributes = {};
					if (data.documentId) {
						attributes['data-document-id'] = data.documentId;
					}
					if (data.src) {
						attributes.src = data.src;
					}
					if (data.title) {
						attributes.title = data.title;
					}
					if (data.alt) {
						attributes.alt = data.alt;
					}
					if (data.height) {
						attributes.height = data.height;
					}
					if (data.width) {
						attributes.width = data.width;
					}
					updateElements(editor, [image], attributes);
				}
				else if (result.action === 'delete') {
					deleteImage(editor);
				}
			}

			CKEditor.plugins.add('proximisMedia', {
				icons: 'proximisMedia',
				hidpi: true,
				init: function(editor) {
					editor.addCommand('proximisInsertMedia', new CKEditor.command(editor, {
						allowedContent: 'img[title,alt,!src,width,height,!data-document-id];video[src,preload,controls,!data-document-id];a[title,href,!data-document-id]',
						async: true,
						exec: function(editor) {
							var cmd = this;

							proximisRichtextCKEditor.openCommandModal(
								editor,
								{
									definitionName: proximisRichtextUaCKEditor.getMediaInsertionModal()
								},
								function(result) {
									if (result.action !== 'insert' || !result.data.documentIds.length) {
										return;
									}

									var loadRestService = proximisRichtextUaCKEditor.getMediaLoadService();
									var params = { mediaIds: result.data.documentIds };
									proximisRestApi.sendData(proximisRestApiDefinition.getData(loadRestService, 'GET', params)).then(
										function(result) {
											var medias = result.data.items;
											var length = medias.length;
											for (var i = 0; i < length; i++) {
												var media = medias[i];
												var element = null;
												switch (media.model) {
													case 'Rbs_Media_Image':
														element = editor.document.createElement('img');
														element.setAttribute('data-document-id', media.id);
														element.setAttribute('src', media.src);
														if (media.title) {
															element.setAttribute('title', media.title);
														}
														if (media.alt) {
															element.setAttribute('alt', media.alt);
														}
														break;
													case 'Rbs_Media_Video':
														element = editor.document.createElement('video');
														element.setAttribute('data-document-id', media.id);
														element.setAttribute('src', media.src);
														element.setAttribute('preload', 'auto');
														element.setAttribute('controls', 'controls');
														if (media.alt) {
															element.setAttribute('alt', media.alt);
														}
														break;
													case 'Rbs_Media_File':
														element = editor.document.createElement('a');
														element.setAttribute('data-document-id', media.id);
														element.setAttribute('href', '');
														element.setText(media.title);
														break;
													default:
														console.error('wysiwygInsertMedia', 'Unexpected model: ' + media.model);
														break;
												}

												if (element) {
													editor.insertElement(element);
													// Add a space between elements:
													editor.document.createText(' ').insertBefore(element); // Before the element for Firefox...
													editor.document.createText(' ').insertAfter(element); // and after for Chrome and Edge.
												}
											}

											editor.fire('afterCommandExec', {
												name: 'proximisInsertMedia',
												command: cmd
											});
										},
										function(error) {
											console.error('[CKEditor.plugin.proximisInsertMedia]', error);
										}
									);
								}
							);
						}
					}));

					if (editor.ui.addButton) {
						editor.ui.addButton('ProximisInsertMedia', {
							label: proximisCoreI18n.trans('m.rbs.ua.richtext.insert_media'),
							command: 'proximisInsertMedia',
							toolbar: 'links,1'
						});
					}

					editor.addCommand('proximisImage', new CKEditor.command(editor, {
						allowedContent: 'img[title,alt,!src,width,height,!data-document-id]',
						async: true,
						exec: function(editor) {
							var cmd = this;
							var selectedImage = getSelectedImage(editor);
							var data = {};
							if (selectedImage && selectedImage.hasAttribute('data-document-id')) {
								data = {
									documentId: selectedImage.getAttribute('data-document-id'),
									title: selectedImage.getAttribute('title'),
									alt: selectedImage.getAttribute('alt'),
									height: selectedImage.getAttribute('height'),
									width: selectedImage.getAttribute('width')
								};
							}
							else {
								console.warn('No selected image');
								return;
							}

							proximisRichtextCKEditor.openCommandModal(
								editor,
								{
									definitionName: 'proximisRichtextUa/image',
									resolve: {
										imageData: data
									}
								},
								function(result) {
									applyImage(editor, selectedImage, result);
									editor.fire('afterCommandExec', {
										name: 'proximisImage',
										command: cmd
									});
								}
							);
						}
					}));

					editor.on('doubleclick', function() {
						var image = getSelectedImage(editor);
						if (image) {
							editor.pxDoubleClickOptions.push({
								labelKey: 'm.rbs.ua.richtext.modal_image',
								callback: function() {
									editor.execCommand('proximisImage');
									editor.getSelection().selectElement(image);
								}
							});
						}
					}, null, null, 0);
				}
			});
		}
	]);
})(CKEDITOR, __change);
