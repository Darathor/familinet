/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	app.controller('ProximisRichtextUaModalsImageController', ['$scope', 'imageData', 'proximisRichtextUaCKEditor', 'proximisRestApi',
		'proximisRestApiDefinition', ProximisRichtextUaModalsImageController]);

	function ProximisRichtextUaModalsImageController(scope, imageData, proximisRichtextUaCKEditor, proximisRestApi, proximisRestApiDefinition) {
		scope.config = {
			allowDeletion: true
		};

		scope.data = {
			width: parseInt(imageData.width) || null,
			height: parseInt(imageData.height) || null,
			title: imageData.title,
			alt: imageData.alt
		};

		var loadRestService = proximisRichtextUaCKEditor.getMediaLoadService();
		var params = { mediaIds: [imageData.documentId], fields: ['variables'] };
		proximisRestApi.sendData(proximisRestApiDefinition.getData(loadRestService, 'GET', params)).then(
			function(result) {
				var image = result.data.items[0];
				if (!image || image.model !== 'Rbs_Media_Image') {
					console.error('Image not found!', result);
					return;
				}

				scope.config.image = image;
				scope.config.variables = result.data.variables[0];
			},
			function(result) {
				console.error('Image not found!', result);
			}
		);

		scope.ok = function() {
			var result = {
				documentId: imageData.documentId,
				src: scope.config.image.src,
				width: scope.data.width,
				height: scope.data.height,
				title: scope.data.title,
				alt: scope.data.alt
			};

			scope.$close({ action: 'save', data: result });
		};

		scope.disableOkButton = function() {
			return (scope.config.needsText && !scope.data.text) || (scope.data.type === 'internal' && !scope.data.documentId) ||
				(scope.data.type === 'external' && !scope.data.url) || (scope.data.type === 'anchor' && !scope.data.anchor);
		};

		scope.delete = function() {
			scope.$close({ action: 'delete' });
		};
	}
})();