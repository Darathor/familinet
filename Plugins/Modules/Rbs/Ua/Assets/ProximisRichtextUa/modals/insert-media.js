/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	app.controller('ProximisRichtextUaModalsInsertMediaController', ['$scope', 'proximisRichtextUaCKEditor', 'proximisRestApiDefinition',
		'proximisRestApi', 'proximisCoreI18n', 'proximisMessages', 'ProximisGlobal', ProximisRichtextUaModalsInsertMediaController]);

	function ProximisRichtextUaModalsInsertMediaController(scope, proximisRichtextUaCKEditor, proximisRestApiDefinition,
		proximisRestApi, proximisCoreI18n, proximisMessages, ProximisGlobal) {
		scope.config = {
			documentSelectorSearchService: proximisRichtextUaCKEditor.getDocumentSelectorSearchService(),
			documentSelectorLoadService: proximisRichtextUaCKEditor.getDocumentSelectorLoadService(),
			imageListService: proximisRichtextUaCKEditor.getImageListService(),
			fileUploadService: proximisRichtextUaCKEditor.getFileUploadService(),
			imageUploadService: proximisRichtextUaCKEditor.getImageUploadService()
		};

		if (!scope.config.documentSelectorSearchService || !scope.config.documentSelectorLoadService) {
			console.error('ProximisRichtextUaModalsInsertMediaController: documentSelectorSearchService and documentSelectorLoadService are required!'
				+ ' Please configure proximisRichtextUaCKEditor service.');
		}

		scope.newFile = {
			uploading: false,
			file: null,
			LCID: null,
			label: null,
			title: null,
			description: null
		};

		scope.uploadFile = function() {
			if (!scope.newFile.file || !scope.newFile.label) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'),
					proximisCoreI18n.trans('m.rbs.ua.common.error_while_uploading_file | ucf'), null, 60000);
				return;
			}

			scope.newFile.uploading = true;
			var params = {
				label: scope.newFile.label,
				title: scope.newFile.title,
				description: scope.newFile.description,
				LCID: scope.newFile.LCID
			};

			var requestData = proximisRestApiDefinition.getData(scope.config.fileUploadService, 'POST', params);
			proximisRestApi.upload(scope.newFile.file, requestData).then(
				function(result) {
					addToSelection(result.data.item);
					proximisMessages.success(proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf'),
						proximisCoreI18n.trans('m.rbs.ua.common.success_file_uploaded | ucf'), null, 5000);
					scope.newFile.uploading = false;
					scope.newFile.file = null;
					scope.newFile.label = null;
					scope.newFile.title = null;
					scope.newFile.fdescriptionile = null;
				},
				function(result) {
					proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'), result.data.status.message, null, 60000);
					scope.newFile.uploading = false;
				}
			);
		};

		scope.newImage = {
			uploading: false,
			file: null,
			LCID: null,
			label: null,
			alt: null,
			title: null
		};

		scope.uploadImage = function() {
			if (!scope.newImage.file || !scope.newImage.label) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'),
					proximisCoreI18n.trans('m.rbs.ua.common.error_while_uploading_image | ucf'), null, 60000);
				return;
			}

			scope.newImage.uploading = true;
			var params = {
				label: scope.newImage.label,
				alt: scope.newImage.alt,
				title: scope.newImage.title,
				LCID: scope.newImage.LCID
			};
			var requestData = proximisRestApiDefinition.getData(scope.config.imageUploadService, 'POST', params);
			proximisRestApi.upload(scope.newImage.file, requestData).then(
				function(result) {
					addToSelection(result.data.item);
					proximisMessages.success(proximisCoreI18n.trans('m.rbs.ua.common.action_success_title | ucf'),
						proximisCoreI18n.trans('m.rbs.ua.common.success_image_uploaded | ucf'), null, 5000);
					scope.newImage.uploading = false;
					scope.newImage.file = null;
					scope.newImage.label = null;
					scope.newImage.alt = null;
					scope.newImage.title = null;
				},
				function(result) {
					proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'), result.data.status.message, null, 60000);
					scope.newImage.uploading = false;
				}
			);
		};

		scope.data = {
			documentIds: []
		};

		scope.ok = function() {
			var result = {
				documentIds: scope.data.documentIds
			};

			scope.$close({ action: 'insert', data: result });
		};

		scope.disableOkButton = function() {
			return !scope.data.documentIds.length;
		};

		var addToSelection = function(element) {
			// Angular's ngModel update events will be triggered only if the whole array is affected to the ngModel variable.
			scope.data.documentIds = scope.data.documentIds.concat(element._meta.id);
		};

		scope.$watch('newImage.file', function(newVal, oldVal) {
			var media = ProximisGlobal.media;
			if (media && newVal && !angular.equals(newVal, oldVal)) {
				if (!checkFileRequirements(newVal, media.image)) {
					scope.newImage.file = null;
				}
			}
		});

		scope.$watch('newFile.file', function(newVal, oldVal) {
			var media = ProximisGlobal.media;
			if (media && newVal && !angular.equals(newVal, oldVal)) {
				if (!checkFileRequirements(newVal, media.file)) {
					scope.newFile.file = null;
				}
			}
		});

		function checkFileRequirements(newVal, mediaConfiguration) {
			var splittedName = newVal.name.split('.');
			if (mediaConfiguration.allowedExtensions.indexOf(splittedName[splittedName.length - 1]) === -1) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'),
					proximisCoreI18n.trans('m.rbs.ua.common.error_file_extension_forbidden | ucf', {
						EXTENSION: splittedName[splittedName.length - 1],
						ALLOWED_EXTENSIONS: mediaConfiguration.allowedExtensions.join(', ')
					}), null, 60000);
				return false;
			}
			else if (newVal.size >= mediaConfiguration.maxSize) {
				proximisMessages.error(proximisCoreI18n.trans('m.rbs.ua.common.action_error_title | ucf'),
					proximisCoreI18n.trans('m.rbs.ua.common.error_file_too_big | ucf',
						{ MAX_FILE_SIZE: proximisCoreI18n.formatFileSize(mediaConfiguration.maxSize) }), null, 60000);
				return false;
			}
			return true;
		}
	}
})();