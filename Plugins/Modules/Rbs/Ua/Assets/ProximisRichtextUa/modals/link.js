/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	app.controller('ProximisRichtextUaModalsLinkController', ['$scope', 'linkData', 'existingAnchors', 'proximisRichtextUaCKEditor',
		ProximisRichtextUaModalsLinkController]);

	function ProximisRichtextUaModalsLinkController(scope, linkData, existingAnchors, proximisRichtextUaCKEditor) {
		scope.config = {
			documentSelectorSearchService: proximisRichtextUaCKEditor.getDocumentSelectorSearchService(),
			documentSelectorLoadService: proximisRichtextUaCKEditor.getDocumentSelectorLoadService(),
			protocols: ['http://', 'https://'],
			anchors: existingAnchors,
			needsText: linkData.needsText,
			allowDeletion: linkData.allowDeletion
		};

		scope.config.allowInternal = !!(scope.config.documentSelectorSearchService && scope.config.documentSelectorLoadService);

		scope.data = {
			text: linkData.text,
			title: linkData.title,
			target: linkData.target === '_blank' ? '_blank' : ''
		};

		if (linkData.href) {
			if (linkData.href.substring(0, 1) === '#') {
				scope.data.anchor = linkData.href.substring(1);
				scope.data.type = 'anchor';
			}
			else if (linkData.href.substring(0, 7) === 'http://') {
				scope.data.protocol = 'http://';
				scope.data.url = linkData.href.substring(7);
				scope.data.type = 'external';
			}
			else if (linkData.href.substring(0, 7) === 'https://') {
				scope.data.protocol = 'https://';
				scope.data.url = linkData.href.substring(7);
				scope.data.type = 'external';
			}
			else {
				scope.data.protocol = '';
				scope.data.url = linkData.href;
				scope.data.type = 'external';
			}
		}
		else if (linkData.documentId) {
			scope.data.documentId = parseInt(linkData.documentId);
			scope.data.type = 'internal';
		}

		if (!scope.data.type) {
			scope.data.type = scope.config.allowInternal ? 'internal' : 'external';
		}

		scope.$watch('data.type', function() {
			if (scope.data.type === 'external' && !scope.data.protocol && !scope.data.url) {
				scope.data.protocol = 'http://';
			}
		});

		scope.ok = function() {
			var result = {
				title: scope.data.title,
				target: scope.data.target,
				type: scope.data.type
			};
			if (scope.config.needsText) {
				result.linkText = scope.data.text;
			}

			switch (scope.data.type) {
				case 'internal':
					result.documentId = scope.data.documentId;
					break;
				case 'external':
					result.href = scope.data.protocol + scope.data.url;
					break;
				case 'anchor':
					result.href = '#' + scope.data.anchor;
					break;
				default:
					console.error('[ProximisRichtextUaModalsLinkController.ok] Invalid type: ' + scope.data.type);
					break;
			}

			scope.$close({ action: 'save', data: result });
		};

		scope.disableOkButton = function() {
			return (scope.config.needsText && !scope.data.text) || (scope.data.type === 'internal' && !scope.data.documentId) ||
				(scope.data.type === 'external' && !scope.data.url) || (scope.data.type === 'anchor' && !scope.data.anchor);
		};

		scope.delete = function() {
			scope.$close({ action: 'delete' });
		};
	}
})();