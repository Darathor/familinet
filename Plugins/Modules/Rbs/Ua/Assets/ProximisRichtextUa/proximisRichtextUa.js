/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	/**
	 * @ngdoc module
	 * @name proximisRichtextUa
	 *
	 * @description
	 * The ProximisRichtextUa module contains extensions for {@link proximisRichtext `ProximisRichtext`} for UA interfaces.
	 *
	 * ## Naming conventions for this module
	 *
	 * All component names are camel-cased and prefixed, the prefix depends on the component type:
	 *  * controllers, constants and objects are prefixed by `ProximisRichtextUa` (with an upper cased first letter)
	 *  * services and providers are prefixed by `proximisRichtextUa` (with an lower cased first letter)
	 *  * directives and filter are prefixed by `pxRichtextUa`
	 */
	var app = angular.module('proximisRichtextUa', ['proximis', 'proximisRichtext']);

	// Configuration.
	app.config(['proximisRichtextCKEditorProvider', function(proximisRichtextCKEditorProvider) {
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisLink');
		proximisRichtextCKEditorProvider.addExtraPlugin('proximisMedia');
	}]);
})(window.__change);