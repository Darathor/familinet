/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function() {
	'use strict';

	var app = angular.module('proximisRichtextUa');

	/**
	 * @ngdoc provider
	 * @id proximisRichtextUa.provider:proximisRichtextUaCKEditorProvider
	 * @name proximisRichtextUaCKEditorProvider
	 *
	 * @description
	 * Use `proximisRichtextUaCKEditorProvider` to configure `proximisRichtextUaCKEditor`.
	 */
	app.provider('proximisRichtextUaCKEditor', proximisRichtextUaCKEditorProvider);

	function proximisRichtextUaCKEditorProvider() {
		var documentSelectorSearchService;
		var documentSelectorLoadService;
		var imageListService;
		var fileUploadService;
		var imageUploadService;
		var mediaLoadService;
		var mediaInsertionModal;

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setDocumentSelectorSearchService
		 *
		 * @description
		 * Sets the webservice name for document selector search.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setDocumentSelectorSearchService = function(name) {
			documentSelectorSearchService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setDocumentSelectorLoadService
		 *
		 * @description
		 * Sets the webservice name for document selector load.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setDocumentSelectorLoadService = function(name) {
			documentSelectorLoadService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setImageListService
		 *
		 * @description
		 * Sets the webservice name for image list.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setImageListService = function(name) {
			imageListService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setFileUploadService
		 *
		 * @description
		 * Sets the webservice name for file upload.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setFileUploadService = function(name) {
			fileUploadService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setImageUploadService
		 *
		 * @description
		 * Sets the webservice name for image upload.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setImageUploadService = function(name) {
			imageUploadService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setMediaLoadService
		 *
		 * @description
		 * Sets the webservice name for media description load.
		 *
		 * @param {string} name The webservice name.
		 */
		this.setMediaLoadService = function(name) {
			mediaLoadService = name;
		};

		/**
		 * @ngdoc method
		 * @methodOf proximisRichtextUaCKEditorProvider
		 * @name setMediaInsertionModal
		 *
		 * @description
		 * Sets the modal name for media insertion.
		 *
		 * @param {string} name The modal name.
		 */
		this.setMediaInsertionModal = function(name) {
			mediaInsertionModal = name;
		};

		this.$get = [function() {
			/**
			 * @ngdoc service
			 * @id proximisRichtextUa.service:proximisRichtextUaCKEditor
			 * @name proximisRichtextUaCKEditor
			 *
			 * @description
			 * Provides methods used to parameterize CKEditor.
			 */
			return {
				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getDocumentSelectorSearchService
				 *
				 * @description
				 * Get the webservice name for document selector load.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getDocumentSelectorSearchService: function() {
					return documentSelectorSearchService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getDocumentSelectorLoadService
				 *
				 * @description
				 * Get the webservice name for document selector load.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getDocumentSelectorLoadService: function() {
					return documentSelectorLoadService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getImageListService
				 *
				 * @description
				 * Get the webservice name for image list.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getImageListService: function() {
					return imageListService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getFileUploadService
				 *
				 * @description
				 * Get the webservice name for file upload.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getFileUploadService: function() {
					return fileUploadService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getImageUploadService
				 *
				 * @description
				 * Get the webservice name for image upload.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getImageUploadService: function() {
					return imageUploadService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getMediaLoadService
				 *
				 * @description
				 * Get the webservice name for media description load.
				 *
				 * @returns {string|null} The webservice name.
				 */
				getMediaLoadService: function() {
					return mediaLoadService;
				},

				/**
				 * @ngdoc method
				 * @methodOf proximisRichtextUaCKEditor
				 * @name getMediaInsertionModal
				 *
				 * @description
				 * Gets the modal name for media insertion.
				 *
				 * @returns {string} The modal name.
				 */
				getMediaInsertionModal: function() {
					return mediaInsertionModal || 'proximisRichtextUa/insertMedia';
				}
			};
		}];
	}
})();

