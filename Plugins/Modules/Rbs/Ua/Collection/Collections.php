<?php
/**
 * Copyright (C) 2016 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Collection;

/**
 * @name \Rbs\Ua\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addUISupportedLCIDs(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$items = [];
			foreach (['fr_FR', 'en_US'] as $LCID)
			{
				$items[$LCID] = $applicationServices->getI18nManager()->transLCID($LCID, ['ucf']);
			}
			$collection = new \Change\Collection\CollectionArray('Rbs_Ua_UISupportedLCIDs', $items);
			$event->setParam('collection', $collection);
		}
	}
}