<?php

/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\FileStorage;

class Helper
{
	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @param array $file
	 * @param string $storageName
	 * @param string $path
	 * @return string The path of the file in the storage
	 */
	public static function moveFileToStorage(\Change\Storage\StorageManager $storageManager, array $file, $storageName, $path = '/')
	{
		if (!isset($file['tmp_name']))
		{
			throw new \RuntimeException('Empty tmp_name for file to upload', 999999);
		}

		$destinationPath = $storageManager->buildChangeURI($storageName, $path . uniqid('', false) . '_' . $file['name'])->normalize()->toString();

		if (!move_uploaded_file($file['tmp_name'], $destinationPath))
		{
			throw new \RuntimeException('Unable to move "' . $file['tmp_name'] . '" in "' . $destinationPath . '"', 999999);
		}

		$itemInfo = $storageManager->getItemInfo($destinationPath);
		if (!$itemInfo || !$itemInfo->isReadable())
		{
			throw new \RuntimeException('Unable to find or read: ' . $destinationPath, 999999);
		}

		return $destinationPath;
	}
}