<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\ActionTrait
 */
trait ActionTrait
{
	/**
	 * @param \Zend\Stdlib\Parameters|array $inputParameters
	 * @return array|null
	 */
	protected function resolveFilter($inputParameters)
	{
		$filter = $inputParameters->get('filter');
		if ($filter)
		{
			if (is_string($filter))
			{
				$filter = json_decode($filter, true);
			}
			if (is_array($filter))
			{
				return $filter;
			}
			throw new \RuntimeException('Invalid filter: ' . $filter, 999999);
		}
		return null;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @param array $context
	 * @param \Change\Documents\Query\Query $query
	 * @return array
	 */
	protected function getDocumentListResult($modelManager, $context, $query)
	{
		if ($filter = $context['filter'] ?? null)
		{
			if (is_string($filter))
			{
				$filter = json_decode($filter, true);
			}
			if (is_array($filter))
			{
				$modelManager->applyDocumentFilter($query, $filter);
			}
			else
			{
				throw new \RuntimeException('Invalid filter: ' . $context['filter'], 999999);
			}
		}

		$items = [];
		$count = $query->getCountDocuments();
		$pagination = $this->resolvePagination($context, $count);

		if ($pagination['limit'])
		{
			$desc = $context['desc'] ?? true;
			if (isset($context['sort']))
			{
				$this->applySortForDocument($query, $context['sort'], $desc);
			}
			$query->addOrder('id', !$desc);

			$items = $query->getDocuments($pagination['offset'], $pagination['limit'])->preLoad()->toArray();
		}
		return ['pagination' => $pagination, 'items' => $items];
	}

	/**
	 * @param array $context
	 * @param \Change\Db\Query\Builder $qbCount
	 * @param \Change\Db\Query\Builder $qb
	 * @param callable $rowConverter
	 * @param string|null $table
	 * @return array
	 */
	protected function getListResult($context, $qbCount, $qb, $rowConverter, $table = null)
	{
		$this->applyFilters($context, [$qbCount, $qb], $table);
		$this->applySortForList($qb, $context['sort'] ?? null, $context['desc'] ?? true, $table);

		$queryCount = $qbCount->query();
		$count = $queryCount->getFirstResult($queryCount->getRowsConverter()->addIntCol('count'));
		$pagination = $this->resolvePagination($context, $count);

		$items = [];
		if ($pagination['limit'])
		{
			$query = $qb->query();
			$query->setMaxResults($pagination['limit']);
			$query->setStartIndex($pagination['offset']);
			$items = $query->getResults($rowConverter);
		}
		return ['pagination' => $pagination, 'items' => $items];
	}

	/**
	 * @param array $context
	 * @param \Change\Db\Query\Builder[] $qbArray
	 * @param string|null $table
	 */
	protected function applyFilters($context, $qbArray, $table = null)
	{
		$filter = $context['filter'] ?? null;
		if ($filter)
		{
			if (is_string($filter))
			{
				$filter = json_decode($filter, true);
			}
			if (is_array($filter))
			{
				foreach ($qbArray as $qb)
				{
					$where = $this->getRestrictionForList($qb, $filter, $table);
					if ($where)
					{
						$qb->andWhere($where);
					}
				}
			}
			else
			{
				throw new \RuntimeException('Invalid filter: ' . $context['filter'], 999999);
			}
		}
	}

	/**
	 * @param \Change\Documents\Query\Query $query
	 * @param string $sort
	 * @param boolean $desc
	 */
	protected function applySortForDocument($query, $sort, $desc)
	{
		if (strpos($sort, '.') > 0)
		{
			[$property, $sort] = explode('.', $sort);
			$subQuery = $query->getPropertyBuilder($property);
			if ($subQuery->getModel()->getProperty($sort))
			{
				$query->addOrder($subQuery->getColumn($sort), !$desc);
			}
			else
			{
				throw new \RuntimeException('Property ' . $sort . ' does not exist on ' . $subQuery->getModel()->getName());
			}
		}
		else
		{
			if ($query->getModel()->getProperty($sort))
			{
				$query->addOrder($sort, !$desc);
			}
			else
			{
				throw new \RuntimeException('Property ' . $sort . ' does not exist on ' . $query->getModel()->getName());
			}
		}
	}

	/**
	 * @param array|null $context
	 * @param integer $count
	 * @returns array
	 */
	protected function resolvePagination($context, $count)
	{
		$pagination = ['offset' => (int)($context['offset'] ?? 0), 'limit' => (int)($context['limit'] ?? 10), 'count' => (int)$count];
		if ($pagination['count'] < $pagination['offset'])
		{
			$pagination['offset'] = 0;
		}
		return $pagination;
	}

	/**
	 * @param \Change\Db\Query\Builder $queryBuilder
	 * @param string|null $sort
	 * @param boolean $desc
	 * @param string|null $table
	 */
	protected function applySortForList($queryBuilder, $sort, $desc, $table = null)
	{
		if ($sort)
		{
			$fb = $queryBuilder->getFragmentBuilder();
			$column = $table ? $fb->column($sort, $table) : $fb->column($sort);
			if ($desc)
			{
				$queryBuilder->orderDesc($column);
			}
			else
			{
				$queryBuilder->orderAsc($column);
			}
		}
	}

	/**
	 * @param \Change\Db\Query\Builder $queryBuilder
	 * @param array $filter
	 * @param string|null $table
	 * @return \Change\Db\Query\Predicates\InterfacePredicate|null
	 */
	protected function getRestrictionForList($queryBuilder, $filter, $table = null)
	{
		if (isset($filter['filters']))
		{
			if ($filters = (array)$filter['filters'])
			{
				$restrictions = [];
				foreach ($filters as $subFilter)
				{
					$restriction = $this->getRestrictionForList($queryBuilder, $subFilter, $table);
					if ($restriction)
					{
						$restrictions[] = $restriction;
					}
				}
				if (count($restrictions))
				{
					if ($filter['parameters']['operator'] === 'OR')
					{
						return $queryBuilder->getFragmentBuilder()->logicOrFromArray($restrictions);
					}
					return $queryBuilder->getFragmentBuilder()->logicAndFromArray($restrictions);
				}
			}
			return null;
		}
		if (isset($filter['parameters']) && is_array($filter['parameters']))
		{
			$fb = $queryBuilder->getFragmentBuilder();
			$parameters = $filter['parameters'];
			if (isset($parameters['operator'], $parameters['propertyName']))
			{
				$column = $table ? $fb->column($parameters['propertyName'], $table) : $fb->column($parameters['propertyName']);
				if (isset($parameters['value']))
				{
					switch ($parameters['operator'])
					{
						case 'eq':
							return $fb->eq($column, $fb->string($parameters['value']));
						case 'neq':
							return $fb->neq($column, $fb->string($parameters['value']));
						case 'lte':
							return $fb->lte($column, $fb->string($parameters['value']));
						case 'lt':
							return $fb->lt($column, $fb->string($parameters['value']));
						case 'gte':
							return $fb->gte($column, $fb->string($parameters['value']));
						case 'gt':
							return $fb->gt($column, $fb->string($parameters['value']));
						case 'contains':
							return $fb->like($column, $fb->string($parameters['value']), \Change\Db\Query\Predicates\Like::ANYWHERE);
						case 'beginsWith':
							return $fb->like($column, $fb->string($parameters['value']), \Change\Db\Query\Predicates\Like::BEGIN);
						case 'endsWith':
							return $fb->like($column, $fb->string($parameters['value']), \Change\Db\Query\Predicates\Like::END);
						case 'isNull':
							return $fb->isNull($column);
						case 'isNotNull':
							return $fb->isNotNull($column);
						case 'in':
							return $fb->in($column, (array)$parameters['value']);
						case 'notIn':
							return $fb->notIn($column, (array)$parameters['value']);
					}
				}
				else
				{
					switch ($parameters['operator'])
					{
						case 'isNull':
							return $fb->isNull($column);
						case 'isNotNull':
							return $fb->isNotNull($column);
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface|null $fallBackContext
	 * @param array|null $inputFields
	 * @param int $httpStatus
	 * @return null|\Rbs\Ua\Http\API\ArrayResult
	 */
	protected function buildArrayResult(\Change\Http\Event $event, $value,
		\Rbs\Ua\Model\FallbackContextInterface $fallBackContext = null, array $inputFields = [], $httpStatus = 200)
	{
		/** @var \Rbs\Ua\Model\Models $models */
		$models = $event->getParam('models');
		/** @var \Rbs\Ua\API\Operation $operation */
		$operation = $event->getParam('operation');
		if ($models instanceof \Rbs\Ua\Model\Models && $operation instanceof \Rbs\Ua\API\Operation)
		{
			$responseModel = $operation->getResponseModel($inputFields, $httpStatus);
			if ($responseModel)
			{
				return new \Rbs\Ua\Http\API\ArrayResult($responseModel->normalize($value, $fallBackContext), $httpStatus);
			}
		}
		return null;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string $message
	 * @return null|\Rbs\Ua\Http\API\ArrayResult
	 */
	protected function buildSuccessResult(\Change\Http\Event $event, $message)
	{
		return $this->buildArrayResult($event, ['status' => ['message' => $message]]);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string $message
	 * @param string $errorCode
	 * @param array|null $data
	 * @param int $httpStatus
	 * @return null|\Rbs\Ua\Http\API\ArrayResult
	 */
	protected function buildErrorResult(\Change\Http\Event $event, $message, $errorCode = 'ERROR', array $data = null, int $httpStatus = 500)
	{
		$resultData = ['status' => ['message' => $message, 'error' => true, 'errorCode' => $errorCode]];
		if ($data)
		{
			$resultData['data'] = $data;
		}
		return $this->buildArrayResult($event, $resultData, null, [], $httpStatus);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @return \DateTimeZone
	 */
	protected function initUserTimeZone(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$user = $applicationServices->getAuthenticationManager()->getCurrentUser();
		$up = $applicationServices->getProfileManager()->loadProfile($user, 'Change_User');
		if ($up instanceof \Change\User\UserProfile && $timeZone = $up->getTimeZone())
		{
			$i18nManager->setTimeZone($timeZone);
		}
		return $i18nManager->getTimeZone();
	}
}