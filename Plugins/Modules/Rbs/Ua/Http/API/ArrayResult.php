<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\ArrayResult
 */
class ArrayResult extends \Change\Http\Result implements \JsonSerializable
{
	/**
	 * @var array
	 */
	protected $array;

	/**
	 * @param array $array
	 * @param int $httpStatusCode
	 */
	public function __construct(array $array = [], $httpStatusCode = \Zend\Http\Response::STATUS_CODE_200)
	{
		$this->array = $array;
		parent::__construct($httpStatusCode);
		$this->getHeaders()->addHeaderLine('Content-Type: application/json');
	}

	/**
	 * @return array|null
	 */
	public function getArray()
	{
		return $this->array;
	}

	/**
	 * @param array|null $array
	 * @return $this
	 */
	public function setArray($array)
	{
		$this->array = $array;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function toArray()
	{
		return (array)$this->array;
	}

	/**
	 * @return array|null
	 */
	public function jsonSerialize()
	{
		return $this->array ?: null;
	}
}