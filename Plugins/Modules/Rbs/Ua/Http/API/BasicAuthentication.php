<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\BasicAuthentication
 */
class BasicAuthentication
{
	public function onAuthenticate(\Change\Http\Event $event)
	{
		$request = $event->getRequest();
		$authorization = $this->parseAuthorizationHeader($request->getHeader('Authorization'));
		if (!$authorization || !($realm = $event->getParam('serverRealm')))
		{
			return;
		}
		$applicationServices = $event->getApplicationServices();
		$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_User_User');
		$groupBuilder = $query->getPropertyBuilder('groups');
		$query->andPredicates($query->activated(), $query->eq('login', $authorization['login']), $groupBuilder->eq('realm', $realm));

		$password = $authorization['password'];
		foreach ($query->getDocuments()->preLoad()->toArray() as $document)
		{
			/* @var $document \Rbs\User\Documents\User */
			if ($document->checkPassword($password))
			{
				$event->getAuthenticationManager()->setCurrentUser(new \Rbs\User\Events\AuthenticatedUser($document));
				break;
			}
		}
	}

	/**
	 * @param \Zend\Http\Header\HeaderInterface $authorization
	 * @return array
	 */
	protected function parseAuthorizationHeader($authorization)
	{
		if ($authorization instanceof \Zend\Http\Header\Authorization)
		{
			$rawHeader = $authorization->getFieldValue();
			if (strpos($rawHeader, 'Basic') === 0)
			{
				$p = explode(':', base64_decode(substr($rawHeader, 6)));
				if (count($p) > 1)
				{
					$login = array_shift($p);
					$password = implode(':', $p);
					return ['login' => $login, 'password' => $password];
				}
			}
		}
		return [];
	}
}