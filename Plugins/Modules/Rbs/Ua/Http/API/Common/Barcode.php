<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Barcode
 */
class Barcode
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Zend\Barcode\Exception\ExceptionInterface
	 */
	public function getBarcodes($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$config = $inputParameters->get('config');
		$texts = $inputParameters->get('texts');

		$barcodesSrc = $this->generateBarcodes($config, $texts, $event->getApplicationServices()->getI18nManager());

		$event->setResult($this->buildArrayResult($event, ['items' => $barcodesSrc]));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 * @throws \Zend\Barcode\Exception\ExceptionInterface
	 */
	public function getBarcodesToPrint($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$config = $inputParameters->get('config');
		$texts = $inputParameters->get('texts');

		$barcodesSrc = $this->generateBarcodes($config, $texts, $event->getApplicationServices()->getI18nManager());

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$resources = $genericServices->getUaManager()->getResources('Rbs', 'Ua', 'common');
		$html = $resources->renderModuleTemplateFile('Rbs_Ua', 'Ua/print/barcode-list.twig', ['barcodesSrc' => $barcodesSrc]);

		$result = $this->buildArrayResult($event, ['item' => ['html' => $html]]);
		$event->setResult($result);
	}

	/**
	 * @param $config
	 * @param $texts
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return array
	 * @throws \Zend\Barcode\Exception\ExceptionInterface
	 */
	protected function generateBarcodes($config, $texts, $i18nManager):array
	{
		if (count($texts) > 100)
		{
			throw new \RuntimeException($i18nManager->trans('m.rbs.ua.common.max_barcode_count_error'));
		}

		foreach ($texts as $text)
		{
			if (strlen($text) > 256)
			{
				throw new \RuntimeException($i18nManager->trans('m.rbs.ua.common.max_barcode_text_length_error'));
			}
		}

		$format = $config['format'] ?? 'code128';
		$options = [];

		$factor = $config['factor'] ?? 1;
		if ($factor >= 1)
		{
			$options['factor'] = $factor;
		}

		$barHeight = $config['barHeight'] ?? 50;
		if ($barHeight >= 1)
		{
			$options['barHeight'] = $barHeight;
		}

		if (!($config['drawText'] ?? true))
		{
			$options['drawText'] = false;
		}
		else
		{
			$options['stretchText'] = $config['stretchText'] ?? false;
			$font = $config['font'] ?? null;
			if ($font && is_readable($font))
			{
				$options['font'] = $font;
			}

			$fontSize = $config['fontSize'] ?? 0;
			if ($fontSize)
			{
				$options['fontSize'] = $fontSize;
			}
		}

		$barcodesSrc = [];
		foreach ($texts as $text)
		{
			$options['text'] = $text;
			$barcode = \Zend\Barcode\Barcode::factory($format, 'image', $options, []);
			if ($barcode instanceof \Zend\Barcode\Renderer\Image)
			{
				ob_start();
				$barcode->render();
				$contents = ob_get_contents();
				ob_end_clean();
				$barcodesSrc[$text] = 'data:image/png;base64,' . base64_encode($contents);
			}
		}

		return $barcodesSrc;
	}
}