<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Collection
 */
class Collection
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getByCode($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$modelsFallback = new \Rbs\Ua\Http\API\Models($event);
		$modelsFallback->setImageFormats($inputParameters['imageFormats'] ?? null);

		$code = $inputParameters['code'];
		$params = is_array($inputParameters['params']) ? $inputParameters['params'] : [];
		$collection = $event->getApplicationServices()->getCollectionManager()->getCollection($code, $params);
		$result = $this->buildArrayResult($event, ['item' => $collection], $modelsFallback);
		$event->setResult($result);
	}
}