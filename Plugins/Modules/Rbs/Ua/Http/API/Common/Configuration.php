<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Configuration
 */
class Configuration
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$application = $event->getApplication();
		$version = $application->getConfiguration('Change/Application/version');
		$lcids = (array)$application->getConfiguration('Change/I18n/supported-lcids');
		$langs = [];
		foreach ($lcids as $lcid)
		{
			$langs[] = ['label' => $i18nManager->transLCIDInSelf($lcid, ['ucf']), 'value' => $lcid];
		}

		$data = ['version' => $version, 'supportedLCIDs' => $langs];

		$data = $this->appendConfiguration($application, $data);

		$result = $this->buildArrayResult($event, ['item' => $data]);
		$result->setHeaderNoCache();
		$event->setResult($result);
	}

	/**
	 * @param \Change\Application $application
	 * @param array $configuration
	 * @return array
	 */
	protected function appendConfiguration(\Change\Application $application, array $configuration)
	{
		return $configuration;
	}
}
