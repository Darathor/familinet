<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\DocumentModel
 */
class DocumentModel
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getList($event)
	{
		$documentModels = [];

		$applicationServices = $event->getApplicationServices();

		$i18n = $applicationServices->getI18nManager();
		$modelManager = $applicationServices->getModelManager();
		foreach ($modelManager->getModelsNames() as $modelName)
		{
			$model = $modelManager->getModelByName($modelName);
			if (!$model || $model->isInline() || $model->isAbstract())
			{
				continue;
			}

			$pluginKey = strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
			$pluginLabel = $i18n->trans($pluginKey, ['ucf']);
			if ($pluginKey == $pluginLabel)
			{
				continue;
			}

			$documentModels[] = [
				'name' => $model->getName(),
				'label' => $i18n->trans($model->getLabelKey(), ['ucf']),
				'pluginLabel' => $pluginLabel,
				'publishable' => $model->isPublishable(),
				'activable' => $model->isActivable(),
				'editable' => $model->isEditable(),
				'instanceOf' => array_merge([$model->getName()], $model->getAncestorsNames())
			];
		}
		$modelsFallback = new \Rbs\Ua\Http\API\Models($event);
		$result = $this->buildArrayResult($event, ['items' => $documentModels], $modelsFallback);
		$event->setResult($result);
	}
}