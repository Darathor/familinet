<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Filter
 */
class Filter
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function list(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$contextName = $event->getParam('applicationName') . '/' . $inputParameters->get('contextName');
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();

		$filters = $applicationServices->getDbProvider()->getFilterStorage()->getByContextForUserIds($contextName, [$userId, 0]);

		$event->setResult($this->buildArrayResult($event, ['items' => $filters]));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 */
	public function insert(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$contextName = $event->getParam('applicationName') . '/' . $inputParameters->get('contextName');
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();

		$filter = new \Change\Db\Filters\Filter($contextName, $userId, $inputParameters->get('content'), $inputParameters->get('label'));

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$applicationServices->getDbProvider()->getFilterStorage()->insert($filter);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
		}

		$event->setResult($this->buildArrayResult($event, ['item' => $filter]));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 */
	public function update(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$filterStorage = $applicationServices->getDbProvider()->getFilterStorage();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$contextName = $event->getParam('applicationName') . '/' . $inputParameters->get('contextName');
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();

		$filter = $filterStorage->load($inputParameters->get('filterId'));
		if (!$filter || $filter->getContextName() !== $contextName || $filter->getUserId() !== $userId)
		{
			throw new \Change\Http\HttpException($applicationServices->getI18nManager()->trans('m.rbs.ua.common.invalid_filter'), 99999, null, 409);
		}

		$content = $inputParameters->get('content');
		if (is_array($content))
		{
			$filter->setContent($content);
		}

		$label = $inputParameters->get('label');
		if ($label)
		{
			$filter->setLabel($label);
		}

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$applicationServices->getDbProvider()->getFilterStorage()->update($filter);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
		}

		$event->setResult($this->buildArrayResult($event, ['item' => $filter]));
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 */
	public function delete(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$filterStorage = $applicationServices->getDbProvider()->getFilterStorage();

		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$contextName = $event->getParam('applicationName') . '/' . $inputParameters->get('contextName');
		$userId = $applicationServices->getAuthenticationManager()->getCurrentUser()->getId();

		$filter = $filterStorage->load($inputParameters->get('filterId'));
		if (!$filter || $filter->getContextName() !== $contextName || $filter->getUserId() !== $userId)
		{
			throw new \Change\Http\HttpException($applicationServices->getI18nManager()->trans('m.rbs.ua.common.invalid_filter'), 99999, null, 409);
		}

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$applicationServices->getDbProvider()->getFilterStorage()->delete($filter);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$transactionManager->rollBack($e);
		}

		$event->setResult($this->buildSuccessResult($event, $applicationServices->getI18nManager()->trans('m.rbs.ua.common.filter_deleted')));
	}
}