<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Media
 */
class Media
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 * inputParameters:
	 *   imageId
	 *   maxHeight optional
	 *   maxWidth optional
	 */
	public function getDisplayData($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document = $documentManager->getDocumentInstance($inputParameters->get('imageId'));

		if ($document instanceof \Rbs\Media\Documents\Image)
		{
			$maxHeight = $inputParameters->get('maxHeight', 0);
			$maxWidth = $inputParameters->get('maxWidth', 0);
			/** @noinspection PhpUndefinedFieldInspection */
			$document->publicUrl = $document->getPublicURL($maxWidth, $maxHeight);
			$result = $this->buildArrayResult($event, ['item' => $document]);
			$event->setResult($result);
		}
	}
}