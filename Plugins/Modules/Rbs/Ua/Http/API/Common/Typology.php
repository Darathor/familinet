<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\Typology
 */
class Typology
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * Event params:
	 *  models
	 *  responseType
	 *  inputParameters:
	 *   typologyId The typology ID
	 *   documentModelName The model of document
	 *   context An optional visibility context to filtrer attributes
	 * @param \Change\Http\Event $event
	 */
	public function attributes(\Change\Http\Event $event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$typologyId = $inputParameters->get('typologyId');

		$applicationServices = $event->getApplicationServices();
		$collectionManager = $applicationServices->getCollectionManager();

		$typology = $applicationServices->getDocumentManager()->getDocumentInstance($typologyId, 'Rbs_Generic_Typology');
		if (!($typology instanceof \Rbs\Generic\Documents\Typology))
		{
			return;
		}

		$contextName = $inputParameters->get('context');
		$visibilities = null;
		if ($contextName)
		{
			$visibilities = $typology->getVisibilities()[$contextName] ?? [];
		}

		$groups = [];
		foreach ($typology->getGroups() as $group)
		{
			$attributes = [];
			foreach ($group->getAttributes() as $attribute)
			{
				// If a visibility context is given, only return attributes visible in this context.
				if ($contextName && !($visibilities[$attribute->getId()] ?? false))
				{
					continue;
				}

				$attributes[] = $this->addAPIPropertiesForAttribute($attribute, $collectionManager);
			}

			if ($attributes)
			{
				\Rbs\Ua\Model\ObjectModel::addAPIProperties($group, [
					'attributes' => $attributes
				]);
				$groups[] = $group;
			}
		}

		\Rbs\Ua\Model\ObjectModel::addAPIProperties($typology, [
			'groups' => $groups
		]);

		$event->setResult($this->buildArrayResult($event, ['item' => $typology], new \Rbs\Ua\Http\API\Models($event)));
	}

	/**
	 * @param \Rbs\Generic\Documents\Attribute $attribute
	 * @param \Change\Collection\CollectionManager $collectionManager
	 * @return \Rbs\Generic\Documents\Attribute
	 */
	public function addAPIPropertiesForAttribute($attribute, $collectionManager)
	{
		\Rbs\Ua\Model\ObjectModel::addAPIProperties($attribute, [
			'name' => $attribute->getName() ?: ('attr_' . $attribute->getId()),
			'type' => $attribute->getValueType(),
			'required' => (bool)$attribute->getRequiredValue(),
			'localized' => (bool)$attribute->getLocalizedValue()
		]);

		$collectionCode = $attribute->getCollectionCode();
		if ($collectionCode)
		{
			$collection = $collectionManager->getCollection($collectionCode);
			if ($collection)
			{
				\Rbs\Ua\Model\ObjectModel::addAPIProperties($attribute, [
					'options' => $collection->getItems()
				]);
			}
		}
		return $attribute;
	}
}