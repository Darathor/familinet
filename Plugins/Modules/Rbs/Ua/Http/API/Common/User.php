<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API\Common;

/**
 * @name \Rbs\Ua\Http\API\Common\User
 */
class User
{
	use \Rbs\Ua\Http\API\ActionTrait;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function getCurrent($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');

		$currentUser = $event->getAuthenticationManager()->getCurrentUser();
		/** @var \Rbs\User\Documents\User $user */
		$user = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($currentUser->getId(), 'Rbs_User_User');

		if (!$user)
		{
			return;
		}

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$profileManager = $event->getApplicationServices()->getProfileManager();

		$profiles = [];
		$profileNames = $inputParameters['profileNames'] ?? [];
		if (!is_array($profileNames) || !count($profileNames))
		{
			$profileNames = ['Change_User'];
		}

		foreach ($profileNames as $profileName)
		{
			$profile = $profileManager->loadProfile($currentUser, $profileName);
			$profileData = [];
			if ($profile)
			{
				foreach ($profile->getPropertyNames() as $propertyName)
				{
					$propertyValue = $profile->getPropertyValue($propertyName);
					if (is_callable([$propertyValue, 'toArray']))
					{
						$propertyValue = $propertyValue->toArray();
					}
					$profileData[$propertyName] = $propertyValue;
				}

				if ($profileName === 'Change_User')
				{
					if ($profileData['LCID'] === null)
					{
						$profileData['LCID'] = $i18nManager->getLCID();
					}
					if ($profileData['TimeZone'] === null)
					{
						$profileData['TimeZone'] = $i18nManager->getTimeZone()->getName();
					}
				}
			}
			$profiles[$profileName] = $profileData;
		}
		/** @noinspection PhpUndefinedFieldInspection */
		$user->profiles = $profiles;

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$avatarManager = $genericServices->getAvatarManager();
		$avatarManager->setUrlManager($event->getUrlManager());
		$url = $avatarManager->getAvatarUrl(80, $user->getEmail(), $user, []);
		\Rbs\Ua\Model\ObjectModel::addAPIProperties($user, [
			'avatar' => $url
		]);

		$modelsFallback = new \Rbs\Ua\Http\API\Models($event);
		$modelsFallback->setImageFormats($inputParameters['imageFormats'] ?? null);
		$result = $this->buildArrayResult($event, ['item' => $user], $modelsFallback);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function putCurrent($event)
	{
		/** @var \Zend\Stdlib\Parameters $inputParameters */
		$inputParameters = $event->getParam('inputParameters');
		$profiles = $inputParameters->get('profiles', []);
		$i18n = $event->getApplicationServices()->getI18nManager();

		$tm = $event->getApplicationServices()->getTransactionManager();
		try
		{
			$tm->begin();

			$user = $event->getAuthenticationManager()->getCurrentUser();
			$profileManager = $event->getApplicationServices()->getProfileManager();

			foreach ($profiles as $name => $data)
			{
				$profile = $profileManager->loadProfile($user, $name);
				if ($profile)
				{
					foreach ($data as $propertyName => $propertyValue)
					{
						$profile->setPropertyValue($propertyName, $propertyValue);
					}
					$profileManager->saveProfile($user, $profile);
				}
			}

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		$message = $i18n->trans('m.rbs.ua.common.success_updating_profiles', ['ucf']);

		$status = ['message' => $message];
		$result = $this->buildArrayResult($event, ['status' => $status]);
		$event->setResult($result);
	}
}