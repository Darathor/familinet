<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\Models
 */
class Models implements \Rbs\Ua\Model\FallbackContextInterface
{
	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\User\ProfileManager
	 */
	protected $profileManager;

	/**
	 * @var \Rbs\Geo\GeoManager
	 */
	protected $geoManager;

	/**
	 * @var array|null
	 */
	protected $imageFormats;

	/**
	 * @var \Change\Http\Web\UrlManager
	 */
	protected $websiteUrlManager;

	/**
	 * @var \Change\Storage\StorageManager
	 */
	protected $storageManager;

	/**
	 * @var string
	 */
	protected $applicationName;

	/**
	 * @param \Change\Http\Event $event
	 */
	public function __construct(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$this->setDocumentManager($applicationServices->getDocumentManager());
		$this->setI18nManager($applicationServices->getI18nManager());
		$this->setProfileManager($applicationServices->getProfileManager());
		$this->setStorageManager($applicationServices->getStorageManager());
		$this->setApplicationName($event->getParam('applicationName'));

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$this->setGeoManager($genericServices->getGeoManager());
	}

	/**
	 * @return \Change\Documents\DocumentManager
	 */
	protected function getDocumentManager()
	{
		return $this->documentManager;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager($documentManager)
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	public function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\User\ProfileManager
	 */
	protected function getProfileManager()
	{
		return $this->profileManager;
	}

	/**
	 * @param \Change\User\ProfileManager $profileManager
	 * @return $this
	 */
	public function setProfileManager($profileManager)
	{
		$this->profileManager = $profileManager;
		return $this;
	}

	/**
	 * @return \Rbs\Geo\GeoManager
	 */
	public function getGeoManager()
	{
		return $this->geoManager;
	}

	/**
	 * @param \Rbs\Geo\GeoManager $geoManager
	 * @return $this
	 */
	public function setGeoManager($geoManager)
	{
		$this->geoManager = $geoManager;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getImageFormats()
	{
		return $this->imageFormats;
	}

	/**
	 * @param array|null $imageFormats
	 * @return $this
	 */
	public function setImageFormats(array $imageFormats = null)
	{
		$this->imageFormats = $imageFormats;
		return $this;
	}

	/**
	 * @return \Change\Http\Web\UrlManager
	 */
	public function getWebsiteUrlManager()
	{
		return $this->websiteUrlManager;
	}

	/**
	 * @param \Change\Http\Web\UrlManager $websiteUrlManager
	 * @return $this
	 */
	public function setWebsiteUrlManager($websiteUrlManager)
	{
		$this->websiteUrlManager = $websiteUrlManager;
		return $this;
	}

	/**
	 * @return \Change\Storage\StorageManager
	 */
	public function getStorageManager()
	{
		return $this->storageManager;
	}

	/**
	 * @param \Change\Storage\StorageManager $storageManager
	 * @return $this
	 */
	public function setStorageManager($storageManager)
	{
		$this->storageManager = $storageManager;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getApplicationName()
	{
		return $this->applicationName;
	}

	/**
	 * @param string $applicationName
	 * @return $this
	 */
	public function setApplicationName($applicationName)
	{
		$this->applicationName = $applicationName;
		return $this;
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @return mixed
	 */
	public function normalizeObject($object, $objectModel)
	{
		$type = $objectModel->getType();
		switch ($type)
		{
			case 'Rbs.Ua.AddressSummary':
				if (is_array($object))
				{
					$object = new \Rbs\Geo\Address\BaseAddress($object);
				}
				if ($object instanceof \Rbs\Geo\Address\BaseAddress && !$object->getLines())
				{
					$object->setLines($this->getGeoManager()->getFormattedAddress($object));
				}
				return $object;

			case 'Rbs.Ua.UserCommon':
				$profileManager = $this->getProfileManager();
				$profile = null;
				if ($object instanceof \Rbs\User\Documents\User)
				{
					$user = new \Rbs\User\Events\AuthenticatedUser($object);
					$profile = $profileManager->loadProfile($user, 'Rbs_User');
				}
				elseif ($object instanceof \Rbs\Order\Customer)
				{
					// If customer info is set in order, does not override data with the user's.
					// Fixes the case where the customer is not linked to a \Rbs\User\User (common in import contexts).
					$object->label = $object->getEmail();
					if (!$object->getFullName() && $object->getUserId())
					{
						/** @var \Rbs\User\Documents\User|null $docUser */
						$docUser = $this->getDocumentManager()->getDocumentInstance($object->getUserId(), 'Rbs_User_User');
						if ($docUser)
						{
							$user = new \Rbs\User\Events\AuthenticatedUser($docUser);
							$profile = $profileManager->loadProfile($user, 'Rbs_User');
						}
					}
				}

				if ($profile)
				{
					$object->RbsUserProfile = $profile;
					$object->fullName = trim(implode(' ', [$profile->getPropertyValue('firstName'), $profile->getPropertyValue('lastName')]));
					$object->phone = $profile->getPropertyValue('phone');
				}

				return $object;

			case 'Rbs.Ua.PhoneNumberData':
				$phoneData = $object;
				if (is_string($object))
				{
					try
					{
						$phoneData = $this->getGeoManager()->parsePhoneNumber($this->getI18nManager(), $object);
					}
					catch (\libphonenumber\NumberParseException $exc)
					{
						$phoneData = ['national' => $object];
					}
				}
				return $phoneData;
		}
		return $object;
	}

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @param string $propertyName
	 * @param boolean $resolved
	 * @return mixed
	 */
	public function resolveProperty($object, $objectModel, $propertyName, &$resolved)
	{
		$resolved = true;
		$key = $objectModel->getType() . '/' . $propertyName;
		switch ($key)
		{
			case 'Rbs.Ua.DocumentMeta/model':
				if ($object instanceof \Change\Documents\AbstractDocument)
				{
					return $object->getDocumentModelName();
				}
				return null;
			case 'Rbs.Ua.UserCommon/userId':
				if ($object instanceof \Rbs\User\Documents\User)
				{
					return $object->getId();
				}
				return null;
			case 'Rbs.Ua.AddressSummary/_meta':
				if ($object instanceof \Rbs\Geo\Address\BaseAddress)
				{
					$id = $object->getFieldValue('__id');
					if ($id)
					{
						return ['id' => (int)$id, 'model' => 'Rbs_Geo_BaseAddress'];
					}
				}
				elseif ($object instanceof \Rbs\Geo\Documents\Address)
				{
					return ['id' => $object->getId(), 'model' => $object->getDocumentModelName()];
				}
				return null;

			case 'Rbs.Ua.AddressSummary/common':
				if ($object instanceof \Rbs\Geo\Address\BaseAddress)
				{
					return [
						'addressFieldsId' => (int)$object->getFieldValue('__addressFieldsId'),
						'name' => $object->getFieldValue('__name')
					];
				}
				if ($object instanceof \Rbs\Geo\Documents\Address)
				{
					return [
						'addressFieldsId' => $object->getAddressFieldsId(),
						'name' => $object->getName()
					];
				}

				return null;
			case 'Rbs.Ua.ImageSummary/urls':
				if ($this->imageFormats && $object instanceof \Rbs\Media\Documents\Image)
				{
					$urls = [];
					/** @noinspection ForeachSourceInspection */
					foreach ($this->imageFormats as $format)
					{
						if (preg_match('/^\d*x\d*$/', $format))
						{
							[$maxWidth, $maxHeight] = explode('x', $format);
							$urls[$format] = $object->getPublicURL($maxWidth, $maxHeight);
						}
					}
					return $urls ?: null;
				}
				return null;

			case 'Rbs.Ua.ItemForSelector/label':
				if ($object instanceof \Change\Documents\Interfaces\Publishable)
				{
					return $object->getDocumentModel()->getPropertyValue($object, 'label');
				}
				return null;
			case 'Rbs.Ua.DocumentForSelector/modelLabel':
				if ($object instanceof \Change\Documents\AbstractDocument)
				{
					return $this->i18nManager->trans($object->getDocumentModel()->getLabelKey(), ['ucf']);
				}
				return null;
			case 'Rbs.Ua.DocumentForSelector/visualUrl':
				if ($object instanceof \Change\Documents\AbstractDocument)
				{
					$property = $object->getDocumentModel()->getProperty('visual');
					if (!$property)
					{
						$property = $object->getDocumentModel()->getProperty('listVisual');
					}
					if ($property)
					{
						$image = $property->getValue($object);
						if ($image instanceof \Rbs\Media\Documents\Image)
						{
							return $image->getPublicURL(57, 32);
						}
					}
				}
				return null;
			case 'Rbs.Ua.DocumentForSelector/publishedUrl':
				if ($object instanceof \Change\Documents\Interfaces\Publishable && $this->websiteUrlManager && $object->published())
				{
					return $this->websiteUrlManager->getByDocument($object, null)->normalize()->toString();
				}
				return null;
		}

		if ($propertyName === 'common' || $propertyName === '_meta')
		{
			$propertyModel = $objectModel->getPropertyModel($propertyName);
			if ($propertyModel && $propertyModel instanceof \Rbs\Ua\Model\ObjectModel)
			{
				return $object;
			}
		}
		$resolved = false;
		return null;
	}
}