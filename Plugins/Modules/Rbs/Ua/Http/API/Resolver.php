<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\API;

/**
 * @name \Rbs\Ua\Http\API\Resolver
 */
class Resolver extends \Change\Http\BaseResolver
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function resolve($event)
	{
		parent::resolve($event);
		$request = $event->getRequest();
		$path = (string)$request->getPath();
		if (!$path || $path === '/')
		{
			return;
		}

		if ($event->getAuthorization() === null)
		{
			$authenticationManager = $event->getAuthenticationManager();
			$event->setAuthorization(function () use ($authenticationManager)
			{
				return $authenticationManager->getCurrentUser()->authenticated();
			});
		}

		$pathParts = explode('/', $path);
		array_shift($pathParts);
		$applicationName = array_shift($pathParts);
		$event->getUrlManager()->setBasePath($applicationName);
		$event->setParam('applicationName', $applicationName);
		$pathInfo = '/' . implode('/', $pathParts);
		$event->setParam('pathInfo', $pathInfo);

		if ($pathParts && $pathParts[0] == 'OAuth')
		{
			$isDirectory = false;
			if (end($pathParts) === '')
			{
				array_pop($pathParts);
				$isDirectory = true;
			}
			$event->setParam('isDirectory', $isDirectory);
			$event->setParam('pathParts', $pathParts);

			array_shift($pathParts);
			(new \Change\Http\Rest\OAuth\AuthenticationListener())->resolve($event, $pathParts, $request->getMethod());
		}
		else
		{
			if ($applicationName === 'server')
			{
				$event->setParam('serverRealm', 'Rbs_Ua_' . $applicationName);
			}
			$definitions = new \Rbs\Ua\API\Definitions();
			$filePath = __DIR__ . '/../../Assets/rest.global.json';
			$definitions->initFromFile($filePath);
			if (!$definitions->resolveApi($event))
			{
				$event->setParam('definitions', $definitions->resetDefinitions());
			}
		}
	}
}