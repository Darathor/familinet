<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\Rest\Actions;

use Change\Http\Rest\V1\ArrayResult;
use Zend\Http\Response as HttpResponse;

/**
 * @name \Rbs\Ua\Http\Rest\Actions\GetApps
 */
class GetApps
{

	/**
	 * @param \Change\Http\Event $event
	 */
	public function execute($event)
	{
		$applicationServices = $event->getApplicationServices();
		$dbProvider = $applicationServices->getDbProvider();
		$query = $dbProvider->getNewQueryBuilder();
		$fb = $query->getFragmentBuilder();
		$query->select('application', 'consumer_key', 'consumer_secret')->from('change_oauth_application')->where($fb->eq($fb->column('active'), 1));
		$apps = $query->query()->getResults();
		$i18nManager = $applicationServices->getI18nManager();

		foreach ($apps as $k => $app)
		{
			list($vendor, $module, $code) = explode('_', $app['application']);
			if ($code)
			{
				$i18n = 'm.' . strtolower($vendor) . '.' . strtolower($module) . '.ua.' . $code . '_title';
				$apps[$k]['name'] = $i18nManager->trans($i18n, ['ucf']);
			}
			else
			{
				$apps[$k]['name'] = ucfirst($module);
			}
		}

		$result = new ArrayResult();
		$result->setArray($apps);
		$result->setHttpStatusCode(HttpResponse::STATUS_CODE_200);
		$event->setResult($result);
	}
}