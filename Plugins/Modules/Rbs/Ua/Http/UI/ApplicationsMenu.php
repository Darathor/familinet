<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\ApplicationsMenu
 */
class ApplicationsMenu
{
	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$eventManager = $event->getController()->getEventManager();
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$LCID = $i18nManager->getLCID();
		$devMode = $event->getApplication()->inDevelopmentMode();
		$attributes = [
			'applicationName' => 'Familinet',
			'devMode' => $devMode,
			'applicationsMenu' => [],
			'baseURL' => $event->getUrlManager()->getByPathInfo(null)->normalize()->toString(),
			'adminURL' => $event->getUrlManager()->getByPathInfo('../admin.php/')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => substr($LCID, 0, 2)
		];
		$event->setName('ApplicationsMenu');
		$event->setParam('applicationsMenu', [
			'contents' => [
				'name' => 'contents',
				'position' => 10,
				'title' => $i18nManager->trans('m.rbs.ua.common.category_contents_title', ['ucf']),
				'items' => []
			],
			'administration' => [
				'name' => 'administration',
				'position' => 60,
				'title' => $i18nManager->trans('m.rbs.ua.common.category_administration_title', ['ucf']),
				'items' => []
			]
		]);

		$eventManager->triggerEvent($event);
		$menu = array_values((array)$event->getParam('applicationsMenu', []));
		usort($menu, [$this, 'compare']);
		foreach ($menu as &$category)
		{
			$items = array_values($category['items']);
			usort($items, [$this, 'compare']);
			$category['items'] = $items;
		}
		unset($category);
		$attributes['applicationsMenu'] = $menu;
		$templateManager = $applicationServices->getTemplateManager();
		$templateManager->addExtension(new \Rbs\Ua\Presentation\Twig\Extension($event->getApplication(),
			$applicationServices->getI18nManager(), $applicationServices->getModelManager()));
		$templateFileName = implode(DIRECTORY_SEPARATOR, [__DIR__, 'Assets', 'applications-menu.twig']);

		$result = new \Rbs\Ua\Http\UI\HtmlResult($templateManager->renderTemplateFile($templateFileName, $attributes));
		$event->setResult($result);
	}

	/**
	 * @param array $a
	 * @param array $b
	 * @return int
	 */
	protected function compare(array $a, array $b)
	{
		if ($a['position'] == $b['position'])
		{
			return 0;
		}

		return ($a['position'] < $b['position']) ? -1 : 1;
	}
}