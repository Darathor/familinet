<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\HomeBase
 */
abstract class HomeBase
{
	/**
	 * @return string
	 */
	abstract protected function getVendor();

	/**
	 * @return string
	 */
	abstract protected function getModule();

	/**
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 * @param array $attributes
	 * @return array
	 */
	protected function appendAttributes(\Rbs\Ua\Http\UI\Resources $resources, $attributes)
	{
		return $attributes;
	}

	/**
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 * @param array $attributes
	 * @return array
	 */
	protected function appendTemplatesAttributes(\Rbs\Ua\Http\UI\Resources $resources, $attributes)
	{
		$cachedTemplates = $attributes['cachedTemplates'] ?? [];

		$templatePaths = $resources->getTemplateFilePaths();
		if ($templatePaths)
		{
			foreach ($templatePaths as $file)
			{
				$parts = \explode('/', $file);
				$moduleName = \array_shift($parts) . '_' . \array_shift($parts);
				$subPath = \implode('/', $parts);
				$cachedTemplates[$file] = $resources->renderModuleTemplateFile($moduleName, $subPath, []);
			}
		}

		$attributes['cachedTemplates'] = $cachedTemplates;
		return $attributes;
	}

	/**
	 * Use Required Event Params:
	 * @param \Change\Http\Event $event
	 */
	public function execute(\Change\Http\Event $event)
	{
		$application = $event->getApplication();
		$applicationServices = $event->getApplicationServices();
		/* @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$vendorName = $this->getVendor();
		$moduleName = $this->getModule();

		$OAuthManager = $applicationServices->getOAuthManager();

		$i18nManager = $applicationServices->getI18nManager();
		$resources = $genericServices->getUaManager()->getResources($vendorName, $moduleName, $event->getParam('applicationName'));
		if (!$resources)
		{
			return;
		}

		$applicationInfo = $resources->getApplicationInfo();

		$consumer = $OAuthManager->getConsumerByApplication($applicationInfo->getRealm());
		if (!$consumer)
		{
			$event->getController()->error($event);
			return;
		}

		$devMode = $application->inDevelopmentMode();
		if ($devMode)
		{
			$event->setParam('ACTION_NAME', \get_class($this) . '::execute');
		}

		// Cleanup routes.
		$routes = $resources->getRoutes();
		foreach ($routes as $path => $route)
		{
			unset($route['controllers']);
			if (isset($route['rule']['labelKey']))
			{
				$route['rule']['label'] = $i18nManager->trans($route['rule']['labelKey'], ['ucf']);
				unset($route['rule']['labelKey']);
			}
			$routes[$path] = $route;
		}

		// Compute LCID labels.
		$supportedLCIDs = $i18nManager->getSupportedLCIDs();
		$availableLanguages = [];

		foreach ($supportedLCIDs as $LCID)
		{
			$availableLanguages[] = ['value' => $LCID, 'label' => $i18nManager->transLCID($LCID, ['ucf'])];
		}

		$LCID = $i18nManager->getLCID();
		$urlManager = $event->getUrlManager();
		$role = $applicationInfo->getRole();
		$applicationShortName =
			$i18nManager->trans('m.' . $vendorName . '.' . $moduleName . '.ua.' . $applicationInfo->getName() . '_title', ['ucf']);
		$applicationPackageName = $i18nManager->trans('m.rbs.ua.common.application_package_name', ['ucf']);

		$attributes = [
			'applicationCode' => $applicationInfo->getName(),
			'applicationName' => $applicationShortName . ' - ' . $applicationPackageName,
			'applicationShortName' => $applicationShortName,
			'applicationPackageName' => $applicationPackageName,
			'role' => $role,
			'snakeRole' => \Change\Stdlib\StringUtils::snakeCase($role, '-'),
			'devMode' => $devMode,
			'angularDevMode' => $devMode && $application->getConfiguration('Change/Presentation/AngularDevMode'),
			'baseURL' => $urlManager->getByPathInfo(null)->normalize()->toString(),
			'restURL' => $urlManager->getByPathInfo('../../uaRest.php/' . $role . '/')->normalize()->toString(),
			'applicationsMenuURL' => $urlManager->getByPathInfo('../')->normalize()->toString(),
			'LCID' => $LCID,
			'lang' => \substr($LCID, 0, 2),
			'lowerHyphenLCID' => \strtolower(\str_replace('_', '-', $LCID)),
			'OAuth' => \array_merge($consumer->toArray(), ['realm' => $applicationInfo->getRealm()]),
			'menu' => $resources->getMainMenu(),
			'modalDefinitions' => $resources->getModalDefinitions(),
			'routes' => $routes,
			'restApi' => $resources->getRestApi(),
			'i18n' => $resources->getI18nKeys(),
			'availableLanguages' => $availableLanguages,
			'vendorName' => $vendorName,
			'moduleName' => $moduleName,
			'documentModelDefinitions' => $this->getModelDefinitions($applicationServices->getModelManager(), $i18nManager),
			'application' => [
				'version' => $application->getConfiguration('Change/Application/version'),
				'uuid' => $application->getConfiguration('Change/Application/uuid'),
				'env' => $application->getConfiguration('Change/Application/env')
			],
			'navigationContext' => [
				'assetBasePath' => $resources->getAssetsBasePath()
			]
		];

		$attributes = $this->appendAttributes($resources, $attributes);
		$attributes['__change'] = $attributes;

		$templateAttributes = $this->appendTemplatesAttributes($resources, $attributes);

		$html = $resources->renderModuleTemplateFile($vendorName . '_' . $moduleName, 'Ua/home.twig', $templateAttributes);
		$result = new \Rbs\Ua\Http\UI\HtmlResult($html);
		$event->setResult($result);
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return array
	 */
	protected function getModelDefinitions($modelManager, $i18nManager)
	{
		$documentModels = [];
		foreach ($modelManager->getModelsNames() as $modelName)
		{
			$model = $modelManager->getModelByName($modelName);
			if (!$model || $model->isInline() || $model->isAbstract())
			{
				continue;
			}

			$pluginKey = strtolower('m.' . $model->getVendorName() . '.' . $model->getShortModuleName() . '.admin.module_name');
			$pluginLabel = $i18nManager->trans($pluginKey, ['ucf']);
			if ($pluginKey === $pluginLabel)
			{
				continue;
			}

			$documentModels[] = [
				'name' => $model->getName(),
				'label' => $i18nManager->trans($model->getLabelKey(), ['ucf']),
				'pluginLabel' => $pluginLabel,
				'publishable' => $model->isPublishable(),
				'activable' => $model->isActivable(),
				'editable' => $model->isEditable(),
				'instanceOf' => \array_merge([$model->getName()], $model->getAncestorsNames())
			];
		}
		return $documentModels;
	}
}