<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\HtmlResult
 */
class HtmlResult extends \Change\Http\Result
{
	/**
	 * @var string|null
	 */
	protected $html;

	/**
	 * @param string $html
	 * @param int $httpStatusCode
	 */
	public function __construct($html = '', $httpStatusCode = \Zend\Http\Response::STATUS_CODE_200)
	{
		$this->setHtml($html);
		parent::__construct($httpStatusCode);
		$this->getHeaders()->addHeaderLine('Content-Type: text/html; charset=utf-8');
	}

	/**
	 * @param string $html
	 * @return $this
	 */
	public function setHtml($html)
	{
		$this->html = $html;
		return $this;
	}

	/**
	 * @return string
	 */
	public function toHtml()
	{
		return (string)$this->html;
	}
}