<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\Resolver
 */
class Resolver extends \Change\Http\BaseResolver
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function resolve($event)
	{
		parent::resolve($event);

		$request = $event->getRequest();
		$path = (string)$request->getPath();
		if (!$path || $path === '/')
		{
			$event->setAction(function ($event)
			{
				(new ApplicationsMenu())->execute($event);
			});
			return;
		}

		$pathParts = explode('/', $path);
		array_shift($pathParts);
		$applicationName = array_shift($pathParts);
		$event->getUrlManager()->setBasePath($applicationName);
		$event->setParam('applicationName', $applicationName);
		$pathInfo = '/' . implode('/', $pathParts);
		$event->setParam('pathInfo', $pathInfo);

		if (preg_match('/^\/([A-Z][a-z0-9]+)\/([A-Z][a-z0-9]+)\/(.*\.twig)$/', $pathInfo, $matches))
		{
			$event->setParam('vendor', $matches[1]);
			$event->setParam('module', $matches[2]);
			$event->setParam('relativePath', $matches[3]);
			$event->setAction(function ($event)
			{
				$this->twigResource($event);
			});
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	protected function twigResource($event)
	{
		$request = $event->getRequest();

		$vendor = $event->getParam('vendor');
		$module = $event->getParam('module');
		$relativePath = $event->getParam('relativePath');

		/* @var $genericServices \Rbs\Generic\GenericServices */
		$genericServices = $event->getServices('genericServices');
		$resources = $genericServices->getUaManager()->getResources('Rbs', 'Ua', 'common');

		try
		{
			$html = $resources->renderModuleTemplateFile($vendor . '_' . $module, $relativePath, $request->getQuery()->toArray());
			$result = new \Rbs\Ua\Http\UI\HtmlResult($html);
			$event->setResult($result);
		}
		catch (\Twig\Error\LoaderError $e)
		{
			$event->getApplication()->getLogging()->error('Template not found', $vendor, $module, $relativePath);
		}
	}
}