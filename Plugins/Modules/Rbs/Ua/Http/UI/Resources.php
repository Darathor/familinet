<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Http\UI;

/**
 * @name \Rbs\Ua\Http\UI\Resources
 */
class Resources extends \Change\Http\AbstractResources
{
	/**
	 * @var \Rbs\Ua\ApplicationInfo
	 */
	protected $applicationInfo;

	/**
	 * @var string
	 */
	protected $cachePath;

	/**
	 * @var \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	protected $extensions;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var array|null
	 */
	protected $resourcesData;

	/**
	 * @var array|null
	 */
	protected $routes;

	/**
	 * @var array|null
	 */
	protected $modals;

	/**
	 * @param string $applicationName
	 */
	public function __construct($applicationName)
	{
		$this->applicationInfo =
			new \Rbs\Ua\ApplicationInfo($applicationName, $this->getVendor() . '_' . $this->getModule() . '_' . $applicationName);
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerAssets($assetsManager)
	{
		$moduleName = $this->getModule();
		$vendorName = $this->getVendor();
		$applicationName = $this->applicationInfo->getName();

		$jsAssets = new \Assetic\Asset\AssetCollection();
		$cssAssets = new \Assetic\Asset\AssetCollection();

		$resourcesData = $this->getResourcesData();
		if ($resourcesData)
		{
			// JS assets.
			if (isset($resourcesData['scripts']['files']))
			{
				foreach (\array_keys(\array_filter($resourcesData['scripts']['files'])) as $file)
				{
					$jsAssets->add(new \Assetic\Asset\FileAsset($this->generateResourcePath($file)));
				}
			}
			if (isset($resourcesData['scripts']['directories']))
			{
				foreach (\array_keys(\array_filter($resourcesData['scripts']['directories'])) as $directory)
				{
					$jsAssets->add(new \Assetic\Asset\GlobAsset($this->generateResourcePath(trim($directory, '/')) . '/*.js'));
				}
			}
			foreach ($this->getControllerFiles() as $file)
			{
				$jsAssets->add(new \Assetic\Asset\FileAsset($file));
			}

			// CSS assets.
			if (isset($resourcesData['styles']['files']))
			{
				foreach (\array_keys(\array_filter($resourcesData['styles']['files'])) as $file)
				{
					$this->addStyleAsset($cssAssets, $file);
				}
			}
			if (isset($resourcesData['styles']['directories']))
			{
				foreach (\array_keys(\array_filter($resourcesData['styles']['directories'])) as $directory)
				{
					$cssAssets->add(new \Assetic\Asset\GlobAsset($this->generateResourcePath(trim($directory, '/')) . '/*.css'));
				}
			}
		}

		if (!$this->devMode)
		{
			$jsAssets->ensureFilter(new \Change\Presentation\Themes\JSMinFilter());
		}
		$jsAssets->setTargetPath('Rbs/Ua/js/' . $vendorName . '_' . $moduleName . '_' . $applicationName . '.js');
		$assetsManager->set('BODY_JS_' . $vendorName . '_' . $moduleName, $jsAssets);

		$cssAssets->setTargetPath('Rbs/Ua/css/' . $vendorName . '_' . $moduleName . '_' . $applicationName . '.css');
		$assetsManager->set('HEAD_CSS_' . $vendorName . '_' . $moduleName, $cssAssets);

		// Images.
		if (isset($resourcesData['images']['files']))
		{
			foreach (\array_keys(\array_filter($resourcesData['images']['files'])) as $file)
			{
				$targetPath = $this->generateImageDestinationPath($file);
				$asset = new \Assetic\Asset\FileAsset($this->generateResourcePath($file));
				$asset->setTargetPath($targetPath);
				$assetsManager->set(\preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
			}
		}
		if (isset($resourcesData['images']['directories']))
		{
			foreach (\array_keys(\array_filter($resourcesData['images']['directories'])) as $directory)
			{
				$targetBasePath = $this->generateImageDestinationPath($directory);
				$directory = $this->generateResourcePath($directory);
				if (\is_dir($directory))
				{
					$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory, \FilesystemIterator::SKIP_DOTS),
						\RecursiveIteratorIterator::SELF_FIRST);
					foreach ($iterator as $fileInfo)
					{
						/* @var $fileInfo \SplFileInfo */
						if ($fileInfo->isFile())
						{
							$targetPath = \str_replace($directory, $targetBasePath, $fileInfo->getPathname());
							$asset = new \Assetic\Asset\FileAsset($fileInfo->getPathname());
							$asset->setTargetPath($targetPath);
							$assetsManager->set(\preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
						}
					}
				}
			}
		}
	}

	/**
	 * @param \Assetic\Asset\AssetCollection $cssAssets
	 * @param string $file
	 */
	protected function addStyleAsset($cssAssets, $file)
	{
		$path = $this->generateResourcePath($file);
		if (\Change\Stdlib\StringUtils::endsWith($file, '.css'))
		{
			$cssAssets->add(new \Assetic\Asset\FileAsset($path));
		}
		elseif (\Change\Stdlib\StringUtils::endsWith($file, '.scss'))
		{
			$cssAssets->add($this->getPreprocessedAsset($path, 'scss'));
		}
		elseif (\Change\Stdlib\StringUtils::endsWith($file, '.less'))
		{
			$cssAssets->add($this->getPreprocessedAsset($path, 'less'));
		}
	}

	/**
	 * @return array
	 */
	protected function getControllerFiles()
	{
		$controllers = [];

		// Controllers from routes.
		foreach ($this->getRoutes() as $route)
		{
			if (isset($route['controllers']))
			{
				foreach ($route['controllers'] as $controller)
				{
					$controllers[$controller] = true;
				}
			}
		}

		// Controllers from modals.
		foreach ($this->getModalDefinitions() as $definition)
		{
			if (isset($definition['controller']))
			{
				$controllers[$definition['controller']] = true;
			}
		}

		$path = $this->getApplication()->getWorkspace()->compilationPath('Ua', 'controllersData.php');
		/** @noinspection PhpIncludeInspection */
		$controllersData = require $path;
		$controllersMapping = [];
		$controllersDependencies = [];
		foreach ($controllersData['mapping'] as $moduleControllers)
		{
			$controllersMapping = \array_merge($controllersMapping, $moduleControllers);
		}
		foreach ($controllersData['dependencies'] as $moduleControllers)
		{
			$controllersDependencies = \array_merge($controllersDependencies, $moduleControllers);
		}

		$projectPath = $this->getApplication()->getWorkspace()->projectPath() . DIRECTORY_SEPARATOR;
		$controllerFiles = [];
		foreach (\array_keys($controllers) as $controller)
		{
			$this->addToControllerFiles($controller, $controllerFiles, $projectPath, $controllersMapping, $controllersDependencies);
		}

		return \array_keys($controllerFiles);
	}

	/**
	 * @param string $controller
	 * @param array $controllerFiles
	 * @param string $projectPath
	 * @param array $controllersMapping
	 * @param array $controllersDependencies
	 */
	protected function addToControllerFiles($controller, &$controllerFiles, $projectPath, $controllersMapping, $controllersDependencies)
	{
		if (isset($controllersMapping[$controller]))
		{
			if (isset($controllersDependencies[$controller]))
			{
				foreach ($controllersDependencies[$controller] as $dependency)
				{
					$this->addToControllerFiles($dependency, $controllerFiles, $projectPath, $controllersMapping, $controllersDependencies);
				}
			}
			$controllerFiles[$projectPath . $controllersMapping[$controller]] = true;
		}
		else
		{
			$this->getApplication()->getLogging()->warn('Controller not found:', $controller,
				'(in application ' . $this->getVendor() . '_' . $this->getModule() . '_' . $this->getApplicationInfo()->getName() . ')');
		}
	}

	/**
	 * @param string $resource
	 * @return string
	 */
	protected function generateResourcePath($resource)
	{
		$parts = \explode('/', $resource);
		$plugin = $this->pluginManager->getModule(\array_shift($parts), \array_shift($parts));
		return $plugin->getAssetsPath() . '/' . \implode('/', $parts);
	}

	/**
	 * @param string $resource
	 * @return string
	 */
	protected function generateImageDestinationPath($resource)
	{
		$parts = \explode('/', $resource);
		return \array_shift($parts) . '/' . \array_shift($parts) . '/' . \implode('/', $parts);
	}

	/**
	 * @param \Assetic\AssetManager $assetsManager
	 */
	public function registerCommonLibAssets($assetsManager)
	{
		$plugin = $this->pluginManager->getModule('Rbs', 'Ua');
		$srcPath = $plugin->getAssetsPath() . '/lib';
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($srcPath, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::SELF_FIRST);
		$targetBasePath = 'Rbs/Ua/lib';
		foreach ($iterator as $fileInfo)
		{
			/* @var $fileInfo \SplFileInfo */
			if ($fileInfo->isFile())
			{
				$targetPath = str_replace($srcPath, $targetBasePath, $fileInfo->getPathname());
				$asset = new \Assetic\Asset\FileAsset($fileInfo->getPathname());
				$asset->setTargetPath($targetPath);
				$assetsManager->set(preg_replace('/[^a-zA-Z0-9]/', '', $targetPath), $asset);
			}
		}
	}

	/**
	 * @api
	 * @return array
	 */
	public function getModalDefinitions()
	{
		if ($this->modals === null)
		{
			$modalsData = ['definitions' => []];
			$workspace = $this->getApplication()->getWorkspace();

			$module = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
			$filePath = $workspace->composePath($module->getAssetsPath(), 'Ua', 'modals.json');
			$modalsData = $this->appendModalsData($filePath, $modalsData);
			$filePath = $workspace->composePath($module->getAssetsPath(), 'Ua', 'modals.' . $this->getApplicationInfo()->getName() . '.json');
			$modalsData = $this->appendModalsData($filePath, $modalsData);

			$uaResources = [
				'uaRichtextUaModals' => 'modals.richtextUa.json',
				'uaRichtextModals' => 'modals.richtext.json',
				'uaCommonModals' => 'modals.common.json'
			];
			foreach ($uaResources as $name => $file)
			{
				if ($modalsData[$name] ?? false)
				{
					$plugin = $this->pluginManager->getModule('Rbs', 'Ua');
					$uaResourcesPath = $workspace->composePath($plugin->getAssetsPath(), 'Ua', $file);
					$modalsData = $this->appendModalsData($uaResourcesPath, $modalsData);
				}
				unset($modalsData[$name]);
			}

			$this->modals = $modalsData['definitions'];
		}

		return $this->modals;
	}

	/**
	 * @api
	 * @return array
	 */
	public function getRoutes()
	{
		if ($this->routes === null)
		{
			$routes = [];
			$module = $this->getPluginManager()->getModule('Rbs', 'Ua');
			$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'Ua', 'routes.common.json');
			$routes = $this->appendRoutes($filePath, $routes);

			if ($this->getVendor() !== 'Rbs' || $this->getModule() !== 'Ua')
			{
				$module = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
				$fileName = 'routes.' . $this->getApplicationInfo()->getName() . '.json';
				$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'Ua', $fileName);
				$routes = $this->appendRoutes($filePath, $routes);
			}

			$this->routes = $routes;
		}

		return $this->routes;
	}

	/**
	 * @api
	 * @return array
	 */
	public function getRestApi()
	{
		$restApi = [];
		$module = $this->getPluginManager()->getModule('Rbs', 'Ua');
		$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'rest.global.json');
		$restApi = $this->appendRestApi($filePath, $restApi);

		$fileNames = $this->getRestApiFileNames();
		if ($fileNames)
		{
			$module = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
			foreach ($fileNames as $fileName)
			{
				$filePath = $this->getApplication()->getWorkspace()->composePath($module->getAssetsPath(), 'Ua', $fileName);
				$restApi = $this->appendRestApi($filePath, $restApi);
			}
		}
		return $this->normalizeApi($restApi);
	}

	/**
	 * @param array $restApi
	 * @return array
	 */
	protected function normalizeApi(array $restApi)
	{
		$api = [];
		if (!isset($restApi['api']))
		{
			return $api;
		}
		foreach ($restApi['api'] as $definition)
		{
			if (!isset($definition['name'], $definition['path']))
			{
				continue;
			}

			$entry = ['path' => $definition['path']];
			foreach ($definition['operations'] as $operation)
			{
				$method = $operation['method'] ?? 'GET';
				$parameters = $operation['parameters'] ?? [];
				foreach ($parameters as &$parameter)
				{
					unset($parameter['description']);
				}
				unset($parameter);

				$fields = [];
				foreach ($operation['responses'] ?? [] as $response)
				{
					foreach ($response['properties'] ?? [] as $propertyName => $property)
					{
						if ($property['fields'] ?? false)
						{
							$fields[] = $propertyName;
						}
					}
				}

				if ($fields)
				{
					$parameters[] = ['name' => 'fields', 'type' => 'String[]', 'src' => 'query', 'enum' => $fields];
				}
				$entry[$method] = ['parameters' => $parameters];
			}
			$api[$definition['name']] = $entry;
		}

		return ['api' => $api];
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @api
	 * @param \Change\Presentation\Templates\Twig\ExtensionInterface $extension
	 * @return $this
	 */
	public function addExtension(\Change\Presentation\Templates\Twig\ExtensionInterface $extension)
	{
		$this->getExtensions();
		$this->extensions[] = $extension;
		return $this;
	}

	/**
	 * @return \Change\Presentation\Templates\Twig\ExtensionInterface[]
	 */
	public function getExtensions()
	{
		if ($this->extensions === null)
		{
			$extension = new \Rbs\Ua\Presentation\Twig\Extension($this->getApplication(), $this->getI18nManager(), $this->getModelManager());
			$this->extensions = [$extension];

			$extension = new \Change\Presentation\Templates\Twig\Extension($this->getI18nManager(), $this->getApplication()->getConfiguration());
			$this->extensions[] = $extension;
		}
		return $this->extensions;
	}

	/**
	 * @return string
	 */
	protected function getCachePath()
	{
		if ($this->cachePath === null)
		{
			$this->cachePath = $this->getApplication()->getWorkspace()->cachePath('Ua', 'Templates', 'Compiled');
			\Change\Stdlib\FileUtils::mkdir($this->cachePath);
		}
		return $this->cachePath;
	}

	/**
	 * @param string $moduleName
	 * @param string $pathName
	 * @param array $attributes
	 * @return string
	 */
	public function renderModuleTemplateFile($moduleName, $pathName, array $attributes)
	{
		$overridePath = $this->getApplication()->getWorkspace()->appPath('Overrides', 'Ua');
		$loader = new \Rbs\Ua\Presentation\Twig\Loader($overridePath, $this->getPluginManager());
		$twig = new \Twig\Environment($loader, ['cache' => $this->getCachePath(), 'auto_reload' => true]);
		foreach ($this->getExtensions() as $extension)
		{
			$twig->addExtension($extension);
		}
		return $twig->render('@' . $moduleName . '/' . $pathName, $attributes);
	}

	/**
	 * @api
	 * @return array
	 */
	public function getI18nKeys()
	{
		$i18nManager = $this->getI18nManager();
		$LCID = $i18nManager->getLCID();
		$packages = [];
		$resourcesData = $this->getResourcesData();
		if (isset($resourcesData['i18n']['packages']))
		{
			foreach (\array_keys(\array_filter($resourcesData['i18n']['packages'])) as $packageName)
			{
				$keys = $i18nManager->getTranslationsForPackage($packageName, $LCID);
				if (\is_array($keys))
				{
					$package = [];
					foreach ($keys as $key => $value)
					{
						$package[$key] = $value;
					}
					$packages[$packageName] = $package;
				}
			}
		}
		return $packages;
	}

	/**
	 * @return \Rbs\Ua\ApplicationInfo
	 */
	public function getApplicationInfo()
	{
		return $this->applicationInfo;
	}

	/**
	 * @param \Rbs\Ua\ApplicationInfo $applicationInfo
	 * @return Resources
	 */
	public function setApplicationInfo($applicationInfo)
	{
		$this->applicationInfo = $applicationInfo;
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getVendor()
	{
		return 'Rbs';
	}

	/**
	 * @return string
	 */
	protected function getModule()
	{
		return 'Ua';
	}

	/**
	 * @return array
	 */
	protected function getRestApiFileNames()
	{
		return ['rest.' . $this->getApplicationInfo()->getName() . '.json'];
	}

	/**
	 * @return array
	 */
	public function getMainMenu()
	{
		$pm = $this->getPluginManager();
		$sections = [];
		$entries = [];
		$plugin = $pm->getModule($this->getVendor(), $this->getModule());
		$mainMenuPath = $plugin->getAssetsPath() . '/Ua/main-menu.' . $this->applicationInfo->getName() . '.json';
		if (\is_readable($mainMenuPath))
		{
			$data = \json_decode(\file_get_contents($mainMenuPath), true);
			if (!\is_array($data))
			{
				throw new \LogicException('Invalid JSON ' . $mainMenuPath);
			}
			$sections = $this->parseSections($data);
			$entries = $this->parseEntries($data);
		}

		if (!$sections)
		{
			return [];
		}

		\uasort($sections, function ($a, $b) {
			$ida = $a['index'] ?? PHP_INT_MAX;
			$idb = $b['index'] ?? PHP_INT_MAX;
			return $ida >= $idb;
		});

		\reset($sections);
		$defaultSection = \key($sections);

		foreach ($entries as $entry)
		{
			$section = isset($entry['section'], $sections[$entry['section']]) ? $entry['section'] : $defaultSection;
			$sections[$section]['entries'][] = $entry;
		}

		$result = [];
		foreach ($sections as $section)
		{
			if (isset($section['entries']))
			{
				$result[] = $section;
			}
		}
		return $result;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	protected function parseSections($menuJson)
	{
		$result = [];
		if (isset($menuJson['sections']) && \is_array($menuJson['sections']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['sections'] as $item)
			{
				if (isset($item['code']))
				{
					if (isset($item['label']) && \is_string($item['label']))
					{
						$item['label'] = $i18nManager->trans($item['label'], ['ucf']);
					}
					$result[$item['code']] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @param array $menuJson
	 * @return array
	 */
	protected function parseEntries($menuJson)
	{
		$result = [];
		if (isset($menuJson['entries']) && \is_array($menuJson['entries']))
		{
			$i18nManager = $this->getI18nManager();
			foreach ($menuJson['entries'] as $item)
			{
				$label = $item['label'] ?? null;
				if ($label && \is_string($label))
				{
					$label = $i18nManager->trans($label, ['ucf']);
					$item['label'] = $label;
				}

				$type = $item['type'] ?? null;
				if ($type === 'separator' || ($type === 'text' && $label))
				{
					$result[] = $item;
				}
				elseif (isset($item['url']) || (isset($item['name']) && (isset($item['module']) || isset($item['model']))))
				{
					$item['type'] = 'link';
					$result[] = $item;
				}
			}
		}
		return $result;
	}

	/**
	 * @return string[]
	 */
	public function getTemplateFilePaths()
	{
		$paths = [];

		// Resources.
		$resourcesData = $this->getResourcesData();
		if ($resourcesData)
		{
			if (isset($resourcesData['templates']['directories']) && $resourcesData['templates']['directories'])
			{
				foreach (\array_keys(\array_filter($resourcesData['templates']['directories'])) as $directory)
				{
					$parts = \explode('/', $directory);
					$plugin = $this->getPluginManager()->getModule(\array_shift($parts), \array_shift($parts));
					$subPath = \implode('/', $parts);
					$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($plugin->getAssetsPath() . '/' . $subPath,
						\FilesystemIterator::CURRENT_AS_SELF + \FilesystemIterator::SKIP_DOTS));
					while ($it->valid())
					{
						/* @var $current \RecursiveDirectoryIterator */
						$current = $it->current();
						if ($current->isFile() && $current->getExtension() === 'twig')
						{
							$subPathname = $current->getSubPathname();
							if (DIRECTORY_SEPARATOR !== '/')
							{
								$subPathname = \str_replace(DIRECTORY_SEPARATOR, '/', $subPathname);
							}
							$paths[] = $directory . '/' . $subPathname;
						}
						$it->next();
					}
				}
			}
			if (isset($resourcesData['templates']['file']) && $resourcesData['templates']['file'])
			{
				foreach (\array_keys(\array_filter($resourcesData['templates']['file'])) as $file)
				{
					$paths[] = $file;
				}
			}
		}

		// Modals.
		foreach ($this->getModalDefinitions() as $definition)
		{
			if (isset($definition['templateUrl']) && $definition['templateUrl'])
			{
				$paths[] = $definition['templateUrl'];
			}
		}

		// Views.
		foreach ($this->getRoutes() as $route)
		{
			if (isset($route['rule']['templateUrl']) && $route['rule']['templateUrl'])
			{
				$paths[] = $route['rule']['templateUrl'];
			}
		}

		return $paths;
	}

	/**
	 * @return array
	 * @throws \RuntimeException
	 */
	public function getResourcesData()
	{
		if ($this->resourcesData === null)
		{
			$this->resourcesData = [];
			$plugin = $this->getPluginManager()->getModule($this->getVendor(), $this->getModule());
			$workspace = $this->getApplication()->getWorkspace();
			$resourcesData = [];
			$globalResourcesPath = $workspace->composePath($plugin->getAssetsPath(), 'Ua/resources.json');
			$localResourcesPath =
				$workspace->composePath($plugin->getAssetsPath(), 'Ua/resources.' . $this->getApplicationInfo()->getName() . '.json');
			if (\is_readable($globalResourcesPath))
			{
				$data = \json_decode(\file_get_contents($globalResourcesPath), true);
				if (!\is_array($data))
				{
					throw new \LogicException('Invalid JSON ' . $globalResourcesPath);
				}
				$resourcesData = $data;
			}
			if (\is_readable($localResourcesPath))
			{
				$data = \json_decode(\file_get_contents($localResourcesPath), true);
				if (!\is_array($data))
				{
					throw new \LogicException('Invalid JSON ' . $localResourcesPath);
				}
				$resourcesData = \array_merge_recursive($resourcesData, $data);
			}

			$uaResources = [
				'uaRichtextUaResources' => 'resources.richtextUa.json',
				'uaRichtextResources' => 'resources.richtext.json',
				'uaCommonResources' => 'resources.common.json'
			];
			foreach ($uaResources as $name => $file)
			{
				if ($resourcesData[$name] ?? false)
				{
					$plugin = $this->pluginManager->getModule('Rbs', 'Ua');
					$uaResourcesPath = $workspace->composePath($plugin->getAssetsPath(), 'Ua', $file);

					$data = \json_decode(\file_get_contents($uaResourcesPath), true);
					if (!\is_array($data))
					{
						throw new \LogicException('Invalid JSON ' . $uaResourcesPath);
					}
					$resourcesData = \array_merge_recursive($data, $resourcesData);
				}
				unset($resourcesData[$name]);
			}

			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['resourcesData' => $resourcesData]);
			$eventManager->trigger('getResourcesData', $this, $args);
			$this->resourcesData = $args['resourcesData'];
		}

		return $this->resourcesData;
	}

	/**
	 * @param string $filePath
	 * @param array $modalsData
	 * @return array
	 */
	protected function appendModalsData($filePath, $modalsData)
	{
		if (\is_readable($filePath))
		{
			$data = \json_decode(\file_get_contents($filePath), true);
			if (!\is_array($data))
			{
				throw new \LogicException('Invalid JSON ' . $filePath);
			}
			$modalsData = \array_merge_recursive($data, $modalsData);
		}
		return $modalsData;
	}

	/**
	 * @param string $filePath
	 * @param array $routes
	 * @return array
	 */
	protected function appendRoutes($filePath, $routes)
	{
		if (\is_readable($filePath))
		{
			$data = \json_decode(\file_get_contents($filePath), true);
			if (!\is_array($data))
			{
				throw new \LogicException('Invalid JSON ' . $filePath);
			}
			$routes = \array_merge($routes, $data);
		}
		return $routes;
	}

	/**
	 * @param string $filePath
	 * @param array $restApi
	 * @return array
	 */
	protected function appendRestApi($filePath, $restApi)
	{
		if (\is_readable($filePath))
		{
			$moduleRestApi = \json_decode(\file_get_contents($filePath), true);
			if (!\is_array($moduleRestApi))
			{
				throw new \LogicException('Invalid JSON ' . $filePath);
			}

			foreach ($moduleRestApi as $key => $entry)
			{
				if ($key === 'includes')
				{
					if ($entry && \is_array($entry))
					{
						$basePath = \dirname($filePath) . DIRECTORY_SEPARATOR;
						foreach ($entry as $include)
						{
							$restApi = $this->appendRestApi($basePath . $include, $restApi);
						}
					}
					continue;
				}
				if (\is_string($entry))
				{
					$restApi[$key] = $entry;
				}
				elseif (\is_array($entry))
				{
					$restApi[$key] = isset($restApi[$key]) ? \array_merge($restApi[$key], $entry) : $entry;
				}
			}
		}
		return $restApi;
	}
}