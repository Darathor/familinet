<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\EnumModel
 */
class EnumModel implements ModelInterface
{
	use \Rbs\Ua\Model\DefinitionTrait;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var array
	 */
	protected $enum = [];

	/**
	 * @var string
	 */
	protected $enumType;

	/**
	 * @var \Rbs\Ua\Model\EnumModel|null
	 */
	protected $extends;

	/**
	 * @param string $type
	 * @param array $definition
	 */
	public function __construct($type, array $definition)
	{
		$this->type = $type;
		$this->setDefinition($definition);
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getEnum()
	{
		return $this->enum;
	}

	/**
	 * @return string
	 */
	public function getEnumType()
	{
		return $this->enumType;
	}

	/**
	 * @return null|\Rbs\Ua\Model\EnumModel
	 */
	public function getExtends()
	{
		return $this->extends;
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return array|null
	 * @throws \LogicException
	 * @throws \RuntimeException
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		if (!$this->isCompiled())
		{
			throw new \LogicException('type ' . $this->getType() . ' not compiled');
		}

		if ($value !== null)
		{
			$k = $this->enumType === ScalarModel::Integer ? (int)$value : (string)$value;
			if (isset($this->enum[$k]))
			{
				return $this->enum[$k];
			}

			if ($this->extends)
			{
				try
				{
					return $this->extends->normalize($value, $fallbackContext);
				}
				catch (\RuntimeException $e)
				{
					throw new \RuntimeException('Invalid enum value: ' . $value . ' on ' . $this->type . ', ' . $e->getMessage());
				}
			}
			throw new \RuntimeException('Invalid enum value: ' . $value . ' on ' . $this->type);
		}
		return null;
	}

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		if (!$this->isCompiled())
		{
			$definition = $this->getDefinition();
			$extendsType = $definition['extends'] ?? null;
			if ($extendsType)
			{
				$extends = $models->getModel($extendsType);
				if (!$extends instanceof EnumModel)
				{
					throw new \LogicException('Invalid extends [' . $extendsType . '] on ' . $this->type);
				}
				$this->extends = $extends;
			}

			$enumType = $definition['enumType'] ?? false;
			if (in_array($enumType, [ScalarModel::Integer, ScalarModel::String]))
			{
				$this->enumType = $enumType;
				$enum = $definition['enum'] ?? false;
				if ($enum && is_array($enum))
				{
					foreach ($enum as $enumVal)
					{
						if ($enumType === ScalarModel::Integer)
						{
							$val = (int)$enumVal;
							$this->enum[$val] = $val;
						}
						else
						{
							$val = (string)$enumVal;
							$this->enum[$val] = $val;
						}
					}
				}
				else
				{
					throw new \LogicException('Invalid enum values on ' . $this->getType());
				}
			}
			else
			{
				throw new \LogicException('Invalid enumType [' . $enumType . '] on ' . $this->getType());
			}
			$this->markCompiled();
		}
		return $this;
	}
}