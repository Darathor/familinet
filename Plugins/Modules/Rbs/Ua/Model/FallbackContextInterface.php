<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\FallbackContextInterface
 */
interface FallbackContextInterface
{
	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @return mixed
	 */
	public function normalizeObject($object, $objectModel);

	/**
	 * @param mixed $object
	 * @param \Rbs\Ua\Model\ObjectModel $objectModel
	 * @param string $propertyName
	 * @param boolean $resolved
	 * @return mixed
	 */
	public function resolveProperty($object, $objectModel, $propertyName, &$resolved);
}