<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\ModelInterface
 */
interface ModelInterface
{
	/**
	 * @return string
	 */
	public function getType();

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile(\Rbs\Ua\Model\Models $models);

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return mixed
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null);
}