<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Model;

/**
 * @name \Rbs\Ua\Model\VariantObjectModel
 */
class VariantObjectModel extends ObjectModel
{
	
	public function __construct()
	{
		parent::__construct('Object', []);
	}

	/**
	 * @param mixed $value
	 * @param \Rbs\Ua\Model\FallbackContextInterface $fallbackContext
	 * @return mixed
	 */
	public function normalize($value, \Rbs\Ua\Model\FallbackContextInterface $fallbackContext = null)
	{
		if ($value === null)
		{
			return $value;
		}
		if ($fallbackContext)
		{
			$value = $this->normalizeObject($value, $fallbackContext);
			if ($value === null)
			{
				return null;
			}
		}
		$result = [];
		if (is_array($value) || $value instanceof \Traversable)
		{
			$result = $this->normalizeHashTable($value);
		}
		elseif (is_object($value))
		{
			if (is_callable([$value, 'toArray']))
			{
				$value = $value->{'toArray'}();
			}
			else
			{
				$value = get_object_vars($value);
			}
			$result = $this->normalizeHashTable($value);
		}
		return $result ?: null;
	}

	/**
	 * @param array|\Traversable $value
	 * @return array
	 */
	protected function normalizeHashTable($value)
	{
		$result = [];
		if (is_array($value) || $value instanceof \Traversable)
		{
			foreach ($value as $key => $ov)
			{
				if ($ov instanceof \DateTime)
				{
					$result[$key] = $ov->format(\DateTime::ATOM);
				}
				elseif (is_object($ov) || is_array($ov))
				{
					$result[$key] = $this->models->normalize($ov, 'Object', $this->getFallbackContext());
				}
				else
				{
					$result[$key] = $ov;
				}
			}
		}
		return $result;
	}

	/**
	 * @param \Rbs\Ua\Model\Models $models
	 * @return $this
	 * @throws \LogicException
	 */
	public function compile(\Rbs\Ua\Model\Models $models)
	{
		if (!$this->isCompiled())
		{
			$this->models = $models;
			$this->markCompiled();
		}
		return $this;
	}
}