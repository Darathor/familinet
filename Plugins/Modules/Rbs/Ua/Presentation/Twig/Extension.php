<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Presentation\Twig;

/**
 * @name \Rbs\Ua\Presentation\Twig\Extension
 */
class Extension implements \Change\Presentation\Templates\Twig\ExtensionInterface
{
	/**
	 * @var \Change\Application
	 */
	protected $application;

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var string|null
	 */
	protected $assetsBaseUrl;

	/**
	 * @param \Change\Application $application
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @param \Change\Documents\ModelManager $modelManager
	 */
	public function __construct(\Change\Application $application, \Change\I18n\I18nManager $i18nManager = null, \Change\Documents\ModelManager $modelManager = null)
	{
		$this->application = $application;
		$this->i18nManager = $i18nManager;
		$this->modelManager = $modelManager;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Rbs_Ua';
	}

	/**
	 * @return \Change\Application
	 */
	public function getApplication()
	{
		return $this->application;
	}

	/**
	 * @param \Change\Application $application
	 * @return $this
	 */
	public function setApplication($application)
	{
		$this->application = $application;
		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getAssetsBaseUrl()
	{
		return $this->assetsBaseUrl;
	}

	/**
	 * @param null|string $assetsBaseUrl
	 * @return $this
	 */
	public function setAssetsBaseUrl($assetsBaseUrl)
	{
		$this->assetsBaseUrl = $assetsBaseUrl;
		return $this;
	}

	/**
	 * Initializes the runtime environment.
	 * This is where you can load some file that contains filter functions for instance.
	 * @param \Twig\Environment $environment The current \Twig\Environment instance
	 */
	public function initRuntime(\Twig\Environment $environment)
	{
	}

	/**
	 * Returns the token parser instances to add to the existing list.
	 * @return array An array of Twig_TokenParserInterface or Twig_TokenParserBrokerInterface instances
	 */
	public function getTokenParsers()
	{
		return [];
	}

	/**
	 * Returns the node visitor instances to add to the existing list.
	 * @return array An array of Twig_NodeVisitorInterface instances
	 */
	public function getNodeVisitors()
	{
		return [];
	}

	/**
	 * Returns a list of filters to add to the existing list.
	 * @return array An array of filters
	 */
	public function getFilters()
	{
		return [
			new \Twig\TwigFilter('snakeCase', [$this, 'snakeCase'])
		];
	}

	/**
	 * Returns a list of tests to add to the existing list.
	 * @return array An array of tests
	 */
	public function getTests()
	{
		return [];
	}

	/**
	 * Returns a list of functions to add to the existing list.
	 * @return array An array of functions
	 */
	public function getFunctions()
	{
		return [
			new \Twig\TwigFunction('propertyKey', [$this, 'propertyKey']),
			new \Twig\TwigFunction('modelKey', [$this, 'modelKey']),
			new \Twig\TwigFunction('transLCID', [$this, 'transLCID']),
			new \Twig\TwigFunction('resourceURL', [$this, 'resourceURL'])
		];
	}

	/**
	 * Returns a list of operators to add to the existing list.
	 * @return array An array of operators
	 */
	public function getOperators()
	{
		return [];
	}

	/**
	 * Returns a list of global variables to add to the existing list.
	 * @return array An array of global variables
	 */
	public function getGlobals()
	{
		return [];
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @param string $string
	 * @param string $separator
	 * @return string
	 */
	public function snakeCase($string, $separator = '_')
	{
		return \Change\Stdlib\StringUtils::snakeCase($string, $separator);
	}

	/**
	 * Get the property translation key for label
	 * @param null|string $modelName
	 * @param null|string $propertyName
	 * @return null|string
	 */
	public function propertyKey($modelName = null, $propertyName = null)
	{
		$mm = $this->getModelManager();
		if ($modelName)
		{
			$model = $mm->getModelByName($modelName);
			if ($model && $model->hasProperty($propertyName))
			{
				return $model->getPropertyLabelKey($propertyName);
			}
		}
		return $propertyName;
	}

	/**
	 * @param null|string $modelName
	 * @return null|string
	 */
	public function modelKey($modelName = null)
	{
		$mm = $this->getModelManager();
		if ($modelName)
		{
			$model = $mm->getModelByName($modelName);
			if ($model)
			{
				return $model->getLabelKey();
			}
		}
		return $modelName;
	}

	/**
	 * @param string $LCID
	 * @return string
	 */
	public function transLCID($LCID)
	{
		return htmlspecialchars($this->getI18nManager()->transLCID($LCID));
	}

	/**
	 * @return string
	 */
	protected function getAssetsVersion()
	{
		return trim((string)$this->getApplication()->getConfiguration()->getEntry('Change/Install/assetsVersion', ''), '/');
	}

	/**
	 * @param string $relativePath
	 * @return string
	 */
	public function resourceURL($relativePath)
	{
		if ($this->assetsBaseUrl === null)
		{
			$this->assetsBaseUrl = '/Assets/' . $this->getAssetsVersion() . '/';
		}

		if (strpos($relativePath, '/') !== 0)
		{
			return $this->assetsBaseUrl . $relativePath;
		}
		return $relativePath;
	}
}