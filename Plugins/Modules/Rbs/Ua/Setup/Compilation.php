<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Setup;

/**
 * @name \Rbs\Ua\Setup\Compilation
 */
class Compilation
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 */
	public function compileAngularControllersMapping(\Change\Plugins\Plugin $plugin, \Change\Application $application)
	{
		$controllers = [];
		$dependencies = [];

		$pluginName = $plugin->getName();
		if ($pluginName === 'Rbs_Ua')
		{
			$proximisPath = $plugin->getRelativePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'Proximis';
			$this->compileDirectory($application, $proximisPath, $controllers, $dependencies);
			$proximisPath = $plugin->getRelativePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'ProximisModal';
			$this->compileDirectory($application, $proximisPath, $controllers, $dependencies);
			$proximisPath = $plugin->getRelativePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'ProximisRichtext';
			$this->compileDirectory($application, $proximisPath, $controllers, $dependencies);
			$proximisPath = $plugin->getRelativePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'ProximisRichtextUa';
			$this->compileDirectory($application, $proximisPath, $controllers, $dependencies);
		}

		$uaPath = $plugin->getRelativePath() . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR . 'Ua';
		$this->compileDirectory($application, $uaPath, $controllers, $dependencies);

		$workspace = $application->getWorkspace();
		\Change\Stdlib\FileUtils::mkdir($workspace->compilationPath('Ua'));
		$destinationFile = $workspace->compilationPath('Ua', 'controllersData.php');
		/** @noinspection PhpIncludeInspection */
		$data = file_exists($destinationFile) ? (include $destinationFile) : ['mapping' => [], 'dependencies' => []];
		if ($controllers)
		{
			$data['mapping'][$pluginName] = $controllers;
		}
		else
		{
			unset($data['mapping'][$pluginName]);
		}
		if ($dependencies)
		{
			$data['dependencies'][$pluginName] = $dependencies;
		}
		else
		{
			unset($data['dependencies'][$pluginName]);
		}
		$content = '<?php' . PHP_EOL . 'return ' . var_export($data, true) . ';';
		\Change\Stdlib\FileUtils::write($destinationFile, $content);
	}

	/**
	 * @param \Change\Application $application
	 * @param string $uaPath
	 * @param array $controllers
	 * @param array $dependencies
	 */
	protected function compileDirectory(\Change\Application $application, $uaPath, &$controllers, &$dependencies)
	{
		$projectPath = $application->getWorkspace()->projectPath();
		$uaPath = $projectPath . DIRECTORY_SEPARATOR . $uaPath;
		if (is_dir($uaPath))
		{
			$projectPathLength = strlen($projectPath) + 1;
			$iterator = new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator($uaPath, \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::KEY_AS_PATHNAME),
				\RecursiveIteratorIterator::SELF_FIRST
			);
			foreach ($iterator as $path => $fileInfo)
			{
				/* @var $fileInfo \SplFileInfo */
				if ($fileInfo->isFile() && $fileInfo->getExtension() === 'js')
				{
					$content = \Change\Stdlib\FileUtils::read($path);

					preg_match_all('#\$controller\(\s*\'([A-Za-z0-9]+)\',#m', $content, $matches);
					$foundDependencies = [];
					foreach ($matches[1] as $dependency)
					{
						$foundDependencies[$dependency] = true;
					}

					preg_match_all('#\.controller\(\s*\'([A-Za-z0-9]+)\',#m', $content, $matches);
					foreach ($matches[1] as $controller)
					{
						if (!isset($controllers[$controller]))
						{
							$controllers[$controller] = substr($path, $projectPathLength);
							if ($foundDependencies)
							{
								$dependencies[$controller] = array_diff(array_keys($foundDependencies), [$controller]);
							}
						}
						else
						{
							$application->getLogging()
								->warn('Duplicate Angular controller declaration in files', $path, 'and' . $controllers[$controller]);
						}
					}
				}
			}
		}
	}
}