<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Setup;

/**
 * Only used by \Change\Plugins\InstallBase descendant
 * @name \Rbs\Ua\Setup\Dependency
 */
trait Dependency
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onSetupInitialize(\Change\Events\Event $event)
	{
		/** @noinspection PhpUndefinedClassInspection */
		parent::onSetupInitialize($event);

		$plugin = $event->getParam('plugin');
		if ($plugin instanceof \Change\Plugins\Plugin && $plugin->getName() === 'Rbs_Ua'
			&& \in_array('Application', $event->getParam('steps'), true))
		{
			$pluginManager = $event->getTarget();
			if ($pluginManager instanceof \Change\Plugins\PluginManager)
			{
				$extraPlugins = $event->getParam('extraPlugins');
				if (!\in_array($this->plugin, $extraPlugins, true))
				{
					$extraPlugins[] = $this->plugin;
				}
				$event->setParam('extraPlugins', $extraPlugins);
			}
		}
	}
}