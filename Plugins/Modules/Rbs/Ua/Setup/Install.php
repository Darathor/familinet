<?php
namespace Rbs\Ua\Setup;

/**
 * @name \Rbs\Ua\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$webBaseDirectory = $configuration->getEntry('Change/Install/webBaseDirectory');
		$workspace = $application->getWorkspace();
		if ($webBaseDirectory && !$workspace->isAbsolutePath($webBaseDirectory))
		{
			$requirePath = \implode(\DIRECTORY_SEPARATOR, \array_fill(0, \substr_count($webBaseDirectory, \DIRECTORY_SEPARATOR) + 1, '..'));
		}
		else
		{
			$requirePath = $workspace->projectPath();
		}

		$webBasePath = $workspace->composeAbsolutePath($webBaseDirectory);
		if (is_dir($webBasePath))
		{
			$srcPath = __DIR__ . '/Assets/ua.php';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$content = str_replace('__DIR__', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($webBasePath . DIRECTORY_SEPARATOR . basename($srcPath), $content);

			$srcPath = __DIR__ . '/Assets/uaRest.php';
			$content = \Change\Stdlib\FileUtils::read($srcPath);
			$content = str_replace('__DIR__', var_export($requirePath, true), $content);
			\Change\Stdlib\FileUtils::write($webBasePath . DIRECTORY_SEPARATOR . basename($srcPath), $content);
		}
		else
		{
			throw new \RuntimeException('Invalid document root path: ' . $webBasePath .
				'. Check "Change/Install/webBaseDirectory" configuration entry.', 999999);
		}
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		parent::executeThemeAssets($plugin, $pluginManager, $application);

		(new \Rbs\Ua\Setup\Compilation())->compileAngularControllersMapping($plugin, $application);

		$manager = new \Rbs\Ua\Http\UI\Resources('common');
		$manager->setApplication($application)->setPluginManager($pluginManager);
		$assetManager = $manager->getNewAssetManager();
		$manager->registerAssets($assetManager);
		$manager->registerCommonLibAssets($assetManager);
		$manager->write($assetManager);

	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \RuntimeException
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$registration = new \Rbs\Ua\Setup\Registration();
		$applicationNames = ['server'];
		$registration->installApplication($plugin, $applicationNames, $applicationServices);
	}
}
