<?php
/**
 * Copyright (C) 2015 Proximis
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua\Setup;

/**
 * @name \Rbs\Ua\Setup\Registration
 */
class Registration
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param string[]|string $names
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function installApplication(\Change\Plugins\Plugin $plugin, $names, \Change\Services\ApplicationServices $applicationServices)
	{
		$OAuthManager = $applicationServices->getOAuthManager();
		$documentManager = $applicationServices->getDocumentManager();
		$dbProvider = $applicationServices->getDbProvider();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			foreach ((array)$names as $name)
			{
				$applicationName = $plugin->getName() . '_' . $name;

				$this->createApplication($applicationName, $OAuthManager, $dbProvider);

				$realm = $applicationName;
				$groupLabel = ucfirst($name);
				$identifier = strtolower($name);
				$this->createGroup($realm, $groupLabel, $identifier, $documentManager);
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $applicationName
	 * @param \Change\Http\OAuth\OAuthManager $OAuthManager
	 * @param \Change\Db\DbProvider $dbProvider
	 */
	public function createApplication($applicationName, \Change\Http\OAuth\OAuthManager $OAuthManager, \Change\Db\DbProvider $dbProvider)
	{
		$consumer = $OAuthManager->getConsumerByApplication($applicationName);
		if (!$consumer)
		{
			$consumer = new \Change\Http\OAuth\Consumer($OAuthManager->generateConsumerKey(),
				$OAuthManager->generateConsumerSecret());
			$isb = $dbProvider->getNewStatementBuilder();
			$fb = $isb->getFragmentBuilder();
			$isb->insert($fb->table($isb->getSqlMapping()->getOAuthApplicationTable()), $fb->column('application'),
				$fb->column('consumer_key'), $fb->column('consumer_secret'), $fb->column('timestamp_max_offset'),
				$fb->column('token_access_validity'), $fb->column('token_request_validity'), $fb->column('active'));
			$isb->addValues($fb->parameter('application'), $fb->parameter('consumer_key'),
				$fb->parameter('consumer_secret'),
				$fb->integerParameter('timestamp_max_offset'), $fb->parameter('token_access_validity'),
				$fb->parameter('token_request_validity'), $fb->booleanParameter('active'));
			$iq = $isb->insertQuery();
			$iq->bindParameter('application', $applicationName);
			$iq->bindParameter('consumer_key', $consumer->getKey());
			$iq->bindParameter('consumer_secret', $consumer->getSecret());
			$iq->bindParameter('timestamp_max_offset', 60);
			$iq->bindParameter('token_access_validity', 'P10Y');
			$iq->bindParameter('token_request_validity', 'P1D');
			$iq->bindParameter('active', true);
			$iq->execute();
		}
	}

	/**
	 * @param string $realm
	 * @param string $groupLabel
	 * @param string $identifier
	 * @param \Change\Documents\DocumentManager $documentManager
	 */
	public function createGroup($realm, $groupLabel, $identifier, \Change\Documents\DocumentManager $documentManager)
	{
		$query = $documentManager->getNewQuery('Rbs_User_Group');
		$query->andPredicates($query->eq('realm', $realm));
		if (!$query->getCountDocuments())
		{
			/** @var \Rbs\User\Documents\Group $group */
			$group = $documentManager->getNewDocumentInstanceByModelName('Rbs_User_Group');
			$group->setRealm($realm);
			$group->setLabel($groupLabel);
			$group->setIdentifier($identifier);
			$group->save();
		}
	}
}