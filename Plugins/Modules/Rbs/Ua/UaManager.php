<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Ua;

/**
 * @name \Rbs\Ua\UaManager
 */
class UaManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	const EVENT_MANAGER_IDENTIFIER = 'UaManager';

	/**
	 * @var \Change\I18n\I18nManager
	 */
	protected $i18nManager;

	/**
	 * @var \Change\Plugins\PluginManager
	 */
	protected $pluginManager;

	/**
	 * @var \Change\Documents\ModelManager
	 */
	protected $modelManager;

	/**
	 * @var array
	 */
	protected $resourcesCache = [];

	/**
	 * @return string
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/Ua/Events/UaManager');
	}

	/**
	 * @return \Change\I18n\I18nManager
	 */
	protected function getI18nManager()
	{
		return $this->i18nManager;
	}

	/**
	 * @param \Change\I18n\I18nManager $i18nManager
	 * @return $this
	 */
	public function setI18nManager($i18nManager)
	{
		$this->i18nManager = $i18nManager;
		return $this;
	}

	/**
	 * @return \Change\Plugins\PluginManager
	 */
	protected function getPluginManager()
	{
		return $this->pluginManager;
	}

	/**
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @return $this
	 */
	public function setPluginManager($pluginManager)
	{
		$this->pluginManager = $pluginManager;
		return $this;
	}

	/**
	 * @return \Change\Documents\ModelManager
	 */
	protected function getModelManager()
	{
		return $this->modelManager;
	}

	/**
	 * @param \Change\Documents\ModelManager $modelManager
	 * @return $this
	 */
	public function setModelManager($modelManager)
	{
		$this->modelManager = $modelManager;
		return $this;
	}

	/**
	 * @param string $vendorName
	 * @param string $moduleName
	 * @param string $applicationName
	 * @return \Rbs\Ua\Http\UI\Resources|null
	 */
	public function getResources($vendorName, $moduleName, $applicationName)
	{
		$cacheKey = $vendorName . '_' . $moduleName . '_' . $applicationName;
		if (!array_key_exists($cacheKey, $this->resourcesCache))
		{
			if ($vendorName === 'Rbs' && $moduleName === 'Ua' && $applicationName === 'common')
			{
				$this->setResources($vendorName, $moduleName, $applicationName, new \Rbs\Ua\Http\UI\Resources($applicationName));
			}
			else
			{
				$eventManager = $this->getEventManager();
				$args = $eventManager->prepareArgs(
					['vendorName' => $vendorName, 'moduleName' => $moduleName, 'applicationName' => $applicationName, 'resources' => null]
				);
				$eventManager->trigger('getResources', $this, $args);
				$this->setResources($vendorName, $moduleName, $applicationName, $args['resources']);
			}
		}
		return $this->resourcesCache[$cacheKey];
	}

	/**
	 * @param string $vendorName
	 * @param string $moduleName
	 * @param string $applicationName
	 * @param \Rbs\Ua\Http\UI\Resources $resources
	 */
	public function setResources($vendorName, $moduleName, $applicationName, $resources)
	{
		$cacheKey = $vendorName . '_' . $moduleName . '_' . $applicationName;
		if ($resources instanceof \Rbs\Ua\Http\UI\Resources)
		{
			$resources->setApplication($this->getApplication())
				->setI18nManager($this->getI18nManager())
				->setPluginManager($this->getPluginManager())
				->setModelManager($this->getModelManager());
			$this->resourcesCache[$cacheKey] = $resources;
		}
		else
		{
			$this->resourcesCache[$cacheKey] = null;
		}
	}
}