(function() {
	'use strict';

	var app = angular.module('RbsChange');

	app.controller('Rbs_User_Group_PublicProfileController', ['$scope', '$routeParams', 'RbsChange.REST', PublicProfileController]);

	function PublicProfileController($scope, $routeParams, REST) {
		REST.resource($routeParams.id).then(function(group) {
			$scope.document = group;

			$scope.query = {
				'model': 'Rbs_User_User',
				'join': [
					{
						'model': 'Rbs_User_Group',
						'name': 'jgroup',
						'parentProperty': 'groups'
					}
				],
				'where': {
					'and': [
						{
							'op': 'eq',
							'lexp': {
								'property': 'id',
								'join': 'jgroup'
							},
							'rexp': {
								'value': group.id
							}
						}
					]
				}
			};
		});
	}

	app.controller('Rbs_User_Group_GroupUsersController', ['$scope', '$routeParams', 'RbsChange.REST', '$http', 'RbsChange.Navigation',
		GroupUsersController]);

	function GroupUsersController($scope, $routeParams, REST, $http, Navigation) {
		$scope.data = {
			usersToAdd: []
		};
		$scope.disableAdd = true;

		$scope.$on('Navigation.saveContext', function(event, args) {
			var data = {
				usersToAdd: $scope.data.usersToAdd
			};
			args.context.label($scope.document.label);
			args.context.savedData('GroupUsers', data);
		});

		function initContextData() {
			var currentContext = Navigation.getCurrentContext();
			if (currentContext) {
				var data = currentContext.savedData('GroupUsers');
				if (angular.isObject(data)) {
					$scope.data.usersToAdd = data.usersToAdd;
				}
			}
		}

		//Init from context
		initContextData();

		REST.resource($routeParams.id).then(function(group) {
			$scope.document = group;

			$scope.groupUsersQuery = {
				'model': 'Rbs_User_User',
				'join': [
					{
						'model': 'Rbs_User_Group',
						'name': 'jgroup',
						'parentProperty': 'groups'
					}
				],
				'where': {
					'and': [
						{
							'op': 'eq',
							'lexp': {
								'property': 'id',
								'join': 'jgroup'
							},
							'rexp': {
								'value': group.id
							}
						}
					]
				}
			};

			function reload() {
				$scope.$broadcast('Change:DocumentList:call', { method: 'reload' });
			}

			$scope.groupUsersList = {
				removeFromGroup: function(users) {
					var userIds = [];
					angular.forEach(users, function(user) {
						userIds.push(user.id);
					});
					var url = REST.getBaseUrl('user/removeUsersFromGroup');
					$http.post(url, { userIds: userIds, groupId: $scope.document.id }).then(
						function() {
							reload();
						}
					);
				}
			};

			$scope.$watch('data.usersToAdd', function(usersToAdd) {
				$scope.disableAdd = !usersToAdd || !usersToAdd.length;
			});

			$scope.addUsersFromPicker = function() {
				if ($scope.data.usersToAdd.length > 0) {
					var userIds = [];
					angular.forEach($scope.data.usersToAdd, function(user) {
						userIds.push(user.id);
					});
					var url = REST.getBaseUrl('user/addUsersInGroup');
					$http.post(url, { userIds: userIds, groupId: $scope.document.id }).then(
						function() {
							reload();
							$scope.data.usersToAdd = [];
						}
					);
				}
			};
		});
	}
})();