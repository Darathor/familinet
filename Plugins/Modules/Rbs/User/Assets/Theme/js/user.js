(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('rbsUserForgotPassword', ['RbsChange.AjaxAPI', rbsUserForgotPassword]);
	function rbsUserForgotPassword(AjaxAPI) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-forgot-password.twig',
			link: function(scope) {
				function createResetPasswordRequest(data) {
					scope.passwordError = null;
					scope.sending = true;
					AjaxAPI.postData('Rbs/User/User/ResetPasswordRequest', data).then(
						function() {
							scope.sending = false;
							scope.successSending = true;
						},
						function(result) {
							scope.sending = false;
							scope.successSending = false;
							scope.passwordError = result && result.data && result.data.message;
							console.error('rbsUserCreateAccount', result);
						}
					);
				}

				scope.diplayResetBox = false;
				scope.sending = false;
				scope.successSending = false;
				scope.resetPasswordEmail = null;
				scope.passwordError = null;

				scope.openBox = function() {
					jQuery('#reset-password-modal-main-content').modal({});
				};

				scope.invalidMail = function() {
					return !scope.resetPasswordEmail || scope.resetPasswordEmail == '';
				};

				scope.askReset = function() {
					createResetPasswordRequest({ email: scope.resetPasswordEmail });
				};
			}
		}
	}

	app.directive('rbsUserShortAccount',
		['$rootScope', 'RbsChange.AjaxAPI', '$window', 'RbsChange.ResponsiveSummaries', rbsUserShortAccount]);
	function rbsUserShortAccount($rootScope, AjaxAPI, window, ResponsiveSummaries) {
		return {
			restrict: 'A',
			link: function(scope) {
				var blockId = scope.blockId;
				var blockData = scope.blockData;
				scope.parameters = scope.blockParameters;
				scope.accessorId = scope.parameters.accessorId;
				scope.accessorName = scope.parameters.accessorName;
				if (blockData) {
					scope.userAccountPageUrl = blockData.userAccountPageUrl;
					scope.rootMenuEntry = blockData.rootMenuEntry;
				}

				$rootScope.$on('rbsUserConnected', function(event, params) {
					scope.accessorId = params['accessorId'];
					scope.accessorName = params['accessorName'];
				});

				$rootScope.$on('rbsUserProfileUpdated', function(event, params) {
					var fullName = params['profile']['profiles']['Rbs_User']['fullName'];
					if (fullName) {
						scope.accessorId = params['userId'];
						scope.accessorName = fullName;
					}
				});

				scope.logout = function() {
					AjaxAPI.getData('Rbs/User/Logout').then(
						function() {
							window.location.reload(true);
						},
						function(result) {
							scope.error = data.message;
							console.log('logout error', result);
						}
					);
				};

				ResponsiveSummaries.registerItem(blockId, scope, '<li data-rbs-user-short-account-responsive-summary=""></li>');
			}
		}
	}

	app.directive('rbsUserShortAccountResponsiveSummary', ['RbsChange.ModalStack',
		rbsUserShortAccountResponsiveSummary]);
	function rbsUserShortAccountResponsiveSummary(ModalStack) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-short-account-responsive-summary.twig',
			link: function(scope) {
				scope.onResponsiveSummaryClick = function() {
					var options = {
						templateUrl: '/rbs-user-short-account-responsive-summary-modal.twig',
						backdropClass: 'modal-backdrop-rbs-user-short-account-responsive-summary',
						windowClass: 'modal-responsive-summary modal-rbs-user-short-account-responsive-summary',
						scope: scope
					};
					ModalStack.open(options);
				}
			}
		};
	}

	app.directive('rbsUserUniqueEmail', ['$q', 'RbsChange.AjaxAPI', rbsUserUniqueEmail]);
	function rbsUserUniqueEmail($q, AjaxAPI) {
		return {
			require: 'ngModel',
			link: function(scope, element, attributes, ngModel) {
				var lastValid;
				ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
					if (viewValue && viewValue.length && viewValue !== lastValid) {
						return AjaxAPI.getData('Rbs/User/CheckEmailAvailability', { email: viewValue }).then(
							function(result) {
								lastValid = result.data.dataSets.user['availableEmail'];
							},
							function(result) {
								lastValid = undefined;
								if (result.status === 409 && result.data && result.data.message && result.data.code) {
									ngModel.errorMessage = result.data.message;
									ngModel.errorCode = result.data.code;
									return $q.reject('uniqueEmail');
								}
								else {
									console.error(result);
								}
							});
					}

					var deferred = $q.defer();
					deferred.resolve(true);
					return deferred.promise;
				};
			}
		}
	}

	app.directive('rbsUserManageAutoLogin', ['RbsChange.AjaxAPI', rbsUserManageAutoLogin]);
	function rbsUserManageAutoLogin(AjaxAPI) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-manage-auto-login.twig',
			controller: ['$scope', function(scope) {
				scope.parameters = scope.blockParameters;
				scope.tokens = scope.blockData;
			}],
			link: function(scope) {
				scope.errors = null;

				scope.deleteToken = function(index) {
					var data = {
						tokenId: scope.tokens[index].id
					};
					AjaxAPI.openWaitingModal();

					scope.errors = null;
					AjaxAPI.deleteData('Rbs/User/RevokeToken', data).then(
						function() {
							AjaxAPI.closeWaitingModal();
							scope.tokens.splice(index, 1);
							scope.errors = null;
							if (scope.tokens.length == 0) {
								scope.tokens = null;
							}
						},
						function(result) {
							AjaxAPI.closeWaitingModal();
							console.log('deleteToken error', result);
						}
					);
				}
			}
		};
	}

	app.directive('rbsUserLogin', ['RbsChange.AjaxAPI', '$rootScope', '$window', rbsUserLogin]);
	function rbsUserLogin(AjaxAPI, $rootScope, window) {
		function buildDevice() {
			var parser = new UAParser();
			var ua = parser.getResult();

			return ua.browser.name + ' - ' + ua.os.name;
		}

		return {
			restrict: 'A',
			templateUrl: '/rbs-user-login.twig',
			scope: false,
			controller: ['$scope', function(scope) {
				scope.error = null;
				scope.parameters = scope.blockParameters;
				scope.data = {
					login: null, password: null, realm: scope.parameters.realm,
					rememberMe: true, device: buildDevice()
				};

				this.getData = function() {
					return scope.data;
				};

				this.login = function(loginData) {
					scope.error = null;
					AjaxAPI.putData('Rbs/User/Login', loginData).then(
						function(result) {
							var user = result.data.dataSets.user;
							scope.parameters.accessorId = user.accessorId;
							scope.parameters.accessorName = user.name;
							if (scope.parameters['onLogin'] === 'reload') {
								window.location.reload(true)
							}
							else if (scope.parameters['onLogin'] === 'redirectToTarget') {
								window.location = scope.parameters['redirectionUrl'];
							}
							else {
								var params = { 'accessorId': user.accessorId, 'accessorName': user.name };
								$rootScope.$broadcast('rbsUserConnected', params);
							}
						},
						function(result) {
							scope.error = result.data.message;
							scope.parameters.password = scope.parameters.login = null;
							console.log('login error', result);
						}
					);
				};

				this.logout = function() {
					var request = AjaxAPI.getData('Rbs/User/Logout').then(
						function() {
							window.location.reload(true); // In logout case, always reload.
						},
						function(result) {
							scope.error = result.data.message;
							console.log('logout error', result);
						}
					);
				}
			}],
			link: function(scope, elm, attrs, controller) {
				scope.showTitle = attrs['showTitle'] !== 'false';
				scope.login = function() {
					controller.login(scope.data);
				};

				scope.logout = function() {
					controller.logout();
				}
			}
		}
	}

	app.directive('rbsUserCreateAccount', ['RbsChange.AjaxAPI', rbsUserCreateAccount]);
	function rbsUserCreateAccount(AjaxAPI) {
		function buildDevice() {
			var parser = new UAParser();
			var ua = parser.getResult();

			return ua.browser.name + ' - ' + ua.os.name;
		}

		return {
			restrict: 'A',
			templateUrl: '/rbs-user-create-account.twig',
			scope: false,
			controllerAs: 'createAccountController',
			controller: ['$scope', '$element', '$attrs', function(scope, elem, attrs) {
				scope.parameters = scope.blockParameters;

				scope.hideTitle = attrs.hideTitle === 'true';
				scope.fixedEmail = attrs.fixedEmail;

				scope.confirmationPage = scope.parameters.confirmationPage ? parseInt(scope.parameters.confirmationPage) : 0;
				scope.handleNewsletter = scope.parameters.handleNewsletter;
				scope.customerFields = scope.parameters.customerFields;
				scope.allowedTitles = scope.parameters.allowedTitles;

				scope.data = {
					email: scope.fixedEmail ? scope.fixedEmail : null, password: null,
					confirmationPage: scope.confirmationPage
				};

				scope.profiles = scope.blockData ? (scope.blockData.profiles || {}) : {};

				this.getData = function() {
					return scope.data;
				};

				this.checkAccountRequest = function(data) {
					scope.requestAccountCreated = true;
					scope.error = null;
					var request = AjaxAPI.getData('Rbs/User/User/AccountRequest', data);
					request.then(function(result) {
						scope.requestAccountCreated = result.data.dataSets && result.data.dataSets.common && result.data.dataSets.common.hasRequest;
					}, function() {
						scope.requestAccountCreated = false;
					});
					return request;
				};

				this.createAccountRequest = function(data) {
					AjaxAPI.openWaitingModal();
					scope.requestAccountCreated = true;
					scope.error = null;
					var request = AjaxAPI.postData('Rbs/User/User/AccountRequest', data);
					request.then(function() {
						delete data.password;
						AjaxAPI.closeWaitingModal();
					}, function(result) {
						if (result.data && result.data.message) {
							scope.error = result.data.message;
						}
						console.error('createAccountRequest', result);
						scope.requestAccountCreated = false;
						AjaxAPI.closeWaitingModal();
					});
					return request;
				};

				this.confirmAccountRequest = function(data) {
					var request = AjaxAPI.putData('Rbs/User/User/AccountRequest', data);
					AjaxAPI.openWaitingModal();
					scope.error = null;
					request.then(function(result) {
						if (result.data && result.data.dataSets && result.data.dataSets.confirmationPage) {
							window.location.href = result.data.dataSets.confirmationPage.url;
						}
						else {
							scope.accountConfirmed = true;
							AjaxAPI.closeWaitingModal();
						}
					}, function(result) {
						if (result.data && result.data.message) {
							scope.error = result.data.message;
						}
						console.error('rbsUserConfirmAccount', result);
						scope.accountConfirmed = true;
						AjaxAPI.closeWaitingModal();
					});
					return request;
				};

				if (scope.parameters.token && scope.parameters.email) {
					this.confirmAccountRequest({
						token: scope.parameters.token,
						email: scope.parameters.email, confirmationPage: scope.data.confirmationPage
					})
				}

				if (scope.data.email) {
					this.checkAccountRequest(scope.data);
				}
			}],

			link: function(scope, elm, attrs, controller) {
				scope.showCreationForm = function() {
					return !scope.requestAccountCreated && !scope.parameters.token;
				};

				scope.showConfirmationForm = function() {
					return (scope.requestAccountCreated && !scope.accountConfirmed) || (scope.accountConfirmed && scope.error);
				};

				scope.submit = function() {
					scope.data.profiles = scope.profiles;
					scope.data.profileData = scope.profiles.Rbs_User || {};
					controller.createAccountRequest(scope.data);
				};
			}
		}
	}

	app.directive('rbsUserGenericFields', rbsUserGenericFields);
	function rbsUserGenericFields() {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-generic-fields.twig',
			link: function(scope) {
				scope.displayTitleCode = true;
			}
		}
	}

	app.directive('rbsUserChangeEmail', ['$location', 'RbsChange.AjaxAPI', rbsUserChangeEmail]);
	function rbsUserChangeEmail($location, AjaxAPI) {
		return {
			restrict: 'A',
			scope: false,
			controllerAs: 'changeEmailController',
			controller: ['$scope', '$attrs', function(scope, attrs) {
				scope.data = {
					password: null,
					email: null,
					confirmationUrl: attrs.confirmationUrl || $location.absUrl()
				};

				this.getData = function() {
					return scope.data;
				};

				this.createChangeEmailRequest = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					AjaxAPI.postData('Rbs/User/User/ChangeEmail', data).then(
						function() {
							delete data.password;
							delete data.confirmationUrl;
							scope.requestEmailCreated = true;
							AjaxAPI.closeWaitingModal();
						},
						function(result) {
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('rbsUserChangeEmail - request creation', result);
							scope.requestEmailCreated = false;
							AjaxAPI.closeWaitingModal();
						}
					);
				};

				this.confirmChangeEmailRequest = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					AjaxAPI.putData('Rbs/User/User/ChangeEmail', data).then(
						function() {
							scope.emailConfirmed = true;
							AjaxAPI.closeWaitingModal();
						},
						function(result) {
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('rbsUserChangeEmail - request confirmation', result);
							scope.emailConfirmed = true;
							AjaxAPI.closeWaitingModal();
						}
					);
				};
			}],

			link: function(scope, elm, attrs, controller) {
				scope.authenticated = scope.blockParameters.authenticated;
				scope.showTitle = scope.blockParameters.showTitle;

				if (scope.blockParameters.token && scope.blockParameters.email) {
					controller.confirmChangeEmailRequest({
						token: scope.blockParameters.token, email: scope.blockParameters.email
					});
				}

				scope.showCreationForm = function() {
					return scope.authenticated && !scope.requestEmailCreated && !scope.blockParameters.token;
				};

				scope.showConfirmationForm = function() {
					return (scope.requestEmailCreated && !scope.emailConfirmed) || (scope.emailConfirmed && scope.error);
				};
			}
		}
	}

	app.directive('rbsUserChangeMobilePhone', ['RbsChange.AjaxAPI', rbsUserChangeMobilePhone]);
	function rbsUserChangeMobilePhone(AjaxAPI) {
		return {
			restrict: 'A',
			controllerAs: 'changeMobilePhoneController',
			controller: ['$scope', function(scope) {
				scope.inputNumber = true;
				scope.confirmNumber = false;

				scope.data = {
					password: null,
					mobilePhone: null,
					tokenValidation: null
				};

				this.getData = function() {
					return scope.data;
				};

				this.changeMobilePhoneNumber = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					AjaxAPI.postData('Rbs/User/User/ChangeMobilePhoneNumber', data).then(
						function() {
							delete scope.data.password;
							scope.inputNumber = false;
							scope.confirmNumber = true;
							AjaxAPI.closeWaitingModal();
						},
						function(result) {
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('ChangeMobilePhoneNumber', result);
							AjaxAPI.closeWaitingModal();
						}
					);
				};

				this.confirmMobilePhoneNumber = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					var request = AjaxAPI.putData('Rbs/User/User/ChangeMobilePhoneNumber', data).then(
						function() {
							delete scope.data.password;
							scope.inputNumber = true;
							scope.confirmNumber = false;
							AjaxAPI.closeWaitingModal();
						},
						function(result) {
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('ChangeMobilePhoneNumber', result);
							AjaxAPI.closeWaitingModal();
						}
					);
				};
			}],

			link: function(scope, elm, attrs, controller) {
				scope.showTitle = scope.blockParameters.showTitle;
				scope.submit = function() {
					if (scope.inputNumber) {
						if (scope.data.mobilePhone && scope.data.password) {
							controller.changeMobilePhoneNumber(scope.data);
						}
					}

					if (scope.confirmNumber) {
						if (scope.data.tokenValidation) {
							controller.confirmMobilePhoneNumber(scope.data);
						}
					}
				}
			}
		}
	}

	app.directive('rbsUserResetPassword', ['RbsChange.AjaxAPI', rbsUserResetPassword]);
	function rbsUserResetPassword(AjaxAPI) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-reset-password.twig',
			scope: false,
			controller: ['$scope', function(scope) {
				scope.parameters = scope.blockParameters;

				scope.data = {
					token: scope.parameters.token ? scope.parameters.token : null,
					password: null
				};

				this.getData = function() {
					return scope.data;
				};

				this.confirmResetPassword = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;

					AjaxAPI.putData('Rbs/User/User/ResetPasswordRequest', data).then(
						function() {
							AjaxAPI.closeWaitingModal();
							scope.passwordConfirmed = true;
						},
						function(result) {
							AjaxAPI.closeWaitingModal();
							scope.passwordConfirmed = false;
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('rbsUserResetPassword', result);
						}
					);
				};
			}],

			link: function(scope, elm, attrs, controller) {
				scope.showTitle = scope.parameters.showTitle;
				scope.showForm = function() {
					return scope.data.token && !scope.passwordConfirmed;
				};

				scope.submit = function() {
					controller.confirmResetPassword(scope.data);
				}
			}
		}
	}

	app.directive('rbsUserChangePassword', ['RbsChange.AjaxAPI', rbsUserChangePassword]);
	function rbsUserChangePassword(AjaxAPI) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-change-password.twig',
			controller: ['$scope', function(scope) {
				scope.parameters = scope.blockParameters;

				scope.data = {
					currentPassword: null,
					password: null
				};

				this.getData = function() {
					return scope.data;
				};

				this.changePassword = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					AjaxAPI.putData('Rbs/User/User/ChangePassword', data).then(
						function() {
							AjaxAPI.closeWaitingModal();
							scope.passwordConfirmed = true;
							scope.data = {
								currentPassword: null,
								password: null
							};
							scope.confirmPassword = null;
						},
						function(result) {
							AjaxAPI.closeWaitingModal();
							scope.passwordConfirmed = false;
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('changePassword', result);
						}
					);
				};
			}],

			link: function(scope, elm, attrs, controller) {
				scope.showTitle = scope.blockParameters.showTitle;
				scope.showForm = function() {
					return scope.parameters['authenticated'];
				};

				scope.submit = function() {
					controller.changePassword(scope.data);
				}
			}
		}
	}

	app.directive('rbsUserAccount', ['RbsChange.AjaxAPI', '$rootScope', rbsUserAccount]);
	function rbsUserAccount(AjaxAPI, $rootScope) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-user-account.twig',
			controller: ['$scope', function(scope) {
				scope.parameters = scope.blockParameters;
				scope.customerFields = scope.parameters.customerFields;
				scope.data = null;
				scope.profiles = {};

				if (scope.parameters.authenticated) {
					scope.parameters.authenticated = false;
					AjaxAPI.getData('Rbs/User/User/Profiles').then(function(result) {
						scope.parameters.authenticated = true;
						scope.data = result.data.dataSets;
						scope.profiles = scope.data.profiles;
						scope.allowedTitles = scope.profiles.Rbs_User.allowedTitles;
					});
				}

				this.saveProfiles = function(data) {
					AjaxAPI.openWaitingModal();
					scope.error = null;
					AjaxAPI.putData('Rbs/User/User/Profiles', data).then(
						function(result) {
							AjaxAPI.closeWaitingModal();
							scope.readonly = true;
							scope.success = true;
							scope.data = result.data.dataSets;
							scope.profiles = result.data.dataSets.profiles;
							var params = { 'profile': scope.data, 'userId': scope.data.common.id };
							$rootScope.$broadcast('rbsUserProfileUpdated', params);
						},
						function(result) {
							AjaxAPI.closeWaitingModal();
							if (result.data && result.data.message) {
								scope.error = result.data.message;
							}
							console.error('saveProfiles', result);
						}
					);
				};
			}],
			link: function(scope, elm, attrs, controller) {
				scope.showTitle = attrs['showTitle'] !== 'false';
				scope.success = false;
				scope.readonly = true;

				scope.openEdit = function() {
					scope.success = false;
					scope.readonly = false;
					scope.dataBackup = angular.copy(scope.data);
				};

				scope.saveAccount = function() {
					scope.data.profiles = scope.profiles;
					controller.saveProfiles(scope.data);
				};

				scope.cancelEdit = function() {
					scope.readonly = true;
					scope.data = scope.dataBackup;
					scope.profiles = scope.data.profiles;
				};
			}
		}
	}
})();