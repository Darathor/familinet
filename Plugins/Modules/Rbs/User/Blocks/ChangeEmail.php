<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

/**
 * @name \Rbs\User\Blocks\ChangeEmail
 */
class ChangeEmail extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * Event Params 'website', 'document', 'page'
	 * @api
	 * Set Block Parameters on $event
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('showTitle', true);
		$parameters->addParameterMeta('confirmationPage', 0);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$parameters->addParameterMeta('authenticated', false);
		$user = $event->getAuthenticationManager()->getCurrentUser();
		if ($user->authenticated())
		{
			$parameters->setParameterValue('authenticated', true);
		}

		$confirmation = $event->getHttpRequest()->getQuery('changeEmail');
		if (is_array($confirmation) && isset($confirmation['token'], $confirmation['email']))
		{
			$parameters->addParameterMeta('token', $confirmation['token']);
			$parameters->addParameterMeta('email', $confirmation['email']);
		}

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		return 'change-email.twig';
	}
}