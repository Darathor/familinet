<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

/**
 * @name \Rbs\User\Blocks\CreateAccountInformation
 */
class CreateAccountInformation extends \Change\Presentation\Blocks\Information
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.user.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.user.admin.create_account', $ucf));

		$this->addParameterInformation('confirmationPage', \Change\Documents\Property::TYPE_DOCUMENTID, false, 0)
			->setAllowedModelsNames('Rbs_Website_StaticPage')
			->setLabel($i18nManager->trans('m.rbs.user.admin.confirmation_page', $ucf));
		$this->addParameterInformation('handleNewsletter', \Change\Documents\Property::TYPE_BOOLEAN, false, false)
			->setLabel($i18nManager->trans('m.rbs.user.admin.handle_newsletter', $ucf));

		$this->addParameterInformation('customerFields', \Change\Documents\Property::TYPE_STRING, false, '')
			->setLabel($i18nManager->trans('m.rbs.user.admin.customer_fields', $ucf))
			->setCollectionCode('Rbs_User_CustomerFields');
	}
}
