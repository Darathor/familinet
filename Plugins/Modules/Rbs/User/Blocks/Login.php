<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Blocks;

/**
 * @name \Rbs\User\Blocks\Login
 */
class Login extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Optional Event method: getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		/** @var \Change\Presentation\Interfaces\Website|null $website */
		$website = $event->getParam('website');

		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('realm', $website ? $website->getRealm() : null);
		$parameters->addParameterMeta('onLogin', 'doNothing');
		$parameters->addParameterMeta('redirectionTargetId');
		$parameters->addParameterMeta('showTitle', true);
		$parameters->setLayoutParameters($event->getBlockLayout());

		$user = $event->getAuthenticationManager()->getCurrentUser();
		if ($user->authenticated())
		{
			$parameters->setParameterValue('accessorId', $user->getId());
			$parameters->setParameterValue('accessorName', $user->getName());
		}

		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 * @throws \RuntimeException
	 * @throws \InvalidArgumentException
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();
		if (!$parameters->getParameter('realm'))
		{
			return null;
		}

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		if ($parameters->getParameter('onLogin') === 'redirectToTarget')
		{
			$redirectionTargetId = $parameters->getParameter('redirectionTargetId');
			$target = $documentManager->getDocumentInstance($redirectionTargetId);
			if (!$target)
			{
				$event->getApplication()->getLogging()->warn(__METHOD__, 'Invalid redirection target id:', $redirectionTargetId);
				$parameters->setParameterValue('onLogin', 'reload');
			}
			else
			{
				$parameters->setParameterValue('redirectionUrl', $event->getUrlManager()->getCanonicalByDocument($target)->normalize()->toString());
			}
		}
		return 'login.twig';
	}
}