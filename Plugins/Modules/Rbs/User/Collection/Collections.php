<?php
/**
 * Copyright (C) 2016 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Collection;

/**
 * @name \Rbs\User\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addUserCustomerFields(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18n = $applicationServices->getI18nManager();
		$items = [
			'' => new \Change\I18n\I18nString($i18n, 'm.rbs.user.admin.none', ['ucf']),
			'rbs-user-generic-fields' => new \Change\I18n\I18nString($i18n, 'm.rbs.user.admin.customer_fields_default', ['ucf'])
		];
		$collection = new \Change\Collection\CollectionArray('Rbs_User_CustomerFields', $items);
		$event->setParam('collection', $collection);
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addOnLoginActions(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18n = $applicationServices->getI18nManager();
		$items = [
			'doNothing' => new \Change\I18n\I18nString($i18n, 'm.rbs.user.admin.action_do_nothing', ['ucf']),
			'reload' => new \Change\I18n\I18nString($i18n, 'm.rbs.user.admin.action_reload', ['ucf']),
			'redirectToTarget' => new \Change\I18n\I18nString($i18n, 'm.rbs.user.admin.action_redirect_to_target', ['ucf'])
		];
		$collection = new \Change\Collection\CollectionArray('Rbs_User_OnLoginActions', $items);
		$event->setParam('collection', $collection);
	}
}