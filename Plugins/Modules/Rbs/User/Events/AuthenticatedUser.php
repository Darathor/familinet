<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Events;

use Change\User\UserInterface;
use Rbs\User\Documents\User;

/**
 * @name \Rbs\User\Events\AuthenticatedUser
 */
class AuthenticatedUser implements UserInterface, \JsonSerializable
{
	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var AuthenticatedGroup[]
	 */
	protected $groups;

	/**
	 * @var integer[]
	 */
	protected $groupIds;

	/**
	 * @var String
	 */
	protected $name;

	/**
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
		$this->setName($user->getLabel());
	}

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->user->getId();
	}

	/**
	 * @param String $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return String
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return integer[]
	 */
	public function getGroupIds()
	{
		if ($this->groupIds === null)
		{
			$this->groupIds = $this->user->getGroupsIds();
		}
		return $this->groupIds;
	}

	/**
	 * @return \Change\User\GroupInterface[]
	 */
	public function getGroups()
	{
		if ($this->groups === null)
		{
			$this->groups = [];

			/** @var \Rbs\User\Documents\Group $group */
			foreach ($this->user->getGroups()->preLoad()->toArray() as $group)
			{
				$this->groups[] = new AuthenticatedGroup($group);
			}
		}
		return $this->groups;
	}

	/**
	 * @return boolean
	 */
	public function authenticated()
	{
		return true;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return ['id' => $this->getId(), 'groups' => $this->groups];
	}
}