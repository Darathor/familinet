<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Events;

/**
 * @name \Rbs\User\Events\Login
 */
class Login
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function execute(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();

		/** @var \Rbs\User\Documents\User|null $user */
		$user = null;

		if ($event->getParam('userId'))
		{
			$user = $applicationServices->getDocumentManager()->getDocumentInstance($event->getParam('userId'));
		}
		else
		{
			$realm = $event->getParam('realm');
			$login = $event->getParam('login');
			$password = $event->getParam('password');
			if (\Change\Stdlib\StringUtils::isEmpty($realm) || \Change\Stdlib\StringUtils::isEmpty($login)
				|| \Change\Stdlib\StringUtils::isEmpty($password)
			)
			{
				return;
			}

			if ($login === 'RBSCHANGE_AUTOLOGIN' && $realm === 'auto_login')
			{
				$qb = $applicationServices->getDbProvider()->getNewQueryBuilder();
				$fb = $qb->getFragmentBuilder();
				$qb->select($fb->column('user_id'));
				$qb->from($fb->table('rbs_user_auto_login'));
				$qb->where($fb->logicAnd(
					$fb->eq($fb->column('token'), $fb->parameter('token')),
					$fb->gt($fb->column('validity_date'), $fb->dateTimeParameter('validityDate'))
				));
				$sq = $qb->query();

				$sq->bindParameter('token', $password);
				$now = new \DateTime();
				$sq->bindParameter('validityDate', $now);
				$userId = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('user_id'));
				if ($userId)
				{
					$user = $applicationServices->getDocumentManager()->getDocumentInstance($userId);
				}
			}
			else
			{
				$query = $applicationServices->getDocumentManager()->getNewQuery('Rbs_User_User');
				$groupBuilder = $query->getPropertyBuilder('groups');
				$or = $query->getFragmentBuilder()->logicOr($query->eq('login', $login), $query->eq('email', $login));
				$query->andPredicates($query->activated(), $or, $groupBuilder->eq('realm', $realm));

				foreach ($query->getDocuments() as $document)
				{
					/* @var $document \Rbs\User\Documents\User */
					if ($document->checkPassword($password))
					{
						$user = $document;
						break;
					}
				}
			}
		}

		if ($user instanceof \Rbs\User\Documents\User && $user->activated())
		{
			$authenticatedUser = new AuthenticatedUser($user);
			$profile = $event->getApplicationServices()->getProfileManager()->loadProfile($authenticatedUser, 'Rbs_User');
			if ($profile)
			{
				$fullName = $profile->getPropertyValue('fullName');
				if (!\Change\Stdlib\StringUtils::isEmpty($fullName))
				{
					$authenticatedUser->setName($fullName);
				}
			}
			$event->setParam('user', $authenticatedUser);
			return;
		}
	}

	/**
	 * Input Param:
	 * - accessorId
	 * Output Param:
	 * - invalidatedAutoLogin
	 * @param \Change\Events\Event $event
	 */
	public function onInvalidateAutoLogin(\Change\Events\Event $event)
	{
		$accessorId = (int)$event->getParam('accessorId', 0);
		if ($accessorId > 0)
		{
			$applicationServices = $event->getApplicationServices();
			$dbProvider = $applicationServices->getDbProvider();
			$sb = $dbProvider->getNewStatementBuilder();
			$fb = $sb->getFragmentBuilder();
			$sb->update('rbs_user_auto_login');
			$sb->assign($fb->column('validity_date'), $fb->dateTimeParameter('validityDate'));
			$sb->where($fb->eq($fb->column('user_id'), $fb->integerParameter('userId')));

			$update = $sb->updateQuery();
			$update->bindParameter('validityDate', new \DateTime());
			$update->bindParameter('userId', $accessorId);
			$event->setParam('invalidatedAutoLogin', $update->execute());
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function saveLastLoginDate(\Change\Events\Event $event)
	{
		$options = $event->getParam('options');
		$user = $event->getParam('user');
		/* @var $httpEvent \Change\Events\Event */
		$httpEvent = $options['httpEvent'];
		$website = $httpEvent ? $httpEvent->getParam('website') : null;
		if ($website instanceof \Rbs\Website\Documents\Website && $user instanceof \Rbs\User\Events\AuthenticatedUser)
		{
			$websiteId = $website->getId();
			$userId = $user->getId();
			$now = new \DateTime();

			$applicationServices = $event->getApplicationServices();
			$transactionManager = $applicationServices->getTransactionManager();
			$dbProvider = $applicationServices->getDbProvider();

			$ro = $dbProvider->getReadOnly();
			try
			{
				$qb = $dbProvider->getNewQueryBuilder();
				$fb = $qb->getFragmentBuilder();

				$qb->select($fb->alias($fb->count($fb->column('user_id')), 'count'));
				$qb->from($fb->table('rbs_user_dat_login_date'));
				$qb->where($fb->logicAnd(
					$fb->eq($fb->column('user_id'), $fb->parameter('user_id')),
					$fb->eq($fb->column('website_id'), $fb->parameter('website_id'))
				));
				$sq = $qb->query();

				$sq->bindParameter('user_id', $userId);
				$sq->bindParameter('website_id', $websiteId);

				$hasLoginDate = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('count'));

				$sb = $dbProvider->getNewStatementBuilder();
				$dbProvider->setReadOnly(false);
				$transactionManager->begin();
				$fb = $sb->getFragmentBuilder();

				if ($hasLoginDate)
				{
					$sb->update($fb->table('rbs_user_dat_login_date'))
						->assign($fb->column('login_date'), $fb->dateTimeParameter('login_date'))
						->where($fb->logicAnd(
							$fb->eq($fb->column('user_id'), $fb->parameter('user_id')),
							$fb->eq($fb->column('website_id'), $fb->parameter('website_id'))
						));
					$query = $sb->updateQuery();
				}
				else
				{
					$sb->insert($fb->table('rbs_user_dat_login_date'),
						$fb->column('user_id'), $fb->column('website_id'), $fb->column('login_date'));
					$sb->addValues(
						$fb->integerParameter('user_id'), $fb->integerParameter('website_id'),
						$fb->dateTimeParameter('login_date'));

					$query = $sb->insertQuery();
				}

				$query->bindParameter('user_id', $userId);
				$query->bindParameter('website_id', $websiteId);
				$query->bindParameter('login_date', $now);

				$query->execute();

				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$event->getApplication()->getLogging()->exception($e);
			}
			$dbProvider->setReadOnly($ro);
		}
	}
}