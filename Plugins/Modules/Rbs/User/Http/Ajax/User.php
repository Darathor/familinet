<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Http\Ajax;

/**
* @name \Rbs\User\Http\Ajax\User
*/
class User
{
	/**
	 * Default actionPath: Rbs/User/User/AccountRequest
	 * Event params:
	 *  - data:
	 *     - email
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function checkAccountRequest(\Change\Http\Event $event)
	{
		$data = $event->getParam('data');
		if (is_array($data) && isset($data['email']))
		{
			$email = trim((string)$data['email']);
			$website = $event->getParam('website');
			$realm = $website instanceof \Change\Presentation\Interfaces\Website ? $website->getRealm() : 'web';

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$result = $genericServices->getUserManager()->hasAccountRequest($email, $realm);
			if ($result)
			{
				$itemData = ['common' => ['email' => $email, 'hasRequest' => true]];
				if (isset($data['confirmationPage']) && $data['confirmationPage'])
				{
					$website = $event->getParam('website');
					if ($website instanceof \Rbs\Website\Documents\Website)
					{
						$uri = $website->getUrlManager($website->getCurrentLCID())->getCanonicalByDocument($data['confirmationPage'], ['email' => $email]);
						$itemData['confirmationPage']['url'] = $uri->normalize()->toString();
					}
				}
			}
			else
			{
				$itemData = ['common' => ['email' => $email, 'hasRequest' => false]];
			}
			$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/AccountRequest', $itemData);
			$event->setResult($result);
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/AccountRequest
	 * Event params:
	 *  - website
	 *  - data:
	 *     - email
	 *     - password
	 *     - ...
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function createAccountRequest(\Change\Http\Event $event)
	{
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['email'], $data['password']))
		{
			$email = trim((string)$data['email']);
			$data['websiteId'] = $website->getId();
			$data['LCID'] = $website->getCurrentLCID();

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$result = $genericServices->getUserManager()->createAccountRequest($email, $data);
			if ($result)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/AccountRequest', ['common' => ['email' => $email]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'CreateAccountRequestError', $genericServices->getUserManager()->getLastError() ,
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$this->setErrorResult($event, 'Bad Request',  $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/AccountRequest
	 * Event params:
	 *  - website
	 *  - data:
	 *     - requestId
	 *     - email
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function confirmAccountRequest(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');

		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['token'], $data['email']))
		{
			$email = trim((string)$data['email']);
			$token = trim((string)$data['token']);

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$user = $genericServices->getUserManager()->confirmAccountRequest($token, $email);
			if ($user)
			{
				$itemData = ['common' => ['email' => $email, 'id' => $user->getId()]];
				if (isset($data['confirmationPage']) && $data['confirmationPage'])
				{
					$uri = $website->getUrlManager($website->getCurrentLCID())
						->getCanonicalByDocument($data['confirmationPage'], ['email' => $email]);
					$itemData['confirmationPage']['url'] = $uri->normalize()->toString();
				}

				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/AccountRequest', $itemData);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'ConfirmAccountRequestError', $genericServices->getUserManager()->getLastError() ,
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request',  $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ChangeEmail
	 * Event params:
	 *  - website
	 *  - data:
	 *     - password
	 *     - email
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function createChangeEmailRequest(\Change\Http\Event $event)
	{
		// Data obfuscation for further process (e.g. audit logs)
		$event->setParam('dataObfuscationCallable',
			new \Change\Stdlib\ChainedArrayCallable($event->getParam('dataObfuscationCallable'), function (array $data)
			{
				if (isset($data['data']['password']))
				{
					$data['data']['password'] = 'XXX';
				}
				return $data;
			}));
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['password'], $data['email']))
		{
			$password = (string)$data['password'];
			unset($data['password']);
			$email = trim((string)$data['email']);
			$data['websiteId'] = $website->getId();
			$data['LCID'] = $website->getCurrentLCID();

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$userId = $genericServices->getUserManager()->createChangeEmailRequest($password, $email, $data);
			if ($userId)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ChangeEmailRequest',
					['common' => ['email' => $email]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'ChangeEmail', $genericServices->getUserManager()->getLastError(),
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/AccountRequest
	 * Event params:
	 *  - website
	 *  - data:
	 *     - requestId
	 *     - email
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function confirmChangeEmailRequest(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['token'], $data['email']))
		{
			$email = trim((string)$data['email']);
			$confirmationCode = trim((string)$data['token']);

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$user = $genericServices->getUserManager()->confirmChangeEmailRequest($confirmationCode, $email);
			if ($user)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ChangeEmailRequest',
					['common' => ['email' => $email, 'id' => $user->getId()]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'ConfirmAccountRequestError', $genericServices->getUserManager()->getLastError(),
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ChangeMobilePhoneNumber
	 * Event params:
	 *  - autenticated User
	 *  - data:
	 *     - password
	 *     - mobilePhone
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function initChangeMobilePhoneRequest(\Change\Http\Event $event)
	{
		// Data obfuscation for further process (e.g. audit logs)
		$event->setParam('dataObfuscationCallable',
			new \Change\Stdlib\ChainedArrayCallable($event->getParam('dataObfuscationCallable'), function (array $data)
			{
				if (isset($data['data']['password']))
				{
					$data['data']['password'] = 'XXX';
				}
				return $data;
			}));
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();

		$currentUser = $event->getAuthenticationManager()->getCurrentUser();
		$data = $event->getParam('data');
		if (is_array($data) && isset($data['password'], $data['mobilePhone'])
			&& $currentUser->authenticated())
		{
			$transactionManager = $applicationServices->getTransactionManager();
			$documentManager = $applicationServices->getDocumentManager();

			$password = (string)$data['password'];
			$mobilePhone = trim((string)$data['mobilePhone']);
			$user = $documentManager->getDocumentInstance($currentUser->getId());

			if ($user instanceof \Rbs\User\Documents\User)
			{
				if ($user->checkPassword($password))
				{
					try
					{
						$transactionManager->begin();
						/* @var \Rbs\Generic\GenericServices $genericServices */
						$genericServices = $event->getServices('genericServices');
						$id = $genericServices->getUserManager()->initMobilePhoneRequest($user, $mobilePhone);
						if ($id)
						{
							$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ChangeMobilePhoneNumber',
								['common' => ['mobilePhone' => $mobilePhone, 'id' => $id]]);
							$event->setResult($result);
						} else {
							$this->setErrorResult($event, 'INVALID_REQUEST_ID',
								$i18nManager->trans('m.rbs.user.front.invalid_request_id', ['ucf']),
								\Zend\Http\Response::STATUS_CODE_409);
						}
						$transactionManager->commit();
					}
					catch (\Exception $e)
					{
						$event->getApplication()->getLogging()->exception($e);
						throw $transactionManager->rollBack($e);
					}
				}
				else
				{
					$this->setErrorResult($event, 'INVALID_PASSWORD',
						$i18nManager->trans('m.rbs.user.front.current_password_not_match', ['ucf']),
						\Zend\Http\Response::STATUS_CODE_409);
				}
			}
			else
			{
				$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.user_not_found', ['ucf']));
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ChangeMobilePhoneNumber
	 * Event params:
	 *  - autenticated User
	 *  - data:
	 *     - tokenValidation
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function validateChangeMobilePhoneRequest(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();

		$currentUser = $event->getAuthenticationManager()->getCurrentUser();
		$data = $event->getParam('data');
		if (is_array($data) && isset($data['tokenValidation']) && $currentUser->authenticated()
		)
		{
			$transactionManager = $applicationServices->getTransactionManager();
			$documentManager = $applicationServices->getDocumentManager();

			$tokenValidation = (string)$data['tokenValidation'];
			$user = $documentManager->getDocumentInstance($currentUser->getId());
			if ($user instanceof \Rbs\User\Documents\User)
			{
				try
				{
					$transactionManager->begin();

					/* @var \Rbs\Generic\GenericServices $genericServices */
					$genericServices = $event->getServices('genericServices');
					$parameters = $genericServices->getUserManager()->validateMobilePhone($user, $tokenValidation);
					if ($parameters)
					{
						$user->setMobilePhone($parameters['mobilePhone']);
						$user->save();
						$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ChangeMobilePhoneNumber',
							['common' => $parameters]);
						$event->setResult($result);
					}
					else
					{
						$this->setErrorResult($event, 'VALIDATION_FAILED',
							$i18nManager->trans('m.rbs.user.front.validation_failed', ['ucf']),
							\Zend\Http\Response::STATUS_CODE_409);
					}
					$transactionManager->commit();
				}
				catch (\Exception $e)
				{
					$event->getApplication()->getLogging()->exception($e);
					throw $transactionManager->rollBack($e);
				}
			}
			else
			{
				$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.user_not_found', ['ucf']));
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request', $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ResetPasswordRequest
	 * Event params:
	 *  - website
	 *  - data:
	 *     - email
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function createResetPasswordRequest(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['email']))
		{
			$email = trim((string)$data['email']);
			$data['websiteId'] = $website->getId();
			$data['LCID'] = $website->getCurrentLCID();

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$result = $genericServices->getUserManager()->createResetPasswordRequest($email, $data);
			if ($result)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ResetPasswordRequest', ['common' => ['email' => $email]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'CreateResetPasswordRequestError', $genericServices->getUserManager()->getLastError(),
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request',  $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ResetPasswordRequest
	 * Event params:
	 *  - website
	 *  - data:
	 *     - token
	 *     - password
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function confirmResetPasswordRequest(\Change\Http\Event $event)
	{
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['token'], $data['password']))
		{
			$token = (string)$data['token'];
			$password = (string)$data['password'];

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$user = $genericServices->getUserManager()->confirmResetPasswordRequest($token, $password);
			if ($user)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ResetPasswordRequest', ['common' => ['id' => $user->getId()]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'ConfirmResetPasswordRequest', $genericServices->getUserManager()->getLastError() ,
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request',  $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/ChangePassword
	 * Event params:
	 *  - website
	 *  - data:
	 *     - currentPassword
	 *     - password
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function changePassword(\Change\Http\Event $event)
	{
		// Data obfuscation for further process (e.g. audit logs)
		$event->setParam('dataObfuscationCallable',
			new \Change\Stdlib\ChainedArrayCallable($event->getParam('dataObfuscationCallable'), function (array $data)
			{
				if (isset($data['data']['password']))
				{
					$data['data']['password'] = 'XXX';
				}
				if (isset($data['data']['currentPassword']))
				{
					$data['data']['currentPassword'] = 'XXX';
				}
				return $data;
			}));
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$website = $event->getParam('website');
		$data = $event->getParam('data');
		if ($website instanceof \Rbs\Website\Documents\Website && is_array($data) && isset($data['currentPassword'], $data['password']))
		{
			$currentPassword = (string)$data['currentPassword'];
			$password = (string)$data['password'];

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$user = $genericServices->getUserManager()->changePassword($currentPassword, $password);
			if ($user)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/ChangePassword', ['common' => ['id' => $user->getId()]]);
				$event->setResult($result);
			}
			else
			{
				$this->setErrorResult($event, 'ChangePassword', $genericServices->getUserManager()->getLastError() ,
					\Zend\Http\Response::STATUS_CODE_409);
			}
		}
		else
		{
			$this->setErrorResult($event, 'Bad Request',  $i18nManager->trans('m.rbs.user.front.bad_request', ['ucf']));
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/Profiles
	 * Event params:
	 *  - website
	 *  - data:
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function getProfiles(\Change\Http\Event $event)
	{
		$currentUser = $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser();
		if ($currentUser->authenticated())
		{
			$event->setParam('detailed', true);
			$context = $event->paramsToArray();
			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$userData = $genericServices->getUserManager()->getUserData($currentUser->getId(), $context);
			if ($userData)
			{
				$result = new \Change\Http\Ajax\V1\ItemResult('Rbs/User/User/Profiles', $userData);
				$event->setResult($result);
			}
		}
	}

	/**
	 * Default actionPath: Rbs/User/User/Profiles
	 * Event params:
	 *  - website
	 *  - data:
	 * @param \Change\Http\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	public function setProfiles(\Change\Http\Event $event)
	{
		$currentUser = $event->getApplicationServices()->getAuthenticationManager()->getCurrentUser();
		if ($currentUser->authenticated())
		{
			$context = $event->paramsToArray();
			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$genericServices->getUserManager()->setUserData($currentUser->getId(), $context);
			$this->getProfiles($event);
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param string $code
	 * @param string$message
	 * @param integer $httpStatus
	 * @return \Change\Http\Ajax\V1\ErrorResult
	 */
	public function setErrorResult(\Change\Http\Event $event, $code, $message, $httpStatus = \Zend\Http\Response::STATUS_CODE_400)
	{
		$errorResult = new \Change\Http\Ajax\V1\ErrorResult($code, $message, $httpStatus);
		$event->setResult($errorResult);
		return $errorResult;
	}


} 