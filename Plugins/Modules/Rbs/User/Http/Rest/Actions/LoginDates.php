<?php
/**
 * Copyright (C) 2017 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Http\Rest\Actions;

use Change\Http\Rest\V1\ArrayResult;

/**
 * @name \Rbs\User\Http\Rest\Actions\LoginDates
 */
class LoginDates
{
	public function execute(\Change\Http\Event $event)
	{
		$userId = $event->getParam('documentId');
		$result = new ArrayResult();

		$documentManager = $event->getApplicationServices()->getDocumentManager();
		/* @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$userManager = $genericServices->getUserManager();

		$user = $documentManager->getDocumentInstance($event->getParam('documentId'));
		if ($user instanceof \Rbs\User\Documents\User)
		{
			$dates = $userManager->getLastLoginDates($userId);
			$result->setArray($dates);
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		}
		else
		{
			$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_404);
		}
		$event->setResult($result);
	}
}