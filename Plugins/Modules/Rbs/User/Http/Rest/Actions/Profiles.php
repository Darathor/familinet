<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Http\Rest\Actions;

use Change\Http\Rest\V1\ArrayResult;
use Rbs\User\Events\AuthenticatedUser;

/**
 * @name \Rbs\User\Http\Rest\Actions\Profiles
 */
class Profiles
{
	public function execute(\Change\Http\Event $event)
	{
		$result = new ArrayResult();
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$pm = $event->getApplicationServices()->getProfileManager();
		$user = $documentManager->getDocumentInstance($event->getParam('documentId'));
		$data = [];
		if ($user instanceof \Rbs\User\Documents\User)
		{
			foreach ($pm->getProfileNames() as $profileName)
			{
				$authenticatedUser = new AuthenticatedUser($user);
				$profile = $pm->loadProfile($authenticatedUser, $profileName);
				if ($profile instanceof \Change\User\ProfileInterface)
				{
					$data[$profileName] = $profile->toRestValues($event);
				}
			}
		}
		$result->setArray($data);
		$result->setHttpStatusCode(\Zend\Http\Response::STATUS_CODE_200);
		$event->setResult($result);
	}
	
	public function update(\Change\Http\Event $event)
	{
		$profiles = $event->getParam('profiles');
		if ($profiles && is_array($profiles)) 
		{
			$pm = $event->getApplicationServices()->getProfileManager();
			$transactionManager = $event->getApplicationServices()->getTransactionManager();
			try
			{
				$transactionManager->begin();
				$user = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($event->getParam('documentId'));
				if ($user instanceof \Rbs\User\Documents\User)
				{
					$authenticatedUser = new AuthenticatedUser($user);
					foreach ($pm->getProfileNames() as $profileName)
					{
						if (isset($profiles[$profileName]) && is_array($profiles[$profileName]))
						{
							$profile = $pm->loadProfile($authenticatedUser, $profileName);
							if ($profile instanceof \Change\User\ProfileInterface
								&& is_callable([$profile, 'setRestValues'])
								&& $profile->{'setRestValues'}($profiles[$profileName]))
							{
								$pm->saveProfile($authenticatedUser, $profile);
							}

						}
					}
				}
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				$event->getApplication()->getLogging()->exception($e);
				throw $transactionManager->rollBack($e);
			}
		}
		$this->execute($event);
	}
}