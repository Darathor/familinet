<?php
/**
 * Copyright (C) 2014 Ready Business System
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Job;

/**
 * @name \Rbs\User\Job\CleanRequestTables
 */
class CleanRequestTables
{
	/**
	 * @param \Change\Job\Event $event
	 * @throws \Exception
	 */
	public function execute(\Change\Job\Event $event)
	{
		$tm = $event->getApplicationServices()->getTransactionManager();

		try
		{
			$tm->begin();

			/* @var $genericServices \Rbs\Generic\GenericServices */
			$genericServices = $event->getServices('genericServices');
			$userManager = $genericServices->getUserManager();
			$userManager->cleanAccountRequestTable();
			$userManager->cleanResetPasswordTable();
			$userManager->cleanMobilePhoneTable();

			$tm->commit();
		}
		catch (\Exception $e)
		{
			throw $tm->rollBack($e);
		}

		//Reschedule the job in 6h
		$now = new \DateTime();
		$event->reported($now->add(new \DateInterval('PT6H')));
	}

	/**
	 * @param \Change\Job\Event $event
	 */
	public function onThrow($event)
	{
		$reportedAt = new \DateTime();
		$reportedAt->add(new \DateInterval('PT6H'));
		$event->reported($reportedAt);

		$job = $event->getJob();
		$event->getApplication()->getLogging()->getLoggerByName('job')->warn('Reschedule on error: ' . $job->getName() . ' ' . $job->getId() . ' at '
			. $reportedAt->format(\DateTime::ATOM));
	}
}