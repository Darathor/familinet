<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Setup;

/**
 * @name \Rbs\User\Setup\Install
 * @ignore
 */
class Install extends \Change\Plugins\InstallBase
{

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		// Patch.
		$configuration->addPersistentEntry('Change/Events/PatchManager/Rbs_User', \Rbs\User\Setup\Patch\Listeners::class);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Db\InterfaceSchemaManager $schemaManager
	 * @throws \RuntimeException
	 */
	public function executeDbSchema($plugin, $schemaManager)
	{
		$schema = new Schema($schemaManager);
		$schema->generate();
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
		$transactionManager = $applicationServices->getTransactionManager();
		$documentManager = $applicationServices->getDocumentManager();
		try
		{
			$transactionManager->begin();

			$groupModel = $applicationServices->getModelManager()->getModelByName('Rbs_User_Group');

			$query = $documentManager->getNewQuery($groupModel);
			if ($query->andPredicates($query->eq('realm', 'Rbs_Admin'))->getCountDocuments() === 0)
			{
				/* @var $group \Rbs\User\Documents\Group */
				$group = $documentManager->getNewDocumentInstanceByModel($groupModel);
				$group->setLabel('Backoffice');
				$group->setRealm('Rbs_Admin');
				$group->setIdentifier('backoffice');
				$group->create();

				$applicationServices->getDocumentCodeManager()->addDocumentCode($group, 'User_BackOffice', 'Rbs_Setup');
			}

			$query = $documentManager->getNewQuery($groupModel);
			if ($query->andPredicates($query->eq('realm', 'web'))->getCountDocuments() === 0)
			{
				/* @var $group2 \Rbs\User\Documents\Group */
				$group2 = $documentManager->getNewDocumentInstanceByModel($groupModel);
				$group2->setLabel('Site Web');
				$group2->setRealm('web');
				$group2->setIdentifier('web');
				$group2->create();
				$applicationServices->getDocumentCodeManager()->addDocumentCode($group2, 'User_Web', 'Rbs_Setup');
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}

		$jobManager = $applicationServices->getJobManager();
		if (!$jobManager->getJobIdsByName('Rbs_User_CleanRequestTables'))
		{
			$jobManager->createNewJob('Rbs_User_CleanRequestTables');
		}

		// Init collection
		$cm = $applicationServices->getCollectionManager();
		if ($cm->getCollection('Rbs_User_Collection_Title') === null)
		{
			$tm = $applicationServices->getTransactionManager();
			try
			{
				$tm->begin();
				/* @var $collection \Rbs\Collection\Documents\Collection */
				$collection = $documentManager->getNewDocumentInstanceByModelName('Rbs_Collection_Collection');

				$item = $collection->newCollectionItem();
				$item->setValue('m.');
				$item->getCurrentLocalization()->setTitle($applicationServices->getI18nManager()->trans('m.rbs.user.setup.mister', ['ucf']));
				$item->setLocked(true);

				$item2 = $collection->newCollectionItem();
				$item2->setValue('mme');
				$item2->getCurrentLocalization()->setTitle($applicationServices->getI18nManager()->trans('m.rbs.user.setup.miss', ['ucf']));
				$item2->setLocked(true);

				$collection->setLabel('User title');
				$collection->setCode('Rbs_User_Collection_Title');
				$collection->setLocked(true);
				$collection->getItems()->add($item);
				$collection->getItems()->add($item2);
				$collection->save();
				$tm->commit();
			}
			catch (\Exception $e)
			{
				throw $tm->rollBack($e);
			}
		}

		$this->cleanupUserProfile($transactionManager, $documentManager);
	}

	/**
	 * @param \Change\Transaction\TransactionManager $transactionManager
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @throws \Exception
	 */
	protected function cleanupUserProfile(\Change\Transaction\TransactionManager $transactionManager,
		\Change\Documents\DocumentManager $documentManager)
	{
		do
		{
			try
			{
				$transactionManager->begin();
				$qb = $documentManager->getNewQuery('Rbs_User_Profile');
				$qb->andPredicates($qb->isNull('user'));
				$profiles = $qb->getDocuments(0, 50)->toArray();
				foreach ($profiles as $profile)
				{
					$profile->delete();
				}
				$transactionManager->commit();
			}
			catch (\Exception $e)
			{
				throw $transactionManager->rollBack($e);
			}
		}
		while ($profiles);
	}
}