<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User\Setup;

/**
 * @name \Rbs\User\Setup\Schema
 * @ignore
 */
class Schema extends \Change\Db\Schema\SchemaDefinition
{
	const ACCOUNT_REQUEST_TABLE = 'rbs_user_account_request';
	const RESET_PASSWORD_TABLE = 'rbs_user_reset_password';
	const AUTO_LOGIN_TABLE = 'rbs_user_auto_login';
	const MOBILE_PHONE_TABLE = 'rbs_user_mobile_phone';
	const LOGIN_DATE_TABLE = 'rbs_user_dat_login_date';

	/**
	 * @var \Change\Db\Schema\TableDefinition[]
	 */
	protected $tables;

	/**
	 * @return \Change\Db\Schema\TableDefinition[]
	 */
	public function getTables()
	{
		if ($this->tables === null)
		{
			$schemaManager = $this->getSchemaManager();

			$this->tables[self::ACCOUNT_REQUEST_TABLE] = $td = $schemaManager->newTableDefinition(self::ACCOUNT_REQUEST_TABLE);
			$requestId = $schemaManager->newIntegerFieldDefinition('request_id')->setNullable(false)->setAutoNumber(true);
			$td->addField($requestId)
				->addField($schemaManager->newVarCharFieldDefinition('email', ['length' => 255])->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('realm', ['length' => 128])->setNullable(false)->setDefaultValue('web'))
				->addField($schemaManager->newVarCharFieldDefinition('token', ['length' => 255])->setNullable(false))
				->addField($schemaManager->newTextFieldDefinition('config_parameters')->setNullable(false))
				->addField($schemaManager->newDateFieldDefinition('request_date')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false)->setDefaultValue(0))
				->addKey($this->newPrimaryKey()->addField($requestId))
				->setOption('AUTONUMBER', 1);

			$this->tables[self::MOBILE_PHONE_TABLE] = $td = $schemaManager->newTableDefinition(self::MOBILE_PHONE_TABLE);
			$requestId = $schemaManager->newIntegerFieldDefinition('request_id')->setNullable(false)->setAutoNumber(true);
			$td->addField($requestId)
				->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false)->setDefaultValue(0))
				->addField($schemaManager->newVarCharFieldDefinition('target_identifier', ['length' => 64])->setNullable(true))
				->addField($schemaManager->newVarCharFieldDefinition('token', ['length' => 64])->setNullable(false))
				->addField($schemaManager->newTextFieldDefinition('config_parameters')->setNullable(false))
				->addField($schemaManager->newDateFieldDefinition('request_date')->setNullable(false))
				->addKey($this->newPrimaryKey()->addField($requestId))
				->setOption('AUTONUMBER', 1);

			$this->tables[self::RESET_PASSWORD_TABLE] =
			$resetPasswordTable = $schemaManager->newTableDefinition(self::RESET_PASSWORD_TABLE);
			$requestId = $schemaManager->newIntegerFieldDefinition('request_id')->setNullable(false)->setAutoNumber(true);
			$resetPasswordTable->addField($requestId)
				->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('token', ['length' => 255])->setNullable(false))
				->addField($schemaManager->newDateFieldDefinition('request_date')->setNullable(false))
				->addKey($this->newPrimaryKey()->addField($requestId))
				->setOption('AUTONUMBER', 1);

			$this->tables[self::AUTO_LOGIN_TABLE] = $autoLoginTable = $schemaManager->newTableDefinition(self::AUTO_LOGIN_TABLE);
			$id = $schemaManager->newIntegerFieldDefinition('id')->setNullable(false)->setAutoNumber(true);
			$autoLoginTable->addField($id)
				->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('token', ['length' => 255])->setNullable(false))
				->addField($schemaManager->newVarCharFieldDefinition('device', ['length' => 255])->setNullable(false))
				->addField($schemaManager->newDateFieldDefinition('validity_date')->setNullable(false))
				->addKey($this->newPrimaryKey()->addField($id))
				->setOption('AUTONUMBER', 1);

			$this->tables[self::LOGIN_DATE_TABLE] = $td = $schemaManager->newTableDefinition(self::LOGIN_DATE_TABLE);
			$td->addField($schemaManager->newIntegerFieldDefinition('user_id')->setNullable(false))
				->addField($schemaManager->newIntegerFieldDefinition('website_id')->setNullable(false)->setDefaultValue('0'))
				->addField($schemaManager->newDateFieldDefinition('login_date')->setNullable(false))
				->addKey($this->newPrimaryKey()->addField($td->getField('user_id'))->addField($td->getField('website_id')));
		}
		return $this->tables;
	}
}
