<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\User;

/**
 * @name \Rbs\User\UserManager
 */
class UserManager implements \Zend\EventManager\EventsCapableInterface
{
	use \Change\Events\EventsCapableTrait;

	public const EVENT_MANAGER_IDENTIFIER = 'Rbs_User_UserManager';

	/**
	 * @var \Change\Db\DbProvider
	 */
	protected $dbProvider;

	/**
	 * @var \Change\Documents\DocumentManager
	 */
	protected $documentManager;

	/**
	 * @return \Change\Db\DbProvider
	 */
	protected function getDbProvider()
	{
		return $this->dbProvider;
	}

	/**
	 * @param \Change\Db\DbProvider $dbProvider
	 * @return $this
	 */
	public function setDbProvider(\Change\Db\DbProvider $dbProvider): self
	{
		$this->dbProvider = $dbProvider;
		return $this;
	}

	/**
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @return $this
	 */
	public function setDocumentManager(\Change\Documents\DocumentManager $documentManager): self
	{
		$this->documentManager = $documentManager;
		return $this;
	}

	/**
	 * @param \Change\Events\EventManager $eventManager
	 */
	protected function attachEvents(\Change\Events\EventManager $eventManager)
	{
		$eventManager->attach('checkAccountRequest',
			function ($event) { $this->onCheckCreateAccountRequest($event); }, 10);

		$eventManager->attach('createAccountRequest',
			function ($event) { $this->onCheckCreateAccountRequest($event); }, 10);
		$eventManager->attach('createAccountRequest',
			function ($event) { $this->onCreateAccountRequest($event); }, 5);

		$eventManager->attach('hasAccountRequest',
			function ($event) { $this->onHasAccountRequest($event); }, 5);

		$eventManager->attach('confirmAccountRequest',
			function ($event) { $this->onCheckConfirmAccountRequest($event); }, 10);
		$eventManager->attach('confirmAccountRequest',
			function ($event) { $this->onConfirmAccountRequest($event); }, 5);

		$eventManager->attach('createResetPasswordRequest',
			function ($event) { $this->onCheckCreateResetPasswordRequest($event); }, 10);
		$eventManager->attach('createResetPasswordRequest',
			function ($event) { $this->onCreateResetPasswordRequest($event); }, 5);

		$eventManager->attach('confirmResetPasswordRequest',
			function ($event) { $this->onCheckConfirmResetPasswordRequest($event); }, 10);
		$eventManager->attach('confirmResetPasswordRequest',
			function ($event) { $this->onConfirmResetPasswordRequest($event); }, 5);

		$eventManager->attach('changePassword',
			function ($event) { $this->onCheckChangePassword($event); }, 10);
		$eventManager->attach('changePassword',
			function ($event) { $this->onChangePassword($event); }, 5);

		$eventManager->attach('definePassword',
			function ($event) { $this->onChangePassword($event); }, 5);

		$eventManager->attach('createChangeEmailRequest',
			function ($event) { $this->onCheckCreateChangeEmailRequest($event); }, 10);
		$eventManager->attach('createChangeEmailRequest',
			function ($event) { $this->onCreateChangeEmailRequest($event); }, 5);

		$eventManager->attach('confirmChangeEmailRequest',
			function ($event) { $this->onCheckConfirmChangeEmailRequest($event); }, 10);
		$eventManager->attach('confirmChangeEmailRequest',
			function ($event) { $this->onConfirmChangeEmailRequest($event); }, 5);

		$eventManager->attach('getUserData',
			function ($event) { $this->onGetUserData($event); }, 5);
		$eventManager->attach('setUserData',
			function ($event) { $this->onSetUserData($event); }, 5);

		$eventManager->attach('initMobilePhoneRequest',
			function ($event) { $this->onInitMobilePhoneRequest($event); }, 5);
		$eventManager->attach('validateMobilePhone',
			function ($event) { $this->onValidateMobilePhone($event); }, 5);

		$eventManager->attach('getLastLoginDates',
			function ($event) { $this->onGetLastLoginDates($event); }, 5);
	}

	/**
	 * @return null|string|string[]
	 */
	protected function getEventManagerIdentifier()
	{
		return static::EVENT_MANAGER_IDENTIFIER;
	}

	/**
	 * @return string[]
	 */
	protected function getListenerAggregateClassNames()
	{
		return $this->getApplication()->getConfiguredListenerClassNames('Rbs/User/Events/UserManager');
	}

	/**
	 * @var string[]
	 */
	protected $errors = [];

	/**
	 * @return string[]
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return string|boolean
	 */
	public function getLastError()
	{
		if ($this->hasErrors())
		{
			return $this->errors[\count($this->errors) - 1];
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function hasErrors()
	{
		return \count($this->errors) !== 0;
	}

	/**
	 * @return $this
	 */
	public function resetErrors()
	{
		$this->errors = [];
		return $this;
	}

	/**
	 * @param string $error
	 * @return $this
	 */
	public function addError($error)
	{
		if (is_string($error) && !\Change\Stdlib\StringUtils::isEmpty($error))
		{
			$this->errors[] = $error;
		}
		return $this;
	}

	/**
	 * @param string $email
	 * @param array $requestParameters
	 * @return int|bool
	 */
	public function createAccountRequest($email, array $requestParameters)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'requestParameters' => $requestParameters]);
		$eventManager->trigger('createAccountRequest', $this, $args);
		if (isset($args['requestId']))
		{
			return $args['requestId'];
		}
		return false;
	}

	/**
	 * @param string $email
	 * @param array $requestParameters
	 * @return int|bool
	 */
	public function checkAccountRequest($email, array $requestParameters)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'requestParameters' => $requestParameters]);
		$eventManager->trigger('checkAccountRequest', $this, $args);
		if (isset($args['requestParameters'], $args['requestParameters']['passwordHash'], $args['requestParameters']['hashMethod']))
		{
			return $args['requestParameters'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * Event input params: email, requestParameters
	 * Event output params: realm, requestParameters
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onCheckCreateAccountRequest($event)
	{
		$email = $event->getParam('email');
		$requestParameters = $event->getParam('requestParameters');

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		if (!is_string($email) || \Change\Stdlib\StringUtils::isEmpty($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_email', ['ucf']));
			return;
		}

		$validator = new \Zend\Validator\EmailAddress();
		if (!$validator->isValid($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_email_invalid', ['ucf'], ['EMAIL' => $email]));
			return;
		}
		$realm = $requestParameters['realm'] ?? null;
		if (!$realm && ($websiteId = $requestParameters['websiteId'] ?? 0))
		{
			$website = $this->documentManager->getDocumentInstance($websiteId);
			if ($website instanceof \Rbs\Website\Documents\Website)
			{
				$realm = $website->getRealm();
			}
		}

		if ($this->userEmailExists($email, $realm))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_user_already_exist', ['ucf'], ['EMAIL' => $email]));
			return;
		}

		if ($this->hasAccountRequest($email, $realm))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_request_already_done', ['ucf'], ['EMAIL' => $email]));
			return;
		}
		$requestParameters['realm'] = $realm;

		if (!isset($requestParameters['passwordHash']) && !isset($requestParameters['hashMethod']))
		{
			if (!isset($requestParameters['password']) || \Change\Stdlib\StringUtils::isEmpty($requestParameters['password']))
			{
				$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_password', ['ucf']));
				return;
			}

			$password = trim((string)$requestParameters['password']);
			unset($requestParameters['password']);
			if (strlen($password) > 50)
			{
				$this->addError($i18nManager->trans('m.rbs.user.front.error_password_exceeds_max_characters', ['ucf']));
				return;
			}

			$encodedPassword = $this->encodeUserPassword($password);
			$requestParameters = array_merge($requestParameters, $encodedPassword);
		}
		$event->setParam('requestParameters', $requestParameters);
		$event->setParam('realm', $realm);
	}

	/**
	 * @param \Change\Events\Event $event
	 * Event input params: email, realm, requestParameters
	 * Event output params: requestId, token
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onCreateAccountRequest($event)
	{
		if ($event->getParam('requestId') || $this->hasErrors())
		{
			return;
		}

		$email = $event->getParam('email');
		$realm = $event->getParam('realm', 'web');
		$parameters = $event->getParam('requestParameters');
		if (!is_array($parameters))
		{
			$parameters = [];
		}

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$website = $this->documentManager->getDocumentInstance($parameters['websiteId'] ?? 0) ?? $parameters['website'];
			if (!($website instanceof \Rbs\Website\Documents\Website))
			{
				$website = null;
			}
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT24H'));
			$token = substr(md5($email . $validRequestDate->getTimestamp()), 0, 10);
			$requestId = $this->insertAccountRequest($email, $realm, $token, $parameters, $validRequestDate);
			$event->setParam('token', $token);
			$event->setParam('requestId', $requestId);

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			if ($genericServices)
			{

				$genericServices->getNotificationManager()->userAccountRequest($email, $token, $website,
					$parameters['link'] ?? null, $parameters['confirmationPage'] ?? false);
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplicationServices()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $email
	 * @param string|string[] $realm
	 * @return bool
	 */
	public function hasAccountRequest($email, $realm)
	{
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'realm' => $realm, 'hasAccount' => false]);
		$eventManager->trigger('hasAccountRequest', $this, $args);
		return $args['hasAccount'] ? true : false;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onHasAccountRequest($event)
	{
		if ($event->getParam('hasAccount') !== false)
		{
			return;
		}

		$email = $event->getParam('email');
		$realm = $event->getParam('realm') ?: 'web';

		$validRequestDate = new \DateTime();
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		if (is_array($realm))
		{
			$restriction = $fb->in('realm', $realm);
		}
		else
		{
			$restriction = $fb->eq('realm', $fb->string($realm));
		}

		$qb->select($fb->column('request_id'));
		$qb->from($fb->table('rbs_user_account_request'));
		$qb->where(
			$fb->logicAnd(
				$fb->eq($fb->column('email'), $fb->parameter('email')), $restriction,
				$fb->gt($fb->column('request_date'), $fb->dateTimeParameter('validRequestDate'))
			)
		);
		$select = $qb->query();
		$select->bindParameter('email', $email);
		$select->bindParameter('validRequestDate', $validRequestDate);
		$id = (int)$select->getFirstResult($select->getRowsConverter()->addIntCol('request_id')->singleColumn('request_id'));
		$event->setParam('hasAccount', $id);
	}

	/**
	 * @param string $token
	 * @param string $email
	 * @return bool|\Rbs\User\Documents\User
	 */
	public function confirmAccountRequest($token, $email)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'token' => $token]);
		$eventManager->trigger('confirmAccountRequest', $this, $args);
		if (isset($args['user']) && $args['user'] instanceof \Rbs\User\Documents\User)
		{
			return $args['user'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onCheckConfirmAccountRequest($event)
	{
		$email = $event->getParam('email');
		$token = $event->getParam('token');
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$parameters = $this->doLoadAccountRequestParameters($token, $email, false);
		if (!$parameters)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_request_expired', ['ucf']));
			return;
		}

		if (!isset($parameters['passwordHash']) || !$parameters['passwordHash'])
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_password_hash', ['ucf']));
			return;
		}

		$event->setParam('requestParameters', $parameters);
		if ($existingUser = $this->findUserByEmailAndRealm($email, $parameters['realm'] ?? null))
		{
			if ($existingUser->getPasswordHash() !== $parameters['passwordHash'])
			{
				$this->addError($i18nManager->trans('m.rbs.user.front.error_user_already_exist', ['ucf'], ['EMAIL' => $email]));
				return;
			}
			$event->setParam('user', $existingUser);
			$event->setParam('duplicateConfirmation', true);
			return;
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onConfirmAccountRequest($event)
	{
		if ($event->getParam('user') || $this->hasErrors() || !is_array($event->getParam('requestParameters')))
		{
			return;
		}

		$email = $event->getParam('email');
		$requestId = $event->getParam('requestId');
		$requestParameters = $event->getParam('requestParameters');

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			/* @var $user \Rbs\User\Documents\User */
			$user = $this->documentManager->getNewDocumentInstanceByModelName('Rbs_User_User');

			$user->setEmail($email);
			$user->setHashMethod($requestParameters['hashMethod']);
			$user->setPasswordHash($requestParameters['passwordHash']);

			$realm = $requestParameters['realm'] ?? 'web';
			$dqb = $this->documentManager->getNewQuery('Rbs_User_Group');
			$dqb->andPredicates($dqb->eq('realm', $realm));
			$groups = $dqb->getDocuments()->toArray();
			if ($groups)
			{
				$userGroups = $user->getGroups();
				foreach ($groups as $g)
				{
					$userGroups->add($g);
				}
			}

			$user->save();
			$event->setParam('user', $user);

			// Delete all requests for this email.
			$this->deleteAccountRequestsByEmail($email);

			$requestParameters['requestId'] = $requestId;

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');

			$website = $this->documentManager->getDocumentInstance($requestParameters['websiteId'] ?? 0);
			if (!($website instanceof \Rbs\Website\Documents\Website))
			{
				$website = null;
			}
			$genericServices->getNotificationManager()->userAccountValid($user, $website);

			// Save profile data
			if (isset($requestParameters['profiles']) && is_array($requestParameters['profiles']))
			{
				$this->setUserData($user, ['data' => ['profileFieldsName' => 'createAccount', 'profiles' => $requestParameters['profiles']]]);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $email
	 * @param array $requestParameters
	 * @return int|bool
	 */
	public function createResetPasswordRequest($email, array $requestParameters)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'requestParameters' => $requestParameters]);
		$eventManager->trigger('createResetPasswordRequest', $this, $args);
		if (isset($args['requestId']))
		{
			return $args['requestId'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onCheckCreateResetPasswordRequest($event)
	{
		$email = $event->getParam('email');
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		if (!is_string($email) || \Change\Stdlib\StringUtils::isEmpty($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_email', ['ucf']));
			return;
		}

		$validator = new \Zend\Validator\EmailAddress();
		if (!$validator->isValid($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_email_invalid', ['ucf'], ['EMAIL' => $email]));
			return;
		}

		$cacheManager = $event->getApplicationServices()->getCacheManager();
		$cacheKey = 'password_reset_' . sha1($email);
		if ($cacheManager->getEntry('user', $cacheKey, ['ttl' => 300]))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_email_already_sent', ['ucf']));
			return;
		}
		$cacheManager->setEntry('user', $cacheKey, true, ['ttl' => 300]);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onCreateResetPasswordRequest(\Change\Events\Event $event)
	{
		if ($event->getParam('requestId') || $this->hasErrors())
		{
			return;
		}

		$email = $event->getParam('email');
		$parameters = $event->getParam('requestParameters');

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		$event->setParam('requestId', true);
		try
		{
			$transactionManager->begin();

			$link = $parameters['link'] ?? null;
			$website = $this->documentManager->getDocumentInstance($parameters['websiteId'] ?? 0);
			if ($website instanceof \Rbs\Website\Documents\Website)
			{
				$realm = $website->getRealm();
				$user = $this->findUserByEmailAndRealm($email, $realm);

				/* @var \Rbs\Generic\GenericServices $genericServices */
				$genericServices = $event->getServices('genericServices');

				if ($user)
				{
					$userId = $user->getId();
					$existingRequest = $this->getResetPasswordRequest($userId);
					$validRequestDate = new \DateTime();
					$validRequestDate->add(new \DateInterval('PT24H'));
					// If no existing request for this user, create one.
					if (!$existingRequest)
					{
						$token = md5($email . $validRequestDate->getTimestamp());

						$requestId = $this->insertResetPasswordRequest($userId, $token, $validRequestDate);
						$event->setParam('requestId', $requestId);

						$parameters['token'] = $token;
						$parameters['requestId'] = $requestId;

						$genericServices->getNotificationManager()->userResetPasswordRequest($user, $token, $website, $link);
					}
					// Else refresh the date and re-send mail.
					else
					{
						$token = $existingRequest['token'];
						$requestId = $existingRequest['request_id'];

						$event->setParam('requestId', $requestId);

						$parameters['token'] = $token;
						$parameters['requestId'] = $requestId;

						$this->updateResetPasswordRequest($requestId);

						$genericServices->getNotificationManager()->userResetPasswordRequest($user, $token, $website, $link);
					}
				}
				else
				{
					$genericServices->getNotificationManager()->suggestAccountCreation($email, $website, $link);
				}
			}
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplicationServices()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $token
	 * @param string $password
	 * @return bool|\Rbs\User\Documents\User
	 */
	public function confirmResetPasswordRequest($token, $password)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['token' => $token, 'password' => $password]);
		$eventManager->trigger('confirmResetPasswordRequest', $this, $args);
		if (isset($args['user']) && $args['user'] instanceof \Rbs\User\Documents\User)
		{
			return $args['user'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onCheckConfirmResetPasswordRequest(\Change\Events\Event $event)
	{
		$token = $event->getParam('token');
		$password = trim((string)$event->getParam('password'));

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();

		if (\Change\Stdlib\StringUtils::isEmpty($password))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_password', ['ucf']));
			return;
		}

		if (strlen($password) > 50)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_password_exceeds_max_characters', ['ucf']));
			return;
		}

		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('user_id'));
		$qb->from($fb->table('rbs_user_reset_password'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('token'), $fb->parameter('token')),
			$fb->gt($fb->column('request_date'), $fb->dateTimeParameter('validityDate'))
		));
		$sq = $qb->query();

		$sq->bindParameter('token', $token);

		// Check the validity of the request by comparing date (delta of 24h after the request).
		$sq->bindParameter('validityDate', new \DateTime());

		$userId = $sq->getFirstResult($sq->getRowsConverter()->addIntCol('user_id')->singleColumn('user_id'));
		if ($userId)
		{
			$event->setParam('userId', $userId);
			$user = $this->documentManager->getDocumentInstance($userId);
			if ($user instanceof \Rbs\User\Documents\User)
			{
				$event->setParam('user', $user);
				return;
			}
		}

		$this->addError($i18nManager->trans('m.rbs.user.front.invalid_token', ['ucf']));
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onConfirmResetPasswordRequest(\Change\Events\Event $event)
	{
		if (!$event->getParam('user') || !$event->getParam('password') || $this->hasErrors())
		{
			return;
		}

		$password = $event->getParam('password');
		$applicationServices = $event->getApplicationServices();

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			/* @var $user \Rbs\User\Documents\User */
			$user = $event->getParam('user');

			$user->setPassword($password);
			$user->save();

			// Delete all token for this user_id
			$qb = $this->getDbProvider()->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();

			$qb->delete($fb->table('rbs_user_reset_password'));
			$qb->where($fb->logicAnd(
				$fb->eq($fb->column('user_id'), $fb->parameter('user_id'))
			));
			$dq = $qb->deleteQuery();
			$dq->bindParameter('user_id', $user->getId());
			$dq->execute();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $currentPassword
	 * @param string $password
	 * @return bool|\Rbs\User\Documents\User
	 */
	public function changePassword($currentPassword, $password)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['currentPassword' => $currentPassword, 'password' => $password]);
		$eventManager->trigger('changePassword', $this, $args);
		if (isset($args['user']) && $args['user'] instanceof \Rbs\User\Documents\User)
		{
			return $args['user'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onCheckChangePassword(\Change\Events\Event $event)
	{
		$currentPassword = trim((string)$event->getParam('currentPassword'));
		$password = trim((string)$event->getParam('password'));

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		if (\Change\Stdlib\StringUtils::isEmpty($currentPassword) || \Change\Stdlib\StringUtils::isEmpty($password))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_password', ['ucf']));
			return;
		}
		if (strlen($password) > 50)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_password_exceeds_max_characters', ['ucf']));
			return;
		}

		$changeUser = $applicationServices->getAuthenticationManager()->getCurrentUser();
		$user = $changeUser->authenticated() ? $this->documentManager->getDocumentInstance($changeUser->getId()) : null;
		if (!($user instanceof \Rbs\User\Documents\User))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.user_not_found', ['ucf']));
			return;
		}

		if (!$user->checkPassword($currentPassword))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.old_password_not_match', ['ucf']));
			return;
		}

		$event->setParam('user', $user);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onChangePassword(\Change\Events\Event $event)
	{
		if (!$event->getParam('user') || !$event->getParam('password') || $this->hasErrors())
		{
			return;
		}

		$password = $event->getParam('password');
		$applicationServices = $event->getApplicationServices();

		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			/* @var $user \Rbs\User\Documents\User */
			$user = $event->getParam('user');

			$user->setPassword($password);
			$user->save();

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @param string $password
	 */
	public function definePassword($user, $password)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['user' => $user, 'password' => $password]);
		$eventManager->trigger('definePassword', $this, $args);
	}

	/**
	 * @param string $password
	 * @param string $email
	 * @param array $data
	 * @return bool|int
	 */
	public function createChangeEmailRequest($password, $email, $data)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['password' => $password, 'email' => $email, 'data' => $data]);
		$eventManager->trigger('createChangeEmailRequest', $this, $args);
		if (isset($args['userId']))
		{
			return (int)$args['userId'];
		}
		return false;
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @param string $email
	 * @param array $data Should contain at least `website`or `websiteId`.
	 */
	public function defineEmail($user, $email, array $data)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$data['userId'] = $user->getId();
		$args = $eventManager->prepareArgs(['user' => $user, 'email' => $email, 'userId' => $user->getId(),
			'requestParameters' => $data, 'realm' => $data['realm'] ?? null]);
		$eventManager->trigger('createChangeEmailRequest', $this, $args);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onCheckCreateChangeEmailRequest(\Change\Events\Event $event)
	{
		if ($event->getParam('user') !== null)
		{
			return;
		}

		$password = trim((string)$event->getParam('password'));
		$email = trim((string)$event->getParam('email'));

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		if (\Change\Stdlib\StringUtils::isEmpty($email))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_empty_email', ['ucf']));
			return;
		}
		if (\strlen($email) > 200)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_password_exceeds_max_characters', ['ucf']));
			return;
		}

		$changeUser = $applicationServices->getAuthenticationManager()->getCurrentUser();
		$user = $changeUser->authenticated() ? $this->documentManager->getDocumentInstance($changeUser->getId()) : null;
		if (!($user instanceof \Rbs\User\Documents\User))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.user_not_found', ['ucf']));
			return;
		}

		if (!$user->checkPassword($password))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.current_password_not_match', ['ucf']));
			return;
		}

		$realms = [];
		foreach ($user->getGroups() as $group)
		{
			if ($realm = $group->getRealm())
			{
				$realms[] = $realm;
			}
		}

		if ($this->userEmailExists($email, $realms))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_user_already_exist', ['ucf'], ['EMAIL' => $email]));
			return;
		}
		if ($this->hasAccountRequest($email, $realms))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_request_already_done', ['ucf'], ['EMAIL' => $email]));
			return;
		}

		$event->setParam('userId', $user->getId());
		$requestParameters = $event->getParam('data');
		$requestParameters['userId'] = $user->getId();
		$requestParameters['realms'] = $realms;
		$event->setParam('requestParameters', $requestParameters);
	}

	/**
	 * @param \Change\Events\Event $event
	 * Event input params: email, realm, userId, requestParameters
	 * Event output params: requestId, token
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onCreateChangeEmailRequest(\Change\Events\Event $event)
	{
		if ($event->getParam('requestId') || $this->hasErrors())
		{
			return;
		}

		$email = $event->getParam('email');
		$realm = $event->getParam('realm', 'web');
		$userId = $event->getParam('userId');
		$parameters = $event->getParam('requestParameters');
		if (!is_array($parameters))
		{
			$parameters = [];
		}

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT24H'));
			$token = substr(md5($email . $validRequestDate->getTimestamp()), 0, 10);
			$requestId = $this->insertChangeEmailRequest($email, $realm, $token, $userId, $parameters, $validRequestDate);
			$event->setParam('token', $token);
			$event->setParam('requestId', $requestId);

			$user = $this->documentManager->getDocumentInstance($userId);
			if ($user instanceof \Rbs\User\Documents\User)
			{
				$website = $this->documentManager->getDocumentInstance($parameters['websiteId'] ?? 0);
				if (!($website instanceof \Rbs\Website\Documents\Website))
				{
					$website = null;
				}
				/* @var \Rbs\Generic\GenericServices $genericServices */
				$genericServices = $event->getServices('genericServices');
				$genericServices->getNotificationManager()->userChangeEmailRequest($user, $email, $token, $website,
					$parameters['confirmationUrl'] ?? null);
			}

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$event->getApplicationServices()->getLogging()->exception($e);
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param \Rbs\User\Documents\User $user
	 * @return \Rbs\Website\Documents\Website[]
	 */
	public function getAuthorizedWebsites(\Rbs\User\Documents\User $user)
	{
		$qb = $this->documentManager->getNewQuery('Rbs_Website_Website');
		$grpQb = $qb->getPropertyModelBuilder('realm', 'Rbs_User_Group', 'realm');
		$userQb = $grpQb->getModelBuilder('Rbs_User_User', 'groups');
		$userQb->andPredicates($userQb->eq('id', $user->getId()));
		/** @noinspection PhpIncompatibleReturnTypeInspection */
		return $qb->getDocuments()->toArray();
	}

	/**
	 * @param string $token
	 * @param string $email
	 * @return bool|\Rbs\User\Documents\User
	 */
	public function confirmChangeEmailRequest($token, $email)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['email' => $email, 'token' => $token]);
		$eventManager->trigger('confirmChangeEmailRequest', $this, $args);
		if (isset($args['user']) && $args['user'] instanceof \Rbs\User\Documents\User)
		{
			return $args['user'];
		}
		return false;
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Exception
	 */
	protected function onCheckConfirmChangeEmailRequest(\Change\Events\Event $event)
	{
		$email = $event->getParam('email');
		$token = $event->getParam('token');
		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$parameters = $this->doLoadAccountRequestParameters($token, $email, true);
		if (!$parameters)
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_request_expired', ['ucf']));
			return;
		}
		$event->setParam('requestParameters', $parameters);

		$userId = $parameters['userId'];

		$applicationServices = $event->getApplicationServices();
		$i18nManager = $applicationServices->getI18nManager();
		$documentManager = $this->documentManager;

		$user = $documentManager->getDocumentInstance($userId);
		if (!($user instanceof \Rbs\User\Documents\User))
		{
			$this->addError($i18nManager->trans('m.rbs.user.front.error_unknown_user_id', ['ucf'], ['USER_ID' => $userId]));
		}

		foreach ($user->getGroups() as $group)
		{
			if (($realm = $group->getRealm()) && ($existingUser = $this->findUserByEmailAndRealm($email, $realm)))
			{
				if ($existingUser->getId() !== $userId)
				{
					$this->addError($i18nManager->trans('m.rbs.user.front.error_email_already_used', ['ucf'], ['EMAIL' => $email]));
					return;
				}
				$event->setParam('user', $existingUser);
				$event->setParam('duplicateConfirmation', true);
				return;
			}
		}

		$event->setParam('user', $user);
	}

	/**
	 * @param \Change\Events\Event $event
	 * @throws \Change\Transaction\RollbackException
	 * @throws \Exception
	 */
	protected function onConfirmChangeEmailRequest(\Change\Events\Event $event)
	{
		$user = $event->getParam('user');
		if (!$user instanceof \Rbs\User\Documents\User)
		{
			return;
		}
		$requestParameters = $event->getParam('requestParameters');
		if (!is_array($requestParameters) || $event->getParam('duplicateConfirmation') || $this->hasErrors())
		{
			return;
		}

		$email = $event->getParam('email');
		$requestId = $event->getParam('requestId');

		$applicationServices = $event->getApplicationServices();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();

			$user->setEmail($email);
			$user->save();
			$event->setParam('user', $user);

			// Delete all requests for this email.
			$this->deleteAccountRequestsByEmail($email);
			$requestParameters['requestId'] = $requestId;

			/* @var \Rbs\Generic\GenericServices $genericServices */
			$genericServices = $event->getServices('genericServices');
			$website = $this->documentManager->getDocumentInstance($requestParameters['websiteId'] ?? 0);
			if (!($website instanceof \Rbs\Website\Documents\Website))
			{
				$website = null;
			}
			$genericServices->getNotificationManager()->userMailChanged($user, $website);

			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			throw $transactionManager->rollBack($e);
		}
	}

	/**
	 * @param string $email
	 * @param string|string[] $realm
	 * @return bool
	 */
	public function userEmailExists($email, $realm)
	{
		$dqb = $this->documentManager->getNewQuery('Rbs_User_User');
		if ($realm)
		{
			$groupBuilder = $dqb->getPropertyBuilder('groups');
			if (is_array($realm))
			{
				$dqb->andPredicates($dqb->eq('email', $email), $groupBuilder->in('realm', $realm));
			}
			else
			{
				$dqb->andPredicates($dqb->eq('email', $email), $groupBuilder->eq('realm', $realm));
			}
		}
		else
		{
			$dqb->andPredicates($dqb->eq('email', $email));
		}
		$count = $dqb->getCountDocuments();
		return ($count !== 0);
	}

	/**
	 * @param string $email
	 * @param string $realm
	 * @return \Rbs\User\Documents\User|null
	 */
	public function findUserByEmailAndRealm($email, $realm)
	{
		$dqb = $this->documentManager->getNewQuery('Rbs_User_User');
		if ($realm)
		{
			$groupBuilder = $dqb->getPropertyBuilder('groups');
			$dqb->andPredicates($dqb->eq('email', $email), $groupBuilder->eq('realm', $realm));
		}
		else
		{
			$dqb->andPredicates($dqb->eq('email', $email));
		}
		$user = $dqb->getFirstDocument();
		return $user instanceof \Rbs\User\Documents\User ? $user : null;
	}

	/**
	 * @param string $password
	 * @return array
	 */
	public function encodeUserPassword($password)
	{
		// Create an unsaved user to get the password hash and the hash method.
		/* @var $user \Rbs\User\Documents\User */
		$user = $this->documentManager->getNewDocumentInstanceByModelName('Rbs_User_User');
		$user->setPassword($password);
		$parameters = [
			'passwordHash' => $user->getPasswordHash(),
			'hashMethod' => $user->getHashMethod()
		];
		return $parameters;
	}

	/**
	 * @param string $email
	 * @param string $realm
	 * @param string $token
	 * @param array $parameters
	 * @param \DateTime|null $validRequestDate
	 * @return int
	 */
	protected function insertAccountRequest($email, $realm, $token, array $parameters = [], \DateTime $validRequestDate = null)
	{
		return $this->doInsertAccountRequest($email, $realm, $token, 0, $parameters, $validRequestDate);
	}

	/**
	 * @param string $email
	 * @param string $realm
	 * @param string $token
	 * @param int $userId
	 * @param array $parameters
	 * @param \DateTime $validRequestDate
	 * @return int
	 */
	protected function insertChangeEmailRequest($email, $realm, $token, $userId, array $parameters = [], \DateTime $validRequestDate = null)
	{
		return $this->doInsertAccountRequest($email, $realm, $token, $userId, $parameters, $validRequestDate);
	}

	/**
	 * @param string $email
	 * @param string $realm
	 * @param string $token
	 * @param int $userId
	 * @param array $parameters
	 * @param \DateTime $validRequestDate
	 * @return int
	 */
	protected function doInsertAccountRequest($email, $realm, $token, $userId, $parameters, \DateTime $validRequestDate = null)
	{
		$dbProvider = $this->getDbProvider();
		$qb = $dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();
		if (!$validRequestDate)
		{
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT24H'));
		}
		$qb->insert($fb->table('rbs_user_account_request'));
		$qb->addColumns(
			$fb->column('email'),
			$fb->column('realm'),
			$fb->column('token'),
			$fb->column('user_id'),
			$fb->column('config_parameters'),
			$fb->column('request_date')
		);
		$qb->addValues(
			$fb->parameter('email'),
			$fb->parameter('realm'),
			$fb->parameter('token'),
			$fb->parameter('userId'),
			$fb->parameter('configParameters'),
			$fb->dateTimeParameter('requestDate')
		);
		$iq = $qb->insertQuery();

		$iq->bindParameter('email', $email);
		$iq->bindParameter('realm', $realm);
		$iq->bindParameter('token', $token);
		$iq->bindParameter('userId', $userId);
		$iq->bindParameter('configParameters', \json_encode($parameters, \JSON_THROW_ON_ERROR));
		$iq->bindParameter('requestDate', $validRequestDate);
		$iq->execute();
		return (int)$dbProvider->getLastInsertId('rbs_user_account_request');
	}

	/**
	 * @param string $token
	 * @param string $email
	 * @param bool $withUserId
	 * @return array|null
	 */
	protected function doLoadAccountRequestParameters($token, $email, $withUserId)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('config_parameters'));
		$qb->from($fb->table('rbs_user_account_request'));
		$qb->where($fb->logicAnd(
			($withUserId ? $fb->neq($fb->column('user_id'), 0) : $fb->eq($fb->column('user_id'), 0)),
			$fb->eq($fb->column('token'), $fb->parameter('token')),
			$fb->eq($fb->column('email'), $fb->parameter('email')),
			$fb->gt($fb->column('request_date'), $fb->dateTimeParameter('validityDate'))
		));
		$sq = $qb->query();

		$sq->bindParameter('token', $token);
		$sq->bindParameter('email', $email);
		$sq->bindParameter('validityDate', new \DateTime());
		$configParameters =
			$sq->getFirstResult($sq->getRowsConverter()->addTxtCol('config_parameters')->singleColumn('config_parameters'));
		if ($configParameters)
		{
			return \json_decode($configParameters, true, 512, \JSON_THROW_ON_ERROR);
		}
		return null;
	}

	/**
	 * @param string $email
	 */
	protected function deleteAccountRequestsByEmail($email)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->delete($fb->table('rbs_user_account_request'));
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('email'), $fb->parameter('email'))
		));

		$dq = $qb->deleteQuery();
		$dq->bindParameter('email', $email);
		$dq->execute();
	}

	/**
	 * @param int $userId
	 * @param string $token
	 * @param \DateTime|null $validRequestDate
	 * @return int
	 */
	protected function insertResetPasswordRequest($userId, $token, \DateTime $validRequestDate = null)
	{
		if (!$validRequestDate)
		{
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT24H'));
		}

		$dbProvider = $this->getDbProvider();
		$qb = $dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->insert($fb->table('rbs_user_reset_password'));
		$qb->addColumns($fb->column('user_id'), $fb->column('token'), $fb->column('request_date'));
		$qb->addValues($fb->parameter('user_id'), $fb->parameter('token'), $fb->dateTimeParameter('requestDate'));
		$iq = $qb->insertQuery();

		$iq->bindParameter('user_id', $userId);
		$iq->bindParameter('token', $token);
		$iq->bindParameter('requestDate', $validRequestDate);
		$iq->execute();
		return (int)$dbProvider->getLastInsertId('rbs_user_account_request');
	}

	/**
	 * @param int $requestId
	 * @param \DateTime|null $validRequestDate
	 */
	protected function updateResetPasswordRequest($requestId, \DateTime $validRequestDate = null)
	{
		if (!$validRequestDate)
		{
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT24H'));
		}

		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->update($fb->table('rbs_user_reset_password'));
		$qb->assign($fb->column('request_date'), $fb->dateTimeParameter('request_date'));
		$qb->where($fb->eq($fb->column('request_id'), $fb->integerParameter('request_id')));
		$uq = $qb->updateQuery();

		$uq->bindParameter('request_id', $requestId);
		$uq->bindParameter('request_date', $validRequestDate);
		$uq->execute();
	}

	/**
	 * @param int $userId
	 * @return array
	 */
	protected function getResetPasswordRequest($userId)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select($fb->column('request_id'), $fb->column('user_id'), $fb->column('token'), $fb->column('request_date'));
		$qb->from($fb->table('rbs_user_reset_password'));
		$qb->where($fb->eq($fb->column('user_id'), $fb->integerParameter('user_id')));
		$sq = $qb->query();
		$sq->bindParameter('user_id', $userId);
		return $sq->getFirstResult($sq->getRowsConverter()->addIntCol('request_id', 'user_id')->addStrCol('token')->addDtCol('request_date'));
	}

	/**
	 * @param int|\Rbs\User\Documents\User $user
	 * @param array $context
	 * @return array|mixed
	 */
	public function getUserData($user, array $context)
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['user' => $user, 'context' => $context]);
		$em->trigger('getUserData', $this, $eventArgs);
		if (isset($eventArgs['userData']))
		{
			$userData = $eventArgs['userData'];
			if (is_object($userData))
			{
				$callable = [$userData, 'toArray'];
				if (is_callable($callable))
				{
					$userData = $callable();
				}
			}
			if (is_array($userData))
			{
				return $userData;
			}
		}
		return [];
	}

	/**
	 * Input params: user, context
	 * Output param: userData
	 * @param \Change\Events\Event $event
	 */
	protected function onGetUserData(\Change\Events\Event $event)
	{
		if (!$event->getParam('userData'))
		{
			$userDataComposer = new \Rbs\User\UserDataComposer($event);
			$event->setParam('userData', $userDataComposer->toArray());
		}
	}

	/**
	 * @param int|\Rbs\User\Documents\User $user
	 * @param array $context
	 */
	public function setUserData($user, array $context)
	{
		$em = $this->getEventManager();
		$eventArgs = $em->prepareArgs(['user' => $user, 'context' => $context]);
		$em->trigger('setUserData', $this, $eventArgs);
	}

	/**
	 * Input params: user, context
	 * @param \Change\Events\Event $event
	 */
	protected function onSetUserData(\Change\Events\Event $event)
	{
		$context = $event->getParam('context', []) + ['data' => [], 'useFullNameAsDefaultPseudonym' => true];
		$userData = $context['data'];

		$user = $event->getParam('user');
		if (is_numeric($user))
		{
			$user = $this->documentManager->getDocumentInstance($user);
		}
		if ($user instanceof \Rbs\User\Documents\User)
		{
			$user = new \Rbs\User\Events\AuthenticatedUser($user);
		}
		if ($user instanceof \Change\User\UserInterface)
		{
			$profileManager = $event->getApplicationServices()->getProfileManager();
			$fields = $profileManager->getProfileFields($userData['profileFieldsName'] ?? 'userAccount');
			$profiles = [];
			foreach ($fields as $field)
			{
				$fieldValue = $userData['profiles'][$field->getProfileName()][$field->getFieldName()] ?? null;
				if ($fieldValue === null || $field->getReadonly())
				{
					continue;
				}

				if ($field->getProfileName() === 'Rbs_User')
				{
					switch ($fieldValue)
					{
						case 'titleCode' :
						case 'firstName' :
						case 'lastName' :
						case 'phone' :
							$fieldValue = trim((string)$fieldValue);
							break;
						case 'birthDate' :
							$fieldValue = trim((string)$fieldValue['birthDate']);
							if ($fieldValue)
							{
								$fieldValue = (new \DateTime($fieldValue))->format('Y-m-d');
							}
							else
							{
								$fieldValue = null;
							}
							break;
					}
				}

				if ($field->getProfileName() === 'Rbs_Website')
				{
					switch ($fieldValue)
					{
						case 'pseudonym' :
							$fieldValue = trim((string)$fieldValue);
							break;
					}
				}

				$profiles[$field->getProfileName()][$field->getFieldName()] = $fieldValue;
			}

			foreach ($profiles as $profileName => $profile)
			{
				$currentProfile = $profileManager->loadProfile($user, $profileName);
				foreach ($profile as $field => $value)
				{
					$currentProfile->setPropertyValue($field, $value);
				}
				$profileManager->saveProfile($user, $currentProfile);
			}

			// Pseudonym specific case
			$webProfile = $profileManager->loadProfile($user, 'Rbs_Website');
			if ($context['useFullNameAsDefaultPseudonym'] && !$webProfile->getPropertyValue('pseudonym'))
			{
				$userProfile = $profileManager->loadProfile($user, 'Rbs_User');
				$value = $userProfile->getPropertyValue('fullName');
				$webProfile->setPropertyValue('pseudonym', $value);
				$profileManager->saveProfile($user, $webProfile);
			}
		}
	}

	/**
	 * @param int $userId
	 * @param string $targetIdentifier
	 * @param string $token
	 * @param array|null $configParameters
	 * @param \DateTime|null $validRequestDate
	 * @return int
	 */
	protected function insertMobilePhoneRequest($userId, $targetIdentifier, $token, $configParameters, \DateTime $validRequestDate = null)
	{
		if (!$validRequestDate)
		{
			$validRequestDate = new \DateTime();
			$validRequestDate->add(new \DateInterval('PT20M'));
		}

		$dbProvider = $this->getDbProvider();
		$qb = $dbProvider->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->insert($fb->table('rbs_user_mobile_phone'));
		$qb->addColumns($fb->column('user_id'), $fb->column('target_identifier'), $fb->column('token'),
			$fb->column('config_parameters'), $fb->column('request_date'));
		$qb->addValues($fb->parameter('user_id'), $fb->parameter('targetIdentifier'), $fb->parameter('token'),
			$fb->lobParameter('config_parameters'), $fb->dateTimeParameter('requestDate'));
		$iq = $qb->insertQuery();

		$iq->bindParameter('user_id', $userId);
		$iq->bindParameter('targetIdentifier', $targetIdentifier);
		$iq->bindParameter('token', $token);
		$iq->bindParameter('config_parameters', $configParameters ? \json_encode($configParameters, \JSON_THROW_ON_ERROR) : null);
		$iq->bindParameter('requestDate', $validRequestDate);
		$iq->execute();
		return (int)$dbProvider->getLastInsertId('rbs_user_mobile_phone');
	}

	/**
	 * @param string $token
	 * @param int $userId
	 * @param string $targetIdentifier
	 * @return array|null
	 */
	protected function selectMobilePhoneRequest($token, $userId, $targetIdentifier)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('request_id'), $fb->column('config_parameters'));
		$qb->from($fb->table('rbs_user_mobile_phone'));

		if ($userId)
		{
			$r = $fb->eq($fb->column('user_id'), $fb->integerParameter('accessor'));
		}
		else
		{
			$r = $fb->eq($fb->column('target_identifier'), $fb->parameter('accessor'));
		}
		$qb->where($fb->logicAnd(
			$fb->eq($fb->column('token'), $fb->parameter('token')),
			$fb->gt($fb->column('request_date'), $fb->dateTimeParameter('validityDate')),
			$r
		));
		$qb->orderDesc($fb->column('request_id'));
		$sq = $qb->query();

		$sq->bindParameter('token', $token);
		$sq->bindParameter('accessor', $userId ?: $targetIdentifier);
		$sq->bindParameter('validityDate', new \DateTime());
		$data = $sq->getFirstResult($sq->getRowsConverter()->addTxtCol('config_parameters')->addIntCol('request_id'));

		if ($data)
		{
			$configParameters = $data['config_parameters'] ? \json_decode($data['config_parameters'], true, 512, \JSON_THROW_ON_ERROR) : [];
			$configParameters['requestId'] = $data['request_id'];
			return $configParameters;
		}
		return null;
	}

	/**
	 * @param int $requestId
	 * @return int
	 */
	protected function deleteMobilePhoneRequest($requestId)
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->delete($fb->table('rbs_user_mobile_phone'));
		$qb->where($fb->eq($fb->column('request_id'), $fb->integerParameter('requestId')));
		$dq = $qb->deleteQuery();

		$dq->bindParameter('requestId', $requestId);
		$dq->execute();
		return (int)$dq->execute();
	}

	/**
	 * Need started Transaction
	 * @param int|string|\Rbs\User\Documents\User|\Change\User\UserInterface $accessor
	 * @param string $mobilePhone
	 * @return int Request Id
	 */
	public function initMobilePhoneRequest($accessor, $mobilePhone)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['accessor' => $accessor, 'mobilePhone' => $mobilePhone, 'requestId' => null]);
		$eventManager->trigger('initMobilePhoneRequest', $this, $args);
		return (int)$args['requestId'];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onInitMobilePhoneRequest(\Change\Events\Event $event)
	{
		if ($event->getParam('requestId') !== null)
		{
			return;
		}

		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$accessor = $event->getParam('accessor');
		$userId = 0;
		$targetIdentifier = null;
		if (\is_int($accessor))
		{
			$userId = $accessor;
		}
		elseif ($accessor instanceof \Rbs\User\Documents\User)
		{
			$userId = $accessor->getId();
		}
		elseif ($accessor instanceof \Change\User\UserInterface && $accessor->authenticated())
		{
			$userId = $accessor->getId();
		}
		elseif (\is_string($accessor))
		{
			$targetIdentifier = $accessor;
		}

		if (!$userId && !$targetIdentifier)
		{
			$event->setParam('requestId', 0);
			$this->addError($i18nManager->trans('m.rbs.user.front.invalid_accessor_identifier'));
			return;
		}
		$mobilePhone = $event->getParam('mobilePhone');

		$requests = $this->getMobilePhoneRequests($userId, $targetIdentifier);
		if (\count($requests) >= 3)
		{
			$reqId = 0;
			foreach ($requests as $req)
			{
				if ($req['mobilePhone'] === $mobilePhone)
				{
					$reqId = $req['id'];
					break;
				}
			}
			$event->setParam('requestId', $reqId);
			$this->addError($i18nManager->trans('m.rbs.user.front.too_many_request'));
			return;
		}

		$token = null;
		foreach ($requests as $req)
		{
			if ($req['mobilePhone'] === $mobilePhone)
			{
				$token = $req['token'];
			}
		}
		if (!$token)
		{
			$token = (string)\random_int(10000, 99999);
		}
		$token = $event->getParam('token', $token);

		$configParameters = ['mobilePhone' => $mobilePhone];

		/** @var \Rbs\Generic\GenericServices $genericServices */
		$genericServices = $event->getServices('genericServices');
		$result = $genericServices->getNotificationManager()->userValidMobilePhoneRequest($mobilePhone, $token);
		if ($result instanceof \Change\Job\JobInterface)
		{
			$configParameters['jobId'] = $result->getId();
			$requestId = $this->insertMobilePhoneRequest($userId, $targetIdentifier, $token, $configParameters);
			$event->setParam('requestId', $requestId);
		}
	}

	/**
	 * @param int $userId
	 * @param int $targetIdentifier
	 * @return array
	 */
	protected function getMobilePhoneRequests($userId, $targetIdentifier)
	{
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();
		$qb->select($fb->column('request_id'), $fb->column('token'), $fb->column('config_parameters'));
		$qb->from($fb->table('rbs_user_mobile_phone'));

		if ($userId)
		{
			$r = $fb->eq($fb->column('user_id'), $fb->integerParameter('accessor'));
		}
		else
		{
			$r = $fb->eq($fb->column('target_identifier'), $fb->parameter('accessor'));
		}
		$qb->where($fb->logicAnd(
			$fb->gt($fb->column('request_date'), $fb->dateTimeParameter('validityDate')),
			$r
		));
		$sq = $qb->query();
		$sq->bindParameter('accessor', $userId ?: $targetIdentifier);
		$sq->bindParameter('validityDate', new \DateTime());
		$data = $sq->getResults($sq->getRowsConverter()->addIntCol('request_id')->addStrCol('token')->addTxtCol('config_parameters'));

		$requests = [];
		foreach ($data as $item)
		{
			$conf = \json_decode($item['config_parameters'], true, 512, \JSON_THROW_ON_ERROR);
			$requests[] = [
				'id' => $item['request_id'],
				'token' => $item['token'],
				'mobilePhone' => $conf['mobilePhone']
			];
		}
		return $requests;
	}

	/**
	 * @param int $userId
	 * @return array Login dates
	 */
	public function getLastLoginDates($userId)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['userId' => $userId, 'loginDates' => null]);
		$eventManager->trigger('getLastLoginDates', $this, $args);
		return $args['loginDates'];
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onGetLastLoginDates(\Change\Events\Event $event)
	{
		if ($event->getParam('loginDates') !== null)
		{
			return;
		}

		$userId = $event->getParam('userId', 0);
		$qb = $this->getDbProvider()->getNewQueryBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->select($fb->alias($fb->column('website_id', 'ld'), 'website_id'),
			$fb->alias($fb->column('login_date', 'ld'), 'login_date'),
			$fb->alias($fb->column('label', 'ws'), 'label'));
		$qb->from($fb->alias($fb->table('rbs_user_dat_login_date'), 'ld'));
		$qb->innerJoin($fb->alias($fb->table('rbs_website_doc_section'), 'ws'),

			$fb->eq($fb->column('website_id', 'ld'), $fb->getDocumentColumn('document_id', 'ws'))
		);
		$qb->where(
			$fb->eq($fb->column('user_id'), $fb->parameter('user_id'))
		);
		$sq = $qb->query();
		$sq->bindParameter('user_id', $userId);

		$loginDates = $sq->getResults($sq->getRowsConverter()->addIntCol('website_id')->addStrCol('label')->addDtCol('login_date'));

		$dates = [];
		foreach ($loginDates as $date)
		{
			/** @var \DateTime $loginDate */
			$loginDate = $date['login_date'];
			$dates[] = [
				'websiteId' => $date['website_id'],
				'websiteName' => $date['label'],
				'loginDate' => $loginDate->format(\DateTime::ATOM)
			];
		}

		$event->setParam('loginDates', $dates);
	}

	/**
	 * Need started Transaction
	 * @param int|string|\Rbs\User\Documents\User|\Change\User\UserInterface $accessor
	 * @param string $token
	 * @return array|false configuration parameters
	 */
	public function validateMobilePhone($accessor, $token)
	{
		$this->resetErrors();
		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['accessor' => $accessor, 'token' => $token, 'configParameters' => null]);
		$eventManager->trigger('validateMobilePhone', $this, $args);
		return is_array($args['configParameters']) ? $args['configParameters'] : false;
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onValidateMobilePhone(\Change\Events\Event $event)
	{
		if ($event->getParam('configParameters') !== null)
		{
			return;
		}

		$accessor = $event->getParam('accessor');
		$userId = 0;
		$targetIdentifier = null;
		if (\is_int($accessor))
		{
			$userId = $accessor;
		}
		elseif ($accessor instanceof \Rbs\User\Documents\User)
		{
			$userId = $accessor->getId();
		}
		elseif ($accessor instanceof \Change\User\UserInterface && $accessor->authenticated())
		{
			$userId = $accessor->getId();
		}
		elseif (\is_string($accessor))
		{
			$targetIdentifier = $accessor;
		}
		if (!$userId && !$targetIdentifier)
		{
			$this->addError('Invalid accessor identifier');
			$event->setParam('configParameters', false);
			return;
		}

		$token = $event->getParam('token');
		if (!$token)
		{
			$this->addError('Empty token');
			$event->setParam('configParameters', false);
			return;
		}
		$configParameters = $this->selectMobilePhoneRequest(strtoupper($token), $userId, $targetIdentifier);
		if (\is_array($configParameters))
		{
			$this->deleteMobilePhoneRequest($configParameters['requestId']);
			$event->setParam('configParameters', $configParameters);
		}
		else
		{
			$event->setParam('configParameters', false);
		}
	}

	/**
	 * Purge old account requests.
	 * @return int
	 */
	public function cleanAccountRequestTable()
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->delete($fb->table('rbs_user_account_request'));
		$qb->where($fb->lt($fb->column('request_date'), $fb->dateTimeParameter('now')));
		$iq = $qb->deleteQuery();
		$iq->bindParameter('now', new \DateTime());
		return $iq->execute();
	}

	/**
	 * Purge old reset password requests.
	 * @return int
	 */
	public function cleanResetPasswordTable()
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->delete($fb->table('rbs_user_reset_password'));
		$qb->where($fb->lt($fb->column('request_date'), $fb->dateTimeParameter('now')));
		$iq = $qb->deleteQuery();
		$iq->bindParameter('now', new \DateTime());
		return $iq->execute();
	}

	/**
	 * Purge old mobile phone requests.
	 * @return int
	 */
	public function cleanMobilePhoneTable()
	{
		$qb = $this->getDbProvider()->getNewStatementBuilder();
		$fb = $qb->getFragmentBuilder();

		$qb->delete($fb->table('rbs_user_mobile_phone'));
		$qb->where($fb->lt($fb->column('request_date'), $fb->dateTimeParameter('now')));
		$iq = $qb->deleteQuery();
		$iq->bindParameter('now', new \DateTime());
		return $iq->execute();
	}
}