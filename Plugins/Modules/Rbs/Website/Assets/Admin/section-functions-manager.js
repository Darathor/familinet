(function() {
	'use strict';

	var QUERY_LIMIT = 500; // 640K ought to be enough for anybody :)

	var app = angular.module('RbsChange');

	app.directive('rbsSectionFunctionsManager', ['$q', 'RbsChange.REST', 'RbsChange.Query', 'RbsChange.Navigation',
		SectionFunctionsManagerDirective]);

	function SectionFunctionsManagerDirective($q, REST, Query, Navigation) {
		return {
			restrict: 'A',
			templateUrl: 'Rbs/Website/section-functions-manager.twig',
			replace: false,
			scope: {
				section: '='
			},

			link: function linkFn(scope) {
				// Set the website.
				scope.$watch('section', function(section) {
					if (section) {
						scope.website = (section.hasOwnProperty('website')) ? section.website : section;
					}
				}, true);

				/**
				 * Loads the Functions implemented in the given Section.
				 *
				 * This method populates the following objects in the scope:
				 * - sectionPageFunctionList : array of all the implemented Functions in the given Section
				 *
				 * @param section
				 * @param sortColumn
				 * @param sortDesc
				 */
				function loadSectionPageFunctions(section, sortColumn, sortDesc) {
					var query = Query.simpleQuery('Rbs_Website_SectionPageFunction', 'section', section.id);
					var p;

					query.limit = QUERY_LIMIT;
					query.offset = 0;
					if (sortColumn) {
						query.order = [{ property: sortColumn, order: sortDesc ? 'desc' : 'asc' }];
					}
					p = REST.query(query, { column: ['functionCode', 'page'] });
					p.then(function(result) {
						scope.sectionPageFunctionList = result.resources;
						angular.forEach(scope.sectionPageFunctionList, function(spf) {
							spf.label = scope.getFunctionLabel(spf);
						});
					});

					return $q.all([p, loadInheritedFunctions(section)]);
				}

				/**
				 * Loads the inherited Functions for the given Section (the Functions implemented on the ancestors
				 * of the Section).
				 *
				 * This method populates the following objects in the scope:
				 * - inheritedFunctions : array of all the inherited Functions
				 *
				 * @param section
				 */
				function loadInheritedFunctions(section) {
					var p = REST.call(REST.getBaseUrl('Rbs/Website/InheritedFunctions'), { 'section': section.id });
					p.then(function(functions) {
						scope.inheritedFunctions = functions;
					});
					return p;
				}

				/**
				 * Loads the pages that can be used to implement a function for the given Section.
				 *
				 * These pages are:
				 * - the child pages of the current section
				 * - all the FunctionalPages attached to the current website
				 *
				 * This method populates the following objects in the scope:
				 * - staticPages     : array of StaticPage documents
				 * - functionalPages : array of FunctionalPage documents
				 *
				 * @param section Rbs_Website_Section document.
				 *
				 * @returns Promise
				 */
				function loadPages(section) {
					var promises = [];
					var query;
					var p;
					var websiteId = section.model === 'Rbs_Website_Website' ? section.id : section.website.id;

					// Load StaticPages.
					query = Query.treeDescendantsQuery('Rbs_Website_StaticPage', section.id !== websiteId ? section.id : websiteId);
					query.limit = QUERY_LIMIT;
					query.offset = 0;
					query.sort = [{ property: 'label' }];
					p = REST.query(query);
					p.then(function(result) {
						scope.staticPages = result.resources;
					});
					promises.push(p);

					// Load FunctionalPages.
					query = Query.simpleQuery('Rbs_Website_FunctionalPage', 'website', websiteId);
					query.limit = QUERY_LIMIT;
					query.offset = 0;
					query.sort = [{ property: 'label' }];
					p = REST.query(query);
					p.then(function(result) {
						scope.functionalPages = result.resources;
					});
					promises.push(p);

					return $q.all(promises);
				}

				/**
				 * Loads the whole list of available Functions.
				 *
				 * This method populates the following objects in the scope:
				 * - allFunctions : array of all the available Functions
				 *
				 * @returns {*}
				 */
				function loadAllFunctions() {
					scope.allFunctionsByCode = {};
					var p = REST.call(REST.getBaseUrl('Rbs/Website/FunctionsList'));
					p.then(function(functions) {
						scope.allFunctions = functions;
						angular.forEach(functions, function(spf) {
							scope.allFunctionsByCode[spf.code] = spf;
						});
					});
					return p;
				}

				/**
				 * Tells whether a Function is implemented in the current Section or not.
				 *
				 * @param functionCode
				 * @returns {boolean}
				 */
				function isFunctionImplemented(functionCode) {
					var i, result = false;
					for (i = 0; i < scope.sectionPageFunctionList.length && !result; i++) {
						if (scope.sectionPageFunctionList[i].functionCode === functionCode) {
							result = true;
						}
					}
					return result;
				}

				/**
				 * Creates the list of unimplemented Functions in the current Section.
				 *
				 * This method populates the following objects in the scope:
				 * - unimplementedFunctions : array of all the unimplemented Functions in the current Section.
				 */
				function initUnimplementedFunctions() {
					scope.unimplementedFunctions.length = 0;
					angular.forEach(scope.allFunctions, function(f) {
						if (!isFunctionImplemented(f.code)) {
							scope.unimplementedFunctions.push(f);
						}
					});
				}

				/**
				 * Prepares the selection of a page for the given function.
				 * This method loads the list of pages that are ready to implement the function.
				 *
				 * This method populates the following objects in the scope:
				 * - readyForFunctionPages    : array of pages ready to implement the function
				 * - notReadyForFunctionPages : array of pages NOT ready to implement the function
				 *
				 * @param functionCode
				 * @returns {*}
				 */
				function preparePageSelectionForFunction(functionCode) {
					var websiteId = scope.website.id;
					var p = REST.call(REST.getBaseUrl('Rbs/Website/PagesForFunction'), { function: functionCode, websiteId: websiteId });
					p.then(function(pages) {
						scope.readyForFunctionPages = [];
						scope.notReadyForFunctionPages = [];

						// Dispatch pages:
						// - into 'scope.readyForFunctionPages' for pages ready to implement the function
						// - into 'scope.notReadyForFunctionPages' for pages NOT ready to implement the function
						function dispatchPages(pages, readyPages) {
							angular.forEach(pages, function(p) {
								var i, ready = false;
								for (i = 0; i < readyPages.length && !ready; i++) {
									if (readyPages[i].id === p.id) {
										scope.readyForFunctionPages.push(p);
										ready = true;
									}
								}
								if (!ready) {
									scope.notReadyForFunctionPages.push(p);
								}
							});
						}

						var sectionPageFunction = scope.allFunctionsByCode[functionCode];
						if (sectionPageFunction) {
							if (!sectionPageFunction.hasOwnProperty(document)) {
								dispatchPages(scope.functionalPages, pages);
								dispatchPages(scope.staticPages, pages);
							}
							else {
								dispatchPages(sectionPageFunction.document ? scope.functionalPages : scope.staticPages, pages);
							}
						}
						else {
							console.error('Unknown section page function:', functionCode);
						}
					});

					return p;
				}

				/**
				 * Gets (creates if needed) the Rbs_Website_SectionPageFunction document suitable for the implementation
				 * of the given Function.
				 *
				 * @param func
				 * @returns {*}
				 */
				function getSectionPageFunctionDocument(func) {
					var spf;
					if (func.model === 'Rbs_Website_SectionPageFunction') {
						spf = func;
					}
					else {
						spf = REST.newResource('Rbs_Website_SectionPageFunction');
						spf.section = scope.section.id;
						spf.functionCode = func.code;
					}
					return spf;
				}

				//----------------------------//
				//                            //
				//   Scope methods and data   //
				//                            //
				//----------------------------//

				scope.pagesFilter = '';
				scope.sectionPageFunctionList = [];
				scope.unimplementedFunctions = [];
				scope.allFunctions = [];
				scope.inheritedFunctions = {};
				scope.newFunction = null;
				scope.showAllPages = false;

				// Returns the label of a Function from its code.
				scope.getFunctionLabel = function(spf) {
					var functionCode = angular.isObject(spf) ? spf.functionCode : spf;
					var f = scope.allFunctionsByCode[functionCode];
					return f ? f.label : '**' + functionCode + '**';
				};

				// Called when the user wants the change the page attributed to a Function.
				scope.changePage = function(spf) {
					preparePageSelectionForFunction(spf.functionCode).then(function() {
						scope.newFunction = spf;
					});
				};

				// Called when the user wants to implement a new Function to the Section.
				scope.implementFunction = function(func) {
					preparePageSelectionForFunction(func.code).then(function() {
						scope.newFunction = func;
					});
				};

				scope.hasInheritedFunctions = function() {
					var count = 0;
					// Here we check if the object is empty or not.
					// (angular.forEach does the hasOwnProperty() check).
					angular.forEach(scope.inheritedFunctions, function() {
						count++;
					});
					return count > 0;
				};

				scope.closePageSelection = function() {
					scope.newFunction = null;
				};

				// Called when the user selects a page for the Function to be implemented.
				scope.selectPage = function(p) {
					var spf = getSectionPageFunctionDocument(scope.newFunction);
					spf.page = p.id;

					REST.save(spf).then(
						function() {
							scope.closePageSelection();
							loadSectionPageFunctions(scope.section, null, null).then(initUnimplementedFunctions);
						},
						function(error) {
							console.error(error);
						}
					);
				};

				// Called when the user wants to remove a Function in the Section.
				scope.removeFunction = function(spf) {
					REST['delete'](spf).then(function() {
						loadSectionPageFunctions(scope.section, null, null).then(initUnimplementedFunctions);
					});
				};

				// Initialization:
				// load available functions, current section's data, child pages, used functions...
				scope.$watch('section', function(section) {
					if (section) {
						scope.document = section;
						loadAllFunctions().then(function() {
							$q.all([
								loadPages(section),
								loadSectionPageFunctions(section, null, null)
							]).then(function() {
								initUnimplementedFunctions();
							});
						});
					}
				});

				scope.$on('Navigation.saveContext', function(event, args) {
					var label = scope.section.label;
					args.context.label(label);
					var data = {
						website: scope.website,
						section: scope.section,
						newFunction: scope.newFunction,
						readyForFunctionPages: scope.readyForFunctionPages,
						notReadyForFunctionPages: scope.notReadyForFunctionPages
					};
					args.context.savedData('functions', data);
				});

				var currentContext = Navigation.getCurrentContext();
				if (currentContext) {
					var contextData = currentContext.savedData('functions');
					if (contextData) {
						scope.website = contextData.website;
						scope.section = contextData.section;
						scope.newFunction = contextData.newFunction;
						scope.readyForFunctionPages = contextData.readyForFunctionPages;
						scope.notReadyForFunctionPages = contextData.notReadyForFunctionPages;
						Navigation.popContext(currentContext);
						if (currentContext.valueKey() === 'editor.page' && currentContext.value()) {
							scope.selectPage(currentContext.value());
						}
					}
				}
			}
		};
	}
})();