(function() {
	'use strict';

	var app = angular.module('RbsChangeApp');

	app.directive('rbsWebsiteResponsiveSummary', ['$rootScope', '$compile', '$q', 'RbsChange.ResponsiveSummaries',
		rbsWebsiteResponsiveSummary]);
	function rbsWebsiteResponsiveSummary($rootScope, $compile, $q, ResponsiveSummaries) {
		return {
			restrict: 'A',
			scope: {
				blockNames: '@'
			},
			link: function(scope, elm) {
				if (!scope.blockNames) {
					console.warn('rbsWebsiteResponsiveSummary', 'No defined block names!');
					return;
				}

				var blockNames = scope.blockNames.split(/[\s,]+/);
				var container = elm.find('.responsive-summary');
				var compiledItems = {};
				scope.items = [];
				refreshItems();

				$rootScope.$on('ResponsiveSummaries.updated', refreshItems);

				function refreshItems() {
					var promises = [];
					scope.items = [];
					container.empty();

					var items = ResponsiveSummaries.getItems(blockNames);
					angular.forEach(items, function(item) {
						if (!item.name || !item.toCompile || !item.scope) {
							console.warn('rbsWebsiteResponsiveSummary', 'Invalid item!', item);
							return;
						}

						var itemCopy = { name: item.name, options: item.options };
						if (compiledItems[item.name]) {
							itemCopy.compiledItem = compiledItems[item.name];
						}
						else {
							var promise = $q.defer();
							promises.push(promise);
							$compile(item.toCompile)(item.scope, function(element) {
								compiledItems[item.name] = element;
								itemCopy.compiledItem = element;
								promise.resolve(true);
							});
						}
						scope.items.push(itemCopy);
					});

					if (!promises.length) {
						appendCompiledItems();
					}
					else {
						$q.all(promises).then(function () {
							appendCompiledItems();
						});
					}
				}

				function appendCompiledItems() {
					for (var i = 0; i < scope.items.length; i++) {
						var item = scope.items[i];
						container.append(item.compiledItem);
					}
				}
			}
		};
	}

	app.directive('rbsWebsiteSwitchLang', ['RbsChange.ResponsiveSummaries', rbsWebsiteSwitchLang]);
	function rbsWebsiteSwitchLang(ResponsiveSummaries) {
		return {
			restrict: 'A',
			link: function(scope) {
				if (scope.blockId) {
					scope.available = scope.available ? scope.available.split(',') : [];
					ResponsiveSummaries.registerItem(scope.blockId, scope, '<li data-rbs-website-switch-lang-responsive-summary=""></li>');
				}
			}
		};
	}

	//data-rbs-website-switch-lang-responsive-summary
	app.directive('rbsWebsiteSwitchLangResponsiveSummary', ['RbsChange.ModalStack', rbsWebsiteSwitchLangResponsiveSummary]);
	function rbsWebsiteSwitchLangResponsiveSummary(ModalStack) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-website-switch-lang-responsive-summary.twig',
			link: function(scope) {
				scope.onResponsiveSummaryClick = function() {
					var options = {
						templateUrl: '/rbs-website-switch-lang-responsive-summary-modal.twig',
						backdropClass: 'modal-backdrop-rbs-website-switch-lang-responsive-summary',
						windowClass: 'modal-responsive-summary modal-rbs-website-switch-lang-responsive-summary',
						scope: scope
					};
					ModalStack.open(options);
				}
			}
		};
	}

	app.directive('rbsWebsiteMenu', ['RbsChange.ResponsiveSummaries', 'RbsChange.AjaxAPI', rbsWebsiteMenu]);
	function rbsWebsiteMenu(ResponsiveSummaries, AjaxAPI) {
		return {
			restrict: 'A',
			scope: {},
			link: function(scope, element, attrs) {
				var blockId = attrs.blockId;
				if (blockId) {
					scope.menu = AjaxAPI.globalVar(blockId);

					ResponsiveSummaries.registerItem(blockId, scope, '<li data-rbs-website-menu-responsive-summary=""></li>');
				}
			}
		};
	}

	app.directive('rbsWebsiteMenuResponsiveSummary', ['RbsChange.ModalStack', rbsWebsiteMenuResponsiveSummary]);
	function rbsWebsiteMenuResponsiveSummary(ModalStack) {
		return {
			restrict: 'A',
			templateUrl: '/rbs-website-menu-responsive-summary.twig',
			link: function(scope) {
				scope.onResponsiveSummaryClick = function() {
					var options = {
						templateUrl: '/rbs-website-menu-responsive-summary-modal.twig',
						backdropClass: 'modal-backdrop-data-rbs-website-menu-responsive-summary',
						windowClass: 'modal-responsive-summary modal-data-rbs-website-menu-responsive-summary',
						scope: scope
					};
					ModalStack.open(options);
				}
			}
		};
	}
})();