<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\Exception
 */
class Exception extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Optional Event method: getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('message');
		$parameters->addParameterMeta('trace');
		$parameters->addParameterMeta('showTrace', false);
		$parameters->setLayoutParameters($event->getBlockLayout());
		$exception = $event->getParam('Exception');

		if ($exception instanceof \Exception)
		{
			$message = 'Exception (code ' . $exception->getCode() . ') : ' . $exception->getMessage();
			$parameters->setParameterValue('message', $message);

			$devMode = $event->getApplication()->inDevelopmentMode();
			if ($devMode || $parameters->getParameter('showTrace'))
			{
				$trace = $this->formatTrace($exception->getTrace(), $event->getApplication()->getWorkspace()->projectPath(), $devMode);
				$parameters->setParameterValue('trace', $trace);
			}
		}
		return $parameters;
	}

	/**
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		return 'exception.twig';
	}

	/**
	 * @param array $trace
	 * @param string $basePath
	 * @param boolean $devMode
	 * @return array
	 */
	protected function formatTrace($trace, $basePath, $devMode)
	{
		$basePathLength = strlen($basePath);
		$traceString = [];
		foreach ($trace as $key => $value)
		{
			if ($key > 50)
			{
				$traceString[] = '#...' . (count($trace) - 1);
				break;
			}

			$args = '';
			if (isset($value['args']) && $value['args'])
			{
				$args = $devMode ? $this->formatArgs($value['args']) : '...';
			}

			$file = '[internal function]';
			if (isset($value['file']))
			{
				$file = strpos($value['file'], $basePath) === 0 ? substr($value['file'], $basePathLength + 1) : $value['file'];
				$file .= ' (line ' . $value['line'] . ')';
			}

			$line = ['#' . $key, $file, ':'];
			$prefix = isset($value['class']) ? $value['class'] . $value['type'] : '';
			$line[] = $prefix . $value['function'] . '(' . $args . ')';

			$traceString[] = implode(' ', $line);
		}
		return implode(PHP_EOL, $traceString);
	}

	/**
	 * @param mixed $item
	 * @param int $level
	 * @return string
	 */
	protected function formatArgs($item, $level = 0)
	{
		if (is_array($item))
		{
			$result = [];
			foreach ($item as $key => $value)
			{
				$result[$key] = $this->formatArgs($value, $level + 1);
			}
			return $level ? '[' . implode(', ', $result) . ']' : implode(', ', $result);
		}
		elseif (is_object($item))
		{
			return get_class($item) . (method_exists($item, '__toString') && is_callable([$item, '__toString']) ? '{' . (string)$item . '}' : '');
		}
		return var_export($item, true);
	}
}