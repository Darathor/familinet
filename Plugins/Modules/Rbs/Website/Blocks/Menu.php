<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\Menu
 */
class Menu extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Event params includes all params from Http\Event (ex: pathRule and page).
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('templateName', 'menu-vertical.twig');
		$parameters->addParameterMeta('showTitle', false);
		$parameters->addParameterMeta('contextual', false);
		$parameters->addParameterMeta(static::DOCUMENT_TO_DISPLAY_PROPERTY_NAME);
		$parameters->addParameterMeta('offset', 0);
		$parameters->addParameterMeta('maxLevel', 1);
		$parameters->addParameterMeta('pageId');
		$parameters->addParameterMeta('sectionId');
		$parameters->addParameterMeta('websiteId');

		$parameters->addParameterMeta('imageFormats');
		$parameters->addParameterMeta('attributesMaxLevel', -1);

		$parameters->setLayoutParameters($event->getBlockLayout());
		$page = $event->getParam('page');
		if ($page instanceof \Rbs\Website\Documents\Page)
		{
			$parameters->setParameterValue('pageId', $page->getId());
			$section = $page->getSection();
			if ($section)
			{
				$parameters->setParameterValue('sectionId', $section->getId());
				$parameters->setParameterValue('websiteId', $section->getWebsite()->getId());
			}
		}

		if ($parameters->getParameter('contextual'))
		{
			$rootId = null;
			$sectionId = $parameters->getParameterValue('sectionId');
			$section = $event->getApplicationServices()->getDocumentManager()->getDocumentInstance($sectionId);
			if ($section instanceof \Rbs\Website\Documents\Section)
			{
				$ancestorIds = $section->getSectionPathIds();
				if ($ancestorIds)
				{
					$offset = $parameters->getParameterValue('offset');
					if (!$offset)
					{
						$rootId = $sectionId;
					}
					if ($offset > 0)
					{
						$rootId = \count($ancestorIds) > $offset + 1 ? $ancestorIds[$offset + 1] : null;
					}
					elseif ($offset < 0)
					{
						if ($section instanceof \Rbs\Website\Documents\Website)
						{
							$rootId = $sectionId;
						}
						else
						{
							$index = \count($ancestorIds) + $offset;
							$rootId = ($index > 0) ? $ancestorIds[$index] : $ancestorIds[1];
						}
					}
				}
			}
			$parameters->setParameterValue(static::DOCUMENT_TO_DISPLAY_PROPERTY_NAME, $rootId);
		}
		else
		{
			$document = $event->getApplicationServices()->getDocumentManager()
				->getDocumentInstance($parameters->getParameter(static::DOCUMENT_TO_DISPLAY_PROPERTY_NAME));
			if (!$this->isValidDocument($document))
			{
				$parameters->setParameterValue(static::DOCUMENT_TO_DISPLAY_PROPERTY_NAME, null);
			}
		}

		return $parameters;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @return boolean
	 */
	protected function isValidDocument($document)
	{
		return (($document instanceof \Rbs\Website\Documents\Menu && $document->activated())
			|| ($document instanceof \Rbs\Website\Documents\Section && $document->published()));
	}

	/**
	 * @api
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices, getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$dm = $event->getApplicationServices()->getDocumentManager();
		$parameters = $event->getBlockParameters();
		$doc = $dm->getDocumentInstance($parameters->getParameter(static::DOCUMENT_TO_DISPLAY_PROPERTY_NAME));
		if ($doc !== null)
		{
			if ($doc->getPersistentState() === \Change\Documents\AbstractDocument::STATE_INITIALIZED)
			{
				$dm->preLoad([[$doc->getId(), $doc->getDocumentModelName()]]);
			}
			/* @var $website \Rbs\Website\Documents\Website */
			$website = $dm->getDocumentInstance($parameters->getParameter('websiteId'));

			/* @var $page \Rbs\Website\Documents\Page */
			$page = $dm->getDocumentInstance($parameters->getParameter('pageId'));

			/* @var $section \Rbs\Website\Documents\Section */
			$section = $dm->getDocumentInstance($parameters->getParameter('sectionId'));

			$path = $section ? $section->getSectionThread() : [];

			$i18nManager = $event->getApplicationServices()->getI18nManager();
			$treeManager = $event->getApplicationServices()->getTreeManager();

			$urlManager = $event->getUrlManager();

			$menuComposer = new \Rbs\Website\Menu\MenuComposer($urlManager, $i18nManager, $dm, $treeManager);

			$maxLevel = $parameters->getParameter('maxLevel');
			$root = $menuComposer->getMenuEntry($website, $doc, $maxLevel, $page, $path);

			$attributesMaxLevel = $parameters->getParameter('attributesMaxLevel');
			if ($root && $attributesMaxLevel >= 0)
			{
				$context = $this->populateContext($event->getApplication(), $dm, $parameters);
				$this->addAttributes($event, $dm, $root, $attributesMaxLevel, $context->toArray());
			}
			$attributes['root'] = $root;
			return $this->getDefaultTemplateName();
		}
		return null;
	}

	/**
	 * @param \Rbs\Website\Menu\MenuEntry $entry
	 * @param integer $attributesMaxLevel
	 * @return array
	 */
	protected function getDocumentsAttributes(\Rbs\Website\Menu\MenuEntry $entry, $attributesMaxLevel)
	{
		$docs = [];
		$docId = $entry->getDocumentId();
		if ($docId)
		{
			$docs[$docId] = true;
		}
		if ($attributesMaxLevel > 0 && $entry->hasChild())
		{
			foreach ($entry->getChildren() as $child)
			{
				$subDocIds = $this->getDocumentsAttributes($child, $attributesMaxLevel - 1);
				if ($subDocIds)
				{
					foreach ($subDocIds as $docId)
					{
						$docs[$docId] = true;
					}
				}
			}
		}

		return array_keys($docs);
	}

	/**
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Rbs\Website\Menu\MenuEntry $entry
	 * @param integer $attributesMaxLevel
	 * @param array $context
	 */
	protected function addAttributes($event, $documentManager, $entry, $attributesMaxLevel, $context)
	{
		$document = $documentManager->getDocumentInstance($entry->getDocumentId());
		if ($document)
		{
			$data = (new \Rbs\Website\Menu\ItemDataComposer($event, $document, $context))->toArray();
			if (isset($data['typology']))
			{
				$entry->setTypology($data['typology']);
			}
		}

		if ($attributesMaxLevel > 0 && $entry->hasChild())
		{
			foreach ($entry->getChildren() as $child)
			{
				$this->addAttributes($event, $documentManager, $child, $attributesMaxLevel - 1, $context);
			}
		}
	}

	/**
	 * @param \Change\Application $application
	 * @param \Change\Documents\DocumentManager $documentManager
	 * @param \Change\Presentation\Blocks\Parameters $parameters
	 * @return \Change\Http\Ajax\V1\Context
	 */
	protected function populateContext($application, $documentManager, $parameters)
	{
		$context = new \Change\Http\Ajax\V1\Context($application, $documentManager);
		$context->setDetailed(true);
		$context->setVisualFormats($parameters->getParameter('imageFormats'));
		$context->setURLFormats(['canonical']);
		$context->setPage($parameters->getParameter('pageId'));
		return $context;
	}

	/**
	 * @return string
	 */
	protected function getDefaultTemplateName()
	{
		return 'menu-inline.twig';
	}
}