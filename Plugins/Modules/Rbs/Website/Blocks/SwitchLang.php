<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\SwitchLang
 */
class SwitchLang extends \Change\Presentation\Blocks\Standard\Block
{
	/**
	 * @api
	 * Set Block Parameters on $event
	 * Required Event method: getBlockLayout, getApplication, getApplicationServices, getServices, getHttpRequest
	 * Event params includes all params from Http\Event (ex: pathRule and page).
	 * @param \Change\Presentation\Blocks\Event $event
	 * @return \Change\Presentation\Blocks\Parameters
	 */
	protected function parameterize($event)
	{
		$parameters = parent::parameterize($event);
		$parameters->addParameterMeta('untranslatedPageAction', 'hideLink');
		$parameters->setLayoutParameters($event->getBlockLayout());

		// Get current website Id
		$parameters->setParameterValue('websiteId', $event->getParam('website')->getId());

		if ($event->getParam('errorFunction'))
		{
			return $parameters;
		}

		// Get current document Id
		$document = $event->getParam('document');
		if ($document instanceof \Change\Documents\AbstractDocument)
		{
			$parameters->setParameterValue('documentId', $document->getId());
		}

		// Get current page Id
		$page = $event->getParam('page');
		if ($page instanceof \Rbs\Website\Documents\Page)
		{
			$parameters->setParameterValue('pageId', $page->getId());
		}

		return $parameters;
	}

	/**
	 * @api
	 * Set $attributes and return a twig template file name OR set HtmlCallback on result
	 * Required Event method: getBlockLayout, getBlockParameters, getApplication, getApplicationServices, getServices,
	 *     getHttpRequest
	 * @param \Change\Presentation\Blocks\Event $event
	 * @param \ArrayObject $attributes
	 * @return string|null
	 */
	protected function execute($event, $attributes)
	{
		$parameters = $event->getBlockParameters();
		$dm = $event->getApplicationServices()->getDocumentManager();
		$i18nManager = $event->getApplicationServices()->getI18nManager();

		$website = $dm->getDocumentInstance($parameters->getWebsiteId());
		if (!($website instanceof \Rbs\Website\Documents\Website))
		{
			return null;
		}

		$LCIDs = $website->getLCIDArray();

		/* @var $page \Rbs\Website\Documents\Page */
		$page = $dm->getDocumentInstance($parameters->getPageId());

		/* @var $doc \Change\Documents\AbstractDocument */
		$doc = $dm->getDocumentInstance($parameters->getDocumentId());
		if (!$doc)
		{
			$doc = $page;
		}

		$urls = [];
		$formatters = ['ucf'];
		if ($parameters->getParameter('displayShort') === true)
		{
			$formatters = ['ucf', 'short'];
		}

		if ($doc)
		{
			$urlManager = $event->getUrlManager();
			$section = $urlManager->getSection();
			$query = $urlManager->getSelf()->getQueryAsArray();

			foreach ($LCIDs as $LCID)
			{
				try
				{
					$dm->pushLCID($LCID);

					$url = $this->getUrl($website, $page, $doc, $LCID, $section, $query, $parameters->getParameter('untranslatedPageAction'));
					if ($url)
					{
						$urls[] = [
							'lang' => substr($LCID, -2),
							'title' => $i18nManager->transLCIDInSelf($LCID, $formatters),
							'LCID' => $LCID,
							'url' => $url
						];
					}

					$dm->popLCID();
				}
				catch (\Exception $e)
				{
					$dm->popLCID($e);
				}
			}
		}

		$currentLCID = $website->getCurrentLCID();
		$attributes['blockData'] = [
			'lang' => substr($currentLCID, -2),
			'title' => $i18nManager->transLCIDInSelf($currentLCID, $formatters),
			'LCID' => $currentLCID,
			'urls' => $urls
		];
		return 'switch-lang.twig';
	}

	/**
	 * @param \Rbs\Website\Documents\Website $website
	 * @param \Rbs\Website\Documents\Page $page
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param string $LCID
	 * @param \Change\Presentation\Interfaces\Section|null $section
	 * @param array|\ArrayObject $query
	 * @param string $untranslatedPageAction
	 * @return string|null
	 */
	protected function getUrl($website, $page, $doc, $LCID, $section, $query, $untranslatedPageAction)
	{
		if (!$website->published())
		{
			return null;
		}

		$urlManager = $website->getUrlManager($LCID);

		if ($this->checkDocument($doc, $LCID) && $this->checkDocument($page, $LCID))
		{
			return $urlManager->getByDocument($doc, $section, $query)->normalize()->toString();
		}

		if ($untranslatedPageAction == 'redirectHomepage')
		{
			return $urlManager->getBaseUri()->normalize()->toString();
		}
		return null;
	}

	/**
	 * @param \Change\Documents\AbstractDocument $doc
	 * @param string $LCID
	 * @return bool
	 */
	protected function checkDocument($doc, $LCID)
	{
		if ($doc instanceof \Change\Documents\Interfaces\Publishable)
		{
			return $doc->published();
		}
		if ($doc instanceof \Change\Documents\Interfaces\Localizable)
		{
			return $doc->hasLocalizedPartArray($LCID);
		}
		return true;
	}
}