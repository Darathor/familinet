<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Blocks;

/**
 * @name \Rbs\Website\Blocks\SwitchLangInformation
 */
class SwitchLangInformation extends \Change\Presentation\Blocks\Information
{
	public function onInformation(\Change\Events\Event $event)
	{
		parent::onInformation($event);
		$i18nManager = $event->getApplicationServices()->getI18nManager();
		$ucf = ['ucf'];
		$this->setSection($i18nManager->trans('m.rbs.website.admin.module_name', $ucf));
		$this->setLabel($i18nManager->trans('m.rbs.website.admin.switchlang', $ucf));

		$this->addParameterInformation('displayShort', \Change\Documents\Property::TYPE_BOOLEAN, false, false)
			->setLabel($i18nManager->trans('m.rbs.website.admin.display_lang_short'));

		$this->addParameterInformation('untranslatedPageAction', \Change\Documents\Property::TYPE_STRING, false, 'hideLink')
			->setLabel($i18nManager->trans('m.rbs.website.admin.untranslated_page_action', $ucf))
			->setCollectionCode('Rbs_Website_UntranslatedPageActions');
	}
}