<?php
/**
 * Copyright (C) 2014 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Collection;

use Change\I18n\I18nString;

/**
 * @name \Rbs\Website\Collection\Collections
 */
class Collections
{
	/**
	 * @param \Change\Events\Event $event
	 */
	public function addInterstitialAudiences(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$items = [
				'all' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_audience_all', ['ucf']),
				'guest' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_audience_guest', ['ucf']),
				'registered' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_audience_registered', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Website_InterstitialAudiences', $items);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addInterstitialDisplayFrequencies(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'always' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_display_frequency_always', ['ucf']),
				'session' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_display_frequency_session', ['ucf']),
				'reprieve' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_display_frequency_reprieve', ['ucf']),
				'once' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_display_frequency_once', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Website_InterstitialDisplayFrequencies', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addInterstitialPopinSizes(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'small' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_popin_size_small', ['ucf']),
				'medium' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_popin_size_medium', ['ucf']),
				'large' => new I18nString($i18n, 'm.rbs.website.admin.interstitial_popin_size_large', ['ucf'])
			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Website_InterstitialPopinSizes', $collection);
			$event->setParam('collection', $collection);
		}
	}

	/**
	 * @param \Change\Events\Event $event
	 */
	public function addUntranslatedPageActions(\Change\Events\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		if ($applicationServices)
		{
			$i18n = $applicationServices->getI18nManager();
			$collection = [
				'hideLink' => new I18nString($i18n, 'm.rbs.website.admin.hide_language_link', ['ucf']),
				'redirectHomepage' => new I18nString($i18n, 'm.rbs.website.admin.redirect_to_homepage', ['ucf'])

			];
			$collection = new \Change\Collection\CollectionArray('Rbs_Website_UntranslatedPageActions', $collection);
			$event->setParam('collection', $collection);
		}
	}
}