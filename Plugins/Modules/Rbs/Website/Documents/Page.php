<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\Page
 */
abstract class Page extends \Compilation\Rbs\Website\Documents\Page implements \Change\Presentation\Interfaces\Page
{
	protected function onCreate()
	{
		if (!$this->getUseCache())
		{
			$this->setTTL(0);
		}
		else
		{
			$this->setTTL(max(0, (int)$this->getTTL()));
		}
	}

	protected function onUpdate()
	{
		if (!$this->getUseCache())
		{
			$this->setTTL(0);
		}
		else
		{
			$this->setTTL(max(0, (int)$this->getTTL()));
		}
	}

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onNormalizeEditableContent($event); }, 5);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onNormalizeEditableContent($event); }, 5);
		
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_CREATE, function ($event) { $this->onInitSupportedFunctions($event); }, 3);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onInitSupportedFunctions($event); }, 3);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onNormalizeEditableContent(\Change\Documents\Events\Event $event)
	{
		if ($event->getDocument() !== $this || !$this->isPropertyModified('editableContent'))
		{
			return;
		}
		$contentLayout = new \Change\Presentation\Layout\Layout($this->getCurrentLocalization()->getEditableContent());
		$blocks = $contentLayout->getBlocks();
		$event->getApplicationServices()->getBlockManager()->normalizeBlocksParameters($blocks);
		$this->getCurrentLocalization()->setEditableContent($contentLayout->toArray());
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	public function onDefaultUpdateRestResult(\Change\Documents\Events\Event $event)
	{
		parent::onDefaultUpdateRestResult($event);

		/** @var $page Page */
		if ($this != $event->getDocument())
		{
			return;
		}

		$restResult = $event->getParam('restResult');
		if ($restResult instanceof \Change\Http\Rest\V1\Resources\DocumentResult)
		{
			$content = $restResult->getProperty('editableContent');
			if ($content)
			{
				foreach ($content as $key => $container)
				{
					if ($container['type'] == 'container'
						&& (!isset($container['items']) || !is_array($container['items']) || count($container['items']) == 0)
					)
					{
						$content[$key]['items'] = [['id' => 1, 'type' => 'block-chooser']];
					}
				}
			}
			$restResult->setProperty('editableContent', $content);
		}
	}

	/**
	 * @see \Change\Presentation\Interfaces\Page::getIdentifier()
	 * @return string
	 */
	public function getIdentifier()
	{
		return $this->getId() . ',' . $this->getCurrentLCID();
	}

	/**
	 * @see \Change\Presentation\Interfaces\Page::getContentLayout()
	 * @return \Change\Presentation\Layout\Layout
	 */
	public function getContentLayout()
	{
		$defaultDisplayColumnsFrom = $this->getPageTemplate() ? $this->getPageTemplate()->getDefaultDisplayColumnsFrom() : 'M';
		return new \Change\Presentation\Layout\Layout($this->getCurrentLocalization()->getEditableContent(), $defaultDisplayColumnsFrom);
	}

	/**
	 * @see \Change\Presentation\Interfaces\Page::getModificationDate()
	 * @return \DateTime
	 */
	public function getModificationDate()
	{
		return $this->getCurrentLocalization()->getModificationDate();
	}

	/**
	 * @see \Change\Presentation\Interfaces\Page::getModificationDate()
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getCurrentLocalization()->getTitle();
	}

	/**
	 * @return \Change\Presentation\Interfaces\Template
	 */
	public function getTemplate()
	{
		return $this->getPageTemplate();
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onInitSupportedFunctions(\Change\Documents\Events\Event $event)
	{
		if ($this != $event->getDocument())
		{
			return;
		}

		if (!$this->isPropertyModified('supportedFunctionsCode') && $this->isPropertyModified('editableContent'))
		{
			$this->refreshSupportedFunctionsCode($event->getApplicationServices()->getPageManager());
		}
	}

	/**
	 * @param \Change\Presentation\Pages\PageManager $pageManager
	 */
	public function refreshSupportedFunctionsCode($pageManager)
	{
		$blocksName = [];
		foreach ($this->getContentLayout()->getBlocks() as $block)
		{
			$blocksName[] = $block->getName();
		}
		if (count($blocksName))
		{
			$supportedFunctions = [];
			$functions = $pageManager->getFunctions();
			foreach ($blocksName as $blockName)
			{
				foreach ($functions as $function)
				{
					if (isset($function['block']))
					{
						if (is_array($function['block']) && in_array($blockName, $function['block']))
						{
							$supportedFunctions[$function['code']] = true;
						}
						elseif (is_string($function['block']) && $function['block'] == $blockName)
						{
							$supportedFunctions[$function['code']] = true;
						}
					}
				}
			}
			$sf = $this->getSupportedFunctionsCode();
			if (is_array($sf))
			{
				$sf[$this->getCurrentLCID()] = array_keys($supportedFunctions);
			}
			else
			{
				$sf = [$this->getCurrentLCID() => array_keys($supportedFunctions)];
			}
			$this->setSupportedFunctionsCode($sf);
		}
	}

	/**
	 * @return array
	 */
	public function getAllSupportedFunctionsCode()
	{
		$allSupportedFunctionsCode = [];
		$array = $this->getSupportedFunctionsCode();
		if (is_array($array))
		{
			foreach ($array as $data)
			{
				if (is_array($data))
				{
					$allSupportedFunctionsCode = array_merge($allSupportedFunctionsCode, $data);
				}
				elseif (is_string($data))
				{
					$allSupportedFunctionsCode[] = $data;
				}
			}
			$allSupportedFunctionsCode = array_values(array_unique($allSupportedFunctionsCode));
		}
		return $allSupportedFunctionsCode;
	}
}