<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\Section
 */
abstract class Section extends \Compilation\Rbs\Website\Documents\Section implements \Change\Presentation\Interfaces\Section
{
	/**
	 * @var integer|false
	 */
	protected $indexPageId = false;

	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_DISPLAY_PAGE, function ($event) { $this->onDocumentDisplayPage($event); }, 10);
		$eventManager->attach('getPageByFunction', function ($event) { $this->onGetPageByFunction($event); }, 10);
		$eventManager->attach(\Change\Documents\Events\Event::EVENT_NODE_UPDATED, function ($event) { $this->onNodeUpdated($event); }, 10);
		$eventManager->attach('getSectionPath', function ($event) { $this->onGetSectionPath($event); }, 10);

		$eventManager->attach(\Change\Documents\Events\Event::EVENT_UPDATE, function ($event) { $this->onSaveAuthorized($event); }, 10);
		$eventManager->attach('loadAuthorized', function ($event) { $this->onLoadAuthorized($event); }, 10);
	}

	/**
	 * @param \Rbs\Website\Documents\Page|integer|false $indexPage
	 * @return $this
	 */
	public function setIndexPage($indexPage)
	{
		$this->indexPageId = 0;
		if ($indexPage instanceof \Rbs\Website\Documents\Page)
		{
			$this->indexPageId = $indexPage->getId();
		}
		elseif ($indexPage === false || is_int($indexPage))
		{
			$this->indexPageId = $indexPage;
		}
		return $this;
	}

	/**
	 * @return \Rbs\Website\Documents\Page|null
	 */
	public function getIndexPage()
	{
		if (false === $this->indexPageId)
		{
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['functionCode' => 'Rbs_Website_Section', 'page' => null]);
			$eventManager->trigger('getPageByFunction', $this, $args);
			$page = $args['page'];
			$this->indexPageId = ($page instanceof \Rbs\Website\Documents\Page) ? $page->getId() : 0;
		}
		$indexPage = $this->indexPageId ? $this->getDocumentManager()->getDocumentInstance($this->indexPageId) : null;
		return $indexPage instanceof \Rbs\Website\Documents\Page ? $indexPage : null;
	}

	/**
	 * @return string
	 */
	public function getPathSuffix()
	{
		return '/';
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDocumentDisplayPage(\Change\Documents\Events\Event $event)
	{
		$document = $event->getDocument();
		if ($document instanceof self)
		{
			/* @var $pathRule \Change\Http\Web\PathRule */
			$pathRule = $event->getParam('pathRule');
			$parameters = $pathRule->getQueryParameters();
			if (!array_key_exists('sectionPageFunction', $parameters)
				|| $parameters['sectionPageFunction'] === 'Rbs_Website_Section'
			)
			{
				$page = $document->getIndexPage();
				if ($page instanceof \Change\Documents\Interfaces\Publishable && !$page->published())
				{
					return;
				}

				if ($page instanceof FunctionalPage)
				{
					$page->setSection($document);
				}
				$event->setParam('page', $page);
				$event->stopPropagation();
			}
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetPageByFunction(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('page') !== null)
		{
			return;
		}

		$functionCode = $event->getParam('functionCode');
		$cacheManager = $event->getApplicationServices()->getCacheManager();
		$namespace = $cacheManager->buildMemoryNamespace('PageByFunction');
		$key = $this->getId() . '-' . $functionCode;

		$entry = $cacheManager->getEntry($namespace, $key);
		if (is_array($entry))
		{
			[$pageId, $sectionId] = $entry;
			$event->setParam('page', $pageId ? $this->getDocumentManager()->getDocumentInstance($pageId) : null);
			$event->setParam('section', $sectionId ? $this->getDocumentManager()->getDocumentInstance($sectionId) : $this);
			return;
		}

		if ($websiteResolver = $cacheManager->isValidNamespace('WebsiteResolver'))
		{
			$entry = $cacheManager->getEntry('WebsiteResolver', $key, ['ttl' => 3600]);
			if (is_array($entry))
			{
				$cacheManager->setEntry($namespace, $key, $entry);
				[$pageId, $sectionId] = $entry;
				$event->setParam('page', $pageId ? $this->getDocumentManager()->getDocumentInstance($pageId) : null);
				$event->setParam('section', $sectionId ? $this->getDocumentManager()->getDocumentInstance($sectionId) : $this);
				return;
			}
		}

		/** @var \Rbs\Website\Documents\Page|\Rbs\Website\Documents\FunctionalPage|\Rbs\Website\Documents\StaticPage $page */
		$page = null;

		/** @var \Rbs\Website\Documents\Section $section */
		$section = null;

		if ($functionCode === 'Rbs_Website_Section')
		{
			$query = $this->getDocumentManager()->getNewQuery('Rbs_Website_Page');
			$subQuery = $query->getModelBuilder('Rbs_Website_SectionPageFunction', 'page');
			$subQuery->andPredicates($subQuery->eq('section', $this), $subQuery->eq('functionCode', 'Rbs_Website_Section'));
			$page = $query->getFirstDocument();
		}
		else
		{
			$treeNode = $event->getApplicationServices()->getTreeManager()->getNodeByDocument($this);
			if ($treeNode)
			{
				$documentManager = $event->getApplicationServices()->getDocumentManager();
				$sectionIds = $treeNode->getAncestorIds();
				$sectionIds[] = $this->getId();

				$q = $documentManager->getNewQuery('Rbs_Website_SectionPageFunction');
				$q->andPredicates($q->eq('functionCode', $functionCode), $q->in('section', $sectionIds));
				$dbq = $q->dbQueryBuilder();
				$fb = $dbq->getFragmentBuilder();
				$dbq->addColumn($fb->getDocumentColumn('page'))->addColumn($fb->getDocumentColumn('section'));
				$sq = $dbq->query();

				$pageBySections = $sq->getResults($sq->getRowsConverter()->addIntCol('page', 'section')
					->indexBy('section')->singleColumn('page'));
				if (count($pageBySections))
				{
					foreach (array_reverse($sectionIds) as $sectionId)
					{
						if (isset($pageBySections[$sectionId]))
						{
							$pageId = (int)$pageBySections[$sectionId];
							$documentManager->preLoad([[$pageId, null]]);
							$page = $documentManager->getDocumentInstance($pageId);

							if ($sectionId !== $this->getId())
							{
								$section = $documentManager->getDocumentInstance($sectionId);
							}
							break;
						}
					}
				}
			}

			if ($page instanceof \Rbs\Website\Documents\Page && !$page->getCurrentLocalization()->isNew())
			{
				if ($page instanceof \Rbs\Website\Documents\StaticPage && !$page->published())
				{
					$page = null;
				}
				elseif ($page instanceof \Rbs\Website\Documents\FunctionalPage)
				{
					$page->setSection($section ?: $this);
				}
			}
		}

		$event->setParam('page', $page);
		$event->setParam('section', $section);

		$entry = [$page ? $page->getId() : 0, $section ? $section->getId() : 0];
		if ($websiteResolver)
		{
			$cacheManager->setEntry('WebsiteResolver', $key, $entry, ['ttl' => 3600]);
		}
		$cacheManager->setEntry($namespace, $key, $entry);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 * @throws \Exception
	 */
	protected function onNodeUpdated(\Change\Documents\Events\Event $event)
	{
		$node = $event->getParam('node');
		if ($node instanceof \Change\Documents\TreeNode && ($website = $this->getWebsite()))
		{
			$applicationServices = $event->getApplicationServices();
			$permissionManager = $applicationServices->getPermissionsManager();
			$accessorIds = $permissionManager->getSectionAccessorIds($node->getParentId(), $website->getId());
			if (count($accessorIds))
			{
				foreach ($accessorIds as $accessorId)
				{
					$permissionManager->addWebRule($this->getId(), $website->getId(), $accessorId);
				}
			}
			else
			{
				$permissionManager->addWebRule($this->getId(), $website->getId());
			}

			$cacheManager = $applicationServices->getCacheManager();
			$cacheManager->removeEntry('user', 'web_permissions_' . $website->getId());
		}
	}

	/**
	 * @return \Rbs\Website\Documents\Section[]
	 */
	public function getSectionThread()
	{
		return $this->getSectionPath();
	}

	/**
	 * @var integer[]
	 */
	protected $sectionPathIds;

	public function getSectionPathIds()
	{
		if ($this->sectionPathIds === null)
		{
			$this->sectionPathIds = [];
			$eventManager = $this->getEventManager();
			$args = $eventManager->prepareArgs(['sectionsIds' => null]);
			$eventManager->trigger('getSectionPath', $this, $args);
			$this->sectionPathIds = \is_array($args['sectionsIds']) ? $args['sectionsIds'] : [];
		}
		return $this->sectionPathIds;

	}

	/**
	 * @return \Rbs\Website\Documents\Section[]
	 */
	public function getSectionPath()
	{
		$sections = [];
		if ($sectionPathIds = $this->getSectionPathIds())
		{
			$documentManager = $this->getDocumentManager();
			/** @noinspection ForeachSourceInspection */
			foreach ($sectionPathIds as $sectionId)
			{
				$section = $documentManager->getDocumentInstance($sectionId);
				if ($section instanceof self)
				{
					$sections[] = $section;
				}
			}
		}
		$sections[] = $this;
		return $sections;
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onGetSectionPath(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('sectionsIds') !== null)
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$cacheManager = $applicationServices->getCacheManager();
		$key = 'sectionsIds-' . $this->getId();
		$sectionIds = $cacheManager->getEntry('WebsiteResolver', $key, ['ttl', 600]);
		if ($sectionIds === null)
		{
			$sectionIds = [];
			$documentManager = $this->getDocumentManager();
			$tm = $applicationServices->getTreeManager();
			$tn = $tm->getNodeByDocument($this);
			if ($tn)
			{
				foreach ($tm->getAncestorNodes($tn) as $node)
				{
					$node->setTreeManager($tm);
					$section = $documentManager->getDocumentInstance($node->getDocumentId());
					if ($section instanceof self)
					{
						$sectionIds[] = $section->getId();
					}
				}
			}
			$cacheManager->setEntry('WebsiteResolver', $key, $sectionIds, ['ttl', 600]);
		}
		$event->setParam('sectionsIds', $sectionIds);
	}

	/**
	 * @see \Change\Presentation\Interfaces\Section::getTitle()
	 * @return string
	 */
	public function getTitle()
	{
		return $this->getCurrentLocalization()->getTitle() ?: $this->getRefLocalization()->getTitle();
	}

	/**
	 * @see \Change\Presentation\Interfaces\Section::getPathPart()
	 * @return string
	 */
	public function getPathPart()
	{
		return $this->getCurrentLocalization()->getPathPart() ?: $this->getRefLocalization()->getPathPart();
	}

	// Authorized Users and groups.

	/**
	 * @var \Rbs\User\Documents\User[]
	 */
	protected $authorizedUsers;

	/**
	 * @var \Rbs\User\Documents\Group[]
	 */
	protected $authorizedGroups;

	/**
	 *
	 */
	protected function loadAuthorized()
	{
		$this->authorizedUsers = [];
		$this->authorizedGroups = [];
		if ($this->isNew())
		{
			return;
		}

		$eventManager = $this->getEventManager();
		$args = $eventManager->prepareArgs(['authorizedUsers' => null, 'authorizedGroups' => null]);
		$eventManager->trigger('loadAuthorized', $this, $args);

		if (is_array($args['authorizedUsers']))
		{
			$this->authorizedUsers = $args['authorizedUsers'];
		}
		if (is_array($args['authorizedGroups']))
		{
			$this->authorizedGroups = $args['authorizedGroups'];
		}
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onLoadAuthorized(\Change\Documents\Events\Event $event)
	{
		$permissionManager = $event->getApplicationServices()->getPermissionsManager();
		$accessorIds = $permissionManager->getSectionAccessorIds($this->getId(), $this->getWebsite()->getId(), 'Rbs_User_User');
		$documents = [];
		foreach ($accessorIds as $accessorId)
		{
			$accessor = $this->getDocumentManager()->getDocumentInstance($accessorId);
			if ($accessor instanceof \Change\Documents\AbstractDocument)
			{
				$documents[] = $accessor;
			}
		}
		$event->setParam('authorizedUsers', $documents);

		$accessorIds = $permissionManager->getSectionAccessorIds($this->getId(), $this->getWebsite()->getId(), 'Rbs_User_Group');
		$documents = [];
		foreach ($accessorIds as $accessorId)
		{
			$accessor = $this->getDocumentManager()->getDocumentInstance($accessorId);
			if ($accessor instanceof \Change\Documents\AbstractDocument)
			{
				$documents[] = $accessor;
			}
		}
		$event->setParam('authorizedGroups', $documents);
	}

	/**
	 * @return \Rbs\User\Documents\User[]|null
	 */
	public function getAuthorizedUsers()
	{
		if ($this->authorizedUsers === null)
		{
			$this->loadAuthorized();
		}
		return $this->authorizedUsers;
	}

	/**
	 * @param \Rbs\User\Documents\User[] $authorizedUsers
	 * @return $this
	 */
	public function setAuthorizedUsers($authorizedUsers)
	{
		$this->authorizedUsers = is_array($authorizedUsers) ? $authorizedUsers : null;
		return $this;
	}

	/**
	 * @return \Rbs\User\Documents\Group[]
	 */
	public function getAuthorizedGroups()
	{
		if ($this->authorizedGroups === null)
		{
			$this->loadAuthorized();
		}
		return $this->authorizedGroups;
	}

	/**
	 * @param \Rbs\User\Documents\Group[] $authorizedGroups
	 * @return $this
	 */
	public function setAuthorizedGroups($authorizedGroups)
	{
		$this->authorizedGroups = is_array($authorizedGroups) ? $authorizedGroups : null;
		return $this;
	}

	protected function onSaveAuthorized(\Change\Documents\Events\Event $event)
	{
		$website = $this->getWebsite();
		if (!$website)
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$permissionManager = $applicationServices->getPermissionsManager();

		/** @var \Rbs\User\Documents\User[] $authorizedUsers */
		$authorizedUsers = $this->getAuthorizedUsers() ?? [];
		$authorizedGroups = $this->getAuthorizedGroups();


		$permissionManager->deleteWebRules($this->getId(), $website->getId());
		/** @noinspection ForeachSourceInspection */
		foreach ($authorizedUsers as $authorizedUser)
		{
			/* @var $authorizedUser \Rbs\User\Documents\User */
			$permissionManager->addWebRule($this->getId(), $website->getId(), $authorizedUser->getId());
		}

		foreach ($authorizedGroups as $authorizedGroup)
		{
			/* @var $authorizedGroup \Rbs\User\Documents\Group */
			$permissionManager->addWebRule($this->getId(), $website->getId(), $authorizedGroup->getId());
		}

		//if no accessor is set, set public access
		if (!count($this->getAuthorizedUsers()) && !count($this->getAuthorizedGroups()))
		{
			$permissionManager->addWebRule($this->getId(), $website->getId());
		}

		$cacheManager = $applicationServices->getCacheManager();
		$cacheManager->removeEntry('user', 'web_permissions_' . $website->getId());
	}
}