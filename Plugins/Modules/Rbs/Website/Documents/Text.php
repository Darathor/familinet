<?php
/**
 * Copyright (C) 2014 Eric Hauswald
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Documents;

/**
 * @name \Rbs\Website\Documents\Text
 */
class Text extends \Compilation\Rbs\Website\Documents\Text
{
	/**
	 * @param \Zend\EventManager\EventManagerInterface $eventManager
	 */
	protected function attachEvents($eventManager)
	{
		parent::attachEvents($eventManager);
		$eventManager->attach('getAJAXData', function ($event) { $this->onDefaultGetAJAXData($event); }, 5);
	}

	/**
	 * @param \Change\Documents\Events\Event $event
	 */
	protected function onDefaultGetAJAXData(\Change\Documents\Events\Event $event)
	{
		if ($event->getParam('data') !== null)
		{
			return;
		}

		$context = $event->getParam('context');
		$data['common']['id'] = $this->getId();

		if (!$context || !($context['website'] instanceof \Rbs\Website\Documents\Website))
		{
			$event->setParam('data', $data);
			return;
		}

		$richTextManager = $event->getApplicationServices()->getRichTextManager();
		$richTextContext = ['website' => $context['website']];

		$data['common']['title'] = $this->getCurrentLocalization()->getTitle();
		$text = $this->getCurrentLocalization()->getText();
		$data['html'] = $text ? $richTextManager->render($text, 'Website', $richTextContext) : null;
		$event->setParam('data', $data);
	}
}