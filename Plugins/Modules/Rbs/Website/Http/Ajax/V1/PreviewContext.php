<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Http\Ajax\V1;

/**
 * @name \Rbs\Website\Http\Ajax\V1\PreviewContext
 */
class PreviewContext
{
	/**
	 * @param \Change\Http\Event $event
	 */
	public function onSetPreviewContext(\Change\Http\Event $event)
	{
		$previewKeyHeader = $event->getRequest()->getHeader('Change-Preview-Key');
		if (!($previewKeyHeader instanceof \Zend\Http\Header\HeaderInterface))
		{
			return;
		}

		$applicationServices = $event->getApplicationServices();
		$cacheManager = $applicationServices->getCacheManager();
		$options = ['ttl' => 600];
		$previewKey = $previewKeyHeader->getFieldValue();
		$previewContext = $cacheManager->getEntry('adminPreview', $previewKey, $options);
		if (!$previewContext)
		{
			return;
		}

		$helper = new \Rbs\Website\Preview\PreviewHelper();
		// Prepare the document.
		$documentManager = $applicationServices->getDocumentManager();
		$LCID = $previewContext['LCID'];
		try
		{
			$documentManager->pushLCID($LCID);
			$previewId = $previewContext['previewId'];
			$helper->preparePreview($event, $documentManager->getDocumentInstance($previewId), $previewContext['properties']);
			$documentManager->popLCID();
		}
		catch (\Exception $e)
		{
			$documentManager->popLCID($e);
		}

		$helper->setContext($event, $previewContext);
	}
}