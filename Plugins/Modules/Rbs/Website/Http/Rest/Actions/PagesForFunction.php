<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Http\Rest\Actions;

/**
 * Returns a list of FunctionalPages that has the given function.
 * @name \Rbs\Website\Http\Rest\Actions\PagesForFunction
 */
class PagesForFunction
{
	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function execute(\Change\Http\Event $event)
	{
		$request = $event->getRequest();
		if ($request->isGet())
		{
			$function = (string)$request->getQuery('function');
			$websiteId = (int)$request->getQuery('websiteId');
			$event->setResult($this->generateResult($event->getApplicationServices(), $function, $websiteId));
		}
		else
		{
			$result = $event->getController()->notAllowedError($request->getMethod(), [\Change\Http\Request::METHOD_GET]);
			$event->setResult($result);
		}
	}

	/**
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @param string $function
	 * @param integer $websiteId
	 * @return \Change\Http\Rest\V1\ArrayResult
	 */
	protected function generateResult($applicationServices, $function, $websiteId)
	{
		$pagesForFunction = [];
		$allFunctions = $applicationServices->getPageManager()->getFunctions();
		foreach ($applicationServices->getPageManager()->getFunctions() as $functionData)
		{
			$allFunctions[$functionData['code']] = $functionData;
		}

		$documentManager = $applicationServices->getDocumentManager();
		$query = $documentManager->getNewQuery('Rbs_Website_FunctionalPage');
		$query->andPredicates($query->eq('website', $websiteId), $query->like('supportedFunctionsCode', '"' . $function . '"'));
		$pages = $query->getDocuments()->toArray();
		if (!$this->isDocumentFunction($allFunctions, $function))
		{
			$query = $documentManager->getNewQuery('Rbs_Website_StaticPage');
			$treePB = new \Change\Documents\Query\TreePredicateBuilder($query, $applicationServices->getTreeManager());
			$query->andPredicates($treePB->descendantOf($websiteId), $query->like('supportedFunctionsCode', '"' . $function . '"'));
			$pages = array_merge($pages, $query->getDocuments()->toArray());
		}

		if (count($pages))
		{
			$website = $documentManager->getDocumentInstance($websiteId);
			$websiteLabel = ($website instanceof \Rbs\website\Documents\Website) ? $website->getLabel() : null;
			foreach ($pages as $page)
			{
				/* @var $page \Rbs\Website\Documents\FunctionalPage */
				$functions = [];
				foreach ($page->getAllSupportedFunctionsCode() as $code)
				{
					if (isset($allFunctions[$code]))
					{
						$functions[] = ['code' => $code, 'label' => $allFunctions[$code]['label']];
					}
				}

				$pagesForFunction[] = [
					'id' => $page->getId(),
					'label' => $page->getLabel(),
					'website' => $websiteLabel,
					'functions' => $functions
				];
			}
		}

		$result = new \Change\Http\Rest\V1\ArrayResult();
		$result->setArray($pagesForFunction);
		return $result;
	}

	/**
	 * @param array $allFunctions
	 * @param string $function
	 * @return array|null
	 */
	protected function isDocumentFunction($allFunctions, $function)
	{
		if (isset($allFunctions[$function]))
		{
			$functionData = $allFunctions[$function];
			return isset($functionData['document']) ? (bool)$functionData['document'] : false;
		}
		return false;
	}
}