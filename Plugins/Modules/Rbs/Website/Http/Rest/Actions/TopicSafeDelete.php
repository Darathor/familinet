<?php
/**
 * Copyright (C) 2016 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Http\Rest\Actions;

/**
 * @name \Rbs\Website\Http\Rest\Actions\TopicSafeDelete
 */
class TopicSafeDelete
{
	protected $originalActionCallback;

	/**
	 * @param $originalActionCallback
	 */
	public function __construct($originalActionCallback)
	{
		$this->originalActionCallback = $originalActionCallback;
	}

	/**
	 * @param \Change\Http\Event $event
	 * @throws \RuntimeException
	 */
	public function delete(\Change\Http\Event $event)
	{
		$applicationServices = $event->getApplicationServices();
		$document = $applicationServices->getDocumentManager()->getDocumentInstance((int)$event->getParam('documentId'));
		if ($document instanceof \Rbs\Website\Documents\Topic)
		{
			$i18nManager = $applicationServices->getI18nManager();
			$errorResult = null;
			$node = $applicationServices->getTreeManager()->getNodeByDocument($document);
			if ($node && $node->hasChildren())
			{
				$errorResult = new \Change\Http\Rest\V1\ErrorResult(
					'TOPIC-NOT-EMPTY', $i18nManager->trans('m.rbs.website.admin.unable_to_delete_not_empty_topic'), \Zend\Http\Response::STATUS_CODE_409);
				$event->setResult($errorResult);
				return;
			}
			call_user_func($this->originalActionCallback, $event);
		}
	}
}