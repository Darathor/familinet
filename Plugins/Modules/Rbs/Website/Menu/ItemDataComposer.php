<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Menu;

/**
 * @name \Rbs\Website\Menu\ItemDataComposer
 */
class ItemDataComposer
{
	use \Change\Http\Ajax\V1\Traits\DataComposer;

	/**
	 * @var \Change\Documents\AbstractDocument
	 */
	protected $document;

	/**
	 * @param \Change\Events\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array $context
	 */
	public function __construct($event, $document, array $context)
	{
		$this->document = $document;

		$this->setContext(is_array($context) ? $context : []);
		$this->setServices($event->getApplicationServices());
	}

	protected function generateDataSets()
	{
		if (!$this->document)
		{
			$this->dataSets = [];
			return;
		}
		$this->generateTypologyDataSet($this->document);
	}
}