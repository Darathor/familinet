<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Preview;

/**
 * @name \Rbs\Website\Preview\PreviewHelper
 */
class PreviewHelper
{
	/**
	 * @param \Change\Http\Event $event
	 * @param \Change\Documents\AbstractDocument $document
	 * @param array|null $restProperties
	 */
	public function preparePreview($event, \Change\Documents\AbstractDocument $document, $restProperties = null)
	{
		$documentManager = $event->getApplicationServices()->getDocumentManager();
		$document->load();
		if (is_array($restProperties))
		{
			// Handle modified properties.
			$newEvent = new \Change\Http\Event();
			$request = new \Change\Http\Request();
			$request->getPost()->fromArray($restProperties);
			$newEvent->setRequest($request);
			$result = $document->populateDocumentFromRestEvent($newEvent);
			if ($result === false)
			{
				throw new \RuntimeException('Invalid properties: ' . var_export($restProperties, true), 99999);
			}

			//// Handle modified attributes.
			if (isset($restProperties['typology$']['__id']))
			{
				$attributes = $restProperties['typology$'];
				$typologyId = $attributes['__id'] ? (int)$attributes['__id'] : 0;
				unset($attributes['__id']);

				$LCID = $documentManager->getLCID();
				if ($typologyId)
				{
					$oldRawValues = $document->getCachedAttributesValue();

					$document->setCachedTypologyId($typologyId);
					$document->setCachedAttributesValue([]);

					$typology = $documentManager->getTypology($typologyId);
					if ($typology instanceof \Rbs\Generic\Attributes\Typology)
					{
						$values = new \Change\Documents\Attributes\AttributeValues($LCID, $attributes);
						$document->setCachedAttributesValue($typology->normalizeValues($oldRawValues, $values, $LCID) ?: []);
					}
				}
				else
				{
					$document->setCachedTypologyId(0);
					$document->setCachedAttributesValue([]);
				}
			}
		}

		// Make sure that if the document is activable, it is activated.
		if ($document instanceof \Change\Documents\Interfaces\Activable)
		{
			if (!$document->activated())
			{
				/** @var \Change\Documents\AbstractDocument $document */
				$model = $document->getDocumentModel();
				$property = $model->getProperty('active');
				if ($property)
				{
					$property->setValue($document, true);
				}
				$property = $model->getProperty('startActivation');
				if ($property)
				{
					$property->setValue($document, null);
				}
				$property = $model->getProperty('endActivation');
				if ($property)
				{
					$property->setValue($document, null);
				}
			}
		}
		// Make sure that if the document is publishable, it is published.
		elseif ($document instanceof \Change\Documents\Interfaces\Publishable)
		{
			if (!$document->published())
			{
				/** @var \Change\Documents\AbstractDocument $document */
				$model = $document->getDocumentModel();
				$property = $model->getProperty('publicationStatus');
				if ($property)
				{
					$property->setValue($document, \Change\Documents\Interfaces\Publishable::STATUS_PUBLISHABLE);
				}
				$property = $model->getProperty('startPublication');
				if ($property)
				{
					$property->setValue($document, null);
				}
				$property = $model->getProperty('endPublication');
				if ($property)
				{
					$property->setValue($document, null);
				}
			}
		}
	}

	/**
	 * @param \Change\Http\Event $event
	 * @param array $context
	 */
	public function setContext($event, $context)
	{
		$userId = $context['userId'] ?? null;
		if ($userId)
		{
			// User login.
			$authenticationManager = $event->getAuthenticationManager();
			$em = $authenticationManager->getEventManager();
			$args = $em->prepareArgs(['userId' => $userId]);
			$loginEvent = new \Change\Events\Event(\Change\User\AuthenticationManager::EVENT_LOGIN, $authenticationManager, $args);
			$em->triggerEvent($loginEvent);
			$user = $loginEvent->getParam('user');
			if (!($user instanceof \Change\User\UserInterface))
			{
				throw new \RuntimeException('Invalid userId: ' . $userId, 99999);
			}
		}
	}
}