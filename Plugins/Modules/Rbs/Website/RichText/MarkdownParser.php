<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\RichText;
use Change\Presentation\RichText\ParserInterface;

/**
 * @name \Rbs\Website\RichText\MarkdownParser
 */
class MarkdownParser extends \Change\Presentation\RichText\MarkdownParser implements ParserInterface
{
	/**
	 * @var \Rbs\Website\Documents\Website
	 */
	protected $website;

	/**
	 * @param string $rawText
	 * @param array $context
	 * @return string
	 */
	public function parse($rawText, $context)
	{
		if (isset($context['website']))
		{
			$this->website = $context['website'];
		}
		return $this->transform($rawText);
	}

	/**
	 * @param $matches
	 * @return string
	 */
	protected function _doAnchors_inline_callback($matches)
	{
		$link_text  = $this->runSpanGamut($matches[2]);
		$documentId = $matches[3] == '' ? $matches[4] : $matches[3];
		$title      = $matches[7] ?? null;

		$params = explode(',', $documentId);
		$model = null;

		$id = null;
		if (count($params) === 1)
		{
			$id = $params[0];
		}
		elseif (count($params) === 2)
		{
			$model = $this->applicationServices->getModelManager()->getModelByName($params[0]);
			$id = $params[1];
		}

		// If the id is not numeric, this is an external link, so use de default link rendering.
		if (!is_numeric($id))
		{
			return parent::_doAnchors_inline_callback($matches);
		}

		$document = $this->applicationServices->getDocumentManager()->getDocumentInstance($id, $model);

		if ($document instanceof \Rbs\Media\Documents\File)
		{
			return $this->parseDownloadLinkTag($document, $title, $link_text);
		}
		if ($document instanceof \Change\Documents\AbstractDocument && $document->getDocumentModel()->isPublishable())
		{
			return $this->parseDocumentLinkTag($document, $title, $link_text);
		}
		return $this->hashPart('<span class="label label-danger">Invalid Document: ' . $documentId . '</span>');
	}

	/**
	 * @param \Rbs\Media\Documents\File $document
	 * @param string $title
	 * @param string $link_text
	 * @return string
	 */
	protected function parseDownloadLinkTag($document, $title, $link_text)
	{
		if ($this->website)
		{
			// TODO clean up when urlManager will be refactored.
			$urlManager = $this->website->getUrlManager($this->website->getLCID());
			$urlManager->setPathRuleManager($this->applicationServices->getPathRuleManager());
			$url = $urlManager->getActionURL('Rbs_Media', 'Download', ['documentId' => $document->getId()]);
		}
		else
		{
			$url = 'javascript:;';
		}

		$result = '<a href="' . $url . '"';
		if (isset($title))
		{
			$title = $this->encodeAttribute($title);
			$result .= ' title="' . $title . '"';
		}

		$link_text = $this->runSpanGamut($link_text);

		$size = $this->applicationServices->getI18nManager()->transFileSize($document->getContentLength());
		$extension = strtoupper($document->getExtension());
		$collection = $this->applicationServices->getCollectionManager()->getCollection('Rbs_Media_FileExtensions');
		if ($collection)
		{
			$item = $collection->getItemByValue(strtolower($extension));
			if ($item)
			{
				$extension = '<abbr title="' . $item->getTitle() . '">' . $extension . '</abbr>';
			}
		}
		$result .= '>' . $link_text . '</a> [' . $extension . ' — ' . $size . ']';

		return $this->hashPart($result);
	}

	/**
	 * @param \Change\Documents\AbstractDocument $document
	 * @param string $title
	 * @param string $link_text
	 * @return string
	 */
	protected function parseDocumentLinkTag($document, $title, $link_text)
	{
		if ($this->website)
		{
			// TODO clean up when urlManager will be refactored.
			$urlManager = $this->website->getUrlManager($this->website->getLCID());
			$urlManager->setPathRuleManager($this->applicationServices->getPathRuleManager());
			$url = $urlManager->getCanonicalByDocument($document);
		}
		else
		{
			$url = 'javascript:;';
		}

		$result = '<a href="' . $url . '"';
		if (isset($title))
		{
			$title = $this->encodeAttribute($title);
			$result .= ' title="' . $title . '"';
		}

		$link_text = $this->runSpanGamut($link_text);
		$result .= '>' . $link_text . '</a>';

		return $this->hashPart($result);
	}

	/**
	 * @param $matches
	 * @return string
	 */
	protected function _doLists_callback($matches)
	{
		// Re-usable patterns to match list item bullets and number markers:
		$marker_ul_re  = '[*+-]';
		$marker_ol_re  = '\d+[\.]';

		$list = $matches[1];
		$list_type = preg_match("/$marker_ul_re/", $matches[4]) ? 'ul' : 'ol';

		$marker_any_re = ($list_type == 'ul' ? $marker_ul_re : $marker_ol_re);

		$list .= "\n";
		$result = $this->processListItems($list, $marker_any_re);
		$result = $this->hashBlock("<$list_type class=\"bullet\">\n" . $result . "</$list_type>");
		return "\n". $result ."\n\n";
	}

	/**
	 * This method is overridden to add the "table" class on the table (cf Bootstrap markup).
	 * @param $matches
	 * @return string
	 */
	protected function _doTable_callback($matches)
	{
		$head = $matches[1];
		$underline = $matches[2];
		$content = $matches[3];

		# Remove any tailing pipes for each line.
		$head = preg_replace('/[|] *$/m', '', $head);
		$underline = preg_replace('/[|] *$/m', '', $underline);
		$content = preg_replace('/[|] *$/m', '', $content);

		# Reading alignment from header underline.
		$separators = preg_split('/ *[|] */', $underline);
		$attr = [];
		foreach ($separators as $n => $s)
		{
			if (preg_match('/^ *-+: *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('right');
			}
			else if (preg_match('/^ *:-+: *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('center');
			}
			else if (preg_match('/^ *:-+ *$/', $s))
			{
				$attr[$n] = $this->_doTable_makeAlignAttr('left');
			}
			else
			{
				$attr[$n] = '';
			}
		}

		# Parsing span elements, including code spans, character escapes,
		# and inline HTML tags, so that pipes inside those gets ignored.
		$head = $this->parseSpan($head);
		$headers = preg_split('/ *[|] */', $head);
		$col_count = count($headers);
		$attr = array_pad($attr, $col_count, '');

		# Write column headers.
		$text = "<table class=\"table\">\n";
		$text .= "<thead>\n";
		$text .= "<tr>\n";
		foreach ($headers as $n => $header)
		{
			$text .= "  <th$attr[$n]>" . $this->runSpanGamut(trim($header)) . "</th>\n";
		}
		$text .= "</tr>\n";
		$text .= "</thead>\n";

		# Split content by row.
		$rows = explode("\n", trim($content, "\n"));

		$text .= "<tbody>\n";
		foreach ($rows as $row)
		{
			# Parsing span elements, including code spans, character escapes,
			# and inline HTML tags, so that pipes inside those gets ignored.
			$row = $this->parseSpan($row);

			# Split row by cell.
			$row_cells = preg_split('/ *[|] */', $row, $col_count);
			$row_cells = array_pad($row_cells, $col_count, '');

			$text .= "<tr>\n";
			foreach ($row_cells as $n => $cell)
			{
				$text .= "  <td$attr[$n]>" . $this->runSpanGamut(trim($cell)) . "</td>\n";
			}
			$text .= "</tr>\n";
		}
		$text .= "</tbody>\n";
		$text .= '</table>';

		return $this->hashBlock($text) . "\n";
	}
}