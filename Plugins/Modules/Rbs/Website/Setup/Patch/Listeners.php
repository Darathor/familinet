<?php
/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Website\Setup\Patch;

/**
 * @name \Rbs\Website\Setup\Patch\Listeners
 * @ignore
 */
class Listeners extends \Change\Plugins\Patch\AbstractListeners
{
	/**
	 * @param \Change\Events\Event $event
	 */
	protected function onApply(\Change\Events\Event $event)
	{
		//Rbs_Website_0001 v1.1 Fill supportedFunctionsCode property for static pages.

		//Rbs_Website_0002 v2.1 Migrate realm property on website document
		$this->executePatch('Rbs_Website_0002', 'Migrate realm property on website document', [$this, 'patch0002']);
	}

	/**
	 * @param \Change\Plugins\Patch\Patch $patch
	 * @throws \Exception
	 */
	protected function patch0002(\Change\Plugins\Patch\Patch $patch)
	{
		$applicationServices = $this->applicationServices;
		$dbProvider = $this->applicationServices->getDbProvider();
		$transactionManager = $applicationServices->getTransactionManager();
		try
		{
			$transactionManager->begin();
			$qb = $dbProvider->getNewStatementBuilder();
			$fb = $qb->getFragmentBuilder();
			$qb->update($fb->getDocumentTable('Rbs_Website_Section'));
			$qb->assign($fb->getDocumentColumn('realm'), $fb->string('web'));
			$qb->where(
				$fb->logicAnd(
					$fb->eq($fb->getDocumentColumn('model'), $fb->string('Rbs_Website_Website')),
					$fb->isNull($fb->getDocumentColumn('realm'))
				)
			);
			$updateQuery = $qb->updateQuery();
			$websitesUpdated = $updateQuery->execute();
			$transactionManager->commit();
		}
		catch (\Exception $e)
		{
			$this->sendError($e->getMessage());
			$patch->setException($e);
			throw $transactionManager->rollBack($e);
		}

		$this->sendInfo('Websites updated ' . $websitesUpdated);
		$patch->addInstallationData('websitesUpdated', $websitesUpdated);

		// Add the entry in change_patch saying that this patch is successfully installed and should not be installed any more.
		$this->patchManager->installedPatch($patch);
	}
}