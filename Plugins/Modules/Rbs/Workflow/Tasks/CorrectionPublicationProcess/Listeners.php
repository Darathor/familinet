<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Workflow\Tasks\CorrectionPublicationProcess;

/**
 * @name \Rbs\Workflow\Tasks\CorrectionPublicationProcess\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			$task = new RequestValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('requestValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new ContentValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('contentValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new PublicationValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('publicationValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new Cancel();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('cancel', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new ContentMerging();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('contentMerging', $callback, 5);
	}
}