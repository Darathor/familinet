<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Rbs\Workflow\Tasks\PublicationProcess;

/**
 * @name \Rbs\Workflow\Tasks\PublicationProcess\Listeners
 * @ignore
 */
class Listeners extends \Zend\EventManager\AbstractListenerAggregate
{
	/**
	 * Attach one or more listeners
	 * @param \Zend\EventManager\EventManagerInterface $events
	 * @param int $priority
	 * @return void
	 */
	public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1)
	{
		$callback = function (\Change\Events\Event $event)
		{
			$task = new RequestValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('requestValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new ContentValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('contentValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new CheckPublication();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('checkPublication', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new PublicationValidation();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('publicationValidation', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new Freeze();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('freeze', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new Unfreeze();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('unfreeze', $callback, 5);

		$callback = function (\Change\Events\Event $event)
		{
			$task = new File();
			$task->execute($event);
		};
		$this->listeners[] = $events->attach('file', $callback, 5);
	}
}