/**
 * Copyright (C) 2015 Proximis
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(__change) {
	"use strict";

	var app = angular.module('RbsChangeApp');

	app.config(['RbsChange.AjaxAPIProvider', function(AjaxAPIProvider) {
		if (__change && __change.previewKey) {
			AjaxAPIProvider.setHeader('Change-Preview-Key', __change.previewKey);
		}
	}]);
})(window.__change);