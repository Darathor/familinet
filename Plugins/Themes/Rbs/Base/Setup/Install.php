<?php
/**
 * Copyright (C) 2014 Ready Business System
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace Themes\Rbs\Base\Setup;

/**
 * @name \Themes\Rbs\Base\Setup\Install
 */
class Install extends \Change\Plugins\ThemeInstallBase
{
	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Application $application
	 * @param \Change\Configuration\EditableConfiguration $configuration
	 * @throws \RuntimeException
	 */
	public function executeApplication($plugin, $application, $configuration)
	{
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/menuItemThumbnail', '300x300');

		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/listItem', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/setItem', '80x60');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/pictogram', '60x45');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detail', '540x405');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detailThumbnail', '80x60');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/detailCompact', '450x300');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/attribute', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/selectorItem', '30x30');

		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/cartItem', '160x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/shortCartItem', '80x120');
		$configuration->addPersistentEntry('Rbs/Media/namedImageFormats/modeThumbnail', '80x60');

		parent::executeApplication($plugin, $application, $configuration);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Plugins\PluginManager $pluginManager
	 * @param \Change\Application $application
	 */
	public function executeThemeAssets($plugin, $pluginManager, $application)
	{
		$workspace = $application->getWorkspace();
		$webThemeAssetsBaseDirectory = $workspace->composePath($pluginManager->getWebAssetsBaseDirectory(), 'Theme');

		$themeAssets = new \Change\Presentation\Themes\ThemeAssets($application);

		$modules = $pluginManager->getModules();
		$defaultTheme = $this->getDefaultThemePlugin($themeAssets, $workspace, $modules);
		$assetBaseDir = $workspace->composePath($webThemeAssetsBaseDirectory, $defaultTheme->getVendor(), $defaultTheme->getShortName());

		$themeAssets->installPluginTemplates($plugin, $defaultTheme);
		$themeAssets->installPluginAssets($plugin, $assetBaseDir);
		foreach ($modules as $module)
		{
			$themeAssets->installPluginTemplates($module, $defaultTheme);
			$themeAssets->installPluginAssets($module, $assetBaseDir);
		}
		$defaultTheme->writeConfiguration($defaultTheme->getAssetConfiguration());

		\Change\Stdlib\FileUtils::mkdir(dirname($this->imageLessFilePath));
		file_put_contents($this->imageLessFilePath, $themeAssets->generateImageLessContent($this->imageFormats));

		\Change\Stdlib\FileUtils::mkdir(dirname($this->imageScssFilePath));
		file_put_contents($this->imageScssFilePath, $themeAssets->generateImageScssContent($this->imageFormats));

		$am = $themeAssets->getAsseticManager($defaultTheme);
		$themeAssets->write($am);
	}

	/**
	 * @param \Change\Plugins\Plugin $plugin
	 * @param \Change\Services\ApplicationServices $applicationServices
	 * @throws \Exception
	 */
	public function executeServices($plugin, $applicationServices)
	{
	}
}