#!/bin/bash
cd ChangeTests/ || exit
if [[ -z "$1" ]];then
	echo "Run all tests"
	mysql -u root -pf -e "DROP DATABASE IF EXISTS familinettest; CREATE DATABASE familinettest;"
	../vendor/bin/phpunit --testsuite change -v #--coverage-html ../tests/report/core
	echo ""
	mysql -u root -pf -e "DROP DATABASE IF EXISTS familinettest; CREATE DATABASE familinettest;"
	../vendor/bin/phpunit --testsuite plugins -v #--coverage-html ../tests/report/plugins
	echo ""
	mysql -u root -pf -e "DROP DATABASE IF EXISTS familinettest; CREATE DATABASE familinettest;"
	../vendor/bin/phpunit --testsuite app -v
	echo ""
	mysql -u root -pf -e "DROP DATABASE IF EXISTS familinettest; CREATE DATABASE familinettest;"
	../vendor/bin/phpunit --testsuite perf -v
else
	echo "Use $1"
	mysql -u root -pf -e "DROP DATABASE IF EXISTS familinettest; CREATE DATABASE familinettest;"
	../vendor/bin/phpunit ../$1
fi
cd ..