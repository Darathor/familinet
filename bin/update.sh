#!/bin/sh
echo '\n- Update Familinet:'
git pull --rebase origin master

echo '- Update Change:'
php composer.phar self-update
php composer.phar install
